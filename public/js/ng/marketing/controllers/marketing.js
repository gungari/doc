hb_marketing_app.controller('marketingController',function($scope, $http){
	$scope.loadDataProfileMarketing = function(){
		$("#menu-left .left-menu.profile").activate_left_menu();
		$scope.marketing = [];
		$scope.marketing_edit = [];
		$http.get(SITE_URL+"marketing/data/index")
			.then(function(response){
				$scope.marketing = response.data.record;
				$scope.marketing_edit = angular.copy($scope.marketing);
			});
	};
	
	$scope.saveDataMarketing = function(update_field, event){
		$(event.target).closest("form").find(".show_error").html("");
		$http({
			url:SITE_URL + "marketing/data/update",
			data : $.param({"dt":$scope.marketing_edit, "update_field":update_field}),
			method : 'POST',
			headers : {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
		}).then(function(response){
			if (response.data.status == "00"){
				$scope.marketing = response.data.record;
				$scope.marketing_edit = angular.copy($scope.marketing);
				$(event.target).closest("form").find(".cancel a").click();
			}else{
				$(event.target).closest("form").find(".show_error").html(response.data.error);
			}
		});
	}
	
	$scope.loadDataVendorRefrence = function(){
		$("#menu-left .left-menu.vendor_refrence").activate_left_menu();
		$scope.vendor_refrences = [];
		$http.get(SITE_URL+"marketing/data/vendor_refrence")
			.then(function(response){
				$scope.vendor_refrences = response.data.record;
				$scope.show_vendor_not_found_msg = $scope.vendor_refrences.qty_vendors == 0;
			});
	};
});
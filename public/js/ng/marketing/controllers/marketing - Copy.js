hb_marketing_app.controller('masterController',function($scope, $http){
	//Agent
	$scope.loadDataAgent = function(){
		$scope.agents = [];
		$scope.show_loading_div= true;
		$http.get(SITE_URL+"pilot/master/agent_getData_ng")
			.then(function(response){
				$scope.agents = response.data.records;
				$scope.show_loading_div= false;
				$scope.checkPagination($scope.agents);
			});
	};
	$scope.editDataAgent = function(id_agent){
		$.fancybox({'type':'ajax', modal : true, 'href':SITE_URL+'pilot/master/agent_add/'+id_agent});
	}
	
	//Customer
	$scope.loadDataCustomers = function(){
		$scope.customers = [];
		$scope.show_loading_div= true;
		$http.get(SITE_URL + "pilot/master/customer_getData_ng")
			.then(function(response){
				$scope.customers = response.data.records;
				$scope.show_loading_div= false;
				$scope.checkPagination($scope.customers);
			});
	};
	$scope.editDataCustomers = function(id_customer){
		$.fancybox({'type':'ajax', modal : true, 'href':SITE_URL+'pilot/master/customer_add/'+id_customer});
	}
	
	//Staff
	$scope.loadDataStaff = function(){
		$scope.staffs = [];
		$scope.show_loading_div= true;
		$http.get(SITE_URL + "pilot/master/staff_getData_ng")
			.then(function(response){
				$scope.staffs = response.data.records;
				$scope.show_loading_div= false;
				$scope.checkPagination($scope.staffs);
			});
	};
	$scope.editDataStaff = function(id_staff){
		$.fancybox({'type':'ajax', modal : true, 'href':SITE_URL+'pilot/master/staff_add/'+id_staff});
	}
	
	//Menu Operator
	$scope.loadDataMenu = function(){
		$scope.menus = [];
		$scope.show_loading_div= true;
		$http.get(SITE_URL + "pilot/master/menu_getData_ng")
			.then(function(response){
				$scope.menus = response.data.records;
				$scope.show_loading_div= false;
				$scope.checkPagination($scope.menus);
			});
	};
	$scope.editDataMenu = function(id_menu){
		$.fancybox({'type':'ajax', modal : true, 'href':SITE_URL+'pilot/master/menu_add/'+id_menu});
	}
	
	//Menu Online
	$scope.loadDataMenuOnline = function(){
		$scope.menus_online = [];
		$scope.show_loading_div= true;
		$http.get(SITE_URL + "pilot/master/menu_online_getData_ng")
			.then(function(response){
				$scope.menus_online = response.data.records;
				$scope.show_loading_div= false;
				$scope.checkPagination($scope.menus_online);
			});
	};
	$scope.editDataMenuOnline = function(id_menu){
		$.fancybox({'type':'ajax', modal : true, 'href':SITE_URL+'pilot/master/menu_online_add/'+id_menu});
	}
	
	//Hotel
	$scope.loadDataHotel = function(){
		$scope.hotels = [];
		$scope.show_loading_div= true;
		$http.get(SITE_URL + "pilot/master/hotel_getData_ng")
			.then(function(response){
				$scope.hotels = response.data.records;
				$scope.show_loading_div= false;
				$scope.checkPagination($scope.hotels);
			});
	};
	$scope.editDataHotel = function(id_hotel){
		$.fancybox({'type':'ajax', modal : true, 'href':SITE_URL+'pilot/master/hotel_add/'+id_hotel});
	}
	
	//Room type
	$scope.loadDataRoomType = function(){
		$scope.room_types = [];
		$scope.show_loading_div= true;
		$http.get(SITE_URL + "pilot/master/room_type_getData_ng")
			.then(function(response){
				$scope.room_types = response.data.records;
				$scope.show_loading_div= false;
				$scope.checkPagination($scope.room_types);
			});
	};

	//Room
	$scope.loadDataRoom = function(){
		$scope.rooms = [];
		$scope.show_loading_div= true;
		$http.get(SITE_URL + "pilot/master/room_getData_ng")
			.then(function(response){
				$scope.rooms = response.data.records;
				$scope.show_loading_div= false;
				$scope.checkPagination($scope.rooms);
			});
	};
	
	$scope.editData = function(master_edit_link){
		$.fancybox({'type':'ajax', modal : true, 'href':SITE_URL+'pilot/master/'+master_edit_link});
	}
	
	//--------
	$scope.dt = [];
	$scope.editData_ng = function(dt_edit){
		$scope.dt = [];
		$scope.dt = angular.copy(dt_edit);
	}
	$scope.saveData = function(save_url){
		console.log($.param($scope.dt));
		
		$http({
			url:SITE_URL + save_url,
			data : $.param({"dt":$scope.dt}),
			method : 'POST',
			headers : {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
		}).then(function(response){
				$scope.result = response.data;
				console.log(response.data);
			});
		
		/*$http.post(SITE_URL + save_url, $.param($scope.dt), {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'})
			.then(function(response){
				$scope.result = response.data;
				console.log(response.data);
			});*/
	}
	
});
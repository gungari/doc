hb_marketing_app.config(function($routeProvider){
  	$routeProvider
	.when('/',{
		templateUrl : VIEW_URL+'marketing/v_ng_home.php',
		controller : 'marketingController'
  	})
  	.when('/vendor_refrence',{
		templateUrl : VIEW_URL+'marketing/v_ng_vendor_refrence.php',
		controller : 'marketingController'
  	})
	
	.when('',{
		redirectTo : '/',
  	})
  	.otherwise({
		redirectTo : '/'
  	})
});
// JavaScript Document
$(document).on('submit','form.ajax_frm_crud', function() {
		var mydata = $(this).serialize();
		var form = $(this);
		$.ajax({
			type	: "POST",
			url		: form.attr("action"),
			data	: mydata,
			beforeSend : function(){
				form.find(".show_error").slideUp().html("");
				form.find("button").attr("disabled","disabled");
			},
			success: function(response, textStatus, xhr) {

				if (response == "00"){
					if (form.data("reloadaftersuccess")){
						location.reload();
					}else if(form.data("hrefaftersuccess")){
						window.location = form.data("hrefaftersuccess");
					}
				}else{
					var IS_JSON = true;
				   	try {
						var json = $.parseJSON(response);
				   	}catch(err){
						IS_JSON = false;
				   	}
					
					if (IS_JSON){
						if (json.status == "00"){
							if (json.hasOwnProperty('redirect_url')){
								window.location = json.redirect_url;
							}else if (json.hasOwnProperty('show_msg')){
								$('html, body').animate({scrollTop: form.find(".show_error").offset().top - 60}, 400);
								form.find(".show_error").hide().html(json.show_msg).slideDown("fast");
								form.find("button").removeAttr("disabled");
							
							}
							if(form.data("callback")){
								var callback_fn = window[form.data("callback")];;
								if (typeof(callback_fn) == "function"){ callback_fn(); }
							}
						}else{
							$('html, body').animate({scrollTop: form.find(".show_error").offset().top - 60}, 400);
							form.find(".show_error").hide().html(json.error).slideDown("fast");
							form.find("button").removeAttr("disabled");
						}
					}else{
						$('html, body').animate({scrollTop: form.find(".show_error").offset().top - 60}, 400);
						form.find(".show_error").hide().html(response).slideDown("fast");
						form.find("button").removeAttr("disabled");
					}
				}
				if(form.data("callback00")){
					var callback_fn = window[form.data("callback00")];;
					if (typeof(callback_fn) == "function"){ callback_fn(); }
				}
			},
			error: function(xhr, textStatus, errorThrown) {
				form.find("button").removeAttr("disabled");
			}
		});
		return false;
	});
	
/*$(document).on('submit','form.ajax_frm_crud', function() {
	var mydata = $(this).serialize();
	var form = $(this);
	$.ajax({
		type	: "POST",
		url		: form.attr("action"),
		data	: mydata,
		beforeSend : function(){
			form.find(".show_error").slideUp().html("");
			form.find("button").attr("disabled","disabled");
		},
		success: function(response, textStatus, xhr) {
			if (response == "00"){
				if (form.data("reloadaftersuccess")){
					location.reload();
				}else if(form.data("hrefaftersuccess")){
					window.location = form.data("hrefaftersuccess");
				}
			}else{
				$('html, body').animate({scrollTop: form.find(".show_error").offset().top - 60}, 400);
				form.find(".show_error").hide().html(response).slideDown("fast");
				form.find("button").removeAttr("disabled");
			}
		},
		error: function(xhr, textStatus, errorThrown) {
			form.find("button").removeAttr("disabled");
		}
	});
	return false;
});*/

$(document).on('keyup','.txa_mxlen', function() {
	var max_length = $(this).data("mxlen");
	if (this.value.length >= max_length){ this.value=this.value.substring(0,max_length); } 
	$(this).parent().find(".dv_mxlen_remain").text(max_length-this.value.length+"/"+max_length);
});

$(document).ready(function(e) {
	$.fancybox_activate = function(){
		$(".fancybox.ajax").fancybox({type:"ajax"});
		$(".fancybox.ajax-data").click(function(){$.fancybox({'type':'ajax', 'href':$(this).data("href")}); return false;});
		$("button.btn-fancybox").click(function(){$.fancybox({'type':'ajax', modal : false, 'href':$(this).data("href")});});
		$("button.btn-fancybox-div").click(function(){$.fancybox({'href':$(this).data("href")});});
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		$(".fancybox").fancybox();
		$(".fancybox.iframe").fancybox({type:"iframe"});
	}
	$.fancybox_activate();

	$.fn.submit_loading = function(){
		var loading_html = '<div id="loading_page">' + '<div class="loading_box">' + '<div class="image"></div>' + '<div class="desc"><img src="'+base_url+'public/images/loading_bar.gif" /></div>' + '</div>' + '</div>';
		$("body").find("#loading_page").remove();
		$("body").prepend(loading_html);
	};
	$("form.submit_loading").submit(function(){$(this).submit_loading();});
	$(".click-submit-loading").click(function(){$(this).submit_loading();});
	$.fn.remove_submit_loading = function(){$("body").find("#loading_page").remove();};
	
});

function editor(selector){
	tinymce.init({
		selector: selector,
		plugins: [
			"advlist autolink lists link image charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste textcolor" //jbimages
		],
		//styleselect 
		toolbar: "insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | link image | forecolor backcolor fontsizeselect",
		statusbar: true,
		relative_urls: false,
	});
}

function formatNumber(number, curency){
	if (curency == "IDR"){
		return accounting.formatNumber(number,0,".");
	}else{
		return accounting.formatNumber(number,2,",")
	}
}

$(document).on("keyup input", "textarea.autoheight", function(){
	var _this	= this;
	var offset 	= this.offsetHeight - this.clientHeight;
	$(this).css('height', 'auto').css('height', _this.scrollHeight + offset);
});

$(document).ready(function(e) {
	$(document).on("click", ".inline-edit .inline-edit-icon a", function(){
	//$(".inline-edit .inline-edit-icon a").click(function(){
		var _this = $(this);
		_this.closest(".main-text").hide();
		_this.closest(".inline-edit").find(".edit-text").show();
		_this.closest(".inline-edit").find("form input:first").focus();
		if(_this.data("callback")){
			var callback_fn = window[_this.data("callback")];;
			if (typeof(callback_fn) == "function"){ callback_fn(); }
		}
		return false;
	});
	$(document).on("click", ".inline-edit .cancel a", function(){
	//$(".inline-edit .cancel a").click(function(){
		var _this = $(this);
		_this.closest(".inline-edit").find(".main-text").show();
		_this.closest(".inline-edit").find(".edit-text").hide();
		return false;
	});
	$(document).on("submit", ".inline-edit form", function(){
	//$(".inline-edit form").submit(function(){
		var mydata = $(this).serialize();
		var form = $(this);
		
		$.ajax({
			type	: "POST",
			url		: form.attr("action"),
			data	: mydata,
			beforeSend : function(){
				form.find(".show_error").slideUp().html("");
				form.find("button").attr("disabled","disabled");
			},
			success: function(response) {
				var IS_JSON = true;
				try {
					var json = $.parseJSON(response);
				}catch(err){
					IS_JSON = false;
				}
				if (IS_JSON){
					if (json.status == "00"){
						form.closest(".inline-edit").find(".main-text .value").html(json.value);
						form.find(".cancel a").click();
						form.find("button").removeAttr("disabled");
					}else{
						//$('html, body').animate({scrollTop: form.find(".show_error").offset().top - 60}, 400);
						form.find(".show_error").hide().html(json.error).slideDown("fast");
						form.find("button").removeAttr("disabled");
					}
				}else{
					//$('html, body').animate({scrollTop: form.find(".show_error").offset().top - 60}, 400);
					form.find(".show_error").hide().html(response).slideDown("fast");
					form.find("button").removeAttr("disabled");
				}
			},
		});
		return false;
	});
});

<?php
class M_boats extends CI_Model {
    function __construct(){
		parent::__construct();
    }
	
	//Query to get all data rooms
	public function search($lang){
				
		$query =  $this->db	 ->where("tb_trans_boat_lang.f_lang",$lang)
							 ->where("tb_trans_boat.f_deleted <> ", '1')
							 ->join("tb_trans_boat_lang","tb_trans_boat.f_id_boat=tb_trans_boat_lang.f_id_boat")
							 ->get("tb_trans_boat");
		
		if ($query->num_rows() > 0){
			$boats = $query->result_array();

			$arr_boats = array();
			foreach ($boats as $boat){
				$arr_boats[] = $this->generateDataBoat($boat);
			}
			return $arr_boats;
			
		}else{
			return FALSE;
		}
	}
	private function generateDataBoat($boats){

		$boat = array();
		$boat["id"] 				= $boats["f_id_boat"];
		$boat["boat_code"] 			= $boats["f_code_boat"];
		$boat["name"] 				= $boats["f_boat_name"];
		$boat["description"] 		= $boats["f_description"];
		$boat["main_image"]			= ($boats["f_main_image_gallery"]!="")?$this->fn->resize_image(100,500,0,"webimages/trans_boat/gallery/$boats[f_main_image_gallery]"):"";
		$boat["lang"] 				= $boats["f_lang"];
		$boat["publish_status"] 	= $boats["f_status"];
		
		return $boat;
	}
	
	//Get Data boats fom current vendor
	public function getBoats($vendor, $lang = "", $status = ""){
		if ($lang == "") $lang = $vendor["default_language"];
		
		$this->db->where("tb_trans_boat.f_id_vendor",$vendor["id"]);
		
		if (strtoupper($status) == "1" || strtoupper($status) == "0"){
			$this->db->where("tb_trans_boat.f_status", $status);
		}elseif (strtoupper($status) != "ALL"){
			$this->db->where("tb_trans_boat.f_status", '1');
		}
		
		$boats = $this->search($lang);
		if (count($boats) > 0){
			return $boats;
		}else{
			return FALSE;
		}
	}
	public function getBoatByID($vendor, $boat_id, $lang = ""){
		if ($lang == "") $lang = $vendor["default_language"];
		
		$this->db->where("tb_trans_boat.f_id_boat",		$boat_id);
		$this->db->where("tb_trans_boat.f_id_vendor",	$vendor["id"]);
		
		$boats = $this->search($lang);
		
		if (count($boats) > 0){
			$boat = $boats[0];
			
			return $boat;
		}else{
			return FALSE;
		}
	}
	public function getBoatByCode($vendor, $boat_code, $lang = ""){
		if ($lang == "") $lang = $vendor["default_language"];
		
		$this->db->where("tb_trans_boat.f_code_boat",	$boat_code);
		$this->db->where("tb_trans_boat.f_id_vendor",	$vendor["id"]);
		
		$boats = $this->search($lang);
		
		if (count($boats) > 0){
			$boat = $boats[0];
			
			return $boat;
		}else{
			return FALSE;
		}
	}
	public function getBoatDetailByCode($vendor, $boat_code, $lang = ""){
		
		$boat = $this->getBoatByCode($vendor, $boat_code, $lang);
		
		if ($boat){
			//Get Gallery
			$boat["gallery"] = $this->getGallery("$boat[id]");
			//--
			return $boat;
		}else{
			return FALSE;
		}
	}
	private function getGallery($id_boat){
		$gallery = $this->db->where("f_id_boat", $id_boat)
							->get("tb_trans_boat_gallery")
							->result_array();
		$gallerys = array();
		
		foreach ($gallery as $index => $img) {
			$gallerys["$index"]["image"] = $this->fn->resize_image(100, 500, 0, "webimages/trans_boat/gallery/$img[f_image]");
			$gallerys["$index"]["title"] = $img['f_title'];
			if ($img["f_default_image"] == "1"){
				$gallerys["$index"]["is_main_image"] = "1";
			}
		}
		return $gallerys;
	}	
	
	//Update Data Boat
	public function updateDataBoat($vendor, $boat, $arr_data, $lang = ""){
		if ($lang == "") $lang = $vendor["default_language"];
		$update = false;
		
		//Update table tb_trans_boat
		$arr_data_update_tb_trans_boat = array();
		if (isset($arr_data["main_image"])) 	$arr_data_update_tb_trans_boat["f_main_image_gallery"] 	= $arr_data["main_image"];
		if (isset($arr_data["publish_status"])) $arr_data_update_tb_trans_boat["f_status"] 				= $arr_data["publish_status"];
		if (isset($arr_data["delete"])) 		$arr_data_update_tb_trans_boat["f_deleted"] 			= $arr_data["delete"];
		
		if (count($arr_data_update_tb_trans_boat) > 0){
		
			$this->db->where("tb_trans_boat.f_id_boat",		$boat["id"]);
			$this->db->where("tb_trans_boat.f_id_vendor",	$vendor["id"]);
			$this->db->where("tb_trans_boat.f_deleted <> ", '1');
			
			$update = $this->db->update("tb_trans_boat", $arr_data_update_tb_trans_boat);
			
		}
		//--
		
		//Update table tb_trans_boat_lang
		$arr_data_update_tb_trans_boat_lang = array();
		if (isset($arr_data["name"])) 			$arr_data_update_tb_trans_boat_lang["f_boat_name"] 		= $arr_data["name"];
		if (isset($arr_data["description"])) 	$arr_data_update_tb_trans_boat_lang["f_description"] 	= $arr_data["description"];
		
		if (count($arr_data_update_tb_trans_boat_lang) > 0){
			
			//Check If lang already available
			$this->db->where("tb_trans_boat_lang.f_id_boat",	$boat["id"]);
			$this->db->where("tb_trans_boat_lang.f_lang",		$lang);
			
			if ($this->db->get("tb_trans_boat_lang")->num_rows() > 0){
				
				$this->db->where("tb_trans_boat_lang.f_id_boat",	$boat["id"]);
				$this->db->where("tb_trans_boat_lang.f_lang",		$lang);
				$update = $this->db->update("tb_trans_boat_lang", $arr_data_update_tb_trans_boat_lang);
				
			}else{
				$arr_data_update_tb_trans_boat_lang["f_lang"]	= $lang;
				$arr_data_update_tb_trans_boat_lang["f_id_boat"]= $boat["id"];
				
				$update = $this->db->insert("tb_trans_boat_lang", $arr_data_update_tb_trans_boat_lang);
				
			}
		}
		//--
		
		return $update;
	}
	
	public function deleteBoat($vendor, $boat){
		return $this->updateDataBoat($vendor, $boat, array("delete"=>1));
	}
	public function publish($vendor, $boat){
		return $this->updateDataBoat($vendor, $boat, array("publish_status"=>1));
	}
	public function unpublish($vendor, $boat){
		return $this->updateDataBoat($vendor, $boat, array("publish_status"=>0));
	}
		
	//Create New Boat
	private function generateBoatCode($vendor){
		$vendor_code = $vendor["code"];
		
		$boat_code = "";
		$ada = true;
		while ($ada){
			//Generate Number
			$random_numeric = $this->fn->randomNumeric(4);
			$boat_code = $vendor_code.$random_numeric;
			
			//Cek Di Database
			if ($this->db->where("f_code_boat", $boat_code)->get("tb_trans_boat")->num_rows() > 0){
				$ada = true;
			}else{
				$ada = false;
			}
		}
		
		return $boat_code;
	}
	public function create($vendor, $data_insert){
		//Insert table tb_trans_boat
		$boat_code = $this->generateBoatCode($vendor);
		$dt_insert_trans_boat["f_id_vendor"] = $vendor["id"];
		$dt_insert_trans_boat["f_code_boat"] = $boat_code;
		$dt_insert_trans_boat["f_status"] 	 = "1";
		
		$insert = $this->db->insert("tb_trans_boat", $dt_insert_trans_boat);
		$id_boat = $this->db->insert_id();
		//--
		
		//Insert table tb_trans_boat_lang
		$lang = (isset($data_insert["lang"]) && $data_insert["lang"] !="")?$data_insert["lang"]:$vendor["default_language"];
		$dt_insert_trans_boat_lang["f_id_boat"] 	= $id_boat;
		$dt_insert_trans_boat_lang["f_lang"] 		= $lang;
		$dt_insert_trans_boat_lang["f_boat_name"] 	= $data_insert["name"];
		$dt_insert_trans_boat_lang["f_description"] = $data_insert["description"];
		
		$insert_lang = $this->db->insert("tb_trans_boat_lang", $dt_insert_trans_boat_lang);
		
		return ($insert && $insert_lang)?array("boat_code" => $boat_code, "lang" => $lang):FALSE;
	}
	
}
?>
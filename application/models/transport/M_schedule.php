<?php
class M_schedule extends CI_Model {
    
	function __construct(){
		parent::__construct();
		$this->fn->db_load_slave();
		
		$this->load->model("m_boats");
    }
	
	//Create New Schedule
	private function generateScheduleCode($vendor){
		$vendor_code = $vendor["code"];
		
		$code = "";
		$ada = true;
		while ($ada){
			//Generate Number
			$random_numeric = $this->fn->randomNumeric(4);
			$code = $vendor_code.$random_numeric;
			
			//Cek Di Database
			if ($this->db->where("f_schedule_code", $code)->get("tbc_trans_schedule")->num_rows() > 0){
				$ada = true;
			}else{
				$ada = false;
			}
		}
		
		return $code;
	}
	public function create($vendor, $data_insert){
		//Insert table tb_trans_trip
		$schedule_code = $this->generateScheduleCode($vendor);
		$dt_insert_trans_schedule["f_id_vendor"] 	= $vendor["id"];
		$dt_insert_trans_schedule["f_schedule_code"]= $schedule_code;
		$dt_insert_trans_schedule["f_id_boat"] 		= $data_insert["boat_id"];
		$dt_insert_trans_schedule["f_status"] 	 	= "0";
		
		$insert = $this->db->insert("tbc_trans_schedule", $dt_insert_trans_schedule);
		$id_schedule = $this->db->insert_id();
		//--
		
		//Insert table tb_trans_trip_lang
		$lang = (isset($data_insert["lang"]) && $data_insert["lang"] !="")?$data_insert["lang"]:$vendor["default_language"];
		$dt_insert_trans_schedule_lang["f_id_schedule"] 	= $id_schedule;
		$dt_insert_trans_schedule_lang["f_lang"] 			= $lang;
		$dt_insert_trans_schedule_lang["f_schedule_name"] 	= $data_insert["name"];
		$dt_insert_trans_schedule_lang["f_description"] 	= (isset($data_insert["description"]))?$data_insert["description"]:"";
		$insert_lang = $this->db->insert("tbc_trans_schedule_lang", $dt_insert_trans_schedule_lang);
		//--
		
		//Insert Detail Schedule
		$insert_detail = $this->insert_detail_schedule($id_schedule, $data_insert["schedule_detail"]);
		//--
		
		return ($insert && $insert_lang && $insert_detail)?array("schedule_code" => $schedule_code, "lang" => $lang):FALSE;
	}
	private function insert_detail_schedule($id_schedule, $arr_schedule_detail){
		//Delete
		$this->db->where("f_id_schedule", $id_schedule)->delete("tbc_trans_schedule_detail");
		
		//Insert Baru
		$dt_insert_trans_schedule_detail = array();
		$order_by = 1;
		foreach ($arr_schedule_detail as $schedule_detail){
			$trans_schedule_detail = array();
			$trans_schedule_detail["f_id_schedule"] 			= $id_schedule;
			$trans_schedule_detail["f_departure_port_id"] 		= $schedule_detail["departure_port_id"];
			$trans_schedule_detail["f_departure_time"] 			= $schedule_detail["departure_time"];
			$trans_schedule_detail["f_departure_allow_pickup"]	= (isset($schedule_detail["departure_allow_pickup"]))?$schedule_detail["departure_allow_pickup"]:"1";
			$trans_schedule_detail["f_arrival_port_id"] 		= $schedule_detail["arrival_port_id"];
			$trans_schedule_detail["f_arrival_time"] 			= $schedule_detail["arrival_time"];
			$trans_schedule_detail["f_arrival_allow_dropoff"] 	= (isset($schedule_detail["arrival_allow_dropoff"]))?$schedule_detail["arrival_allow_dropoff"]:"1";
			$trans_schedule_detail["f_order_by"] 				= $order_by;
			
			$dt_insert_trans_schedule_detail[] = $trans_schedule_detail;
			$order_by++;
		}
		$insert_detail = $this->db->insert_batch("tbc_trans_schedule_detail", $dt_insert_trans_schedule_detail);
		//--
		
		return $insert_detail;
	}
	//--
	
	//Query to get all data Schedule
	public function search($lang){
		
		$query = $this->dbsl->select("tbc_trans_schedule.*,
										tbc_trans_schedule_lang.f_id_schedule, tbc_trans_schedule_lang.f_lang, tbc_trans_schedule_lang.f_schedule_name, tbc_trans_schedule_lang.f_description as f_schedule_description,
										tb_trans_boat.f_id_boat, tb_trans_boat.f_code_boat, tb_trans_boat.f_main_image_gallery, tb_trans_boat_lang.f_boat_name, tb_trans_boat_lang.f_description as f_boat_description")
							
							->where("tbc_trans_schedule.f_permanent_delete <> ", '1')
							->where("tbc_trans_schedule_lang.f_lang", $lang)
							->where("tb_trans_boat_lang.f_lang", $lang)
							
							->join("tbc_trans_schedule_lang", 	"tbc_trans_schedule.f_id_schedule = tbc_trans_schedule_lang.f_id_schedule")
							->join("tb_trans_boat", 			"tbc_trans_schedule.f_id_boat = tb_trans_boat.f_id_boat")
							->join("tb_trans_boat_lang", 		"tb_trans_boat.f_id_boat = tb_trans_boat_lang.f_id_boat")
							
							->get("tbc_trans_schedule");
		//echo $this->dbsl->last_query();				
		if ($query->num_rows() > 0){
			$schedules = $query->result_array();

			$arr_schedules = array();
			foreach ($schedules as $schedule){
				$arr_schedules[] = $this->generateDataSchedule($schedule);
			}
			
			return $arr_schedules;
			
		}else{
			return FALSE;
		}
	}
	private function generateDataSchedule($data){

		$schedule = array();
		//$trip = $trips;
		$schedule["id"] 				= $data["f_id_schedule"];
		$schedule["schedule_code"] 		= $data["f_schedule_code"];
		$schedule["name"] 				= $data["f_schedule_name"];
		$schedule["description"] 		= $data["f_schedule_description"];
		$schedule["publish_status"] 	= $data["f_status"];
		$schedule["lang"] 				= $data["f_lang"];
		
		$boat = array();
		$boat["id"] 					= $data["f_id_boat"];
		$boat["boat_code"] 				= $data["f_code_boat"];
		$boat["name"] 					= $data["f_boat_name"];
		$boat["description"] 			= $data["f_boat_description"];
		$boat["main_image"]				= ($data["f_main_image_gallery"]!="")?$this->fn->resize_image(100,500,0,"webimages/trans_boat/gallery/$data[f_main_image_gallery]"):"";
		
		$schedule["boat"] 				= $boat;
		
		return $schedule;
	}
	//--
	
	//Get Data Schedule fom current vendor
	public function getSchedules($vendor, $lang = "", $publish_status = ""){
		if ($lang == "") $lang = $vendor["default_language"];
		
		$this->dbsl->where("tbc_trans_schedule.f_id_vendor",$vendor["id"]);
		
		if (strtoupper($publish_status) == "1" || strtoupper($publish_status) == "0"){
			$this->dbsl->where("tbc_trans_schedule.f_status", $publish_status);
		}elseif (strtoupper($publish_status) != "ALL"){
			$this->dbsl->where("tbc_trans_schedule.f_status", '1');
		}
		
		$schedules = $this->search($lang);
		if (count($schedules) > 0){
			return $schedules;
		}else{
			return FALSE;
		}
	}
	//--
	
	public function getScheduleByCode($vendor, $schedule_code, $lang = ""){
		if ($lang == "") $lang = $vendor["default_language"];
		
		$this->dbsl->where("tbc_trans_schedule.f_schedule_code",	$schedule_code);
		$this->dbsl->where("tbc_trans_schedule.f_id_vendor",		$vendor["id"]);
		
		$schedule = $this->search($lang);
		
		if (count($schedule) > 0){
			$schedule = $schedule[0];
			
			return $schedule;
		}else{
			return FALSE;
		}
	}
	public function getScheduleDetailByCode($vendor, $schedule_code, $lang = ""){
		
		$schedule = $this->getScheduleByCode($vendor, $schedule_code, $lang);
		
		if ($schedule){
			
			$schedule["boat"] = $this->m_boats->getBoatDetailByCode($vendor, $schedule["boat"]["boat_code"], $lang);
			$schedule["schedule_detail"] = $this->getScheduleDetail($schedule["id"]);
			
			return $schedule;
		}else{
			return FALSE;
		}
	}
	private function getScheduleDetail($id_schedule){
		$query = $this->dbsl->select("	vw_port_dep.f_id_fasboat_port as id_port_dep, vw_port_dep.f_port_name as port_name_dep, vw_port_dep.f_code as code_dep,
										vw_port_arr.f_id_fasboat_port as id_port_arr, vw_port_arr.f_port_name as port_name_arr, vw_port_arr.f_code as code_arr,
										tbc_trans_schedule_detail.*, TIMEDIFF(f_arrival_time, f_departure_time) as f_duration")
							->where("tbc_trans_schedule_detail.f_id_schedule", $id_schedule)
							->join("tb_s_fastboat_port_name as vw_port_dep", "tbc_trans_schedule_detail.f_departure_port_id	= vw_port_dep.f_id_fasboat_port")
							->join("tb_s_fastboat_port_name as vw_port_arr", "tbc_trans_schedule_detail.f_arrival_port_id	= vw_port_arr.f_id_fasboat_port")
							->order_by("f_order_by asc")
							->get("tbc_trans_schedule_detail");
		
		if ($query->num_rows() > 0){
			$schedule_detail = $query->result_array();
			
			$arr_schedule_detail = array();
			foreach ($schedule_detail as $schedule){
				$detail = array();
			
				$detail["departure_port"] 			= array("id"		=> $schedule["id_port_dep"],
															"port_code"	=> $schedule["code_dep"],
															"name"		=> $schedule["port_name_dep"]);
				$detail["departure_time"]			= $schedule["f_departure_time"];
				$detail["departure_allow_pickup"]	= $schedule["f_departure_allow_pickup"];
				
				$detail["arrival_port"] 			= array("id"		=> $schedule["id_port_arr"],
															"port_code"	=> $schedule["code_arr"],
															"name"		=> $schedule["port_name_arr"]);
				$detail["arrival_time"]				= $schedule["f_arrival_time"];
				$detail["arrival_allow_dropoff"]	= $schedule["f_arrival_allow_dropoff"];
				
				$detail["duration"]					= $schedule["f_duration"];
				
				$arr_schedule_detail[] = $detail;
			}
			
			return $arr_schedule_detail;
		}else{
			return FALSE;
		}
		
	}
	
	//Update Data Trip
	public function updateDataSchedule($vendor, $schedule, $arr_data, $lang = ""){
		if ($lang == "") $lang = $vendor["default_language"];
		$update = false;

		//Update table tbc_trans_schedule
		$arr_data_update_tb_trans_schedule = array();
		if (isset($arr_data["publish_status"])) $arr_data_update_tb_trans_schedule["f_status"] 				= $arr_data["publish_status"];
		if (isset($arr_data["delete"])) 		$arr_data_update_tb_trans_schedule["f_permanent_delete"] 	= $arr_data["delete"];
		if (isset($arr_data["boat_id"])) 		$arr_data_update_tb_trans_schedule["f_id_boat"] 			= $arr_data["boat_id"];
		
		if (count($arr_data_update_tb_trans_schedule) > 0){
		
			$this->db->where("tbc_trans_schedule.f_id_schedule",	$schedule["id"]);
			$this->db->where("tbc_trans_schedule.f_id_vendor",		$vendor["id"]);
			$this->db->where("tbc_trans_schedule.f_permanent_delete <> ", '1');
			
			$update = $this->db->update("tbc_trans_schedule", $arr_data_update_tb_trans_schedule);
			
		}
		//--
		
		//Update table tbc_trans_schedulet_lang
		$arr_data_update_tb_trans_schedule_lang = array();
		if (isset($arr_data["name"])) 			$arr_data_update_tb_trans_schedule_lang["f_schedule_name"] 	= $arr_data["name"];
		if (isset($arr_data["description"])) 	$arr_data_update_tb_trans_schedule_lang["f_description"] 	= $arr_data["description"];
		
		if (count($arr_data_update_tb_trans_schedule_lang) > 0){
			
			//Check If lang already available
			$this->db->where("tbc_trans_schedule_lang.f_id_schedule",	$schedule["id"]);
			$this->db->where("tbc_trans_schedule_lang.f_lang",			$lang);
			
			if ($this->db->get("tbc_trans_schedule_lang")->num_rows() > 0){
				
				$this->db->where("tbc_trans_schedule_lang.f_id_schedule",	$schedule["id"]);
				$this->db->where("tbc_trans_schedule_lang.f_lang",			$lang);
				$update = $this->db->update("tbc_trans_schedule_lang", 		$arr_data_update_tb_trans_schedule_lang);
				
			}else{
				$arr_data_update_tb_trans_schedule_lang["f_lang"]		= $lang;
				$arr_data_update_tb_trans_schedule_lang["f_id_schedule"]= $schedule["id"];
				
				$update = $this->db->insert("tbc_trans_schedule_lang", $arr_data_update_tb_trans_schedule_lang);
				
			}
		}
		//--
		
		//Insert Detail Schedule
		if (isset($arr_data["schedule_detail"])){
			$update = $this->insert_detail_schedule($schedule["id"], $arr_data["schedule_detail"]);
		}
		//--
		
		return $update;
	}
	
	public function deleteSchedule($vendor, $schedule){
		return $this->updateDataSchedule($vendor, $schedule, array("delete"=>1));
	}
	public function publish($vendor, $schedule){
		return $this->updateDataSchedule($vendor, $schedule, array("publish_status"=>1));
	}
	public function unpublish($vendor, $schedule){
		return $this->updateDataSchedule($vendor, $schedule, array("publish_status"=>0));
	}
	
}
?>
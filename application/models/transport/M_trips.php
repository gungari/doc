<?php
class M_trips extends CI_Model {
    
	function __construct(){
		parent::__construct();
		$this->fn->db_load_slave();
    }
	
	//Port List
	public function portList(){
		$ports = $this->dbsl->order_by("f_port_name asc")
							->where('tb_s_fastboat_port_name.f_status',1)
							->get("tb_s_fastboat_port_name");
							
		if ($ports->num_rows() > 0){
			
			$ports = $ports->result_array();
			$arr_ports = array();
			foreach ($ports as $port){
				$arr_ports[] = $this->generateDataPort($port);
			}
			return $arr_ports;
		}else{
			return FALSE;
		}
	}
	private function generateDataPort($ports){

		$port = array(	"id" 		=> $ports["f_id_fasboat_port"],
						"port_code" => $ports["f_code"],
						"name" 		=> $ports["f_port_name"],
					  );
		
		return $port;
	}
	
	private function _getAvailablePort($vendor, $dep = 'f_departure'){
		$ports = $this->dbsl->select('tb_s_fastboat_port_name.*')->where('tb_trans_trip.f_id_vendor',$vendor["id"])
							->where('tb_s_fastboat_port_name.f_status',1)
							->where('tb_trans_trip.f_id_boat IS NULL')
							->where('tb_trans_trip.f_deleted',0)
							->where('tb_trans_trip.f_permanent_deleted',0)
							->order_by("tb_s_fastboat_port_name.f_port_name asc")
							->join('tb_s_fastboat_port_name','tb_s_fastboat_port_name.f_id_fasboat_port=tb_trans_trip.'.$dep)
							->group_by('f_id_fasboat_port')
							->get('tb_trans_trip')
							->result_array();

		$arr_ports = array();
		foreach ($ports as $port){
			$arr_ports[] = $this->generateDataPort($port);
		}
		return $arr_ports;
	}
	public function getAvailablePort($vendor){
		//Departure Port
		$ports["departure"] = $this->_getAvailablePort($vendor, "f_departure");
		
		//Arrival Port
		$ports["arrival"] = $this->_getAvailablePort($vendor, "f_arrival");
		
		return $ports;
	}
	
	public function getPortByCode($port_code){
		
		$this->dbsl->where("tb_s_fastboat_port_name.f_code",	$port_code);
		
		$ports = $this->portList();
		
		if (count($ports) > 0){
			$port = $ports[0];
			
			return $port;
		}else{
			return FALSE;
		}
	}
	public function getPortByID($port_id){
		
		$this->dbsl->where("tb_s_fastboat_port_name.f_id_fasboat_port", $port_id);
		
		$ports = $this->portList();
		
		if (count($ports) > 0){
			$port = $ports[0];
			
			return $port;
		}else{
			return FALSE;
		}
	}
	
	//Query to get all data Trips
	public function search($lang){
		
		$query = $this->dbsl->select("	vw_port_dep.f_id_fasboat_port as id_port_dep, vw_port_dep.f_port_name as port_name_dep, vw_port_dep.f_code as code_dep,
										vw_port_arr.f_id_fasboat_port as id_port_arr, vw_port_arr.f_port_name as port_name_arr, vw_port_arr.f_code as code_arr,
										tb_trans_trip.*, tb_trans_trip_lang.*")
							->where("tb_trans_trip.f_permanent_deleted <> ", '1')
							->where("tb_trans_trip_lang.f_lang", $lang)
							->join("tb_s_fastboat_port_name as vw_port_dep", "tb_trans_trip.f_departure	= vw_port_dep.f_id_fasboat_port")
							->join("tb_s_fastboat_port_name as vw_port_arr", "tb_trans_trip.f_arrival	= vw_port_arr.f_id_fasboat_port")
							->join("tb_trans_trip_lang", "tb_trans_trip.f_id_trip = tb_trans_trip_lang.f_id_trip")
							->get("tb_trans_trip");
		
		if ($query->num_rows() > 0){
			$trips = $query->result_array();

			$arr_trips = array();
			foreach ($trips as $trip){
				$arr_trips[] = $this->generateDataTrips($trip);
			}
			return $arr_trips;
			
		}else{
			return FALSE;
		}
	}
	private function generateDataTrips($trips){

		$trip = array();
		//$trip = $trips;
		$trip["id"] 				= $trips["f_id_trip"];
		$trip["trip_code"] 			= $trips["f_code_trip"];
		$trip["name"] 				= $trips["f_trip_name"];
		$trip["description"] 		= $trips["f_description"];
		$trip["departure_port"] 	= array("id"		=> $trips["id_port_dep"],
											"port_code"	=> $trips["code_dep"],
											"name"		=> $trips["port_name_dep"]);
		$trip["arrival_port"] 		= array("id"		=> $trips["id_port_arr"],
											"port_code"	=> $trips["code_arr"],
											"name"		=> $trips["port_name_arr"]);
		$trip["publish_status"] 	= ($trips["f_deleted"]=="1")?0:1;
		$trip["lang"] 				= $trips["f_lang"];
		return $trip;
	}
	
	//Get Data Trips fom current vendor
	public function getTrips($vendor, $lang = "", $publish_status = ""){
		if ($lang == "") $lang = $vendor["default_language"];
		
		$this->dbsl->where("tb_trans_trip.f_id_vendor",$vendor["id"]);
		
		if (strtoupper($publish_status) == "1" || strtoupper($publish_status) == "0"){
			$this->dbsl->where("tb_trans_trip.f_deleted", ($publish_status=="1"?0:1));
		}elseif (strtoupper($publish_status) != "ALL"){
			$this->dbsl->where("tb_trans_trip.f_deleted <> ", '1');
		}
		
		$trips = $this->search($lang);
		if (count($trips) > 0){
			return $trips;
		}else{
			return FALSE;
		}
	}
	public function getTripByCode($vendor, $trip_code, $lang = ""){
		if ($lang == "") $lang = $vendor["default_language"];
		
		$this->dbsl->where("tb_trans_trip.f_code_trip",	$trip_code);
		$this->dbsl->where("tb_trans_trip.f_id_vendor",	$vendor["id"]);
		
		$trips = $this->search($lang);
		
		if (count($trips) > 0){
			$trip = $trips[0];
			
			return $trip;
		}else{
			return FALSE;
		}
	}
	public function getTripDetailByCode($vendor, $trip_code, $lang = ""){
		
		$trip = $this->getTripByCode($vendor, $trip_code, $lang);
		
		if ($trip){
			return $trip;
		}else{
			return FALSE;
		}
	}
	
	//Update Data Trip
	public function updateDataTrip($vendor, $trip, $arr_data, $lang = ""){
		if ($lang == "") $lang = $vendor["default_language"];
		$update = false;

		//Update table tb_trans_boat
		$arr_data_update_tb_trans_trip = array();
		if (isset($arr_data["publish_status"])) $arr_data_update_tb_trans_trip["f_deleted"] 			= ($arr_data["publish_status"]=="1")?"0":"1";
		if (isset($arr_data["delete"])) 		$arr_data_update_tb_trans_trip["f_permanent_deleted"] 	= $arr_data["delete"];
		if (isset($arr_data["departure_port_id"])) 		$arr_data_update_tb_trans_trip["f_departure"] 	= $arr_data["departure_port_id"];
		if (isset($arr_data["arrival_port_id"])) 		$arr_data_update_tb_trans_trip["f_arrival"] 	= $arr_data["arrival_port_id"];
		
		if (count($arr_data_update_tb_trans_trip) > 0){
		
			$this->db->where("tb_trans_trip.f_id_trip",		$trip["id"]);
			$this->db->where("tb_trans_trip.f_id_vendor",	$vendor["id"]);
			$this->db->where("tb_trans_trip.f_permanent_deleted <> ", '1');
			
			$update = $this->db->update("tb_trans_trip", $arr_data_update_tb_trans_trip);
			
		}
		//--
		
		//Update table tb_trans_boat_lang
		$arr_data_update_tb_trans_trip_lang = array();
		if (isset($arr_data["name"])) 			$arr_data_update_tb_trans_trip_lang["f_trip_name"] 		= $arr_data["name"];
		if (isset($arr_data["description"])) 	$arr_data_update_tb_trans_trip_lang["f_description"] 	= $arr_data["description"];
		
		if (count($arr_data_update_tb_trans_trip_lang) > 0){
			
			//Check If lang already available
			$this->db->where("tb_trans_trip_lang.f_id_trip",	$trip["id"]);
			$this->db->where("tb_trans_trip_lang.f_lang",		$lang);
			
			if ($this->db->get("tb_trans_trip_lang")->num_rows() > 0){
				
				$this->db->where("tb_trans_trip_lang.f_id_trip",	$trip["id"]);
				$this->db->where("tb_trans_trip_lang.f_lang",		$lang);
				$update = $this->db->update("tb_trans_trip_lang", $arr_data_update_tb_trans_trip_lang);
				
			}else{
				$arr_data_update_tb_trans_trip_lang["f_lang"]	= $lang;
				$arr_data_update_tb_trans_trip_lang["f_id_trip"]= $trip["id"];
				
				$update = $this->db->insert("tb_trans_trip_lang", $arr_data_update_tb_trans_trip_lang);
				
			}
		}
		//--
		
		return $update;
	}
	
	public function deleteTrip($vendor, $trip){
		return $this->updateDataTrip($vendor, $trip, array("delete"=>1));
	}
	public function publish($vendor, $trip){
		return $this->updateDataTrip($vendor, $trip, array("publish_status"=>1));
	}
	public function unpublish($vendor, $trip){
		return $this->updateDataTrip($vendor, $trip, array("publish_status"=>0));
	}
	
	//Create New Trip
	private function generateTripCode($vendor){
		$vendor_code = $vendor["code"];
		
		$trip_code = "";
		$ada = true;
		while ($ada){
			//Generate Number
			$random_numeric = $this->fn->randomNumeric(4);
			$trip_code = $vendor_code.$random_numeric;
			
			//Cek Di Database
			if ($this->db->where("f_code_trip", $trip_code)->get("tb_trans_trip")->num_rows() > 0){
				$ada = true;
			}else{
				$ada = false;
			}
		}
		
		return $trip_code;
	}
	public function create($vendor, $data_insert){
		//Insert table tb_trans_trip
		$trip_code = $this->generateTripCode($vendor);
		$dt_insert_trans_trip["f_id_vendor"] = $vendor["id"];
		$dt_insert_trans_trip["f_code_trip"] = $trip_code;
		$dt_insert_trans_trip["f_departure"] = $data_insert["departure_port_id"];
		$dt_insert_trans_trip["f_arrival"]	 = $data_insert["arrival_port_id"];
		$dt_insert_trans_trip["f_deleted"] 	 = "0";
		
		$insert = $this->db->insert("tb_trans_trip", $dt_insert_trans_trip);
		$id_trip = $this->db->insert_id();
		//--
		
		//Insert table tb_trans_trip_lang
		$lang = (isset($data_insert["lang"]) && $data_insert["lang"] !="")?$data_insert["lang"]:$vendor["default_language"];
		$dt_insert_trans_trip_lang["f_id_trip"] 	= $id_trip;
		$dt_insert_trans_trip_lang["f_lang"] 		= $lang;
		$dt_insert_trans_trip_lang["f_trip_name"] 	= $data_insert["name"];
		$dt_insert_trans_trip_lang["f_description"] = $data_insert["description"];
		
		$insert_lang = $this->db->insert("tb_trans_trip_lang", $dt_insert_trans_trip_lang);
		
		return ($insert && $insert_lang)?array("trip_code" => $trip_code, "lang" => $lang):FALSE;
	}
}
?>
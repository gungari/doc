<?php
class M_customer_review extends CI_Model {
    
	function __construct(){
		parent::__construct();
		$this->fn->db_load_slave();
    }
	
	public function search($vendor, $code = ""){

		if ($this->m_vendor->isVendorACT($vendor)){
			
		}elseif ($this->m_vendor->isVendorACC($vendor)){
			
		}elseif ($this->m_vendor->isVendorTRANS($vendor)){
			
		}
		
		$reviews = $this->dbsl	->where("f_id_vendor", $vendor["id"])
								->get("tb_product_review");
		
		if ($reviews->num_rows() > 0){
			$reviews = $reviews->result_array();
			
			return $reviews;
		}else{
			return FALSE;
		}
	}
	
	public function getReviews($vendor, $param = ""){
		
		$reviews = $this->search($vendor, $param);
		
		if ($reviews) {
			return $reviews;
		}else{
			return FALSE;
		}
		
		return FALSE;
	}
	
}
?>
<?php
class M_vendor extends CI_Model {
    function __construct(){
		parent::__construct();
    }

	private function getVendor(){
		$query = $this->db->select("tb_vendor_detail.*, tb_vendor.*, 
										tb_s_provinsi.f_provinsi, tb_s_country.f_country, tb_s_kabupaten.f_nama_kabupaten, tb_s_location.f_location, 
										tb_vendor_category.f_permalink as f_vendor_category")
						  ->join("tb_s_location", 	"tb_s_location.f_id_location = tb_vendor.f_id_location", "left")
						  ->join("tb_s_kabupaten", 	"tb_s_kabupaten.f_id_kabupaten = tb_vendor.f_id_kabupaten", "left")
						  ->join("tb_s_provinsi", 	"tb_s_provinsi.f_id_provinsi = tb_vendor.f_id_provinsi", "left")
						  ->join("tb_s_country", 	"tb_s_country.f_country_code = tb_vendor.f_country_code", "left")
						  ->join("tb_vendor_detail","tb_vendor.f_id_vendor = tb_vendor_detail.f_id_vendor", "left")
						  ->join("tb_vendor_registered_category", "tb_vendor.f_id_vendor = tb_vendor_registered_category.f_id_vendor")
						  ->join("tb_vendor_category", "tb_vendor_registered_category.f_id_vendor_category = tb_vendor_category.f_id")
						  //->limit(10)
						  ->get("tb_vendor");
		if ($query->num_rows() > 0){
			$vendors = $query->result_array();
			
			$arr_vendor = array();
			foreach ($vendors as $vendor){
				$arr_vendor[] = $this->generateDataVendor($vendor);
			}
			
			return $arr_vendor;
			
		}else{
			return FALSE;
		}
	}
	
	private function generateDataVendor($vendor){
		$arr_user = array();
		
		if (isset($vendor["f_company"])){
			$arr_user["id"] 		= $vendor["f_id_vendor"];
			$arr_user["code"] 		= $vendor["f_code_vendor"];
			$arr_user["business_name"] 	= $vendor["f_company"];
			$arr_user["hybrid_pages"]	= site_url($vendor["f_permalink"]);
			$arr_user["email"] 		= $vendor["f_email"];
			$arr_user["telephone"] 	= $vendor["f_phone"];
			$arr_user["website"] 	= $vendor["f_website"];
			$arr_user["address"] 	= $vendor["f_address"];
			$arr_user["country"] 	= $vendor["f_country"];
			$arr_user["province"] 	= $vendor["f_provinsi"];
			$arr_user["regency"] 	= $vendor["f_nama_kabupaten"];
			$arr_user["area"]		= $vendor["f_location"];
			$arr_user["latitude"] 	= $vendor["f_loc_latitude"];
			$arr_user["longitude"] 	= $vendor["f_loc_longitude"];
			$arr_user["time_zone"] 	= $vendor["f_time_zone"];
			$arr_user["default_currency"] 	= $vendor["f_currency_filter"];
			$arr_user["language"] 		  	= explode("|", $vendor["f_available_lang"]);
			$arr_user["default_language"] 	= $vendor["f_default_lang"];
			$arr_user["category"] 			= $vendor["f_vendor_category"];
			$arr_user['image_profile']	  	= ($vendor["f_image_profile"]!="")?base_url("webimages/vendor/thumb/".$vendor["f_image_profile"]):"";
			$arr_user["logo"] 				= ($vendor["f_image_logo"]!="")?base_url("webimages/vendor/thumb/".$vendor["f_image_logo"]):"";
			//$arr_user["description"] 		= ($vendor["f_description"]!="")?$vendor["f_description"]:"";
			
			return $arr_user;
		}else{
			return FALSE;
		}
	}

	public function isVendorACT($vendor){
		return (strtoupper($vendor["category"]) =="ACTIVITIES");
	}
	public function isVendorTRANS($vendor){
		return (strtoupper($vendor["category"]) =="TRANSPORT");
	}
	public function isVendorACC($vendor){
		return (strtoupper($vendor["category"]) =="ACCOMMODATION");
	}
	
	public function getActiveVendors(){
		$this->db->where("tb_vendor.f_deleted", "0");
		$this->db->where("tb_vendor.f_status_online", "1");
		
		return $this->getVendor();
	}
	
	public function getVendorByCode($merchant_code){
		$this->db->where("tb_vendor.f_code_vendor", $merchant_code);
		$vendors = $this->getVendor();
		if ($vendors){
			return $vendors[0];
		}else{
			return FALSE;
		}
	}
	
	public function getVendorByID($merchant_id){
		$this->db->where("tb_vendor.f_id_vendor", $merchant_id);
		$vendors = $this->getVendor();
		if ($vendors){
			return $vendors[0];
		}else{
			return FALSE;
		}
	}
	
	
}
?>
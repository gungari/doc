<?php
class M_customer extends CI_Model {
    
	function __construct(){
		parent::__construct();
		$this->fn->db_load_slave();
    }
	
	public function search($vendor, $search = ""){
		
		$this->dbsl->limit(10);
		
		$customers = $this->dbsl->select("tb_members.*, tb_s_country.f_country as f_country_name")
								->join("tb_s_country", "tb_s_country.f_country_code = tb_members.f_country")
								->get("tb_members");
		
		if ($customers->num_rows() > 0){
			$customers = $customers->result_array();
			$arr_customers = array();
			foreach ($customers as $customer){
				$arr_customers[] = $this->generateDataCustomer($customer);
			}
			return $arr_customers;
		}else{
			return FALSE;
		}
	}
	
	private function generateDataCustomer($customers){
		
		$customer = array(	"id" 			=> $customers["f_id_member"],
							"first_name"	=> $customers["f_first_name"],
							"last_name" 	=> $customers["f_last_name"],
							"email" 		=> $customers["f_email"],
							"mobile" 		=> $customers["f_mobile"],
							"address" 		=> $customers["f_address"],
							"country_code" 	=> $customers["f_country"],
							"country_name" 	=> $customers["f_country_name"],
							"photo" 		=> $customers["f_photo"],
							"facebook_id" 	=> $customers["f_id_facebook"],
							"google_id" 	=> $customers["f_id_google"],
							"publish_status"=> ($customers["f_deleted"] == "1")?0:1,
					  	);
		
		return $customer;
	}
	
	public function getCustomers($vendor, $publish_status = "", $search = ""){
		
		if (strtoupper($publish_status) == "1" || strtoupper($publish_status) == "0"){
			$this->dbsl->where("tb_members.f_deleted", ($publish_status=="1"?0:1));
		}elseif (strtoupper($publish_status) != "ALL"){
			$this->dbsl->where("tb_members.f_deleted <> ", '1');
		}
		
		if ($search != ""){
			$this->dbsl->where("tb_members.f_deleted <> ", '1');
		}
		
		$customers = $this->search($vendor, $search);
		
		if ($customers) {
			return $customers;
		}else{
			return FALSE;
		}
		
		return FALSE;
	}
	
}
?>
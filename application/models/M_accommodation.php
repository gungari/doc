<?php
class M_accommodation extends CI_Model {
    function __construct(){
		parent::__construct();
    }
	
	//Query to get all data rooms
	public function searchRooms($lang){
		$this->db->select("tb_htl_room_type.*, tb_htl_room_type_lang.*, tb_htl_room_type.f_id_room_type, tb_s_htl_bed_type.f_bed_type_name", false);
		$this->db->join("tb_s_htl_bed_type", "tb_s_htl_bed_type.f_id_bed_type = tb_htl_room_type.f_id_bed_type", "left");
		$this->db->join("(SELECT * FROM tb_htl_room_type_lang where tb_htl_room_type_lang.f_lang = '$lang') tb_htl_room_type_lang",
											"tb_htl_room_type.f_id_room_type = tb_htl_room_type_lang.f_id_room_type", "left");		
		$this->db->join("tb_vendor", "tb_vendor.f_id_vendor = tb_htl_room_type.f_id_vendor");
		$this->db->where("tb_vendor.f_deleted	= '0'")
				 ->where("tb_htl_room_type.f_deleted	= '0'")
				 ->order_by("f_deleted asc, f_room_name asc");
		
		$query = $this->db->get("tb_htl_room_type");
		
		if ($query->num_rows() > 0){
			$rooms = $query->result_array();
			
			$arr_rooms = array();
			foreach ($rooms as $room){
				$arr_rooms[] = $this->generateDataRoom($room);
			}
			return $arr_rooms;
			
		}else{
			return FALSE;
		}
	}
	private function generateDataRoom($rooms){
		$room = array();
		$room["id"] 				= $rooms["f_id_room_type"];
		$room["room_code"] 			= $rooms["f_code_room"];
		$room["name"] 				= $rooms["f_room_name"];
		$room["description"] 		= $rooms["f_description"];
		$room["specious_room"] 		= $rooms["f_specious_room"];
		$room["max_person"] 		= $rooms["f_max_person"];
		if ($rooms["f_extra_bed"] == "1"){
			$room["extrabed"]['max_extra_bed'] 	= $rooms['f_max_extra_bed'];
			$room["extrabed"]["currency"] 		= $rooms["f_currency_extra_bed"];
			$room["extrabed"]["price"]			= $rooms["f_price_extra_bed"];
		}
		$room["main_image"]			= ($rooms["f_thumbnail"]!="")?$this->fn->resize_image(100,700,0,"webimages/product/gallery/$rooms[f_thumbnail]"):"";
		$room["rate_type"]			= $rooms["f_rate_type"];
		if ($rooms["f_bed_type_name"] != ""){
			$room["bed_type"] 		= $rooms["f_bed_type_name"];
		}
		return $room;
	}
	
	//Get All rooms fom current vendor
	public function getRooms($vendor, $lang = ""){
		if ($lang == "") $lang = $vendor["default_language"];
		
		$this->db->where("tb_htl_room_type.f_id_vendor", $vendor["id"]);
		$rooms = $this->searchRooms($lang);
		if (count($rooms) > 0){
			return $rooms;
		}else{
			return FALSE;
		}
	}
	
	//GET Only data room by code
	public function getRoomByCode($vendor, $room_code, $lang = ""){
		if ($lang == "") $lang = $vendor["default_language"];

		if (is_numeric($room_code)){
			$this->db->where("tb_htl_room_type.f_id_room_type", $room_code);
		}else{
			$this->db->where("tb_htl_room_type.f_code_room", $room_code);
		}
		$rooms = $this->getRooms($vendor, $lang);
		if ($rooms){
			$room = $rooms[0];			
			return $room;
		}else{
			return FALSE;
		}
	}
	
	//Get Data detail room by code
	public function getRoomDetailByCode($vendor, $room_code, $lang = ""){
		
		$room = $this->getRoomByCode($vendor, $room_code, $lang);
		
		if ($room){
			//Get Gallery
			$room["gallery"] = $this->getGallery("$room[id]");
			//--
			
			//Facilities
			$room["facilities"] = array_merge($this->getRoomFacilities($room["id"]), $this->getOtherFacilities($room["id"]));

			//Get Data Start From Rates
			$room["start_from_rates"]= $this->getStartFromRate($room, $vendor);
			
			return $room;
		}else{
			return FALSE;
		}
	}	
	private function getGallery($id_room_type){
		$gallery = $this->db->where("f_id_room_type", $id_room_type)
							->get("tb_htl_room_type_gallery")
							->result_array();
		$gallerys = array();
		foreach ($gallery as $index => $img) {
			$gallerys["$index"]["image"] = $this->fn->resize_image(100, 700, 400, "webimages/product/gallery/$img[f_image]");
			$gallerys["$index"]["title"] = $img['f_title'];
		}
		return $gallerys;
	}	
	private function getOtherFacilities($id_room_type){
		
		$this->db->where("f_id_room_type = '$id_room_type'");
		$facilities = $this->db->get("tb_htl_room_type_other_facilities")->result_array();
		
		$arr_facilities = array();
		foreach($facilities as $fac){
			$arr_facilities[] = $fac['f_fasilitas'];
		} 
		return $arr_facilities; 
	}	
	private function getRoomFacilities($id_room_type){
		
		$this->db->where("tb_htl_room_type_fasilitas.f_id_room_type", $id_room_type);
		//$this->db->order_by("tb_s_htl_fasilitas.f_id_fasilitas_kategori asc, f_fasilitas asc");
		$this->db->join("tb_s_htl_fasilitas", "tb_s_htl_fasilitas.f_id_fasilitas = tb_htl_room_type_fasilitas.f_id_fasilitas");
		//$this->db->join("tb_s_htl_fasilitas_kategori", "tb_s_htl_fasilitas.f_id_fasilitas_kategori = tb_s_htl_fasilitas_kategori.f_id_fasilitas_kategori");
		$facility = $this->db->get("tb_htl_room_type_fasilitas")->result_array();
		
		$arr_facilities = array();
		foreach($facility as $index => $fac){
			$arr_facilities[] = $fac['f_fasilitas'];
		} 
		return $arr_facilities;
	}
	private function getStartFromRate($room, $vendor){
		$now = $this->date->now()->getDateOnly(); 
		$start_from_rates = $this->db->select("tb_htl_rates.f_id_room_type, min(tb_htl_rates_detail.f_rates) as start_from, tb_htl_rates.f_currency")
									  ->join("tb_htl_rates", "tb_htl_rates.f_id_rates = tb_htl_rates_detail.f_id_rates")
									  ->join("tb_htl_room_type", "tb_htl_rates.f_id_room_type = tb_htl_room_type.f_id_room_type")
									  ->group_by("tb_htl_rates.f_id_room_type")
									  ->where("tb_htl_room_type.f_id_vendor", $vendor["id"])
									  ->where("tb_htl_room_type.f_id_room_type", $room["id"])
									  ->where("tb_htl_rates.f_status", "1")
									  ->where("tb_htl_rates_detail.f_date > ", "$now")
									  ->get("tb_htl_rates_detail");
		
		if ($start_from_rates->num_rows() > 0){
			$rates = $start_from_rates->row_array();
			return array("rates"=>$rates["start_from"],
						 "currency"=>$rates["f_currency"]);
		}else{
			return FALSE;
		}
		//pre($start_from_rates);
	}
	
	//Additional Items
	public function getAdditionalItems($vendor){
		$additionalItems = $this->db->where("f_id_vendor", $vendor["id"])
									->where("f_deleted", "0")
									->where("f_permanent_deleted <> ", "1")
									->order_by("f_title asc")
									->get("tb_vendor_additional_charge");
		if ($additionalItems->num_rows() > 0){
			$additionalItems =  $additionalItems->result_array();
			$arr_additionalItems = array();
			foreach ($additionalItems as $item){
				$arr_additionalItems[] = $this->generateDataAdditionalItems($item);
			}
			return $arr_additionalItems;
		}else{
			return FALSE;
		}
	}
	private function generateDataAdditionalItems($additionalItem){
		$item = array();
		
		$item["id"] 	= $additionalItem["f_id_additional_charge"];
		$item["name"] 	= $additionalItem["f_title"];
		$item["description"] = $additionalItem["f_description"];
		$item["unit"]	= $additionalItem["f_satuan"];
		$item["price"]	= array("currency" 	=> $additionalItem["f_currency"],
								"price"		=> $additionalItem["f_price"],
								"based_on"	=> $additionalItem["f_payment_based_on"]);
		$item["selection_type"] = $additionalItem["f_mandatory_type"];
		
		return $item;
	}
	
	
	//Check Availability
	function getRoomAllotment($vendor, $arr_room_code, $start_date, $end_date, $jml_room){
		
		//Check Allotment
		$arr_room_id = array();
		$sql = "SELECT tb_htl_room_type.f_id_room_type, datediff('$end_date','$start_date') as night
				FROM `tb_htl_allotment` 
					inner join tb_htl_room_type on tb_htl_allotment.f_id_room_type = tb_htl_room_type.f_link_with_room_id
				WHERE `f_date` >= '$start_date' AND `f_date` < '$end_date' and f_allotment >= '$jml_room'
					and tb_htl_room_type.f_id_vendor = '".$vendor["id"]."'
					".((is_array($arr_room_code) && count($arr_room_code)>0)?" and tb_htl_room_type.f_code_room in ('".implode("','",$arr_room_code)."')":"")."
				group by tb_htl_room_type.f_id_room_type
				having count(tb_htl_room_type.f_id_room_type) >= datediff('$end_date','$start_date')";
		$callotment = $this->db->query($sql)->result_array();
		
		echo $this->db->last_query();
		
		pre_exit($callotment);
		
		
		
		
		
		
		
		//pre_exit($callotment->result_array());

		if ($callotment->num_rows() > 0){
			foreach ($callotment->result_array() as $dtr){ $arr_room_id[] = $dtr["f_id_room_type"]; }
		}
		
		if (count($arr_room_id) > 0){
			//Check Closed Room
			$arr_closed_room = array();
			$sql = "select distinct(tb_htl_room_type.f_id_room_type) as f_id_room_type 
					from tb_htl_close_date
						inner join tb_htl_room_type on tb_htl_close_date.f_id_room_type = tb_htl_room_type.f_link_with_room_id
					where f_date >= '$start_date' and f_date <= '$end_date'
						and tb_htl_room_type.f_id_room_type in ('".implode("','", $arr_room_id)."')";
			$closed_rooms = $this->db->query($sql)->result_array();
			foreach ($closed_rooms as $dtr){ $arr_closed_room[] = $dtr["f_id_room_type"]; }
			
			$arr_available_room = array();
			
			foreach ($arr_room_id as $room_id){
				if (!in_array($room_id, $arr_closed_room)){
					$arr_available_room[] = $room_id;
				}
			}
			
			//Check CTA
			$arr_CTA_room = array();
			if (count($arr_available_room) > 0){
				$sql = "select distinct(tb_htl_room_type.f_id_room_type) as f_id_room_type 
						from tb_htl_close_to_arrival
							inner join tb_htl_room_type on tb_htl_close_to_arrival.f_id_room_type = tb_htl_room_type.f_link_with_room_id
						where f_date = '$start_date' and tb_htl_room_type.f_id_room_type in ('".implode("','", $arr_available_room)."')";
				$CTA_rooms = $this->db->query($sql)->result_array();
				foreach ($CTA_rooms as $dtr){ 
					$arr_CTA_room[] = $dtr["f_id_room_type"]; 
				}
			}
			
			//Check CTD
			$arr_CTD_room = array();
			if (count($arr_available_room) > 0){
				$sql = "select distinct(tb_htl_room_type.f_id_room_type) as f_id_room_type 
						from tb_htl_close_to_departure
							inner join tb_htl_room_type on tb_htl_close_to_departure.f_id_room_type = tb_htl_room_type.f_link_with_room_id
						where f_date = '$end_date' and tb_htl_room_type.f_id_room_type in ('".implode("','", $arr_available_room)."')";
				$CTD_rooms = $this->db->query($sql)->result_array();
				foreach ($CTD_rooms as $dtr){ 
					$arr_CTD_room[] = $dtr["f_id_room_type"]; 
				}
			}
			
			
			foreach ($arr_available_room as $index => $id_available_room){
				if (in_array($id_available_room, $arr_CTA_room) || in_array($id_available_room, $arr_CTD_room)){
					unset($arr_available_room[$index]);
				}
			}
			
			return $arr_available_room;
		}else{
			return false;
		}
	}
	
	
	
	
	

	
	
	public function checkAvailability($vendor, $checkin, $checkout, $qty, $room_code = "", $now = "", $page_number = 1, $search_query = ""){
		$max_limit_room_perpage = 5;
		$current_page = (is_numeric($page_number) && $page_number > 0)?$page_number:1;
		$start_data = ($current_page - 1) * $max_limit_room_perpage;
		
		//Get Data Room
		$this->db->limit($max_limit_room_perpage, $start_data);
		//$this->db->where_in("tb_htl_room_type.f_id_room_type", $arr_available_allotment_room);
		if ($room_code != ""){
			$this->db->where("tb_htl_room_type.f_code_room", $room_code);
		}
		if ($search_query != ""){
			$this->db->where("(tb_htl_room_type_lang.f_room_name like '%$search_query%')");
		}
		$available_rooms = $this->getRooms($vendor);
		$arr_available_rooms = array();
		$arr_available_rooms_id = array();

		if ($available_rooms){
			foreach ($available_rooms as $room) {
				$arr_available_rooms["$room[id]"] = $room;
				$arr_available_rooms_id[] = $room['id'];
			}
		}
		//--
		
		//Cek Allotment
		$arr_available_allotment_room = $this->m_accommodation->getAvailableRoom($vendor["f_id_vendor"], $checkin, $checkout, $qty);
		
		foreach ($arr_available_allotment_room as $index => $id_room){
			if (!in_array($id_room, $arr_available_rooms_id)){
				unset($arr_available_allotment_room[$index]);
			}
		}
		
		if ($arr_available_allotment_room){ //Jika ada allotment
			
			//GET ALLOTMENT FOREACH ROOM
			$this->db->select ("tb_htl_room_type.f_id_room_type, min(f_allotment) as f_allotment")
					 ->join("tb_htl_room_type", "tb_htl_allotment.f_id_room_type = tb_htl_room_type.f_link_with_room_id")
					 ->where_in("tb_htl_room_type.f_id_room_type", $arr_available_allotment_room)
					 ->where("f_date >= '$checkin' and f_date < '$checkout'")
					 ->group_by("tb_htl_room_type.f_id_room_type");
			
			$arr_allotment = array();
			foreach ($this->db->get("tb_htl_allotment")->result_array() as $allotment){
				$arr_allotment["$allotment[f_id_room_type]"] = $allotment["f_allotment"];
			}
			//--
			
			//Get Room Rate
			$arr_promo_rate = $this->m_accommodation->getPromoRate($arr_available_allotment_room, $checkin, $checkout , $vendor["f_id_vendor"]);
			//--
			
			$availabilities = array();
			foreach ($arr_promo_rate as $promo_rates){
				if (isset($arr_available_rooms["$promo_rates[f_id_room]"])){
					$availability 				= $this->generateDataRates($promo_rates);
					$availability["breakdown_price"] = $this->get_breakdown_price($promo_rates["f_id_room"], $checkin, $checkout, $promo_rates["f_ss_create"], $promo_rates["f_id_rate_type"]);
					$availability["allotment"]	= $arr_allotment["$promo_rates[f_id_room]"];
					$availability["room"]		= $arr_available_rooms["$promo_rates[f_id_room]"];
					
					$availabilities[] = $availability;
				}
			}
			
			//Packages
			$night 	= strtotime($checkout) - strtotime($checkin);
			$night 	= ceil($night/3600/24);
			
			$packages = $this->db->where_in("f_id_room_type", $arr_available_rooms_id)
								 ->where("((f_booking_start_date = '0000-00-00' and f_booking_end_date = '0000-00-00')
											or 
										  (f_booking_start_date <= '$now' and f_booking_end_date >= '$now'))")
								 ->where("f_start_date <= ", $checkin)
								 ->where("f_end_date >= ", $checkout)
								 ->where("f_night <= ", $night)
								 ->where("f_id_vendor", $vendor["f_id_vendor"])
								 ->where("tb_htl_package_rates.f_status", "1")
								 ->order_by("f_rates asc")
								 ->join("tb_htl_package_rates_detail", "tb_htl_package_rates_detail.f_id_package_rates = tb_htl_package_rates.f_id_package_rates")
								 ->get("tb_htl_package_rates")
								 ->result_array();
			foreach ($packages as $package){
				if (isset($arr_available_rooms["$package[f_id_room_type]"])){
					
					if ($night == $package["f_night"] || ($night >= $package["f_night"] && $package["f_allow_to_extend"] == '1')){
						$availability 				= $this->generateDataRates($package);
						$availability["breakdown_price"] = $this->get_breakdown_price_packages($promo_rates["f_id_room"], $checkin, $checkout, $package);
						$availability["allotment"]	= $arr_allotment["$promo_rates[f_id_room]"];
						$availability["room"]		= $arr_available_rooms["$package[f_id_room_type]"];
						
						$availabilities[] = $availability;
					}
				}
			}
			
			$data_return["availabilities"]		= $availabilities;
			$data_return["pagination"]["page"]	= $current_page;
			if ($search_query!= ''){
				$data_return["pagination"]["search_query"]	= $search_query;
			}
			
			return $data_return;
		}else{ //Jika Allotment tidak ada
			return FALSE;
		}
	}
	
	private function generateDataRates($rates){
		if (isset($rates["f_id_package_rates"])){
			$arr_rates ["code"] 		= $rates["f_id_package_rates"];
			$arr_rates ["rates_name"] 	= $rates["f_package_name"];
			$arr_rates ["rates_type"] 	= "Packages";
			$arr_rates ["id_rates_type"]= 'PACKAGE';
			$arr_rates ["currency"] 	= $rates["f_currency"];
			$arr_rates ["start_from"] 	= $rates["f_rates"];
			$arr_rates ["include_breakfast"]= ($rates["f_inc_breakfast"]==1)?"YES":"NO";
			$arr_rates ["description"] 	= $rates["f_term_and_condition"];
			$arr_rates ["booking_handling"]	= $rates["f_booking_type"];
		}else{
			$arr_rates ["code"] 		= $rates["f_ss_create"];
			$arr_rates ["rates_name"] 	= $rates["f_promo_name"];
			$arr_rates ["rates_type"] 	= $rates["f_rate_description"];
			$arr_rates ["id_rates_type"]= $rates["f_id_rate_type"];
			$arr_rates ["currency"] 	= $rates["f_currency"];
			$arr_rates ["start_from"] 	= $rates["f_rates"];
			$arr_rates ["include_breakfast"]= ($rates["f_inc_breakfast"]==1)?"YES":"NO";
			$arr_rates ["description"] 	= $rates["f_term_and_condition"];
			$arr_rates ["booking_handling"]	= $rates["f_booking_type"];
		}
		
		return $arr_rates;
	}
	
	private function get_breakdown_price($id_room_type, $checkin_date, $checkout_date, $f_ss_create, $f_id_rate_type){
		//Get Publish Rate 
		$id_publish_rate = SYSTEM_BASIC_SETTING("HTL_RATE_TYPE_PUBLISH");
		$publish_rate = $this->db->where("tb_htl_rates.f_id_room_type",	$id_room_type)
								 ->where("tb_htl_rates.f_id_rate_type", 	$id_publish_rate)
								 ->where("tb_htl_rates_detail.f_date >= ", 	$checkin_date)
								 ->where("tb_htl_rates_detail.f_date < ", 	$checkout_date)
								 ->join("tb_htl_rates_detail", "tb_htl_rates_detail.f_id_rates = tb_htl_rates.f_id_rates")
								 ->order_by("f_date asc")
								 ->get("tb_htl_rates")
								 ->result_array();
		$arr_publish_rate = array();
		foreach ($publish_rate as $pr){
			$arr_publish_rate["$pr[f_date]"] = $pr;
		}
		
		$breakdown_rate = $this->db->where("tb_htl_rates.f_id_room_type",	$id_room_type)
								 ->where("tb_htl_rates.f_id_rate_type", 	$f_id_rate_type)
								 ->where("tb_htl_rates.f_ss_create", 		$f_ss_create)
								 ->where("tb_htl_rates_detail.f_date >= ", 	$checkin_date)
								 ->where("tb_htl_rates_detail.f_date < ", 	$checkout_date)
								 ->join("tb_htl_rates_detail", "tb_htl_rates_detail.f_id_rates = tb_htl_rates.f_id_rates")
								 ->order_by("f_date asc")
								 ->get("tb_htl_rates")
								 ->result_array();
		$arr_breakdown = array();
		foreach ($breakdown_rate as $br){
			$arr_breakdown["$br[f_date]"] = $br;
		}
	
		$arr_breakdown_return = array();
		if ($f_id_rate_type == SYSTEM_BASIC_SETTING("HTL_RATE_TYPE_LAST_MINUTES")){
			$current_date = $checkin_date;
			$last_minutes = @$breakdown_rate[0]["f_last_minutes"];
			$i = 1;
			do{
				if (isset($arr_breakdown["$current_date"]) && $i <= $last_minutes){
					$arr_breakdown_return["$current_date"] = $arr_breakdown["$current_date"];
					$arr_breakdown_return["$current_date"]["is_promo"] = true;
				}else{
					$arr_breakdown_return["$current_date"] = isset($arr_publish_rate["$current_date"])?$arr_publish_rate["$current_date"]:false;
					$arr_breakdown_return["$current_date"]["is_promo"] = false;
				}
				$i++ ;
				$current_date = date("Y-m-d", strtotime("+1 day",strtotime($current_date)));
			}while(strtotime($current_date) < strtotime($checkout_date));
		}else{
			$current_date = date("Y-m-d", strtotime($checkin_date));
			do{
				if (isset($arr_breakdown["$current_date"])){
					$arr_breakdown_return["$current_date"] = $arr_breakdown["$current_date"];
					$arr_breakdown_return["$current_date"]["is_promo"] = true;
				}else{
					$arr_breakdown_return["$current_date"] = $arr_publish_rate["$current_date"];
					$arr_breakdown_return["$current_date"]["is_promo"] = false;
				}
				$current_date = date("Y-m-d", strtotime("+1 day",strtotime($current_date)));
			}while(strtotime($current_date) < strtotime($checkout_date));
		}
		
		$generate_data_breakdown_rate = array();
		foreach ($arr_breakdown_return as $date => $breakdown){
			$rates = array();
			$rates["date"]		= $date;
			$rates["currency"]	= $breakdown["f_currency"];
			$rates["rates"]		= $breakdown["f_rates"];
			$generate_data_breakdown_rate[] = $rates;
		}
		return $generate_data_breakdown_rate;
	}
	
	private function get_breakdown_price_packages($id_room_type, $checkin_date, $checkout_date, $packages){
		$breakdown_rate = array();

		if ($packages){
			$looping = 1;
			$curent_date = $checkin_date;
			while (strtotime($curent_date) < strtotime($checkout_date)){
				$rate = array();
				$rate["date"] 	= $curent_date;
				$rate["currency"] = $packages["f_currency"];
				if ($looping == 1){
					$rate["rates"] = $packages["f_rates"];
				}elseif ($looping <= $packages['f_night']){
					$rate["rates"] = 0;
				}else{
					$rate["rates"] = $packages["f_extend_rates"];
				}
				
				$breakdown_rate[$curent_date] = $rate;
				$curent_date = date("Y-m-d", strtotime("+1 days", strtotime($curent_date)));
				$looping++; 
			}
		}
		//pre($packages);
		return $breakdown_rate;
	}
	
	public function propertyDetail($vendor){
		$property_setting = $this->db->where("f_id_vendor", $vendor["f_id_vendor"])->get("tb_htl_detail");
		if ($property_setting->num_rows() > 0){
			$property_setting = $property_setting->row_array();
			
			$return["allow_children"]		= $property_setting["f_allow_children"]=="1"?"YES":"NO";
			$return["children_free_share_with_parent"] = $property_setting["f_free_share_with_parent"]=="1"?"YES":"NO";
			$return["children_age_min"] 	= $vendor["f_age_child_start"];
			$return["children_age_max"] 	= $vendor["f_age_child_until"];
			
			$return["checkin_time"] 		= $property_setting["f_check_in_time"];
			$return["checkout_time"] 		= $property_setting["f_check_out_time"];
			
			$return["pickup_service"] 		= $property_setting["f_pick_up_transport"]=="1"?"YES":"NO";
			$return["pickup"]["currency"] 	= $property_setting["f_pick_up_transport_currency"];
			$return["pickup"]["price"] 		= $property_setting["f_pick_up_transport_price"];
			$return["pickup"]["per"] 		= $property_setting["f_pick_up_satuan"];
			$return["pickup"]["remarks"] 	= $property_setting["f_pick_up_remarks"];
			
			return $return;
		}else{
			return FALSE;
		}
	}
	
}
?>
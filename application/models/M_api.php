<?php
class M_api extends CI_Model {
	public $err_delimiter = "{||}";
	
    function __construct(){
		parent::__construct();
    }
	
	public function validate_merchant($merchant_code = "", $merchant_key = ""){
		if (isset($_POST["merchant_code"]) && isset($_POST["merchant_key"])){
			$merchant_code 	= $_POST["merchant_code"];
			$merchant_key	= $_POST["merchant_key"];
		}
		
		$arr_return["status"] 		= "ERROR";
		$arr_return["error_msg"] 	= array();
		
		if ($merchant_code == ""){
			$arr_return["status"] 		= "ERROR";
			$arr_return["error_msg"][]	= "Merchant Code is required";
		}elseif ($merchant_key == ""){
			$arr_return["status"] 		= "ERROR";
			$arr_return["error_msg"][]	= "Merchant Key is required";
		}else{
			$merchant = $this->db->select("f_id_vendor, f_company, f_code_vendor, f_merchant_API_key")
								 ->where("f_code_vendor", $merchant_code)
								 ->where("f_merchant_API_key", $merchant_key)
								 ->get("tb_vendor");
			if ($merchant->num_rows() > 0){
				$arr_return = TRUE;
			}else{
				$arr_return["status"] 		= "ERROR";
				$arr_return["error_msg"][]	= "Merchant Not Found";
			}
		}
		return $arr_return;
	}

}
?>
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Phpqrcode {
	protected $_ci;
	function __construct(){
		$this->_ci =& get_instance();
	}
	
	function generate($param_str, $param_level = "", $param_size = ""){
		require_once(dirname(__FILE__).'/phpqrcode/qrlib.php');

		//set it to writable location, a place for temp generated PNG files
		$PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
		$PNG_TEMP_DIR = "public/qr".DIRECTORY_SEPARATOR;
		
		//html PNG location prefix
		$PNG_WEB_DIR = base_url("public/qr/");
	
		//ofcourse we need rights to create temp dir
		if (!file_exists($PNG_TEMP_DIR)) mkdir($PNG_TEMP_DIR);
		
		$filename = $PNG_TEMP_DIR.md5($param_str).'.png';
		
		//processing form input
		//remember to sanitize user input in real-life solution !!!
		$errorCorrectionLevel = 'H';
		if ($param_level != "" && in_array($param_level, array('L','M','Q','H')))
			$errorCorrectionLevel = $param_level;    
	
		$matrixPointSize = 7;
		if ($param_size!="")
			$matrixPointSize = min(max((int)$param_size, 1), 10);
			
		// user data
		$filename = $PNG_TEMP_DIR.md5($param_str.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
		QRcode::png($param_str, $filename, $errorCorrectionLevel, $matrixPointSize, 2);    
						
		//display generated file
		return $PNG_WEB_DIR.basename($filename);
	}
}
?>
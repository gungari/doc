<?php  
if(!isset($_SESSION)) @session_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Fn extends Fnlib {
	protected $_ci;
	function __construct(){
		$this->_ci =& get_instance();
		//$this->_ci->load->database();
		//$this->_ci->load->library('session');
	}
	
	function set_ss_CRS($ss){
		$_SESSION["ss_login_CRS"] = $ss;
	}
	function get_ss_CRS($ss = ""){
		$session = $_SESSION;
		
		if (isset($session["ss_login_CRS"])){
			if ($ss==""){
				return $session["ss_login_CRS"];
			}elseif (isset($session["ss_login_CRS"][$ss])){
				return $session["ss_login_CRS"][$ss];
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	function set_ss_CRS_agent($ss){
		$_SESSION["ss_login_CRS_agent"] = $ss;
	}
	function get_ss_CRS_agent($ss = ""){
		$session = $_SESSION;
		
		if (isset($session["ss_login_CRS_agent"])){
			if ($ss==""){
				return $session["ss_login_CRS_agent"];
			}elseif (isset($session["ss_login_CRS_agent"][$ss])){
				return $session["ss_login_CRS_agent"][$ss];
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	function isAgentCRS_ACT($vendor){
		
		$cat = "activities";
		if (is_array($vendor["category"])){
			return in_array($cat, $vendor["category"]);
		}else{
			return (strtoupper($vendor["category"]) == strtoupper($cat));
		}
		/*
		if (isset($vendor["category"]) && strtoupper($vendor["category"]) == strtoupper("activities")){
			return TRUE;
		}else{
			return FALSE;
		}*/
	}
	function isAgentCRS_TRANS($vendor){
		$cat = "transport";
		if (is_array($vendor["category"])){
			return in_array($cat, $vendor["category"]);
		}else{
			return (strtoupper($vendor["category"]) == strtoupper($cat));
		}
		/*
		if (isset($vendor["category"]) && strtoupper($vendor["category"]) == strtoupper("transport")){
			return TRUE;
		}else{
			return FALSE;
		}*/
	}
	function isAgentCRS_ACCOM($vendor){
		$cat = "accommodation";
		if (is_array($vendor["category"])){
			return in_array($cat, $vendor["category"]);
		}else{
			return (strtoupper($vendor["category"]) == strtoupper($cat));
		}
		/*
		if (isset($vendor["category"]) && strtoupper($vendor["category"]) == strtoupper("transport")){
			return TRUE;
		}else{
			return FALSE;
		}*/
	}
	
	function isVendorCRS_ACT($vendor){
		
		$cat = "activities";
		if (is_array($vendor["category"])){
			return in_array($cat, $vendor["category"]);
		}else{
			return (strtoupper($vendor["category"]) == strtoupper($cat));
		}
		/*
		if (isset($vendor["category"]) && strtoupper($vendor["category"]) == strtoupper("activities")){
			return TRUE;
		}else{
			return FALSE;
		}*/
	}
	function isVendorCRS_TRANS($vendor){
		$cat = "transport";
		if (is_array($vendor["category"])){
			return in_array($cat, $vendor["category"]);
		}else{
			return (strtoupper($vendor["category"]) == strtoupper($cat));
		}
		/*
		if (isset($vendor["category"]) && strtoupper($vendor["category"]) == strtoupper("transport")){
			return TRUE;
		}else{
			return FALSE;
		}*/
	}
	function isVendorCRS_ACCOM($vendor){
		$cat = "accommodation";
		if (is_array($vendor["category"])){
			return in_array($cat, $vendor["category"]);
		}else{
			return (strtoupper($vendor["category"]) == strtoupper($cat));
		}
		/*
		if (isset($vendor["category"]) && strtoupper($vendor["category"]) == strtoupper("transport")){
			return TRUE;
		}else{
			return FALSE;
		}*/
	}
	
	
	
	
	
	function get_ss_pilot($ss=""){
		$session = $this->_ci->session->all_userdata();
		if (isset($session["ss_login_pilot"])){
			if ($ss==""){
				return $session["ss_login_pilot"];
			}else{
				return $session["ss_login_pilot"][$ss];
			}
		}else{
			return false;
		}
	}
	function get_ss_extranet($ss=""){
		//$session = $this->_ci->session->all_userdata();
		$session = $_SESSION;
		if (isset($session["ss_login_extranet"])){
			if ($ss==""){
				return $session["ss_login_extranet"];
			}elseif (isset($session["ss_login_extranet"][$ss])){
				return $session["ss_login_extranet"][$ss];
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	function get_ss_vendor($ss=""){
		return $this->get_ss_extranet($ss);
	}
	function isExtranetAutoLogin(){
		if ($this->get_ss_extranet("auto_login") && $this->get_ss_extranet("auto_login")=="1"){ 
			return true;
		}else{
			return false;
		}
	}
	function isVendorAutoLogin(){
		return $this->isExtranetAutoLogin();
	}
	function isVendorHybridPayment($vendor=null) {
		$vendor = $vendor==null ? $this->get_ss_vendor() : $vendor;
		if(isset($vendor["f_id_vendor"])) {
			$cek_payment_gateway = $this->_ci->db->where("f_id_vendor", $vendor["f_id_vendor"])
				->where("f_payment_type", "HYBRID")
				->get("tb_vendor_payment_type");
			return ($cek_payment_gateway->num_rows() > 0);
		} else {
			return false;
		}
	}
	function isVendorAutoSettlement($vendor=null) {
		$vendor = $vendor==null ? $this->_ci->m_vendor->getVendorBySS() : $vendor;
		if(isset($vendor["BookingEngineSetting"]["f_HYBRID_auto_settlement"])&&$vendor["BookingEngineSetting"]["f_HYBRID_auto_settlement"]=="1") {
			return true;
		} else {
			return false;
		}
	}
	function set_ss_extranet($arr_vendor, $auto_login = false){
		//$this->_ci->session->unset_userdata("ss_login_extranet");
		$id_role = @$arr_vendor["f_id_role"];
		if($id_role==""){
			$id_role="1";
		}
		$session = array(
						"f_id_vendor"       =>$arr_vendor["f_id_vendor"],
						"f_email"           =>$arr_vendor["f_email"],
						"f_company"         =>$arr_vendor["f_company"],
						"f_code_vendor"     =>$arr_vendor["f_code_vendor"],
						"f_id_role"         =>@$id_role,
						"f_id_login_access" =>@$arr_vendor["f_id_login_access"],
						"f_name" =>@$arr_vendor["f_name"],
						"f_role_name" =>@$arr_vendor["f_role_name"],
					);
		if ($auto_login) $session["auto_login"] = true;
		//$ss["ss_login_extranet"] = $session;
		//$this->_ci->session->set_userdata($ss);
		$_SESSION["ss_login_extranet"] = $session;
	}
	function set_ss_vendor($arr_vendor){
		return $this->set_ss_extranet($arr_vendor);
	}
	function get_ss_member($ss=""){
		//$session = $this->_ci->session->userdata('ss_login_member');
		$session = (isset($_SESSION["ss_login_member"]))?$_SESSION["ss_login_member"]:false;
		if ($session){
			if ($ss==""){
				return $session;
			}else{
				return $session[$ss];
			}
		}else{
			return false;
		}
	}
	function set_ss_member($arr_member){
		$this->_ci->session->unset_userdata("ss_login_member");
		$member = $arr_member;
		$session = array(
					"f_id_member"		=>$member["f_id_member"],
					"f_first_name"		=>$member["f_first_name"],
					"f_last_name"		=>$member["f_last_name"],
					"f_name"			=>$member["f_first_name"]." ".$member["f_last_name"],
					"f_code_member"		=>$member["f_code_member"],
					"f_email"			=>$member["f_email"],
					"f_mobile"			=>$member["f_mobile"],
					"f_country"			=>$member["f_country"],
					"f_is_custom"		=>(isset($member["f_is_custom"]))?$member["f_is_custom"]:false
					//"f_type"			=>$member["f_type"],
					//"f_activation_code"	=>$member["f_activation_code"],
					//"is_active"			=>($member["f_activation_code"]==""),
					);
		$_SESSION["ss_login_member"] = $session;
		//$ss["ss_login_member"] = $session;
		//$this->_ci->session->set_userdata($ss);
	}
	function unset_ss_member(){
		unset($_SESSION["ss_login_member"]);
	}
	function set_ss_currency($currency = "USD"){
		$_SESSION["ss_currency_view"] = $currency;
		//$ss_currency["ss_currency_view"] = $currency;
		//$this->_ci->session->set_userdata($ss_currency);
	}
	function get_ss_currency(){
		if (isset($_SESSION["ss_currency_view"])){
			return $_SESSION["ss_currency_view"];
		}else{
			return false;
		}
		//if (!$this->_ci->session->userdata('ss_currency_view')){ $this->set_ss_currency(); }
		//return $this->_ci->session->userdata('ss_currency_view');
	}
	function set_ss_lang($lang = "EN"){//yoga
		if (!$this->_ci->my_config->getLanguage($lang)){ $lang = "EN"; }
		$_SESSION["ss_language_view"] = $lang;
		//$ss_lang["ss_language_view"] = $lang;
		//$this->_ci->session->set_userdata($ss_lang);
	}	
	//yoga
	function set_ss_lang_extranet($lang = "EN"){
		if (!$this->_ci->my_config->getLanguage($lang)){ $lang = "EN"; }
		$_SESSION["ss_language_view_extranet"] = $lang;
		//$ss_lang["ss_language_view"] = $lang;
		//$this->_ci->session->set_userdata($ss_lang);
	}
	function get_ss_lang(){
		if (isset($_SESSION["ss_language_view"])){
			return $_SESSION["ss_language_view"];
		}else{
			return false;
		}
		//if (!$this->_ci->session->userdata('ss_language_view')){ $this->set_ss_lang(); }
		//return $this->_ci->session->userdata('ss_language_view');
	}
	//yoga
	function get_ss_lang_extranet(){
		if (isset($_SESSION["ss_language_view_extranet"])){
			return $_SESSION["ss_language_view_extranet"];
		}else{
			return false;
		}
		//if (!$this->_ci->session->userdata('ss_language_view')){ $this->set_ss_lang(); }
		//return $this->_ci->session->userdata('ss_language_view');
	}
	function set_lang_and_currency_from_get_parameter(){
		if (isset($_GET["setlang"])){
			$_GET["setlang"] = strtoupper($_GET["setlang"]);
			if ($this->_ci->my_config->getLanguage($_GET["setlang"])){
				$this->_ci->fn->set_ss_lang($_GET["setlang"]);
			}
		}
		if (isset($_GET["setcurrency"])){
			$_GET["setcurrency"] = strtoupper($_GET["setcurrency"]);
			if ($this->_ci->my_config->getCurrency($_GET["setcurrency"])){
				$this->_ci->fn->set_ss_currency($_GET["setcurrency"]);
			}
		}
	}
	function set_ss_data_rsv($code_vendor, $new_session){
		$data_rsv = $_SESSION["data_rsv"]; //$this->get_ss_data_rsv("data_rsv");
		$data_rsv[$code_vendor] = $new_session;
		//$this->_ci->session->unset_userdata("data_rsv");
		$_SESSION["data_rsv"] = $data_rsv;
		//$this->_ci->session->set_userdata("data_rsv", $data_rsv);
	}
	function get_ss_data_rsv($code_vendor, $ss=""){
		///$session = $this->_ci->session->userdata('data_rsv');
		$session = isset($_SESSION["data_rsv"])?$_SESSION["data_rsv"]:false;
		if (isset($session[$code_vendor])){
			if ($ss==""){
				return $session[$code_vendor];
			}else{
				if (isset($session[$code_vendor][$ss])){
					return $session[$code_vendor][$ss];
				}else{
					return false;
				}
			}
		}else{
			return false;
		}
		//return $this->_ci->session->userdata('data_rsv');
	}
	function set_ss_data_rsv_vendor($code_vendor, $new_session){
		$data_rsv = $this->get_ss_data_rsv("data_rsv_vendor");
		$data_rsv[$code_vendor] = $new_session;
		//$this->_ci->session->unset_userdata("data_rsv");
		$_SESSION["data_rsv_vendor"] = $data_rsv;
		//$this->_ci->session->set_userdata("data_rsv", $data_rsv);
	}
	function get_ss_data_rsv_vendor($code_vendor, $ss=""){
		///$session = $this->_ci->session->userdata('data_rsv');
		$session = isset($_SESSION["data_rsv_vendor"])?$_SESSION["data_rsv_vendor"]:false;
		if (isset($session[$code_vendor])){
			if ($ss==""){
				return $session[$code_vendor];
			}else{
				if (isset($session[$code_vendor][$ss])){
					return $session[$code_vendor][$ss];
				}else{
					return false;
				}
			}
		}else{
			return false;
		}
		//return $this->_ci->session->userdata('data_rsv');
	}
	function get_ss_wishlist(){
		$session = $this->_ci->session->userdata('ss_wishlist');
		if ($session){
			return $session;
		}else{
			return false;
		}
		//return $this->_ci->session->userdata('data_rsv');
	}
	function show_star_rating($rating){
		?>
		<?php for($i=1; $i<=$rating; $i++){ ?>
        <img src="<?=base_url()?>public/images/stars/full.png" class="star" />
        <?php } ?>
        <?php if ($rating > floor($rating)){ ?>
        <img src="<?=base_url()?>public/images/stars/half.png" class="star" />
        <?php } ?>
        <?php for($i=ceil($rating); $i<5; $i++){ ?>
        <img src="<?=base_url()?>public/images/stars/silver.png" class="star" />
        <?php } ?>
		<?php
	}
	function isVendorACC($vendor){
		if (isset($vendor["RegisteredCategory"]["f_id_extranet_type"])){
			return ($vendor["RegisteredCategory"]["f_id_extranet_type"] == SYSTEM_BASIC_SETTING("EXTRANET_TYPE_ACCOMMODATION"));
		}else{
			return false;
		}
	}
	function isVendorACT($vendor){
		if (isset($vendor["RegisteredCategory"]["f_id_extranet_type"])){
			return ($vendor["RegisteredCategory"]["f_id_extranet_type"] == SYSTEM_BASIC_SETTING("EXTRANET_TYPE_ACTIVITIES"));
		}else{
			return false;
		}
	}
	function isVendorRESTO($vendor){
		if (isset($vendor["RegisteredCategory"]["f_id_extranet_type"])){
			return ($vendor["RegisteredCategory"]["f_id_extranet_type"] == SYSTEM_BASIC_SETTING("EXTRANET_TYPE_RESTO"));
		}else{
			return false;
		}
	}
	function isVendorTRANS($vendor){
		if (isset($vendor["RegisteredCategory"]["f_id_extranet_type"])){
			return ($vendor["RegisteredCategory"]["f_id_extranet_type"] == SYSTEM_BASIC_SETTING("EXTRANET_TYPE_TRANSPORT"));
		}else{
			return false;
		}
	} 
	function isVendorTRANSV2($vendor){
		// hanya untuk migrasi dari transport v1 ke v2
		//if ($this->isExtranetAutoLogin() && $this->get_ss_extranet("f_id_vendor")==197){ 
		//	return true;
		//}
		if (isset($vendor["RegisteredCategory"]["f_id_extranet_type"])&&$vendor['RegisteredCategory']['f_is_transport_v2']==1){
			return ($vendor["RegisteredCategory"]["f_id_extranet_type"] == SYSTEM_BASIC_SETTING("EXTRANET_TYPE_TRANSPORT"));
		}else{
			return false;
		}
	} 
	function isVendorTRANS_V2($vendor){
		return $this->isVendorTRANSV2($vendor);
	} 
	function get_ss_vendorxx($ss=""){
		$session = $this->_ci->session->all_userdata();
		if (isset($session["ss_login_vendor_activities"])){
			if ($ss==""){
				return $session["ss_login_vendor_activities"];
			}else{
				return $session["ss_login_vendor_activities"][$ss];
			}
		}else{
			return false;
		}
	}
	function get_ss_www148($ss=""){
		$session = $this->_ci->session->all_userdata();
		if (isset($session["ss_login_www148"])){
			if ($ss==""){
				return $session["ss_login_www148"];
			}else{
				return $session["ss_login_www148"][$ss];
			}
		}else{
			return false;
		}
	}
	/*function set_ss_currency($currency = ""){
		//$_SESSION["ss_currency_view"] = $currency;
		if ($currency==""){
			$server = $_SERVER["SERVER_NAME"];
			if (strtolower(substr($server, 0, 2)) == "en"){
				$currency = "USD";
			}else{
				$currency = "IDR";
			}
		}
		$ss_currency["ss_currency_view"] = $currency;
		$this->_ci->session->set_userdata($ss_currency);
	}
	function get_ss_currency(){
		if (!$this->_ci->session->userdata('ss_currency_view')){
			$this->set_ss_currency();
		}
		return $this->_ci->session->userdata('ss_currency_view');
		//if (!isset($_SESSION["ss_currency_view"])){
		//	$this->set_ss_currency();
		//}
		//return $_SESSION["ss_currency_view"];
	}
	function set_ss_lang($lang = "ID"){
		//$_SESSION["ss_language_view"] = $lang;
		$ss_lang["ss_language_view"] = $lang;
		$this->_ci->session->set_userdata($ss_lang);
	}	
	function get_ss_lang(){
		$server = $_SERVER["SERVER_NAME"];
		$lang = "ID";
		if (strtolower(substr($server, 0, 2)) == "en"){
			$lang = "EN";
		}else{
			$lang = "ID";
		}
		return $lang;
		//if (!$this->_ci->session->userdata('ss_language_view')){
		//	$this->set_ss_lang();
		//}
		//return $this->_ci->session->userdata('ss_language_view');
		//if (!isset($_SESSION["ss_language_view"])){
		//	$this->set_ss_lang();
		//}
		//return $_SESSION["ss_language_view"];
	}*/
	function isMemberAutoLogin(){
		//return true;
		$session = $this->_ci->session->all_userdata();
		if (isset($session["ss_login_member"]["auto_login"])){
			return true;
		}else{
			return false;
		}
	}
	function getCountryByCode($code){
		$country = $this->_ci->where("code", $code)->get("tb_countries");
		if ($country->num_rows() > 0){
			$country = $country->row_array();
			return $country["name"];
		}else{
			return false;
		}
	}
	function user_log($f_email, $f_id_user, $f_user_name, $f_act, $f_description, $isAutoLogin = "0", $f_receipt_number="", $f_long_description = ""){
		$save_log = true;
		if (isset($_GET["print_email"])) $save_log = false;
		if ($save_log){
			$this->_ci->load->library('user_agent');
			$data["f_email"] 		= $f_email;
			$data["f_id_user"] 		= (!is_numeric($f_id_user))?"0":$f_id_user ;
			$data["f_user_name"] 	= $f_user_name;
			$data["f_act"] 			= $f_act;
			$data["f_url"] 			= current_url();
			$data["f_ip_address"] 	= $this->_ci->input->ip_address();;
			$data["f_user_agent"] 	= $this->_ci->agent->browser()." - ".$this->_ci->agent->agent_string()." - ".$this->_ci->agent->platform()." - ".$this->_ci->agent->mobile();
			$data["f_description"] 	= $f_description;
			$data["f_is_auto_login"] 	= $isAutoLogin;
			$data["f_receipt_number"] 	= $f_receipt_number;
			$data["f_long_description"] = $f_long_description;
			$this->_ci->db->insert("tb_user_log", $data);
		}
	}
	function save_vendor_log($action, $status = "SUCCESS"){
		$max_log = 20;
		$lv = $this->get_ss_vendor();
		$sql = "SELECT * FROM `tb_user_log` WHERE `f_id_user` = '".$lv["f_id_vendor"]."' ORDER BY `f_id`desc limit $max_log";
		$log = $this->_ci->db->query($sql);
		if ($log->num_rows() >= $max_log){
			$log = $log->result_array();
			$log = $log[($max_log-1)];
			$this->_ci->db->where("f_id_user", $lv["f_id_vendor"])
						  ->where("f_id <= ", $log["f_id"])
						  ->delete("tb_user_log");
		}
		$log_desc = $status; if ($this->isVendorAutoLogin()) $log_desc.= " - Auto Login - ".$this->get_ss_www148("f_username");
		$this->user_log($lv["f_email"], $lv["f_id_vendor"], $lv["f_company"], "VENDOR - ".$action, $log_desc, ($this->isVendorAutoLogin())?"1":"0");
	}	
	function save_member_log($action, $status = "SUCCESS"){
		$lv = $this->get_ss_member();
		$log_desc = $status; if ($this->isMemberAutoLogin()) $log_desc.= " - Auto Login - ".$this->get_ss_www148("f_username");
		$this->user_log($lv["f_email"], $lv["f_id_member"], $lv["f_name"], "MEMBER - ".$action, $log_desc, ($this->isMemberAutoLogin())?"1":"0");
	}
	function save_www148_log($action, $status = "SUCCESS"){
		$lv = $this->get_ss_www148();
		$log_desc = $status;
		$this->user_log($lv["f_username"], $lv["f_id"], $lv["f_name"], "WWW148 - ".$action, $log_desc);
	}
	function product_sudah_edit(){
		$res = $this->_ci->db->where("f_type", "P")
					  		 ->get("tb_act_venues_vendor_sudah_edit")
					  		 ->result_array();
		$arr_res = array();
		foreach ($res as $r) $arr_res[] = $r["f_id_product_vendor"];
		return $arr_res;
	}
	function vendor_sudah_edit(){
		$res = $this->_ci->db->where("f_type", "V")
					  		 ->get("tb_act_venues_vendor_sudah_edit")
					  		 ->result_array();
		$arr_res = array();
		foreach ($res as $r) $arr_res[] = $r["f_id_product_vendor"];
		return $arr_res;
	}
function set_resto_sess($data,$id_outlet){
			$_SESSION["id_vendor"] = $data['f_id_vendor'];
			$_SESSION["f_currency_filter"] = $data['f_currency_filter'];
			$_SESSION["id_outlet"] = $id_outlet;
			//$this->set_ss_currency($data['f_currency_filter']);
		}
	function get_resto_sess(){
		if(isset($_SESSION["id_vendor"])){
			$data['id_vendor']=$_SESSION["id_vendor"];
			$data['f_currency_filter']=$_SESSION["f_currency_filter"];
			$data['id_outlet']=$_SESSION["id_outlet"];
			return $data;
		}else{return false;}
	}
	function add_resto_cart($data=null){
		$cart_products = $this->get_resto_cart();
        $cart_products[] = array(
            'id_product' => $data['id_product'],
            'num' =>  $data['num'],
            'title' => $data['title']
        );
		$_SESSION['cart_products' . $_SESSION["id_vendor"]]=$cart_products;
	}
	function get_resto_cart(){
		return @$_SESSION['cart_products' . $_SESSION["id_vendor"]];
	}
	function remove_cart_by_id($id) {
      $cart_products = $this->get_resto_cart();
      $cart_products2=null;
        for ($i = 0; $i < count($cart_products); $i++) {
              if ($cart_products[$i]['id_product']!=$id) {
                    $cart_products2[] = array(
                        'id_product' => $cart_products[$i]['id_product'],
                        'num' => $cart_products[$i]['num'],
                        'title' => $cart_products[$i]['title']
                    );
                }
        }
       	unset($_SESSION['cart_products' . $_SESSION["id_vendor"]]);
        $_SESSION['cart_products' . $_SESSION["id_vendor"]]=$cart_products2;
		}
		function update_cart($x,$num){
		$cart_products = $this->get_resto_cart();
		if($num>0){
			$cart_products[$x] = array(
				'id_product' => $cart_products[$x]['id_product'],
				'num' =>  $num,
				'title' => $cart_products[$x]['title']
			);
			$_SESSION['cart_products' . $_SESSION["id_vendor"]]=$cart_products;
		}else{
			$this->remove_cart_by_id($cart_products[$x]['id_product']);
		}
		}
		function set_table_sess($table){
			$table=array(
			'pax'=>$table['pax'],
			'date'=>$table['date'],
			'timing_type'=>$table['timing_type'],
			'time'=>$table['time'],
			'duration'=>$table['duration'],
			'table'=>$table['table']
			//'area'=>$table['area']
			);
			$_SESSION['cart_table' . $_SESSION["id_vendor"]]=$table;
	}
	function get_table_cart(){
		return @$_SESSION['cart_table' . $_SESSION["id_vendor"]];
	}
	function set_sess_inv($invoice){
	return $_SESSION['invoice']=$invoice;
	}
	function get_sess_inv(){
	return @$_SESSION['invoice'];
	}
	
	//session for custom_booking extranet
	function set_ss_data_rsv_custom($code_vendor, $new_session){
		$data_rsv = $_SESSION["data_rsv_custom"];
		$data_rsv[$code_vendor] = $new_session;
		$_SESSION["data_rsv_custom"] = $data_rsv;
	}
	
	function get_ss_data_rsv_custom($code_vendor, $ss=""){
		$session = isset($_SESSION["data_rsv_custom"])?$_SESSION["data_rsv_custom"]:false;
		if (isset($session[$code_vendor])){
			if ($ss==""){
				return $session[$code_vendor];
			}else{
				if (isset($session[$code_vendor][$ss])){
					return $session[$code_vendor][$ss];
				}else{
					return false;
				}
			}
		}else{
			return false;
		}
	}
	//--
	function set_ss_extranet_arr($arr_ss, $auto_login = false){
		$sess=$this->get_ss_extranet_arr();
		if($sess != null){
			foreach($sess as $row){
				if($row['f_id_vendor']==$this->get_ss_extranet('f_id_vendor')){
					$ada=true;
				}
			}
		}
		if(!isset($ada)){
			if ($auto_login) $arr_ss["auto_login"] = true;
			$sess[]=$arr_ss;
			$_SESSION["ss_login_extranet_arr"]= $sess;
		}
	}
	function get_ss_extranet_arr($id=null){
		//session_destroy();
		if(isset($_SESSION["ss_login_extranet_arr"])){
			return($id!=null)?$_SESSION["ss_login_extranet_arr"][$id]:$_SESSION["ss_login_extranet_arr"];
		}else{
			return NULL;
		}
	}
	function set_ss_revisement($arr_member, $auto_login = false){
		$session = array(
						"f_id_member"       =>$arr_member["f_id_member"],
						"f_receipt_number"  =>$arr_member["f_receipt_number"],
						"f_id_vendor"  =>$arr_member["f_id_vendor"]
					);
		$_SESSION["ss_revisement"][$arr_member["f_receipt_number"]] =$arr_member;
	}
	function get_ss_revisement($inv,$ss=""){
		$session = $_SESSION;
		if (isset($session["ss_revisement"][$inv])){
			if ($ss==""){
				return $session["ss_revisement"][$inv];
			}elseif (isset($session["ss_revisement"][$inv][$ss])){
				return $session["ss_revisement"][$inv][$ss];
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	function set_ss_revisement_cart($inv,$new_session, $auto_login = false){
		$data_rsv = $this->get_ss_revisement($inv,'voucher');
		$data_rsv = $new_session;
		$_SESSION["ss_revisement"][$inv]['voucher'] = $data_rsv;
	}
	function interval_checker($arg=null) {
		/*
			Interval Checker mengecek apakah interval hari yang terpilih telah memasuki masa interval atau belum
			DOKUMENTASI :
			$arg['interval'] : Interval jangka unit/waktu yang dipilih. Default 1 unit/hari
			$arg['unit'] : Pilihan yang digunakan dalam interval. Pilihan Day, Week, Month. Default: Day
			$arg['startday'] : Hari yang diplih utk mulai interval.
								Unit 'week': Pilihan 0 sampai 6. 0: Minggu, 3: Rabu, 6: Sabtu. Default 0 / Minggu
								Unit 'month': Pilihan 1 sampai tgl akhir bulan. Default tanggal 1
			$arg['date'] : hari yang dipilih. Hanya utk development. Default hari kemarin dalam format Y-m-d
			$arg['thismonth'] : memuat tanggal bulan sekarang. Berguna utk vendor yang melakukan report di akhir bulan.
								Default false / bulan kemarin. Hanya berjalan jika startday > 7
			$arg['echo'] : tampilkan text hasil perhitungan interval. Default false
		*/
		$result = false;
		$interval = isset($arg['interval'])&&intval($arg['interval'])>0 ? intval($arg['interval']) : 1;
		$unit = isset($arg['unit']) ? $arg['unit'] : 'day';
		$startday = isset($arg['startday'])&&$arg['startday']>=0 ? intval($arg['startday']) : 0 ;
		$date = isset($arg['date']) ? date('Y-m-d', strtotime($arg['date'])) : date('Y-m-d');
		$thismonth = isset($arg['thismonth'])&&$arg['thismonth']=='true'&&$startday>7 ? true : false;
		$echo = isset($arg['echo'])&&$arg['echo']=='true' ? true : false;
		switch ($unit) {
			case 'week':
				if($startday>6) $startday = 6; // report day tidak boleh lebih dari 6 (sabtu)
				$time_zero = strtotime(date('Y-m-d', strtotime(date('Y-m-d', 86400*($startday-date('w', 0))))));
				$time_from_beginning = ((strtotime($date)-$time_zero)/86400)*7;
				if($time_from_beginning%$interval==0&&date('w', strtotime($date))==$startday) $result = true;
				$date_from = date('Y-m-d', strtotime($date)-(86400*7*$interval));
				$date_to = date('Y-m-d', strtotime($date)-(86400));
				break;
			case 'month':
				if($startday<1) $startday = 1; // report day tidak boleh kurang dari tanggal 1
				if($startday>date('t', strtotime(date('Y-m-01', strtotime($date)))))
					$startday = date('t', strtotime(date('Y-m-01', strtotime($date)))); // report day tidak boleh lebih dari tanggal bulan dipilih
				$time_zero = strtotime(date('Y-01-01', 0));
				$time_from_beginning = ((date('Y', strtotime($date))-date('Y', $time_zero))*12)+date('m', strtotime($date));
				if($thismonth) {
					if(($time_from_beginning+1)%$interval==0&&date('j', strtotime($date))==$startday) $result = true;
					$date_from = date('Y-m-01', strtotime(date('Y-m-01', strtotime($date)))-(86400*28*($interval-1)));
					$date_to = date('Y-m-d', strtotime($date)-86400);
				}
				else {
					if($time_from_beginning%$interval==0&&date('j', strtotime($date))==$startday) $result = true;
					$date_from = date('Y-m-01', strtotime(date('Y-m-01', strtotime($date)))-(86400*28*$interval));
					$date_to = date('Y-m-t', strtotime(date('Y-m-01', strtotime($date)))-86400);
				}
				break;
			default:
				$time_zero = strtotime(date('Y-m-d', 0));
				$time_from_beginning = (strtotime($date)-$time_zero)/86400;
				if($time_from_beginning%$interval==0) $result = true;
				$date_from = date('Y-m-d', strtotime($date)-(86400*$interval));
				$date_to = date('Y-m-d', strtotime($date)-86400);
				break;
		}
		if($echo) {
			if($result) {
				echo "Hari Interval<br>";
				echo "Terpilih dari tanggal $date_from sampai $date_to<br>";
			} else {
				echo "Bukan Hari Interval<br>";
			}
			echo "<br>Tanggal dipilih: ".date('Y-m-d (l)', strtotime($date)).",<br>
			Start Day: $startday,<br>
			Interval setiap $interval $unit dari total $time_from_beginning $unit<br>
			Patokan tanggal mulai ".date('Y-m-d l, H:i:s', $time_zero)."<br><br>";
		}

		return array('interval'=>$result, 'from'=>$date_from, 'to'=>$date_to);
	}
	
	function send_log_payment($arr_data, $subject = "", $vendor= ""){
		
		$subject = "LOG - ".$subject;
		
		if (isset($vendor["f_company"])) $subject = $subject." - ".$vendor["f_company"];
		
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		
		ob_start();
		pre($arr_data);
		$message = ob_get_clean();
		$message = $message."<br />".$_SERVER["REQUEST_URI"];
		
		mail("suta@hybridbooking.com", $subject, $message, $headers);
	}
	
}
?>
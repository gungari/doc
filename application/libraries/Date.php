<?php  
@session_start();

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Date{
	private $date			= "";
	private $formated_date	= "";
	private $format			= "";

	function __construct(){}
	
	public function setDate($date){
		$this->date = $date;
	}
	public function getDate(){
		return $this->date;
	}
	
	public function getDateOnly($date = ""){
		if ($date == ""){ $date = $this->date; }
		return $this->format($date, "Y-m-d");
	}
	
	public function getFormatedDate(){
		return $this->formated_date;
	}
	
	//Ambil Tanggal Sekarang
	function now($format = ""){
		$_ci =& get_instance();
		
		$now = $_ci->db->query("select now() as now")->row_array();
		$now = $now["now"];
		
		$this->format($now);
		
		return $this;
	}
	
	//HARI DAN TANGGAL
	//Ambil Tanggal -> d = 1, dd = 01, ddd = Sun, dddd = Sunday
	function getDay($format = "dd", $date=""){
		
		if ($date == ""){ $date = $this->date; }
		
		if ($format == "d"){
			return $this->format($date, "j");
		}elseif ($format == "dd"){
			return $this->format($date, "d");
		}elseif ($format == "ddd"){
			return $this->format($date, "D");
		}elseif ($format == "dddd"){
			return $this->format($date, "l");
		}else{
			return $this->format($date, "d");
		}
	}
	//Ambil Nomor dari Hari -> sunday = 0
	function getNumericDay($date = ""){
		if ($date == ""){ $date = $this->date; }
		return $this->format($date, "w");
	}
	//Ambil hari ke-xx dalam tahun 1 januari = 0
	function getDayOfTheYear($date = ""){
		if ($date == ""){ $date = $this->date; }
		return $this->format($date, "z");
	}
	//Ambil Jumlah hari dalam 1 bulan
	function getNumberOfDaysInMonth($date = ""){
		if ($date == ""){ $date = $this->date; }
		return $this->format($date, "t");
	}
	
	//BULAN
	//Ambil bulan
	function getMonth($format = "mm", $date = ""){
		
		if ($date == ""){ $date = $this->date; }
		
		if ($format == "m"){
			return $this->format($date, "n");
		}elseif ($format == "mm"){
			return $this->format($date, "m");
		}elseif ($format == "mmm"){
			return $this->format($date, "M");
		}elseif ($format == "mmmm"){
			return $this->format($date, "F");
		}else{
			return $this->format($date, "m");
		}
	}
	
	//Ambil Tahun
	function getYear($format = "yyyy", $date = ""){
		
		if ($date == ""){ $date = $this->date; }
		
		if ($format == "yy"){
			return $this->format($date, "y");
		}elseif ($format == "yyyy"){
			return $this->format($date, "Y");
		}else{
			return $this->format($date, "Y");
		}
	}
	
	function format($date, $format = ""){
		$formated_date	= $date;
		
		if ($format != ""){
			$formated_date = date ($format, strtotime($date));
		}
		
		$this->date		= $date;
		$this->format 	= $format;
		$this->formated_date = $formated_date;
		
		return $this->formated_date;
	}
}

?>
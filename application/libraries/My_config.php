<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_config {
	protected $_ci;
	function __construct(){
		$this->_ci =& get_instance();
		//$this->_ci->load->database();
	}

	//Untuk Pengiriman Email 
	public $send_name				= "HybridBooking.Com";
	public $send_username			= "noreply@hybridbooking.com";
	public $email_sandbox			= "suta.alamaya@gmail.com";

	//Company Profile
	public $company_name 			= "HybridBooking.com";
	public $company_logo			= "public/images/ih-birdbigcolor_login.png";
	public $company_address_line1 	= "By Pass IB Mantra, Puri Chandra Asri A-25 ";
	public $company_address_line2 	= "Denpasar Timur - Bali - Indonesia";
	public $company_phone 			= "+62 361 468407";
	public $company_fax 			= "";
	public $company_email 			= "contact@hybridbooking.com";
	public $company_website 		= "www.hybridbooking.com";
	public $company_facebook 		= "";//"<a href='https://www.facebook.com/KutaRaya'> KUTARaya</a>";
	public $company_twitter 		= "";//"<a href='https://www.twitter.com/KUTARaya'> @KUTARaya</a>";
	public $company_instagram 		= "";//"<a href='https://www.instagram.com/KUTARaya'> KUTARaya</a>";
	
	//Vendor Extranet Type
	public $extranet_type_activities	= 1;
	public $extranet_type_acomodation	= 2;
	public $extranet_type_public_service= 3;
	public $extranet_type_culinary		= 4;
	public $extranet_type_shopping		= 5;
	public $extranet_business_and_service = 6;
	
	//Membership Type
	public $merchant_free_membership	= 1;
	public $merchant_silv_membership	= 2;
	public $merchant_gold_membership	= 3;
	public $merchant_adva_membership	= 4;
	
	//Vendor Category ID
	//public $vendor_cat_activities	= 1;
	//public $vendor_cat_acomodation	= 2;
	//public $vendor_cat_rentcar		= 3;
	//public $vendor_cat_travel		= 4;
	//public $vendor_cat_spa			= 5;

	//Market
	public $market_all 				= "ALL";
	public $market_domestic 		= "DOMESTIC";
	public $market_foreign 			= "FOREIGN";
	
	//Lain-lain
	public $var_no_pickup			= "NO-PICKUP";
	public $page_cache_time			= 0; //minutes
	
	//Invoice
	public $tax						= 10; //%
	public $invoice_due_date		= 1; //month
	
	
	public function get_SYSTEM_BASIC_SETTING($var_name, $return_all_data = false){
		return SYSTEM_BASIC_SETTING($var_name, $return_all_data);
	}
	
	public function duration_en($duration_unit){
		$durasi["MENIT"]	= "MINUTE(S)";
		$durasi["JAM"] 		= "HOUR(S)";
		$durasi["HARI"] 	= "DAY(S)";
		$durasi["MINGGU"] 	= "WEEK(S)";
		$durasi["BULAN"] 	= "MONTH(S)";
		$durasi["TAHUN"] 	= "YEAR(S)";
		$durasi["KALI"] 	= "TIME(S)";
		if (isset($durasi[strtoupper($duration_unit)])){
			return $durasi[strtoupper($duration_unit)];
		}else{
			return "";
		}
	}
	
	public function payment_gateway_text($payment_gateway = ""){
		$payment_gateway_type = array (	"PAYPAL"		=>"Paypal",
										"DOKU"			=>"Credit Card",
										"DOKU_ATM"	=>"ATM Transfer",
										"DOKU_ALFAMART"			=>"Alfa Group",
										"DOKU_MANDIRI_CLICKPAY"	=>"Mandiri ClickPay",
										"DOKU_EPAY_BRI"			=>"ePay BRI",
										"DOKU_BCA_CLICKPAY"		=>"BCA KlikPay",
										"SOF"			=>"Credit Card",
										"FINNET_ATM"	=>"ATM Transfer",
										"FINNET_CC"		=>"Credit Card",
										"HYBRID"		=>"Credit Card",
										"HYBRID_ATM"	=>"ATM Transfer",
										"HYBRID_ALFAMART"			=>"Alfa Group",
										"HYBRID_MANDIRI_CLICKPAY"	=>"Mandiri ClickPay",
										"HYBRID_EPAY_BRI"			=>"ePay BRI",
										"HYBRID_BCA_CLICKPAY"		=>"BCA KlikPay",
										"MOLPAY"		=>"Credit Card",
										"IPAY88"		=>"Credit Card",
										"IPAYMU"		=>"Credit Card",
										"ALIPAY"		=>"Alipay",
										"IONPAY"		=>"IonPay",
										"NICEPAY"		=>"Credit Card", 
										"NICEPAY_ATM"	=>"ATM Transfer",
										"REDDOTPAY-PY"	=>"Paypal",
										"REDDOTPAY-VM"	=>"Credit Card - Visa / Master",
										"REDDOTPAY-UP"	=>"Credit Card - Union Pay");
		if (isset($payment_gateway_type[strtoupper($payment_gateway)])){
			return $payment_gateway_type[strtoupper($payment_gateway)];
		}else{
			return $payment_gateway_type;
		}
	}
	
	function getLanguage($lang_code = ""){
		$lang["ID"] = "Bahasa Indonesia";
		$lang["EN"] = "English";
		$lang["FR"] = "Français";
		$lang["CN"] = "中文";
		$lang["JP"] = "日本語";
		
		if ($lang_code == ""){
			return $lang;
		}else{
			if (isset($lang[$lang_code])){
				return $lang[$lang_code];
			}else{
				return false;
			}
		}
	}
	function getTimeZone(){
		return ($this->_ci->fn->isOnline())?15:0;
	}
	function GoogleMapsAPI_URL(){
		//Jika Online
		if ($this->_ci->fn->isOnline()){
			return "https://maps.googleapis.com/maps/api/js?key=AIzaSyBiTCBHChTBPTd8Fh_H9enEzQDuOfcH_N0&sensor=true";
			//return "https://maps.googleapis.com/maps/api/js?key=AIzaSyDGWS1NqPfSl8MDZNvOtsdGSJsIMTHc8g8&sensor=true";
			//return "https://maps.googleapis.com/maps/api/js?key=AIzaSyB2e0XGUd8tXvUdjLdANmzeiMMFtecPEF8&sensor=true";
			//return "http://maps.googleapis.com/maps/api/js?key=AIzaSyB2e0XGUd8tXvUdjLdANmzeiMMFtecPEF8&sensor=true";
			//return "http://maps.googleapis.com/maps/api/js?key=AIzaSyBJfWrdpZYCAM2LNxN5SfKRfINQL4E6I9U&sensor=true";
			
			//return "http://maps.googleapis.com/maps/api/js?key=AIzaSyCx176mqhaEqDPkB4ZPRUMkR3TgnfVomdc&sensor=true";
		}

		//Jika Offline
		else{
			return "http://maps.googleapis.com/maps/api/js?key=AIzaSyBiTCBHChTBPTd8Fh_H9enEzQDuOfcH_N0&sensor=true";
			//return "http://maps.googleapis.com/maps/api/js?key=AIzaSyDGWS1NqPfSl8MDZNvOtsdGSJsIMTHc8g8&sensor=true";
			//return "http://maps.googleapis.com/maps/api/js?key=AIzaSyDUjIDcfJ-YABWYcpOOkDG4Ra0Dk1pc3N8&sensor=true";
		}

	}
	function GoogleAPI_Key(){
		return "AIzaSyBiTCBHChTBPTd8Fh_H9enEzQDuOfcH_N0";
		
		//return "AIzaSyBJfWrdpZYCAM2LNxN5SfKRfINQL4E6I9U";
		
		//Jika Online
		if ($this->_ci->fn->isOnline()){
			//return "AIzaSyB2e0XGUd8tXvUdjLdANmzeiMMFtecPEF8";
		}

		//Jika Offline
		else{
			
		}

	}
	function pagination(){
		$config['full_tag_open']	= "<ul  class='pagination pagination-sm'>";
		$config['full_tag_close']	= "</ul>";
		$config['num_tag_open']		= '<li>';
		$config['num_tag_close']	= '</li>';
		$config['cur_tag_open'] 	= '<li class="active"><a>';
		$config['cur_tag_close']	= '</a></li>';
		$config['prev_link'] 		= '<span class="glyphicon glyphicon-chevron-left"></span>';
		$config['prev_tag_open'] 	= '<li>';
		$config['prev_tag_close'] 	= '</li>';
		$config['next_tag_open'] 	= '<li>';
		$config['next_tag_close'] 	= '</li>';
		$config['next_link'] 		= '<span class="glyphicon glyphicon-chevron-right"></span>';
		$config['first_tag_open'] 	= '<li>';
		$config['first_tag_close']	= '</li>';
		$config['first_link'] 		= '<span class="glyphicon glyphicon-step-backward"></span> First';
		$config['last_tag_open'] 	= '<li>';
		$config['last_tag_close'] 	= '</li>';
		$config['last_link'] 		= 'Last <span class="glyphicon glyphicon-step-forward"></span>';
		return $config;
	}
	
	function getDefaultCurrencyVendor($currency = ""){

		$cr["CNY"] = "(CNY) China Yuan Renminbi";
		$cr["EUR"] = "(EUR) Euro";
		$cr["IDR"] = "(IDR) Indonesia Rupiah";
		$cr["JPY"] = "(JPY) Japan Yen";
		$cr["USD"] = "(USD) United States Dollar";

		
		
		if ($currency==""){
			return $cr;
		}else{
			if (isset($cr["$currency"])){
				return $cr["$currency"];
			}else{
				return false;
			}
		}
	}
	function getCurrency($currency = ""){
		$cr["IDR"] = "Indonesia Rupiah (IDR)";
		$cr["USD"] = "United States Dollar (USD)";
		$cr["AUD"] = "Australian Dollar (AUD)";
		$cr["CNY"] = "China Yuan Renminbi (CNY)";
		$cr["EUR"] = "Euro (EUR)";
		$cr["JPY"] = "Japan Yen (JPY)";
		$cr["NZD"] = "New Zealand Dollar (NZD)";
		$cr["ARS"] = "Argentina Pesos (ARS)";
		$cr["BRL"] = "Brazil Reais (BRL)";
		$cr["GBP"] = "British Pound (GBP)";
		$cr["CAD"] = "Canada Dollars (CAD)";
		$cr["XPF"] = "Comptoirs Français du Pacifique Francs (XPF)";
		$cr["DKK"] = "Denmark Kroner (DKK)";
		$cr["FJD"] = "Fiji Dollars (FJD)";
		$cr["HKD"] = "Hong Kong Dollar (HKD)";
		$cr["INR"] = "India Rupees (INR)";
		$cr["MYR"] = "Malaysia Ringgits (MYR)";
		$cr["NOK"] = "Norway Kroner (NOK)";
		$cr["PHP"] = "Philippines Pesos (PHP)";
		$cr["RUB"] = "Russia Rubles (RUB)";
		//$cr["WST"] = "Samoa Tala (WST)";
		$cr["SGD"] = "Singapore Dollar (SGD)";
		$cr["ZAR"] = "South Africa Rand (ZAR)";
		$cr["KRW"] = "South Korea Won (KRW)";
		//$cr["SEK"] = "Sweden Kronor (SEK)";
		$cr["TWD"] = "Taiwan New Dollars (TWD)";
		$cr["THB"] = "Thailand Baht (THB)";
		$cr["TOP"] = "Tonga Pa'anga (TOP)";
		$cr["VUV"] = "Vanuatu Vatu (VUV)";
		$cr["VND"] = "Vietnam Dong (VND)";
		//$cr["ZMW"] = "Zambia Kwacha (ZMW)";
		
		if ($currency==""){
			return $cr;
		}else{
			if (isset($cr["$currency"])){
				return $cr["$currency"];
			}else{
				return false;
			}
		}
	}
	function getMarketDescription($market){
		$arr_market["FOREIGN"]	= "International Rate";
		$arr_market["DOMESTIC"]	= "KTP & KITAS Holder";
		$arr_market["ALL"]		= "ALL Market";
		
		if (isset($arr_market[$market])){
			return $arr_market[$market];
		}else{
			return "";
		}
	}
	
	function getPromotionalCodeType($type = '', $extranet_type){
		$discount_type["fixed_cart"] 		= "Cart Discount";
		$discount_type["percent_cart"] 		= "Cart % Discount";
		$discount_type["fixed_product"] 	= "Product Discount";
		$discount_type["percent_product"] 	= "Product % Discount";
		//$discount_type["auto_fixed_product"] 	= "Product Auto Discount";
		//$discount_type["auto_percent_product"] 	= "Product % Auto Discount";
		if ($extranet_type=="acc"){
			$discount_type["fixed_product"] 	= "Room Discount";
			$discount_type["percent_product"] 	= "Room % Discount";
		} elseif($extranet_type=="trans") {
			$discount_type["fixed_product"] 	= "Trip Discount";
			$discount_type["percent_product"] 	= "Trip % Discount";	
		} elseif($extranet_type=="transv2"){
			$discount_type["fixed_product"] 	= "Schedule Discount";
			$discount_type["percent_product"] 	= "Schedule % Discount";	
		}
		$discount_type["value_add"] 		= "Value Added";
		if ($type == ""){
			return $discount_type;
		}elseif (isset($discount_type[$type])){
			return $discount_type[$type];
		}else{
			return "";
		}
	}

	function getVoucherTermAndConditionDefautl($lang = "EN", $vendor){
		$tac["ID"][] = "Voucher tidak berlaku jika anda datang diluar dari tanggal yang tercantum di atas.";
		$tac["ID"][] = $vendor["f_company"]." berhak menagih biaya tambahan di luar dari produk dan atau servis yang tercantum pada voucher.";
		$tac["ID"][] = "Voucher tidak dapat ditukarkan dengan layanan lain dan dipindahtangankan tanpa kesepakatan dari ".$vendor["f_company"].".";
		$tac["ID"][] = "Voucher tidak dapat dibatalkan dan diuangkan";
		$tac["ID"][] = "Jika ada pertanyaan mohon menghubungi ".$vendor["f_company"]." ".$vendor["f_email"]." atau telepon ".$vendor["f_phone"].".";
		
		$tac["EN"][] = "Voucher is valid for the above mentioned date only.";
		$tac["EN"][] = $vendor["f_company"]." is entitled to ask additional charge for products and or services that are excluded from products and or services that have been mentioned on voucher.";
		$tac["EN"][] = "Voucher can not be exchanged with other products and or services without approval from ".$vendor["f_company"].".";
		$tac["EN"][] = "Voucher can not be canceled and refunded.";
		$tac["EN"][] = "If you have any questions please contact ".$vendor["f_company"]." ".$vendor["f_email"]." or call ".$vendor["f_phone"].".";
		
		$lang = "EN";
		
		if (isset($tac["$lang"])){
			return $tac["$lang"];
		}else{
			return false;
		}
	}
	
	function getVendorTermAndConditionBooking($vendor, $lang = "EN"){
		
		$tac["EN"]= "<p>The booking can be done through instant booking or enquiry booking depending on $vendor[f_company] and vendor's agreement and vendor's service availability.</p>".
					"<p>The product's pricing are in IDR (Indonesian Rupiah) and other currencies but the final settlement will be in IDR using exchange rates provided by&nbsp;<em>yahoo.com.</em></p>".
					"<p>For every successful booking transaction user / buyer will get voucher and receipt of payment through email. The voucher is a proof of valid transaction. The email could go to your spam box, please check your spam box. If the email goes to your spam box, please mark it as As Not Spam' for your future benefit. Should you not receive the voucher, please contact us at $vendor[f_email].</p>".
					"<p>User / buyer should print the voucher or save the voucher in their gadget (e-voucher) as user must show it to $vendor[f_company] staff. User should contact us at $vendor[f_email], if staff refuse to deliver their service(s) so we can solve it immediately.</p>".
					"<p><strong>Payment security</strong></p>".
					"<p>All payments using credit cards (Visa and Master Card) and Debit cards processed via Payment Gateway as a payment processor secured by International Standard Security Scanning encrypted with 128 bit SSL.".
					"<p><strong>Usage of vouchers policy</strong></p>".
					"<p>The usage of vouchers should be according to $vendor[f_company] terms and conditions and listed vendor's terms and conditions. Listed vendors are the sellers that provide the products and / or services.</p>";

		$tac["FR"]= "<p>La réservation peut se faire grâce à la réservation instantanée ou par la demande de réservation sur le site du $vendor[f_company], selon la disponibilité du service.</p>".
					"<p>Les prix des produits sont en IDR (en rupiah) et d'autres monnaies, mais le règlement final se fera en IDR (en rupiah) en utilisant les taux de change fournis par <em>yahoo.com</em>.</p>".
					"<p>Pour toute réservation et transaction, l'utilisateur / acheteur  devra obtenir un bon/ticket avec la réception du paiement par e-mail. Le bon/ticket est une preuve de la transaction. Le courriel pourrait aller à votre boîte de spam. Veuillez s'il vous plaît vérifier votre boîte de spam. Si l'e-mail va à votre boîte de spam, veuillez s'il vous plaît marquer comme « non spam » pour pouvoir l’utiliser par la suite. Si vous ne recevez pas le bon/ticket, veuillez s'il vous plaît nous contacter à $vendor[f_email].</p>".
					"<p>L’utilisateur / acheteur doit imprimer le bon/ticket ou enregistrer le bon/ticket dans leur smartphone ou tablette (e-bon/ticket), et il devra le montrer au personnel de $vendor[f_company]. Les utilisateurs doivent nous contacter à $vendor[f_email], dans le cas où le personnel refuse de délivrer leur service, afin que nous puissions le résoudre immédiatement.</p>".
					"<p><strong>Sécurité des paiements :</strong> </p>".
					"<p>Tous les paiements par carte de crédit (Visa et Master Card) et les cartes de débit traitées via la passerelle de paiement comme un processeur de paiement, sont sécurisés par International Scanning de sécurité standard chiffrées avec SSL 128 bits.".
					"<p><strong>Utilisation des bons/tickets : </strong></p>".
					"<p>Les bons/tickets devront être utilisés selon les termes de $vendor[f_company] et valables pour les fournisseurs répertoriés. Les fournisseurs et prestataires listés sont ceux qui fournissent les produits et / ou services.</p>";
		
		if (isset($tac["$lang"])){
			return $tac["$lang"];
		}else{
			return $tac["EN"];
		}
	}

	public function DOKU_PaymentChannel_CODE($p_channel = ""){
		$PaymentChannel["01"] = "C";
		$PaymentChannel["02"] = "MANDIRICP";
		$PaymentChannel["05"] = "A";
		$PaymentChannel["07"] = "A";
		$PaymentChannel["06"] = "EPAYBRI";
		$PaymentChannel["14"] = "ALFA";
		$PaymentChannel["15"] = "C";
		$PaymentChannel["24"] = "BCAKLIKPAY";
		
		if ($p_channel == ""){
			return $PaymentChannel;
		}elseif(isset($PaymentChannel[$p_channel])){
			return $PaymentChannel[$p_channel];
		}else{
			return "";
		}
	}

	function getPaymentType($type = ""){
		$payment_type["C"] = "Credit Card";
		$payment_type["A"] = "ATM";
		$payment_type["P"] = "Paypal";
		$payment_type["D"] = "salDOKU";
		$payment_type["T"] = "Cash / Direct Transfer";
		$payment_type["ENQUIRY"] 	= "Enquiry Booking";
		$payment_type["ALFA"] 		= "Alfa Group";
		$payment_type["MANDIRICP"] 	= "Mandiri ClikPay";
		$payment_type["EPAYBRI"] 	= "EPay BRI";
		$payment_type["BCAKLIKPAY"]	= "BCA KlikPay";
		$payment_type["AL"] = "Alipay";
		$payment_type["NOPAYMENT"]	= "Pay On Arrival";
		
		if ($type == ""){
			return $payment_type;
		}elseif(isset($payment_type[$type])){
			return $payment_type[$type];
		}else{
			return "";
		}
	}
	
	function getTransactionStatus($status = ""){
		$trans_status["T"] = "<span class='label label-success'>Paid</span>";
		$trans_status["F"] = "<span class='label label-warning'>Pending</span>";
		$trans_status["C"] = "<span class='label label-danger'>Failed</span>";
		$trans_status["CL"] = "<span class='label label-danger'>Canceled</span>";

		if ($status == ""){
			return $trans_status;
		}elseif(isset($trans_status[$status])){
			return $trans_status[$status];
		}else{
			return "";
		}
	}
	
	function getTransactionStatusAccommodation($status = ""){
		$trans_status["NEW"] 		= "<span class='label label-warning'>Pending</span>";
		$trans_status["PAID"] 		= "<span class='label label-success'>Paid</span>";
		$trans_status["PENDING"] 	= "<span class='label label-warning'>Pending</span>";
		$trans_status["EXPIRED"] 	= "<span class='label label-danger'>Failed</span>";

		if ($status == ""){
			return $trans_status;
		}elseif(isset($trans_status[$status])){
			return $trans_status[$status];
		}else{
			return "";
		}
	}
	function getLanguage_icon($lang_code = ""){
		$lang["ID"] = "id.png";
		$lang["EN"] = "en.png";
		$lang["FR"] = "fr.png";
		$lang["CN"] = "cn.png";
		$lang["JP"] = "jp.png";
		$lang["PH"] = "ph.png";
		$lang["MY"] = "my.png";
		
		if ($lang_code == ""){
			return $lang;
		}else{
			if (isset($lang[$lang_code])){
				return $lang[$lang_code];
			}else{
				return false;
			}
		}
	}
	function getLanguageSpoken($lang_code = ""){
		$lang["EN"] = "English";
		$lang["FR"] = "Français";
		$lang["JP"] = "日本語";
		$lang["CN"] = "中文";
		$lang["ID"] = "Bahasa Indonesia";
		$lang["PH"] = "Tagalog";
		$lang["MY"] = "Malaysia";
		
		if ($lang_code == ""){
			return $lang;
		}else{
			if (isset($lang[$lang_code])){
				return $lang[$lang_code];
			}else{
				return false;
			}
		}
	}
	/*
	function getOrderStatus(){
		$trans_status["T"] = "<span class='label label-success'>Paid</span>";
		$trans_status["F"] = "<span class='label label-warning'>Pending</span>";
		$trans_status["C"] = "<span class='label label-danger'>Cancel</span>";
		
		return $trans_status;
	}
	*/
	
	
	
	/*
	//Currency Setting
	public $foreign_rate 			= "USD";
	public $local_rate				= "IDR";
	//public $htl_currency			= "IDR";
	
	//Market
	public $market_all 				= "ALL";
	public $market_domestic 		= "DOMESTIC";
	public $market_foreign 			= "FOREIGN";
	
	//Untuk ATM Transfer
	public $vmerchant_195_id		= "INON067";
	public $vmerchant_195_password	= "INON#1612";
	public $vmerchant_195_timeout	= "1440";  		//maksimal 24x60menit = 24 Jam
	//public $vmerchant_195_URL		= "http://demos.finnet-indonesia.com/195/response-insert.php"; //Demo
	public $vmerchant_195_URL		= "https://billhosting.finnet-indonesia.com/prepaidsystem/195/response-insert.php"; //Online
	
	//Untuk Kredit Card
	//public $vmerchant_CC_id		= "KTRY001";			//ID yang baru
	//public $vmerchant_CC_password	= "KTRY001$0924";		//PASS yang baru
	//public $vmerchant_CC_timeout	= "5"; 					//Maksimal 5 Menit
	//public $vmerchant_CC_URL		= "https://finpaydev.finnet-indonesia.com/transaction/TransactionMain.do"; //URL yang Baru //Development
	
	public $vmerchant_CC_id			= "0633";
	public $vmerchant_CC_password	= "INON#1612";
	public $vmerchant_CC_timeout	= "5"; 			//Maksimal 5 Menit
	//public $vmerchant_CC_URL		= "https://kuta.finnet-indonesia.com/telkom/PaymentUser.action"; //Demo
	public $vmerchant_CC_URL		= "https://ipg.finnet-indonesia.com/telkom/PaymentUser.action"; //Online
	
	//Untuk Cash Flow
	public $id_pos_account_kutaraya_receive_payment	= "167";
	public $id_pos_account_kutaraya_send_payment	= "164";
	
	public $id_cash_book_kutaraya_receive_payment	= "2";
	public $id_cash_book_kutaraya_send_payment		= "2";
	
	//Akun Untuk Pengiriman Email
	public $send_name				= "KUTARaya.com";
	public $send_username			= "noreply@kutaraya.com";
	public $send_password			= "kutaraya148";
	
	//Lain-lain
	public $var_no_pickup			= "NO-PICKUP";*/

	
}
?>
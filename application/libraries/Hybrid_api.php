<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hybrid_api{
	protected $_ci;
	
	public $merchant_code 	= "";
	public $api_key			= "";
	
	//public $api_URL		= "http://api.hybridbooking.com/api/";
	public $api_URL			= "http://localhost:81/IH/REST_API/";
	//public $api_URL			= "192.168.244.161:81/IH/REST_API/";
	
	public $curl_text		= "";
	
	public function __construct(){
		$this->_ci =& get_instance();
		
		$ss_login = $this->_ci->fn->get_ss_CRS();

		if ($ss_login){
			$this->merchant_code 	= $ss_login["credential"]["merchant_code"];
			$this->api_key			= $ss_login["credential"]["merchant_key"];
		}
	}
	
	public function getData($path, $data = array()){
		
		$data_m["merchant_code"] 	= $this->merchant_code;
		$data_m["merchant_key"] 	= $this->api_key;
		
		$url = $this->api_URL.$path."?".http_build_query($data_m);
		
		$json_data = $this->curl_post($url, $data);

		$this->curl_text = $json_data;
		$arr_data = json_decode($json_data, true);
		if (!$arr_data) $arr_data = $json_data;
		
		return $arr_data;
		
	}
	
	private function curl_post($url, $postdata, $timeout=0){
		
		$sentdata = count($postdata)>0?http_build_query($postdata):"";
		
		$ssl_active = false;
		if(strtolower(substr($url,0,5))=="https"){
			$ssl_active = true;
		}
		$channel = curl_init($url);

		curl_setopt ($channel, CURLOPT_HEADER, false);
		curl_setopt ($channel, CURLINFO_HEADER_OUT, false);
		curl_setopt	($channel, CURLOPT_POST, 1);
		curl_setopt	($channel, CURLOPT_POSTFIELDS, $sentdata);
		//curl_setopt	($channel, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt ($channel, CURLOPT_ENCODING, "");
		curl_setopt ($channel, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($channel, CURLOPT_AUTOREFERER, 1);
		curl_setopt ($channel, CURLOPT_URL, $url);
		curl_setopt ($channel, CURLOPT_VERBOSE, true);
		if($ssl_active==true){
			//curl_setopt ($channel, CURLOPT_PORT , 443);
			//curl_setopt ($channel, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt ($channel, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt ($channel, CURLOPT_SSLVERSION, 3); 
			curl_setopt ($channel, CURLOPT_CAINFO, dirname(__FILE__) . DIRECTORY_SEPARATOR . 'bes_cer.cer');
			//curl_setopt ($channel, CURLOPT_SSL_CIPHER_LIST, 'SSLv3');
			//curl_setopt($channel, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		}
		if($timeout>0){
			curl_setopt ($channel, CURLOPT_CONNECTTIMEOUT, $timeout );
			curl_setopt ($channel, CURLOPT_TIMEOUT, $timeout );
		}
		curl_setopt ($channel, CURLOPT_MAXREDIRS, 10);
		curl_setopt ($channel, CURLOPT_VERBOSE, 1);
		$output_data = curl_exec($channel);
		
		//echo $output_data;
		//pre(curl_error($channel));
		//pre(curl_getinfo($channel));
		
		if(!$output_data){
			//var_dump(curl_getinfo($channel));
			//echo curl_error($channel).'<br />';
			//$output["status"] 		= "ERROR";
			$error	= "CURL error : ".curl_error($channel);
			$this->curl_text = curl_error($channel);
			curl_close 	($channel);
			return FALSE;
		}else{
			curl_close 	($channel);
			return $output_data;
		}
	}
}
?>
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template {
	protected $_ci;
	
	function __construct(){
		$this->_ci =& get_instance();
	}
	
	public function checkLogin(){
		
		$checkLogin = $this->_ci->fn->get_ss_CRS();
		
		if ($checkLogin){
			return $checkLogin;
		}else{
			redirect("login");
		}
	}
	
	public function getMenuLink($credential){
		$this->_ci->load->library("hybrid_api");
		
		$data = $credential;

		$this->_ci->hybrid_api->merchant_code = $credential["merchant_code"];
		$this->_ci->hybrid_api->api_key = $credential["merchant_key"];
		
		$hybrid_api = $this->_ci->hybrid_api->getData("access_role/menu_link", $data);
		
		if ($hybrid_api["status"] == "SUCCESS"){
			return $hybrid_api["menu_link"];
		}else{
			return false;
		}
	}

	public function home($view, $data = NULL, $is_home = false){
		
		$data_checkLogin = $this->checkLogin();
		
		if ($data_checkLogin){
			
			$data["_menu_link"] = $this->getMenuLink($data_checkLogin["credential"]);
			
			if ($data == NULL) $data = $data_checkLogin;
			else $data = array_merge($data, $data_checkLogin);
			
			$data["_content"] = $this->_ci->load->view($view, $data, true);
			if ($is_home) {
				$this->_ci->load->view("template/crs/_ng_inside_.php", $data);
			}else{
				$this->_ci->load->view("template/crs/_ng_index.php", $data);
			}
		}
	}

	public function print_page($view = "", $data = NULL, $is_home = false){

		$data_checkLogin = $this->checkLogin();
		
		if ($data_checkLogin){
			
			if ($data == NULL) $data = $data_checkLogin;
			else $data = array_merge($data, $data_checkLogin);
			
			if ($view != ""){
				$data["_content"] = $this->_ci->load->view($view, $data, true);
			}

			$data["print_template"] = TRUE;
            $this->_ci->load->view("template/crs/_print.php", $data);
			
		}
		
	}

	public function resend_email($view = "", $data = NULL, $is_home = false){
		
		$data_checkLogin = $this->checkLogin();
		
		if ($data_checkLogin){
			
			if ($data == NULL) $data = $data_checkLogin;
			else $data = array_merge($data, $data_checkLogin);
			
			if ($view != ""){
				$data["_content"] = $this->_ci->load->view($view, $data, true);
			}
			$data["email_template"] = TRUE;
            $this->_ci->load->view("template/crs/_email.php", $data);
			
		}
		
	}	

	
	// untuk login agent
	public function checkLogin_agent(){
		
		$checkLogin = $this->_ci->fn->get_ss_CRS_agent();
		
		if ($checkLogin){
			return $checkLogin;
		}else{
			redirect("login_agent");
		}
	}

	public function getMenuLink_agent($credential){
		$this->_ci->load->library("hybrid_api");
		
		$data = $credential;

		$this->_ci->hybrid_api->merchant_code = $credential["merchant_code"];
		$this->_ci->hybrid_api->api_key = $credential["merchant_key"];
		

		$hybrid_api = $this->_ci->hybrid_api->getData("access_role/menu_link_agent", $data);
		
		if ($hybrid_api["status"] == "SUCCESS"){
			return $hybrid_api["menu_link"];
		}else{
			return false;
		}
	}

	public function home_agent($view, $data = NULL, $is_home = false){
		
		$data_checkLogin = $this->checkLogin_agent();
		
		if ($data_checkLogin){
			$data_checkLogin["credential"]['merchant_code'] = $data_checkLogin['vendor']['code'];
			$data_checkLogin['credential']['merchant_key'] = "ag-".md5($data_checkLogin['agent']['agent_code']);
			// $data["_menu_link"] = $this->getMenuLink($data_checkLogin["credential"]);
			$data["_menu_link"] = $this->getMenuLink_Agent($data_checkLogin["credential"]);
			//pre_exit($data);
			if ($data == NULL) $data = $data_checkLogin;
			else $data = array_merge($data, $data_checkLogin);
			
			$data["_content"] = $this->_ci->load->view($view, $data, true);
			//pre_exit($data);

			if ($is_home) {
				$this->_ci->load->view("template/crs/_ng_home_agent.php", $data);
			}else{
				$this->_ci->load->view("template/crs/_ng_index_agent.php", $data);
			}	
		}
		
	}	

	public function agent_layout($code_vendor, $view, $data = NULL){
			$this->_ci->load->library("hybrid_api");

			$this->_ci->hybrid_api->merchant_code = @$data["code"];
			$this->_ci->hybrid_api->api_key = @$data["key"];
			
			$hybrid_api = $this->_ci->hybrid_api->getData("vendor", $data);

			$data["_content"] = $this->_ci->load->view($view, $data, true);


			
		            $is_standard_template = true;
		            $not_standard_template_exist = false;

		            if (@file_exists(APPPATH."views/template/crs/layout_agent_inside/$code_vendor/_index.php")){
		                $not_standard_template_exist = true;
		            }

		            if (isset($hybrid_api["setting"]["active_template_crs"]) && $hybrid_api["setting"]["active_template_crs"] != ""){
		                $is_standard_template = false;
		            }

		            if ($not_standard_template_exist && !$is_standard_template && $data["code"] == $hybrid_api["setting"]["active_template_crs"]){
		                $this->_ci->load->view("template/crs/layout_agent_inside/$code_vendor/_index.php", $data);
		            }else{
		                $this->_ci->load->view("template/crs/_login_agent.php", $data);
		            }

	}

	public function inside_agent($view, $data = NULL, $is_home = false){
		
		$data_checkLogin = $this->checkLogin_agent();

		if ($data_checkLogin){
			$data_checkLogin["credential"]['merchant_code'] = $data_checkLogin['vendor']['code'];
			$data_checkLogin['credential']['merchant_key'] = "ag-".md5($data_checkLogin['agent']['agent_code']);
			
			$data["_menu_link"] = $this->getMenuLink_Agent($data_checkLogin["credential"]);
			
			$hybrid_api = $this->_ci->hybrid_api->getData("vendor", $data_checkLogin["credential"]);
			//pre_exit($hybrid_api);

			if ($data == NULL) $data = $data_checkLogin;
			else $data = array_merge($data, $data_checkLogin);
			
			$data["_content"] = $this->_ci->load->view($view, $data, true);

			$code_vendor = $data_checkLogin['vendor']['code'];
			
			// $this->_ci->load->view("template/crs/_ng_index_agent.php", $data);

			//DEFAULT
			// $this->_ci->load->view("template/crs/v_inside_agent_default.php", $data);
			
			//SESUAI TEMPLATE VENDOR
			// $this->_ci->load->view("template/crs/layout_agent_inside/$code_vendor/_index.php", $data);

			$is_standard_template = true;
            $not_standard_template_exist = false;

            if (@file_exists(APPPATH."views/template/crs/layout_agent_inside/$code_vendor/_index.php")){
                $not_standard_template_exist = true;
            }

            if (isset($hybrid_api["setting"]["active_template_crs"]) && $hybrid_api["setting"]["active_template_crs"] != ""){
                $is_standard_template = false;
            }

            if ($not_standard_template_exist && !$is_standard_template && ($data_checkLogin['vendor']['code'] == $hybrid_api["setting"]["active_template_crs"]) ){
                $this->_ci->load->view("template/crs/layout_agent_inside/$code_vendor/_index.php", $data);
            }else{
                $this->_ci->load->view("template/crs/v_inside_agent_default.php", $data);
            }

		}
		
	}

}
?>
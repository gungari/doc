<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fnlib {
	protected $_ci;
	function __construct(){
		$this->_ci =& get_instance();
		//$this->_ci->load->database();
		//$this->_ci->load->library('session');
	}
	function isMobile(){
		if (isset($_GET["mobile"])){ return true;}
		//return true;
		require_once (dirname(__FILE__)."/mobile_Detect.php");
		$detect = new Mobile_Detect();
		//pre($detect);
		//return ($this->_ci->agent->is_mobile());
		return ($this->_ci->agent->is_mobile() || $detect->isTablet());
	}
	function isOnline(){
		return !($_SERVER["HTTP_HOST"]=="localhost:81" || $_SERVER["HTTP_HOST"]=="ri1:81");
	}
	function isSandBox(){
		//return true;
		//$arr_production = array("be.indonesiaholiday.com", "bes.hybridbooking.com", "api.hybridbooking.com", "report.hybridbooking.com", "payment.hybridbooking.com");
		
		//if (in_array($_SERVER["SERVER_NAME"],$arr_production)){
		
		if ($_SERVER["HTTP_HOST"]=="localhost:81" || $_SERVER["HTTP_HOST"]=="localhost"){
			return true;
		}else{
			return false;
		}
	}
	
	
	function now($format = ""){
		$res = $this->_ci->db->query("select now() as sekarang")->row_array();
		if ($format==""){
			return $res["sekarang"];
		}else{
			return date($format, strtotime($res["sekarang"]));
		}
	}
	function conver_server_datetime($server_datetime, $vendor_time_zone){
		if (isset($_SESSION["SERVER_TIME_ZONE"])){
			$server_time_zone = $_SESSION["SERVER_TIME_ZONE"];
		}else{
			$server_time_zone = SYSTEM_BASIC_SETTING("SERVER_TIME_ZONE");
			$_SESSION["SERVER_TIME_ZONE"] = $server_time_zone;
		}
		$server_time_zone;
		$vendor_time_zone = $vendor_time_zone;
		$diff_time_zone = $vendor_time_zone - $server_time_zone; 
		
		$server_to_local_time = new DateTime($server_datetime);
		$dateinterval = new DateInterval("PT".abs($diff_time_zone)."H");
		if($diff_time_zone<0) {
			$dateinterval->invert = 1;
		}
		$server_to_local_time->add($dateinterval);
		return $server_to_local_time->format("Y-m-d H:i:s");
	}
	function convert_server_datetime($server_datetime, $vendor_time_zone){
		return $this->conver_server_datetime($server_datetime, $vendor_time_zone);
	}
	function revert_server_datetime($target_datetime, $current_time_zone=false){
		if (isset($_SESSION["SERVER_TIME_ZONE"])){
			$server_time_zone = $_SESSION["SERVER_TIME_ZONE"];
		}else{
			$server_time_zone = SYSTEM_BASIC_SETTING("SERVER_TIME_ZONE");
			$_SESSION["SERVER_TIME_ZONE"] = $server_time_zone;
		}
		if($current_time_zone===false) {
			$current_time_zone = $this->_ci->m_vendor->getVendorBySS()['f_time_zone'];
		}
		$now = date("Y-m-d H:i:s");
		//$server_time_zone = timezone_offset_get(timezone_open(date_default_timezone_get()), date_create($now))/3600;
		$timezone_interval = $server_time_zone - $current_time_zone;
		$time_offset = $server_time_zone + $timezone_interval;
		return $this->convert_server_datetime($target_datetime, $time_offset);
	}
	
	
	private function _currency_convert($from, $to){
		$query 	= 'select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20("'.$from.$to.'")&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=';
		$url 	= "http://query.yahooapis.com/v1/public/yql?q=".$query;
		//$url 	= "http://106.10.137.174/v1/public/yql?q=".$query;
		
		$rawdata = @file_get_contents($url);
		$rawdata = json_decode($rawdata);
		
		return $rawdata;
	}
	function currency_convert($from, $to, $amount="", $vendor_currency_convert_setting = "", $json=''){
		//$arr_local_convert = array ("USD","IDR");
		/*if ($vendor_currency_convert_setting != "" && isset($vendor_currency_convert_setting["f_bookkeeping_rates_from_yahoo"]) 
			&& $vendor_currency_convert_setting["f_bookkeeping_rates_from_yahoo"] == "0"
			&& in_array($from, $arr_local_convert) && in_array($to, $arr_local_convert)){
			
			$data["from"] 	= strtoupper($from);
			$data["to"] 	= strtoupper($to);
			$data["amount"]	= $amount;
			
			if ($from == $to){
				$data["rate"] = 1;
			}elseif ($from == "USD" && $to == "IDR"){
				$data["rate"] = $vendor_currency_convert_setting["f_bookkeeping_rates_usd_to_idr"];
			}else{
				$data["rate"] = 1/$vendor_currency_convert_setting["f_bookkeeping_rates_usd_to_idr"];
			}
			
			$data["rate_convert"] = $amount * $data["rate"];
			return $data;
		}*/
		
		if (isset($vendor_currency_convert_setting["bookkeeping_rate"]) && $vendor_currency_convert_setting["bookkeeping_rate"]
				&& isset($vendor_currency_convert_setting["bookkeeping_rate"][strtoupper($from)][strtoupper($to)])){
			
			$data["from"] 	= strtoupper($from);
			$data["to"] 	= strtoupper($to);
			$data["amount"]	= $amount;
			$data["rate"]	= $vendor_currency_convert_setting["bookkeeping_rate"][strtoupper($from)][strtoupper($to)];
			$data["rate_convert"] = $amount * $data["rate"];
			return $data;
			
		}else{
			//echo gethostbyname("query.yahooapis.com");
			$code_vendor = $this->_ci->uri->segment("4");
			$data["from"] 	= strtoupper($from);
			$data["to"] 	= strtoupper($to);
			$data["amount"]	= $amount;
			//session_destroy();
	
			if (isset($_SESSION["currency_convert"][$data["from"]][$data["to"]])){
				$data["rate"] = $_SESSION["currency_convert"][$data["from"]][$data["to"]]["rate"];
	
			}elseif ($from == $to){
	
				$data["rate"] = 1;
				$data["rate_convert"] = $amount;
	
			}elseif(($from == "USD" || $from == "IDR") && ($to == "IDR" || $to == "USD") &&
				isset($_SESSION["vendor"]["$code_vendor"]["AdvancedSetting"]["f_bookkeeping_rates_from_yahoo"]) &&
				$_SESSION["vendor"]["$code_vendor"]["AdvancedSetting"]["f_bookkeeping_rates_from_yahoo"] == "0"){
				
				$bookkeeping_rate  = $_SESSION["vendor"]["$code_vendor"]["AdvancedSetting"]["f_bookkeeping_rates_usd_to_idr"];
				if ($from == "USD" && $to == "IDR"){
					$data["rate"] = $bookkeeping_rate;
					$data["rate_convert"] =$amount * $data["rate"];
				}elseif ($from == "IDR" && $to == "USD"){
					$data["rate"] = 1/$bookkeeping_rate;
					$data["rate_convert"] = $amount * $data["rate"];
				}
			}else{
				{
					$rawdata = $this->_currency_convert($from, $to);
					if (isset($rawdata->query->results->rate->Rate)){
						$data["rate"]	= $rawdata->query->results->rate->Rate;
						if ($data["rate"]!=0){
							if ($data["rate"] < 1){
								$rawdata = $this->_currency_convert($to, $from);
								if (isset($rawdata->query->results->rate->Rate)){
									$data["rate"]	= $rawdata->query->results->rate->Rate;
									$data["rate"]	= 1/$data["rate"];
									if ($amount!="") $data["rate_convert"] = (float)$amount * (float)$data["rate"];
								}else{
									$data = false;
									//return false;
								}
							}else{
								if ($amount!="") $data["rate_convert"] = (float)$amount * (float)$data["rate"];
							}
							if (strtoupper($json)==strtoupper("json")){
								return json_encode($data);
							}else{
								//return data;
							}
						}else{
							$data = false;
							//return false;
						}
					}else{
						$data = false;
						//return false;
					}
					
					if ($data == false){
						$where = "(f_currency_from = '$from' and f_currency_to = '$to') or (f_currency_from = '$to' and f_currency_to = '$from')";
						$convert = $this->_ci->db->where("$where")->get("tb_s_hb_bookkeeping_rates");
						if ($convert->num_rows() > 0){
							$convert = $convert->row_array();
							
							if (strtoupper($from) == strtoupper($convert["f_currency_from"])){
								$rate = $convert["f_amount_to"]/$convert["f_amount_from"];
							}else{
								$rate = $convert["f_amount_from"]/$convert["f_amount_to"];
							}
							
							$data["from"] 	= strtoupper($from);
							$data["to"] 	= strtoupper($to);
							$data["amount"]	= $amount;
							$data["rate"]	= $rate;
							$data["rate_convert"] = $amount * $data["rate"];
						}
					}
				}
			}
			
			if ($data){
				$data["rate_convert"] = $amount * $data["rate"];
				$_SESSION["currency_convert"][$from][$to] = $data;
				return $data;
			}else{
				$data["from"] 	= strtoupper($from);
				$data["to"] 	= strtoupper($from);
				$data["amount"]	= $amount;
				$data["rate"]	= 1;
				$data["rate_convert"] = $amount * $data["rate"];
				return $data;
			}
		}
	}
	
	function number_format($number, $currency){
		if ($currency == "IDR"){
			$def_dec 	= 0;
			$decimal_sp = ",";
			$ribuan_sp  = ".";
		}else{
			$def_dec 	= 2;
			$decimal_sp = ".";
			$ribuan_sp  = ",";
		}
				
		return number_format($number, $def_dec, $decimal_sp, $ribuan_sp);
	}
	function resize_image($q, $w, $h, $src){
		//return base_url("$src");
		//return base_url("image.php?q=$q&w=$w&h=$h&src=$src");
		
		$URL_BES = "http://localhost:81/IH/extranet/";
		$img_url = trim($URL_BES,"/")."/image/$q/$w/$h/$src";
		return $img_url;
		
		//return base_url("image/$q/$w/$h/$src");
	}
	function date_time($date_time, $format = "Y-m-d"){
		$beda_waktu = $this->_ci->my_config->getTimeZone();
		$waktu = strtotime($date_time) + ($beda_waktu * 3600); //Jam
		return date($format, $waktu);
	}
	function isWeekend($date) {
		return (date('N', strtotime($date)) >= 6);
	}
	
	function randomString($length){
		$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randstring = '';
		for ($i = 0; $i < $length; $i++) {
			$randstring.= $characters[rand(0, (strlen($characters))-1)];
		}
		return $randstring;
	}
	
	function randomNumeric($length){
		$characters = '0123456789';
		$randstring = '';
		for ($i = 0; $i < $length; $i++) {
			$randstring.= $characters[rand(0, (strlen($characters))-1)];
		}
		return $randstring;
	}
	
	function randomAlpaNumeric($length){
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randstring = '';
		for ($i = 0; $i < $length; $i++) {
			$randstring.= $characters[rand(0, (strlen($characters))-1)];
		}
		return $randstring;
	}
	
	public function youtube_api($url){
		$arr_url 	= parse_url($url);
		parse_str(@$arr_url["query"], $url_query);
		//pre($url_query);
		$dt["id"]  		= @$url_query["v"];
		$dt["key"] 		= $this->_ci->my_config->GoogleAPI_Key();
		$dt["part"]		= "snippet,contentDetails,statistics,status";

		$params_string = http_build_query($dt);
		
		// Request URL
		$url = "https://www.googleapis.com/youtube/v3/videos?$params_string";
		
		//pre(file_get_contents($url));
		
		// Make our API request
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_SSLVERSION, 3); 
		$return = curl_exec($curl); 
		//echo curl_error($curl);
		curl_close($curl);
			
		$return = json_decode($return);
		//pre($return);
		if (count(@$return->items) > 0){
			return $return->items[0];
		}else{
			return false;
		}
	}
	
	
	public function cache($time){
		if (isset($_GET["clear_all_cache"])){
			$this->_ci->output->clear_all_cache();
		}elseif (isset($_GET["clear_cache"])){
			$this->_ci->output->clear_path_cache(uri_string());
		}else{
			$this->_ci->output->cache($time);
		}
	}
	
	function aasort (&$array, $key) {
		$sorter=array();
		$ret=array();
		reset($array);
		foreach ($array as $ii => $va) {
			$sorter[$ii]=$va[$key];
		}
		asort($sorter);
		foreach ($sorter as $ii => $va) {
			$ret[$ii]=$array[$ii];
		}
		$array=$ret;
	}
	
	function phone_number_parse($input_phone_number){
		$phone_country_code	= "";
		$phone_number 		= "";
		if (substr($input_phone_number, 0, 1) == "0"){ 
			$phone_country_code	= "+62"; 
			$phone_number 		= substr($input_phone_number, 1, (strlen($input_phone_number) - 1));
		}else{
			$arr_phone_number 	= explode(" ", $input_phone_number);
			if (count($arr_phone_number) > 0){
				$phone_country_code	= $arr_phone_number[0]; unset($arr_phone_number[0]);
				$phone_number 		= implode(" ", $arr_phone_number);
			}
		}
		$arr_return["phone_country_code"]	= $phone_country_code;
		$arr_return["phone_number"] 		= $phone_number;
		
		return $arr_return;
	}
	
	
	
	
	
	
	
	
	
	function load_db_gcms(){
		$this->_ci->db_gcms = $this->_ci->load->database("gcms", TRUE);
	}
	function load_db_gcsl(){
		$this->_ci->db_gcsl = $this->_ci->load->database("gcsl", TRUE);
	}
	function load_db_default(){
		$this->_ci->db = $this->_ci->load->database("default", TRUE);
	}
	
	function db_loadrestama(){
		$this->_ci->restamadb = $this->_ci->load->database("restama", TRUE);
	}
	function db_loaddefault(){
		$this->_ci->db = $this->_ci->load->database("default", TRUE);
	}
	
	function db_load_slave(){
		$this->_ci->dbsl = $this->_ci->load->database("slave", TRUE);
	}
}
?>
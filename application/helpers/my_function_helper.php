<?php
function pre($str){
	echo "<pre>";
	print_r($str);
	echo "</pre>";
}

function pre_test($str){
	if (isset($_GET["TEST"])){
		echo "<pre>";
		print_r($str);
		echo "</pre>";
	}
}


function pre_exit($str){
	echo "<pre>";
	print_r($str);
	echo "</pre>";
	
	exit();
}

function SYSTEM_BASIC_SETTING($var_name, $return_all_data = false){
	$_ci =& get_instance();
	
	$value = $_ci->db->where("f_var_name", $var_name)
					 ->get("tb_s_system_basic_setting");
	
	if ($value->num_rows() > 0){
		$value = $value->row_array();
		if ($return_all_data){
			return $value;
		}else{
			return $value["f_var_value"];
		}
	}else{
		return false;
	}
}

function HELP_POPOVER($code){
	$_ci =& get_instance();
	$help = $_ci->db->where("f_code", $code)->get("tb_help_extranet");
	
	if ($help->num_rows() > 0){
		$help = $help->row_array();
		?>
		<a class="help-popover" data-toggle="popover" data-placement="top" data-trigger="hover" title="<?=$help["f_title_en"]?>" data-content="<?=$help["f_short_description_en"]?>" style="background:none;color:#31B0D5;font-size:1em;vertical-align:middle;"><span class="glyphicon glyphicon-info-sign"></span></a>
		<?php
	}
}

function ACC_ROOM_CAPTION($vendor){
	$room_caption = "Room";
	if (isset($vendor["htl_detail"]["f_room_caption"])){
		$room_caption = $vendor["htl_detail"]["f_room_caption"];
	}elseif (isset($vendor["f_room_caption"])){
		$room_caption = $vendor["f_room_caption"];
	}
	return $room_caption;
}

function ACC_ROOM_CAPTION_BE($vendor){
	$room_caption = "Room";
	if ($vendor["htl_detail"]["f_rate_type"] == "PERSON"){
		$room_caption = "Person";
	}else{
		if (isset($vendor["htl_detail"]["f_room_caption"])){
			$room_caption = $vendor["htl_detail"]["f_room_caption"];
		}elseif (isset($vendor["f_room_caption"])){
			$room_caption = $vendor["f_room_caption"];
		}
	}
	return $room_caption;
}

if(!function_exists('array_column')) {
	function array_column(array $input, $columnKey, $indexKey = null) {
		$array = array();
		foreach ($input as $value) {
			if ( ! isset($value[$columnKey])) {
				//trigger_error("Key \"$columnKey\" does not exist in array");
				//return false;
			}
			if (is_null($indexKey)) {
				$array[] = $value[$columnKey];
			}
			else {
				if ( ! isset($value[$indexKey])) {
					//trigger_error("Key \"$indexKey\" does not exist in array");
					//return false;
				}
				if ( ! is_scalar($value[$indexKey])) {
					//trigger_error("Key \"$indexKey\" does not contain scalar value");
					//return false;
				}
				$array[$value[$indexKey]] = $value[$columnKey];
			}
		}
		return $array;
	}
}

function isset_p(&$var, $default=false) {
	return isset($var) ? $var : $default;
}

function empty_p($var, $default=false) {
	return empty($var) ? $default : $var;
}

?>
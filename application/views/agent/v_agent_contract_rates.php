<h1>Contract Rates</h1> 

<div ng-init="loadDataAgentContracRatesAssign()" style="margin: 10px 0;">
	<div ng-show="DATA.contract_rates_assign.status=='SUCCESS'">
        <div class="products">
            <div class="product" style="margin: 10px 0;">
                Filter : <input type="text" ng-model="filter_categories" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
            </div>
            
            <div ng-show='!DATA.contract_rates_assign.contract_rates_assign'>
                <img src="<?=base_url("public/images/loading_bar.gif")?>" />
            </div>
            
            <table class="table table-bordered table-condensed">
                <tr class="info" style="font-weight:bold">
                   
                    <td width="100">Code</td>
                    <td>Contract Rates Name</td>
                    <td width="150">Type</td>
                    <td width="160" align="center">Periode</td>

                </tr>
                <tr ng-class="{'danger':category.publish_status!='1'}" ng-repeat="category in DATA.contract_rates_assign.contract_rates_assign | orderBy:['name'] | filter : filter_categories ">
                 
                    <td align="center">{{category.contract_rate_code}}</td>
                    <td>
                        <a href="" data-toggle="modal" ng-click='assignAgentContractRates(category)' data-target="#add-edit-contract-rates">
                            {{category.name}}
                        </a>
                    </td>
                    
                    <td width="100">
                        {{category.contract_rate_type_desc}}
                        <span ng-show="category.contract_rate_type == 'PERCENT'"> 
                        	{{category.percentage}}%
                        </span>
                    </td>
                    <td align="center">
                        {{fn.newDate(category.start_date) | date:'dd MMM yyyy'}} -
                        {{fn.newDate(category.end_date) | date:'dd MMM yyyy'}}
                    </td>
                    
                </tr>
                <tr ng-repeat="category in DATA.contract_rates_assign.real_agent_categories | orderBy:['name'] | filter : filter_categories ">
                    
                    <td align="center">{{category.code_agent_real_category}}</td>
                    <td>
                        <a href="" ui-sref="agent.real_category.detail.contract_rates({'real_category_code':category.code_agent_real_category})" target="_blank">
                            {{category.name}}
                        </a>
                    </td>
                    
                    <td width="100">
                        {{category.contract_rate_type_desc}}
                        <?php /*?><span ng-show="category.contract_rate_type == 'PERCENT'"> 
                            {{category.percentage}}%
                        </span><?php */?>
                    </td>
                    <td align="center">
                        {{fn.newDate(category.start_date) | date:'dd MMM yyyy'}} -
                        {{fn.newDate(category.end_date) | date:'dd MMM yyyy'}}
                    </td>
                    
                </tr>
            </table>
        </div>
    </div>
    <div ng-show='!DATA.contract_rates_assign.contract_rates_assign' class="alert alert-warning"><em>Data not found...</em></div>
    <hr>

    <br>
    <br>
    
    <div class="modal fade" id="add-edit-contract-rates" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <form ng-submit='saveAssignAgentContractRates($event)'>
      <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">
            	Detail Contract Rates - <?=$vendor["business_name"]?>
            </h4>
          </div>
          <div class="modal-body">
            <div ng-show='DATA.myContractRates.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myContractRates.error_msg'>{{err}}</li></ul></div>
            <table class="table table-borderless table-condensed">
                <tr ng-show='!DATA.myContractRates.is_edit'>
                    <td width="150">Contract Rates Templates</td>
                    <td>
                    	<select ng-disabled='DATA.myContractRates.is_edit' ng-model='DATA.myContractRates' ng-change='selectContractRatesGetDetail()' 
                        	ng-options="item.name + ' (' + item.category_code + ') ' for item in DATA.agent_contract_rates | orderBy:'name'" class="form-control input-md" required style="max-width:500px">
                        	<option value="" disabled="disabled"> -- Contract Rates -- </option>
                        </select>
                    </td>
                </tr>
                <tr ng-show='DATA.myContractRates'>
                	<td width="120">Name</td>
                    <td><strong>: {{DATA.myContractRates.name}}</strong></td>
                </tr>
                <tr ng-show='DATA.myContractRates'>
                	<td>Code</td>
                    <td>: {{DATA.myContractRates.category_code}}</td>
                </tr>
               
                <tr ng-show='DATA.myContractRates'>
                	<td>Type</td>
                    <td>
                    	: {{DATA.myContractRates.contract_rate_type_desc}}

                        <span ng-show='DATA.myContractRates.contract_rate_type == "PERCENT"'>
                        	: 
                        	 {{DATA.myContractRates.percentage}} %
                        </span>
                    </td>
                </tr>
                <tr ng-show='DATA.myContractRates'>
                	<td>Periode</td>
                    <td>
                    	: {{fn.newDate(DATA.myContractRates.start_date) | date:'dd MMM yyyy'}} 
                        -
                        {{fn.newDate(DATA.myContractRates.end_date) | date:'dd MMM yyyy'}}

                    </td>
                </tr>
                <tr ng-show='DATA.myContractRates'>
                	<td>Description</td>
                    <td>
                    	<p>: {{DATA.myContractRates.description}}</p>
                    </td>
                </tr>
                <tr ng-show='DATA.myContractRates'>
                    <td width="150">Agent Payment Type</td>
                    <td>
                       : {{DATA.myContractRates.agent_payment_type}}
                    </td>
            	</tr>
            </table>
            
            <div ng-show="DATA.myContractRates.contract_rates">
            	<hr />
            	<h4>Detail Contract Rates</h4>
                
                <table class="table table-bordered table-condensed">
                    <tbody>
                        <tr class="header">
                            <td></td>
                            <td ng-show="DATA.myContractRates.contract_rate_type == 'PERCENT'"><strong>Publish</strong></td>
                            <td ng-show="DATA.myContractRates.contract_rate_type == 'PERCENT'"></td>
                            <td ><strong>Nett</strong></td>
                            <td ng-show="DATA.myContractRates.contract_rate_type == 'PERCENT'" width="55" align="center"></td>
                        </tr>
                        <tr ng-repeat='rates in DATA.myContractRates.contract_rates'>
                            <td>
                                <strong>{{rates.departure.port.name}} ({{rates.departure.time}}) 
                                <i class="fa fa-chevron-right"></i>
                                {{rates.arrival.port.name}} ({{rates.arrival.time}})</strong>
                                <br />
                                <small><strong>{{rates.rates_code}}</strong> - {{rates.name}}</small>
                            </td>
                            <td width="120">
                                <strong>One Way</strong><br />
                                <small>
                                    <div>
                                        Adult : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.one_way_rates.rates_1, rates.currency)}}</strong>
                                    </div>
                                    <div ng-show='rates.rates.one_way_rates.rates_2 > 0'>
                                        Child : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.one_way_rates.rates_2, rates.currency)}}</strong>
                                    </div>
                                    <div ng-show='rates.rates.one_way_rates.rates_3 > 0'>
                                        Infant : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.one_way_rates.rates_3, rates.currency)}}</strong>
                                    </div>
                                </small>
                                <strong>Return</strong><br />
                                <small>
                                    <div ng-show='rates.rates.return_rates.rates_1 > 0'>
                                        Adult : <strong>{{rates.currency}} {{fn.formatNumber((rates.rates.return_rates.rates_1*2), rates.currency)}}</strong>
                                    </div>
                                    <div ng-show='rates.rates.return_rates.rates_2 > 0'>
                                        Child : <strong>{{rates.currency}} {{fn.formatNumber((rates.rates.return_rates.rates_2*2), rates.currency)}}</strong>
                                    </div>
                                    <div ng-show='rates.rates.return_rates.rates_3 > 0'>
                                        Infant : <strong>{{rates.currency}} {{fn.formatNumber((rates.rates.return_rates.rates_3*2), rates.currency)}}</strong>
                                    </div>
                                </small>
                            </td>
                            <td width="30" style="vertical-align:middle; font-size:18px; text-align:center" ng-show="DATA.myContractRates.contract_rate_type == 'PERCENT'">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </td>
                            <td width="120" ng-show="DATA.myContractRates.contract_rate_type == 'PERCENT'">
                                <strong>One Way</strong><br />
                                <small>
                                    <div>
                                        Adult : <strong>{{rates.currency}} {{fn.formatNumber(rates.nett_rates.one_way_rates.rates_1, rates.currency)}}</strong>
                                    </div>
                                    <div ng-show='rates.rates.one_way_rates.rates_2 > 0'>
                                        Child : <strong>{{rates.currency}} {{fn.formatNumber(rates.nett_rates.one_way_rates.rates_2, rates.currency)}}</strong>
                                    </div>
                                    <div ng-show='rates.rates.one_way_rates.rates_3 > 0'>
                                        Infant : <strong>{{rates.currency}} {{fn.formatNumber(rates.nett_rates.one_way_rates.rates_3, rates.currency)}}</strong>
                                    </div>
                                </small>
                                <strong>Return</strong><br />
                                <small>
                                    <div ng-show='rates.rates.return_rates.rates_1 > 0'>
                                        Adult : <strong>{{rates.currency}} {{fn.formatNumber((rates.nett_rates.return_rates.rates_1*2), rates.currency)}}</strong>
                                    </div>
                                    <div ng-show='rates.rates.return_rates.rates_2 > 0'>
                                        Child : <strong>{{rates.currency}} {{fn.formatNumber((rates.nett_rates.return_rates.rates_2*2), rates.currency)}}</strong>
                                    </div>
                                    <div ng-show='rates.rates.return_rates.rates_3 > 0'>
                                        Infant : <strong>{{rates.currency}} {{fn.formatNumber((rates.nett_rates.return_rates.rates_3*2), rates.currency)}}</strong>
                                    </div>
                                </small>
                            </td>
                            <td align="center" ng-show="DATA.myContractRates.contract_rate_type == 'PERCENT'">
                                {{rates.percentage}} %
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                
            </div>
            
          </div>
         
        </div>
      </div>
      </form>
    </div>
    
    
    
    
    
    
</div>

<!-- <script>activate_sub_menu_agent_detail("contract_rates");</script> -->
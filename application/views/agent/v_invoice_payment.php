<h1>INVOICES</h1>
<div class="sub-title"> Payment Information</div>
<br />
<div ng-init="loadDataInvoicePayment()" class="reservation-detail">
	<div ng-show='!(DATA.payment)'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	<div ng-show='(DATA.current_invoice)'>
		<div class="pull-right">
			<div class="btn-group">
			  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fa fa-bars" aria-hidden="true"></i> Menu <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu">
				<li>
					<a href="<?=site_url("home/print_page/#/print/invoice_agent/")?>{{DATA.current_invoice.invoice.invoice_code}}" target="_blank" >
					<i class="fa fa-print" aria-hidden="true"></i> Print
					</a>
				</li>
				
			  </ul>
			</div>
		</div>
		<div class="title">
			<h1>#{{DATA.current_invoice.invoice.invoice_code}} - {{DATA.current_invoice.invoice.agent_name}}</h1>
			<div class="code"></div>
		</div>

		<ul class="nav nav-tabs sub-nav">
			<li role="presentation" class="detail"><a ui-sref="invoice_detail({'invoice_code':DATA.current_invoice.invoice.invoice_code})">Detail</a></li>
			<li role="presentation" class="payment"><a ui-sref="invoice_payment({'invoice_code':DATA.current_invoice.invoice.invoice_code})">Billing Payment
			</a></li>
		</ul>
		<br /><br />

	<div ng-show='(DATA.payment)'>
		<div class="table-responsive">
			<table class="table table-bordered table-payment">
				<tr class="info" style="font-weight:bold">
					<td align="center" width="100">Date</td>
					<td align="center" width="130">Code</td>
					<td>Description</td>
					<td width="130">Payment Type</td>
					<td align="right" colspan="2">Amount</td>
					<!-- <td width="40"></td> -->
				</tr>
				<tr ng-repeat='payment in DATA.payment.payment.detail'>
					<td align="center">{{fn.formatDate(payment.date, "dd M yy")}}</td>
					<td align="center">
						<span ng-show='payment.amount<0'>
							<a href="" data-toggle="modal" ng-click="paymentDetail(payment)" data-target="#payment-detail">
								{{payment.payment_code}}
							</a>
						</span>
						<span ng-show='payment.amount>=0'>{{payment.payment_code}}</span>
					</td>
					<td>{{payment.description}}</td>
					<td>{{payment.payment_type}}</td>
					<td align="center" width="50">{{payment.currency}}</td>
					<td align="right" width="110">
						<strong ng-show='payment.amount>=0'>{{fn.formatNumber(payment.amount, payment.currency)}}</strong>
						<strong ng-show='payment.amount<0'>({{fn.formatNumber(payment.amount*-1, payment.currency)}})</strong>
					</td>
					<?php /* <!-- <td align="center">
						<a href="" ng-click="paymentDetail(payment, true)" data-toggle="modal" data-target="#payment-detail" class="delete-icon" ng-show="payment.id != ''">
							<span class="glyphicon glyphicon-trash"></span>
						</a>
					</td> --> */?>
				</tr>
				<tr>
					<td colspan="7"></td>
				</tr>
				<tr class="success" style="font-weight:bold">
					<td colspan="4" align="right">Total Transaction</td>
					<td>{{DATA.payment.payment.currency}}</td>
					<td align="right">{{fn.formatNumber(DATA.payment.payment.total_transaction, DATA.payment.payment.currency)}}</td>
					<!-- <td></td> -->
				</tr>
				<tr class="success" style="font-weight:bold">
					<td colspan="4" align="right">Total Payment</td>
					<td>{{DATA.payment.payment.currency}}</td>
					<td align="right">({{fn.formatNumber(DATA.payment.payment.total_payment, DATA.payment.payment.currency)}})</td>
					<!-- <td></td> -->
				</tr>
				<tr style="font-weight:bold" ng-class="{'danger':(DATA.payment.payment.balance>0), 'info':(DATA.payment.payment.balance<=0)}">
					<td colspan="4" align="right">Balance</td>
					<td>{{DATA.payment.payment.currency}}</td>
					<td align="right">{{fn.formatNumber(DATA.payment.payment.balance, DATA.payment.payment.currency)}}</td>
					<!-- <td></td> -->
				</tr>
			</table>
		</div>
		<hr>
		<?php /* <!-- <div class="add-product-button" ng-show="DATA.payment.payment.balance > 0"> 
			<a href="" class="btn btn-success btn-lg btn-block" data-toggle="modal" ng-click='addEditPayment()' data-target="#add-edit-payment"> <span class="glyphicon glyphicon-plus"></span> Add Payment </a> 
		</div>
		<br>
		<br> --> */?>
	</div>
	
	<!-- modal add payment -->	
	<div class="modal fade" id="add-edit-payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <form ng-submit='saveDataPayment($event)'>
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">
				<span ng-show='!DATA.myPayment.id'>Add</span><span ng-show='DATA.myPayment.id'>Edit</span> Payment
			</h4>
		  </div>
		  <div class="modal-body">
		  	<div ng-show='DATA.myPayment.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myPayment.error_msg'>{{err}}</li></ul></div>
			<table class="table table-borderless table-condenseds">
				<tr>
					<td width="130">Invoice Code</td>
					<td><strong>#{{DATA.current_invoice.invoice.invoice_code}}</strong></td>
				</tr>
				<tr>
					<td>Payment Date*</td>
					<td><input placeholder="Payment Date" required="required" type="text" class="form-control input-md datepicker" ng-model='DATA.myPayment.date' style="width:150px" /></td>
				</tr>
				<tr>
					<td>Payment Type*</td>
					<td>
						<select required="required" class="form-control input-md" ng-model='DATA.myPayment.payment_type' ng-change='changePaymentType()'>
							<option value="" disabled="disabled">-- Select Payment Type --</option>
							<option value="CC">Credit Card/Debit Card</option>
							<option value="ATM">Bank Transfer</option>
							<option value="CASH">Cash</option>
						</select>
					</td>
				</tr>
				<tr ng-show="DATA.myPayment.payment_type=='CC' || DATA.myPayment.payment_type=='ATM'" class="header">
					<td><span ng-show="DATA.myPayment.payment_type=='CC'">Card Number</span><span ng-show="DATA.myPayment.payment_type=='ATM'">Account Number</span></td>
					<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.myPayment.account_number' /></td>
				</tr>
				<tr ng-show="DATA.myPayment.payment_type=='CC' || DATA.myPayment.payment_type=='ATM'" class="header">
					<td>Name On Card</td>
					<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.myPayment.name_on_card' /></td>
				</tr>
				<tr ng-show="DATA.myPayment.payment_type=='CC' || DATA.myPayment.payment_type=='ATM'" class="header">
					<td><span ng-show="DATA.myPayment.payment_type=='CC'">EDC Machine</span><span ng-show="DATA.myPayment.payment_type=='ATM'">Bank Name</span></td>
					<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.myPayment.bank_name' /></td>
				</tr>
				<tr ng-show="DATA.myPayment.payment_type=='CC' || DATA.myPayment.payment_type=='ATM'" class="header">
					<td>Approval Number</td>
					<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.myPayment.payment_reff_number' /></td>
				</tr>
				<tr>
					<td>Description*</td>
					<td><input placeholder="Description" required="required" type="text" class="form-control input-md" ng-model='DATA.myPayment.description' /></td>
				</tr>
				<tr ng-show="DATA.myPayment.payment_type!='OPENVOUCHER'">
					<td>Payment Amount*</td>
					<td>
						<div class="input-group">
							<span class="input-group-addon">{{DATA.current_invoice.invoice.invoice_curr}}</span>
							<input placeholder="Payment Amount" required="required" type="number" min="0" class="form-control input-md" ng-model='DATA.myPayment.amount' style="width:160px" />
						</div>
					</td>
				</tr>
			</table>
			
		  </div>
		  <div class="modal-footer" style="text-align:center">
		  	<button type="submit" class="btn btn-primary">Save</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
	  </div>
	  </form>
	</div>
	<!--/ modal add payment -->

	<!-- modal delete payment -->
	<div class="modal fade" id="payment-detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">
				Payment Detail #{{myPaymentDetail.payment_code}}
			</h4>
		  </div>
		  <div class="modal-body">
		  	<div ng-show='DATA.myPayment.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myPayment.error_msg'>{{err}}</li></ul></div>
			<table class="table table-borderless table-condensed">
				<tr>
					<td width="130">Booking Code</td>
					<td><strong>#{{DATA.current_invoice.invoice.invoice_code}}</strong></td>
				</tr>
				<tr>
					<td>Payment Date</td>
					<td>{{fn.formatDate(myPaymentDetail.date, "d MM yy")}}</td>
				</tr>
				<tr>
					<td>Payment Type</td>
					<td>{{myPaymentDetail.payment_type}}</td>
				</tr>
				<tr ng-show="myPaymentDetail.payment_type=='CC' || myPaymentDetail.payment_type=='ATM'">
					<td><span ng-show="myPaymentDetail.payment_type=='CC'">Credit Card Number</span><span ng-show="myPaymentDetail.payment_type=='ATM'">Account Number</span></td>
					<td>{{myPaymentDetail.account_number}}</td>
				</tr>
				<tr ng-show="myPaymentDetail.payment_type=='CC' || myPaymentDetail.payment_type=='ATM'">
					<td>Name On Card</td>
					<td>{{myPaymentDetail.name_on_card}}</td>
				</tr>
				<tr ng-show="myPaymentDetail.payment_type=='CC' || myPaymentDetail.payment_type=='ATM'">
					<td><span ng-show="myPaymentDetail.payment_type=='CC'">EDC Machine</span><span ng-show="myPaymentDetail.payment_type=='ATM'">Bank Name</span></td>
					<td>{{myPaymentDetail.bank_name}}</td>
				</tr>
				<tr ng-show="myPaymentDetail.payment_type=='CC' || myPaymentDetail.payment_type=='ATM'">
					<td>Approval Number</td>
					<td>{{myPaymentDetail.payment_reff_number}}</td>
				</tr>
				<tr ng-show="myPaymentDetail.payment_type=='OPENVOUCHER'">
					<td>Open Voucher Code</td>
					<td>{{myPaymentDetail.payment_reff_number}}</td>
				</tr>
				<tr>
					<td>Description</td>
					<td>{{myPaymentDetail.description}}</td>
				</tr>
				<tr>
					<td>Payment Amount</td>
					<td>{{myPaymentDetail.currency}} {{fn.formatNumber(myPaymentDetail.amount, myPaymentDetail.currency)}}</td>
				</tr>
			</table>
			<div ng-show='myPaymentDetail.delete_payment' class="alert alert-warning">
				<form ng-submit='submitDeletePayment($event)'>
					<strong>Are you sure to delete this payment?</strong><br />
					<label><input type="radio" name="delete_payment_confirmation" value="1" ng-model='myPaymentDetail.sure_to_delete' /> Yes</label>
					&nbsp;&nbsp;
					<label><input type="radio" name="delete_payment_confirmation" value="0" ng-model='myPaymentDetail.sure_to_delete' /> No</label>
					<div ng-show="myPaymentDetail.sure_to_delete=='1'">
						<hr style="margin:5px 0" />
						
						<div ng-show='myPaymentDetail.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in myPaymentDetail.error_msg'>{{err}}</li></ul></div>
						
						<strong>Remarks:</strong>
						<textarea rows='2' placeholder="Remarks" class="form-control input-md autoheight" ng-model='myPaymentDetail.delete_remarks' required="required"></textarea>
						<br />
						<button type="submit" class="btn btn-primary">Delete Payment</button>
					</div>
				</form>
			</div>
		  </div>
		  <div class="modal-footer" style="text-align:center">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
	  </div>
	</div>
	<!--/ modal delete payment -->

</div>
<!-- <script>activate_sub_menu_invioce_detail("payment");</script> -->
<style>
	table.table-payment .delete-icon{opacity:0.3; color:red}
	table.table-payment a:hover.delete-icon{opacity:1;}
</style>
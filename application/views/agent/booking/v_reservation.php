
<div ng-controller="reservation_controller" ng-init="initData()">
<div id="container_outer" class="container">
    <div id="booking_step">
        <div class="res_nav">
            <ol class="custom-theme-booking-step">
                <li class="step-1 active"><span>Product List</span></li>
                <li class="sp-step-2 sp"></li>
                <li class="step-2"><span>Shopping Cart</span></li>
                <li class="sp-step-3 sp"></li>
                <li class="step-3"><span>Booking Details & Payment</span></li>
                <li class="sp-step-4 sp"></li>
                <li class="step-4"><span>Finish</span></li>
            </ol>
        </div>
      <!--   <div class="dropdown pull-right" style="margin-top:-55px;margin-right: 70px;">
        <button class="btn btn-default dropdown-toggle" type="button" id="dr-lang" data-toggle="dropdown" aria-expanded="true">
            English <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dr-lang">
            <li role="presentation"><a role="menuitem" tabindex="-1">Bahasa Indonesia</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1">English</a></li>
        </ul>
        </div> -->
     
    </div>

    <div class="col-lg-12 col-md-12" >
      <form ng-submit='check_availabilities($event)'>
        <div class="row" style="background-color:#FAFAFA;border-radius:3px; border:1px solid #DDDDDD;padding:15px 15px 15px 15px;">
          <div class="col-lg-5 col-md-5 col_dep">
            <div class="row"> <b>Departure</b>
            <select tabindex="3" class="form-control input-sm departure-port" ng-model='add_trip.departure_port_id' required="required">
                <option value="">-- Departure Port --</option>
                <option value="{{port.id}}" ng-repeat='port in DATA.available_port.ports'>{{port.port_code}} - {{port.name}}</option>
            </select>
            </div>
            <div class="row"> <b>Destination</b>
            <select tabindex="3" class="form-control input-sm arrival-port" ng-model='add_trip.arrival_port_id' required="required">
                <option value="">-- Destination Port --</option>
                <option value="{{port.id}}" ng-repeat='port in DATA.available_port.ports'>{{port.port_code}} - {{port.name}}</option>
            </select>
             </div>
            <div class="row" style="font-size:10px; line-height:normal;"> </div>
          </div>
           <div class="col-lg-7 col-md-7 col_date" >
            <div class="row" >
              <div class="col-lg-6 col-md-6" style="padding:2px;"> <b>Departure Date</b>
                <input tabindex="5" type="text" class="form-control input-sm datepicker" required="required" placeholder="Departure Date" ng-model='add_trip.departure_date' />
              </div>
              <div class="col-lg-6 col-md-6" id="div_arrival" style="padding:2px; " ng-disabled=""> <b>Return</b>
                <div class="input-group"> <span id="c2" class="input-group-addon"  style='background-color:#DEDEDE'>
                  <input type="checkbox" name="typ" id="type_2" ng-model='trip_model' value="RETURN" ng-change="changeClick(trip_model)" >
                  </span>
                 <input disabled tabindex="6" type="text" class="form-control input-sm datepicker" id="return" ng-required="add_trip.trip_model=='RETURN'" placeholder="Return Date" ng-model='add_trip.return_date' />
                </div>
                <input type="hidden" name="ret" class="altCheckOut" id="date_arrival2"   value=""/>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-3 col-md-3 col-xs-3"> <b>Adult</b>
                <input tabindex="7" type="number" class="form-control input-sm" placeholder="Adult" required="required" ng-model='add_trip.adult' min="0" />
                <p ></p> </div>
              <div class="col-lg-43 col-md-3 col-xs-3" style="padding:2px;"> <b>Child</b>
                <input tabindex="8" type="number" class="form-control input-sm" placeholder="Child" required="required" ng-model='add_trip.child' min="0" /><p >13 – 16 years old</p> 
              </div>
              <div class="col-lg-3 col-md-3 col-xs-3" style="padding:2px;"> <b>Infant</b>
                <input tabindex="9" type="number" class="form-control input-sm" placeholder="Infant" required="required" ng-model='add_trip.infant' min="0" />
               <p >3 – 12 years old</p>
                </div>
                <div class="col-lg-3 col-md-3 " style="padding:2px;padding-top:20px;">
                <button type="submit" class="btn btn-success pull-right custom-theme-button-1">Search &nbsp;<i class="fa fa-search "></i></button>
                </div>
            </div>
           
          </div>
        </div>
      </form>
    </div>

    <div style="margin: 10px;" class="text-center" ng-show='check_availabilities_show_loading'>
       <img style="height: 30px;width: auto;" src="<?=base_url("public/images/loading_bar.gif")?>" />
    </div>
    
    <div ng-show='check_availabilities_data.departure && !check_availabilities_data.departure.availabilities'>
        <div class="alert alert-warning">
            <strong>Sorry</strong>, Trip not available, please select another date...
        </div>
    </div>

    <!-- Product List -->
    <div class="col-lg-12 col-md-12" style="margin-top:15px;margin-bottom:15px;" ng-show='check_availabilities_data.departure.availabilities'>

        <div class="row" style="background-color:#eeeeee;padding-bottom:15px" align="center">
        <h4>
            {{check_availabilities_data.departure.check.departure.name}}&nbsp;

            <i class="fa fa-angle-right" style="font-weight:bold"></i>&nbsp; 

            {{check_availabilities_data.departure.check.arrival.name}}          
        </h4>
            <b>{{fn.formatDate(check_availabilities_data.departure.check.date, "dd M yy")}}</b> 
        </div>
        <hr style="margin:5px 0" />
        <div class="trip" ng-class="{'selected':availabilities.is_selected}" ng-show='!availabilities.is_hidden' ng-repeat="availabilities in check_availabilities_data.departure.availabilities | orderBy : 'departure.time'">
          <form ng-submit="check_availabilities_select_this_trip(availabilities, check_availabilities_data.departure)">
           
            <div class="row">
              <div class="col-lg-2 col-md-2 col-xs-2" style="padding:0px;margin-right: 10px;">
                
                <img ng-src="{{availabilities.schedule.boat.main_image}}" width="100%">
              </div>
              <div class="col-lg-7 col-md-7 col-xs-7" style="padding:3px;">

               
              
                <p class="pull-right text-right" ng-show='!availabilities.smart_pricing'>
                    Adult: <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_1, availabilities.currency)}}</strong><br />
                    Child: <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_2, availabilities.currency)}}</strong><br />
                    Infant: <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_3, availabilities.currency)}}</strong><br />
                </p>
               
                <i class="fa fa-clock-o"></i> <strong>{{availabilities.departure.time}} - {{availabilities.arrival.time}}</strong> 
                    <span ng-show="availabilities.schedule.boat.available_space > 0">({{availabilities.schedule.boat.available_space}} space left) </span>
                    <span ng-show="availabilities.schedule.boat.available_space <= 0">(0 space left) </span>
                    <span ng-show="availabilities.schedule.boat.is_extra_seat == '1'">({{availabilities.schedule.boat.extra_seat_capacity}} extra seat)</span>
                <br /> 
                <i class="fa fa-ship"></i>   <strong>{{availabilities.schedule.boat.name}} ({{availabilities.schedule.boat.capacity}} seats)</strong>
                <br />
                <i class="fa fa-dollar"></i> &nbsp; {{availabilities.name}}<br />
                <i class="fa fa-car" aria-hidden="true"></i> 
                    <i class="fa fa-check" aria-hidden="true" ng-show="availabilities.pickup_service == 'yes'"></i> <i class="fa fa-remove" aria-hidden="true" ng-show="availabilities.pickup_service != 'yes'" style="color:red"></i> Pickup 
                    &nbsp;
                    <i class="fa fa-check" aria-hidden="true" ng-show="availabilities.dropoff_service == 'yes'"></i> <i class="fa fa-remove" aria-hidden="true" ng-show="availabilities.dropoff_service != 'yes'" style="color:red"></i> Dropoff
               <br />
                
              </div>
              <div class="col-lg-2 col-md-2 col-xs-2" style="padding:0px;" align="right">
                <div ng-show='availabilities.smart_pricing'>
                    <div ng-repeat='pricing_level in availabilities.smart_pricing.applicable_rates'>
                        <hr style="margin:5px 0" />
                        <div class="pull-right">{{pricing_level.allotment}} pax <span class="glyphicon glyphicon-ok" ng-show='pricing_level.allotment > 0' style="color:green"></span></div>
                        {{($index+1)}}.
                        Adult: <strong>{{pricing_level.currency}} {{fn.formatNumber(pricing_level.rates.rates_1, pricing_level.currency)}}</strong>
                        Child: <strong>{{pricing_level.currency}} {{fn.formatNumber(pricing_level.rates.rates_2, pricing_level.currency)}}</strong>
                        Infant: <strong>{{pricing_level.currency}} {{fn.formatNumber(pricing_level.rates.rates_3, pricing_level.currency)}}</strong>
                        <div class="clearfix"></div>
                    </div>
                    <hr style="margin:5px 0" /><br />
                </div>
                <!-- <div class="row">
                    <span style="font-size: 20px;"><b><?=$vendor["default_currency"]?> {{fn.formatNumber(DATA.data_rsv.TOTAL.grand_total, "<?=$vendor["default_currency"]?>")}}</b></span>
                </div> -->
                <div class="row">

                  <span ng-show="!button_check"><button type="submit" class="btn  btn-warning pull-right btn_select_dep"><b>select <i class="fa fa-chevron-circle-right"></i></b></button></span>
                  <span ng-show="button_check"><button disabled class="btn  btn-success pull-right btn_select_dep"><b><i class="fa fa-check"></i></b></button></span>
                </div>
              </div>
              
            </div>
         
                 
            </form>
        </div>
         <hr />
    </div>
   
    <!-- End PL -->

    <!-- RETURN -->
     <div class="col-lg-12 col-md-12" style="margin-top:15px;margin-bottom:15px;" ng-show='check_availabilities_data.return.availabilities'>

        <div class="row" style="background-color:#eeeeee;padding-bottom:15px" align="center">
        <h4>
            {{check_availabilities_data.return.check.departure.name}}&nbsp;

            <i class="fa fa-angle-right" style="font-weight:bold"></i>&nbsp; 

            {{check_availabilities_data.return.check.arrival.name}}          
        </h4>
            <b>{{fn.formatDate(check_availabilities_data.return.check.date, "dd M yy")}}</b> 
        </div>
        <hr style="margin:5px 0" />
        <div class="trip" ng-class="{'selected':availabilities.is_selected}" ng-show='!availabilities.is_hidden' ng-repeat="availabilities in check_availabilities_data.return.availabilities | orderBy : 'return.time'">
          <form ng-submit="check_availabilities_select_this_trip(availabilities, check_availabilities_data.return)">
           
            <div class="row">
              <div class="col-lg-2 col-md-2 col-xs-2" style="padding:0px;margin-right: 10px;">
                
                <img ng-src="{{availabilities.schedule.boat.main_image}}" width="100%">
              </div>
              <div class="col-lg-7 col-md-7 col-xs-7" style="padding:3px;">

               
              
                <p class="pull-right text-right" ng-show='!availabilities.smart_pricing'>
                    Adult: <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_1, availabilities.currency)}}</strong><br />
                    Child: <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_2, availabilities.currency)}}</strong><br />
                    Infant: <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_3, availabilities.currency)}}</strong><br />
                </p>
               
                <i class="fa fa-clock-o"></i> <strong>{{availabilities.return.time}} - {{availabilities.arrival.time}}</strong> 
                    <span ng-show="availabilities.schedule.boat.available_space > 0">({{availabilities.schedule.boat.available_space}} space left) </span>
                    <span ng-show="availabilities.schedule.boat.available_space <= 0">(0 space left) </span>
                    <span ng-show="availabilities.schedule.boat.is_extra_seat == '1'">({{availabilities.schedule.boat.extra_seat_capacity}} extra seat)</span>
                <br /> 
                <i class="fa fa-ship"></i>   <strong>{{availabilities.schedule.boat.name}} ({{availabilities.schedule.boat.capacity}} seats)</strong>
                <br />
                <i class="fa fa-dollar"></i> &nbsp; {{availabilities.name}}<br />
                <i class="fa fa-car" aria-hidden="true"></i> 
                    <i class="fa fa-check" aria-hidden="true" ng-show="availabilities.pickup_service == 'yes'"></i> <i class="fa fa-remove" aria-hidden="true" ng-show="availabilities.pickup_service != 'yes'" style="color:red"></i> Pickup 
                    &nbsp;
                    <i class="fa fa-check" aria-hidden="true" ng-show="availabilities.dropoff_service == 'yes'"></i> <i class="fa fa-remove" aria-hidden="true" ng-show="availabilities.dropoff_service != 'yes'" style="color:red"></i> Dropoff
               <br />
                
              </div>
              <div class="col-lg-2 col-md-2 col-xs-2" style="padding:0px;" align="right">
               
                <div class="row">

                  <button type="submit" class="btn  btn-warning pull-right btn_select_dep"><b>select <i class="fa fa-chevron-circle-right"></i></b></button>
                  <button ng-show="" type="submit" class="btn  btn-success pull-right btn_select_dep"><b><i class="fa fa-check"></i></b></button>
                </div>
              </div>
              
            </div>
         
                 
            </form>
            <hr style="margin:10px 0" />
        </div>

    </div>
    <!-- END RETURN -->
    
</div>
<div style="margin-top: 40px;">
        <?php $this->load->view("agent/booking/v_logo_payment") ?>
    </div>
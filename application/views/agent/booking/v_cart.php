<style type="text/css">
  .return {
    margin-left: 50px !important;
    margin-bottom: 20px !important;
  }
</style>
<div id="container_outer" class="container">
<div id="booking_step">
      <div class="res_nav">
          <ol class="custom-theme-booking-step">
              <li class="step-1" ><span>Product List</span></li>
              <li class="sp-step-2 sp sp_active"></li>
              <li class="step-2 active"><span>Shopping Cart</span></li>
              <li class="sp-step-3 sp"></li>
              <li class="step-3"><span>Booking Details & Payment</span></li>
              <li class="sp-step-4 sp"></li>
              <li class="step-4"><span>Finish</span></li>
          </ol>
      </div>
      <!-- <div class="dropdown pull-right" style="margin-top:-55px;margin-right: 70px;">
      <button class="btn btn-default dropdown-toggle" type="button" id="dr-lang" data-toggle="dropdown" aria-expanded="true">
          English <span class="caret"></span>
      </button>
      <ul class="dropdown-menu" role="menu" aria-labelledby="dr-lang">
          <li role="presentation"><a role="menuitem" tabindex="-1">Bahasa Indonesia</a></li>
          <li role="presentation"><a role="menuitem" tabindex="-1">English</a></li>
      </ul>
      </div> -->
   
  </div>
<div ng-init="loadCart()">

  
<a ui-sref="rsv"><i class="fa fa-angle-left"></i> Book other trip</a>
<div class="text-center" ng-show="loading">
    <img style="height: 80px;width: auto;" src="<?=base_url("public/images/loading_new.gif")?>" />
</div>
<div style="margin-top: 20px;" ng-show="CART_is_empty">
  <span style="color: rgba(0 0 0 0.1); font-size: 14px;">You have not made a purchase voucher.
  To make a purchase, please choose one of the vouchers by pressing the Get Voucher </span> <br /><br />

</div>

  <div class="col-xs-12 products" ng-show="!CART_is_empty">
   
     <!-- ng-class="{'return': (DATA.cart[$index].type != 'RETURN') }"  -->
    <div  ng-class="{'return': (DATA.cart[$index].type == 'RETURN') }" class="row product" style="margin: 10px 0; " ng-repeat="($index,item) in product_detail" ng-repeat="($index,item) in CART">
           
          <span style="font-size: 14px;">
            <strong>{{product_rate[$index].arrival.port.name}} to {{product_rate[$index].departure.port.name}}</strong>
          </span>
          
          <div class="col-md-12" >
              <div ng-show="DATA.cart[$index].type != 'RETURN'" class="pull-right"><span class="align-bottom" style="color: red;"><button class="btn btn-sm" style="color: grey; background-color: none;" ng-click="deleteCart($index)"><small><i class="fa fa-trash"></i> Delete</small></button></span></div>
          </div>

          <div class="col-sm-2 col-xs-4">
                                              
              <div class="thumb">       
                 <img ng-src="{{item.boat.main_image}}" width="100%">                 
              <!-- <div id="myCarousel_{{item.schedule_code}}" class="carousel slide" data-ride="carousel"> -->
                 
                  <!-- <ol class="carousel-indicators" >
                    <li ng-repeat="($index,slide) in item.gallery" ng-class="{active: $index==0}" data-target="#myCarousel_{{item.schedule_code}}" data-slide-to="{{$index}}"></li>
                  </ol>

                  <div class="carousel-inner">
                    <div class="item" ng-repeat="($index,slide) in item.boat.gallery" ng-class="{active: $index==0}">
                      <img src="{{slide.image}}" alt="{{slide.title}}" class="img-responsive">
                    </div>
                  </div> -->
                  

                 <!-- Left and right controls -->
                 <!-- <a class="left carousel-control" ng-click="carouselPrev(item.schedule_code)" role="button">
                   <span class="glyphicon glyphicon-chevron-left"></span>
                   <span class="sr-only">Previous</span>
                 </a>
                 <a class="right carousel-control" ng-click="carouselNext(item.schedule_code)" role="button">
                   <span class="glyphicon glyphicon-chevron-right"></span>
                   <span class="sr-only">Next</span>
                 </a>
               </div> -->
              </div>
                              
          </div>
          <div class="col-sm-8 col-xs-6">
              <div class="row">
                  <div class="col-sm-12 short-description">
                      <div class="short" style="min-height:9em;">
                          <table>
                            <tr>
                              <td>
                                <b>{{product_rate[$index].name}}</b>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <strong><i class="fa fa-map-marker"></i>&nbsp;{{product_rate[$index].arrival.port.name}} at {{product_rate[$index].arrival.time}} &nbsp;<i class="fa fa-angle-right"></i>&nbsp;
                                {{product_rate[$index].departure.port.name}} at {{product_rate[$index].departure.time}} </strong>
                              </td>
                            </tr>
                          </table>
                          <table>
                            <tr>
                              <td>Date </td>
                              <td><strong> : {{fn.formatDate(CART[$index].date,"dd MM yy")}}</strong></td>
                            </tr>
                            <tr>
                              <td>Boat </td>
                              <td><strong> : {{product_rate[$index].departure.port.name}}</strong></td>
                            </tr>
                            <tr ng-show="CART[$index].adult">
                              <td>Adult </td>
                              <td><strong> : {{CART[$index].adult}} @ {{product_rate[$index].currency}} {{fn.formatNumber(CART[$index].applicable_rates.rates_1, product_rate[$index].currency)}}</strong></td>
                            </tr>
                            <tr ng-show="CART[$index].child">
                              <td>
                                Student 
                              </td>
                              <td>
                                <strong> : {{CART[$index].child}} @ {{product_rate[$index].currency}} {{fn.formatNumber(CART[$index].applicable_rates.rates_2, product_rate[$index].currency)}}</strong>
                              </td>
                            </tr>
                            <tr ng-show="CART[$index].infant">
                              <td>Child </td>
                              <td><strong> : {{CART[$index].infant}} @ {{product_rate[$index].currency}} {{fn.formatNumber(CART[$index].applicable_rates.rates_3, product_rate[$index].currency)}}</strong></td>
                            </tr>
                            
                          </table>
                                   
                      </div>
                      
                  </div>
              </div>  

          </div>
           <hr style="margin:10px 0" />   
          <div class="col-md-2 text-right" style="margin-top: 10px;" >
              
              <span class="align-middle" style="font-size: 16px;">{{product_rate[$index].currency}} {{fn.formatNumber(total_product[$index],product_rate[$index].currency)}}</span>
            
             <!--  <span class="align-bottom"><button class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></button></span> -->
          </div>
          
      
    </div>

    
    <div class="row product">
      <div class="col-md-6"></div>
      <div class="col-md-2 text-right" >
         Grand Total:
      </div>
      <div class="col-md-4 text-right" style="font-size: 20px;">
         <strong> {{product_rate[0].currency}} {{fn.formatNumber(CART.grand_total, product_rate[0].currency)}}</strong>
      </div>
    </div>
    <dir class="row product">
      <div class="col-md-12 text-right" >
         <button ui-sref="rsv_detail" class="btn btn-info btn-lg custom-theme-button-2" id="btn-sumbit" >Proceed to Checkout <span class="glyphicon glyphicon-chevron-right"></span> </button>
      </div>
     
    </dir>
  
</div>
</div>
</div>

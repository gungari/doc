
<div id="container_outer" class="container">
<div id="booking_step">
      <div class="res_nav">
          <ol class="custom-theme-booking-step">
              <li class="step-1" ><span>Product List</span></li>
              <li class="sp-step-2 sp"></li>
              <li class="step-2 "><span>Shopping Cart</span></li>
              <li class="sp-step-3 sp sp_active"></li>
              <li class="step-3 active"><span>Booking Details & Payment</span></li>
              <li class="sp-step-4 sp"></li>
              <li class="step-4"><span>Finish</span></li>
          </ol>
      </div>
     <!--  <div class="dropdown pull-right" style="margin-top:-55px;margin-right: 70px;">
      <button class="btn btn-default dropdown-toggle" type="button" id="dr-lang" data-toggle="dropdown" aria-expanded="true">
          English <span class="caret"></span>
      </button>
      <ul class="dropdown-menu" role="menu" aria-labelledby="dr-lang">
          <li role="presentation"><a role="menuitem" tabindex="-1">Bahasa Indonesia</a></li>
          <li role="presentation"><a role="menuitem" tabindex="-1">English</a></li>
      </ul>
      </div> -->
   
  </div>
<!-- <div ng-init="loadCart();check_submit();loadTerm()" >  --> 
	<div ng-init="loadCart();loadTerm()" > 
<form ng-submit="submit_booking($event)" method="post" id="form-booking" class="submit_loading">
	<div class="content-page cart-list-detail" style="font-size:12px">
	<div class="row ">

	<div class="col-lg-8 col-md-8 ">
		<h2 class="title-page" style="margin-top:0">Booking Details</h2>
		
<hr />
<div ng-show='DATA.data_rsv.error_desc.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.data_rsv.error_desc'>{{err}}</li></ul></div>

<h4>Guest Information</h4>
<div style="background:#F8F8F8;" class="table-field">
<div class="sub-title">Guest Information</div>
<table class="table">
	<tr>
		<td width="150">Name*</td>
		<td>			
			<input type="text" placeholder="First Name" class="form-control input-sm" ng-model='DATA.data_rsv.customer.first_name' style="display:inline; width:45.5%" />
			&nbsp;&nbsp;&nbsp;
			<input type="text" placeholder="Last Name" class="form-control input-sm" ng-model='DATA.data_rsv.customer.last_name' style="display:inline; width:45.5%" />
		</td>
	</tr>
	<tr>
		<td>Email</td>
		<td><input type="text" placeholder="Email" class="form-control input-sm" ng-model='DATA.data_rsv.customer.email' /></td>
	</tr>
	
	<tr>
		<td>Phone / Mobile*</td>
		<td><input type="text" placeholder="Phone / Mobile" class="form-control input-sm" ng-model='DATA.data_rsv.customer.phone' /></td>
	</tr>
	<tr>
		<td>Country*</td>
		<td>
			<select class="form-control input-sm" ng-model='DATA.data_rsv.customer.country_code'>
				<option value="">-- Select Country --</option>
				<option value="{{country.code}}" ng-repeat='country in DATA.country_list.country_list'>{{country.name}}</option>
			</select>
		</td>
	</tr>
</table>

</div>

<hr />
<label>
	<input name="cbsetuju" type="checkbox" id="cbsetuju" value="1" onclick="if($(this).is(':checked')){$('#form-booking #btn-sumbit').removeAttr('disabled');}else{$('#form-booking #btn-sumbit').attr('disabled','disabled')}">
	<strong style="line-height: 1.5em;">
	I agree to the 
	<a href="#" data-href="https://bes.hybridbooking.com/booking/rsv/term_and_condition/SCO?ajax" class="fancybox ajax-data">Terms And Conditions</a> 
	</strong>
</label>
<hr />
<div align="center">
	<button type="submit" class="btn btn-info btn-lg custom-theme-button-2" id="btn-sumbit" disabled="disabled">Proceed to Payment <span class="glyphicon glyphicon-chevron-right"></span> </button>
</div>

</div>

	<div class="col-lg-4 col-md-4 cart-list desktop-view" ng-show="CART">
		<div style="background:#007d9d; border-radius:10px; color:#FFF" class='custom-theme-sidebar'>
		<div ng-repeat="($index,item) in product_detail" ng-repeat="($index,item) in CART"   style="padding:10px; line-height:1.3em; border-bottom:solid 1px #4799AD">
			<div><strong style="font-size:14px">{{product_rate[$index].arrival.port.name}} to {{product_rate[$index].departure.port.name}}</strong></div>
			<div>({{product_rate[$index].arrival.port.name}} to {{product_rate[$index].departure.port.name}})</div>
			<div style="font-size:11px">
				{{fn.formatDate(CART[$index].date,"dd MM yy")}}                 
				<br />
				<span ng-show="CART[$index].adult">{{CART[$index].adult}} Adult </span>													
				<span ng-show="CART[$index].child">| {{CART[$index].child}} Student </span>
				<span ng-show="CART[$index].infant">| {{CART[$index].infant}} Child</span>														
			</div>
			<div style="text-align:right">
			{{product_rate[$index].currency}} <strong> {{fn.formatNumber(total_product[$index],product_rate[$index].currency)}}</strong>
			</div>
		</div>

		<div style="text-align:right; padding:15px 10px 10px 10px">
			{{product_rate[0].currency}} <strong> {{fn.formatNumber(get_total(), product_rate[0].currency)}}</strong>
		</div>
		</div>

	</div>
</div>

</div>
</form>
</div>
</div>
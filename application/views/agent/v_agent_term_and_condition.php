<script type="text/javascript">
    var is_cancellation = false;
</script>

<div ng-init="loadDataTermsAndConditions()">
	<div ng-show="!terms_and_conditions">
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
    <br />
    <div ng-show="terms_and_conditions">
    	<div class="sub-title">Voucher Terms And Conditions</div>
        <br />
        <div ng-show="terms_and_conditions">
            <ol>
                <li ng-repeat='tac in terms_and_conditions'>{{tac}}</li>
            </ol>
            
        </div>
        <br />
        
        <div class="sub-title">Voucher Notes</div>
        <div ng-show="DATA.notes">
        	<br />
            <ol ng-show="DATA.notes.list">
                <li ng-repeat='note in DATA.notes.list'>{{note}}</li>
            </ol>
            <div ng-show="DATA.notes.text">
                <p>{{DATA.notes.text}}</p>
            </div> 
        </div>
        <div>
	      
        </div>
        <div ng-if="advanced_setting.f_is_cancellation && advanced_setting.f_is_cancellation == 1">
            <script type="text/javascript">
                is_cancellation = true;   
            </script>
        </div>
        <div style="margin-top: 20px;" class="sub-title">BOOKING RESCHEDULE &amp; CANCELLATION POLICIES</div>
        <div ng-show="rules">
            <table class="table form-inline table-hover table-revisement">
                    <tr>
                        <td width="140">Enable</td>
                        <td width="10">:</td>
                        <td>
                            <div class="inline-edit">
                                <div class="main-text">
                                    
                                    <span class="value">
                                        <strong class="label-status-revisement" ng-show="is_cancellation">Yes</strong>
                                        <strong class="label-status-revisement" ng-show="!is_cancellation">No</strong>

                                        <table ng-show="is_cancellation" class="table table-condensed table-hover table-revisement-days" style="width:auto;margin:0;width:100%;">
                                            <thead>
                                                <tr class="successs">
                                                    <th>Cancellation prior to arrival</th>
                                                    <th>Cancellation Fee</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="rule in rules" class="tr-available-date">
                                               
                                                    <td>
                                                        <span class="st">{{min[$index]}}{{num(rule.f_min_activation_days, $index)}}</span> - {{rule.f_min_activation_days}} 
                                                        <span> days before scheduled arrival</span>
                                                    </td>
                                                    <td>{{rule.f_percentage}} %</td>
                                                    <!--  {{min + $index}} -->
                                                </tr>
                                                 
                                            </tbody>
                                        </table>
                                
                                    </span>
                                </div>
                            </div>
                        </td>
                    </tr> 
                </table>
            </div>
            <div>
        </div>
    </div>
    

</div>

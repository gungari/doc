<div class="sub-title">
	<span ng-show='!DATA.current_agent.agent_code'><i class="fa fa-plus-square" aria-hidden="true"></i> Add New</span> 
	<span ng-show='DATA.current_agent.agent_code'><i class="fa fa-pencil" aria-hidden="true"></i> Edit</span>
	Business Source
</div>

<div ng-init="agentAddEdit();">
	<div class="products">
		<div>
			<div ng-show='!DATA.current_agent.ready'>
				<img src="<?=base_url("public/images/loading_bar.gif")?>" />
			</div>
				
			<form ng-submit="saveDataAgent($event)" ng-show='DATA.current_agent.ready'>
				<div ng-show='DATA.current_agent.error_desc.length>0' class="alert alert-danger show_error"><ul><li ng-repeat='err in DATA.current_agent.error_desc'>{{err}}</li></ul></div>
				<table class="table table-borderlesss">
					<tr>
						<td width="150">Name*</td>
						<td>
							<input type="text" class="form-control" placeholder="Business Source Name" required="required" ng-model='DATA.current_agent.name' />
						</td>
					</tr>
					
					<tr>
						<td>Phone*</td>
						<td>
							<input type="text" class="form-control" placeholder="Phone" required="required" ng-model='DATA.current_agent.phone' />
						</td>
					</tr>
					<tr>
						<td>Website</td>
						<td>
							<input type="url" class="form-control" placeholder="Website" ng-model='DATA.current_agent.website' />
						</td>
					</tr>
					<tr>
						<td>Address</td>
						<td>
							<input type="text" class="form-control" placeholder="Address" ng-model='DATA.current_agent.address' />
						</td>
					</tr>
					<tr>
						<td>Country*</td>
						<td>
							<select class="form-control" ng-model='DATA.current_agent.country_code' style="width:250px" required="required">
								<option value="">-- Country --</option>
								<option value="{{country.code}}" ng-repeat="country in DATA.country_list.country_list | orderBy : 'name'">{{country.name}}</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>PIC</td>
						<td>
							<input type="text" class="form-control" placeholder="Name" ng-model='DATA.current_agent.contact_person.name' />
						</td>
					</tr>
				</table>
				<div class="sub-title">Category</div>
				<table class="table table-borderlesss">
					<tr>
						<td width="150">Category*</td>
						<td>
							<select class="form-control" ng-model='DATA.current_agent.category_code'>
								<option value="" disabled="disabled">-- Category --</option>
								<option value="{{category.category_code}}" ng-repeat="category in DATA.categories.categories | orderBy : 'name'">{{category.name}}</option>
							</select>
						</td>
					</tr>
				</table>
				<div class="sub-title">Agent Payment Type</div>
				<table class="table table-borderlesss">
					<tr>
						<td width="150">Agent Payment Type*</td>
						<td>
							<select class="form-control" ng-model='DATA.current_agent.payment_method_code'>
								<option value="" disabled="disabled">-- Agent Payment Type --</option>
								<option value="ACL">Agent Credit Limit</option>
								<option value="DEPOSIT">Deposit Payment</option>
								<option value="REGULAR">Regular</option>
								<?php /*?><option value="COTG">Commission On The Go</option><?php */?>
							</select>
						</td>
					</tr>
					<tr ng-show="DATA.current_agent.payment_method_code == 'ACL'">
						<td>Credit Limit*</td>
						<td>
							<input type="number" ng-required="DATA.current_agent.payment_method_code == 'ACL'" class="form-control" min="0" ng-model='DATA.current_agent.credit_limit' placeholder="Credit Limit"/>
						</td>
					</tr>
				</table>
				<?php /*?><div class="sub-title">Contact Person</div>
				<table class="table table-borderlesss">
					<tr>
						<td width="150">Name*</td>
						<td>
							<input type="text" class="form-control" placeholder="Name" required="required" ng-model='DATA.current_agent.contact_person.name' />
						</td>
					</tr>
					<tr>
						<td>Email*</td>
						<td>
							<input type="email" class="form-control" placeholder="Email" required="required" ng-model='DATA.current_agent.contact_person.email' />
						</td>
					</tr>
					<tr>
						<td>Phone*</td>
						<td>
							<input type="text" class="form-control" placeholder="Phone" required="required" ng-model='DATA.current_agent.contact_person.phone' />
						</td>
					</tr>
				</table><?php */?>
				<div class="sub-title">Remarks</div>
				<table class="table table-borderlesss">
					<tr>
						<td width="150"></td>
						<td>
							<textarea class="form-control autoheight" placeholder="Remarks" ng-model='DATA.current_agent.remarks' rows="3"></textarea>
						</td>
					</tr>
				</table>
				
				<table class="table table-borderlesss">
					<tr>
						<td width="150"></td>
						<td>
							<button type="submit" class="btn btn-primary">Save</button>
							&nbsp;&nbsp;&nbsp;
							<a ng-show='!DATA.current_agent.agent_code' ui-sref="agent"><strong>Cancel</strong></a>
							<a ng-show='DATA.current_agent.agent_code' ui-sref="agent.detail({'agent_code':DATA.current_agent.agent_code})"><strong>Cancel</strong></a>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>

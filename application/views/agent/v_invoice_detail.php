<h1>INVOICES</h1>
<div class="sub-title"> Invoice Detail </div>
<br />
<div ng-init="loadDataInvoiceDetail(true)" class="invoice-detail">

	<div ng-show='!(DATA.current_invoice)'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>

	<div ng-show='(DATA.current_invoice)'>
		<div class="pull-right">
			<div class="btn-group">
			  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fa fa-bars" aria-hidden="true"></i> Menu <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu">
				<li>
					<a href="<?=site_url("home/print_page/#/print/invoice_agent/")?>{{DATA.current_invoice.invoice.invoice_code}}" target="_blank" >
					<i class="fa fa-print" aria-hidden="true"></i> Print
					</a>
				</li>
				
			  </ul>
			</div>
		</div>
		<div class="title">
			<h1>#{{DATA.current_invoice.invoice.invoice_code}} - {{DATA.current_invoice.invoice.agent_name}}</h1>
			<div class="code"></div>
		</div>

		<ul class="nav nav-tabs sub-nav">
			<li role="presentation" class="detail active"><a ui-sref="invoice_detail({'invoice_code':DATA.current_invoice.invoice.invoice_code})">Detail</a></li>
			<li role="presentation" class="payment"><a ui-sref="invoice_payment({'invoice_code':DATA.current_invoice.invoice.invoice_code})">Payment</a></li>
		</ul>
		<br /><br />
		<div ui-view>
			<table class="table">
				<tr>
					<td width="120">Invoice code</td>
					<td><strong>{{DATA.current_invoice.invoice.invoice_code}}</strong></td>
					<td>Invoice date</td>
					<td><strong>{{fn.formatDate(DATA.current_invoice.invoice.invoice_date, "d MM yy")}}</strong></td>
				</tr>
				<tr>
					<td>Created by</td>
					<td><strong>{{DATA.current_invoice.invoice.invoice_creby}} on {{DATA.current_invoice.invoice.invoice_creon}}</strong></td>
					<td>Due date</td>
					<td><strong>{{fn.formatDate(DATA.current_invoice.invoice.invoice_due, "d MM yy")}}</strong></td>
				</tr>
				<tr ng-show="DATA.current_invoice.invoice.agent_code">
					<td>Business Source</td>
					<td colspan="3">
						<strong>{{DATA.current_invoice.invoice.agent_code}} - {{DATA.current_invoice.invoice.agent_name}}</strong><br />
						<small style="font-size: 96%">{{DATA.current_invoice.invoice.agent_addr}}, ph. {{DATA.current_invoice.invoice.agent_cont.phone}}, em. {{DATA.current_invoice.invoice.agent_cont.email}}</small>
					</td>
				</tr>
			</table>	
			<br />
			
			<table class="table table-bordered">
				<tr class="header bold">
					<td width="120">Booking#</td>
					<td>Customer</td>
					<td width="130" align="right">Subtotal</td>
					<td width="120" align="right">Discount</td>
					<td width="120" align="right">Already paid</td>
					<td width="120" align="right">Total</td>
				</tr>
				<tbody>
					<tr ng-repeat="invoice_detail in DATA.current_invoice.invoice.detail">
						<td>
							<a ui-sref="detail_reservation({'booking_code':invoice_detail.booking_code})" target="_blank"><strong>{{invoice_detail.booking_code}}</strong></a>
                            <br />
                            {{fn.formatDate(invoice_detail.reservation_date, "d M yy")}}
						</td>
						<td>{{invoice_detail.customer.first_name}} {{invoice_detail.customer.last_name}}</td>
						<td align="right">{{invoice_detail.currency}} {{fn.formatNumber(invoice_detail.subtotal, invoice_detail.currency)}}</td>
						<td align="right">{{invoice_detail.currency}} {{fn.formatNumber(invoice_detail.discount, invoice_detail.currency)}}</td>
						<td align="right">{{invoice_detail.currency}} {{fn.formatNumber(invoice_detail.total_payment, invoice_detail.currency)}}</td>
						<td align="right">{{invoice_detail.currency}} {{fn.formatNumber(invoice_detail.grandtotal, invoice_detail.currency)}}</td>
					</tr>
				</tbody>
				<tfoot>
					<tr class="success">
						<td colspan="5" align="right"><strong>Grand Total</strong></td>
						<td align="right"><strong>{{DATA.current_invoice.invoice.invoice_curr}} {{fn.formatNumber(DATA.current_invoice.invoice.invoice_total, DATA.current_invoice.invoice.invoice_curr)}}</strong></td>
					</tr>
					<tr ng-class="{'danger':(DATA.current_invoice.invoice.balance>0), 'info':(DATA.current_invoice.invoice.balance<=0)}">
						<td colspan="5" align="right"><strong>Total Payment</strong></td>
						<td align="right">
							<strong>{{DATA.current_invoice.invoice.invoice_curr}} ({{fn.formatNumber(DATA.current_invoice.invoice.total_payment, DATA.current_invoice.invoice.invoice_curr)}})</strong>
						</td>
					</tr>
					<tr ng-class="{'danger':(DATA.current_invoice.invoice.balance>0), 'info':(DATA.current_invoice.invoice.balance<=0)}">
						<td colspan="5" align="right"><strong>Balance</strong></td>
						<td align="right"><strong>{{DATA.current_invoice.invoice.invoice_curr}} {{fn.formatNumber(DATA.current_invoice.invoice.balance, DATA.current_invoice.invoice.invoice_curr)}}</strong></td>
					</tr>
				</tfoot>
				
						
			</table>
			
			<br />
			You may pay the invoices as following method : 
			<br />
			<div style="margin-top: 20px;">
				<table>

					<tbody ng-repeat="data in DATA.current_invoice.detail">
					
						<tr>
							<td width="40">{{$index+1}}.   </td>
							<td><b>Wire Transfer</b></td>
						</tr>
						<tr>
							<td></td>
							<td>{{data.account_name}}</td>
						</tr>
						<tr>
							<td></td>
							<td>Account : {{data.account_number}}</td>
						</tr>
						<tr>
							<td></td>
							<td>{{data.bank_name}}</td>
						</tr>	
						<tr>
							<td></td>
							<td>{{data.branch}}</td>
						</tr>	
						<tr>
							<td></td>
							<td>Please email the transfer receipt to <a href="mailto:<?=$vendor['email']?>"><?=$vendor["email"]?></a> once the payment has been made.</td>
						</tr>	
						
					</tbody>	
				</table>
			</div>	
			<br />
			
			<table class="table" ng-show="DATA.current_invoice.invoice.remarks">
				<tr class="header bold">
					<td>Remarks</td>
				</tr>
				<tr>
					<td><pre style='font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;font-size: 13px;padding: 0;margin: 0;border: 0;background-color: transparent;line-height: 1;padding-bottom: 10px'>{{DATA.current_invoice.invoice.remarks}}</pre></td>
				</tr>
			</table>
			<hr />
			<a ui-sref="invoices"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back to list</a>
			<a class="pull-right" data-ng-click="loadDataInvoiceDetail(true)" style="cursor: pointer"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh</a>
			<!-- <script>activate_sub_menu_invioce_detail("detail");</script>
			<script>GeneralJS.activateLeftMenu("invoice");</script> -->
		</div>
	</div>
</div>
<!-- modal add payment -->	
<div class="modal fade" id="edit_bank_account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <form ng-submit='saveBankAccount($event,send_this_to_email)'>
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="myModalLabel">
			Edit Bank Account
		</h4>
	  </div>
	  <div class="modal-body">
	  	<div ng-show='DATA.bank_account_inv.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.bank_account_inv.error_msg'>{{err}}</li></ul></div>
		<table class="table table-borderless table-condenseds">
			<tr class="modal_pp_show">
				<td colspan="2"><strong>Current Bank Account</strong></td>
			</tr>
			<tr class="modal_pp_show" ng-show="DATA.bank_account_inv.bank_account.bank_name">
				<td width="130">Bank Name</td>
				<td><strong>{{DATA.bank_account_inv.bank_account.bank_name}}</strong></td>
			</tr>
			<tr class="modal_pp_show" ng-show="DATA.bank_account_inv.bank_account.bank_name">
				<td width="130">Account Number</td>
				<td><strong>{{DATA.bank_account_inv.bank_account.account_number}}</strong></td>
			</tr>
			<tr class="modal_pp_show" ng-show="DATA.bank_account_inv.bank_account.bank_name">
				<td width="130">Account Name</td>
				<td><strong>{{DATA.bank_account_inv.bank_account.account_name}}</strong></td>
			</tr>
			<tr class="modal_pp_show" ng-show="DATA.bank_account_inv.bank_account.bank_name">
				<td width="130">Branch</td>
				<td><strong>{{DATA.bank_account_inv.bank_account.branch}}</strong></td>
			</tr>
			<tr class="modal_pp_show" ng-show="DATA.bank_account_inv.bank_account && !DATA.bank_account_inv.bank_account.bank_name">
				<td colspan="2">
					<div class="alert alert-danger">
						Bank Account Not Set!
					</div>
				</td>
			</tr>
			<tr class="modal_pp_show">
				<td colspan="2"><br/></td>
			</tr>
			<tr class="modal_pp_show">
				<td colspan="2" align="center">
					<button ng-show="send_this_to_email===1 && DATA.bank_account_inv.bank_account && !DATA.bank_account_inv.bank_account.bank_name" class="btn btn-warning"><i class="fa fa-send" aria-hidden="true"></i> Send without Bank account</button>
					<button type="button" class="btn btn-primary" onclick="$('.modal_pp_show').hide();$('.modal_pp_hide').show();"><i class="fa fa-pencil"></i> Change Bank account</button>
				</td>
			</tr>
			<tr class="modal_pp_hide" style="display:none">
				<td colspan="2"><strong>Change Bank Account</strong></td>
			</tr>
			<tr class="modal_pp_hide" style="display:none">
				<td width="130">Bank Name</td>
				<td>
					<select class="form-control input-sm" ng-model="DATA.bank_account_inv.bank_account.id">
						<option value="">-- Select Bank Account --</option>
						<option ng-show="bank.id" ng-repeat="bank in DATA.rekening" value="{{bank.id}}">{{bank.bank_name+" - "+bank.account_number}}</option>
					</select>
				</td>
			</tr>
			<tr class="modal_pp_hide" style="display:none">
				<td width="130"></td>
				<td>
					<button class="btn btn-primary">Save</button> 
					<button class="btn btn-default" type="button" onclick="$('.modal_pp_show').show();$('.modal_pp_hide').hide();">Cancel</button>
				</td>
			</tr>
		</table>
	  </div>
	</div>
  </div>
  </form>
</div>
<style>
	.invoice-detail .title{margin-bottom:20px}
	.invoice-detail .title h1{margin-bottom:10px !important;}
	.invoice-detail .title .code{margin-bottom:5px;}
</style>



<div ng-init="loadDataAgentDetail();" class="agent-detail">

	<div ng-show='!(DATA.current_agent)'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>

	<div ng-show='(DATA.current_agent)'>
		<div class="title">
			<h1>{{DATA.current_agent.name}}</h1>
			<div class="code"> Code : {{DATA.agent_code}}</div>			
		</div>
		<ul class="nav nav-tabs sub-nav profile">
			<li role="presentation" class="profile"><a ui-sref="profile">Profile</a></li>
			<li role="presentation" class="terms_and_condition"><a ui-sref="profile.term_and_condition">Terms And Conditions</a></li>
			<?php /*?><li role="presentation" class="contract-rates"><a ui-sref="agent.detail.contract_rates">Contract Rates</a></li><?php */?>
		</ul>
		<br />
		<div ui-view>
		<script>activate_sub_menu_agent_detail("profile");</script> 
			<div class="sub-title">Profile</div>
			<div ng-show='DATA.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.error_msg'>{{err}}</li></ul></div>
			<table class="table">
				<tr>
					<td width="160">Name</td>
					<td>
						<div class="inline-edit">
							<div class="main-text">
								<span class="inline-edit-icon pull-right"><a style="cursor: pointer;"><span class="glyphicon glyphicon-pencil"></span>Edit</a></span>
								<strong>{{DATA.current_agent.name}}</strong>
							</div>
							<div class="edit-text hidden-field">
								<form method="post" ng-submit="save_profile($event, 'name')">
									<div class="cancel pull-right"><a style="cursor: pointer;">Cancel</a></div>
									<div class="input-group">
										<input type="text" required="required" class="form-control input-sm" ng-model="DATA.current_agent.name" maxlength="100" />
										<span class="input-group-btn">
											<button class="btn btn-info btn-sm" type="submit">Save</button>
										</span>
									</div>
								</form>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td>Code</td>
					<td><strong>{{DATA.agent_code}}</strong></td>
				</tr>
				<tr>
					<td>Phone</td>
					
					<td>
						<div class="inline-edit">

							<div class="main-text">
								<span class="inline-edit-icon pull-right"><a style="cursor: pointer;"><span class="glyphicon glyphicon-pencil"></span>Edit</a></span>
								<strong>{{DATA.current_agent.phone}}</strong>
							</div>
							<div class="edit-text hidden-field">
								<form method="post" ng-submit="save_profile($event, 'phone')">
									<div class="cancel pull-right"><a style="cursor: pointer;">Cancel</a></div>
									<div class="input-group">

										<?php /*<select class="form-control input-sm" ng-model='phone'>
											<option value="">-- Select Country --</option>
											<option value="{{phone.code}}" ng-repeat='phone in code_phone'>({{phone.dial_code}}) {{phone.name}}</option>
										</select> */?>
										<input type="text" required="required" class="form-control input-sm" ng-model="DATA.current_agent.phone" maxlength="100" />
										<span class="input-group-btn">
											<button class="btn btn-info btn-sm" type="submit">Save</button>
										</span>
									</div>
								</form>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td>Website</td>
					
					<td>
						<div class="inline-edit">
							<div class="main-text">
								<span class="inline-edit-icon pull-right"><a style="cursor: pointer;"><span class="glyphicon glyphicon-pencil"></span>Edit</a></span>
								<strong>{{DATA.current_agent.website}}</strong>
							</div>
							<div class="edit-text hidden-field">
								<form method="post" ng-submit="save_profile($event, 'website')">
									<div class="cancel pull-right"><a style="cursor: pointer;">Cancel</a></div>
									<div class="input-group">
										<input type="text" required="required" class="form-control input-sm" ng-model="DATA.current_agent.website" maxlength="100" />
										<span class="input-group-btn">
											<button class="btn btn-info btn-sm" type="submit">Save</button>
										</span>
									</div>
								</form>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td>Address</td>
					
					<td>
						<div class="inline-edit">
							<div class="main-text">
								<span class="inline-edit-icon pull-right"><a style="cursor: pointer;"><span class="glyphicon glyphicon-pencil"></span>Edit</a></span>
								<strong>{{DATA.current_agent.address}}</strong>
							</div>
							<div class="edit-text hidden-field">
								<form method="post" ng-submit="save_profile($event, 'address')">
									<div class="cancel pull-right"><a style="cursor: pointer;">Cancel</a></div>
									<div class="input-group">
										<input type="text" required="required" class="form-control input-sm" ng-model="DATA.current_agent.address" maxlength="100" />
										<span class="input-group-btn">
											<button class="btn btn-info btn-sm" type="submit">Save</button>
										</span>
									</div>
								</form>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td>Country</td>
					<td>
						<div class="inline-edit">
						<div class="main-text">
							<span class="inline-edit-icon pull-right"><a style="cursor: pointer;"><span class="glyphicon glyphicon-pencil"></span>Edit</a></span>
							<strong>{{DATA.current_agent.country.name}}</strong>
						</div>
						<div class="edit-text hidden-field">
							<form method="post" ng-submit="save_profile($event,'country')">
								<div class="cancel pull-right"><a style="cursor: pointer;">Cancel</a></div>
								<div class="input-group">
									<select class="form-control input-sm" ng-model='DATA.current_agent.country_code'>
										<option value="">-- Select Country --</option>
										<option value="{{country.code}}" ng-repeat='country in DATA.country_list'>{{country.name}}</option>
									</select>
									<span class="input-group-btn">
										<button class="btn btn-info btn-sm" type="submit">Save</button>
									</span>
								</div>
							</form>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td>PIC</td>
					
					<td>
						<div class="inline-edit">
							<div class="main-text">
								<span class="inline-edit-icon pull-right"><a style="cursor: pointer;"><span class="glyphicon glyphicon-pencil"></span>Edit</a></span>
								<strong>{{DATA.current_agent.contact_person.name}}</strong>
							</div>
							<div class="edit-text hidden-field">
								<form method="post" ng-submit="save_profile($event,'contact_person')">
									<div class="cancel pull-right"><a style="cursor: pointer;">Cancel</a></div>
									<div class="input-group">
										<input type="text" required="required" class="form-control input-sm" ng-model="DATA.current_agent.contact_person.name" maxlength="100" />
										<span class="input-group-btn">
											<button class="btn btn-info btn-sm" type="submit">Save</button>
										</span>
									</div>
								</form>
							</div>
						</div>
					</td>
				</tr>
			</table>	
			<br />
		
		<div class="sub-title">Account Login</div>
		<?php /* <!-- <table class="table table-hover" ng-init="loaddataChangePwd('<?=$email_user?>','<?=$code_user?>')"> -->*/ ?>
		<table class="table table-hover">
			<tr>
				<td width="160">Email</td>
				
				<td>
					<div class="inline-edit">
						<div class="main-text">
							<span class="inline-edit-icon pull-right"><a style="cursor: pointer;"><span class="glyphicon glyphicon-pencil"></span>Edit</a></span>
							<strong>{{DATA.current_agent.email}}</strong>
						</div>
						<div class="edit-text hidden-field">
							<form method="post" ng-submit="save_profile($event,'email')">
								<div class="cancel pull-right"><a style="cursor: pointer;">Cancel</a></div>
								<div class="input-group">
									<input type="text" required="required" class="form-control input-sm" ng-model="DATA.current_agent.email" maxlength="100" />
									<span class="input-group-btn">
										<button class="btn btn-info btn-sm" type="submit">Save</button>
									</span>
								</div>
							</form>
						</div>
					</div>
				</td>
			</tr>
		
			<tr>
				<div ng-show='DATA.error_pwd.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.error_pwd'>{{err}}</li></ul></div>
				<td>Password</td>
				
				<td>
					<div class="inline-edit">
						<div class="main-text">
							<span class="inline-edit-icon" style="display:inherit; font-size:12px"><a style="cursor: pointer;"><span class="glyphicon glyphicon-pencil"></span> Change Password</a></span>
						</div>
						<div class="edit-text hidden-field">
							<form method="post" ng-submit="change_password($event)">
								<div style="background:#FAFAFA; padding:10px" class="contact-edit">
								    <table class="table" style="width:100%">
								        <tr>
								            <td width="150">Old Password</td>
								            <td><input ng-model="DATA.dataPwd.old_pwd" class="form-control input-sm" type="password"> </td>
								        </tr>
								        <tr>
								            <td>New Password</td>
								            <td><input ng-model="DATA.dataPwd.new_pwd" class="form-control input-sm" type="password"> </td>
								        </tr>
								        <tr>
								            <td>Retype New Password</td>
								            <td><input ng-model="DATA.dataPwd.ret_pwd" class="form-control input-sm" type="password"> </td>
								        </tr>
								        <tr>
								            <td></td>
								            <td>
								                <button class="btn btn-info btn-sm" type="submit">Save</button>
								                <span class="cancel"><a style="cursor: pointer">Cancel</a></span>
								            </td>
								        </tr>
								    </table>
								</div>
							</form>
						</div>
					</div>
				</td>
			</tr>
		</table>
			<?php /* <!-- <div class="sub-title">Category</div>	
			<table class="table">
				<tr>
					<td width="160">Category</td>
					<td><strong>{{DATA.current_agent.category.name}}</strong></td>
				</tr>
				<tr ng-show="DATA.current_agent.category.description != ''">
					<td>Description</td>
					<td><strong>{{DATA.current_agent.category.description}}</strong></td>
				</tr>
				<tr ng-show="DATA.current_agent.category.percentage">
					<td>Contract Rates</td>
					<td><strong>{{DATA.current_agent.category.percentage}}%</strong></td>
				</tr>
			</table>
			<br /> --> */?>
			<br />
			<!-- <div class="sub-title">Agent Payment Type</div> -->
			<table class="table table-borderlesss">
				<tr>
					<td width="160">Agent Payment Type</td>
					<td><strong>{{DATA.current_agent.payment_method.description}}</strong></td>
				</tr>
				
				<tr ng-show="DATA.current_agent.payment_method.payment_code == 'ACL'">
					<td width="160">Credit Limit</td>
					<td><strong>{{DATA.out_standing_invoice.currency}} {{fn.formatNumber(DATA.out_standing_invoice.credit_limit, DATA.out_standing_invoice.currency)}}</strong></td>
				</tr>

				<tr ng-show="DATA.current_agent.payment_method.payment_code == 'ACL'">
					<td width="160">Uninvoicing</td>
					<td><strong>{{DATA.out_standing_invoice.currency}} {{fn.formatNumber(DATA.uninvoicing, DATA.out_standing_invoice.currency)}}</strong></td>
				</tr>
			
				<tr ng-show="DATA.current_agent.payment_method.payment_code == 'ACL'">
					<td width="160">Credit Remaining</td>
					<td><strong>{{DATA.out_standing_invoice.currency}} {{fn.formatNumber(DATA.out_standing_invoice.current_limit, DATA.out_standing_invoice.currency)}}</strong></td>
				</tr>
				<tr ng-show='DATA.deposit.current_deposit'>
					<td>Deposit Remaining </td>
					<td><strong>{{DATA.deposit.currency}} {{fn.formatNumber(DATA.deposit.current_deposit, DATA.deposit.currency)}}</strong></td>
				</tr>
			</table>
			<br />
			<div class="sub-title">Remarks</div>		
			<table class="table">
				<tr>
					<td width="160"></td>
					<td><span ng-show="!DATA.current_agent.remarks">-</span>{{DATA.current_agent.remarks}}</td>
				</tr>
			</table>
			<?php /*
			<!-- <br /><hr />
			<a ui-sref="profile.edit({'agent_code':DATA.current_agent.agent_code})"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Business Source</a> -->
			<!-- <script>activate_sub_menu_agent_detail("profile");</script> --> */ ?>
		</div>
	</div>
</div>
<script>activate_sub_menu_agent_detail("profile");</script>
<style>
	.agent-detail .title{margin-bottom:20px}
	.agent-detail .title h1{margin-bottom:10px !important;}
	.agent-detail .title .code{margin-bottom:5px;}
</style>

<!-- <script>activate_sub_menu_agent_detail("profile");</script> -->
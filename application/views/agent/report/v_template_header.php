<style type="text/css">
	.tr-muted {
		color: #ccc;
	}
	ul li.title {
		margin-top: 10px;
	}
	ul li.title a {
		padding: 5px;
		font-weight: bold;
	}
	a.muted-link {
		text-decoration: inherit;
		color: inherit;
	}
	a.muted-link:hover {
		text-decoration: inherit;
		color: #1B77C0;
	}
	.report-content table th, .report-content table td {
		vertical-align: middle !important;
	}
	.report-content .report-margin {
		margin:10px auto;
	}
	.report-content .report-border {
		border: #ccc solid 0.5px;;
	}
	.report-content table .header, .report-content table .footer {
		background-color: #eee;
		font-weight: bold;
	}
</style>

<?php if(isset($breadcrumbs)) : ?>
	<div class="unprint" id="bread">
		<ol class="breadcrumb">
			<?php foreach($breadcrumbs as $breadcrumb) : ?>
				<?php if($breadcrumb['li_class']==='active') : ?>
					<li class="<?=$breadcrumb['li_class'];?>">
						<?=$breadcrumb['title'];?>
					</li>
				<?php else : ?>
					<li class="<?=$breadcrumb['li_class'];?>">
						<a href="<?=$breadcrumb['url'];?>"><?=$breadcrumb['title'];?></a>
					</li>
				<?php endif; ?>
			<?php endforeach; ?>
		</ol>
	</div>
<?php endif; ?>

<div class="btn-group pull-right unprint">
	<button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background:#1B77C0; border-color:#1B77C0">
		<span class="fa fa-pie-chart"></span> Select Report
	</button>
	<ul class="dropdown-menu">
		<li class="title"><a href="<?=site_url('extranet/reports');?>">Reports</a></li>
		<li role="separator" class="divider"></li>
		<li class="title"><a href="#">Visitor</a></li>
		<li class=""><a href="<?=site_url("extranet/reports/visitor")?>">General</a></li>
		<li class=""><a href="<?=site_url("extranet/reports/visitor?type=country")?>">By Country</a></li>
		<li class=""><a href="<?=site_url("extranet/reports/visitor?type=browser")?>">By Browser</a></li>
		<li class=""><a href="<?=site_url("extranet/reports/visitor?type=platform")?>">By OS</a></li>
		<li class=""><a href="<?=site_url("extranet/reports/visitor?type=device")?>">By Device</a></li>
		<li class=""><a href="<?=site_url("extranet/reports/visitor/conversion")?>">Conversion</a></li>
		<li class=""><a href="<?=site_url('extranet/reports/visitor/view')?>"><?=ucwords($product_type)?> View</a></li>
		<li class="title"><a href="#"><?=ucwords($booking_text)?></a></li>
		<li class=""><a href="<?=site_url("extranet/reports/$booking_text")?>">General</a></li>
		<li class=""><a href="<?=site_url("extranet/reports/$booking_text/leadtime")?>">Lead Time</a></li>
		<li class=""><a href="<?=site_url("extranet/reports/$booking_text/nationality")?>">By Nationality</a></li>
		<li class=""><a href="<?=site_url("extranet/reports/$booking_text/country")?>">By Country IP's</a></li>
		<li class=""><a href="<?=site_url("extranet/reports/$booking_text/payment")?>">By Payment Type</a></li>
		<?php /* <li class=""><a href="<?=site_url("extranet/reports/$booking_text/expected_arrival")?>">Expected Arrival</a></li>
		<?php if($vendor['RegisteredCategory']['f_id_vendor_category']===SYSTEM_BASIC_SETTING('EXTRANET_TYPE_ACCOMMODATION')) : ?>
			<li class=""><a href="<?=site_url("extranet/reports/$booking_text/expected_departure")?>">Expected Departure</a></li>
		<?php endif; */?>
		<li class="title"><a href="<?=site_url("extranet/reports/revenue")?>">Revenue</a></li>
		<?php if($vendor['RegisteredCategory']['f_is_commission_base']) : ?>
			<li class=""><a href="<?=site_url("extranet/reports/revenue/commission")?>">Commission</a></li>
		<?php endif; ?>
		<?php if(in_array('HYBRID', array_column($vendor['PaymentGateway'], 'f_payment_type'))) : ?>
			<li class=""><a href="<?=site_url("extranet/reports/revenue/hybrid_payment")?>">Hybrid Integrated Payment</a></li>
			<li class=""><a href="<?=site_url("extranet/reports/revenue/refund")?>">Refund</a></li>
		<?php endif; ?>
	</ul>
</div>

<h1 class="unprint"><?=$title;?></h1>
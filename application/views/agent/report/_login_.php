<!DOCTYPE html>
<html>
  <head>
  	<title>CRS - <?=$this->my_config->company_name?></title>
    <?php $this->load->view("template/crs/_include_js_css"); ?>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  </head>
  <body class='login' style="background-color:#eeeeee; background-image:url(<?=base_url("public/admin_clean/login-left-corner.png")?>); background-repeat: no-repeat;">
	
    <div style="text-align:center; margin-top:100px">
	    <img src="<?=base_url("public/images/hybrid-png.png")?>" width="300px" />
    </div>
    
	<div id="MainContainer" align="center" style="background:#FFF; padding:20px 10px; box-shadow:0 0 0 #CCCCCC; border-radius: 5px;"> 
   <?php /*?> <a href="<?=site_url()?>"><img src="<?=base_url($this->my_config->company_logo)?>" alt="<?=$this->my_config->company_name?>" border="0" /></a><br /><br />
	<div id="DivSeparator" style="width:100%; border-color:#999"></div><?php */?>
    
    <?php if (isset($recover_password)){ ?>
	<style>#login{display:none}</style>
    <div id="recover_password">
        <h3 class="title-page">Recover Your Password</h3>
        <hr />
        <form action="<?=site_url("home/update_recover_password")?>" method="post" id="frm_change_password">
            <input type="hidden" name="f_validation_forgot_pass" value="<?=$vendor["f_validation_forgot_pass"]?>" />
            <div class="show_error hidden-field" style="text-align:left"></div>
            <table border="0" align="center" cellpadding="3" cellspacing="0" class="table table-borderless">
                <tr>
                    <td width="150">Full Name</td>
                    <td><strong><?=@$vendor["f_company"]?><?=@$vendor["f_name"]?></strong></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><strong><?=$vendor["f_email"]?></strong></td>
                </tr>
                <tr>
                    <td>New Password</td>
                    <td><input type="password" name="new_password" class="form-control input-sm" /> </td>
                </tr>
                <tr>
                    <td>Retype New Password</td>
                    <td><input type="password" name="new_password_2" class="form-control input-sm" /> </td>
                </tr>
                <tr>
                    <td></td>
                    <td><button type="submit" class="btn btn-success">Update Password</button></td>
                </tr>
            </table>
        </form>
        <hr />
        <a href="<?=site_url("home")?>">Login</a>
    </div>
    <script>
        $("#frm_change_password").submit(function(){
            var mydata = $(this).serialize();
            var form = $(this);
            $.ajax({
                type: "POST",
                url: form.attr("action")+"/ajax",
                data: mydata,
                beforeSend : function(){
                    form.find(".show_error").slideUp().html("");
                    form.find("button[type=submit]").attr("disabled","disabled");
                },
                success: function(response, textStatus, xhr) {
                    if ($.trim(response) == "00"){
                        form.find(".show_error").hide().html("<div class='alert alert-success'><strong>Success</strong>. Your password is successfully updated.</div>").slideDown("fast");
                        form.find("button[type=submit]").removeAttr("disabled");
                        form.find("input[type=password]").val("");
                    }else{
                        form.find(".show_error").hide().html(response).slideDown("fast");
                        form.find("button[type=submit]").removeAttr("disabled");
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    form.find("button[type=submit]").removeAttr("disabled");
                }
            });
            return false;
        });
    </script>
    <?php } ?>
    
    <div id="login">
    
        <form id="frm_login" name="form1" method="post" action="<?=site_url("login/checklogin/".(isset($_GET["redirect"])?"?redirect=$_GET[redirect]":""))?>">
            <h3 style="margin-top:0">Sign in to your Extranet Page </h3>
            Please enter your email and password.
			
			<?php if (isset($err_login) && is_array($err_login) && count($err_login) > 0){ ?>
			<br><br>
			<div class="alert alert-danger text-left">
				<strong>Sorry..</strong><br /><br />
				<ul style="padding-left:20px">
					<li><?=implode("</li></li>", $err_login)?></li>
				</ul>
			</div>
			<?php } ?>
			
            <hr style="margin:10px 0" />
          	<table border="0" align="center" cellpadding="5" cellspacing="0" width="90%" class="table table-borderless">
              <tr>
                <td>EMAIL</td>
                <td><input type="text" name="email" value="<?=(isset($_POST["email"]))?$_POST["email"]:""?>" id="f_email" class='form-control' placeholder='Type your email' />
                </td>
              </tr>
              <tr>
                <td>PASSWORD</td>
                <td><input type="password" name="password" id="f_password" class='form-control' placeholder='Type your password' />
                </td>
              </tr>
			  <tr>
                <td>CODE</td>
                <td><input type="text" name="code" value="<?=(isset($_POST["code"]))?$_POST["code"]:""?>" id="f_code" maxlength="3" style="width:100px" class='form-control' placeholder='Code' />
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>
                    <button type="submit" class='btn btn-info'><span class="glyphicon glyphicon-log-in"></span> Login</button>
                    <?php /*?><span style="color:#FFF">{elapsed_time} |  {memory_usage}</span><?php */?>
                    
                    <div class="pull-right" style="margin-top:15px"><a href="#" onClick="$('#frm_login').slideUp('fast'); $('#frm-forgot-password').slideDown('fast'); return false;">Forgot Password</a></div>
                </td>
              </tr>
              <tr>
                <td colspan="2" valign="top"></td>
              </tr>
            </table>
        </form>
        
        <form action="<?=site_url("home/forgot_password")?>" method="post" class="ajax_frm_crud hidden-field" id="frm-forgot-password">
            <h3>Forgot your password?</h3>
            <p>Enter your email and we will send you a link to get your password back.</p>
            <div style="margin:auto" class="table-field">
                <div class="row tr-field">
                    <div class="col-xs-1"></div>
                    <div class="col-xs-10 td-field right">
                        <div class="show_error"></div>
                        <input class="form-control input-sm" type="email" required="required" name="email" placeholder="Type your email..." />
                        <br />
                        <button type="submit" class="btn btn-info">Submit</button>
                        <button type="button" class="btn btn-danger" onClick="$('#frm_login').slideDown('fast'); $('#frm-forgot-password').slideUp('fast');">Back</button>
                    </div>
                    <div class="col-xs-1"></div>
                </div>
            </div>
        </form>
	
    </div>
    </div>
	<script> $(document).ready(function(e){$("#f_email").focus();}); </script>

  </body>
</html>

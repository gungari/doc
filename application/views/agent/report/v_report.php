<div class="pull-right">
	<div class="dropdown">
		<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			<i class="fa fa-bars" aria-hidden="true"></i> Menu
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">

		<li><a ui-sref="reports.sales">Sales</a></li>
		
		<li><a ui-sref="reports.cancel">Cancellation</a></li>
		</ul>
	</div>
</div>

<h1>Report</h1>

<div ui-view ng-init="loadReportHome()">
	
	<hr />

		<div class="panel panel-primary">
			<div class="panel-heading">
				<label>SALES REPORT ( Last 30 Days )</label>
			</div>
			<div class="panel-body panel-chart">
				<div ng-show='show_loading_DATA'>
					<img src="<?=base_url("public/images/loading_bar.gif")?>" />
				</div> 
				<div ng-show="show_graph && !date_data && !show_error" class="report-content" style="display: block;">
					<dir class="embed-responsive embed-responsive-16by9 report-margin report-border">
						<div id="chartLine_home" style="width:100%; height:350px; margin: 0px auto;"></div>
					</dir>
					<span style="margin:0 15px;">Total Sales: <b>{{currency_home}} {{fn.formatNumber(total_home, currency_home)}}</b></span>
					<div class="pull-right">
						<a ui-sref="reports.sales"  target="_blank" class="btn btn-default btn-sm">More Detail</a>
					</div>
				</div>
			</div>
		</div>

		
		<div class="panel panel-primary">
			<div class="panel-heading">
				<label>CANCELATION REPORT ( Last 30 Days )</label>
			</div>
			<div class="panel-body panel-chart">
				<div ng-show='show_loading_home && !total_cancell'>
					<img src="<?=base_url("public/images/loading_bar.gif")?>" />
				</div>  
				<div ng-show="show_graph && !date_data && !show_error" class="report-content" style="display: block;">
					<dir class="embed-responsive embed-responsive-16by9 report-margin report-border">
						<div id="cancelLine_home" style="width:100%; height:350px; margin: 0px auto;"></div>
					</dir>
					<span>Total Cancel: <b>{{total_cancell}}</b></br> Amount: <b>{{currency_cancell}} {{fn.formatNumber(amount, currency_cancell)}}</b></span>
					<div class="pull-right">
						<a ui-sref="reports.cancel" target="_blank" class="btn btn-default btn-sm">More Detail</a>
					</div>
				</div>
			</div>
		</div>
</div>


<!-- <div ui-view ng-controller="report_home_controller" ng-init="testing()">
	<div id="chartLine" style="width:800px; height:350px; margin: 0px auto;">Loading...</div>
</div> -->
</body>
</html>
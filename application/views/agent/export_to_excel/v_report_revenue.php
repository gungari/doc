
<?php
if (!isset($_GET["TEST"])){

	$file="Revenue - ".$vendor["business_name"].".xls";
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=$file");

}
//pre($data["vendor"]);

?>

<?php if (isset($revenue)){ ?>

	<table border="1" cellspacing="1">
		<tr style="background:#DDD">
			<td rowspan="2">No</td>
			<td rowspan="2" align="center">Voucher#</td>
			<td rowspan="2" align="center">Date</td>
			<td rowspan="2">Customer Name</td>
			<td rowspan="2" align="center" title="Nationality">Nationality</td>
			<td rowspan="2" colspan="2" align="center">Trip</td>
			<td rowspan="2">Booking Source</td>
			<td colspan="3" align="center" title="Passenger">Passenger</td>
			<td colspan="3" align="center" title="Passenger">Price</td>
			<td rowspan="2" align="center">Pickup</td>
			<td rowspan="2" align="center">Dropoff</td>
			<td rowspan="2" align="center">Other</td>
			<td rowspan="2" align="center">Sub Total</td>
			<td rowspan="2" align="center">Discount</td>
			<td rowspan="2" align="center">Fee</td>
			<td rowspan="2" align="center">TOTAL</td>
			<td rowspan="2">Passengers</td>
		</tr>
		<tr style="background:#DDD">
			<td align="center">Adl.</td>
			<td align="center">Chi.</td>
			<td align="center">Inf.</td>
			<td align="center">Adl.</td>
			<td align="center">Chi.</td>
			<td align="center">Inf.</td>
		</tr>
		
		<?php foreach ($revenue as $revenue_data){ ?>
		<tr style="background:#FAFAFA">
			<td colspan="22">
				<strong>
					<?=$revenue_data["departure_port"]["name"]?> (<?=$revenue_data["departure_time"]?>)
					-
					<?=$revenue_data["arrival_port"]["name"]?> (<?=$revenue_data["arrival_time"]?>)
				</strong>
			</td>
		</tr>
		<?php foreach ($revenue_data["revenue"] as $index => $vcr){ ?>
		
		<tr>
			<td><?=($index+1)?></td>
			<td><?=$vcr["detail"]["voucher_code"]?></td>
			<td><?=date("Y-m-d", strtotime($vcr["detail"]["date"]))?></td>
			<td><?=$vcr["booking"]["customer"]["full_name"]?></td>
			<td><?=$vcr["booking"]["customer"]["country_code"]?></td>
			<td><?=$vcr["detail"]["departure"]["port"]["port_code"]?> (<?=$vcr["detail"]["departure"]["time"]?>)</td>
			<td><?=$vcr["detail"]["arrival"]["port"]["port_code"]?> (<?=$vcr["detail"]["arrival"]["time"]?>)</td>
			<td>
				<?php if (isset($vcr["booking"]["agent"]["name"])){ ?>
					<?=$vcr["booking"]["agent"]["name"]?>
				<?php }else{ ?>
					<?=$vcr["booking"]["source"]?>
				<?php } ?>
			</td>
			<td><?=$vcr["detail"]["qty_1"]?></td>
			<td><?=$vcr["detail"]["qty_2"]?></td>
			<td><?=$vcr["detail"]["qty_3"]?></td>
			<td><?=$vcr["detail"]["rates"]["rates_1"] * $vcr["detail"]["qty_1"]?></td>
			<td><?=$vcr["detail"]["rates"]["rates_2"] * $vcr["detail"]["qty_2"]?></td>
			<td><?=$vcr["detail"]["rates"]["rates_3"] * $vcr["detail"]["qty_3"]?></td>
			<td><?=$vcr["detail"]["pickup_price"]?></td>
			<td><?=$vcr["detail"]["dropoff_price"]?></td>
			<td><?=(isset($vcr["detail"]["additional_charge_price"])?number_format($vcr["detail"]["additional_charge_price"],0,'',''):0)?></td>
			<td><?=$vcr["detail"]["subtotal"]?></td>
			<td><?=(isset($vcr["detail"]["discount"])?number_format($vcr["detail"]["discount"],0,'',''):0)?></td>
			<td><?=(isset($vcr["detail"]["hybrid_fee"])?number_format($vcr["detail"]["hybrid_fee"],0,'',''):0)?></td>
			<td><?=number_format($vcr["detail"]["total"],0,'','')?></td>
			<td>
				<?php
				$arr_passenger = array();
				foreach ($vcr["detail"]["passengers"] as $passenger){
					$arr_passenger[] = $passenger["first_name"]." ".$passenger["last_name"];
				}
				?>
				<?=implode(", ", $arr_passenger)?>
			</td>
		</tr>
		
		<?php } ?>
		<?php } ?>
	</table>
<?php
//pre($revenue);
?>

<?php } ?>

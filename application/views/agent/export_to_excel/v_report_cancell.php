
<?php
if (!isset($_GET["TEST"])){

	$file="Sales.xls";
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=$file");

}
//pre($data["vendor"]);

?>

<?php if (isset($sales)){ ?>
	<?php if ($search['view'] != 'date') { ?>
		<table border="1" cellspacing="1">
		<tr class="header">
			<td ><b>Date</b></td>
			<td align="right"><b>Sales</b></td>
			
		</tr>
		
		
		<?php foreach ($sales as $revenue_data){ ?>
			<tr >
				<td width="180"><?=$revenue_data['date']?></td>
				<td width="250" align="right" >
					<?=$revenue_data['sources']['currency']?> <?=$revenue_data['sources']['total']?>
						<hr style="margin:2px 0">
						<small>
							<span style="margin-right: 20px;">Booking(s): <?=$revenue_data['sources']['total_book']?></span>
							<span>Guest(s): <?=$revenue_data['sources']['total_guest']?></span>
						</small>
				</td>
			</tr>
			
		<?php } ?>
	</table>
	<?php }else{ ?>
		<table border="1" cellspacing="1">
			<tr class="header">
				
				<td><b>Order#</b></td>
				<td><b>Customer Name</b></td>
				<td align="right"><b>Amount</b></td>
				<td><b>Total Trip</b></td>
			</tr>
			<?php foreach ($sales as $value) { ?>
				<tr>
					
						<td><strong><?=$value['code']?></strong></td>
						<td><?=$value['firs_name']?> <?=$value['last_name']?><hr /><small>
							<span style="margin-left: 10px;">Adult(s): <?=$value['qty1']?></span>
							<span style="margin-left: 10px;">Child(s): <?=$value['qty2']?></span>
							<span style="margin-left: 10px;">Invant(s): <?=$value['qty3']?></span>
						</small></td>
						<td class="text-right"><?=$value['currency']?> <?=$value['total']?> </td>
						<td class="text-center"><?=$value['book']?></td>
				</tr>
			<?php } ?>
		</table>
	<?php } ?>
<?php
//pre($revenue);
?>

<?php } ?>

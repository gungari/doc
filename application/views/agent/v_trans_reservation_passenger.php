<h1>Reservation</h1>
<div class="sub-title"> Passenger Information</div>
<br />
<div ng-init="loadDataBookingTransportPassenger();" class="reservation-detail">
	<div ng-show='!(DATA.current_booking)'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	<div ng-show='(DATA.current_booking)'>
		<div class="pull-right">
			<div class="btn-group">
			  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fa fa-print" aria-hidden="true"></i> Print <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu">
				<li><a href="<?=site_url("home/print_page/#/print/receipt_trans/")?>{{DATA.current_booking.booking.booking_code}}" target="_blank">Receipt #{{DATA.current_booking.booking.booking_code}}</a></li>
				<li role="separator" class="divider"></li>
				<li ng-repeat='detail in DATA.current_booking.booking.detail'>
					<a href="<?=site_url("home/print_page/#/print/voucher_trans/")?>{{DATA.current_booking.booking.booking_code}}/{{detail.voucher_code}}" target="_blank">Voucher {{detail.voucher_code}}</a>
				</li>
			  </ul>
			</div>
			<?php /* <!-- <div class="btn-group">
			  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fa fa-bars" aria-hidden="true"></i> Menu <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu">
				<li ng-show="DATA.current_booking.booking.status_code != 'CANCEL'">
					<a href="" data-toggle="modal" data-target="#cancel-booking-form" ng-click="cancelBooking(DATA.current_booking.booking)">
						<i class="fa fa-remove" aria-hidden="true"></i> Cancel Booking
					</a>
				</li>
				<li ng-show='DATA.current_booking.booking.total_payment!=0'>
					<a href="" data-toggle="modal" data-target="#refund-booking-form" ng-click="refundBooking(DATA.current_booking.booking)">
						<i class="fa fa-reply" aria-hidden="true"></i> Refund
					</a>
				</li>
				<li>
					<a ui-sref="trans_reservation.edit({'booking_code':DATA.current_booking.booking.booking_code})">
						<i class="fa fa-pencil" aria-hidden="true"></i> Edit Booking
					</a>
				</li>
			  </ul>
			</div> --> */ ?>
		</div>
		<div class="title">
			<h1>#{{DATA.current_booking.booking.booking_code}} - {{DATA.current_booking.booking.customer.first_name}} {{DATA.current_booking.booking.customer.last_name}}</h1>
			<?php /*?><div class="code"> Date : {{fn.formatDate(DATA.current_booking.booking.transaction_date, "dd M yy")}}</div><?php */?>
		</div>

		<ul class="nav nav-tabs sub-nav">
			<li role="presentation" class="detail"><a ui-sref="detail_reservation({'booking_code':DATA.current_booking.booking.booking_code})">Detail</a></li>
			<li role="presentation" class="passenger"><a ui-sref="detail_passenger({'booking_code':DATA.current_booking.booking.booking_code})">Passenger</a></li>
			<li role="presentation" class="payment"><a ui-sref="detail_payment({'booking_code':DATA.current_booking.booking.booking_code})">Billing Statement</a></li>
		</ul>
		<br />
	<div class="table-responsive">
		
		<table class="table table-bordered table-condensed" ng-repeat='detail in DATA.current_booking.booking.detail' style="margin-bottom:10px">
			<tr class="info">
				<td colspan="7">
					<div class="pull-right" ng-show="($index > 0)">
						<a href="" ng-click="copyPassenger(($index-1), $index)">
							<span class="glyphicon glyphicon-duplicate"></span> Same as above
						</a>
					</div>
				
					Voucher# : 
					<strong>{{detail.voucher_code}}</strong> - 
					<strong>{{fn.newDate(detail.date) | date : 'dd MMM yyyy'}}</strong> -
					<strong>{{detail.departure.port.name}} ({{detail.departure.port.port_code}}) : {{detail.departure.time}}</strong>
					&nbsp;&nbsp;<i class="fa fa-chevron-right"></i>&nbsp;&nbsp;
					<strong>{{detail.arrival.port.name}} ({{detail.arrival.port.port_code}}) : {{detail.arrival.time}}</strong>
				</td>
			</tr>
			<tr class="header bold">
				<td width="40">#</td>
				<td>Full Name</td>
				<td width="150">Email</td>
				<td width="130">Phone</td>
				<td width="130">ID Number</td>
				<td width="130">Nationality</td>
				<td width="40"></td>
			</tr>
			<tr ng-repeat='passenger in detail.passenger' ng-class="{'warning':passenger.is_data_updated != '1'}">
				<td>{{($index+1)}}</td>
				<td>
					<a href="" title="Edit Passenger" data-toggle="modal" ng-click="addEditPassenger(passenger, detail)" data-target="#add-edit-passenger">
						{{passenger.first_name}} {{passenger.last_name}}
					</a>
				</td>
				<td>{{passenger.email}}</td>
				<td>{{passenger.phone}}</td>
				<td>{{passenger.passport_number}}</td>
				<td>{{passenger.country_name}}</td>
				<td align="center"><a href="" title="Edit Passenger" data-toggle="modal" ng-click="addEditPassenger(passenger, detail)" data-target="#add-edit-passenger"><span class="glyphicon glyphicon-pencil"></span></a></td>
			</tr>
		</table>
		
	</div>
	<div class="modal fade" id="add-edit-passenger" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <form ng-submit='saveDataPassenger($event)'>
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">
				Edit Passenger
			</h4>
		  </div>
		  <div class="modal-body">
		  	<div ng-show='DATA.myPassenger.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myPassenger.error_msg'>{{err}}</li></ul></div>
			<table class="table table-borderless table-condensed">
				<tr>
					<td width="120">Name*</td>
					<td>
					
						<input type="text" required="required" placeholder="First Name" class="form-control input-md" ng-model='DATA.myPassenger.first_name' style="display:inline; width:48%" />
						&nbsp;&nbsp;&nbsp;
						<input type="text" required="required" placeholder="Last Name" class="form-control input-md" ng-model='DATA.myPassenger.last_name' style="display:inline; width:48%" />
					</td>
				</tr>
				<tr>
					<td>Email</td>
					<td><input type="text" placeholder="Email" class="form-control input-md" ng-model='DATA.myPassenger.email' /></td>
				</tr>
				<tr>
					<td>Phone / Mobile</td>
					<td><input type="text" placeholder="Phone / Mobile" class="form-control input-md" ng-model='DATA.myPassenger.phone' /></td>
				</tr>
				<tr>
					<td>ID Number</td>
					<td><input type="text" placeholder="ID Number" class="form-control input-md" ng-model='DATA.myPassenger.passport_number' /></td>
				</tr>
				<tr>
					<td>Country</td>
					<td>
						<select class="form-control input-md" ng-model='DATA.myPassenger.country_code'>
							<option value="">-- Select Country --</option>
							<option value="{{country.code}}" ng-repeat='country in DATA.country_list.country_list'>{{country.name}}</option>
						</select>
					</td>
				</tr>
			</table>
		  </div>
		  <div class="modal-footer" style="text-align:center">
		  	<button type="submit" class="btn btn-primary">Save</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
	  </div>
	  </form>
	</div>
	
</div>
<script>activate_sub_menu_agent_detail("passenger");</script>

<style type="text/css">
	hr {
		margin: 0 !important;
		padding: 0 !important;
	}
</style>
<style type="text/css">
	.muted{
		background: #f9f9f9;
	}
	.tr-muted {
		color: #ccc;
	}
	ul li.title {
		margin-top: 10px;
	}
	ul li.title a {
		padding: 5px;
		font-weight: bold;
	}
	a.muted-link {
		text-decoration: inherit;
		color: inherit;
	}
	a.muted-link:hover {
		text-decoration: inherit;
		color: #1B77C0;
	}
	.report-content table th, .report-content table td {
		vertical-align: middle !important;
	}
	.report-content .report-margin {
		margin:10px auto;
	}
	.report-content .report-border {
		border: #ccc solid 0.5px;;
	}
	.report-content table .header, .report-content table .footer {
		background-color: #eee;
		font-weight: bold;
	}
	.text-middle{
		vertical-align: middle;
	}
	[ng-click],
	[data-ng-click],
	[x-ng-click] {
	    cursor: pointer;
	}
</style>
<div class="sub-title">Transaction</div>
<div class="products" ng-controller="trans_transaction_controller" ng-init="loadReportTrasc()">
		<div class="product">
			<form class="form-inline" id="formFilter" ng-submit="loadReportTrascData()" style="margin-bottom: 10px;">
				<div class="table-responsive">
				<table class="table table-condensed table-borderless" width="100%">
					<tr>
						<td width="70">View</td>
						<td width="70" ng-show="search.view == 'date'">Start Date</td>
						<td width="70" ng-show="search.view == 'date'">End Date</td>
						<td width="120" ng-show="search.view == 'month'">Month</td>
						<td width="70" ng-show="search.view == 'month' || search.view == 'year'">Year</td>
						<?php if($vendor['category'] == 'activities'){ ?>
							<td width="120">Product</td>
						<?php } ?>	
						<td></td>
					</tr>
					<tr>
						<td>
							<select name="view" class="form-control input-sm" ng-model="search.view" style="width: 80px;">
								<?php foreach(array("date"=>"Date", "month"=>"Month", "year"=>"Year") as $key => $value) : ?>
									<?php if($key==$view) : ?>
										<option value="<?=$key?>" selected><?=$value?></option>
									<?php else : ?>
										<option value="<?=$key?>"><?=$value?></option>
									<?php endif; ?>
								<?php endforeach; ?>
							</select>
						</td>
						<td ng-show="search.view == 'date'">
							<input type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='search.start_date' />
						</td>
						<td ng-show="search.view == 'date'">
							<input type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='search.end_date' />
						</td>
						<td ng-show="search.view == 'month'">
							<select name="month" class="form-control input-sm" ng-model="search.month" style="width: 120px;">
								<?php foreach(array("1"=>"January","2"=>"February","3"=>"March","4"=>"April","5"=>"May","6"=>"June","7"=>"July","8"=>"August","9"=>"September","10"=>"October","11"=>"November","12"=>"December") as $key => $value) : ?>
									<?php if($month==$key) : ?>
										<option value="<?=$key?>" selected><?=$value?></option>
									<?php else : ?>
										<option value="<?=$key?>"><?=$value?></option>
									<?php endif; ?>
								<?php endforeach; ?>
							</select>
						</td>
						<td ng-show="search.view == 'month' || search.view == 'year'">
							<select name="year" class="form-control input-sm" ng-model="search.year" style="width: 90px;">
								<?php for($i=date("Y") + 1;$i>=2014;$i--) : ?>
									<?php if($year==$i) : ?>
										<option value="<?=$i?>" selected><?=$i?></option>
									<?php else : ?>
										<option value="<?=$i?>"><?=$i?></option>
									<?php endif; ?>
								<?php endfor; ?>
							</select>
						</td>
						<?php if($vendor['category'] == 'activities'){ ?>
						<td ng-init="loadProduct()">
							<select class="form-control input-sm" placeholder="Product Name" ng-model='search.product'>
								<option value="">All Products</option>
								<option value="{{product.product_code}}" ng-repeat='product in DATA.products'>{{product.name}}</option>
							</select>
						</td>
						<?php } ?>	
						<td>
							<button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-menu-right"></span></button>
						</td>
					</tr>
				</table>
			</div>
		
		</form>
	</div>
	<div ng-show='show_loading'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		</div>
  
	<hr />

	<div ng-show="show_error" class="alert alert-warning report-margin" id="tableBooking" role="alert">
		No data transaction
	</div>
	<div style="margin-top: 10px;" ng-show="search.view == 'date'">
		<table class="table table-bordered report-margin" style="font-size:12px">
			<tr class="header">
				<th width="150" align="center">Trx. ID#</th>
				<th>Booking Source</th>
				<th width="130">Payment</th>
				<th width="120" class="text-right">Money In</th>
				<th width="120" class="text-right">Money Out</th>
			</tr>
			<tbody ng-repeat="data in transaction">
				<tr>
					<td>
						{{data.payment_code}}
						<hr style="margin:2px" />
						
						<strong><div ng-show="search.view!='year'">{{fn.formatDate(data.date, "dd MM yy")}}</div></strong>
						<strong><div ng-show="search.view=='year'">{{fn.formatDate(data.date, "MM yy")}}</div></strong>
					</td>
				
					<td>
						{{data.booking_source}}
					</td>
					<td>{{data.fund_source_code}}</td>
					<td class="text-right">
						{{data.currency}} {{fn.formatNumber(data.money_in, data.currency)}}
					</td>
					<td class="text-right">
						{{data.currency}} {{fn.formatNumber(data.money_out, data.currency)}}
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div style="margin-top: 10px;" ng-show="search.view != 'date'">
		<table class="table table-bordered report-margin" style="font-size:12px">
			<tr>
				<th>Date</th>
				<th>Booking Source</th>
				<th class="text-right">Money In</th>
				<th class="text-right">Money Out</th>
			</tr>
			<tbody ng-repeat="(index,data) in transaction" >
				<tr >
					<td ng-class="{'muted':(data.trsc == '')}" width="100">
						<span ng-show="search.view != 'year' && data.trsc != ''"><a ng-click="loadReportTrascData(data.date,'date')">{{fn.formatDate(data.date, "dd MM yy")}}</a></span>
						<span ng-show="search.view == 'year' && data.trsc  !=''"><a ng-click="loadReportTrascData(data.date,'month')">{{fn.formatDate(data.date, "MM yy")}}</a></span>
						<span ng-show="data.trsc == '' && search.view == 'year'">{{fn.formatDate(data.date, "dd MM yy")}}</span>
						<span ng-show="data.trsc == '' && search.view != 'year'">{{fn.formatDate(data.date, "dd MM yy")}}</span>
					</td>
					<td ng-class="{'muted':(data.trsc == '')}">
						<span ng-repeat="item in data.trsc" style="margin-bottom: 10px;">
							<span ng-show="item.payment_type_name">{{item.payment_type_name}}</span>
							<span ng-show="!item.payment_type_name">{{item.fund_source_code}}</span>
							<hr style="margin-bottom: :5px">
						</span>
					</td>
					<td ng-class="{'muted':(data.trsc == '')}" class="text-right">
						<span ng-repeat="item in data.trsc">
							{{item.currency}} {{fn.formatNumber(item.money_in, item.currency)}}
							<hr style="margin-bottom: :5px">
						</span>
					</td>
					<td ng-class="{'muted':(data.trsc == '')}" class="text-right">
						<span ng-repeat="item in data.trsc">
							{{item.currency}} {{fn.formatNumber(item.money_out, item.currency)}}
							<hr style="margin-bottom: :5px">
						</span>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<hr />
	<br>
	<div class="text-right" ng-show="summary">
	<table class="table table-condensed table-borderless">
		<tr align="right">
			<td colspan="2" ><strong><span style="font-size: 14px;">Summary </span></strong></td>
			
		</tr>
		
		<tr ng-repeat="(index,data) in summary">
			<td>
				<span style="font-size: 14px;" ng-show="data.name">{{data.name}} :</span>
				<span style="font-size: 14px;" ng-show="!data.name">{{data.fund_source_code}} :</span></td>
			<td width="250"><span style="font-size: 14px;"><strong>{{data.currency}} {{fn.formatNumber(data.total_in,data.currency)}}</strong></span></td>
		</tr>
		
		
	</table>
	<hr>

</div>
<style type="text/css">
	td.color-red {
		color: red;
	}
</style>
	<br />
	<!-- <span><a href="<?=site_url("home/print_page/#/print/report_sales/")?>{{search.date}}/{{search.month}}/{{search.year}}/{{search.view}}/{{search.product}}" target="_blank" >
			<i class="fa fa-print" aria-hidden="true"></i> Print 
		</a></span>&nbsp;|&nbsp;
	<span><a href="<?=site_url("export_to_excel/sales")?>?
		s[year]={{search.year}}&
		s[date]={{search.date}}&
		s[month]={{search.month}}&
		s[view]={{search.view}}&
		s[product]={{search.product}}
		" target="_blank">
			<i class="fa fa-file-excel-o" aria-hidden="true"></i> Export To Excel
	</a></span> -->
</div>

<script type="text/javascript">
	function setDateOption(){
		var year = $('#formFilter [name="year"]').val();
		var month = $('#formFilter [name="month"]').val();
		var date = $('#formFilter [name="date"]').val();
		var last_date = new Date(year, month, 0).getDate();
		$('#formFilter [name="date"]').empty();
		for(i=1;i<=last_date;i++) {
			if(i==date) {
				$('#formFilter [name="date"]').append('<option value='+i+' selected>'+i+'</option>');
			} else {
				$('#formFilter [name="date"]').append('<option value='+i+'>'+i+'</option>');
			}
		}
	}
	function hideFormElement() {
		$("#formFilter [name='month']").hide();
		$("#formFilter [name='date']").hide();
		var view = $("#formFilter [name='view']").val(); 
		switch(view) {
			case 'year':
				// tidak ada yang disembunyikan
				break;
			case 'date':
				$("#formFilter [name='month']").show();
				$("#formFilter [name='date']").show();
				break;
			default:
				$("#formFilter [name='month']").show();
				break;
		}
	}
	$("#formFilter").change(function(){
		// hideFormElement();
		// setDateOption();
	});
	

	
</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-111878953-1','auto');
    ga('set', 'forceSSL', true);
    ga('set', 'userId', '167160160');
    ga('send', 'pageview');
</script>
<div ng-init="printTransactionTransport()">

	<div class="no-print text-center" style="text-align:center !important">
		
		<br /><br />
	</div>
	<br />
	<div ng-show='!DATA.transaction'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		</div>
	<div class="normal-paper">
	<div style="margin-top: 10px;">
		<div class="header" style="margin: 0 auto; width:700px;height: auto;">
			<div class="pull-right text-center" align="center">
				<div style="font-size:14px" class="title"><strong><?=$vendor["business_name"]?></strong></div>
				<div style="font-size:12px">Transaction</div>

			</div>
			<div class="title">Transaction <span ng-show="start_date">({{fn.newDate(start_date) | date:'dd MMM yyyy'}} - {{fn.newDate(end_date) | date:'dd MMM yyyy'}})</span><span ng-show="!start_date">All</span></div>

		</div>
		<div ng-show='DATA.transaction'>
			<div ng-show='!show_loading_DATA_bookings'>
				<div class="table-responsive">
					<table class="table table-condensed table-bordered">
						<tr class="header bold">
							<td width="120" align="center">Trx. ID#</td>
							<td width="120" align="center">Order# / Invoice#</td>
							<td>Booking Source</td>
							<td width="130">Payment</td>
							<!-- <td width="120" align="center">Status</td> -->
							<td width="120" align="right">Total Amount</td>
						</tr>
						<tbody ng-repeat="transaction in DATA.transaction.transactions.data">
							<tr ng-class="{'danger':(transaction.trx_type == 'REFUND' || transaction.trx_type == 'COMMISSION')}">
								<td rowspan="2" align="center">
									{{transaction.trx_code}}
									<hr style="margin:2px" />
									{{fn.newDate(transaction.date) | date:'dd MMM yyyy'}} 
									<!-- <small>{{fn.newDate(transaction.date) | date:'HH:mm'}}</small> -->
								</td>
								<td align="center">
									<span ng-show="transaction.trx_type != 'INVOICE'" ui-sref="trans_reservation.detail({'booking_code':transaction.booking_code})" target="_blank"><strong>{{transaction.booking_code}}</strong></span>
									<span ng-show="transaction.trx_type == 'INVOICE'" ui-sref="invoice.detail({'invoice_code':transaction.booking_code})" target="_blank"><strong>{{transaction.booking_code}}</strong></span>
								</td>
								<td>
                                	<div ng-show="transaction.trx_type == 'REFUND'">REFUND</div>
                                	<div ng-show="transaction.trx_type == 'COMMISSION'">COMMISSION</div>
									<div ng-show="!transaction.agent">
										{{transaction.booking_source}}<br />
										<strong>{{transaction.customer.full_name}}</strong>
									</div>
									<div ng-show="transaction.agent">
										<strong>{{transaction.agent.name}}</strong>
									</div>
								</td>
								<td>
                                	<div ng-show="transaction.trx_type == 'REFUND' && !transaction.payment_type"><span class="label label-warning">PENDING</span></div>
									{{transaction.payment_type}}
								</td>
								<!-- <td align="center">{{transaction.trx_type}}</td> -->
								<td align="right">
									{{transaction.currency}} {{fn.formatNumber(transaction.amount, transaction.currency)}}
								</td>
							</tr>
							<tr ng-class="{'danger':(transaction.trx_type == 'REFUND' || transaction.trx_type == 'COMMISSION')}">
								<td colspan="7" style="font-size:11px">
									<em>Remarks : {{transaction.description}}</em>
									<hr style="margin:2px" />
									<em style="color: green;">Post Time: {{fn.newDate(transaction.trx_date) | date:'dd MMM yyyy'}} 
									<small>{{fn.newDate(transaction.trx_date) | date:'HH:mm'}}</small></em>
								</td>
							</tr>
							<tr>
								<td colspan="8" style="background:#FAFAFA"></td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="clearfix"></div>
				
				<div class="text-right" ng-show="DATA.transaction.transactions">
					<table class="table table-condensed table-borderless">
						<td colspan="2" ><strong><span style="font-size: 14px;">Summary <span ng-show="search.start_date">({{fn.newDate(search.start_date) | date:'dd MMM yyyy'}} - {{fn.newDate(search.end_date) | date:'dd MMM yyyy'}})</span ng-show="!search.start_date">All<span></span></span></strong></td>
						<tr>
							<td>Total Paid : </td>
							<td width="130"><strong>{{DATA.transaction.transactions.data[0].currency}} {{fn.formatNumber(DATA.transaction.transactions.total_paid, DATA.transaction.transactions.data[0].currency)}}</strong></td>
						</tr>
						<tr>
							<td class="color-red">Total Refund : </td>
							<td class="color-red"><strong>{{DATA.transaction.transactions.data[0].currency}} {{fn.formatNumber(DATA.summary.total_refund,DATA.transaction.transactions.data[0].currency)}}</strong></td>
						</tr>
						<!-- <tr>
							<td>{{DATA.transaction.transactions.total_acc}}</td>
						</tr> -->
						<tr ng-repeat="(index,data) in DATA.transaction.transactions.total_acc" ng-show="data.total!=0">
							<td ng-class="{'color-red':(data.name == 'Total Refund' || data.name == 'Commission Payment')}">{{data.name}} : </td>
							<td ng-class="{'color-red':(data.name == 'Total Refund' || data.name == 'Commission Payment')}"><strong>{{DATA.transaction.transactions.data[0].currency}} {{fn.formatNumber(data.total,DATA.transaction.transactions.data[0].currency)}}</strong></td>
						</tr>
						
					</table>
					<hr>

						<table class="table table-condensed table-borderless">
							<tr ng-show="DATA.transaction.transactions.agent">
								<td>Total Deposit: </td>
								<td><strong>{{DATA.transaction.transactions.agent.currency}} {{fn.formatNumber(DATA.transaction.transactions.agent.deposit, DATA.transaction.transactions.agent.currency)}}</strong></td>
							</tr>
							<tr ng-show="DATA.transaction.transactions.agent">
								<td>Used Deposit: </td>
								<td width="130"><strong>{{DATA.transaction.transactions.agent.currency}} {{fn.formatNumber(DATA.transaction.transactions.agent.transaction.total, DATA.summary.agent.currency)}}</strong></td>
							</tr>
						</table>
				</div>
				<style type="text/css">
						td.color-red {
							color: red;
						}
					</style>
			</div>
		</div>
	</div>
		
	</div>
</div>	

<style type="text/css" media="print">
	
	 @media screen and (orientation:landscape) { … }
</style>
<style>


	.print_area{background:none !important; padding:0 !important;}
	.small_paper{width:8cm !important; margin:0 auto !important}
	.normal-paper{background:#FFF; padding:10px; font-size: 9px;}
	.small-paper{width:8cm; margin:auto; padding:10px; background:#FFF}
	.small-paper .header .title{font-size:18px !important}
	.landscape {width: 1030px; height: auto; margin-left: -139px;}
</style>
<?php
//pre($vendor)
?>
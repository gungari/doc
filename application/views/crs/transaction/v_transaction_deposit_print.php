<div ng-init="printTransactionDeposit()">

	<div class="no-print text-center" style="text-align:center !important">
		
		<br /><br />
	</div>
	<br />
	<div ng-show='!DATA.deposit'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		</div>
	<div class="normal-paper">
	<div style="margin-top: 10px;">
		<div class="header" style="margin: 0 auto; width:700px;height: auto;">
			<div class="pull-right text-center" align="center">
				<div style="font-size:14px" class="title"><strong><?=$vendor["business_name"]?></strong></div>
				<div style="font-size:12px">Deposit</div>

			</div>
			<div class="title">Deposit</div>

		</div>
		
		<div ng-show="DATA.deposit">
			<div class="table-responsive">
				<table class="table table-condensed table-bordered">
					<tr class="header bold">
						<td width="120" align="center">Trx. ID#</td>
						<td width="120" align="center">Order# / Invoice#</td>
						<td>Booking Source</td>
						<td width="130">Payment</td>
						<!-- <td width="120" align="center">Status</td> -->
						<td width="120" align="right">Total Amount</td>
					</tr>
					<tbody ng-repeat="transaction in DATA.deposit.transaction.data">
						<tr ng-class="{'danger':(transaction.trx_type == 'REFUND')}">
							<td rowspan="2" align="center">
								{{transaction.trx_code}}
								<hr style="margin:2px" />
								{{fn.newDate(transaction.trx_date) | date:'dd MMM yyyy'}} <small>{{fn.newDate(transaction.trx_date) | date:'HH:mm'}}</small>
							</td>
							<td align="center">
								<a ng-show="transaction.trx_type != 'INVOICE'" ui-sref="trans_reservation.detail({'booking_code':transaction.booking_code})" target="_blank"><strong>{{transaction.booking_code}}</strong></a>
								<a ng-show="transaction.trx_type == 'INVOICE'" ui-sref="invoice.detail({'invoice_code':transaction.booking_code})" target="_blank"><strong>{{transaction.booking_code}}</strong></a>
							</td>
							<td>
                            	<div ng-show="transaction.trx_type == 'REFUND'">REFUND</div>
								<div ng-show="!transaction.agent">
									{{transaction.booking_source}}<br />
									<strong>{{transaction.customer.full_name}}</strong>
								</div>
								<div ng-show="transaction.agent">
									<strong>{{transaction.agent.name}}</strong>
								</div>
							</td>
							<td>
                            	<div ng-show="transaction.trx_type == 'REFUND' && !transaction.payment_type"><span class="label label-warning">PENDING</span></div>
								{{transaction.payment_type}}
							</td>
							<!-- <td align="center">{{transaction.trx_type}}</td> -->
							<td align="right">
								{{transaction.currency}} {{fn.formatNumber(transaction.amount, transaction.currency)}}
							</td>
						</tr>
						<tr ng-class="{'danger':(transaction.trx_type == 'REFUND')}">
							<td colspan="7" style="font-size:11px">
								<em>Remarks : {{transaction.description}}</em>
							</td>
						</tr>
						<tr>
							<td colspan="8" style="background:#FAFAFA"></td>
						</tr>
					</tbody>

				</table>
			</div>
		</div>
	</div>
		
	</div>
</div>	

<style type="text/css" media="print">
	
	 @media screen and (orientation:landscape) { … }
</style>
<style>


	.print_area{background:none !important; padding:0 !important;}
	.small_paper{width:8cm !important; margin:0 auto !important}
	.normal-paper{background:#FFF; padding:10px; font-size: 12px;}
	.small-paper{width:8cm; margin:auto; padding:10px; background:#FFF}
	.small-paper .header .title{font-size:18px !important}
	.landscape {width: 1030px; height: auto; margin-left: -139px;}
</style>
<?php
//pre($vendor)
?>
<div ng-init="loadDataBookingOpenVoucherCode();" class="reservation-detail"> 
	<table class="table table-bordered">
		<tr class="info bold">
			<td width="40">#</td>
			<td><strong>Pre Paid Ticket Code</strong></td>
			<td width="150" align="center"><strong>Sold Date</strong></td>
			<td width="150" align="center"><strong>Booking Code</strong></td>
            <td width="40"></td>
		</tr>
		<tr ng-repeat="voucher_code in DATA.current_booking.booking.open_voucher_detail.voucher_code <?php /*?>| orderBy : '-used.date'<?php */?>" 
        	ng-class="{'is_used':voucher_code.used, 'danger':voucher_code.blocked}">
			<td width="40">{{($index+1)}}</td>
			<td>
            	<strong>{{voucher_code.code}}</strong>
                <div ng-show='voucher_code.blocked'><small><em>Remarks : {{voucher_code.blocked.reason}}</em></small></div>
            </td>
			<td width="150" align="center">
				<span ng-show='voucher_code.used'>{{fn.formatDate(voucher_code.used.date, "dd M yy")}}</span>
			</td>
			<td align="center">
				<span ng-show='voucher_code.used'>
					<a target="_blank" ui-sref="trans_reservation.detail({'booking_code':voucher_code.used.booking_code})"><strong>{{voucher_code.used.booking_code}}</strong></a>
				</span>
			</td>
            <td align="center">
                <div class="dropdown pull-right">
                    <button class="btn btn-default btn-xs dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <span class="glyphicon glyphicon-menu-hamburger"></span>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li ng-class="{'disabled':(voucher_code.used || voucher_code.blocked)}" ng-show='!(voucher_code.used || voucher_code.blocked)'>
                        	<a href="" data-toggle="modal" data-target="#block-open-ticket-form" ng-click="blockOpenTicketCode(voucher_code);"><span class="glyphicon glyphicon-remove" style="color:red"></span> Block Pre Paid Ticket</a>
                        </li>
                        <li ng-class="{'disabled':(voucher_code.used || voucher_code.blocked)}" ng-show='(voucher_code.used || voucher_code.blocked)'>
                        	<a href=""><span class="glyphicon glyphicon-remove" style="color:grey"></span> Block Pre Paid Ticket</a>
                        </li>
                    </ul>
                </div>
            </td>
		</tr>
	</table>
</div>
<style>.reservation-detail .is_used{background:#F9F9F9; color:#999} </style>
<script>activate_sub_menu_agent_detail("voucher_code");</script>

<div class="modal fade" id="block-open-ticket-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <form ng-submit='saveBlockOpenTicketCOde($event)'>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    Block Pre Paid Ticket #{{DATA.myOpenTicketCode.code}}
                </h4>
              </div>
              <div class="modal-body">
                <div ng-show='DATA.myOpenTicketCode.error_desc.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myOpenTicketCode.error_desc'>{{err}}</li></ul></div>
                <table class="table table-borderless table-condensed">
                    <tr>
                        <td width="130">Block Reason</td>
                        <td>
                            <textarea placeholder="Description" required="required" type="text" class="form-control input-md autoheight" ng-model='DATA.myOpenTicketCode.blocked_reason' rows="3"></textarea>
                        </td>
                    </tr>
                </table>
              </div>
              <div class="modal-footer" style="text-align:center">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              </div>
            </div>
        </div>
    </form>
</div>

<?php $this->load->library("phpqrcode"); ?> 
<div ng-init="printReceiptOpenVoucher();" class="reservation-detail">
	<div ng-repeat="voucher_code in DATA.current_booking.booking.open_voucher_detail.voucher_code">
		<div class="open-voucher-page">
			<div class="sub">
				<div class="agent">
					<span ng-show="DATA.current_booking.booking.agent.name">{{DATA.current_booking.booking.agent.name}}</span>
					<span ng-show="!DATA.current_booking.booking.agent.name">{{DATA.current_booking.booking.customer.full_name}}</span>
				</div>
				<div class="barcode"><img src="<?=base_url()?>public/plugin/barcode.php?text={{voucher_code.code}}&size=80&orientation=vertical&codetype=code128" /></div>
				<!-- <div class="barcode">
					<img src="<?=site_url("view/qrcode/")?>?str={{voucher_code.code}}&level=L&size=5" />
				</div> -->
				
				<!-- <div class="date">{{fn.formatDate(DATA.current_booking.booking.transaction_date, "dd MM yy")}}</div> -->
				<div class="code">{{voucher_code.code}}</div>
			</div>
			<div class="sub2">
				<div class="agent">
					<span ng-show="DATA.current_booking.booking.agent.name">{{DATA.current_booking.booking.agent.name}}</span>
					<span ng-show="!DATA.current_booking.booking.agent.name">{{DATA.current_booking.booking.customer.full_name}}</span>
				</div>
				<div class="barcode"><img src="<?=base_url()?>public/plugin/barcode.php?text={{voucher_code.code}}&size=80&orientation=vertical&codetype=code128" /></div>
				<!-- <div class="barcode">
					<img src="<?=site_url("view/qrcode/")?>?str={{voucher_code.code}}&level=L&size=5" />
				</div> -->
				
				<!-- <div class="date">{{fn.formatDate(DATA.current_booking.booking.transaction_date, "dd MM yy")}}</div> -->
				<div class="code">{{voucher_code.code}}</div>
			</div>
			<div class="sub3">
				<div class="agent">
					<span ng-show="DATA.current_booking.booking.agent.name">{{DATA.current_booking.booking.agent.name}}</span>
					<span ng-show="!DATA.current_booking.booking.agent.name">{{DATA.current_booking.booking.customer.full_name}}</span>
				</div>
				<div class="barcode"><img src="<?=base_url()?>public/plugin/barcode.php?text={{voucher_code.code}}&size=80&orientation=vertical&codetype=code128" /></div>
				<!-- <div class="barcode">
					<img src="<?=site_url("view/qrcode/")?>?str={{voucher_code.code}}&level=L&size=5" />
				</div> -->
				
				<!-- <div class="date">{{fn.formatDate(DATA.current_booking.booking.transaction_date, "dd MM yy")}}</div> -->
				<div class="code">{{voucher_code.code}}</div>
			</div>
		</div>
		<div class="page-break"></div>
	</div>
</div>
<style>
	.print_area{background:none !important; padding-top:0 !important}
	.open-voucher-page{width:21.2cm; height:12.4cm; background:#FFF; margin-bottom:10px;  /*border:solid 1px #FFF*/}
	.open-voucher-page .sub{width:33.33%; float:left;/* border-left:solid 0px #EEE;*/}
	.open-voucher-page .sub2{width:33.33%; float:left;/* border-left:solid 0px #EEE;*/}
	.open-voucher-page .sub3{width:33.33%; float:left;/* border-left:solid 0px #EEE;*/}
	/*.open-voucher-page .sub .date{padding-left:3.5cm; font-weight:bold; font-size:12px; color:red; }*/
	.open-voucher-page .sub .code{font-size:16px; text-align: center; color:red; font-weight:bold;margin-top: 1.2cm;}
	.open-voucher-page .sub2 .code{font-size:16px; text-align: center; color:red; font-weight:bold;margin-top: 1.2cm;}
	.open-voucher-page .sub3 .code{font-size:16px; text-align: center; color:red; font-weight:bold;margin-top: 1.2cm;}

	.open-voucher-page .sub .agent{font-size:12px;  text-align: center; color:red; font-weight:bold;margin-top: 5.3cm;}
	.open-voucher-page .sub2 .agent{font-size:12px; text-align: center; color:red; font-weight:bold;margin-top: 5.3cm;}
	.open-voucher-page .sub3 .agent{font-size:12px; text-align: center; color:red; font-weight:bold;margin-top: 5.3cm;}

	.open-voucher-page .sub .barcode{position:absolute; transform: rotate(90deg);left:3cm;top: 3.8cm;}
	.open-voucher-page .sub .barcode img{width:100%;height: 5cm; width: 1cm;}
	.open-voucher-page .sub2 {position:relative}
	.open-voucher-page .sub2 .barcode{position:absolute; transform: rotate(90deg);left:3cm;top: 3.8cm;}
	.open-voucher-page .sub2 .barcode img{width:100%;height: 5cm; width: 1cm;}
	.open-voucher-page .sub3 {position:relative}
	.open-voucher-page .sub3 .barcode{position:absolute; transform: rotate(90deg);left:3cm;top: 3.8cm;}
	.open-voucher-page .sub3 .barcode img{width:100%;height: 5cm; width: 1cm;}
	
	/*@ page {size: 10cm 14cm !important; margin:0}*/
	@page{
		margin-left: 0cm;
		margin-right: 0cm;
		margin-top: 0cm;
		margin-bottom: 0cm;
	}
	
	@media all {
		.page-break	{ display: none !important; }
		.open-voucher-page .sub {position:relative}
		
		
	}
	
	@media print {
		.open-voucher-page{margin-left:0.5cm}
		.print_area{margin:0 !important;}
		.page-break	{ display: block !important; page-break-before: always !important; }
		.open-voucher-page{width:21.2cm; height:12.4cm; background:#FFF; margin:0 auto !important; /*border:solid 1px #EEE*/}
		/*.open-voucher-page .sub .date{margin:0; margin-top:6.5cm !important; font-weight:bold; font-size:12px; color:red !important}*/
		/*.open-voucher-page .sub .code{margin:0; margin-top:0.1cm !important; color:red !important; font-weight:bold !important;margin-top: 7cm;}*/
		.open-voucher-page .sub .code{ position: absolute; font-size:16px;left: 1.7cm;color:red; font-weight:bold;margin-top: 7.0cm; color:red !important; font-weight:bold !important;}
		.open-voucher-page .sub2 .code{ position: absolute; font-size:16px;left: 8.7cm;color:red; font-weight:bold;margin-top: 7.0cm; color:red !important; font-weight:bold !important;}
		.open-voucher-page .sub3 .code{ position: absolute; font-size:16px;left: 15.7cm;color:red; font-weight:bold;margin-top: 7.0cm; color:red !important; font-weight:bold !important;}

		.open-voucher-page .sub{width:100%;}
		.open-voucher-page .sub2{width:100%;}
		.open-voucher-page .sub3{width:100%; }


		.open-voucher-page .sub .barcode{position:absolute; transform: rotate(90deg);left: 3.0cm;top: 4.0cm;}
		.open-voucher-page .sub .barcode img{width:100%;height: 5cm; width: 1cm;}
		.open-voucher-page .sub2 {position:relative}
		.open-voucher-page .sub2 .barcode{position:absolute; transform: rotate(90deg);left:10.0cm;top: 4.0cm;}
		.open-voucher-page .sub2 .barcode img{width:100%;height: 5cm; width: 1cm;}
		.open-voucher-page .sub3 {position:relative}
		.open-voucher-page .sub3 .barcode{position:absolute; transform: rotate(90deg);left:17.0cm;top: 4.0cm;}
		.open-voucher-page .sub3 .barcode img{width:100%;height: 5cm; width: 1cm;}

		.open-voucher-page .sub .agent{ position: absolute; font-size:12px;left: 2.3cm; margin-top:5.0cm;color:red; font-weight:bold;top: 0cm;color:red !important; font-weight:bold !important;}
		.open-voucher-page .sub2 .agent{ position: absolute; font-size:12px;left: 9.5cm; margin-top:5.0cm;color:red; font-weight:bold;top: 0cm;color:red !important; font-weight:bold !important;}
		.open-voucher-page .sub3 .agent{ position: absolute; font-size:12px;left: 16.7cm; margin-top:5.0cm;color:red; font-weight:bold;top: 0cm;color:red !important; font-weight:bold !important;}

	}
	 
</style>

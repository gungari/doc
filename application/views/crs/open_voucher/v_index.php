<div class="pull-right">
	<ul class="nav nav-pills">
	  	<li role="presentation" class="rates"><a ui-sref="open_voucher.rates"><i class="fa fa-address-book-o" aria-hidden="true"></i> Rates</a></li>
		<li role="presentation" class="booking"><a ui-sref="open_voucher"><i class="fa fa-book" aria-hidden="true"></i> Bookings</a></li>
	</ul>
</div>

<h1>Pre Paid Ticket</h1>

<div ui-view>
	<div ng-init="loadDataOpenVoucherBooking()">
		<div class="sub-title">Bookings</div>
		
		<div class="products">
			<div class="product">
				<form ng-submit='loadDataOpenVoucherBooking()'>
				<div class="table-responsive">
					<table class="table table-condensed table-borderless" width="100%">
						<tr>
							<td width="110">From</td>
							<td width="110">To</td>
							<td width="200">Search</td>
							<td width="130">Source</td>
							<td width="130">Status</td>
							<td></td>
						</tr>
						<tr>
							<td><input type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='search.start_date' /></td>
							<td><input type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='search.end_date' /></td>
							<td><input type="text" class="form-control input-sm" placeholder="Search" ng-model='search.q' /></td>
							<td>
								<select class="form-control input-sm" ng-model='search.booking_source'>
									<option value="">All</option>
									<option value="{{booking_source.code}}" ng-repeat='booking_source in DATA.booking_source.booking_source'>{{booking_source.name}}</option>
								</select>
							</td>
							<td>
								<select class="form-control input-sm" ng-model='search.booking_status'>
									<option value="">All</option>
									<option value="{{booking_status.code}}" ng-repeat='booking_status in DATA.booking_status.booking_status'>{{booking_status.name}}</option>
								</select>
							</td>
							<td><button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button></td>
						</tr>
					</table>
					
					<?php /*?>Filter : <input type="text" ng-model="DATA.filter_booking" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
					<button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button><?php */?>
				</div>	
				</form>
				<?php /*?>Filter : <input type="text" ng-model="filter_booking" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" /><?php */?>
			</div>
		</div>
		
		<div ng-show='show_loading_DATA_bookings'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		</div>
		
		<div ng-show='!show_loading_DATA_bookings'>
			<table class="table table-condensed table-bordered">
				<tr class="header bold">
					<td align="center" width="130">Order#</td>
					<td align="center" width="130">Resv. Date</td>
					<?php /*?><td>Customer Name</td>
					<td align="center" width="40" title="Nationality">Nat.</td><?php */?>
					<td>Source</td>
					<td align="center" width="80">Status</td>
					<td align="right" width="120">Total</td>
				</tr>
				<tbody ng-repeat='booking in DATA.bookings.bookings | filter:filter_booking'>
					<tr ng-class="{'danger':(booking.status_code=='CANCEL'), 'warning':(booking.status_code=='UNDEFINITE')}">
						<td align="center" rowspan="2"><a ui-sref="open_voucher.booking_detail({'booking_code':booking.booking_code})"><strong>{{booking.booking_code}}</strong></a></td>
						<td align="center">{{fn.newDate(booking.transaction_date) | date:'dd MMM yyyy'}} <small>{{fn.newDate(booking.transaction_date) | date:'HH:mm'}}</small> </td>
						<?php /*?><td>
							<strong>{{booking.customer.first_name}} {{booking.customer.last_name}}</strong>
						</td>
						<td align="center" class="text-capitalize" title="{{booking.customer.country_name}}">{{booking.customer.country_code}}</td><?php */?>
						<td class="text-capitalize">
							<div ng-show='!booking.agent'><strong>{{booking.customer.first_name}} {{booking.customer.last_name}}</strong></div>
							<div ng-show='booking.agent'><strong>{{booking.agent.name}}</strong></div>
						</td>
						<td align="center" title="{{booking.status}}">{{booking.status_initial}}</td>
						<td align="right">{{booking.currency}} {{fn.formatNumber(booking.grand_total, booking.currency)}}</td>
					</tr>
					<tr ng-class="{'danger':(booking.status_code=='CANCEL'), 'warning':(booking.status_code=='UNDEFINITE')}">
						<td style="border-left:solid 5px #EEE; font-size:11px" colspan="4">
							Qty. : <strong>{{booking.open_voucher.qty}}</strong>
							&nbsp; - &nbsp;
							Price : <strong>{{booking.open_voucher.currency}} {{fn.formatNumber(booking.open_voucher.price,booking.open_voucher.currency)}}</strong>
							&nbsp; - &nbsp;
							Used : <strong>{{booking.open_voucher.used}}</strong>
							&nbsp; - &nbsp;
							Remains : <strong>{{booking.open_voucher.qty - booking.open_voucher.used}}</strong>
						</td>
					</tr>
					<tr>
						<td colspan="7" style="background:#FAFAFA"></td>
					</tr>
				</tbody>
			</table>
			
			<hr />
			<a href="<?=site_url("export_to_excel/bookings_open_voucher")?>" target="_blank"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export To Excel</a>
			
		</div>
		<br />
		
		<div class="add-product-button"> <a ui-sref="open_voucher.new_booking" class="btn btn-success btn-lg btn-block" > <span class="glyphicon glyphicon-plus"></span> New Booking Pre Paid Ticket </a> </div>
		<br>
		<br>
		<script>GeneralJS.activateSubMenu(".nav-pills", "li", ".booking");</script>
	</div>
</div>
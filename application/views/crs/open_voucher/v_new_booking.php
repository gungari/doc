<div ng-init="newBookingOpenVoucher()">
<form ng-submit='submit_booking($event)'>
	<div class="sub-title">New Booking</div>
	
	<div ng-show='DATA.data_rsv.error_desc.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.data_rsv.error_desc'>{{err}}</li></ul></div>
	
	<table class="table table-borderless">
		<?php /*?><tr>
			<td width="150">Booking Source</td>
			<td>
				<select class="form-control input-md" style="width:200px" ng-model='DATA.data_rsv.booking_source' ng-change='aplicable_rates_for_agent()'>
					<option value="{{booking_source.code}}" ng-repeat='booking_source in DATA.booking_source.booking_source' ng-show="booking_source.code!='ONLINE'">{{booking_source.name}}</option>
				</select>
			</td>
		</tr><?php */?>
		<tr ng-show='DATA.data_rsv.booking_source == "AGENT"'>
			<td width="150">Agent Name</td>
			<td>
				<select class="form-control input-md" ng-model='DATA.data_rsv.agent' ng-change='aplicable_rates_for_agent();'
					ng-options="(agents.name +' - '+agents.agent_code) for agents in DATA.agents.agents" required>
					<option value="">-- Select Agent --</option>
					<?php /*?><option value="{{agent.agent_code}}" ng-repeat='agent in DATA.agents.agents'>{{agent.name}} - {{agent.agent_code}}</option><?php */?>
				</select>
                <input type="hidden" ng-model='DATA.data_rsv.booking_source' />
                <input type="hidden" ng-model='DATA.data_rsv.booking_status' />
			</td>
		</tr>
		<?php /*?><tr>
			<td>Booking Status</td>
			<td>
				<select class="form-control input-md" style="width:200px" ng-model='DATA.data_rsv.booking_status'>
					<option value="{{booking_status.code}}" ng-repeat='booking_status in DATA.booking_status.booking_status' ng-show="booking_status.code!='CANCEL'">{{booking_status.name}}</option>
				</select>
			</td>
		</tr><?php */?>
	</table>
	<br />
	
	<div class="sub-title">Pre Paid Ticket Rates</div>
	<table class="table table-borderless">
		<tr>
			<td width="150">Pre Paid Ticket Rates</td>
			<td>
				<select class="form-control input-md" ng-model='DATA.data_rsv.rates' ng-change='calculateTotal()' required
					ng-options="(open_vouchers.name +' - '+open_vouchers.openvoucher_code) for open_vouchers in DATA.open_vouchers.open_vouchers">
					<option value="">-- Select Rates --</option>
				</select>
			</td>
		</tr>
		<tr ng-show='DATA.data_rsv.rates'>
			<td>Code</td>
			<td>{{DATA.data_rsv.rates.openvoucher_code}}</td>
		</tr>
		<tr ng-show='DATA.data_rsv.rates'>
			<td>Range of dates</td>
			<td>{{fn.formatDate(DATA.data_rsv.rates.start_date, "dd MM yy")}} - {{fn.formatDate(DATA.data_rsv.rates.end_date, "dd MM yy")}}</td>
		</tr>
		<tr ng-show='DATA.data_rsv.rates'>
			<td>Price</td>
			<td>{{DATA.data_rsv.rates.currency}} {{fn.formatNumber(DATA.data_rsv.rates.price, DATA.data_rsv.rates.currency)}}</td>
		</tr>
		<tr ng-show='DATA.data_rsv.rates'>
			<td>Description</td>
			<td>{{DATA.data_rsv.rates.description}}</td>
		</tr>
		<tr ng-show='DATA.data_rsv.rates'>
			<td>Min. Purchase</td>
			<td>{{DATA.data_rsv.rates.minimum_purchase}}</td>
		</tr>
		<tr ng-show='DATA.data_rsv.rates'>
			<td>Qty.</td>
			<td><input placeholder="Qty" min='0' type="number" class="form-control input-md" ng-model='DATA.data_rsv.qty' style="width:100px" ng-change='calculateTotal()' /></td>
		</tr>
		<tr ng-show='DATA.data_rsv.rates' style="font-size:22px">
			<td>TOTAL</td>
			<td><strong>{{DATA.data_rsv.rates.currency}} {{fn.formatNumber(DATA.data_rsv.TOTAL.total, DATA.data_rsv.rates.currency)}}</strong></td>
		</tr>
	</table>
	<br />
    
    
	<?php /*?><div class="sub-title">Guest Information</div>
	<table class="table">
		<tr>
			<td width="150">Name*</td>
			<td>
			
				<input type="text" placeholder="First Name" class="form-control input-md" ng-model='DATA.data_rsv.customer.first_name' style="display:inline; width:48.5%" />
				&nbsp;&nbsp;&nbsp;
				<input type="text" placeholder="Last Name" class="form-control input-md" ng-model='DATA.data_rsv.customer.last_name' style="display:inline; width:48.5%" />
			</td>
		</tr>
		<tr>
			<td>Email</td>
			<td><input type="text" placeholder="Email" class="form-control input-md" ng-model='DATA.data_rsv.customer.email' /></td>
		</tr>
		<tr>
			<td>Phone / Mobile*</td>
			<td><input type="text" placeholder="Phone / Mobile" class="form-control input-md" ng-model='DATA.data_rsv.customer.phone' /></td>
		</tr>
		<tr>
			<td>Country*</td>
			<td>
				<select class="form-control input-md" ng-model='DATA.data_rsv.customer.country_code'>
					<option value="">-- Select Country --</option>
					<option value="{{country.code}}" ng-repeat='country in DATA.country_list.country_list'>{{country.name}}</option>
				</select>
			</td>
		</tr>
	</table>
	<br /><?php */?>
	
	<div class="sub-title">Remarks / Special Request</div>
	<table class="table">
		<tr>
			<td width="150"></td>
			<td><textarea placeholder="Description" class="form-control input-md autoheight" ng-model='DATA.data_rsv.remarks' rows="3"></textarea></td>
		</tr>
	</table>
	
	<br />
	<div class="sub-title">
		Payment
	</div>
	
	<table class="table table-borderless table-condenseds">
		<tr>
			<td width="150"></td>
			<td><label><input type="checkbox" ng-model='DATA.data_rsv.add_payment' ng-true-value='1' ng-false-value='0' /> Add Payment</label></td>
		</tr>
	</table>
	<table class="table table-borderless table-condenseds" ng-show="DATA.data_rsv.add_payment=='1'">
		<tr>
			<td width="150">Payment Type*</td>
			<td>
            	<select ng-required="DATA.data_rsv.add_payment=='1'" class="form-control input-md" ng-model='DATA.myPayment.payment_type' ng-change='changePaymentType()'>
                    <option value="" disabled="disabled">-- Select Payment Type --</option>
                    <option value="{{payment_method.code}}" ng-repeat='payment_method in $root.DATA_payment_method'
                        ng-hide="payment_method.code=='OPENVOUCHER' || payment_method.code=='ONLINE'">
                        {{payment_method.name}}
                    </option>
                </select>


				<?php /*?><select ng-required="DATA.data_rsv.add_payment=='1'" class="form-control input-md" ng-model='DATA.data_rsv.payment.payment_type'>
					<option value="" disabled="disabled">-- Select Payment Type --</option>
					<option value="ONLINE">Online Payment</option>
					<option value="CC">Credit Card/Debit Card</option>
					<option value="ATM">ATM Transfer</option>
					<option value="CASH">Cash</option>
				</select><?php */?>
			</td>
		</tr>
		<tr ng-show="DATA.data_rsv.payment.payment_type=='OPENVOUCHER'" class="header">
			<td>Pre Paid Ticket Code</td>
			<td><input placeholder="Pre Paid Ticket Code" type="text" class="form-control input-md" ng-model='DATA.myPayment.account_number' /></td>
		</tr>
		<tr ng-show="DATA.data_rsv.payment.payment_type=='CC' || DATA.data_rsv.payment.payment_type=='ATM'" class="header">
			<td><span ng-show="DATA.data_rsv.payment.payment_type=='CC'">Card Number</span><span ng-show="DATA.data_rsv.payment.payment_type=='ATM'">Account Number</span></td>
			<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.data_rsv.payment.account_number' /></td>
		</tr>
		<tr ng-show="DATA.data_rsv.payment.payment_type=='CC' || DATA.data_rsv.payment.payment_type=='ATM'" class="header">
			<td>Name On Card</td>
			<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.data_rsv.payment.name_on_card' /></td>
		</tr>
		<tr ng-show="DATA.data_rsv.payment.payment_type=='CC' || DATA.data_rsv.payment.payment_type=='ATM'" class="header">
			<td><span ng-show="DATA.data_rsv.payment.payment_type=='CC'">EDC Machine</span><span ng-show="DATA.data_rsv.payment.payment_type=='ATM'">Bank Name</span></td>
			<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.data_rsv.payment.bank_name' /></td>
		</tr>
		<tr ng-show="DATA.data_rsv.payment.payment_type=='CC' || DATA.data_rsv.payment.payment_type=='ATM'" class="header">
			<td>Approval Number</td>
			<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.data_rsv.payment.payment_reff_number' /></td>
		</tr>
		<tr>
			<td>Description</td>
			<td><input placeholder="Description" type="text" class="form-control input-md" ng-model='DATA.myPayment.description' /></td>
		</tr>
		<tr>
			<td>Payment Amount*</td>
			<td>
				<div class="input-group">
					<span class="input-group-addon"><?=$vendor["default_currency"]?></span>
					<input placeholder="Payment Amount" min='0' ng-required="DATA.data_rsv.add_payment=='1'" type="number" min="0" class="form-control input-md" ng-model='DATA.data_rsv.payment.payment_amount' style="width:160px" 
						ng-change='DATA.data_rsv.payment.balance = DATA.data_rsv.TOTAL.grand_total - DATA.data_rsv.payment.payment_amount'/>
				</div>
			</td>
		</tr>
		<tr>
			<td>Balance</td>
			<td>
				<div class="input-group">
					<span class="input-group-addon"><?=$vendor["default_currency"]?></span>
					<span class="form-control input-md" style="width:160px">{{fn.formatNumber(DATA.data_rsv.payment.balance, "<?=$vendor["default_currency"]?>")}}</span>
					<?php /*?><input placeholder="Balance" disabled="disabled" type="number" min="0" ng-model='DATA.data_rsv.payment.balance' class="form-control input-md" style="width:160px" /><?php */?>
				</div>
			</td>
		</tr>
	</table>
	
	
	<hr />
	<div class="text-center">
		<button class="btn btn-lg btn-primary">Submit Booking</button>
	</div>
	
	<?php /*?>{{DATA_RSV}}<?php */?>
</form>
</div>
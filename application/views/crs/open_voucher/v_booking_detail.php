	
<div class="sub-title"> Reservation Detail </div>
<br />
<div ng-init="loadDataBookingOpenVoucherDetail();" class="reservation-detail">

	<div ng-show='!(DATA.current_booking)'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	
	<div ng-show='(DATA.current_booking)'>
		<div class="pull-right">
			<div class="btn-group">
			  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fa fa-print" aria-hidden="true"></i> Print <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu">
				<li><a href="<?=site_url("home/print_page/#/print/receipt_trans_open_voucher/")?>{{DATA.current_booking.booking.booking_code}}" target="_blank">Receipt #{{DATA.current_booking.booking.booking_code}}</a></li>
				<li role="separator" class="divider"></li>
				<li><a href="<?=site_url("home/print_page/#/print/receipt_trans_open_voucher_code/")?>{{DATA.current_booking.booking.booking_code}}" target="_blank">Voucher Code</a></li>
			  </ul>
			</div>
			<div class="btn-group" xng-show="DATA.current_booking.booking.status_code != 'CANCEL'">
			  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fa fa-bars" aria-hidden="true"></i> Menu <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu">
				<li>
					<a href="" data-toggle="modal" data-target="#cancel-booking-form" ng-click="cancelBooking(DATA.current_booking.booking)">
						<i class="fa fa-remove" aria-hidden="true"></i> Cancel Booking
					</a>
				</li>
			  </ul>
			</div>
		</div>
		<div class="title">
			<h1>#{{DATA.current_booking.booking.booking_code}} - {{DATA.current_booking.booking.customer.first_name}} {{DATA.current_booking.booking.customer.last_name}}</h1>
			<?php /*?><div class="code"> Date : {{fn.formatDate(DATA.current_booking.booking.transaction_date, "dd M yy")}}</div><?php */?>
		</div>

		<ul class="nav nav-tabs sub-nav">
			<li role="presentation" class="detail"><a ui-sref="open_voucher.booking_detail({'booking_code':DATA.current_booking.booking.booking_code})">Detail</a></li>
			<li role="presentation" class="voucher_code"><a ui-sref="open_voucher.booking_detail.voucher_code">Voucher Code</a></li>
			<li role="presentation" class="payment"><a ui-sref="open_voucher.booking_detail.payment">Payment</a></li>
			<?php /*?><li role="presentation" class="contract-rates"><a ui-sref="agent.detail.contract_rates">Contract Rates</a></li><?php */?>
		</ul>
		<br />
		<div ui-view>
			<div class="sub-title"> Booking Information </div>
			<table class="table">
				<tr>
					<td width="130">Booking Code</td>
					<td><strong>{{DATA.current_booking.booking.booking_code}}</strong></td>
				</tr>
				<tr>
					<td>Booking Date</td>
					<td><strong>{{fn.formatDate(DATA.current_booking.booking.transaction_date, "dd MM yy")}}</strong></td>
				</tr>
				<tr>
					<td>Booking Status</td>
					<td class="text-capitalize">
						<strong><span ng-class="{'label label-danger':DATA.current_booking.booking.status_code == 'CANCEL', 'label label-warning':DATA.current_booking.booking.status_code == 'UNDEFINITE'}">
							{{DATA.current_booking.booking.status.toLowerCase()}}
						</span></strong>
					</td>
				</tr>
				<tr ng-show='DATA.current_booking.booking.cancelation'>
					<td>Cancelation Date</td>
					<td><strong>{{fn.formatDate(DATA.current_booking.booking.cancelation.date, "dd MM yy")}}</strong></td>
				</tr>
				<tr ng-show='DATA.current_booking.booking.cancelation'>
					<td>Cancelation Reason</td>
					<td><strong>{{DATA.current_booking.booking.cancelation.cancelation_reason}}</strong></td>
				</tr>
				<tr>
					<td>Booking Source</td>
					<td class="text-capitalize"><strong>{{DATA.current_booking.booking.source.toLowerCase()}}</strong></td>
				</tr>
				<tr ng-show="DATA.current_booking.booking.agent">
					<td>Agent</td>
					<td class="text-capitalize"><strong>{{DATA.current_booking.booking.agent.name}}</strong></td>
				</tr>
			</table>	
			<br />
            <div ng-show="!DATA.current_booking.booking.agent">
                <div class="sub-title"> Customer Information </div>
                <table class="table">
                    <tr>
                        <td width="130">Full Name</td>
                        <td><strong>{{DATA.current_booking.booking.customer.first_name}} {{DATA.current_booking.booking.customer.last_name}}</strong></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td><strong>{{DATA.current_booking.booking.customer.email}}</strong></td>
                    </tr>
                    <tr>
                        <td>Country</td>
                        <td><strong>{{DATA.current_booking.booking.customer.country_name}}</strong></td>
                    </tr>
                    <tr>
                        <td>Telephone</td>
                        <td><strong>{{DATA.current_booking.booking.customer.phone}}</strong></td>
                    </tr>
                </table>
                <br />
			</div>
            
			<div class="sub-title">Remarks / Special Request</div>
			<table class="table">
				<tr>
					<td width="130">Remarks</td>
					<td>
						<span ng-show="!DATA.current_booking.booking.remarks">-</span>
						<div ng-show="DATA.current_booking.booking.remarks" class="">
							<strong>{{DATA.current_booking.booking.remarks}}</strong>
						</div>
					</td>
				</tr>
			</table>
			<br />
			
			<div class="sub-title"> Booking Details </div>
			<table class="table table-bordered">
				<tr class="info">
					<td><strong>Description</strong></td>
					<td width="150" align="right"><strong>Sub Total</strong></td>
				</tr>
				<tbody ng-show='DATA.current_booking.booking.open_voucher_detail'>
					<tr>
						<td>
							<strong>Pre Paid Ticket</strong><br />
							{{DATA.current_booking.booking.open_voucher_detail.qty}} x 
							{{DATA.current_booking.booking.open_voucher_detail.rates_name}}
							-
							@ {{DATA.current_booking.booking.currency}} {{fn.formatNumber(DATA.current_booking.booking.open_voucher_detail.price, DATA.current_booking.booking.currency)}}
						</td>
						<td align="right">
							<strong>
								{{DATA.current_booking.booking.currency}}
								{{fn.formatNumber(DATA.current_booking.booking.open_voucher_detail.subtotal, DATA.current_booking.booking.currency)}}
							</strong>
						</td>
					</tr>
				</tbody>
				<tr class="success">
					<td align="right"><strong>Total</strong></td>
					<td align="right">
						<strong>
							{{DATA.current_booking.booking.currency}}
							{{fn.formatNumber(DATA.current_booking.booking.total_before_discount,DATA.current_booking.booking.currency)}}
						</strong>
					</td>
				</tr>
				<tr class="success">
					<td align="right">
						<strong>
							Discount 
							<span ng-show='DATA.current_booking.booking.discount.amount > 0'>
								&nbsp;&nbsp;
								<span ng-show="DATA.current_booking.booking.discount.type == '%'">({{DATA.current_booking.booking.discount.value}}%)</span>
								<span ng-show="DATA.current_booking.booking.discount.type != '%'">({{fn.formatNumber(DATA.current_booking.booking.discount.value,DATA.current_booking.booking.currency)}})</span>
							</span>
						</strong>
					</td>
					<td align="right">
						<strong>
							{{DATA.current_booking.booking.currency}}
							{{fn.formatNumber(DATA.current_booking.booking.discount.amount,DATA.current_booking.booking.currency)}}
						</strong>
					</td>
				</tr>
				<tr class="success">
					<td align="right"><strong>Grand Total</strong></td>
					<td align="right">
						<strong>
							{{DATA.current_booking.booking.currency}}
							{{fn.formatNumber(DATA.current_booking.booking.grand_total,DATA.current_booking.booking.currency)}}
						</strong>
					</td>
				</tr>
				<tr class="info">
					<td align="right"><strong>Total Payment</strong></td>
					<td align="right">
						<strong>
							{{DATA.current_booking.booking.currency}}
							({{fn.formatNumber(DATA.current_booking.booking.total_payment,DATA.current_booking.booking.currency)}})
						</strong>
					</td>
				</tr>
				<tr style="font-weight:bold" ng-class="{'danger':(DATA.current_booking.booking.balance>0), 'info':(DATA.current_booking.booking.balance<=0)}">
					<td align="right"><strong>Balance</strong></td>
					<td align="right">
						<strong>
							{{DATA.current_booking.booking.currency}}
							{{fn.formatNumber(DATA.current_booking.booking.balance,DATA.current_booking.booking.currency)}}
						</strong>
					</td>
				</tr>
			</table>
			
			<br />
			
			
			<br /><hr />
			
			<div class="modal fade" id="cancel-booking-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  	<form ng-submit='saveCancelation($event)'>
					<div class="modal-dialog" role="document">
						<div class="modal-content">
						  <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">
								Cancel Booking #{{DATA.current_booking.booking.booking_code}}
							</h4>
						  </div>
						  <div class="modal-body">
							<div ng-show='DATA.cancelation.error_desc.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.cancelation.error_desc'>{{err}}</li></ul></div>
							<table class="table table-borderless table-condensed">
								<tr>
									<td width="130">Cancellation Reason</td>
									<td>
										<textarea placeholder="Description" required="required" type="text" class="form-control input-md autoheight" ng-model='DATA.cancelation.cancelation_reason' rows="3"></textarea>
									</td>
								</tr>
							</table>
						  </div>
						  <div class="modal-footer" style="text-align:center">
							<button type="submit" class="btn btn-primary">Save</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						  </div>
						</div>
					</div>
				</form>
			</div>
			
			
			<?php /*?><a ui-sref="agent.edit({'agent_code':DATA.current_agent.agent_code})"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Agent</a><?php */?>
			<script>activate_sub_menu_agent_detail("detail");</script>
		</div>
	</div>
</div>
<style>
	.reservation-detail .title{margin-bottom:20px}
	.reservation-detail .title h1{margin-bottom:10px !important;}
	.reservation-detail .title .code{margin-bottom:5px;}
</style>
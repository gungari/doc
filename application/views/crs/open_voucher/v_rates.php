<div class="sub-title">Rates</div>
<br />

<div ng-init="loadDataOpenVoucherRates()">
	<div class="products">
		<div class="product ">
			Filter : <input type="text" ng-model="filter_categories" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
		</div>
		
		<div ng-show='!DATA.open_vouchers'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		</div>
		
		<table class="table table-bordered table-condensed" ng-show='DATA.open_vouchers.open_vouchers'>
			<tr class="info" style="font-weight:bold">
				<td width="40">No.</td>
				<td width="100">Code</td>
				<td>Name</td>
				<td width="150" align="center">Range of dates</td>
				<td width="100" align="right">Price</td>
				<td width="70" align="center">Min. Purchase</td>
				<td width="70"></td>
			</tr>
			<tr ng-class="{'danger':open_vouchers.publish_status!='1'}" ng-repeat="open_vouchers in DATA.open_vouchers.open_vouchers | filter : filter_categories ">
				<td>{{($index+1)}}</td>
				<td width="100" align="center">
					<a href="" data-toggle="modal" data-target="#add-edit-open-vouchers" ng-click="addEditOpenVouchersRates(open_vouchers)">{{open_vouchers.openvoucher_code}}</a>
				</td>
				<td>{{open_vouchers.name}}</td>
				<td align="center">{{fn.formatDate(open_vouchers.start_date, "dd M y")}} - {{fn.formatDate(open_vouchers.end_date, "dd M y")}}</td>
				<td align="right">{{open_vouchers.currency}} {{fn.formatNumber(open_vouchers.price, open_vouchers.currency)}}</td>
				<td align="center">{{open_vouchers.minimum_purchase}}</td>
				<td align="center">
					<a href="" ng-click="publishUnpublishOpenVoucherRates(open_vouchers, 0)" ng-show="open_vouchers.publish_status == '1'">
						<div style="color:#090"><img src="<?=base_url("public/images/on-button.png")?>" width="100%" /></span> </div>
					</a> 
					<a href="" ng-click="publishUnpublishOpenVoucherRates(open_vouchers, 1)" ng-show="open_vouchers.publish_status != '1'">
						<div style="color:#F30"><img src="<?=base_url("public/images/off-button.png")?>" width="100%" /></span></div>
					</a> 
				</td>
			</tr>
		</table>
	</div>
	<hr>
	<div class="add-product-button"> <a href="" class="btn btn-success btn-lg btn-block" data-toggle="modal" ng-click='addEditOpenVouchersRates()' data-target="#add-edit-open-vouchers"> <span class="glyphicon glyphicon-plus"></span> New Rates </a> </div>
	<br>
	<br>

	<div class="modal fade" id="add-edit-open-vouchers" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <form ng-submit='saveDataOpenVouchersRates($event)'>
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">
				<span ng-show='!DATA.myOpen_voucher.id'>Add</span><span ng-show='DATA.myOpen_voucher.id'>Edit</span> Rates
			</h4>
		  </div>
		  <div class="modal-body">
		  	<div ng-show='DATA.myOpen_voucher.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myOpen_voucher.error_msg'>{{err}}</li></ul></div>
			<table class="table table-borderless table-condensed">
				<tr>
					<td width="120">Name*</td>
					<td><input placeholder="Name" required="required" type="text" class="form-control input-md" ng-model='DATA.myOpen_voucher.name' /></td>
				</tr>
				<tr>
					<td>Description</td>
					<td>
						<textarea placeholder="Description" class="form-control input-md" ng-model='DATA.myOpen_voucher.description' rows="5"></textarea>
					</td>
				</tr>
				<tr>
					<td>Range of Dates*</td>
					<td class="form-inline">
						<input required="required" type="text" class="form-control datepicker start_date datepicker" placeholder="Start Date" style="width:150px" ng-model='DATA.myOpen_voucher.start_date' />
						-
						<input required="required" type="text" class="form-control datepicker end_date datepicker" placeholder="End Date" style="width:150px" ng-model='DATA.myOpen_voucher.end_date' />
					</td>
				</tr>
				<tr>
					<td>Price*</td>
					<td>
						<div class="input-group" style="width:200px">
							<span class="input-group-addon" id="basic-addon1"><?=$vendor["default_currency"]?></span>
						  	<input required="required" type="number" class="form-control " placeholder="Price" ng-model='DATA.myOpen_voucher.price' />
						</div>
					</td>
				</tr>
				<tr>
					<td>Minimum Purchase*</td>
					<td class="form-inline">
						<input required="required" type="number" class="form-control " placeholder="Minimum Purchase" style="width:100px" ng-model='DATA.myOpen_voucher.minimum_purchase' />
					</td>
				</tr>
			</table>
		  </div>
		  <div class="modal-footer" style="text-align:center">
		  	<button type="submit" class="btn btn-primary">Save</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
	  </div>
	  </form>
	</div>

</div>
<script>GeneralJS.activateSubMenu(".nav-pills", "li", ".rates");</script>

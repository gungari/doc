<div ng-init='printReceiptOpenVoucher()'>
	<div class="header">
		<div class="pull-right text-right">
			<br>
			<div style="font-size:18px">Receipt</div>
			<div class="title">
				#{{DATA.current_booking.booking.booking_code}}
			</div>
		</div>
		
		<div class="title"><?=$vendor["business_name"]?></div>
		<div><?=$vendor["address"]?>, <?=$vendor["regency"]?>, <?=$vendor["province"]?>, <?=$vendor["country"]?></div>
		<div>[P] : <?=$vendor["telephone"]?>, [E] : <?=$vendor["email"]?></div>
		<div><?=$vendor["website"]?></div>
	</div>
	
	<br />
	<table width="100%">
		<tr>
			<td width="50%" valign="top">
				<strong>BOOKING INFORMATION</strong>
				<table width="100%">
					<tr>
						<td width="70">Code</td>
						<td><strong>#{{DATA.current_booking.booking.booking_code}}</strong></td>
					</tr>
					<tr>
						<td>Date</td>
						<td><strong>{{fn.formatDate(DATA.current_booking.booking.transaction_date, "dd MM yy")}}</strong></td>
					</tr>
					<tr>
						<td>Status</td>
						<td class="text-capitalize"><strong>{{DATA.current_booking.booking.status.toLowerCase()}}</strong></td>
					</tr>
					<tr>
						<td>Source</td>
						<td class="text-capitalize"><strong>{{DATA.current_booking.booking.source.toLowerCase()}}</strong></td>
					</tr>
					<tr ng-show="DATA.current_booking.booking.agent">
						<td>Agent</td>
						<td class="text-capitalize"><strong>{{DATA.current_booking.booking.agent.name}}</strong></td>
					</tr>
				</table>
				<br>
				<strong>Remarks / Special Request</strong><br>
				<span ng-show="!DATA.current_booking.booking.remarks">-</span>
				<div ng-show="DATA.current_booking.booking.remarks" class="">
					{{DATA.current_booking.booking.remarks}}
				</div>
			</td>
			<td width="50%" valign="top">
				<strong>CUSTOMER INFORMATION</strong>
				<table width="100%">
					<tr>
						<td width="100">Full Name</td>
						<td><strong>{{DATA.current_booking.booking.customer.first_name}} {{DATA.current_booking.booking.customer.last_name}}</strong></td>
					</tr>
					<tr>
						<td>Email</td>
						<td><strong>{{DATA.current_booking.booking.customer.email}}</strong></td>
					</tr>
					<tr>
						<td>Country</td>
						<td><strong>{{DATA.current_booking.booking.customer.country_name}}</strong></td>
					</tr>
					<tr>
						<td>Telephone</td>
						<td><strong>{{DATA.current_booking.booking.customer.phone}}</strong></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br />
	<table class="table table-bordered table-condensed" width="100%" cellspacing="1" cellpadding="8">
		<tr class="info table-header">
			<td><strong>Description</strong></td>
			<td width="150" align="right"><strong>Sub Total</strong></td>
		</tr>
		<tbody ng-show='DATA.current_booking.booking.open_voucher_detail'>
			<tr>
				<td>
					<strong>Pre Paid Ticket</strong><br />
					{{DATA.current_booking.booking.open_voucher_detail.qty}} x 
					{{DATA.current_booking.booking.open_voucher_detail.rates_name}}
					-
					@ {{DATA.current_booking.booking.currency}} {{fn.formatNumber(DATA.current_booking.booking.open_voucher_detail.price, DATA.current_booking.booking.currency)}}
				</td>
				<td align="right">
					<strong>
						{{DATA.current_booking.booking.currency}}
						{{fn.formatNumber(DATA.current_booking.booking.open_voucher_detail.subtotal, DATA.current_booking.booking.currency)}}
					</strong>
				</td>
			</tr>
		</tbody>
		<tr class="success table-header">
			<td align="right"><strong>Total</strong></td>
			<td align="right">
				<strong>
					{{DATA.current_booking.booking.currency}}
					{{fn.formatNumber(DATA.current_booking.booking.total_before_discount,DATA.current_booking.booking.currency)}}
				</strong>
			</td>
		</tr>
		<tr class="success table-header">
			<td align="right">
				<strong>
					Discount 
					<span ng-show='DATA.current_booking.booking.discount.amount > 0'>
						&nbsp;&nbsp;
						<span ng-show="DATA.current_booking.booking.discount.type == '%'">({{DATA.current_booking.booking.discount.value}}%)</span>
						<span ng-show="DATA.current_booking.booking.discount.type != '%'">({{fn.formatNumber(DATA.current_booking.booking.discount.value,DATA.current_booking.booking.currency)}})</span>
					</span>
				</strong>
			</td>
			<td align="right">
				<strong>
					{{DATA.current_booking.booking.currency}}
					{{fn.formatNumber(DATA.current_booking.booking.discount.amount,DATA.current_booking.booking.currency)}}
				</strong>
			</td>
		</tr>
		<tr class="info table-header">
			<td align="right"><strong>Grand Total</strong></td>
			<td align="right">
				<strong>
					{{DATA.current_booking.booking.currency}}
					{{fn.formatNumber(DATA.current_booking.booking.grand_total,DATA.current_booking.booking.currency)}}
				</strong>
			</td>
		</tr>
		<tr class="info table-header">
			<td align="right"><strong>Total Payment</strong></td>
			<td align="right">
				<strong>
					{{DATA.current_booking.booking.currency}}
					({{fn.formatNumber(DATA.current_booking.booking.total_payment,DATA.current_booking.booking.currency)}})
				</strong>
			</td>
		</tr>
		<tr class="info table-header">
			<td align="right"><strong>Balance</strong></td>
			<td align="right">
				<strong>
					{{DATA.current_booking.booking.currency}}
					{{fn.formatNumber(DATA.current_booking.booking.balance,DATA.current_booking.booking.currency)}}
				</strong>
			</td>
		</tr>
	</table>
</div>

<?php
//pre($vendor)
?>

<div ng-init="smartPricingEdit();">
	<div class="products">
		<table class="table">
			<tr>
				<td width="130">Rates Name</td>
				<td><strong>{{DATA.current_rates.name}}</strong></td>
			</tr>
			<tr>
				<td>Code</td>
				<td><strong>{{DATA.current_rates.rates_code}}</strong></td>
			</tr>
			<tr>
				<td>Trip</td>
				<td>
					<strong>{{DATA.current_rates.departure.port.name}} ({{DATA.current_rates.departure.port.port_code}}) : {{DATA.current_rates.departure.time}}</strong>
					&nbsp;&nbsp;<i class="fa fa-chevron-right"></i>&nbsp;&nbsp;
					<strong>{{DATA.current_rates.arrival.port.name}} ({{DATA.current_rates.arrival.port.port_code}}) : {{DATA.current_rates.arrival.time}}</strong>
				</td>
			</tr>
		</table>
	</div>
	
	
	<br />
	
	<div class="sub-title">
		<span class="glyphicon glyphicon-signal"></span> Smart Pricing
	</div>
	<br />
	
	<div ng-show="DATA.current_rates && (!DATA.current_rates.use_smart_pricing || DATA.current_rates.use_smart_pricing != 'YES')">
		<div class="alert alert-warning">
			<h4>Activate Smart Pricing</h4>
			
			<button id="btn-activate-smart-pricing" class="btn btn-info pull-right" ng-click="enableDisableSmartPricing(DATA.current_rates, '1')"><span class="glyphicon glyphicon-ok"></span> Activate Now</button>
			<br />
			Click this button to activate Smart Pricing : 
		</div>
	</div>
	
	
	<div ng-show="DATA.current_rates.use_smart_pricing == 'YES'">
		<div>
			<div class="pull-right">
				<button type="button" class="btn btn-info btn-sm" data-toggle="modal" ng-click='editSmartPricing()' data-target="#add-edit-smart-pricing">Update Rates</button>
			</div>
			
			<form ng-submit='smartPricingEdit()' class="form-inline" style="margin-bottom:10px">
				Period &nbsp;&nbsp;
				<input type="text" required="required" ng-model='DATA.smart_pricing.search.start_date' class="form-control input-sm datepicker" placeholder="Start Date" style="width:130px; text-align:center; display:inline" /> -
				<input type="text" required="required" ng-model='DATA.smart_pricing.search.end_date' class="form-control input-sm datepicker" placeholder="End Date" style="width:130px; text-align:center; display:inline" />
				<button type="submit" class="btn btn-info btn-sm">OK</button>
			</form>

		</div>
		<br />
		<table class="table table-bordered table-condensed">
			<tr class="header bold">
				<td rowspan="2" align="center" width="100">Date</td>
				<td rowspan="2" width="30" align="center">#</td>
				<td rowspan="2" width="30" align="center"></td>
				<td colspan="3">One Way</td>
				<td colspan="3">Return</td>
				<td rowspan="2" align="center" width="50">Qty.</td>
			</tr>
			<tr class="header bold">
				<td align="right">Adult</td>
				<td align="right">Child</td>
				<td align="right">Infant</td>
				<td align="right">Adult</td>
				<td align="right">Child</td>
				<td align="right">Infant</td>
			</tr>
			
			<tbody ng-repeat='pricing in DATA.smart_pricing.smart_pricing'>
				<tr ng-class="{'warning':pricing.pricing_level[0].allotment == 0}">
					<td rowspan="{{pricing.pricing_level.length}}" align="center" style="background:#FFF">
						{{pricing.date | date:'dd MMM yyyy'}}<br />
						<a href="" data-toggle="modal" ng-click='editSmartPricing(pricing)' data-target="#add-edit-smart-pricing">Edit</a>
					</td>
					<td>1</td>
					<td>{{pricing.pricing_level[0].currency}}</td>
					<td align="right">{{fn.formatNumber(pricing.pricing_level[0].rates.one_way_rates.rates_1, pricing.pricing_level[0].currency)}}</td>
					<td align="right">{{fn.formatNumber(pricing.pricing_level[0].rates.one_way_rates.rates_2, pricing.pricing_level[0].currency)}}</td>
					<td align="right">{{fn.formatNumber(pricing.pricing_level[0].rates.one_way_rates.rates_3, pricing.pricing_level[0].currency)}}</td>
					<td align="right">{{fn.formatNumber(pricing.pricing_level[0].rates.return_rates.rates_1*2, pricing.pricing_level[0].currency)}}</td>
					<td align="right">{{fn.formatNumber(pricing.pricing_level[0].rates.return_rates.rates_2*2, pricing.pricing_level[0].currency)}}</td>
					<td align="right">{{fn.formatNumber(pricing.pricing_level[0].rates.return_rates.rates_3*2, pricing.pricing_level[0].currency)}}</td>
					<td align="center">{{pricing.pricing_level[0].allotment}}</td>
				</tr>
				<tr ng-repeat='pricing_level in pricing.pricing_level' ng-show='$index>0' ng-class="{'warning':pricing_level.allotment==0}">
					<td>{{(pricing_level.level+1)}}</td>
					<td>{{pricing_level.currency}}</td>
					<td align="right">{{fn.formatNumber(pricing_level.rates.one_way_rates.rates_1, pricing_level.currency)}}</td>
					<td align="right">{{fn.formatNumber(pricing_level.rates.one_way_rates.rates_2, pricing_level.currency)}}</td>
					<td align="right">{{fn.formatNumber(pricing_level.rates.one_way_rates.rates_3, pricing_level.currency)}}</td>
					<td align="right">{{fn.formatNumber(pricing_level.rates.return_rates.rates_1*2, pricing_level.currency)}}</td>
					<td align="right">{{fn.formatNumber(pricing_level.rates.return_rates.rates_2*2, pricing_level.currency)}}</td>
					<td align="right">{{fn.formatNumber(pricing_level.rates.return_rates.rates_3*2, pricing_level.currency)}}</td>
					<td align="center">{{pricing_level.allotment}}</td>
				</tr>
				<tr>
					<td colspan="10" style="background:#FAFAFA"></td>
				</tr>
			</tbody>
		</table>
		
		<hr />
		<a href="" ng-click="enableDisableSmartPricing(DATA.current_rates, '0')"><span class="glyphicon glyphicon-remove"></span> Disable Smart Pricing</a>
	</div>
	
	<div class="modal fade" id="add-edit-smart-pricing" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <form ng-submit='saveDataSmartPricing($event)'>
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">
				Update Smart Pricing
			</h4>
		  </div>
		  <div class="modal-body">
		  	<div ng-show='currentSmartPricing.error_desc.length>0' class="alert alert-danger"><ul><li ng-repeat='err in currentSmartPricing.error_desc'>{{err}}</li></ul></div>
			<table class="table table-borderless table-condensed form-inline" style="margin-bottom:10px">
				<tbody><tr>
					<td width="50%">
						Start Date
						<input type="text" placeholder="Start Date" ng-model="currentSmartPricing.start_date" required="required" class="form-control input-sm datepicker" style="width:150px">
					</td>
					<td>
						End Date
						<input type="text" placeholder="End Date" ng-model="currentSmartPricing.end_date" required="required" class="form-control input-sm datepicker" style="width:150px">
					</td>
				</tr>
			</tbody></table>
			<table class="table table-bordered table-condensed" style="margin-bottom:5px">
				<tr class="success" style="font-weight:bold">
					<td width="30" align="center">#</td>
					<td width="80" align="center"></td>
					<td align="center">Adult</td>
					<td align="center">Child</td>
					<td align="center">Infant</td>
					<td align="center" width="80">Qty.</td>
					<td align="center" width="30"></td>
				</tr>
				<tbody ng-repeat="levelRates in currentSmartPricing.level">
					<tr>
						<td rowspan="2"  align="center">{{($index+1)}}</td>
						<td>One Way</td>
						<td><input type="number" placeholder="Adult" required="required" min="0" class="form-control input-sm" ng-model="levelRates.oneway.rates_1" /></td>
						<td><input type="number" placeholder="Child" required="required" min="0" class="form-control input-sm" ng-model="levelRates.oneway.rates_2" /></td>
						<td><input type="number" placeholder="Infant" required="required" min="0" class="form-control input-sm" ng-model="levelRates.oneway.rates_3" /></td>
						<td rowspan="2"><input type="number" required="required" min="0" max="200" class="form-control input-sm" ng-model="levelRates.allotment" /></td>
						<td rowspan="2" align="center">
							<a href="#" style="color:red" onclick="return false" ng-click="removeSmartPricingAddLevel($index)">
								<span class="glyphicon glyphicon-remove"></span>
							</a>
						</td>
					</tr>
					<tr>
						<td>Return</td>
						<td><input type="number" placeholder="Adult" required="required" min="0" class="form-control input-sm" ng-model="levelRates.return.rates_1" /></td>
						<td><input type="number" placeholder="Child" required="required" min="0" class="form-control input-sm" ng-model="levelRates.return.rates_2" /></td>
						<td><input type="number" placeholder="Infant" required="required" min="0" class="form-control input-sm" ng-model="levelRates.return.rates_3" /></td>
					</tr>
				</tbody>
			</table>
			<div style="text-align:right">
				<button type="button" class="btn btn-xs btn-info" ng-click="addSmartPricingAddLevel()">Add Item</button>
			</div>
		  </div>
		  <div class="modal-footer" style="text-align:center">
		  	<button type="submit" class="btn btn-primary">Save</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
	  </div>
	  </form>
	</div>
	
	<script>activate_sub_menu_schedule_detail("rates");</script>

</div>
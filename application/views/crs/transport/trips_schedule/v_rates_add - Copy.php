<div class="sub-title">
	<span ng-show='!DATA.current_rates.rates_code'><i class="fa fa-plus-square" aria-hidden="true"></i> Add New</span> 
	<span ng-show='DATA.current_rates.rates_code'><i class="fa fa-pencil" aria-hidden="true"></i> Edit</span>
	Rates
</div>
<br />

<div ng-init="ratesAddEdit();">
	<div class="products">
		<form ng-submit="saveDataScheduleRates($event)">
			<?php /*?><div ng-show='!DATA.current_rates'>
				<img src="<?=base_url("public/images/loading_bar.gif")?>" />
			</div><?php */?>
			
			<div ng-show='DATA.current_rates.error_desc.length>0' class="alert alert-danger show_error"><ul><li ng-repeat='err in DATA.current_rates.error_desc'>{{err}}</li></ul></div>
			
			<table width="100%" class="table td_center_align form-inline">
				<tr>
					<td width="150">Rates Name*</td>
					<td>
						<input required="required" placeholder="Rates Name" type="text" class="form-control input-sm txa_mxlen" data-mxlen="90" maxlength="90" style="width:90%" ng-model='DATA.current_rates.name' />
						<span class="dv_mxlen_remain">90/90</span>
					</td>
				</tr>
				<tr>
					<td>Departure Port*</td>
					<td>
						<select required="required" class="form-control input-sm" style="width:50%" ng-model='DATA.current_rates.departure_trip_number'>
							<option value="">-- Departure Port --</option>
							<option value="{{trip.trip_number}}" ng-repeat='trip in DATA.schedule_detail.schedule_detail'>{{trip.departure_port.port_code}} - {{trip.departure_port.name}} : {{trip.departure_time}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Arrival Port*</td>
					<td>
						<select required="required" class="form-control input-sm" style="width:50%" ng-model='DATA.current_rates.arrival_trip_number'>
							<option value="">-- Arrival Port --</option>
							<option value="{{trip.trip_number}}" ng-repeat='trip in DATA.schedule_detail.schedule_detail'>{{trip.arrival_port.port_code}} - {{trip.arrival_port.name}} : {{trip.arrival_time}}</option>
						</select>
					</td>
				</tr>
				<tr id="tr_rod">
					<td>Range of dates*</td>
					<td>
						<input required="required" type="text" class="form-control input-sm datepicker start_date datepicker" placeholder="Start Date" style="width:150px" ng-model='DATA.current_rates.start_date' />
						-
						<input required="required" type="text" class="form-control input-sm datepicker end_date datepicker" placeholder="End Date" style="width:150px" ng-model='DATA.current_rates.end_date' />
					</td>
				</tr>
				<tr>
					<td style="vertical-align:top !important">Sell Rate</td>
					<td>
						<div class="sub-title"><label style="margin:0"><input type="checkbox" checked="checked" disabled="disabled"> One Way Rates</label></div>
						<table width="100%" class="table-borderless table-condensed">
							<tr>
								<td>Currency</td>
								<td>Adult*</td>
								<td>Child*</td>
								<td>Infant*</td>
							</tr>
							<tr>
								<td width="19%">
									<input type="text" disabled="disabled" class="form-control input-sm" style="width:100%; text-align:center" value="<?=@$vendor['default_currency']?>" />
								</td>
								<td width="27%">
									<input type="number" required="required" min="0" step="any" class="form-control input-sm" placeholder="Sell Rate Adult " ng-model='DATA.current_rates.rates.one_way_rates.rates_1' style="width:100%" />
								</td>
								<td width="27%">
									<input type="number" required="required" min="0" step="any" class="form-control input-sm" placeholder="Sell Rate Child " ng-model='DATA.current_rates.rates.one_way_rates.rates_2' style="width:100%" />
								</td>
								<td width="27%">
									<input type="number" required="required" min="0" step="any" class="form-control input-sm" placeholder="Sell Rate Infant" ng-model='DATA.current_rates.rates.one_way_rates.rates_3' style="width:100%" />
								</td>
							</tr>
						</table> 
						<hr />
						<div class="sub-title"><label style="margin:0"><input type="checkbox" ng-true-value="1" ng-false-value="0" ng-model='DATA.current_rates.is_rates_return_active' /> Return Rates</label></div>
						<table width="100%" class="table-borderless table-condensed" ng-show='DATA.current_rates.is_rates_return_active'>
							<tr>
								<td>Currency</td>
								<td>Adult</td>
								<td>Child</td>
								<td>Infant</td>
							</tr>
							<tr>
								<td width="19%">
									<input type="text" disabled="disabled" class="form-control input-sm" style="width:100%; text-align:center" value="<?=@$vendor['default_currency']?>">
								</td>
								<td width="27%">
									<input type="number" ng-disabled='!DATA.current_rates.is_rates_return_active' min="0" step="any" class="form-control input-sm" placeholder="Sell Rate Adult " ng-model='DATA.current_rates.rates.return_rates.rates_1' style="width:100%" />
								</td>
								<td width="27%">
									<input type="number" ng-disabled='!DATA.current_rates.is_rates_return_active' min="0" step="any" class="form-control input-sm" placeholder="Sell Rate Child " ng-model='DATA.current_rates.rates.return_rates.rates_2' style="width:100%" />
								</td>
								<td width="27%">
									<input type="number" ng-disabled='!DATA.current_rates.is_rates_return_active' min="0" step="any" class="form-control input-sm" placeholder="Sell Rate Infant" ng-model='DATA.current_rates.rates.return_rates.rates_3' style="width:100%" />
								</td>
							</tr>
						</table>
						<br />
					</td>
				</tr>
				<tr>
					<td style="vertical-align:top !important">Pickup Service</td>
					<td>
						<label><input type="radio" value="no" class="pickup_service" ng-model='DATA.current_rates.pickup_service' /> Not Available </label> &nbsp;&nbsp;&nbsp;
						<label><input type="radio" value="yes" class="pickup_service" ng-model='DATA.current_rates.pickup_service' /> Available</label> &nbsp;&nbsp;&nbsp;
						<?php /*?><label><input name="dt[f_pickup_service]" type="radio" class="f_pickup_service" value="BUS" onclick="chk_tbl_pickup_service()"> Shuttle Bus</label> &nbsp;&nbsp;&nbsp;
						<label><input name="dt[f_pickup_service]" type="radio" class="f_pickup_service" value="CUST" onclick="chk_tbl_pickup_service()"> Custom</label><?php */?>
						
						<div ng-show='DATA.current_rates.pickup_service=="yes"'>
							<table class="table table-condensed table-hover" style="margin:0">
								<tr class="successs">
									<th width="200">Area</th>
									<th width="100">Time</th>
									<th colspan="2">Price (<?=@$vendor['default_currency']?>)</th>
									<th></th>
								</tr>
								<tr ng-repeat='area in DATA.current_rates.pickup_area'>
									<td><input ng-model='area.area' ng-required='DATA.current_rates.pickup_service=="yes"' maxlength="100" type="text" class="form-control  input-sm" placeholder="Area" style="width:100%" /></td>
									<td><input ng-model='area.time' ng-required='DATA.current_rates.pickup_service=="yes"' maxlength="25" type="text" class="form-control  input-sm" placeholder="Time" style="width:100%" /></td>
									<td width="150"><input ng-model='area.price' ng-required='DATA.current_rates.pickup_service=="yes"' maxlength="25" type="number" step="0.1" placeholder="Price" class="form-control input-sm" style="width:100%" /></td>
									<td width="90">
										<select ng-model='area.type' ng-required='DATA.current_rates.pickup_service=="yes"' class="form-control input-sm" style="width:100%">
											<option value="way">/ way</option>
											<option value="pax">/ pax</option>
										</select>
									</td>
									<td>
										<a href="" ng-click="DATA.current_rates.pickup_area.splice($index,1)" title="Remove"><span class="glyphicon glyphicon-trash"></span></a>
									</td>
								</tr>
							</table>
							<button type="button" class="btn btn-info btn-xs" ng-click="ratesAddPickUpOrDropOffArea(DATA.current_rates.pickup_area)"><span class="glyphicon glyphicon-plus"></span> Add Item </button>
						</div>
					</td>
				</tr>
				<tr>
					<td style="vertical-align:top !important">Dropoff Service</td>
					<td>
						<label><input type="radio" value="no" class="dropoff_service" ng-model='DATA.current_rates.dropoff_service' /> Not Available </label> &nbsp;&nbsp;&nbsp;
						<label><input type="radio" value="yes" class="dropoff_service" ng-model='DATA.current_rates.dropoff_service' /> Available</label> &nbsp;&nbsp;&nbsp;
						<?php /*?><label><input name="dt[f_pickup_service]" type="radio" class="f_pickup_service" value="BUS" onclick="chk_tbl_pickup_service()"> Shuttle Bus</label> &nbsp;&nbsp;&nbsp;
						<label><input name="dt[f_pickup_service]" type="radio" class="f_pickup_service" value="CUST" onclick="chk_tbl_pickup_service()"> Custom</label><?php */?>
						
						<div ng-show='DATA.current_rates.dropoff_service=="yes"'>
							<table class="table table-condensed table-hover" style="margin:0">
								<tr class="successs">
									<th width="200">Area</th>
									<th width="100">Time</th>
									<th colspan="2">Price (<?=@$vendor['default_currency']?>)</th>
									<th></th>
								</tr>
								<tr ng-repeat='area in DATA.current_rates.dropoff_area'>
									<td><input ng-model='area.area' ng-required='DATA.current_rates.dropoff_service=="yes"' maxlength="100" type="text" class="form-control  input-sm" placeholder="Area" style="width:100%" /></td>
									<td><input ng-model='area.time' ng-required='DATA.current_rates.dropoff_service=="yes"' maxlength="25" type="text" class="form-control  input-sm" placeholder="Time" style="width:100%" /></td>
									<td width="150"><input ng-model='area.price' ng-required='DATA.current_rates.dropoff_service=="yes"' maxlength="25" type="number" step="0.1" placeholder="Price" class="form-control input-sm" style="width:100%" /></td>
									<td width="90">
										<select ng-model='area.type' ng-required='DATA.current_rates.dropoff_service=="yes"' class="form-control input-sm" style="width:100%">
											<option value="way">/ way</option>
											<option value="pax">/ pax</option>
										</select>
									</td>
									<td>
										<a href="" ng-click="DATA.current_rates.dropoff_area.splice($index,1)" title="Remove"><span class="glyphicon glyphicon-trash"></span></a>
									</td>
								</tr>
							</table>
							<button type="button" class="btn btn-info btn-xs" ng-click="ratesAddPickUpOrDropOffArea(DATA.current_rates.dropoff_area)"><span class="glyphicon glyphicon-plus"></span> Add Item </button>
						</div>
					</td>
				</tr>
				<tr>
					<td>Minimum Order*</td>
					<td>
						<div class="input-group">
							<input required="required" type="number" min="1" class="form-control input-sm" style="width:100px" ng-model='DATA.current_rates.min_order' />
							<select required="required" name="pax_or_paket" id="pax_or_paket" class="form-control input-sm" style="width:100px">
								<option value="pax" selected="selected">Person</option>
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<td>Cut Off Booking*</td>
					<td>
						<input type="number" min="0" required="required" class="form-control input-sm" style="width:100px" maxlength="2" ng-model='DATA.current_rates.cut_of_booking' /> 
						<span>days in advance</span>
					</td>
				</tr>
				<tr>
					<td>Booking Handling</td>
					<td>
						<select class="form-control input-sm" style="width:50%" ng-model='DATA.current_rates.booking_handling'>
							<option value="INSTANT">Instant Booking</option>
							<option value="ENQUIRY">Enquiry Booking</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Available On</td>
					<td class="form-inline">
						<div class="btn-group btn-days">
							<button type="button" class="btn btn-sm" ng-click="ratesCheckAvailableDayInWeek('mon')" ng-class="{'btn-info':DATA.current_rates.available_on.indexOf('mon') >= 0}">Mon</button>
							<button type="button" class="btn btn-sm" ng-click="ratesCheckAvailableDayInWeek('tue')" ng-class="{'btn-info':DATA.current_rates.available_on.indexOf('tue') >= 0}">Tue</button>
							<button type="button" class="btn btn-sm" ng-click="ratesCheckAvailableDayInWeek('wed')" ng-class="{'btn-info':DATA.current_rates.available_on.indexOf('wed') >= 0}">Wed</button>
							<button type="button" class="btn btn-sm" ng-click="ratesCheckAvailableDayInWeek('thu')" ng-class="{'btn-info':DATA.current_rates.available_on.indexOf('thu') >= 0}">Thu</button>
							<button type="button" class="btn btn-sm" ng-click="ratesCheckAvailableDayInWeek('fri')" ng-class="{'btn-info':DATA.current_rates.available_on.indexOf('fri') >= 0}">Fri</button>
							<button type="button" class="btn btn-sm" ng-click="ratesCheckAvailableDayInWeek('sat')" ng-class="{'btn-info':DATA.current_rates.available_on.indexOf('sat') >= 0}">Sat</button>
							<button type="button" class="btn btn-sm" ng-click="ratesCheckAvailableDayInWeek('sun')" ng-class="{'btn-info':DATA.current_rates.available_on.indexOf('sun') >= 0}">Sun</button>
						</div>
					</td>
				</tr>
				<tr>
					<td style="vertical-align:top !important">Inclusion</td>
					<td>
						<div class="form-inline important-infoEN" style="background:#FAFAFA; border:solid 1px #EEE; padding:10px">
							<div class="important-info-list" data-id="EN-1">
								<ul style="margin:5px 0 10px 15px; padding:0" class="sortable ui-sortable">
									<li style="margin-bottom: 5px;" ng-repeat='str in DATA.current_rates.inclusion'>
										<input type="text" required="required" class="form-control input-sm input-sm" ng-model='str.str' style="width:90%" maxlength="255"> 
										<a href="" ng-click="DATA.current_rates.inclusion.splice($index,1)" title="Remove"><span class="glyphicon glyphicon-trash"></span></a>
									</li>
								</ul>
								<button type="button" class="btn btn-info btn-xs" ng-click="ratesAddImportantInfo(DATA.current_rates.inclusion)"><span class="glyphicon glyphicon-plus"></span> Add Item </button>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td style="vertical-align:top !important">Exclusion</td>
					<td>
						<div class="form-inline important-infoEN" style="background:#FAFAFA; border:solid 1px #EEE; padding:10px">
							<div class="important-info-list" data-id="EN-1">
								<ul style="margin:5px 0 10px 15px; padding:0" class="sortable ui-sortable">
									<li style="margin-bottom: 5px;" ng-repeat='str in DATA.current_rates.exclusion'>
										<input type="text" required="required" class="form-control input-sm input-sm" ng-model='str.str' style="width:90%" maxlength="255"> 
										<a href="" ng-click="DATA.current_rates.exclusion.splice($index,1)" title="Remove"><span class="glyphicon glyphicon-trash"></span></a>
									</li>
								</ul>
								<button type="button" class="btn btn-info btn-xs" ng-click="ratesAddImportantInfo(DATA.current_rates.exclusion)"><span class="glyphicon glyphicon-plus"></span> Add Item </button>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td style="vertical-align:top !important">Additional Info</td>
					<td>
						<div class="form-inline important-infoEN" style="background:#FAFAFA; border:solid 1px #EEE; padding:10px">
							<div class="important-info-list" data-id="EN-1">
								<ul style="margin:5px 0 10px 15px; padding:0" class="sortable ui-sortable">
									<li style="margin-bottom: 5px;" ng-repeat='str in DATA.current_rates.additional_info'>
										<input type="text" required="required" class="form-control input-sm input-sm" ng-model='str.str' style="width:90%" maxlength="255"> 
										<a href="" ng-click="DATA.current_rates.additional_info.splice($index,1)" title="Remove"><span class="glyphicon glyphicon-trash"></span></a>
									</li>
								</ul>
								<button type="button" class="btn btn-info btn-xs" ng-click="ratesAddImportantInfo(DATA.current_rates.additional_info)"><span class="glyphicon glyphicon-plus"></span> Add Item </button>
							</div>
						</div>
					</td>
				</tr>
				<tr >
					<td style="vertical-align:top !important">Exception Date</td>
					<td>
                	<div  class="form-inline other-exception-dates" style="background:#FAFAFA; border:solid 1px #EEE; padding:10px">
	                    <ul style="margin:5px 0 10px 15px; padding:0">
	               <!-- 	ng-repeat="date in DATA.date_picker" -->
		                    <li ng-repeat="date in DATA.date_picker" ng-click="loadDate($index)" style="margin-bottom:5px">

		                    	<input  required="required" type="text" id="{{$index+1}}" class="form-control input-sm datepicker start_date{{$index}} datepicker" placeholder="Exception Date" style="width:100px" ng-model='date.date' />
		                      	<a href="" style="color:red" ng-click="remove($index)"><i class="fa fa-remove" aria-hidden="true"></i></a>
		                    </li>
		                    <li  ng-show="range=='1'" style="margin-bottom:5px">
		                      <input type="text" class="form-control input-sm datepicker start_date datepicker" placeholder="Start Date" style="width:100px" ng-model='start_date' ng-change="loadDateChange(start_date)" />
		                      <input type="text" class="form-control input-sm datepicker end_date datepicker" placeholder="Finish Date" style="width:100px" ng-model='end_date' />
		                      <a href="" style="color:red" onclick="$(this).closest('li').remove();" ><i class="fa fa-remove" aria-hidden="true"></i></a>
		                    </li>
	                  
	                    </ul>
                	</div>
                    <button type="button" class="btn btn-info btn-xs" ng-click="addItem($event, 0)"><span class="glyphicon glyphicon-plus"></span> Single Date </button>
                    <button type="button" class="btn btn-info btn-xs" ng-click="addItem($event, 1)"><span class="glyphicon glyphicon-plus range"></span> Range Date </button>
            		</td>

				</tr>
				<tr>
					<td></td>
					<td>
						<button type="submit" class="btn btn-lg btn-primary">Save Rates</button>
						&nbsp;&nbsp;&nbsp;
						<a href="#/transport/trips_schedule_detail/{{DATA.schedule_detail.schedule_code}}/rates"><strong>Cancel</strong></a>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>

<script>activate_sub_menu_schedule_detail("rates");</script>

<?php /*?>

<br />
<div ng-init="scheduleAddEdit();">
	<div class="products">
		<div ng-show='!(DATA.boats && DATA.port_list)'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		</div>
		
		<div ng-show='(DATA.boats && DATA.port_list)'>
			<form ng-submit="saveDataSchedule($event)">
				<div ng-show='DATA.current_schedule.error_desc.length>0' class="alert alert-danger show_error"><ul><li ng-repeat='err in DATA.current_schedule.error_desc'>{{err}}</li></ul></div>
				<table class="table table-borderlesss">
					<tr>
						<td width="150">Name</td>
						<td>
							<input type="text" class="form-control" placeholder="Schedule Name" required="required" ng-model='DATA.current_schedule.name' />
						</td>
					</tr>
					<tr>
						<td>Boat</td>
						<td>
							<select class="form-control" placeholder="Schedule Name" required="required" ng-model='DATA.current_schedule.boat_id'>
								<option value="">--Select Boat--</option>
								<option value="{{boat.id}}" ng-repeat='boat in DATA.boats.boats'>{{boat.name}}</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Description</td>
						<td>
							<textarea class="form-control autoheight" placeholder="Description" ng-model='DATA.current_schedule.description' rows="3"></textarea>
						</td>
					</tr>
					<tr>
						<td>Schedule</td>
						<td>
							<table class="table table-bordered">
								<tr class="info">
									<th width="50%" colspan="2" class="text-center">Departure</th>
									<th width="50%" colspan="2" class="text-center">Arrival</th>
									<th width="50"></th>
								</tr>
								<tr class="info">
									<th class="text-center">Port</th>
									<th class="text-center">Time</th>
									<th class="text-center">Port</th>
									<th class="text-center">Time</th>
									<th></th>
								</tr>
								<tr ng-repeat="schedule_detail in DATA.current_schedule.schedule_detail">
									<td width="30%">
										<select class="form-control input-sm" ng-disabled='$index>0' placeholder="Schedule Name" required="required" ng-model='schedule_detail.departure_port_id'>
											<option value="">--Select Port--</option>
											<option value="{{port.id}}" ng-repeat='port in DATA.port_list.ports'>{{port.port_code}} - {{port.name}}</option>
										</select>
									</td>
									<td width="20%">
										<select class="form-control input-sm" style="width:45%; display:inline; padding:5px 3px;" required="required" ng-model='schedule_detail.departure_time_hh'>
											<option value="" disabled="disabled">hh</option>
											<option value="" disabled="disabled">--</option>
											<?php for ($i=0; $i<24; $i++){ $str_i = str_repeat("0", (2-strlen($i))).$i; ?>
											<option value="<?=$str_i?>"><?=$str_i?></option>
											<?php } ?>
										</select>
										:
										<select class="form-control input-sm" style="width:45%; display:inline; padding:5px 3px;" required="required" ng-model='schedule_detail.departure_time_mm'>
											<option value="" disabled="disabled">mm</option>
											<option value="" disabled="disabled">--</option>
											<?php for ($i=0; $i<60; $i++){ $str_i = str_repeat("0", (2-strlen($i))).$i; ?>
											<option value="<?=$str_i?>"><?=$str_i?></option>
											<?php } ?>
										</select>
									</td>
									<td width="30%">
										<select class="form-control input-sm" placeholder="Schedule Name" required="required" ng-model='schedule_detail.arrival_port_id' ng-change="changeArrivalPort($index)">
											<option value="">--Select Port--</option>
											<option value="{{port.id}}" ng-repeat='port in DATA.port_list.ports'>{{port.port_code}} - {{port.name}}</option>
										</select>
									</td>
									<td width="20%">
										<select class="form-control input-sm" style="width:45%; display:inline; padding:5px 3px;" required="required" ng-model='schedule_detail.arrival_time_hh'>
											<option value="" disabled="disabled">hh</option>
											<option value="" disabled="disabled">--</option>
											<?php for ($i=0; $i<24; $i++){ $str_i = str_repeat("0", (2-strlen($i))).$i; ?>
											<option value="<?=$str_i?>"><?=$str_i?></option>
											<?php } ?>
										</select>
										:
										<select class="form-control input-sm" style="width:45%; display:inline; padding:5px 3px;" required="required" ng-model='schedule_detail.arrival_time_mm'>
											<option value="" disabled="disabled">mm</option>
											<option value="" disabled="disabled">--</option>
											<?php for ($i=0; $i<60; $i++){ $str_i = str_repeat("0", (2-strlen($i))).$i; ?>
											<option value="<?=$str_i?>"><?=$str_i?></option>
											<?php } ?>
										</select>
									</td>
									<td>
										<a href="" style="color:red" ng-click="scheduleDetailRemoveLastItem()" ng-show='($index+1)==DATA.current_schedule.schedule_detail.length'><i class="fa fa-remove" aria-hidden="true"></i></a>
									</td>
								</tr>
							</table>
							<button type="button" class="btn btn-info btn-xs" ng-click='scheduleDetailAddItem()'>Add item</button>
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<button type="submit" class="btn btn-primary">Save Schedule</button>
							&nbsp;&nbsp;&nbsp;
							<a ng-show='!DATA.current_schedule.schedule_code' ui-sref="transport.trips_schedule"><strong>Cancel</strong></a>
							<a ng-show='DATA.current_schedule.schedule_code' ui-sref="transport.trips_schedule_detail({'schedule_code':DATA.current_schedule.schedule_code})"><strong>Cancel</strong></a>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>

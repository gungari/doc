<div class="sub-title"> Schedule Detail </div>

<br />
<div ng-init="loadDataScheduleDetail();" class="schedule-detail">
	<div class="products">
		<div ng-show='!(DATA.current_schedule)'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		</div>
		
		<div ng-show='(DATA.current_schedule)'>
			<div class="title">
				<?php /*?><h1>{{DATA.current_schedule.name}}</h1><?php */?>
				<h1>Code : {{DATA.current_schedule.schedule_code}}</h1>
				<?php /*?><div class="code"> Code : {{DATA.current_schedule.schedule_code}}</div><?php */?>
				<div class="code"> <i class="fa fa-ship" aria-hidden="true"></i> Boat : {{DATA.current_schedule.boat.name}}</div>
				<div ng-show='DATA.current_schedule.description'>{{DATA.current_schedule.description}}</div>
			</div>

			<ul class="nav nav-tabs sub-nav">
			  	<li role="presentation" class="description"><a ui-sref="transport.trips_schedule_detail({'schedule_code':DATA.current_schedule.schedule_code})">Description</a></li>
			  	<li role="presentation" class="boat_and_inventory"><a ui-sref="transport.trips_schedule_detail.boat_and_inventory">Boat & Inventory</a></li>
			  	<li role="presentation" class="rates"><a ui-sref="transport.trips_schedule_detail.rates">Rates</a></li>
			</ul>
			<br /><br />
			<div ui-view>
				<div class="sub-title">Description</div>
				<table class="table">
					<?php /*?><tr>
						<td width="120">Name</td>
						<td><strong>{{DATA.current_schedule.name}}</strong></td>
					</tr><?php */?>
					<tr>
						<td width="120">Code</td>
						<td><strong>{{DATA.current_schedule.schedule_code}}</strong></td>
					</tr>
					<tr>
						<td>Boat</td>
						<td>
							<div style="width:100px; padding:0 10px 0 0" class="pull-left">
								<img ng-src="{{DATA.current_schedule.boat.main_image}}" width="100%" />
							</div>
							<div style="width:150px"></div>
							<strong>{{DATA.current_schedule.boat.name}}</strong><br />
							{{DATA.current_schedule.boat.capacity}} seats
							{{xxDATA.current_schedule.boat}}
						</td>
					</tr>
					<tr>
						<td>Description</td>
						<td>
							<div ng-show="DATA.current_schedule.description == ''">-</div>
							{{DATA.current_schedule.description}}
						</td>
					</tr>
				</table>
				
				<br />
				<div class="sub-title">Schedule</div>
				<table class="table">
					<tr class="infoss">
						<th style="text-align:center" width="50">No.</th>
						<th colspan="2" style="text-align:center">Departure</th>
						<th></th>
						<th colspan="2" style="text-align:center">Arrival</th>
					</tr>
					<tr ng-repeat='schedule_detail in DATA.current_schedule.schedule_detail'>
						<td align="center">{{$index+1}}</td>
						<td>{{schedule_detail.departure_port.port_code}} - {{schedule_detail.departure_port.name}}</td>
						<td width="100px" align="center">{{schedule_detail.departure_time}}</td>
						<td align="center"> <i class="fa fa-chevron-right" aria-hidden="true"></i> </td>
						<td>{{schedule_detail.arrival_port.port_code}} - {{schedule_detail.arrival_port.name}}</td>
						<td width="100px" align="center">{{schedule_detail.arrival_time}}</td>
					</tr>
				</table>
				<script>activate_sub_menu_schedule_detail("description");</script>
				<br /><hr />
				<a ui-sref="transport.trips_schedule_edit({'schedule_code_edit':DATA.current_schedule.schedule_code})"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Schedule</a>
			</div>
			
		</div>
	</div>
</div>

<style>
	.schedule-detail .title{margin-bottom:20px}
	.schedule-detail .title h1{margin-bottom:10px !important;}
	.schedule-detail .title .code{margin-bottom:5px;}
</style>
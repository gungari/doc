<style>
	.boat-and-inventory .tbl-boat-and-inventory td{position:relative}
	.boat-and-inventory .tbl-boat-and-inventory td .dropdown{top:0; right:0; position:absolute; display:none}
	.boat-and-inventory .tbl-boat-and-inventory td:hover .dropdown{display:block}
</style>

<div ng-init="loadBoatAndInventory();" class="boat-and-inventory">
	
	<div class="products">
		<div class="product text-center">
			<form ng-submit="DATA.running_boat = false;loadBoatAndInventory()">
				<button type="button" class="btn btn-info btn-sm pull-left" ng-click='DATA.running_boat = false;loadBoatAndInventoryPrevNext(-30)'><span class="fa fa-chevron-left"></span></button>
			
				Select Date : <input type="text" ng-model="filter_date_search.str" class="form-control input-sm text-center datepicker" placeholder="Select Date" style="width:120px; display:inline;" />

				&nbsp;&nbsp;
				
				Trip :
				<select ng-model="filter_search.departure_port_id" class="form-control input-sm" style="width:150px; display:inline;">
					<option value="">-- Departure Port --</option>
					<option ng-repeat="port in DATA.port_list.ports" value="{{port.id}}">{{port.port_code}} - {{port.name}}</option>
				</select>
				To : 
				<select ng-model="filter_search.arrival_port_id" class="form-control input-sm" style="width:150px; display:inline;">
					<option value="">-- Arrival Port --</option>
					<option ng-repeat="port in DATA.port_list.ports" value="{{port.id}}">{{port.port_code}} - {{port.name}}</option>
				</select>
				
				<button type="submit" class="btn btn-primary btn-sm">OK</button>
				
				<button type="button" class="btn btn-info btn-sm pull-right" ng-click='DATA.running_boat = false;loadBoatAndInventoryPrevNext(30)'><span class="fa fa-chevron-right"></span></button>
			</form>
		</div>
	</div>
	
	<div ng-show='!DATA.running_boat'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	
	<div ng-show='DATA.running_boat'>
		
		<?php $show_perline = 10; ?>
		
		<?php for ($i=0; $i<3; $i++){ ?>
		<table class="table table-bordered tbl-boat-and-inventory" ng-show="DATA.running_boat.running_boat.length > <?=($i*$show_perline)?>">
			<tr class="success">
				<td align="center" ng-repeat='running_boat in DATA.running_boat.running_boat' ng-show='$index >= <?=($i*$show_perline)?> && $index < <?=(($i+1)*$show_perline)?>' ng-class="{'danger':DATA.closed_date.closed_date[running_boat.date]}" title='{{DATA.closed_date.closed_date[running_boat.date].remarks}}'>
					
					<div class="dropdown pull-right">
						<button class="btn btn-xs btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
							<li ng-show="!DATA.closed_date.closed_date[running_boat.date]"><a href="" ng-click="setupOpenCloseCurrentDate(running_boat.date,0)"> <i class="fa fa-times fa-3x text-danger" aria-hidden="true"></i> Close Date</a></li>
							<li ng-show="DATA.closed_date.closed_date[running_boat.date]"><a href="" ng-click="setupOpenCloseCurrentDate(running_boat.date,1)"><i class="fa fa-circle-o fa-3x text-success" aria-hidden="true"></i> Open Date</a></li>
						</ul>
					</div>
					
					<strong>{{fn.formatDate(running_boat.date,"D")}} <br /> {{fn.formatDate(running_boat.date,"dd M")}}</strong>
					
				</td>
			</tr>
			<tr class="info">
				<td align="center" ng-repeat='running_boat in DATA.running_boat.running_boat' ng-show='$index >= <?=($i*$show_perline)?> && $index < <?=(($i+1)*$show_perline)?>' ng-class="{'danger':DATA.closed_date.closed_date[running_boat.date]}" title='{{running_boat.boat.name}}'>
					<div class="dropdown pull-right">
						<button class="btn btn-xs btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
							<li><a href="" ng-click="setupChangeBoat(running_boat.boat, running_boat.date)" data-toggle="modal" data-target="#popup-change-boat"> <i class="fa fa-ship fa-3x  text-info" aria-hidden="true"></i> Change Boat</a></li>
							<li role="separator" class="divider"></li>
							<li class="disabled"><a href="#">Change Boat To : </a></li>
							<li role="separator" class="divider"></li>
							<li ng-repeat='boat in DATA.boats.boats' ng-show='running_boat.boat.id != boat.id'><a href="" ng-click="changeCurrentBoat(boat, running_boat.date)"> {{boat.name}} ({{boat.capacity}} seats)</a></li>
						</ul>
					</div>
					
					<small>
						{{running_boat.boat.boat_code}}<br />
						{{running_boat.boat.name}}
					</small>
				</td>
			</tr>
			<tr>
				<td align="center" ng-repeat='running_boat in DATA.running_boat.running_boat' ng-show='$index >= <?=($i*$show_perline)?> && $index < <?=(($i+1)*$show_perline)?>' ng-class="{'danger':DATA.closed_date.closed_date[running_boat.date]}">
					<div ng-show='!DATA.calendar.availability_calendar[running_boat.date]'>
						{{running_boat.boat.capacity}}
					</div>
					<div ng-show='DATA.calendar.availability_calendar[running_boat.date]'>
						{{DATA.calendar.availability_calendar[running_boat.date].available_space}}
					</div>
				</td>
			</tr>
		</table>
		<br /><br />
		
		<?php } ?>
		
	</div>
	
	
	<div class="text-center">
		<button type="button" class="btn btn-md btn-success" data-toggle="modal" ng-click='setupChangeBoat()' data-target="#popup-change-boat"><i class="fa fa-ship" aria-hidden="true"></i> Setup Boat</button>
		<button type="button" class="btn btn-md btn-danger" data-toggle="modal" ng-click='setupOpenCloseDate()' data-target="#popup-open-close-date"><i class="fa fa-calendar" aria-hidden="true"></i> Open/Close Date</button>
	</div>

	<div class="modal fade" id="popup-change-boat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <form ng-submit='saveDataChangeBoat($event)'>
	  <div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel"> Setup Boat </h4>
		  </div>
		  <div class="modal-body">
		  	<div ng-show='DATA.editChangeBoat.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.editChangeBoat.error_msg'>{{err}}</li></ul></div>
			<table class="table table-borderless table-condensed">
				<tr>
					<td width="100">Start Date*</td>
					<td><input placeholder="Start Date" required="required" type="text" class="form-control input-md datepicker" ng-model='DATA.editChangeBoat.start_date' /></td>
				</tr>
				<tr>
					<td>End Date*</td>
					<td><input placeholder="End Date" required="required" type="text" class="form-control input-md datepicker" ng-model='DATA.editChangeBoat.end_date' /></td>
				</tr>
				<tr>
					<td>Boat*</td>
					<td>
						<select class="form-control" placeholder="Schedule Name" required="required" ng-model='DATA.editChangeBoat.boat_id'>
							<option value="">--Select Boat--</option>
							<option value="{{boat.id}}" ng-repeat='boat in DATA.boats.boats'>{{boat.name}} ({{boat.capacity}} seats)</option>
						</select>
					</td>
				</tr>
			</table>
		  </div>
		  <div class="modal-footer" style="text-align:center">
		  	<button type="submit" class="btn btn-primary">Save</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
	  </div>
	  </form>
	</div>
	
	<div class="modal fade" id="popup-open-close-date" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <form ng-submit='saveDataOpenCloseDate($event)'>
	  <div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel"> Open / Close Date </h4>
		  </div>
		  <div class="modal-body">
		  	<div ng-show='DATA.editOpenCloseDate.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.editOpenCloseDate.error_msg'>{{err}}</li></ul></div>
			<table class="table table-borderless table-condensed">
				<tr>
					<td width="100">Start Date*</td>
					<td><input placeholder="Start Date" required="required" type="text" class="form-control input-md datepicker" ng-model='DATA.editOpenCloseDate.start_date' /></td>
				</tr>
				<tr>
					<td>End Date*</td>
					<td><input placeholder="End Date" required="required" type="text" class="form-control input-md datepicker" ng-model='DATA.editOpenCloseDate.end_date' /></td>
				</tr>
				<tr>
					<td width="120">Status*</td>
					<td>
						<select required="required" class="form-control input-md" ng-model='DATA.editOpenCloseDate.status'>
							<option value="close">Close</option>
							<option value="open">Open</option>
						</select>
					</td>
				</tr>
				<tr ng-show="DATA.editOpenCloseDate.status=='close'">
					<td>Remarks</td>
					<td><input placeholder="Remarks" type="text" class="form-control input-md" ng-model='DATA.editOpenCloseDate.remarks' /></td>
				</tr>
			</table>
		  </div>
		  <div class="modal-footer" style="text-align:center">
		  	<button type="submit" class="btn btn-primary">Save</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
	  </div>
	  </form>
	</div>
	
	<script>
		activate_sub_menu_schedule_detail("boat_and_inventory");
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
	</script>
</div>
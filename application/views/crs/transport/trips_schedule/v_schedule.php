<div class="sub-title">Schedules</div>
<br />
<div ng-init="loadDataSchedule();">
	<div class="products">
		<div class="product ">
			Filter : <input type="text" ng-model="filter_schedules" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
		</div>
			
		<div ng-show='DATA.schedules && !DATA.schedules.schedules'>
            <div class="alert alert-warning">
                <strong>Sorry,</strong> data not found...
            </div>
        </div>
		
		<div class="container lesspadding">
			<div ng-show='!DATA.schedules'>
				<img src="<?=base_url("public/images/loading_bar.gif")?>" />
			</div>
			<div class="reorder product row product-trip" ng-class="{'danger':schedule.publish_status!='1'}" ng-repeat="schedule in DATA.schedules.schedules | orderBy : '-publish_status' | filter : filter_schedules">
				<div class="col-xs-7 col" style="padding-right:20px !important;border-right:#ccc 1px solid">
					<div class="desc">
						<div class="title"> 
							<a ui-sref="transport.trips_schedule_detail({'schedule_code':schedule.schedule_code})" title="View Detail"> 
								Code : {{schedule.schedule_code}}<?php /*?>{{schedule.name}}<?php */?>
							</a> 
						</div>
						<?php /*?><div class="code"> Code : {{schedule.schedule_code}}</div><?php */?>
						<div class="code"> <i class="fa fa-ship" aria-hidden="true"></i> Boat : {{schedule.boat.name}}</div>
						<div ng-show='schedule.description'>{{schedule.description}}</div>
						<?php /*?><div class="code"> Trip: Lembongan <span class="fa fa-chevron-right"></span> Lombok </div>
						<div class="stats">Last update: 25 Apr 2017</div><?php */?>
						<br />
						<div class="button-edit">
							<div class="pull-right" style="padding-top:8px"> 
								<a href="" ng-click="publishUnpublishSchedule(schedule, 0)" ng-show="schedule.publish_status == '1'">
									<div style="color:#090"><img src="<?=base_url("public/images/on-button.png")?>"></span> </div>
								</a> 
								<a href="" ng-click="publishUnpublishSchedule(schedule, 1)" ng-show="schedule.publish_status != '1'">
									<div style="color:#F30"><img src="<?=base_url("public/images/off-button.png")?>"></span></div>
								</a> 
							</div>
							<a ui-sref="transport.trips_schedule_detail({'schedule_code':schedule.schedule_code})<?php /*?>transport.trips_schedule_edit({'schedule_code_edit':schedule.schedule_code})<?php */?>" class="btn btn-info btn-sm" title="Edit Schedule"> <span class="glyphicon glyphicon-pencil"></span> Edit Schedule & Rates</a> </div>
					</div>
				</div>
				<div class="col-xs-5 col">
					<div ng-show='!schedule.schedule_detail'>
						<img src="<?=base_url("public/images/loading_bar.gif")?>" width="100px" />
					</div>
					<div style="padding:5px" ng-show='schedule.schedule_detail.status=="SUCCESS"'>
						<table class="table table-condensed table-bordered">
							<tr class="info">
								<th colspan="2" style="text-align:center">Departure</th>
								<th colspan="2" style="text-align:center">Arrival</th>
							</tr>
							<tr ng-repeat='schedule_detail in schedule.schedule_detail.schedule_detail' ng-class="{'hide':($index>=3)}">
								<td width="25%" align="center">{{schedule_detail.departure_port.port_code}}</td>
								<td width="25%" align="center">{{schedule_detail.departure_time}}</td>
								<td width="25%" align="center">{{schedule_detail.arrival_port.port_code}}</td>
								<td width="25%" align="center">{{schedule_detail.arrival_time}}</td>
							</tr>
						</table>
						<a href="" ng-show='schedule.schedule_detail.schedule_detail.length>3' onclick="$(this).closest('div').find('tr').removeClass('hide'); $(this).hide(); return false; "> 
							<em>{{schedule.schedule_detail.schedule_detail.length-3}} more...</em>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<hr>
	<div class="add-product-button"> <a ui-sref="transport.trips_schedule_add" class="btn btn-success btn-lg btn-block" > <span class="glyphicon glyphicon-plus"></span> New Schedule </a> </div>
	<br>
	<br>


	<?php 
	//pre($_GET);
	//pre($_SESSION);
	?>
</div>

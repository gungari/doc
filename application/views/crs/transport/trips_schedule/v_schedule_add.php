<div class="sub-title">
	<span ng-show='!DATA.current_schedule.schedule_code'><i class="fa fa-plus-square" aria-hidden="true"></i> Add New</span> 
	<span ng-show='DATA.current_schedule.schedule_code'><i class="fa fa-pencil" aria-hidden="true"></i> Edit</span>
	Schedule
</div>

<br />
<div ng-init="scheduleAddEdit();">
	<div class="products">
		<div ng-show='!(DATA.boats && DATA.port_list)'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		</div>
		
		<div ng-show='(DATA.boats && DATA.port_list)'>
			<form ng-submit="saveDataSchedule($event)">
				<div ng-show='DATA.current_schedule.error_desc.length>0' class="alert alert-danger show_error"><ul><li ng-repeat='err in DATA.current_schedule.error_desc'>{{err}}</li></ul></div>
				<table class="table table-borderlesss">
					<?php /*?><tr>
						<td width="150">Name</td>
						<td>
							<input type="text" class="form-control" placeholder="Schedule Name" required="required" ng-model='DATA.current_schedule.name' />
						</td>
					</tr><?php */?>
					<tr>
						<td width="150">Boat</td>
						<td>
							<select class="form-control" placeholder="Schedule Name" required="required" ng-model='DATA.current_schedule.boat_id'>
								<option value="">--Select Boat--</option>
								<option value="{{boat.id}}" ng-repeat='boat in DATA.boats.boats'>{{boat.name}}</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Description</td>
						<td>
							<textarea class="form-control autoheight" placeholder="Description" ng-model='DATA.current_schedule.description' rows="3"></textarea>
						</td>
					</tr>
					<tr>
						<td>Schedule</td>
						<td>
							<table class="table table-bordered">
								<tr class="info">
									<th width="50%" colspan="2" class="text-center">Departure</th>
									<th width="50%" colspan="2" class="text-center">Arrival</th>
									<th width="50"></th>
								</tr>
								<tr class="info">
									<th class="text-center">Port</th>
									<th class="text-center">Time</th>
									<th class="text-center">Port</th>
									<th class="text-center">Time</th>
									<th></th>
								</tr>
								<tr ng-repeat="schedule_detail in DATA.current_schedule.schedule_detail">
									<td width="30%">
										<select class="form-control input-sm" ng-disabled='$index>0' placeholder="Schedule Name" required="required" ng-model='schedule_detail.departure_port_id'>
											<option value="">--Select Port--</option>
											<option value="{{port.id}}" ng-repeat='port in DATA.port_list.ports'>{{port.port_code}} - {{port.name}}</option>
										</select>
									</td>
									<td width="20%">
										<select class="form-control input-sm" style="width:45%; display:inline; padding:5px 3px;" required="required" ng-model='schedule_detail.departure_time_hh'>
											<option value="" disabled="disabled">hh</option>
											<option value="" disabled="disabled">--</option>
											<?php for ($i=0; $i<24; $i++){ $str_i = str_repeat("0", (2-strlen($i))).$i; ?>
											<option value="<?=$str_i?>"><?=$str_i?></option>
											<?php } ?>
										</select>
										:
										<select class="form-control input-sm" style="width:45%; display:inline; padding:5px 3px;" required="required" ng-model='schedule_detail.departure_time_mm'>
											<option value="" disabled="disabled">mm</option>
											<option value="" disabled="disabled">--</option>
											<?php for ($i=0; $i<60; $i++){ $str_i = str_repeat("0", (2-strlen($i))).$i; ?>
											<option value="<?=$str_i?>"><?=$str_i?></option>
											<?php } ?>
										</select>
									</td>
									<td width="30%">
										<select class="form-control input-sm" placeholder="Schedule Name" required="required" ng-model='schedule_detail.arrival_port_id' ng-change="changeArrivalPort($index)">
											<option value="">--Select Port--</option>
											<option value="{{port.id}}" ng-repeat='port in DATA.port_list.ports'>{{port.port_code}} - {{port.name}}</option>
										</select>
									</td>
									<td width="20%">
										<select class="form-control input-sm" style="width:45%; display:inline; padding:5px 3px;" required="required" ng-model='schedule_detail.arrival_time_hh'>
											<option value="" disabled="disabled">hh</option>
											<option value="" disabled="disabled">--</option>
											<?php for ($i=0; $i<24; $i++){ $str_i = str_repeat("0", (2-strlen($i))).$i; ?>
											<option value="<?=$str_i?>"><?=$str_i?></option>
											<?php } ?>
										</select>
										:
										<select class="form-control input-sm" style="width:45%; display:inline; padding:5px 3px;" required="required" ng-model='schedule_detail.arrival_time_mm'>
											<option value="" disabled="disabled">mm</option>
											<option value="" disabled="disabled">--</option>
											<?php for ($i=0; $i<60; $i++){ $str_i = str_repeat("0", (2-strlen($i))).$i; ?>
											<option value="<?=$str_i?>"><?=$str_i?></option>
											<?php } ?>
										</select>
									</td>
									<td>
										<a href="" style="color:red" ng-click="scheduleDetailRemoveLastItem()" ng-show='($index+1)==DATA.current_schedule.schedule_detail.length'><i class="fa fa-remove" aria-hidden="true"></i></a>
									</td>
								</tr>
							</table>
							<button type="button" class="btn btn-info btn-xs" ng-click='scheduleDetailAddItem()'>Add item</button>
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<button type="submit" class="btn btn-primary">Save Schedule</button>
							&nbsp;&nbsp;&nbsp;
							<a ng-show='!DATA.current_schedule.schedule_code' ui-sref="transport.trips_schedule"><strong>Cancel</strong></a>
							<a ng-show='DATA.current_schedule.schedule_code' ui-sref="transport.trips_schedule_detail({'schedule_code':DATA.current_schedule.schedule_code})"><strong>Cancel</strong></a>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>

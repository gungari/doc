<div class="sub-title">Packages</div>
<br />
<div ng-init="loadDataPackages();">

	<div ng-show='!DATA.packages'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	
	<div ng-show='DATA.packages'>
		<table class="table table-bordered table-condensedx product-rates-list">
            <tr style="background:#F5F5F5">
                <td><strong>Packages Name</strong></td>
                <td width="40%"><strong>Rates</strong></td>
            </tr>
            <tbody ng-repeat="rates in DATA.packages.packages | orderBy : '-publish_status'">
                <tr ng-class="{'danger':rates.publish_status!='1'}">
                    <td>
                        <a href="" onclick="$(this).closest('tr').next().toggle();" ng-click='loadPackagesDetailForRatesList(rates);'><strong>{{rates.package_code}} - {{rates.name}}</strong></a><br />
                        
                        <div ng-show="rates.rates_for.walkin || rates.rates_for.offline">
                            <small>From {{fn.formatDate(rates.start_date, "dd M yy")}} to {{fn.formatDate(rates.end_date, "dd M yy")}}</small>
                        </div>
                        
                        <div>
                            <span class="label label-danger" ng-show='rates.rates_for.offline'>Offline Booking</span>
                            <span class="label label-warning" ng-show='rates.rates_for.walkin'>Walk In Booking</span>
                            <span class="label label-info" ng-show='rates.rates_for.agent'>Agencies and Colleague</span>
                        </div>
                        
                        <div class="pull-right" ng-show="rates.use_smart_pricing == 'YES'">
                            <small title="Smart Pricing">
                                <span class="label label-success">
                                    <span class="glyphicon glyphicon-signal"></span> Smart Pricing
                                </span>
                            </small>
                        </div>
                        <?php /*?>
                        <div>
                            <small>Aplied for  <strong>{{rates.rates_applied_for}} <span ng-show="rates.rates_applied_for!='ALL'">only</span></strong></small>
                        </div>
                        <div style="margin-top:10px;">
                            <a href=""><i class="fa fa-calendar" aria-hidden="true"></i> View Calendar</a>
                        </div><?php */?>
                    </td>
                    <td>
                        <div>
                            Adult : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.rates_1, rates.currency)}}</strong>
                        </div>
                        <div ng-show='rates.rates.rates_2 > 0'>
                            Child : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.rates_2, rates.currency)}}</strong>
                        </div>
                        <div ng-show='rates.rates.rates_3 > 0'>
                            Infant : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.rates_3, rates.currency)}}</strong>
                        </div>
                    </td>
                </tr>
                <tr ng-class="{'danger':rates.publish_status!='1'}" class="hidden-field">
                    <td colspan="3">
                        <table class="table table-borderless table-condensed" style="background:none">
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td>
                                        <div class="pull-right"> 
                                            <a href="" ng-click="publishUnpublishPackages(rates, 0)" ng-show="rates.publish_status == '1'">
                                                <div style="color:#090"><img src="<?=base_url("public/images/on-button.png")?>"></span> </div>
                                            </a> 
                                            <a href="" ng-click="publishUnpublishPackages(rates, 1)" ng-show="rates.publish_status != '1'">
                                                <div style="color:#F30"><img src="<?=base_url("public/images/off-button.png")?>"></span></div>
                                            </a> 
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100">Code</td>
                                    <td><strong>{{rates.package_code}}</strong></td>
                                </tr>
                                <tr>
                                    <td>Booking Handling</td>
                                    <td>{{rates.booking_handling}}</td>
                                </tr>
                                <tr>
                                    <td width="140">Available On</td>
                                    <td>
                                        <div class="btn-group btn-days">
                                            <button type="button" class="btn btn-xs" ng-class="{'btn-info':rates.available_on == 'all_day' || rates.available_on.indexOf('mon') >= 0}">Mon</button>
                                            <button type="button" class="btn btn-xs" ng-class="{'btn-info':rates.available_on == 'all_day' || rates.available_on.indexOf('tue') >= 0}">Tue</button>
                                            <button type="button" class="btn btn-xs" ng-class="{'btn-info':rates.available_on == 'all_day' || rates.available_on.indexOf('wed') >= 0}">Wed</button>
                                            <button type="button" class="btn btn-xs" ng-class="{'btn-info':rates.available_on == 'all_day' || rates.available_on.indexOf('thu') >= 0}">Thu</button>
                                            <button type="button" class="btn btn-xs" ng-class="{'btn-info':rates.available_on == 'all_day' || rates.available_on.indexOf('fri') >= 0}">Fri</button>
                                            <button type="button" class="btn btn-xs" ng-class="{'btn-info':rates.available_on == 'all_day' || rates.available_on.indexOf('sat') >= 0}">Sat</button>
                                            <button type="button" class="btn btn-xs" ng-class="{'btn-info':rates.available_on == 'all_day' || rates.available_on.indexOf('sun') >= 0}">Sun</button>
                                        </div>
                                    </td>
                                </tr>
                                <tr ng-show="rates.rates_for.walkin || rates.rates_for.offline">
                                    <td>Range of dates</td>
                                    <td><strong>{{fn.formatDate(rates.start_date, "d MM yy")}}</strong> to <strong>{{fn.formatDate(rates.end_date, "d MM yy")}}</strong></td>
                                </tr>
                                <?php /*?><tr>
                                    <td>Auto Discount</td>
                                    <td> IDR 100.000 </td>
                                </tr><?php */?>
                                <tr>
                                    <td>Cut Off Booking</td>
                                    <td>{{rates.cut_of_booking}} days in advance</td>
                                </tr>
                                <tr>
                                    <td>Minimum Order</td>
                                    <td>{{rates.min_order}} Pax</td>
                                </tr>
                                
                                <tr>
                                    <td>Routes Option</td>
                                    <td>
                                    	<ol style="margin:0; padding-left:15px">
                                        	<li ng-repeat="routes in rates.routes_option">{{routes.str_route}}</li>
                                        </ol>
                                    </td>
                                </tr>
                                
                                <?php /*?><tr>
                                    <td> Pickup Service </td>
                                    <td> <span class="text-uppercase">{{rates.pickup_service}}</span> </td>
                                </tr>
                                <tr ng-show="rates.pickup_service=='yes' && rates.pickup_area">
                                    <td> Pick Up Area </td>
                                    <td><ul style="margin:0; padding-left:15px">
                                            <li ng-repeat='area in rates.pickup_area'> 
                                                {{area.area}} - {{area.time}} <strong ng-show='area.price > 0'> - {{area.currency}} {{fn.formatNumber(area.price, area.currency)}} / {{area.type}}</strong> 
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td> Drop Off Service </td>
                                    <td> <span class="text-uppercase">{{rates.dropoff_service}}</span> </td>
                                </tr>
                                <tr ng-show="rates.dropoff_service=='yes' && rates.dropoff_area">
                                    <td> Dropoff Area </td>
                                    <td>
                                        <ul style="margin:0; padding-left:15px">
                                            <li ng-repeat='area in rates.dropoff_area'> 
                                                {{area.area}} - {{area.time}} <strong ng-show='area.price > 0'> - {{area.currency}} {{fn.formatNumber(area.price, area.currency)}} / {{area.type}}</strong> 
                                            </li>
                                        </ul>
                                    </td>
                                </tr><?php */?>
                                
                                <tr>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td>Additional Item</td>
                                    <td>
                                        <ul style="margin:0; padding-left:15px" ng-show='rates.inclusion'>
                                            <li ng-repeat='str in rates.additional_item'>{{str}}</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Inclusions</td>
                                    <td>
                                        <ul style="margin:0; padding-left:15px" ng-show='rates.inclusion'>
                                            <li ng-repeat='str in rates.inclusion'>{{str}}</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Exclusions</td>
                                    <td>
                                        <ul style="margin:0; padding-left:15px" ng-show='rates.exclusion'>
                                            <li ng-repeat='str in rates.exclusion'>{{str}}</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Additional info</td>
                                    <td>
                                        <ul style="margin:0; padding-left:15px" ng-show='rates.additional_info'>
                                            <li ng-repeat='str in rates.additional_info'>{{str}}</li>
                                        </ul>
                                    </td>
                                </tr>
                                <?php /*?><tr>
                                    <td>Exception Date</td>
                                    <td>
                                        <ul style="margin:0; padding-left:15px" ng-show='rates.close_date'>
                                            <li ng-repeat='str in rates.close_date'>{{fn.formatDate(str.date, "dd M yy")}}</li>
                                        </ul>
                                    </td>
                                </tr><?php */?>
                            </tbody>
                        </table>
                        <hr style="margin:10px 0">
                        <div align="right">
                            <a ui-sref="transport.packages_edit({'package_code':rates.package_code})"><span class="glyphicon glyphicon-pencil"></span> Edit Packages</a>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <?php /*?><a ui-sref="transport.trips_schedule_detail.smart_pricing({'rates_code':rates.rates_code})"><span class="glyphicon glyphicon-signal"></span> Smart Pricing</a>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="" ng-click="duplicateScheduleRates(rates)"><span class="glyphicon glyphicon-duplicate"></span>  Duplicate Rates</a>
                            &nbsp;&nbsp;&nbsp;&nbsp;<?php */?>
                            <a href="" style="color:red" ng-click="deletePackages(rates)"><span class="glyphicon glyphicon-trash"></span> </a>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
	</div>
	
	<hr>
	<div class="add-product-button"> <a ui-sref="transport.packages_add" class="btn btn-success btn-lg btn-block" > <span class="glyphicon glyphicon-plus"></span> New Packages</a> </div>
	<br>
	<br>
</div>

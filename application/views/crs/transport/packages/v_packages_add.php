<div class="sub-title">
    <span ng-show='!DATA.current_package.package_code'><i class="fa fa-plus-square" aria-hidden="true"></i> Add New</span> 
	<span ng-show='DATA.current_package.package_code'><i class="fa fa-pencil" aria-hidden="true"></i> Edit</span>
	Packages
</div>

<br />
<div ng-init="packageAddEdit();">
	<div class="products">
		<div ng-show='!DATA.current_package'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		</div>
		
		<div ng-show='DATA.current_package'>
			<form ng-submit="saveDataPackages($event)">
				<div ng-show='DATA.current_package.error_desc.length>0' class="alert alert-danger show_error"><ul><li ng-repeat='err in DATA.current_package.error_desc'>{{err}}</li></ul></div>


				<table width="100%" class="table td_center_align form-inline">
                	<tr>
                        <td>Rates For*</td>
                        <td>
                            <label><input type="checkbox" ng-model='DATA.current_package.rates_for.offline' ng-true-value="1" /> Offline Booking </label>
                            &nbsp;&nbsp;&nbsp;
                            <label><input type="checkbox" ng-model='DATA.current_package.rates_for.walkin' ng-true-value="1" /> Walk In Booking </label>
                            &nbsp;&nbsp;&nbsp;
                            <label><input type="checkbox" ng-model='DATA.current_package.rates_for.agent' ng-true-value="1" /> Agencies and Colleague </label>
                        </td>
                    </tr>
					<tr>
						<td width="150">Name*</td>
						<td>
							<input type="text" class="form-control" placeholder="Package Name" xrequired="required" ng-model='DATA.current_package.name' style="width:100%" />
						</td>
					</tr>
					<tr>
						<td style="vertical-align:top !important">Description</td>
						<td>
							<textarea class="form-control autoheight" placeholder="Description" ng-model='DATA.current_package.description' rows="3" style="width:100%"></textarea>
						</td>
					</tr>
                    <tr id="tr_rod" ng-hide="!(DATA.current_package.rates_for.offline || DATA.current_package.rates_for.walkin)">
                        <td>Range of dates*</td>
                        <td>
                            <input required="required" type="text" class="form-control input-sm datepicker start_date datepicker" placeholder="Start Date" style="width:150px" ng-model='DATA.current_package.start_date'	ng-disabled="!(DATA.current_package.rates_for.offline || DATA.current_package.rates_for.walkin)" />
                            -
                            <input required="required" type="text" class="form-control input-sm datepicker end_date datepicker" placeholder="End Date" style="width:150px" ng-model='DATA.current_package.end_date'		ng-disabled="!(DATA.current_package.rates_for.offline || DATA.current_package.rates_for.walkin)" />
                        </td>
                    </tr>
                    
                    
                    <tr>
                        <td>Minimum Order*</td>
                        <td>
                            <div class="input-group">
                                <input required="required" type="number" min="1" class="form-control input-sm" style="width:100px" ng-model='DATA.current_package.min_order' />
                                <select required="required" name="pax_or_paket" id="pax_or_paket" class="form-control input-sm" style="width:100px">
                                    <option value="pax" selected="selected">Person</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top !important">Sell Rate</td>
                        <td>
                        	<table width="100%" class="table-borderless table-condensed">
                                <tr>
                                    <td>Currency</td>
                                    <td>Adult*</td>
                                    <td>Child*</td>
                                    <td>Infant*</td>
                                </tr>
                                <tr>
                                    <td width="19%">
                                        <input type="text" disabled="disabled" class="form-control input-sm" style="width:100%; text-align:center" value="<?=@$vendor['default_currency']?>" />
                                    </td>
                                    <td width="27%">
                                        <input type="number" required="required" min="0" step="any" class="form-control input-sm" placeholder="Sell Rate Adult " ng-model='DATA.current_package.rates.rates_1' style="width:100%" />
                                    </td>
                                    <td width="27%">
                                        <input type="number" required="required" min="0" step="any" class="form-control input-sm" placeholder="Sell Rate Child " ng-model='DATA.current_package.rates.rates_2' style="width:100%" />
                                    </td>
                                    <td width="27%">
                                        <input type="number" required="required" min="0" step="any" class="form-control input-sm" placeholder="Sell Rate Infant" ng-model='DATA.current_package.rates.rates_3' style="width:100%" />
                                    </td>
                                </tr>
                            </table> 
                        </td>
                    </tr>
                    
                    
                    <tr>
                    	<td style="vertical-align:top !important">Routes Option</td>
                        <td>
                        	<?php //for ($jx=1;$jx<=2;$jx++){ ?>
                        	<div ng-repeat="routes_option in DATA.current_package.routes_option">
                            	<table class="table">
                                	<tr class="active">
                                    	<td colspan="5"><strong>Route {{($index+1)}}</strong></td>
                                    </tr>
                                    <?php //for ($j=1;$j<=3;$j++){ ?>
                                    <tr ng-repeat="trip in routes_option.trip">
                                    	<td width="200">
                                        	<select class="form-control input-sm" style="width:100%" ng-model='trip.departure_port_id' ng-change='packageTripOptionSelectDeparturePort(trip)'>
                                            	<option value="" disabled="disabled">-- Departure --</option>
                                            	<option value="---" disabled="disabled">---------------</option>
                                                <option ng-repeat="port in trip.departure_port_list" value="{{port.id}}">{{port.name}}</option>
                                            </select>
                                        </td>
                                        <td width="40" align="center"><span class="fa fa-chevron-right"></span></td>
                                        <td width="200">
                                        	<select class="form-control input-sm" style="width:100%" ng-model='trip.arrival_port_id' ng-change='packageTripOptionSelectArrivalPort(trip)'>
                                            	<option value="" disabled="disabled">-- Arrival --</option>
                                                <option value="---" disabled="disabled">---------------</option>
                                            	<option ng-repeat="port in trip.arrival_port_list" value="{{port.id}}">{{port.name}}</option>
                                            </select>
                                        </td>
                                        <td width="150">
                                        	<select class="form-control input-sm" style="width:100%" ng-model='trip.time'>
                                                <option value="">Anytime</option>
                                                <option value="---" disabled="disabled">---------------</option>
                                                <option ng-repeat="time in trip.available_time_list" value="{{time.value}}">{{time.text}}</option>
                                            </select>
                                        </td>
                                        <td width="40" align="right">
                                        	<a href="" style="color:red" ng-hide="routes_option.trip.length==2" ng-click="routes_option.trip.splice($index,1)"><span class="glyphicon glyphicon-trash"></span></a>
                                        </td>
                                    </tr>
                                    <?php //} ?>
                                    <tr>
										<td colspan="5">
											<button type="button" class="btn btn-xs btn-info" ng-click="packagesRoutesOptionAddTrip(routes_option)">
												<span class="glyphicon glyphicon-plus"></span> Add Trip
											</button>
											&nbsp;
											<button type="button" class="btn btn-xs btn-danger" ng-hide="DATA.current_package.routes_option.length==1" title="Delete Route" ng-click="packagesRemoveRoutesOption($index)">
												<span class="glyphicon glyphicon-trash"></span> Delete Route
											</button>
										</td>
									</tr>
                                </table>
                            </div>
                            <?php //} ?>
                            
                            <hr ng-show='DATA.current_package.routes_option.length > 0' />
                            <button type="button" class="btn btn-xs btn-info" ng-click="packagesAddRoutesOption()">
                                <span class="glyphicon glyphicon-plus"></span> Add New Route
                            </button>
                        </td>
                    </tr>
                    
                    
                    
                    <tr>
                        <td>Cut Off Booking*</td>
                        <td>
                            <input type="number" min="0" required="required" class="form-control input-sm" style="width:100px" maxlength="2" ng-model='DATA.current_package.cut_of_booking' /> 
                            <span>days in advance</span>
                        </td>
                    </tr>
                    <tr>
                        <td>Available On</td>
                        <td class="form-inline">
                            <div class="btn-group btn-days">
                                <button type="button" class="btn btn-sm" ng-click="packageCheckAvailableDayInWeek('mon')" ng-class="{'btn-info':DATA.current_package.available_on.indexOf('mon') >= 0}">Mon</button>
                                <button type="button" class="btn btn-sm" ng-click="packageCheckAvailableDayInWeek('tue')" ng-class="{'btn-info':DATA.current_package.available_on.indexOf('tue') >= 0}">Tue</button>
                                <button type="button" class="btn btn-sm" ng-click="packageCheckAvailableDayInWeek('wed')" ng-class="{'btn-info':DATA.current_package.available_on.indexOf('wed') >= 0}">Wed</button>
                                <button type="button" class="btn btn-sm" ng-click="packageCheckAvailableDayInWeek('thu')" ng-class="{'btn-info':DATA.current_package.available_on.indexOf('thu') >= 0}">Thu</button>
                                <button type="button" class="btn btn-sm" ng-click="packageCheckAvailableDayInWeek('fri')" ng-class="{'btn-info':DATA.current_package.available_on.indexOf('fri') >= 0}">Fri</button>
                                <button type="button" class="btn btn-sm" ng-click="packageCheckAvailableDayInWeek('sat')" ng-class="{'btn-info':DATA.current_package.available_on.indexOf('sat') >= 0}">Sat</button>
                                <button type="button" class="btn btn-sm" ng-click="packageCheckAvailableDayInWeek('sun')" ng-class="{'btn-info':DATA.current_package.available_on.indexOf('sun') >= 0}">Sun</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top !important">Additional Item</td>
                        <td>
                            <div class="form-inline important-infoEN" style="background:#FAFAFA; border:solid 1px #EEE; padding:10px">
                                <div class="important-info-list" data-id="EN-1">
                                    <ul style="margin:5px 0 10px 15px; padding:0" class="sortable ui-sortable">
                                        <li style="margin-bottom: 5px;" ng-repeat='str in DATA.current_package.additional_item'>
                                            <input type="text" required="required" class="form-control input-sm input-sm" ng-model='str.str' style="width:90%" maxlength="255"> 
                                            <a href="" ng-click="DATA.current_package.additional_item.splice($index,1)" title="Remove"><span class="glyphicon glyphicon-trash"></span></a>
                                        </li>
                                    </ul>
                                    <button type="button" class="btn btn-info btn-xs" ng-click="ratesAddImportantInfo(DATA.current_package.additional_item)"><span class="glyphicon glyphicon-plus"></span> Add Item </button>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top !important">Inclusion</td>
                        <td>
                            <div class="form-inline important-infoEN" style="background:#FAFAFA; border:solid 1px #EEE; padding:10px">
                                <div class="important-info-list" data-id="EN-1">
                                    <ul style="margin:5px 0 10px 15px; padding:0" class="sortable ui-sortable">
                                        <li style="margin-bottom: 5px;" ng-repeat='str in DATA.current_package.inclusion'>
                                            <input type="text" required="required" class="form-control input-sm input-sm" ng-model='str.str' style="width:90%" maxlength="255"> 
                                            <a href="" ng-click="DATA.current_package.inclusion.splice($index,1)" title="Remove"><span class="glyphicon glyphicon-trash"></span></a>
                                        </li>
                                    </ul>
                                    <button type="button" class="btn btn-info btn-xs" ng-click="ratesAddImportantInfo(DATA.current_package.inclusion)"><span class="glyphicon glyphicon-plus"></span> Add Item </button>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top !important">Exclusion</td>
                        <td>
                            <div class="form-inline important-infoEN" style="background:#FAFAFA; border:solid 1px #EEE; padding:10px">
                                <div class="important-info-list" data-id="EN-1">
                                    <ul style="margin:5px 0 10px 15px; padding:0" class="sortable ui-sortable">
                                        <li style="margin-bottom: 5px;" ng-repeat='str in DATA.current_package.exclusion'>
                                            <input type="text" required="required" class="form-control input-sm input-sm" ng-model='str.str' style="width:90%" maxlength="255"> 
                                            <a href="" ng-click="DATA.current_package.exclusion.splice($index,1)" title="Remove"><span class="glyphicon glyphicon-trash"></span></a>
                                        </li>
                                    </ul>
                                    <button type="button" class="btn btn-info btn-xs" ng-click="ratesAddImportantInfo(DATA.current_package.exclusion)"><span class="glyphicon glyphicon-plus"></span> Add Item </button>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top !important">Additional Info</td>
                        <td>
                            <div class="form-inline important-infoEN" style="background:#FAFAFA; border:solid 1px #EEE; padding:10px">
                                <div class="important-info-list" data-id="EN-1">
                                    <ul style="margin:5px 0 10px 15px; padding:0" class="sortable ui-sortable">
                                        <li style="margin-bottom: 5px;" ng-repeat='str in DATA.current_package.additional_info'>
                                            <input type="text" required="required" class="form-control input-sm input-sm" ng-model='str.str' style="width:90%" maxlength="255"> 
                                            <a href="" ng-click="DATA.current_package.additional_info.splice($index,1)" title="Remove"><span class="glyphicon glyphicon-trash"></span></a>
                                        </li>
                                    </ul>
                                    <button type="button" class="btn btn-info btn-xs" ng-click="ratesAddImportantInfo(DATA.current_package.additional_info)"><span class="glyphicon glyphicon-plus"></span> Add Item </button>
                                </div>
                            </div>
                        </td>
                    </tr>
                    
                    
					<tr>
						<td></td>
						<td>
							<button type="submit" class="btn btn-primary">Save Package</button>
							&nbsp;&nbsp;&nbsp;
							<a ui-sref="transport.packages"><strong>Cancel</strong></a>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>
<div class="sub-title">Boats</div>
<br />
<div ng-init="loadDataBoat()">
	<div xng-show="DATA.boats.status=='SUCCESS'">
		<div class="products">
			<div class="product ">
				Filter : <input type="text" ng-model="filter_boats" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
			</div>
			<div class="container lesspadding">
				<div ng-show='!DATA.boats.boats'>
					<img src="<?=base_url("public/images/loading_bar.gif")?>" />
				</div>
				<div class="reorder product row" ng-class="{'danger':boats.publish_status!='1'}" ng-repeat="boats in DATA.boats.boats | orderBy:['-publish_status','name'] | filter : filter_boats ">
					<div class="col-xs-3 col">
						<div class="image"> <img ng-src="{{boats.main_image}}" width="100%"> </div>
						<?php /*?><div style="float:left;margin-top:10px;color:#aaa;">Last update 17 Oct 2016</div><?php */?>
					</div>
					<div class="col-xs-9 col">
						<div class="desc">
							<?php /*?><div class="handle pull-right"><span class="glyphicon glyphicon-sort"></span></div><?php */?>
							<div class="title"> <a href="" title="Edit Boat" data-toggle="modal" ng-click='addEditBoat(boats)' data-target="#add-edit-boat"> {{boats.name}} </a> </div>
							<div class="code">Code : {{boats.boat_code}}</div>
							<div class="code">Capacity : {{boats.capacity}}</div>
							<div class="button-edit">
								<div class="pull-right" style="padding-top:10px"> 
									<a href="" ng-click="publishUnpublishBoat(boats, 0)" ng-show="boats.publish_status == '1'">
										<div style="color:#090"><img src="<?=base_url("public/images/on-button.png")?>"></span> </div>
									</a> 
									<a href="" ng-click="publishUnpublishBoat(boats, 1)" ng-show="boats.publish_status != '1'">
										<div style="color:#F30"><img src="<?=base_url("public/images/off-button.png")?>"></span></div>
									</a> 
								</div>
								<a href="" class="btn btn-info btn-sm" title="Edit Boat" data-toggle="modal" ng-click='addEditBoat(boats)' data-target="#add-edit-boat"> <span class="glyphicon glyphicon-pencil"></span> Edit </a> </div>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="add-product-button"> <a href="" class="btn btn-success btn-lg btn-block" data-toggle="modal" ng-click='addEditBoat()' data-target="#add-edit-boat"> <span class="glyphicon glyphicon-plus"></span> New Boat </a> </div>
			<br>
			<br>
		</div>
	</div>
	
	<div class="modal fade" id="add-edit-boat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <form ng-submit='saveDataBoat($event)'>
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">
				<span ng-show='!DATA.myBoat.id'>Add</span><span ng-show='DATA.myBoat.id'>Edit</span> Boat
			</h4>
		  </div>
		  <div class="modal-body">
		  	<div ng-show='DATA.myBoat.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myBoat.error_msg'>{{err}}</li></ul></div>
			<table class="table table-borderless table-condensed">
				<tr>
					<td width="120">Name*</td>
					<td><input placeholder="Name" required="required" type="text" class="form-control input-md" ng-model='DATA.myBoat.name' /></td>
				</tr>
				<tr>
					<td>Capacity*</td>
					<td><input placeholder="Capacity" required="required" type="number" min="0" step="1" class="form-control input-md" ng-model='DATA.myBoat.capacity' style="width:100px" /></td>
				</tr>
				<tr>
					<td>Description</td>
					<td>
						<textarea placeholder="Description" class="form-control input-md" ng-model='DATA.myBoat.description' rows="5"></textarea>
					</td>
				</tr>
			</table>
		  </div>
		  <div class="modal-footer" style="text-align:center">
		  	<button type="submit" class="btn btn-primary">Save</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
	  </div>
	  </form>
	</div>
	
</div>

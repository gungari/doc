<div>
	<h1>Boats Add</h1>
	<div ng-show="DATA.boats.status=='SUCCESS'">
		<div class="products">
			<div class="container lesspadding">
				<div class="reorder product row" ng-repeat="boats in DATA.boats.boats">
					<div class="col-xs-4 col">
						<div class="image"> <img ng-src="{{boats.main_image}}" width="100%"> </div>
						<?php /*?><div style="float:left;margin-top:10px;color:#aaa;">Last update 17 Oct 2016</div><?php */?>
					</div>
					<div class="col-xs-8 col">
						<div class="desc">
							<?php /*?><div class="handle pull-right"><span class="glyphicon glyphicon-sort"></span></div><?php */?>
							<div class="title"> <a href="https://bes.hybridbooking.com/extranet/transport/edit_detail_boat/43" title="View Detail"> {{boats.name}} </a> </div>
							<div class="code">Code : {{boats.boat_code}}</div>
							<div class="code">Language : &nbsp;&nbsp; <a href="https://bes.hybridbooking.com/extranet/transport/edit_detail_boat/43/ID" id="get_step_status-ID" class="get_step_status active"> <span class="glyphicon glyphicon-ok active" id="status-check-lang-ID"></span> Bahasa Indonesia </a> &nbsp;&nbsp; <a href="https://bes.hybridbooking.com/extranet/transport/edit_detail_boat/43/EN" id="get_step_status-EN" class="get_step_status active"> <span class="glyphicon glyphicon-ok active" id="status-check-lang-EN"></span> English </a> &nbsp;&nbsp; </div>
							<div class="button-edit">
								<div class="pull-right" style="padding-top:10px"> <a href="#" data-href="https://bes.hybridbooking.com/extranet/transport/publish_unpublish_boat/0/17e62166fc8586dfa4d1bc0e1742c08b" data-classafter="danger" class="publish_unpublish_product activeStatus" data-original-title="" title="">
									<div style="color:#090"> <span data-content="Active" rel="popover" data-placement="right" data-trigger="hover" class="activeStatus" style="color:green;" data-original-title="" title=""><img src="https://bes.hybridbooking.com/public/images/on-button.png"></span> </div>
									</a> <a href="#" data-href="https://bes.hybridbooking.com/extranet/transport/publish_unpublish_boat/1/17e62166fc8586dfa4d1bc0e1742c08b" data-classafter="" class="publish_unpublish_product nonActiveStatus" hidden="" data-original-title="" title="">
									<div style="color:#F30"><span data-content="Non Active" rel="popover" data-placement="right" data-trigger="hover" class="nonActiveStatus" style="color:red" data-original-title="" title=""><img src="https://bes.hybridbooking.com/public/images/off-button.png"></span></div>
									</a> </div>
								<a href="https://bes.hybridbooking.com/extranet/transport/boat_gallery/43" class="btn btn-info btn-sm" title="Edit Boat"> <span class="glyphicon glyphicon-pencil"></span> Edit </a> </div>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="add-product-button"> <a href="https://bes.hybridbooking.com/extranet/transport/new_boat" class="btn btn-success btn-lg btn-block"> <span class="glyphicon glyphicon-plus"></span> New Boat </a> </div>
			<br>
			<br>
		</div>
	</div>
</div>

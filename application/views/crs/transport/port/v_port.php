<div class="sub-title">Available Port</div>
<br />
<div ng-init="loadDataAvailablePort();loadDataPortList();">

	<div ng-show='!DATA.availablePort'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	
	<div ng-show='DATA.availablePort'>
		<table class="table">
			<tr ng-repeat='port in DATA.availablePort.ports'>
				<td width="40">{{$index+1}}.</td>
				<td>{{port.port_code}} - {{port.name}}</td>
				<td width="50" align="right">
					<a href="" style="color:red" ng-click='removeAvailablePort(port)'><span class="glyphicon glyphicon-trash"></span></a>
				</td>
			</tr>
		</table>
	</div>
	
	<hr>
	<div class="form-inline">
		Add Port :
		<select class="form-control input-sm" ng-model='add_port_id'>
			<option value="">-- Add New Port --</option>
			<option ng-repeat='port in DATA.portList.ports' value="{{port.id}}">{{port.port_code}} - {{port.name}}</option>
		</select>
		<button type="button" ng-disabled='!add_port_id || add_port_id==""' class="btn btn-info btn-sm" ng-click="addAvailablePort(add_port)">Add</button>
		{{add_port}}
	</div>
</div>

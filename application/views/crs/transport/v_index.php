<?php /*?><div class="pull-right">
	<a ui-sref="transport.boats" class="btn btn-sm btn-info" title="Show Boat List">
		<i class="fa fa-ship" aria-hidden="true"></i> Boat
	</a>
	<a ui-sref="transport.trips_schedule" class="btn btn-sm btn-info" title="Show Boat List">
		<i class="fa fa-clock-o" aria-hidden="true"></i> Schedule
	</a>
</div><?php */?>

<div class="pull-right">
	<ul class="nav nav-pills">
	  	<li role="presentation" class="port"><a ui-sref="transport.port"><i class="fa fa-anchor" aria-hidden="true"></i> Port</a></li>
		<li role="presentation" class="boats"><a ui-sref="transport.boats"><i class="fa fa-ship" aria-hidden="true"></i> Boats</a></li>
		<li role="presentation" class="schedules"><a ui-sref="transport.trips_schedule"><i class="fa fa-clock-o" aria-hidden="true"></i> Schedules</a></li>
        <li role="presentation" class="packages"><a ui-sref="transport.packages"><i class="fa fa-cubes" aria-hidden="true"></i> Packages</a></li>
	</ul>
</div>

<h1>Trips & Schedules</h1>
{{pills_submenu}}
<div ui-view></div>
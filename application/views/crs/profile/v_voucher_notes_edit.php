<div class="modal" id="modal-edit-voucher-notes" tabindex="-1" role="dialog" aria-labelledby="ModalEditPicdrpLabel">
	<form method="post" ng-submit="save_voucher_notes($event)">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="ModalEditPicdrpLabel">Edit Voucher Notes</h4>
			</div>
			<div class="modal-body">
            	<div ng-show='my_voucher_notes.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in my_voucher_notes.error_msg'>{{err}}</li></ul></div>
				
                <p>
                	Type : 
                    <select class="form-control input-sm" ng-model='my_voucher_notes.type' style="display:inline; width:auto" required="required">
                    	<option value=""></option>
                    	<option value="LIST">List</option>
                        <option value="TEXT">Text</option>
                    </select>
                </p>
                <hr />
                <ol style="padding-left:20px; margin-bottom:0" ng-show="my_voucher_notes.type=='LIST'">
					<?php for((int)$i=0;$i<15;$i++){ ?>
                    <li>
                        <input ng-model="my_voucher_notes.list[<?=$i?>]" class="form-control input-sm" maxlength="200" type="text" style="display:inline; margin-bottom:5px" />
                    </li>
                    <?php } ?>
                </ol>
                <div ng-show="my_voucher_notes.type=='TEXT'">
                	<textarea class="form-control input-sm" rows="8" ng-model='my_voucher_notes.text'></textarea>
                </div>
			</div>	
			<div class="modal-footer" style="text-align:center">
				<button type="submit" class="btn btn-primary btn-submit-edit-picdrp" >Save</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
    </form>
</div>
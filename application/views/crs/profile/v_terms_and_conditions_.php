<div ng-init="loadDataTermsAndConditions()">
	<div ng-show="!terms_and_conditions">
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
    <br />
    <div ng-show="terms_and_conditions">
    	<div class="sub-title">Voucher Terms And Conditions</div>
        <br />
        <div ng-show="terms_and_conditions">
            <ol>
                <li ng-repeat='tac in terms_and_conditions'>{{tac}}</li>
            </ol>
            <a href="" data-toggle="modal" data-target="#modal-edit-tac" ng-click='edit_terms_and_conditions(terms_and_conditions)'>
                <button type="button" class="btn btn-sm btn-info btn-xs"><span class="glyphicon glyphicon-edit"></span>  Edit</button>
            </a>
        </div>
        <br />
        
        <div class="sub-title">Voucher Notes</div>
        <div ng-show="DATA.notes">
        	<br />
            <ol ng-show="DATA.notes.list">
                <li ng-repeat='note in DATA.notes.list'>{{note}}</li>
            </ol>
            <div ng-show="DATA.notes.text">
                <p>{{DATA.notes.text}}</p>
            </div> 
        </div>
        <div>
	        <a href="" data-toggle="modal" data-target="#modal-edit-voucher-notes" ng-click='edit_voucher_notes(DATA.notes)'>
				<button type="button" class="btn btn-sm btn-info btn-xs"><span class="glyphicon glyphicon-edit"></span>  Edit</button>
        	</a>
        </div>
    </div>
    
    <?php $this->load->view("crs/profile/v_terms_and_conditions_edit") ?>
    <?php $this->load->view("crs/profile/v_voucher_notes_edit") ?>

</div>
<script>activate_sub_menu_agent_detail("terms_and_conditions");</script>
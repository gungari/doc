<div class="modal" id="modal-edit-tac" tabindex="-1" role="dialog" aria-labelledby="ModalEditPicdrpLabel">
	<form method="post" ng-submit="save_term_and_condition($event)">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="ModalEditPicdrpLabel">Edit Voucher Terms And Conditions</h4>
			</div>
			<div class="modal-body">
            	<div ng-show='my_terms_and_conditions.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in my_terms_and_conditions.error_msg'>{{err}}</li></ul></div>
	
                <ol style="padding-left:20px; margin-bottom:0">
					<?php for((int)$i=0;$i<15;$i++){ ?>
                    <li>
                        <input ng-model="my_terms_and_conditions[<?=$i?>]" class="form-control input-sm" maxlength="200" type="text" style="display:inline; margin-bottom:5px" />
                    </li>
                    <?php } ?>
                </ol>
			</div>	
			<div class="modal-footer" style="text-align:center">
				<button type="submit" class="btn btn-primary btn-submit-edit-picdrp" >Save</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
    </form>
</div>
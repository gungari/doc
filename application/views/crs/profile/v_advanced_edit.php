<style type="text/css">
	.col-md-4{
		margin: 0 !important;
	}

	.col-md-3{
		margin: 0 !important;
	}
	.col-md-1{
		margin: 0 !important;
	}
</style>
 <div class="modal fade" id="modal-edit-advanced" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
		<form ng-submit="saveAdvanceSetting($event)" method="post" id="formRevisementPolicy">
		  <div class="modal-header">
		  	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="ModalEditPicdrpLabel">Edit Booking Reschedule &amp; Cancellation Policies</h4>
		  </div>

		   <div class="modal-body">
		   	
				<div class="show_error"></div>
				<input style="margin-left:3px;margin-bottom:15px" type="radio" class="chk-prior-cancellation" ng-model="check" ng-change="change_check(check)" value="1"/>
				Yes 
				&nbsp;&nbsp;
				<input style="margin-left:3px;margin-bottom:15px" type="radio" class="chk-prior-cancellation" ng-model="check" value="0" ng-change="change_check(check)" />
				No
				<div  ng-show="check_edit">
					<table class="table" style="width: 100%;">
						<thead>
							<tr class="successs">
								<th>Cancellation prior to arrival</th>
								<th colspan="2">Cancellation Fee</th>
							</tr>
						</thead>

                        <tr ng-repeat="rule in rules_edit">
                       		
							<td> 
								<div class="row" style="width: 200px;">
									<div class="col-md-2" style="margin-top: 6px;">
										<span>{{min[$index]}}{{num(rule.f_min_activation_days, $index)}}</span>
									</div>
									<div class="col-md-1" style="margin-top: 10px;"> - </div>
									<div class="col-md-4" style="margin-bottom: 5px;">
										<span><input type="number" name="f_min_activation_days[]" ng-model="rule.f_min_activation_days" min="{{min[$index]}}" class="form-control input-sm input-days" style="width:60px" required> </span> 
										
									</div>
									<div class="col-md-2" style="margin-top: 6px;">
										<span>days</span>
									</div>
								</div>
							</td>
							<td>
								<div class="row">
									<div class="col-md-4">
										<input name="f_percentage[]" type="number" ng-model="rule.f_percentage" min="0" max="100" class="form-control input-sm input-percentage" style="width:60px" required>
									</div>
									<div class="col-md-2" style="margin-top: 6px;">
										%
									</div>
								</div>
							</td>
							<td width="5%">
								<a href="" style="color:red" ng-show="rules_edit.length == $index+1" ng-click="scheduleDetailRemoveLastItem()" ><i class="fa fa-remove" aria-hidden="true"></i></a>
							</td>
							
						</tr>

					</table>
					<br>
					<button type="button" class="btn btn-info btn-xs" id="btn_add_days" ng-click="addItem()">
						<span class="glyphicon glyphicon-plus"></span> add
					</button>
				</div>
				
				<div class="modal-footer" style="text-align:center">
					<button class="btn btn-info btn-sm" type="submit">Save</button>&nbsp;&nbsp;
					<a style="cursor: pointer;" data-dismiss="modal">Cancel</a>	
				</div>
				</form>
		   </div>
		   
		</div>
	</div>
</div>
<?php
	$email_user = isset($_SESSION["ss_login_CRS"]["credential"]["user"])?$_SESSION["ss_login_CRS"]["credential"]["user"]["email"]:$_SESSION["ss_login_CRS"]["vendor"]["email"];
	$code_user  = isset($_SESSION["ss_login_CRS"]["credential"]["user"])?$_SESSION["ss_login_CRS"]["credential"]["user"]["id"]:"master";
?>
<style type="text/css">
	.popup-edit .inline-edit-icon{
		display: none;
		font-size: 11px;
	}
	tr:hover .popup-edit .inline-edit-icon{
		display: none;
	}
	.inline-edit .inline-edit-icon{display:none}
	tr:hover .inline-edit .inline-edit-icon{display:none !important}
</style>
<h1>Profile</h1>

<ul class="nav nav-tabs sub-nav profile">
	<li role="presentation" class="profile"><a ui-sref="profile">Profile</a></li>
	<li role="presentation" class="terms_and_conditions"><a ui-sref="profile.terms_and_conditions">Terms And Conditions</a></li>
	<?php /*?><li role="presentation" class="contract-rates"><a ui-sref="agent.detail.contract_rates">Contract Rates</a></li><?php */?>
</ul>
<br />
<div ui-view>
	<div ng-show="!DATA.vendor">
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	<div ng-init="loadDataVendor()" ng-show="DATA.vendor">
		<div class="sub-title">Information</div>
		<div ng-show='DATA.vendor.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.vendor.error_msg'>{{err}}</li></ul></div>

		<table class="table table-hover" >
			<tbody>
				<tr>
					<td width="150">Business Name</td>
					<td width="5">:</td>
					<td>
						<div class="inline-edit">
							<div class="main-text">
								<span class="inline-edit-icon pull-right"><a style="cursor: pointer;"><span class="glyphicon glyphicon-pencil"></span>Edit</a></span>
								<strong>{{DATA.vendor.business_name}}</strong>
							</div>
							<div class="edit-text hidden-field">
								<form method="post" ng-submit="save_profile($event,'business_name')">
									<div class="cancel pull-right"><a style="cursor: pointer;">Cancel</a></div>
									<div class="input-group">
										<input type="text" required="required" class="form-control input-sm" ng-model="DATA.vendor.business_name" maxlength="100" />
										<span class="input-group-btn">
											<button class="btn btn-info btn-sm" type="submit">Save</button>
										</span>
									</div>
								</form>
							</div>
						</div>
					</td>
				</tr>
				<?php /* ?><tr>
					<td>Hybrid Pages</td>
					<td>:</td>
					<td><a ng-href="{{DATA.vendor.hybrid_pages}}" target="_blank">{{DATA.vendor.hybrid_pages}}</a></td>
				</tr><?php */ ?>
				<tr>
					<td>Website</td>
					<td>:</td>
					<td>
						<div class="inline-edit">
							<div class="main-text">
								<span class="inline-edit-icon pull-right"><a style="cursor: pointer;"><span class="glyphicon glyphicon-pencil"></span>Edit</a></span>
								<a ng-href="{{DATA.vendor.website}}" target="_blank">{{DATA.vendor.website}}</a>
							</div>
							<div class="edit-text hidden-field">
								<form method="post" ng-submit="save_profile($event,'website')">
									<div class="cancel pull-right"><a style="cursor: pointer;">Cancel</a></div>
									<div class="input-group">
										<input class="form-control input-sm" ng-model="DATA.vendor.website" type="url">
										<span class="input-group-btn">
											<button class="btn btn-info btn-sm" type="submit">Save</button>
										</span>
									</div>
								</form>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td>Address</td>
					<td>:</td>
					<td>{{DATA.vendor.full_address}}</td>
				</tr>
				<tr ng-init="loadTimezoneList()">
					<td>Time Zone</td>
					<td>:</td>
					<td>
						<div class="inline-edit">
							<div class="main-text">
								<span class="inline-edit-icon pull-right"><a style="cursor: pointer;"><span class="glyphicon glyphicon-pencil"></span>Edit</a></span>
								{{DATA.vendor.time_zone.area}}
							</div>
							<div class="edit-text hidden-field">
								<form method="post" ng-submit="save_profile($event,'time_zone')">
									<div class="cancel pull-right"><a style="cursor: pointer;">Cancel</a></div>
									<div ng-show="!DATA.timezone_list">
										<img src="<?=base_url("public/images/loading_bar.gif")?>" />
									</div>
									<div class="input-group" ng-show="DATA.timezone_list">
										<select class="form-control input-sm" ng-model="DATA.vendor.time_zone.code">
											<option ng-repeat="time_zone in DATA.timezone_list" value="{{time_zone.code}}">{{time_zone.name}}</option>
										</select>
										<span class="input-group-btn">
											<button class="btn btn-info btn-sm" type="submit">Save</button>
										</span>
									</div>
								</form>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td width="150" style="padding-top: 45px;">Logo</td>
					<td width="5" style="padding-top: 45px;">:</td>
					<td><div class="popup-edit">
							<span class="inline-edit-icon pull-right"><a data-toggle="modal" data-target="#modal_logo" style="cursor: pointer" ><span class="glyphicon glyphicon-pencil"></span>Edit</a></span>
							<div style="width:100px; height:100px">
								<div class="thumbnail">
									<img src="<?=base_url("public/images/no_image.jpg")?>" width="100%" ng-show="!DATA.vendor.logo" />
									<img ng-src="{{DATA.vendor.logo}}" width="100%" ng-show='DATA.vendor.logo' />
								</div>
							</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
		<div class="sub-title">Contact</div>
		<table class="table table-hover">
			<tr>
				<td width="150">Contact Person</td>
				<td width="5">:</td>
				<td>
					<div class="inline-edit">
						<div class="main-text">
							<span class="inline-edit-icon pull-right"><a style="cursor: pointer;"><span class="glyphicon glyphicon-pencil"></span>Edit</a></span>
							<strong class="value">{{DATA.vendor.contact.name}}</strong>
						</div>
						<div class="edit-text hidden-field">
							<form method="post" ng-submit="save_profile($event,'contact_name')">
								<div class="cancel pull-right"><a style="cursor: pointer;">Cancel</a></div>
								<div class="input-group">
									<input type="text" required="required" class="form-control input-sm" ng-model="DATA.vendor.contact.name" maxlength="100" />
									<span class="input-group-btn">
										<button class="btn btn-info btn-sm" type="submit">Save</button>
									</span>
								</div>
							</form>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td>Email Address</td>
				<td>:</td>
				<td>
					<div class="inline-edit">
						<div class="main-text">
							<span class="inline-edit-icon pull-right"><a style="cursor: pointer;"><span class="glyphicon glyphicon-pencil"></span>Edit</a></span>
							<span class="value">{{DATA.vendor.contact.email}}</span>
						</div>
						<div class="edit-text hidden-field">
							<form method="post" ng-submit="save_profile($event,'contact_email')">
								<div class="cancel pull-right"><a style="cursor: pointer;">Cancel</a></div>
								<div class="input-group">
									<input type="text" required="required" class="form-control input-sm" ng-model="DATA.vendor.contact.email" maxlength="100" />
									<span class="input-group-btn">
										<button class="btn btn-info btn-sm" type="submit">Save</button>
									</span>
								</div>
							</form>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td>Telephone</td>
				<td>:</td>
				<td>
					<div class="inline-edit">
						<div class="main-text">
							<span class="inline-edit-icon pull-right"><a style="cursor: pointer;"><span class="glyphicon glyphicon-pencil"></span>Edit</a></span>
							<span class="value">{{DATA.vendor.telephone}}</span>
						</div>
						<div class="edit-text hidden-field" >
							<form method="post" ng-submit="save_profile($event,'telephone')" ng-init="loadPhoneCodeList()">
								<div class="cancel pull-right"><a style="cursor: pointer;">Cancel</a></div>
								<div ng-show="!DATA.phone_code_list">
									<img src="<?=base_url("public/images/loading_bar.gif")?>" />
								</div>
								<div class="input-group" ng-show="DATA.phone_code_list" style="max-width: 300px">
									<span class="input-group-btn">
										<select id="phoneNumberCountryCode" ng-model="DATA.vendor.telephone_detail.dial_code" class="form-control input-sm" title="Country Code" onchange="fn_set_phone_number()" style="width:60px;padding:0px;" required="required">
											<option ng-repeat="phone_list in DATA.phone_code_list" value="{{phone_list.dial_code}}" >{{phone_list.dial_code}}&ensp;&ensp;&ensp;{{phone_list.name}}</option>
										</select>
									</span>
									<input type="text" required="required" id="phoneNumber" class="form-control input-sm" ng-model="DATA.vendor.telephone_detail.number"  onchange="fn_set_phone_number()" placeholder="Phone number"/>
									
									<span class="input-group-btn">
										<button class="btn btn-info btn-sm" type="submit">Save</button>
									</span>
								</div>
							</form>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td>Other</td>
				<td>:</td>
				<td>
					<div class="inline-edit" ng-init="loadSosmedList()">
						<div class="main-text">
							<span class="inline-edit-icon pull-right"><a style="cursor: pointer;"><span class="glyphicon glyphicon-pencil"></span>Edit</a></span>
							<span class="value">
								<table class="table table-borderless table-condensed" style="width:90%; background:none">
									<tr ng-repeat="dt_other in DATA.vendor.contact.other">
										<td width="80">{{dt_other.type}}</td>
										<td width="5">:</td>
										<td>
										<span ng-show="dt_other.url == ''">{{dt_other.account}}</span>
										<span ng-hide="dt_other.url == ''"><a target="_blank" href="{{dt_other.url}}">{{dt_other.url}}</a></span>
										</td>
									</tr>
								</table>
							</span>
						</div>
						<div class="edit-text hidden-field">
							<div ng-show="!DATA.sosmed_list">
								<img src="<?=base_url("public/images/loading_bar.gif")?>" />
							</div>
							<form method="post" ng-submit="save_profile($event,'contact_other')" ng-show="DATA.sosmed_list">
								<table class="table-borderless table-condensed" style="width:90%; background:none">
									<tr id="sosmed_list_form">
										<td colspan="2">
											<div class="row contact" ng-repeat="dt_other in DATA.vendor.contact.other track by $index">
								                <div class="col-xs-4 col left">
								                    <select ng-model="DATA.vendor.contact.other[$index].code" class="form-control input-sm">
														<option ng-repeat="sosmedl in DATA.sosmed_list" value="{{sosmedl.id}}">{{sosmedl.name}}</option>
													</select>
								                </div>
								                <div class="col-xs-7 col center"><input class="form-control input-sm" type="text" ng-model="DATA.vendor.contact.other[$index].account"></div>
								                <div class="col-xs-1 col right"><a title="Remove" data-ng-click="removeItem(dt_other)" class="remove-contact" style="color:red; cursor: pointer;"><span class="glyphicon glyphicon-remove"></span></a></div>
								            </div>
										</td>
									</tr>
									<tr>
										<td colspan="2"><a style="cursor: pointer;" data-ng-click="addItem()">+ Add another account</a></td>
									</tr>
									<tr>
										<td width="60"><button class="btn btn-info btn-sm" type="submit">Save</button></td>
										<td><div class="cancel"><a style="cursor: pointer;">Cancel</a></div></td>
									</tr>
								</table>
							</form>
						</div>
					</div>
				</td>
			</tr>
		</table>

		<div class="sub-title">Account Login</div>
		<table class="table table-hover" ng-init="loaddataChangePwd('<?=$email_user?>','<?=$code_user?>')">
			<tr>
				<td width="150">Email</td>
				<td width="5">:</td>
				<td>
					<strong><?=$email_user?></strong>
				</td>
			</tr>
			<tr>
				<td width="150">Code</td>
				<td width="5">:</td>
				<td>
					<strong>{{DATA.vendor.code}}</strong>
				</td>
			</tr>
			<tr style="display:none">
				<td>Password</td>
				<td>:</td>
				<td>
					<div class="inline-edit">
						<div class="main-text">
							<span class="inline-edit-icon" style="display:inherit; font-size:12px"><a style="cursor: pointer;"><span class="glyphicon glyphicon-pencil"></span> Change Password</a></span>
						</div>
						<div class="edit-text hidden-field">
							<form method="post" ng-submit="change_password($event)">
								<div style="background:#FAFAFA; padding:10px" class="contact-edit">
								    <table class="table" style="width:100%">
								        <tr>
								            <td width="150">Old Password</td>
								            <td><input ng-model="DATA.dataPwd.old_pwd" class="form-control input-sm" type="password"> </td>
								        </tr>
								        <tr>
								            <td>New Password</td>
								            <td><input ng-model="DATA.dataPwd.new_pwd" class="form-control input-sm" type="password"> </td>
								        </tr>
								        <tr>
								            <td>Retype New Password</td>
								            <td><input ng-model="DATA.dataPwd.ret_pwd" class="form-control input-sm" type="password"> </td>
								        </tr>
								        <tr>
								            <td></td>
								            <td>
								                <button class="btn btn-info btn-sm" type="submit">Save</button>
								                <span class="cancel"><a style="cursor: pointer">Cancel</a></span>
								            </td>
								        </tr>
								    </table>
								</div>
							</form>
						</div>
					</div>
				</td>
			</tr>
		</table>

		<div class="sub-title">Language</div>
		<table class="table table-hover">
		<tr>
			<td width="150">Language</td>
			<td width="5">:</td>
			<td>
				<div class="inline-edit">
					<div class="main-text">
						<span class="inline-edit-icon pull-right"><a style="cursor: pointer;"><span class="glyphicon glyphicon-pencil"></span>Edit</a></span>
						<span class="value" ng-repeat="(kay_lang,dt_lang) in DATA.vendor.language">
						<span ng-hide="kay_lang==0">, &nbsp;</span><strong>{{dt_lang.name}}</strong> <span ng-show="dt_lang.code == DATA.vendor.default_language">&nbsp;&nbsp;(Default)</span></span>
					</div>
					<div class="edit-text hidden-field">
						<form method="post" ng-submit="save_lang($event,'language')" ng-init="loadLanguageList()">
							<div class="cancel pull-right"><a style="cursor: pointer;">Cancel</a></div>
							<div ng-show="!DATA.language">
								<img src="<?=base_url("public/images/loading_bar.gif")?>" />
							</div>
							<div class="input-group" ng-show="DATA.language">
								<table class="table table-borderless" style="background-color: transparent;">
									<tr ng-repeat="Lang in DATA.language">
										<td style="padding-top: 0;padding-bottom: 0;">
											<label>
												<input type="checkbox" checkbox-lang >
												{{Lang.language}}
											</label>
										</td>
										<td style="padding-top: 0;padding-bottom: 0;">
											<label>
												<input type="radio" ng-model="DATA.vendor.default_language" value="{{Lang.code}}">
												Default
											</label>
										</td>
									</tr>
									<tr>
							            <td style="padding-top: 0;padding-bottom: 0;" colspan="2">
							                <button class="btn btn-info btn-sm" type="submit">Save</button>
							            </td>
							        </tr>
								</table>			
							</div>
						</form>
					</div>
				</div>
			</td>
		</tr>
		</table>
		
		<div class="sub-title">Default Currency</div>
		<table class="table table-hover">
		<tr>
			<td width="150">Currency</td>
			<td width="5">:</td>
			<td>
				<strong>{{DATA.vendor.currency.name}}</strong>
			</td>
		</tr>
		</table>
		<script>activate_sub_menu_agent_detail("profile");</script>
		<?php /*?>{{DATA.status}}<br />
		{{DATA}}<?php */?>
		<?php 
		//pre($_GET);
		//pre($_SESSION);
		?>
	</div>
</div>
<script type="text/javascript">
	function fn_set_phone_number() {
		var input_full_phone = $('#phone_number');
		var input_phone_country_code = $('#phoneNumberCountryCode');
		var input_phone_number = $('#phoneNumber');
		if(input_phone_country_code.length && input_phone_number.length) {
			var phone_code = input_phone_country_code.val();
			var phone_number = input_phone_number.val();
			// hapus angka 0 di depan pada nomor telepon
			var index_substr = 0;
			var stop_index = false;
			phone_number = phone_number.replace('+','').replace('-','').replace(' ','').replace(/\D/g,'');
			var digit_phone_number = phone_number.split('');
			for(i=0;i<digit_phone_number.length;i++) {
				if(!stop_index) {
					if(digit_phone_number[i]=='0') {
						index_substr++;
					} else {
						stop_index = true;
					}
				}
			}
			phone_number = phone_number.substring(phone_number.length, index_substr);
			input_phone_number.val(phone_number);
			if($.isNumeric(phone_number) && phone_number.length>0 && phone_number.length<=20) {
				input_full_phone.val(phone_code+' '+phone_number);
			} else {
				input_full_phone.val('');
			}
		}
	}
	
</script>
<!-- Modal -->
<div class="modal fade" id="modal_logo" tabindex="-1" role="dialog" aria-labelledby="modalLogo" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
      	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="modalLogo">Change Logo</h5>
      </div>
      <div class="modal-body">
      	<div ng-show='DATA.vendor.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.vendor.error_msg'>{{err}}</li></ul></div>
      	<form name="form" ng-submit="upload_logo($event)">
      		<input type="file" img-cropper-fileread image="cropper.sourceImage" />
      		<div>
      		     <canvas style="margin: auto;" width="270" height="250" id="canvas" image-cropper image="cropper.sourceImage" cropped-image="cropper.croppedImage" crop-width="200" crop-height="200" keep-aspect="true" touch-radius="30" crop-area-bounds="bounds"></canvas>
      		</div>
      		<input type="hidden" id="logo_croped" value="{{cropper.croppedImage}}" />
      		<div style="text-align: right;border-top: 1px solid #e5e5e5;padding-top: 10px;">
	      	 	<button type="submit" class="btn btn-info"><i class="fa fa-cut"></i>&nbsp;Crop & Change</button>&nbsp;
	      	 	<button type="reset" class="btn btn-danger" data-dismiss="modal">Cancel</button>
	      	</div>
      	</form>
      </div>
    </div>
  </div>
</div>

<?php /*?><h1>Profile</h1>

<ul class="nav nav-tabs sub-nav profile">
	<li role="presentation" class="profile"><a ui-sref="profile">Profile</a></li>
	<li role="presentation" class="terms_and_conditions"><a ui-sref="profile.terms_and_conditions">Terms And Conditions</a></li>
</ul>
<br />
<div ui-view>
	<div ng-init="loadDataVendor()">
		<div class="sub-title">Information</div>
		<table class="table table-hover">
			<tbody>
				<tr>
					<td width="150">Business Name</td>
					<td width="5">:</td>
					<td><strong>{{DATA.vendor.business_name}}</strong></td>
				</tr>
				<tr>
					<td>Hybrid Pages</td>
					<td>:</td>
					<td><a ng-href="{{DATA.vendor.hybrid_pages}}" target="_blank">{{DATA.vendor.hybrid_pages}}</a></td>
				</tr>
				<tr>
					<td>Website</td>
					<td>:</td>
					<td><a ng-href="{{DATA.vendor.website}}" target="_blank">{{DATA.vendor.website}}</a></td>
				</tr>
				<tr>
					<td>Address</td>
					<td>:</td>
					<td></td>
				</tr>
				<tr>
					<td>Time Zone</td>
					<td>:</td>
					<td>{{DATA.vendor.time_zone.area}}</td>
				</tr>
				<tr>
					<td width="150" style="padding-top: 45px;">Logo</td>
					<td width="5" style="padding-top: 45px;">:</td>
					<td>
						<div style="width:100px; height:100px">
							<div class="thumbnail">
								<img src="<?=base_url("public/images/no_image.jpg")?>" width="100%" ng-show="!DATA.vendor.logo" />
								<img ng-src="{{DATA.vendor.logo}}" width="100%" ng-show='DATA.vendor.logo' />
							</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
		<script>activate_sub_menu_agent_detail("profile");</script>
	</div>
</div><?php */?>
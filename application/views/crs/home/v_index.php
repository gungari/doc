<div ng-init="loadDataVendor()">
	<h1>HOME</h1>
	<div id="php" class="section-block">
        <div class="code-block">
            <h6>PHP Code Example</h6>
            <pre>
            	<code class="language-php">
						<p>Parameter :
							<br>
							openvoucher_code	(O/M)	(string) 	-> [M] if update data
							name				(M)		(string)
							description			(M)		(text)
							start_date			(M)		(date)
							end_date			(M)		(date)
							price				(M)		(number)
							minimum_purchase	(M)		(number)
						</p>
				</code>
			</pre>
		</div><!--//code-block-->
	</div><!--//section-block-->
</div>
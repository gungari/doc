<div class="sub-title">
	<span ng-show='!DATA.current_invoice.invoice_code'><i class="fa fa-plus-square" aria-hidden="true"></i> Add New</span> 
	<span ng-show='DATA.current_invoice.invoice_code'><i class="fa fa-pencil" aria-hidden="true"></i> Edit</span>
	Invoice {{DATA.current_invoice.invoice_code}}
</div>
<div ng-init="invoiceAddEdit();loadRekNumber()">
	<div class="products">
		<div>
			<div ng-show='!DATA.current_invoice.ready'>
				<img src="<?=base_url("public/images/loading_bar.gif")?>" />
			</div>

			<form class="form-inline" ng-submit="saveDataInvoice($event)" ng-show='DATA.current_invoice.ready'>
				
				<input type="hidden" name="create_by" value="<?=isset($_SESSION["ss_login_CRS"]["credential"]["user"])?$_SESSION["ss_login_CRS"]["credential"]["user"]['name']:$_SESSION["ss_login_CRS"]["vendor"]['business_name'];?>">
                <div class="products">
                    <div class="product">
                        <div class="table-responsive">
                            <table class="table table-condensed table-borderless" width="100%">
                                <tr>
                                    <td width="150">Filter By</td>
                                    <td width="110">From</td>
                                    <td width="110">To</td>
                                    <td width="200">Business Source</td>
                                    <td width="150">Type</td>
                                    <td width="130"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                	<td>
                                    	<select style="width: 100%; margin-top:5px;margin-bottom: 10px;" class="form-control input-sm " ng-model='DATA.current_invoice.filter_by'>
                                            <option value="BOOKINGDATE">Booking Date</option>
                                            <option value="TRIPDATE">Departure Date</option>
                                        </select>
                                    </td>
                                    <td><input style="width: 100%;margin-top:5px;margin-bottom: 10px;" type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='DATA.current_invoice.start_date' /></td>
                                    <td><input style="width: 100%;margin-top:5px;margin-bottom: 10px;" type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='DATA.current_invoice.finish_date' /></td>
                                    <td>
                                    	<select class="form-control input-sm" ng-model='DATA.current_invoice.agent_code' style="width:100%;margin-top:5px;margin-bottom: 10px;">
                                            <option value="">All</option>
                                            <option ng-show="agent.agent_code" value="{{agent.agent_code}}" ng-repeat="agent in DATA.agents | orderBy : 'name'">{{agent.name}}</option>
                                        </select>
                                    </td>
                                    <td>
                                    	<select style="width: 100%; margin-top:5px;margin-bottom: 10px;" class="form-control input-sm " ng-model='DATA.current_invoice.type'>
                                            <option value="RESERVATION">Reservation</option>
                                            <option value="OPENVOUCHER">Pre Sold Ticket</option>
                                        </select>
                                    </td>
                                    <td>
                                    	<button type="button" style="margin-top:5px;margin-bottom: 10px;" class="btn btn-sm btn-info form-control input-sm" data-ng-click="loadBookinganAgent(DATA.current_invoice.agent_code)"><i class="fa fa-check"></i> Ok</button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                	</div>
                </div>
                
                
				<?php /*?><div ng-show='DATA.current_invoice.error_desc.length>0' class="alert alert-danger show_error"><ul><li ng-repeat='err in DATA.current_invoice.error_desc'>{{err}}</li></ul></div>
				<div class="row" style="background-color: #f9f9f9;border:1px solid #eeeeee;margin: 5px 0 10px;padding: 10px 0">
					<div class="col-sm-4">
						<div class="row">
							<div class="col-xs-6">
								<span>From</span>
								<div><input style="width: 100%;margin-top:5px;margin-bottom: 10px;" type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='DATA.current_invoice.start_date' /></div>
							</div>
							<div class="col-xs-6">
								<span>To</span>
								<div><input style="width: 100%;margin-top:5px;margin-bottom: 10px;" type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='DATA.current_invoice.finish_date' /></div>
							</div>
						</div>
					</div>

					<div class="col-sm-4" >
						<span>Business Source</span>
						<div><select class="form-control input-sm" ng-model='DATA.current_invoice.agent_code' style="width:100%;margin-top:5px;margin-bottom: 10px;">
								<option value="">All</option>
								<option ng-show="agent.agent_code" value="{{agent.agent_code}}" ng-repeat="agent in DATA.agents | orderBy : 'name'">{{agent.name}}</option>
							</select></div>
					</div>
					<div class="col-sm-4">
						<div class="row">
							<div class="col-sm-8">
								<span>Type</span>
								<div ><select style="width: 100%; margin-top:5px;margin-bottom: 10px;" class="form-control input-sm " ng-model='DATA.current_invoice.type'>
										<option value="RESERVATION">Reservation</option>
										<option value="OPENVOUCHER">Pre Sold Ticket</option>
									</select>
								</div>
							</div>
							<div class="col-sm-4">
								<span>&nbsp;</span>
								<div><button type="button" style="margin-top:5px;margin-bottom: 10px;" class="btn btn-sm btn-info form-control input-sm" data-ng-click="loadBookinganAgent(DATA.current_invoice.agent_code)"><i class="fa fa-check"></i> Ok</button></div>
							</div>
						</div>
					</div>
				</div><?php */?>
		
				<div ng-show='DATA.loadingBookingan'>
					<img src="<?=base_url("public/images/loading_bar.gif")?>" />
				</div>
				<style type="text/css">
					.booking td label{
						font-weight: normal;
					}
				</style>
				<div ng-show="DATA.bookings.ready && !DATA.loadingBookingan && DATA.bookings.type =='RESERVATION'">
					<div class="sub-title">Reservation</div>
					<div ng-show='DATA.bookings.error_desc.length>0' class="alert alert-warning show_error"><ul><li ng-repeat='err in DATA.bookings.error_desc'>{{err}}</li></ul></div>
					<div ng-show='!DATA.bookings.error_desc' class="table-responsive">
						<table class="table table-condensed table-bordered table-hover">
							<tr class="info">
								<th width="30" ng-hide="DATA.current_invoice.disabled">
									<input ng-disabled="DATA.current_invoice.disabled" type="checkbox"  select-all="checkedData" id="{{booking.id}}">
								</th>
								<td align="center" width="90">Order#</td>
								<td align="center" width="110">Resv. Date</td>
								<td>Customer Name</td>
								<td align="center" width="40" title="Nationality">Nat.</td>
								<td width="200" ng-show="DATA.current_invoice.disabled">Booking Source</td>
								<td align="right">Amount</td>
							</tr>
							<tbody ng-repeat="booking in DATA.bookings.bookings" ng-show="booking.status_code!='CANCEL'" ng-hide="!booking.booking_code">
								<tr ng-class="{'danger':(booking.status_code=='CANCEL'), 'warning':(booking.status_code=='UNDEFINITE')}">
									<td rowspan="2" ng-hide="DATA.current_invoice.disabled">
										<input rel="checkedData" ng-disabled="DATA.current_invoice.disabled" type="checkbox" id="{{booking.id}}" checkbox-Booking data-amount="{{booking.balance}}" data-currency="{{booking.currency}}"  ng-checked="DATA.selectedlist.indexOf(booking.id) > -1">
									</td>
									<td align="center" rowspan="2">
										<a ui-sref="reservation.detail({'booking_code':booking.booking_code})" target="_blank"><strong>{{booking.booking_code}}</strong></a>
									</td>
									<td align="center">{{fn.newDate(booking.transaction_date) | date:'dd MMM yyyy'}} <small>{{fn.newDate(booking.transaction_date) | date:'HH:mm'}}</small> </td>
									<?php /*?><td align="center">{{fn.formatDate(booking.transaction_date, "dd M yy")}}</td><?php */?>
									<td>
										<strong>{{booking.customer.full_name}}</strong>
										<?php /*?><br /> {{booking.customer.email}}<?php */?>
									</td>
									<td align="center" class="text-capitalize" title="{{booking.customer.country_name}}">{{booking.customer.country_code}}</td>
									<td ng-show="DATA.current_invoice.disabled">
										<div ng-show='!booking.agent' class="text-capitalize">{{booking.source.toLowerCase()}}</div>
										<div ng-show='booking.agent'>{{booking.agent.name}}</div>
									</td>
									<td align="right">
										{{booking.currency}} {{fn.formatNumber(booking.balance, booking.currency)}}
									</td>
								</tr>
								<tr ng-class="{'danger':(booking.status_code=='CANCEL'), 'warning':(booking.status_code=='UNDEFINITE')}">
									<td colspan="{{DATA.bookings.colspan - 2 }}" style="padding:0 !important">
										
										<table class="table table-bordered table-condensed" style="margin:-1px !important; background:none">
											<tr ng-show='booking.remarks'>
												<td style="border-left:solid 5px #FFF; font-size:11px">
													<em>Remarks : {{booking.remarks}}</em>
												</td>
											</tr>
											<tr ng-repeat="voucher in booking.detail | orderBy : '-booking_detail_status_code'" ng-class="{'danger':(voucher.booking_detail_status_code=='CANCEL')}">
												<td style="border-left:solid 5px #EEE; font-size:11px">
													{{voucher.voucher_code}} - 
													{{fn.formatDate(voucher.date, "dd / M / y")}} - 
													{{voucher.departure.port.port_code}} ({{voucher.departure.time}})
													<span class="glyphicon glyphicon-chevron-right"></span>
													{{voucher.arrival.port.port_code}} ({{voucher.arrival.time}})
													
													<span ng-show="voucher.qty_1 > 0"> - {{voucher.qty_1}} Adl. </span>
													<span ng-show="voucher.qty_2 > 0"> - {{voucher.qty_2}} Chi. </span>
													<span ng-show="voucher.qty_3 > 0"> - {{voucher.qty_3}} Inf. </span>
													
													<span ng-show="voucher.is_pickup=='YES'"> - Pickup </span>
													<span ng-show="voucher.is_dropoff=='YES'"> - Dropoff </span>
												</td>
											</tr>
										</table>
									
									</td>
								</tr>
								<tr>
									<td colspan="{{DATA.bookings.colspan}}" style="background:#FAFAFA"></td>
								</tr>
							</tbody>
						</table>
						<nav aria-label="Page navigation" class="pull-right" ng-show="DATA.bookings.search.total_rows > DATA.bookings.search.per_page">
						  <ul class="pagination pagination-sm">
							<li ng-class="{'disabled':DATA.bookings.search.page <= 1}">
							  <a href="" ng-click='loadBookinganAgent(DATA.current_invoice.agent_code,DATA.bookings.search.page-1,DATA.bookings.total_amount)' aria-label="Prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
							</li>

							<li	ng-show="DATA.bookings.search.page-3 > 1">
			  					<a href="" ng-click='loadBookinganAgent(DATA.current_invoice.agent_code,1,DATA.bookings.total_amount)'>1</a>
			  				</li>
			  				<li class="disabled" ng-show="DATA.bookings.search.page-4 > 1">
			  					<a>...</a>
			  				</li>

							<li ng-repeat='pagination in DATA.bookings.search.pagination' ng-class="{'active':DATA.bookings.search.page == pagination}" ng-show="$index+1 > DATA.bookings.search.page-4 && $index+1 < DATA.bookings.search.page+4">
								<a href="" ng-click='loadBookinganAgent(DATA.current_invoice.agent_code,($index+1),DATA.bookings.total_amount)'>{{($index+1)}}</a>
							</li>

							<li class="disabled" ng-show="DATA.bookings.search.page+4 < DATA.bookings.search.number_of_pages">
			  					<a>...</a>
			  				</li>
			  				<li	ng-show="DATA.bookings.search.page+3 < DATA.bookings.search.number_of_pages">
			  					<a href="" ng-click='loadBookinganAgent(DATA.current_invoice.agent_code,DATA.bookings.search.number_of_pages,DATA.bookings.total_amount)'>{{DATA.bookings.search.number_of_pages}}</a>
			  				</li>

							<li ng-class="{'disabled':DATA.bookings.search.page >= DATA.bookings.search.number_of_pages}">
							  <a href="" ng-click='loadBookinganAgent(DATA.current_invoice.agent_code,DATA.bookings.search.page+1,DATA.bookings.total_amount)' aria-label="Next"><span class="glyphicon glyphicon-chevron-right"></span></a>
							</li>
						  </ul>
						</nav>
						<div class="clearfix"></div>
						<table class="table table-borderlesss" ng-hide="DATA.current_invoice.disabled">
							<tr>
								<td width="150"></td>
								<td align="right">
									<table class="table table-borderlesss" style="width: auto;">
										<tr>
											<td colspan="3" align="right">
												<a href="" select-All="checkedData"><i class="fa fa-check-square-o" style="font-size: 16px;"></i> Check All</a>
												&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; 
												<a href="" select-Allun="checkedData"><i class="fa fa-square-o" style="font-size: 16px;"></i> Uncheck All</a>
											</td>
										</tr>
										<tr>
											<td><strong>Checked</strong></td>
											<td><strong>&nbsp;:&nbsp;</strong></td>
											<td width="125" align="right"><strong class="g_total" data-amount="{{DATA.bookings.total_amount}}">{{DATA.bookings.currency}} {{fn.formatNumber(DATA.bookings.total_amount, DATA.bookings.currency)}} from {{DATA.uninvoicing.currency}} {{fn.formatNumber(DATA.uninvoicing.uninvoicing, DATA.uninvoicing.currency)}}</strong></td>
										</tr>
										<tr>
											<td>Due Date*</td>
											<td>&nbsp;:&nbsp;</td>
											<td><input type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='DATA.bookings.due' required="required" /></td>
										</tr>
										<tr ng-show="DATA.rekening.length>0">
											<td>Bank Account</td>
											<td>&nbsp;:&nbsp;</td>
											<td>
												<select class="form-control input-sm" ng-model="DATA.bookings.bank" style="max-width: 250px">
													<option value="">-- Select Bank Account --</option>
													<option ng-show="bank.id" ng-repeat="bank in DATA.rekening" value="{{bank.id}}">{{bank.bank_name+" - "+bank.account_number}}</option>
												</select>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<textarea placeholder="Remarks" ng-model="DATA.bookings.remarks" class="form-control" style="width: 100%"></textarea>
								</td>
							</tr>
							<tr>
								<td width="150"></td>
								<td align="right">
									<button type="submit" class="btn btn-primary">Submit</button>
									<!--<button ng-show="DATA.selectedlist.length<=0" type="button" class="btn btn-primary" disabled="disabled">Save</button>-->
									&nbsp;&nbsp;&nbsp;
									<a ng-show='!DATA.current_invoice.invoice_code' ui-sref="invoice"><strong>Cancel</strong></a>
									<a ng-show='DATA.current_invoice.invoice_code' ui-sref="invoice.detail({'invoice_code':DATA.current_invoice.invoice_code})"><strong>Cancel</strong></a>
								</td>

							</tr>
						</table>
					</div>
				</div>
				<div ng-show="DATA.bookings.ready && !DATA.loadingBookingan && DATA.bookings.type =='OPENVOUCHER'">
					<div class="sub-title">Pre Sold Ticket</div>
					<div ng-show='DATA.bookings.error_desc.length>0' class="alert alert-warning show_error"><ul><li ng-repeat='err in DATA.bookings.error_desc'>{{err}}</li></ul></div>
					<div ng-show='!DATA.bookings.error_desc' class="table-responsive">
						<table class="table table-condensed table-bordered table-hover">
							<tr class="info">
								<th width="30" ng-hide="DATA.current_invoice.disabled"></th>
								<td align="center" width="200">Pre Sold Ticket Code#</td>
								<td align="center" width="110">Resv. Date</td>
								<td align="center" ng-show="DATA.current_invoice.disabled">Booking Source</td>
								<td align="right">Amount</td>
							</tr>
							<tbody ng-repeat="booking in DATA.bookings.bookings" ng-show="booking.status_code!='CANCEL' " ng-hide="!booking.open_voucher_code">
								<tr ng-class="{'danger':(booking.status_code=='CANCEL'), 'warning':(booking.status_code=='UNDEFINITE')}">
									<td rowspan="2" ng-hide="DATA.current_invoice.disabled">
										<input rel="checkedDataOT" ng-disabled="DATA.current_invoice.disabled" type="checkbox" id="{{booking.open_voucher_code}}"  checkbox-Bookopticket data-amount="{{booking.balance}}" data-currency="{{booking.currency}}" ng-checked="DATA.selectedlist.indexOf(booking.open_voucher_code) > -1">
									</td>
									<td rowspan="2">
										<a ui-sref="open_voucher.booking_detail.voucher_code({'booking_code':booking.booking_code})" target="_blank"><strong>{{booking.open_voucher_code}}</strong></a>
									</td>
									<td align="center">{{fn.newDate(booking.transaction_date) | date:'dd MMM yyyy'}} <small>{{fn.newDate(booking.transaction_date) | date:'HH:mm'}}</small> </td>
									<td ng-show="DATA.current_invoice.disabled">
										<div ng-show='!booking.agent.name' class="text-capitalize">{{booking.customer.full_name}}</div>
										<div ng-show='booking.agent.name'>{{booking.agent.name}}</div>
									</td>
									<td align="right">
										{{booking.currency}} {{fn.formatNumber(booking.balance, booking.currency)}}
									</td>
								</tr>
								<tr ng-class="{'danger':(booking.status_code=='CANCEL'), 'warning':(booking.status_code=='UNDEFINITE')}">
									<td colspan="{{DATA.bookings.colspan - 2 }}" style="padding:0 !important">
										
										<table class="table table-bordered table-condensed" style="margin:-1px !important; background:none">
											<tr ng-show='booking.remarks'>
												<td style="border-left:solid 5px #FFF; font-size:11px">
													<em>Remarks : {{booking.remarks}}</em>
												</td>
											</tr>
										</table>
										
										<?php /*?><table class="table table-bordered table-condensed" style="margin:-1px !important">
											<tr ng-repeat='voucher in booking.detail' ng-class="{'danger':(booking.status_code=='CANCEL'), 'warning':(booking.status_code=='UNDEFINITE')}">
												<td width="100">{{voucher.voucher_code}}</td>
												<td width="90">{{fn.formatDate(voucher.date, "dd M yy")}}</td>
												<td width="90">{{voucher.departure.port.port_code}} ({{voucher.departure.time}})</td>
												<td width="90">{{voucher.arrival.port.port_code}} ({{voucher.arrival.time}})</td>
												<td width="45" align="center" title="Pickup">{{(voucher.is_pickup=='YES')?'PU':'-'}}</td>
												<td width="45" align="center" title="Dropoff">{{(voucher.is_dropoff=='YES')?'DO':'-'}}</td>
												<td></td>
											</tr>
										</table><?php */?>
										
									</td>
								</tr>
								<tr>
									<td colspan="{{DATA.bookings.colspan }}" style="background:#FAFAFA"></td>
								</tr>
							</tbody>

							<!--<tr  class="booking">
								
								<td><label for="{{booking.booking_code}}" >#{{booking.booking_code}}</label></td>
								<td><label for="{{booking.booking_code}}" >{{booking.customer.first_name}} {{booking.customer.last_name}}</label></td>
								<td><label for="{{booking.booking_code}}" >{{booking.currency}}</label></td>
								<td align="right"><label for="{{booking.booking_code}}" >{{booking.total_before_discount | number:2}}</label></td>
								<td align="right"><label for="{{booking.booking_code}}" >{{booking.discount.amount | number:2}}</label></td>
								<td align="right"><label for="{{booking.booking_code}}" >{{booking.grand_total | number:2}}</label></td>
							</tr>-->
						</table>
						
						<nav aria-label="Page navigation" class="pull-right" ng-show="DATA.bookings.search.total_rows > DATA.bookings.search.per_page">
						  <ul class="pagination pagination-sm">
							<li ng-class="{'disabled':DATA.bookings.search.page <= 1}">
							  <a href="" ng-click='loadBookinganAgent(DATA.current_invoice.agent_code,DATA.bookings.search.page-1,DATA.bookings.total_amount)' aria-label="Prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
							</li>
							
							<li	ng-show="DATA.bookings.search.page-3 > 1">
			  					<a href="" ng-click='loadBookinganAgent(DATA.current_invoice.agent_code,1,DATA.bookings.total_amount)'>1</a>
			  				</li>
			  				<li class="disabled" ng-show="DATA.bookings.search.page-4 > 1">
			  					<a>...</a>
			  				</li>

							<li ng-repeat='pagination in DATA.bookings.search.pagination' ng-class="{'active':DATA.bookings.search.page == pagination}" ng-show="$index+1 > DATA.bookings.search.page-4 && $index+1 < DATA.bookings.search.page+4">
								<a href="" ng-click='loadBookinganAgent(DATA.current_invoice.agent_code,($index+1),DATA.bookings.total_amount)'>{{($index+1)}}</a>
							</li>

							<li class="disabled" ng-show="DATA.bookings.search.page+4 < DATA.bookings.search.number_of_pages">
			  					<a>...</a>
			  				</li>
			  				<li	ng-show="DATA.bookings.search.page+3 < DATA.bookings.search.number_of_pages">
			  					<a href="" ng-click='loadBookinganAgent(DATA.current_invoice.agent_code,DATA.bookings.search.number_of_pages,DATA.bookings.total_amount)'>{{DATA.bookings.search.number_of_pages}}</a>
			  				</li>

							<li ng-class="{'disabled':DATA.bookings.search.page >= DATA.bookings.search.number_of_pages}">
							  <a href="" ng-click='loadBookinganAgent(DATA.current_invoice.agent_code,DATA.bookings.search.page+1,DATA.bookings.total_amount)' aria-label="Next"><span class="glyphicon glyphicon-chevron-right"></span></a>
							</li>
						  </ul>
						</nav>
						<table class="table table-borderlesss" ng-hide="DATA.current_invoice.disabled">
							<tr>
								<td width="150"></td>
								<td align="right">
									<table class="table table-borderlesss" style="width: auto;">
										<tr>
											<td colspan="3" align="right">
												<a href="" select-Allot="checkedDataOT"><i class="fa fa-check-square-o" style="font-size: 16px;"></i> Check All</a>
												&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; 
												<a href="" select-Allunot="checkedDataOT"><i class="fa fa-square-o" style="font-size: 16px;"></i> Uncheck All</a>
											</td>
										</tr>
										<tr>
											<td><strong>Grand Total</strong></td>
											<td><strong>&nbsp;:&nbsp;</strong></td>
											<td width="125" align="right"><strong class="g_total" data-amount="{{DATA.bookings.total_amount}}">{{DATA.bookings.currency}} {{fn.formatNumber(DATA.bookings.total_amount, DATA.bookings.currency)}}</strong></td>
										</tr>
										<tr>
											<td>Due Date*</td>
											<td>&nbsp;:&nbsp;</td>
											<td><input type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='DATA.bookings.due' required="required" /></td>
										</tr>
										<tr ng-show="DATA.rekening.length>0">
											<td>Bank Account</td>
											<td>&nbsp;:&nbsp;</td>
											<td>
												<select class="form-control input-sm" ng-model="DATA.bookings.bank" style="max-width: 250px">
													<option value="">-- Select Bank Account --</option>
													<option ng-show="bank.id" ng-repeat="bank in DATA.rekening" value="{{bank.id}}">{{bank.bank_name+" - "+bank.account_number}}</option>
												</select>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<textarea placeholder="Remarks" ng-model="DATA.bookings.remarks" class="form-control" style="width: 100%"></textarea>
								</td>
							</tr>
							<tr>
								<td width="150"></td>
								<td align="right">
									<button type="submit" class="btn btn-primary">Submit</button>
									<!--<button ng-show="DATA.selectedlist.length<=0" type="button" class="btn btn-primary" disabled="disabled">Save</button>-->
									&nbsp;&nbsp;&nbsp;
									<a ng-show='!DATA.current_invoice.invoice_code' ui-sref="invoice"><strong>Cancel</strong></a>
									<a ng-show='DATA.current_invoice.invoice_code' ui-sref="invoice.detail({'invoice_code':DATA.current_invoice.invoice_code})"><strong>Cancel</strong></a>
								</td>

							</tr>
						</table>
					</div>
				</div>
			</form>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

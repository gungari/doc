<div ng-init="printInvoiceDetailOpenVoucher()">
	<div class="no-print text-center" style="text-align:center !important">
		<select class="form-control input-md change-paper-size" ng-model='print_type' style="width:auto; display:inline">
			<option value="">Normal Paper</option>
			<option value="small"> Small Paper</option>
		</select>
		<br /><br />
	</div>
	<div class="small-paper" ng-show="print_type == 'small'">
		<div class="header">
			<div class="title"><?=$vendor["business_name"]?></div>
			<div><?=$vendor["address"]?>, <?=$vendor["regency"]?>, <?=$vendor["province"]?>, <?=$vendor["country"]?></div>
			<div>[P] : <?=$vendor["telephone"]?></div>
			<div>[E] : <?=$vendor["email"]?></div>
			<div><?=$vendor["website"]?></div>
		</div>
		
		<div class="text-center">
			<strong>INVOICE</strong><br />
			<div>
				<img src="<?=base_url()?>public/plugin/barcode.php?text={{DATA.current_invoice.invoice.invoice_code}}&size=40" />
			</div>
			<div style="font-size:16px">
				{{DATA.current_invoice.invoice.invoice_code}}
			</div>
		</div>
		
		<hr />
		<strong>INVOICE INFORMATION</strong><br />
		<table width="100%" class="print-table">
			<tr>
				<td width="90">Invoice code</td>
				<td><strong>#{{DATA.current_invoice.invoice.invoice_code}}</strong></td>
			</tr>
			<tr>
				<td>Invoice Date</td>
				<td><strong>{{fn.formatDate(DATA.current_invoice.invoice.invoice_date, "d MM yy")}}</strong></td>
			</tr>
			<tr>
				<td>Due Date</td>
				<td><strong>{{fn.formatDate(DATA.current_invoice.invoice.invoice_due, "d MM yy")}}</strong></td>
			</tr>
			<tr>
				<td>Status</td>
				<td>
						<strong ng-show="DATA.current_invoice.invoice.invoice_due >= DATA.current_invoice.now && DATA.current_invoice.invoice.invoice_total == DATA.current_invoice.invoice.balance">UNPAID</strong>
						<strong ng-show="DATA.current_invoice.invoice.balance == 0">PAID</strong>
						<strong ng-show="DATA.current_invoice.invoice.invoice_due >= DATA.current_invoice.now && DATA.current_invoice.invoice.invoice_total > DATA.current_invoice.invoice.balance && DATA.current_invoice.invoice.balance > 0">PENDING</strong>
						<strong ng-show="DATA.current_invoice.invoice.invoice_due < DATA.current_invoice.now && DATA.current_invoice.invoice.balance > 0">EXPIRED</strong>
						
				</td>
			</tr>
		</table>

		<hr/>
		<strong>BUSINESS SOURCE</strong><br />
		<table width="100%" class="print-table" ng-show="DATA.current_invoice.invoice.agent_name">
			<tr>
				<td width="90">Name</td>
				<td><strong>{{DATA.current_invoice.invoice.agent_name}}</strong></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><strong>{{DATA.current_invoice.invoice.agent_cont.email}}</strong></td>
			</tr>
			<tr>
				<td>Address</td>
				<td><strong>{{DATA.current_invoice.invoice.agent_addr}}</strong></td>
			</tr>
			<tr>
				<td>Telephone</td>
				<td><strong>{{DATA.current_invoice.invoice.agent_cont.phone}}</strong></td>
			</tr>
		</table>
		<hr />
		
		<hr />
		<strong>PRE SOLD TICKET</strong><br />
		<table class="table table-bordered table-condensed" width="100%" cellspacing="1" cellpadding="8">
			<tbody>
				<tr class="info table-header">
					<td>Pre Sold Ticket Code</td>
					<td>Total Amount</td>
				</tr>
			</tbody>
			<tr ng-repeat="invoice_detail in DATA.current_invoice.invoice.detail">
				<td><strong>{{invoice_detail.open_voucher_code}}</strong></td>
				<td align="right"><strong>{{invoice_detail.currency}} {{fn.formatNumber(invoice_detail.grandtotal, invoice_detail.currency)}}</strong></td>
			</tr>
			<tbody>
				<tr class="info table-header">
					<td align="right"><strong>Grand Total</strong></td>
					<td align="right">
						<strong class="ng-binding">
							{{DATA.current_invoice.invoice.invoice_curr}} {{fn.formatNumber(DATA.current_invoice.invoice.invoice_total, DATA.current_invoice.invoice.invoice_curr)}}
						</strong>
					</td>
				</tr>
				<tr class="info table-header">
					<td align="right"><strong>Total Payment</strong></td>
					<td align="right">
						<strong class="ng-binding">
							{{DATA.current_invoice.invoice.invoice_curr}} ({{fn.formatNumber(DATA.current_invoice.invoice.total_payment, DATA.current_invoice.invoice.invoice_curr)}})
						</strong>
					</td>
				</tr>
				<tr class="info table-header">
					<td align="right"><strong>Balance</strong></td>
					<td align="right">
						<strong class="ng-binding">
							{{DATA.current_invoice.invoice.invoice_curr}} {{fn.formatNumber(DATA.current_invoice.invoice.balance, DATA.current_invoice.invoice.invoice_curr)}}
						</strong>
					</td>
				</tr>
			</tbody>
		</table>
		<table class="table" ng-show="DATA.current_invoice.invoice.remarks" width="100%">
			<tbody>
				<tr class="info table-header">
					<td><strong>Remarks</strong></td>
				</tr>
				<tr>
					<td><pre style='font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;font-size: 13px;padding: 0;margin: 0;border: 0;background-color: transparent;line-height: 1;padding-bottom: 10px'>{{DATA.current_invoice.invoice.remarks}}</pre></td>
				</tr>
			</tbody>
		</table>
		<br />
		<hr />
	</div>
	
	<br />
	<div class="normal-paper" ng-show="print_type != 'small'">
		
		<div class="header">
			<div class="pull-right text-center" align="center">
				<div style="font-size:16px">INVOICE</div>
				<div>
					<img src="<?=base_url()?>public/plugin/barcode.php?text={{DATA.current_invoice.invoice.invoice_code}}&size=40" />
				</div>
				<div style="font-size:16px">
					{{DATA.current_invoice.invoice.invoice_code}}
				</div>
			</div>
			
			<div class="title"><?=$vendor["business_name"]?></div>
			<div><?=$vendor["address"]?>, <?=$vendor["regency"]?>, <?=$vendor["province"]?>, <?=$vendor["country"]?></div>
			<div>[P] : <?=$vendor["telephone"]?>, [E] : <?=$vendor["email"]?></div>
			<div><?=$vendor["website"]?></div>
		</div>
		
		<table width="100%">
			<tr>
				<td>
					<table width="100%" class="print-table" ng-show="DATA.current_invoice.invoice.agent_name">
						<tr>
							<td colspan="2">
								<strong>BUSINESS SOURCE</strong>
							</td>
						</tr>
						<tr>
							<td width="100">Name</td>
							<td><strong>{{DATA.current_invoice.invoice.agent_name}}</strong></td>
						</tr>
						<tr>
							<td>Email</td>
							<td><strong>{{DATA.current_invoice.invoice.agent_cont.email}}</strong></td>
						</tr>
						<tr>
							<td>Address</td>
							<td><strong>{{DATA.current_invoice.invoice.agent_addr}}</strong></td>
						</tr>
						<tr>
							<td>Telephone</td>
							<td><strong>{{DATA.current_invoice.invoice.agent_cont.phone}}</strong></td>
						</tr>
					</table>
				</td>
				<td width="250">
					<table class="print-table">
						<tr>
							<td colspan="2">
								<strong>INVOICE INFORMATION</strong>
							</td>
						</tr>
						<tr>
							<td width="100">Invoice code</td>
							<td width="150" align="right"><strong>#{{DATA.current_invoice.invoice.invoice_code}}</strong></td>
						</tr>
						<tr>
							<td>Invoice Date</td>
							<td align="right"><strong>{{fn.formatDate(DATA.current_invoice.invoice.invoice_date, "d MM yy")}}</strong></td>
						</tr>
						<tr>
							<td>Due Date</td>
							<td align="right"><strong>{{fn.formatDate(DATA.current_invoice.invoice.invoice_due, "d MM yy")}}</strong></td>
						</tr>
						<tr>
							<td>Status</td>
							<td align="right">
									<strong ng-show="DATA.current_invoice.invoice.invoice_due >= DATA.current_invoice.now && DATA.current_invoice.invoice.invoice_total == DATA.current_invoice.invoice.balance">UNPAID</strong>
									<strong ng-show="DATA.current_invoice.invoice.balance == 0">PAID</strong>
									<strong ng-show="DATA.current_invoice.invoice.invoice_due >= DATA.current_invoice.now && DATA.current_invoice.invoice.invoice_total > DATA.current_invoice.invoice.balance && DATA.current_invoice.invoice.balance > 0">PENDING</strong>
									<strong ng-show="DATA.current_invoice.invoice.invoice_due < DATA.current_invoice.now && DATA.current_invoice.invoice.balance > 0">EXPIRED</strong>
									
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
		<br />
		
		<table class="table table-bordered table-condensed" width="100%" cellspacing="1" cellpadding="8">
			<tr class="info table-header">
				<td colspan="6"><strong>PRE SOLD TICKET</strong></td>
			</tr>
			<tr class="info table-header">
					<td>Pre Sold Ticket Code</td>
					<td width="120" align="right">Total Amount</td>
				</tr>
			<tbody>
				<tr ng-repeat="invoice_detail in DATA.current_invoice.invoice.detail">
					<td><strong>{{invoice_detail.open_voucher_code}}</strong></td>
					<td align="right">{{invoice_detail.currency}} {{fn.formatNumber(invoice_detail.grandtotal, invoice_detail.currency)}}</td>
				</tr>
			</tbody>
			<tbody>
				<tr class="info table-header">
					<td align="right"><strong>Grand Total</strong></td>
					<td align="right">
						<strong class="ng-binding">
							{{DATA.current_invoice.invoice.invoice_curr}} {{fn.formatNumber(DATA.current_invoice.invoice.invoice_total, DATA.current_invoice.invoice.invoice_curr)}}
						</strong>
					</td>
				</tr>
				<tr class="info table-header">
					<td align="right"><strong>Total Payment</strong></td>
					<td align="right">
						<strong class="ng-binding">
							{{DATA.current_invoice.invoice.invoice_curr}} ({{fn.formatNumber(DATA.current_invoice.invoice.total_payment, DATA.current_invoice.invoice.invoice_curr)}})
						</strong>
					</td>
				</tr>
				<tr class="info table-header">
					<td align="right"><strong>Balance</strong></td>
					<td align="right">
						<strong class="ng-binding">
							{{DATA.current_invoice.invoice.invoice_curr}} {{fn.formatNumber(DATA.current_invoice.invoice.balance, DATA.current_invoice.invoice.invoice_curr)}}
						</strong>
					</td>
				</tr>
			</tbody>
		</table>
		<br/>
		<table class="table" ng-show="DATA.current_invoice.invoice.remarks" width="100%">
			<tbody>
				<tr class="info table-header">
					<td><strong>Remarks</strong></td>
				</tr>
				<tr>
					<td><pre style='font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;font-size: 13px;padding: 0;margin: 0;border: 0;background-color: transparent;line-height: 1;padding-bottom: 10px'>{{DATA.current_invoice.invoice.remarks}}</pre></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<script>
	$(".change-paper-size").change(function(){
		var _this = $(this);
		
		if (_this.val() == 'small'){
			$(".print_area").addClass("small_paper");
		}else{
			$(".print_area").removeClass("small_paper");
		}
	});
</script>

<style>


	.print_area{background:none !important; padding:0 !important;}
	.small_paper{width:8cm !important; margin:0 auto !important}
	.normal-paper{background:#FFF; padding:10px}
	.small-paper{width:8cm; margin:auto; padding:10px; background:#FFF}
	.small-paper .header .title{font-size:18px !important}

</style>


<?php
//pre($vendor)
?>
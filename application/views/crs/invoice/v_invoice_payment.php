<div class="sub-title"> Payment Information</div>
<br />
<div ng-init="loadDataInvoicePayment()" class="reservation-detail">
	<div ng-show='!(DATA.payment)'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	<div ng-show='(DATA.payment)'>
		<div class="table-responsive">
			<table class="table table-bordered table-payment">
				<tr class="info" style="font-weight:bold">
					<td align="center" width="100">Date</td>
					<td align="center" width="130">Code</td>
					<td>Description</td>
					<td width="130">Payment Type</td>
					<td align="right" colspan="2">Amount</td>
					<td width="40"></td>
				</tr>
				<tr ng-repeat='payment in DATA.payment.payment.detail'>
					<td align="center">{{fn.formatDate(payment.date, "dd M yy")}}</td>
					<td align="center">
						<span ng-show='payment.amount<0'>
							<a href="" data-toggle="modal" ng-click="paymentDetail(payment)" data-target="#payment-detail">
								{{payment.payment_code}}
							</a>
						</span>
						<span ng-show='payment.amount>=0'>{{payment.payment_code}}</span>
					</td>
					<td>{{payment.description}}</td>
					<td>{{payment.payment_type}}</td>
					<td align="center" width="50">{{payment.currency}}</td>
					<td align="right" width="110">
                    	<div>
                            <strong ng-show='payment.amount>=0'>{{fn.formatNumber(payment.amount, payment.currency)}}</strong>
                            <strong ng-show='payment.amount<0'>({{fn.formatNumber(payment.amount*-1, payment.currency)}})</strong>
                        </div>
                        <div ng-show="payment.payment_currency && payment.payment_currency != ''">
                        	<small><em>
                            	Paid in : <br />
                                ({{payment.payment_currency}} {{fn.formatNumber(payment.payment_amount*-1, payment.payment_currency)}})
                            </em></small>
                        </div>
					</td>
					<td align="center">
						<a href="" ng-click="paymentDetail(payment, true)" data-toggle="modal" data-target="#payment-detail" class="delete-icon" ng-show="payment.id != ''">
							<span class="glyphicon glyphicon-trash"></span>
						</a>
					</td>
				</tr>
				<tr>
					<td colspan="7"></td>
				</tr>
				<tr class="success" style="font-weight:bold">
					<td colspan="4" align="right">Total Transaction</td>
					<td>{{DATA.payment.payment.currency}}</td>
					<td align="right">{{fn.formatNumber(DATA.payment.payment.total_transaction, DATA.payment.payment.currency)}}</td>
					<td></td>
				</tr>
				<tr class="success" style="font-weight:bold">
					<td colspan="4" align="right">Total Payment</td>
					<td>{{DATA.payment.payment.currency}}</td>
					<td align="right">({{fn.formatNumber(DATA.payment.payment.total_payment, DATA.payment.payment.currency)}})</td>
					<td></td>
				</tr>
				<tr style="font-weight:bold" ng-class="{'danger':(DATA.payment.payment.balance>0), 'info':(DATA.payment.payment.balance<=0)}">
					<td colspan="4" align="right">Outstanding Invoice</td>
					<td>{{DATA.payment.payment.currency}}</td>
					<td align="right">{{fn.formatNumber(DATA.payment.payment.balance, DATA.payment.payment.currency)}}</td>
					<td></td>
				</tr>
			</table>
		</div>
		<hr>
		<div class="add-product-button" ng-show="DATA.payment.payment.balance > 0"> 
			<a href="" class="btn btn-success btn-lg btn-block" data-toggle="modal" ng-click='addEditPayment()' data-target="#add-edit-payment"> <span class="glyphicon glyphicon-plus"></span> Add Payment </a> 
		</div>
		<br>
		<br>
	</div>
	
	<!-- modal add payment -->	
	<div class="modal fade" id="add-edit-payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <form ng-submit='saveDataPayment($event)'>
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">
				<span ng-show='!DATA.myPayment.id'>Add</span><span ng-show='DATA.myPayment.id'>Edit</span> Payment
			</h4>
		  </div>
		  <div class="modal-body">
		  	<div ng-show='DATA.myPayment.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myPayment.error_msg'>{{err}}</li></ul></div>
			<table class="table table-borderless table-condenseds">
				<tr>
					<td width="130">Invoice Code</td>
					<td><strong>#{{DATA.current_invoice.invoice.invoice_code}}</strong></td>
				</tr>
				<tr>
					<td>Payment Date*</td>
					<td><input placeholder="Payment Date" required="required" type="text" class="form-control input-md datepicker" ng-model='DATA.myPayment.date' style="width:150px" /></td>
				</tr>
				<tr>
					<td>Payment Type*</td>
					<td>
						<select required="required" class="form-control input-md" ng-model='DATA.myPayment.payment_type' ng-change='changePaymentType()'>
							<option value="" disabled="disabled">-- Select Payment Type --</option>
							<option value="{{payment_method.code}}" ng-repeat='payment_method in $root.DATA_payment_method'
								ng-hide="payment_method.code=='OPENVOUCHER' || payment_method.code=='ONLINE'">
								<div ng-hide="payment_method.name == 'Deposit'">{{payment_method.name}}</div>
							</option>
                            <?php /*?><option value="CC">Credit Card/Debit Card</option>
							<option value="ATM">Bank Transfer</option>
							<option value="CASH">Cash</option>
							<option value="ACL" ng-show='agent_allow_to_use_acl' ng-disabled="DATA.myPayment.amount > DATA.current_booking.booking.agent.out_standing_invoice.current_limit">
	                        	Agent Credit Limit
	                            (Maksimum : {{DATA.current_booking.booking.agent.out_standing_invoice.currency}} {{fn.formatNumber(DATA.current_booking.booking.agent.out_standing_invoice.current_limit,DATA.current_booking.booking.agent.out_standing_invoice.currency)}})
	                            {{(DATA.myPayment.amount > DATA.current_booking.booking.agent.out_standing_invoice.current_limit)?<?="'<-- Cannot use ACL (Over Limit)'"?>:""}}
	                        </option>
	                        <option value="DEPOSIT" ng-show='agent_allow_to_use_deposit'>
									Deposit Payment 
							</option><?php */?>
						</select>
					</td>
				</tr>
				<tr ng-show="DATA.payment_is_cc" class="header">
					<td>
                    	<span ng-show="DATA.payment_is_cc">Card Number</span>
                        <span ng-show="DATA.payment_is_atm">Account Number</span>
                    </td>
					<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.myPayment.account_number' /></td>
				</tr>
				<tr ng-show="DATA.payment_is_cc" class="header">
					<td>Name On Card</td>
					<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.myPayment.name_on_card' /></td>
				</tr>
				<tr ng-show="DATA.payment_is_cc" class="header">
					<td>Bank Name</td>
					<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.myPayment.bank_name' /></td>
				</tr>
				<tr ng-show="DATA.payment_is_cc" class="header">
					<td>Approval Number</td>
					<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.myPayment.payment_reff_number' /></td>
				</tr>
				<tr>
					<td>Remarks*</td>
					<td><input placeholder="Remarks" required="required" type="text" class="form-control input-md" ng-model='DATA.myPayment.description' /></td>
				</tr>
				<tr ng-show="DATA.myPayment.payment_type!='OPENVOUCHER'">
					<td>Payment Amount*</td>
					<td>
						<div class="input-group">
							<span class="input-group-addon" style="width:80px">{{DATA.current_invoice.invoice.invoice_curr}}</span>
							<input placeholder="Payment Amount" required="required" type="number" min="0" class="form-control input-md" ng-model='DATA.myPayment.amount' style="width:160px" ng-blur="convert_currency()" />
						</div>
					</td>
				</tr>
                <tr ng-show="DATA.myPayment.payment_type!='OPENVOUCHER' && $root.DATA_available_currency.currency">
                	<td>Paid In*</td>
                    <td>
                        <div class="input-group" ng-show='$root.DATA_available_currency.currency'>
							<select class="form-control input-md" ng-model='DATA.myPayment.payment_currency' style="width:80px" ng-change="convert_currency()" ng-disabled="!$root.DATA_available_currency.currency">
                            	<option ng-repeat="crr in $root.DATA_available_currency.currency" value="{{crr}}">{{crr}}</option>
                            </select>
                            <input placeholder="Payment Amount" disabled="disabled" type="number" min="0" step="0.1" class="form-control input-md payment_amount" ng-model='DATA.myPayment.payment_amount' style="width:160px" />
						</div>
                        <div ng-show='DATA.currency_converter.bookkeeping_rates' style="margin:10px 0">
                        	{{DATA.currency_converter.bookkeeping_rates.from.currency}} {{DATA.currency_converter.bookkeeping_rates.from.amount}} = 
                            {{DATA.currency_converter.bookkeeping_rates.to.currency}} {{DATA.currency_converter.bookkeeping_rates.to.amount}}
                        </div>
                        <em ng-show='DATA.myPayment.payment_currency_loading'>Loading...</em>
					</td>
                </tr>
			</table>
			
		  </div>
		  <div class="modal-footer" style="text-align:center">
		  	<button type="submit" class="btn btn-primary">Save</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
	  </div>
	  </form>
	</div>
	<!--/ modal add payment -->

	<!-- modal delete payment -->
	<div class="modal fade" id="payment-detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">
				Payment Detail #{{myPaymentDetail.payment_code}}
			</h4>
		  </div>
		  <div class="modal-body">
		  	<div ng-show='DATA.myPayment.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myPayment.error_msg'>{{err}}</li></ul></div>
			<table class="table table-borderless table-condensed">
				<tr>
					<td width="130">Booking Code</td>
					<td><strong>#{{DATA.current_invoice.invoice.invoice_code}}</strong></td>
				</tr>
				<tr>
					<td>Payment Date</td>
					<td>{{fn.formatDate(myPaymentDetail.date, "d MM yy")}}</td>
				</tr>
				<tr>
					<td>Payment Type</td>
					<td>{{myPaymentDetail.payment_type}}</td>
				</tr>
				<tr ng-show="myPaymentDetail.payment_type=='CC'">
					<td><span ng-show="myPaymentDetail.payment_type=='CC'">Credit Card Number</span><span ng-show="myPaymentDetail.payment_type=='ATM'">Account Number</span></td>
					<td>{{myPaymentDetail.account_number}}</td>
				</tr>
				<tr ng-show="myPaymentDetail.payment_type=='CC'">
					<td>Name On Card</td>
					<td>{{myPaymentDetail.name_on_card}}</td>
				</tr>
				<tr ng-show="myPaymentDetail.payment_type=='CC'">
					<td><span ng-show="myPaymentDetail.payment_type=='CC'">EDC Machine</span><span ng-show="myPaymentDetail.payment_type=='ATM'">Bank Name</span></td>
					<td>{{myPaymentDetail.bank_name}}</td>
				</tr>
				<tr ng-show="myPaymentDetail.payment_type=='CC'">
					<td>Approval Number</td>
					<td>{{myPaymentDetail.payment_reff_number}}</td>
				</tr>
				<tr ng-show="myPaymentDetail.payment_type=='OPENVOUCHER'">
					<td>Open Voucher Code</td>
					<td>{{myPaymentDetail.payment_reff_number}}</td>
				</tr>
				<tr>
					<td>Description</td>
					<td>{{myPaymentDetail.description}}</td>
				</tr>
				<tr>
					<td>Payment Amount</td>
					<td>{{myPaymentDetail.currency}} {{fn.formatNumber(myPaymentDetail.amount, myPaymentDetail.currency)}}</td>
				</tr>
			</table>
			<div ng-show='myPaymentDetail.delete_payment' class="alert alert-warning">
				<form ng-submit='submitDeletePayment($event)'>
					<strong>Are you sure to delete this payment?</strong><br />
					<label><input type="radio" name="delete_payment_confirmation" value="1" ng-model='myPaymentDetail.sure_to_delete' /> Yes</label>
					&nbsp;&nbsp;
					<label><input type="radio" name="delete_payment_confirmation" value="0" ng-model='myPaymentDetail.sure_to_delete' /> No</label>
					<div ng-show="myPaymentDetail.sure_to_delete=='1'">
						<hr style="margin:5px 0" />
						
						<div ng-show='myPaymentDetail.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in myPaymentDetail.error_msg'>{{err}}</li></ul></div>
						
						<strong>Remarks:</strong>
						<textarea rows='2' placeholder="Remarks" class="form-control input-md autoheight" ng-model='myPaymentDetail.delete_remarks' required="required"></textarea>
						<br />
						<button type="submit" class="btn btn-primary">Delete Payment</button>
					</div>
				</form>
			</div>
		  </div>
		  <div class="modal-footer" style="text-align:center">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
	  </div>
	</div>
	<!--/ modal delete payment -->

</div>
<script>activate_sub_menu_invioce_detail("payment");</script>
<style>
	table.table-payment .delete-icon{opacity:0.3; color:red}
	table.table-payment a:hover.delete-icon{opacity:1;}
</style>
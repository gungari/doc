<div ng-init="loadReportHome()">
<div class="pull-right">
	<div class="dropdown">
		<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			<i class="fa fa-bars" aria-hidden="true"></i> Menu
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
		<?php /*<li><a ui-sref="reports.revenue">Revenue</a></li> */?>
		<?php if ($vendor['category'] == 'transport') { ?>
			<li><a ui-sref="reports.forecast">Forecast</a></li>
		<?php }else if($vendor['category'] == 'activities'){ ?>
			<li><a ui-sref="reports.act_forecast">Forecast</a></li>
		<?php	} ?>
		<!-- <li><a ui-sref="reports.forecast">Forecast</a></li> -->
		<li><a ui-sref="reports.sales">Sales</a></li>
		<li><a ui-sref="reports.checkin">Check-in</a></li>
		<li><a ui-sref="reports.agent">Most Active Agencies</a></li>
		<li><a ui-sref="reports.cancel">Cancellation</a></li>
		
		<?php if ($vendor['category'] == 'transport') { ?>
			<li><a ui-sref="reports.expected">Expected Arrival</a></li>
		<?php }else if($vendor['category'] == 'activities'){ ?>
			<li><a ui-sref="reports.act_expected">Expected Arrival</a></li>
		<?php	} ?>
		</ul>
	</div>
</div>

<h1>Report</h1>

<div ui-view >
	
	<hr />

		<div class="panel panel-primary">
			<div class="panel-heading">
				<label>SALES REPORT ( Last 30 Days )</label>
			</div>
			<div class="panel-body panel-chart">
				<!-- <div ng-show='show_loading_sales'>
					<img src="<?=base_url("public/images/loading_bar.gif")?>" />
				</div>  -->
				<div ng-show='error_sales'>
					<span class="label label-danger">No Data</span>
				</div>
				<div ng-show="show_graph && !date_data && !show_error" class="report-content" style="display: block;">
					<dir class="embed-responsive embed-responsive-16by9 report-margin report-border">
						<div id="chartLine_home" style="width:100%; height:350px; margin: 0px auto;"></div>
					</dir>
					<span style="margin:0 15px;">Total Sales: <b>{{currency_home}} {{fn.formatNumber(total_home, currency_home)}}</b></span>
					<div class="pull-right">
						<a ui-sref="reports.sales"  target="_blank" class="btn btn-default btn-sm">More Detail</a>
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-primary">
			<div class="panel-heading">
				<label>CHECK-IN REPORT ( Last 30 Days )</label>
			</div>
			<div class="panel-body panel-chart">
				<div ng-show='show_loading_check'>
					<img src="<?=base_url("public/images/loading_bar.gif")?>" />
				</div> 
				<div ng-show='error_check'>
					<span class="label label-danger">No Data</span>
				</div>
				<div ng-show="show_graph && !date_data && !show_error" class="report-content" style="display: block;">
					<dir class="embed-responsive embed-responsive-16by9 report-margin report-border">
						<div id="checkLine_home" style="width:100%; height:350px; margin: 0px auto;"></div>
					</dir>
					<span style="margin:0 15px;">Check-in / Total : <b>{{total_checkin}} /  {{total_guest}}</b></span>
					<div class="pull-right">
						<a ui-sref="reports.checkin"  target="_blank" class="btn btn-default btn-sm">More Detail</a>
					</div>
				</div>
			</div>
		</div>
		<?php /*<div class="panel panel-primary">
			<div class="panel-heading">
				<label>REVENUE REPORT ( Last 30 Days )</label>
			</div>
			<div class="panel-body panel-chart">
				<div ng-show='show_loading_trans'>
					<img src="<?=base_url("public/images/loading_bar.gif")?>" />
				</div>  
				<div ng-show='error_trans'>
					<span class="label label-danger">No Data</span>
				</div>
				<div ng-show="show_graph && !date_data && !show_error" class="report-content" style="display: block;">
					<dir class="embed-responsive embed-responsive-16by9 report-margin report-border">
						<div id="revenueLine_home" style="width:100%; height:350px; margin: 0px auto;"></div>
					</dir>
					<span style="margin:0 15px;">Total Revenue: <b>{{currency}} {{fn.formatNumber(total_revenue, currency)}}</b></span>
					<div class="pull-right">
						<a ui-sref="reports.revenue"  target="_blank" class="btn btn-default btn-sm">More Detail</a>
					</div>
				</div>
			</div>
		</div>
		*/?>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<label>MOST ACTIVE AGENCIES REPORT ( Last 30 Days )</label>
			</div>
			<div class="panel-body panel-chart">
				<div ng-show='show_loading_agent'>
					<img src="<?=base_url("public/images/loading_bar.gif")?>" />
				</div>  
				<div ng-show='error_agent'>
					<span class="label label-danger">No Data</span>
				</div>
				<div ng-show="show_graph && !date_data && !show_error" class="report-content" style="display: block;">
					<dir class="embed-responsive embed-responsive-16by9 report-margin report-border">
						<div id="agentLine_home" style="width:100%; height:350px; margin: 0px auto;"></div>
					</dir>
					
					<div class="pull-right">
						<a ui-sref="reports.agent" target="_blank" class="btn btn-default btn-sm">More Detail</a>
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-primary">
			<div class="panel-heading">
				<label>CANCELATION REPORT ( Last 30 Days )</label>
			</div>
			<div class="panel-body panel-chart">
				<div ng-show='show_loading_cancel'>
					<img src="<?=base_url("public/images/loading_bar.gif")?>" />
				</div>  
				<div ng-show='error_cancel'>
					<span class="label label-danger">No Data</span>
				</div>
				<div ng-show="show_graph && !date_data && !show_error" class="report-content" style="display: block;">
					<dir class="embed-responsive embed-responsive-16by9 report-margin report-border">
						<div id="cancelLine_home" style="width:100%; height:350px; margin: 0px auto;"></div>
					</dir>
					<span>Total Cancel: <b>{{total_cancell}}</b></br> Cancellation Fee: <b>{{currency_cancell}} {{fn.formatNumber(amount_cancell, currency_cancell)}}</b></span>
					<div class="pull-right">
						<a ui-sref="reports.cancel" target="_blank" class="btn btn-default btn-sm">More Detail</a>
					</div>
				</div>
			</div>
		</div>
</div>
</div>
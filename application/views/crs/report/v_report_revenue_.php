<div class="sub-title">Revenue</div>
<div ng-init='loadReportRevenueTrans()'>
	<div class="products">
		<div class="product">
			<form ng-submit='loadReportRevenueTrans()'>
				<div class="table-responsive">
					<table class="table table-condensed table-borderless" width="100%">
						<tr>
							<td width="140">Date</td>
							<td width="200">Source</td>
							<td></td>
						</tr>
						<tr>
							<td>
								<input type="text" ng-model="search.date" class="form-control input-sm datepicker" placeholder="Date" style="width:140px; display:inline" />
							</td>
							<td>
								<select class="form-control input-sm" ng-model='search.booking_source'>
									<option value="">All</option>
									<option value="{{booking_source.code}}" ng-repeat='booking_source in $root.DATA_booking_source'>{{booking_source.name}}</option>
								</select>
							</td>
							<td>
								<button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button>
							</td>
						</tr>
					</table>
				</div>
			</form>
		
			<?php /*?><form ng-submit='loadReportRevenueTrans()'>
				Date : <input type="text" ng-model="filter_date" class="form-control input-sm datepicker" placeholder="Date" style="width:140px; display:inline" />
				<button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button>
			</form><?php */?>
		</div>
	</div>
	
	<div ng-show='show_loading_DATA'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	
	<div ng-show='!show_loading_DATA'>
		<div class="table-responsive" zzzstyle="overflow-x:scroll">
			<div style="width:1200px">
			<table class="table table-condensed table-bordered" style="font-size:11px">
				<tr class="header bold">
					<?php /*?><td rowspan="2" align="center" width="90">Order#<br />Booking Date</td><?php */?>
					<td rowspan="2" align="center" width="90">Voucher#<br />Date</td>
					<td rowspan="2">Customer Name</td>
					<td rowspan="2" align="center" width="30" title="Nationality">Nat.</td>
					<td rowspan="2" colspan="2" align="center">Trip</td>
					<td rowspan="2" width="170">Booking Source</td>
					<td colspan="3" align="center" title="Passenger">Passenger</td>
					<td colspan="3" align="center" title="Passenger">Price</td>
					<td rowspan="2" align="center" width="60">Pickup</td>
					<td rowspan="2" align="center" width="60">Dropoff</td>
					<td rowspan="2" align="center" width="60">Other</td>
					<td rowspan="2" align="center" width="65">Sub Total</td>
					<td rowspan="2" align="center" width="60">Discount</td>
					<td rowspan="2" align="center" width="60">Fee</td>
					<td rowspan="2" align="center" width="60">TOTAL</td>
				</tr>
				<tr class="header bold">
					<td width="30" align="center"><small>Adl.</small></td>
					<td width="30" align="center"><small>Chi.</small></td>
					<td width="30" align="center"><small>Inf.</small></td>
					<td width="60" align="center"><small>Adl.</small></td>
					<td width="60" align="center"><small>Chi.</small></td>
					<td width="60" align="center"><small>Inf.</small></td>
				</tr>
				<tbody ng-repeat='revenue_data in DATA.revenue'>
					<tr style="background:#FAFAFA">
						<td colspan="18"><strong>{{revenue_data.departure_port.name}} ({{revenue_data.departure_time}}) - {{revenue_data.arrival_port.name}} ({{revenue_data.arrival_time}})</strong></td>
					</tr>
					
					<tr ng-repeat='revenue in revenue_data.revenue'>
						<td align="center">
							<a href="" ng-click="openPopUpWindowVoucher('<?=site_url("home/print_page/#/print/voucher_trans/")?>', revenue.booking.booking_code, revenue.detail.voucher_code)">
								<strong>{{revenue.detail.voucher_code}}</strong>
							</a><br />
							<small>{{fn.formatDate(revenue.detail.date, "dd M yy")}}</small>
						</td>
						<td>{{revenue.booking.customer.full_name}}</td>
						<td align="center" class="text-capitalize" title="{{revenue.booking.customer.country_name}}">{{revenue.booking.customer.country_code}}</td>
						<td width="40" align="center">
							{{revenue.detail.departure.port.port_code}} <br /><small>{{revenue.detail.departure.time}}</small>
						</td>
						<td width="40" align="center">
							{{revenue.detail.arrival.port.port_code}} <br /><small>{{revenue.detail.arrival.time}}</small>
						</td>
						<td>
							<div ng-show='!revenue.booking.agent' class="text-capitalize">{{revenue.booking.source.toLowerCase()}}</div>
							<div ng-show='revenue.booking.agent'>{{revenue.booking.agent.name}}</div>
						</td>
						<td align="center">{{(revenue.detail.qty_1>0?revenue.detail.qty_1:'-')}}</td>
						<td align="center">{{(revenue.detail.qty_2>0?revenue.detail.qty_2:'-')}}</td>
						<td align="center">{{(revenue.detail.qty_3>0?revenue.detail.qty_3:'-')}}</td>
						
						<td align="right">{{fn.formatNumber((revenue.detail.rates.rates_1*revenue.detail.qty_1), revenue.detail.rates.currency)}}</td>
						<td align="right">{{fn.formatNumber((revenue.detail.rates.rates_2*revenue.detail.qty_2), revenue.detail.rates.currency)}}</td>
						<td align="right">{{fn.formatNumber((revenue.detail.rates.rates_3*revenue.detail.qty_3), revenue.detail.rates.currency)}}</td>
						
						<td align="right">{{fn.formatNumber(revenue.detail.pickup_price, revenue.detail.rates.currency)}}</td>
						<td align="right">{{fn.formatNumber(revenue.detail.dropoff_price, revenue.detail.rates.currency)}}</td>
						<td align="right">{{fn.formatNumber(revenue.detail.additional_charge_price, revenue.detail.rates.currency)}}</td>
						<td align="right">{{fn.formatNumber(revenue.detail.subtotal, revenue.detail.rates.currency)}}</td>
						<td align="right">{{fn.formatNumber(revenue.detail.discount, revenue.detail.rates.currency)}}</td>
						<td align="right">{{fn.formatNumber(revenue.detail.hybrid_fee, revenue.detail.rates.currency)}}</td>
						
						<td align="right">{{fn.formatNumber(revenue.detail.total, revenue.detail.rates.currency)}}</td>
					</tr>
				</tbody>
				<?php /*?><tr ng-repeat='revenue in DATA.revenue'>
					<td align="center">
						<a href="" ng-click="openPopUpWindowVoucher('<?=site_url("home/print_page/#/print/voucher_trans/")?>', revenue.booking.booking_code, revenue.detail.voucher_code)">
							<strong>{{revenue.detail.voucher_code}}</strong>
						</a><br />
						<small>{{fn.formatDate(revenue.detail.date, "dd M yy")}}</small>
					</td>
					<td>{{revenue.booking.customer.full_name}}</td>
					<td align="center" class="text-capitalize">{{revenue.booking.customer.country_code}}</td>
					<td width="40" align="center">
						{{revenue.detail.departure.port.port_code}} <br /><small>{{revenue.detail.departure.time}}</small>
					</td>
					<td width="40" align="center">
						{{revenue.detail.arrival.port.port_code}} <br /><small>{{revenue.detail.arrival.time}}</small>
					</td>
					<td align="center">{{(revenue.detail.qty_1>0?revenue.detail.qty_1:'-')}}</td>
					<td align="center">{{(revenue.detail.qty_2>0?revenue.detail.qty_2:'-')}}</td>
					<td align="center">{{(revenue.detail.qty_3>0?revenue.detail.qty_3:'-')}}</td>
					
					<td align="right">{{fn.formatNumber((revenue.detail.rates.rates_1*revenue.detail.qty_1), revenue.detail.rates.currency)}}</td>
					<td align="right">{{fn.formatNumber((revenue.detail.rates.rates_2*revenue.detail.qty_2), revenue.detail.rates.currency)}}</td>
					<td align="right">{{fn.formatNumber((revenue.detail.rates.rates_3*revenue.detail.qty_3), revenue.detail.rates.currency)}}</td>
					
					<td align="right">{{fn.formatNumber(revenue.detail.pickup_price, revenue.detail.rates.currency)}}</td>
					<td align="right">{{fn.formatNumber(revenue.detail.dropoff_price, revenue.detail.rates.currency)}}</td>
					<td align="right">{{fn.formatNumber(revenue.detail.subtotal, revenue.detail.rates.currency)}}</td>
					<td align="right">{{fn.formatNumber(revenue.detail.discount, revenue.detail.rates.currency)}}</td>
					<td align="right">{{fn.formatNumber(revenue.detail.hybrid_fee, revenue.detail.rates.currency)}}</td>
					
					<td align="right">{{fn.formatNumber(revenue.detail.total, revenue.detail.rates.currency)}}</td>
				</tr><?php */?>
			</table>
			</div>
		</div>
		<hr />
		<div ng-show='DATA.summary'>
			<table class="table table-borderless table-condensed" style="font-size:14px; font-weight:bold">
				<tr>
					<td align="right">TOTAL</td>
					<td width="10">:</td>
					<td align="right" width="130">{{DATA.summary.currency}} {{fn.formatNumber(DATA.summary.total_price, DATA.summary.currency)}}</td>
				</tr>
				<tr>
					<td align="right">Adult</td>
					<td width="10">:</td>
					<td align="right">{{DATA.summary.total_qty_1}}</td>
				</tr>
				<tr>
					<td align="right">Child</td>
					<td width="10">:</td>
					<td align="right">{{DATA.summary.total_qty_2}}</td>
				</tr>
				<tr>
					<td align="right">Infant</td>
					<td width="10">:</td>
					<td align="right">{{DATA.summary.total_qty_3}}</td>
				</tr>
			</table>
		</div>
		
		<hr />
		<a href="<?=site_url("export_to_excel/report_revenue")?>?s[date]={{DATA.search.start_date}}&s[booking_source]={{DATA.search.booking_source}}" target="_blank"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export To Excel</a>
		&nbsp; | &nbsp;
		<a href="<?=site_url("export_to_excel/report_revenue")?>?s[date]={{DATA.search.start_date}}&s[booking_source]={{DATA.search.booking_source}}" target="_blank"><i class="fa fa-print" aria-hidden="true"></i> Print</a>
		
	</div>
</div>
<style>
	.header td{vertical-align:middle !important}
</style>
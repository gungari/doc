<div ng-init="printReportRevenue()">

	<div class="no-print text-center" style="text-align:center !important">
		
		<br /><br />
	</div>
	
	
	<br />
	<div ng-show='show_loading_DATA'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		</div>
	<div class="normal-paper"  ng-show="month_data && DATA.sales">
		
		
		
		<div style="margin-top: 10px;">
			<div class="header" style="margin: 0 auto; width:700px;height: auto;">
				<div class="pull-right text-center" align="center">
					<div style="font-size:14px" class="title"><strong><?=$vendor["business_name"]?></strong></div>
					<div style="font-size:12px">Revenue</div>

				</div>
				<div class="title">Revenue Report {{month}} - {{year}}</div>

			</div>
			<table class="table table-bordered report-margin" style="font-size:9px">
				<tr class="header">
					<th>Activity Date</th>
					<th>Fund Source</th>
					<th class="text-right">Revenue</th>
					<th>Total</th>
				</tr>
				
				<tbody ng-repeat="data in DATA.sales" style="border-top: 1px !important;">
					<tr>
						<td rowspan="3" ng-show="search.view!='year'"><strong>{{fn.formatDate(data.date, "dd MM yy")}}</strong></td>
						<td rowspan="3" ng-show="search.view=='year'"><strong>{{fn.formatDate(data.date, "MM yy")}}</strong></td>
						<!-- <td rowspan="3">{{data.sources.source}}</td> -->
						<td ng-class="{'tr-muted':(data.sources.OFFLINE.total==0)}"><strong>{{data.sources.OFFLINE.name}}</strong></td>
						
						<td class="text-right" ng-class="{'tr-muted':(data.sources.OFFLINE.total==0)}">{{data.sources.currency}} {{fn.formatNumber(data.sources.OFFLINE.total, data.sources.currency)}}</td>
						<td ng-class="{'tr-muted':(data.sources.total==0)}" rowspan="3" class="text-right" style="vertical-align: middle">{{data.sources.currency}} {{fn.formatNumber(data.sources.total, data.sources.currency)}}</td>

					</tr>
					<tr>
						<td ng-class="{'tr-muted':(data.sources.AGENT.total==0)}"><strong>{{data.sources.AGENT.name}}</td>
						<td class="text-right" ng-class="{'tr-muted':(data.sources.AGENT.total==0)}">{{data.sources.currency}} {{fn.formatNumber(data.sources.AGENT.total, data.sources.currency)}}</td>
					</tr>
					<tr>
						<td ng-class="{'tr-muted':(data.sources.ONLINE.total==0)}"><strong>{{data.sources.ONLINE.name}}</td>
						<td class="text-right" ng-class="{'tr-muted':(data.sources.ONLINE.total==0)}">{{data.sources.currency}} {{fn.formatNumber(data.sources.ONLINE.total, data.sources.currency)}}</td>
					</tr>
					
				</tbody>
				<tr class="header">
					<td class="text-right" colspan="3" align="right"><strong>Total</strong></td>
					<td class="text-right"><strong>{{currency}} {{fn.formatNumber(total, currency)}}</strong></td>
				</tr>
			</table>
			<hr />
			<div style="margin-top: 10px;" ng-show="data_summary">
				<table class="table table-borderless table-condensed" style="font-size:12px; font-weight:bold">
					<tr ng-repeat="data in data_summary">
						<td align="right">{{data.name}}</td>
						<td width="10">:</td>
						<td align="right" width="130">{{data.currency}} {{fn.formatNumber(data.total, data.currency)}}</td>
					</tr>
					
				</table>
			</div>
		</div>
	</div>
	<div class="normal-paper landscape"  ng-show='date_data && DATA.revenue'>
		<div>
			<div class="header" style="margin: 0 auto; width:1000px;height: auto;">
				<div class="pull-right text-center" align="center">
					<div style="font-size:14px" class="title"><strong><?=$vendor["business_name"]?></strong></div>
					<div style="font-size:12px">Revenue</div>

				</div>
				<div class="title">Revenue Report {{start_date}}</div>

			</div>
			<div class="table-responsive" zzzstyle="overflow-x:scroll">
				<div style="width:1000px">
				<table class="table table-condensed table-bordered" style="font-size:9px">
					<tr class="header bold">
						<?php /*?><td rowspan="2" align="center" width="90">Order#<br />Booking Date</td><?php */?>
						<td rowspan="2" align="center" width="90">Booking#<br />Date</td>
						<td rowspan="2" align="center" width="60">Voucher#<br />Date</td>
						<td rowspan="2" width="100">Customer Name</td>
						<td rowspan="2" align="center" width="30" title="Nationality">Nat.</td>
						<?php if ($vendor['category'] == 'transport') { ?>
							<td rowspan="2" colspan="2" align="center">Trip</td>
						<?php } ?>
						<td rowspan="2" width="100">Booking Source</td>
						<td colspan="3" align="center" title="Passenger">Passenger</td>
						<td colspan="3" align="center" title="Passenger">Price</td>
						<td rowspan="2" align="center" width="50">Pickup</td>
						<td rowspan="2" align="center" width="50">Dropoff</td>
						<td rowspan="2" align="center" width="50">Other</td>
						<td rowspan="2" align="center" width="50">Sub Total</td>
						<td rowspan="2" align="center" width="50">Discount</td>
						<td rowspan="2" align="center" width="50">Fee</td>
						<td rowspan="2" align="center" width="50">TOTAL</td>
					</tr>
					<tr class="header bold">
						<td width="30" align="center"><small>Adl.</small></td>
						<td width="30" align="center"><small>Chi.</small></td>
						<td width="30" align="center"><small>Inf.</small></td>
						<td width="50" align="center"><small>Adl.</small></td>
						<td width="50" align="center"><small>Chi.</small></td>
						<td width="50" align="center"><small>Inf.</small></td>
					</tr>
					<tbody ng-repeat='revenue_data in DATA.revenue'>
						<tr style="background:#eeeeee">
							<?php if ($vendor['category'] != 'transport') { ?>

								<td colspan="18"><strong>{{revenue_data.product_name}}</strong></td>
							<?php }else{ ?>
								<td colspan="20"><strong>{{revenue_data.departure_port.name}} ({{revenue_data.departure_time}}) - {{revenue_data.arrival_port.name}} ({{revenue_data.arrival_time}})</strong></td>
							<?php } ?>
						</tr>
						
						<tr ng-repeat='revenue in revenue_data.revenue'>
							<?php if ($vendor['category'] == 'activities') { ?>
								<td align="center">
									<strong>{{revenue.booking.booking_code}}</strong>
									<br />
									<small>{{fn.formatDate(revenue.booking.transaction_date, "dd M yy")}}</small>
								</td>
							<?php }else{ ?>
								<td align="center">
									<strong>{{revenue.booking.booking_code}}</strong>
									<br />
									<small>{{fn.formatDate(revenue.booking.transaction_date, "dd M yy")}}</small>
								</td>
							<?php } ?>
							
							<td align="center">
								
									<strong>{{revenue.detail.voucher_code}}</strong>
								<br />
								<small>{{fn.formatDate(revenue.detail.date, "dd M yy")}}</small>
							</td>
							<td>{{revenue.booking.customer.full_name}}</td>
							<td align="center" class="text-capitalize" title="{{revenue.booking.customer.country_name}}">{{revenue.booking.customer.country_code}}</td>
							<?php if ($vendor['category'] == 'transport') { ?>
								<td width="40" align="center">
									{{revenue.detail.departure.port.port_code}} <br /><small>{{revenue.detail.departure.time}}</small>
								</td>
								<td width="40" align="center">
									{{revenue.detail.arrival.port.port_code}} <br /><small>{{revenue.detail.arrival.time}}</small>
								</td>
							<?php } ?>
							<td>
								<div ng-show='!revenue.booking.agent' class="text-capitalize">{{revenue.booking.source.toLowerCase()}}</div>
								<div ng-show='revenue.booking.agent'>{{revenue.booking.agent.name}}</div>
							</td>
							<td align="center">{{(revenue.detail.qty_1>0?revenue.detail.qty_1:'-')}}</td>
							<td align="center">{{(revenue.detail.qty_2>0?revenue.detail.qty_2:'-')}}</td>
							<td align="center">{{(revenue.detail.qty_3>0?revenue.detail.qty_3:'-')}}</td>
							
							<td align="right">{{fn.formatNumber((revenue.detail.rates.rates_1), revenue.detail.rates.currency)}}</td>
							<td align="right">{{fn.formatNumber((revenue.detail.rates.rates_2), revenue.detail.rates.currency)}}</td>
							<td align="right">{{fn.formatNumber((revenue.detail.rates.rates_3), revenue.detail.rates.currency)}}</td>
							
							<td align="right">{{fn.formatNumber(revenue.detail.pickup_price, revenue.detail.rates.currency)}}</td>
							<td align="right">{{fn.formatNumber(revenue.detail.dropoff_price, revenue.detail.rates.currency)}}</td>
							<td align="right">{{fn.formatNumber(revenue.detail.additional_charge_price, revenue.detail.rates.currency)}}</td>
							<td align="right">{{fn.formatNumber(revenue.detail.subtotal, revenue.detail.rates.currency)}}</td>
							<td align="right">{{fn.formatNumber(revenue.detail.discount, revenue.detail.rates.currency)}}</td>
							<td align="right">{{fn.formatNumber(revenue.detail.hybrid_fee, revenue.detail.rates.currency)}}</td>
							
							<td align="right">{{fn.formatNumber(revenue.detail.total, revenue.detail.rates.currency)}}</td>
						</tr>
						<tr style="background:#FFFFFF">
							<?php if ($vendor['category'] != 'transport') { ?>

								<td colspan="5"><strong>TOTAL</strong></td>
							<?php }else{ ?>
								<td colspan="7"><strong>TOTAL</strong></td>
							<?php } ?>
							<td align="center"><b>{{(revenue_data.summary.qty_1>0?revenue_data.summary.qty_1:'-')}}</b></td>
							<td align="center"><b>{{(revenue_data.summary.qty_2>0?revenue_data.summary.qty_2:'-')}}</b></td>
							<td align="center"><b>{{(revenue_data.summary.qty_3>0?revenue_data.summary.qty_3:'-')}}</b></td>
							
							<td align="right"><b>{{fn.formatNumber((revenue_data.summary.rates_1), revenue_data.summary.currency)}}</b></td>
							<td align="right"><b>{{fn.formatNumber((revenue_data.summary.rates_2), revenue_data.summary.currency)}}</b></td>
							<td align="right"><b>{{fn.formatNumber((revenue_data.summary.rates_3), revenue_data.summary.currency)}}</b></td>
							
							<td align="right"><b>{{fn.formatNumber(revenue_data.summary.pickup_price, revenue_data.summary.currency)}}</b></td>
							<td align="right"><b>{{fn.formatNumber(revenue_data.summary.dropoff_price, revenue_data.summary.currency)}}</b></td>
							<td align="right"><b>{{fn.formatNumber(revenue_data.summary.additional_charge_price, revenue_data.summary.currency)}}</b></td>
							<td align="right"><b>{{fn.formatNumber(revenue_data.summary.subtotal, revenue_data.summary.currency)}}</b></td>
							<td align="right"><b>{{fn.formatNumber(revenue_data.summary.discount, revenue_data.summary.currency)}}</b></td>
							<td align="right"><b>{{fn.formatNumber(revenue_data.summary.hybrid_fee, revenue_data.summary.currency)}}</b></td>
							
							<td align="right"><b>{{fn.formatNumber(revenue_data.summary.fee, revenue_data.summary.currency)}}</b></td>
						</tr>
					</tbody>
				</table>
				</div>
			</div>
			<hr />
			<div ng-show='DATA.summary'>
				<table class="table table-borderless table-condensed" style="font-size:14px; font-weight:bold">
					<tr>
						<td align="right">TOTAL</td>
						<td width="10">:</td>
						<td align="right" width="130">{{DATA.summary.currency}} {{fn.formatNumber(DATA.summary.total_price, DATA.summary.currency)}}</td>
					</tr>
					<tr>
						<td align="right">Adult</td>
						<td width="10">:</td>
						<td align="right">{{DATA.summary.total_qty_1}}</td>
					</tr>
					<tr>
						<td align="right">Child</td>
						<td width="10">:</td>
						<td align="right">{{DATA.summary.total_qty_2}}</td>
					</tr>
					<tr>
						<td align="right">Infant</td>
						<td width="10">:</td>
						<td align="right">{{DATA.summary.total_qty_3}}</td>
					</tr>
				</table>
			</div>
			
			
			
		</div>
	</div>

<style type="text/css" media="print">
	
	 @media screen and (orientation:landscape) { … }
</style>
<style>


	.print_area{background:none !important; padding:0 !important;}
	.small_paper{width:8cm !important; margin:0 auto !important}
	.normal-paper{background:#FFF; padding:10px; font-size: 9px;}
	.small-paper{width:8cm; margin:auto; padding:10px; background:#FFF}
	.small-paper .header .title{font-size:18px !important}
	.landscape {width: 1030px; height: auto; margin-left: -139px;}
</style>
<?php
//pre($vendor)
?>
<style type="text/css">
	/*.ui-datepicker-calendar {
    	display: none;
    }​*/
    .ui-datepicker-year
    {
    	color: black !important;
    }
    .ui-datepicker-month
    {
    	color: black !important;
    }
   

}
</style>
<div class="sub-title">Forecast Report</div>
<div ng-init='loadReportRevenueTrans();loadReportForecast();loadTransport();'>
	<div class="products">
		<div class="product">
			<form ng-submit='loadReportForecast()'>
				<div class="table-responsive">
					<table class="table table-condensed table-borderless" width="100%">
						<tr>
							<td width="150">Month</td>
							<td width="100">Year</td>
							<td></td>
						</tr>
						<tr>
							<td>
								<select ng-model="search.month" class="form-control input-sm" style="width: 120px;">
									<option value="01">January</option>
									<option value="02">February</option>
									<option value="03">March</option>
									<option value="04">April</option>
									<option value="05">May</option>
									<option value="06">June</option>
									<option value="07">July</option>
									<option value="08">August</option>
									<option value="09">September</option>
									<option value="10">October</option>
									<option value="11">November</option>
									<option value="12">December</option>
								</select>
							</td>
							<td>
								<select ng-model="search.year" class="form-control input-sm" style="width: 70px;">
									<option ng-repeat="year in year" value="{{year}}">{{year}}</option>
								</select>
							</td>
							
							<td>
								<button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button>
							</td>
						</tr>
					</table>
				</div>
			</form>

		</div>
	</div>
	<div class="products">
	<div ng-show='show_loading_DATA'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	<div ng-show='!show_loading_DATA' class="row" style="margin: 0 !important;">
		<div class="col-md-2" style="margin:0 !important;padding:0 !important;">
			<table class="table table-striped table-bordered" style="font-size:11px">
					
					<tr>
						<td><strong>DEPART FROM</strong></td>
					</tr>
					<tr ng-repeat="data in report.forcast.report">
						<td><span style="padding-left: 5px;"><strong>{{data.code_gabung}}</strong></span></td>
					</tr>
					<tr>
						<td><strong>TOTAL</strong></td>
					</tr>
					<!-- <tr>
						<td><strong>TTL SEAT</strong></td>
					</tr>
					<tr>
						<td><strong>TTL LEFT</strong></td>
					</tr> -->
				</table>
				
		</div>
		<div class="table-responsive col-md-10" zzzstyle="overflow-x:scroll" style="margin:0 !important;padding:0 !important;">
			<div style="width:1200px">
				<table class="table table-striped table-bordered" style="font-size:11px">
					<tr>
						<th style="text-align: center;" ng-repeat="date in report.forcast.date">{{fn.formatDate(date, "dd")}}</th>
						<th>TOTAL</th>
					</tr>
					
					<tr ng-repeat="data in report.forcast.report">
						
						<td style="text-align: center;" ng-repeat="dates in data.dates">{{dates}}</td>
						<td style="text-align: center;">{{data.total}}</td>
					</tr>
					<tr>
						
						<td style="text-align: center;" ng-repeat="total in report.total"><strong>{{total}}</strong></td>
						<td style="text-align: center;">{{total_forcst}}</td>
					</tr>
					<!-- <tr>
						
						<td style="text-align: center;" colspan="{{report.forcast.date.length}}"><strong>{{report.forcast.boat.capacity}}</strong>
						</td>
						<td style="text-align: center;"><strong>{{report.forcast.boat.capacity * (report.forcast.date.length)}}</strong></td>
					</tr> -->
					<!-- <tr>
						<td><strong>TTL USED</strong></td>
						<td ng-repeat="total in report.forcast.total"><strong>{{total}}</strong></td>
						
					</tr> -->
					<!-- <tr>
						
						<td ng-repeat="total in report.total"><strong>{{report.forcast.boat.capacity - total}}
							
						</strong></td>
						<td style="text-align: center;"><strong>{{report.forcast.boat.capacity * (report.forcast.date.length) - total_forcst}}</strong></td>
					</tr> -->
				</table>
			</div>
		</div>
				
			</div>
			
	</div>
	</div>

	<hr />
	<div ng-repeat="data in report.forcast.report" class="row">
			<div class="col-md-2"><strong>{{data.code_gabung}}</strong></div><div class="col-md-6">: {{data.departure + ' - ' + data.arrival }}</div>
	</div>

	<hr />
	<a href="<?=site_url("export_to_excel/report_forecast")?>?s[month]={{search.month}}&s[year]={{search.year}}" target="_blank"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export To Excel</a>
	&nbsp; | &nbsp;
	<!-- <a href="<?=site_url("home/print_page/#/print/report_forecast/")?>{{search.month}}/{{search.year}}" target="_blank" >
		<i class="fa fa-print" aria-hidden="true"></i> Print
	</a> -->
	
	</div>
</div>
<style>
	.header td{vertical-align:middle !important}
</style>
<style type="text/css">
	/*.ui-datepicker-calendar {
    	display: none;
    }​*/
    .ui-datepicker-year
    {
    	color: black !important;
    }
    .ui-datepicker-month
    {
    	color: black !important;
    }
}
</style>
<div class="sub-title">Forecast Report</div>
<div ng-init='loadTransport();loadReportRevenueTrans();loadReportForecast();'>
	<div class="products">
		<div class="product">
			<form ng-submit='loadReportForecast()'>
				<div class="table-responsive">
					<table class="table table-condensed table-borderless" width="100%">
						<tr>
							<td width="150">Month</td>
							<td width="100">Year</td>
							<td width="200">Boats</td>
							<td></td>
						</tr>
						<tr>
							<td>
								<select ng-model="search.month" class="form-control input-sm" style="width: 120px;">
									<option value="01">January</option>
									<option value="02">February</option>
									<option value="03">March</option>
									<option value="04">April</option>
									<option value="05">May</option>
									<option value="06">June</option>
									<option value="07">July</option>
									<option value="08">August</option>
									<option value="09">September</option>
									<option value="10">October</option>
									<option value="11">November</option>
									<option value="12">December</option>
								</select>
							</td>
							<td>
								<select ng-model="search.year" class="form-control input-sm" style="width: 70px;">
									<option ng-repeat="year in year" value="{{year}}">{{year}}</option>
								</select>
							</td>
							<td>
								<select class="form-control input-sm" ng-model='search.boat_id' style="width: auto;">
									<option value="">All Boats</option>
									<option value="{{boat.id}}" ng-repeat='boat in DATA.boats.boats'>{{boat.name}}</option>
								</select>
							</td>
							<td>
								<button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button>
							</td>
						</tr>
						
					</table>
				</div>
			</form>

		</div>
	</div>
	<div class="products">
	<div ng-show='show_loading_DATA'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	<div ng-show='not_found' class="alert alert-warning"><em>Data not found...</em></div>
	<div ng-show='!show_loading_DATA && !not_found' class="row" style="margin: 0 !important;">
		<!-- <div class="col-xs-2" style="margin:0 !important;padding:0 !important;">
			<table class="table table-striped table-bordered" style="font-size:11px">	
				<tr style="height:33px">
					<td><strong>DEPART FROM</strong></td>
				</tr>
				<tr style="height:33px" ng-repeat="data in report.forcast.report">
					<td><span style="padding-left: 5px;"><strong>{{data.code_gabung}}</strong></span></td>
				</tr>
				<tr style="height:33px">
					<td><strong>TOTAL</strong></td>
				</tr>	
			</table>
		</div> -->
		<div class="table-responsive col-md-18" zzzstyle="overflow-x:scroll" style="margin:0 !important;padding:0 !important;">
			<div style="width:1200px">
				<table class="table table-bordered" style="font-size:11px">
					<tr>
						<td></td>
						<td  ng-repeat="date in DATA.forcast.date">{{fn.formatDate(date, "dd")}}</td>
					</tr>

					<tbody ng-repeat="(index, data) in DATA.forcast.report">
						<tr style="background:#FAFAFA">

							<td colspan="{{DATA.forcast.date.length + 1}}"><b>{{data.port_name}}  ({{index}})</b></td>
						</tr>
						
						<tr ng-show="data" ng-repeat="(index2,forcast) in data.data">
							<td><span style="margin-left: 5px;">{{index2}}</span></td>
							<td ng-repeat="date in forcast.dates">{{date}}</td>
							<tr>
								<td><span style="margin-left: 5px;"><b>Total</b></span></td>
								<td ng-repeat="total in data.total"><b>{{total}}</b></td>
							</tr>
						</tr>
					</tbody>
				
					
				</table>
			</div>
		</div>	
				
	</div>
			
	</div>
	</div>

<!-- 	<hr />
	<div ng-repeat="data in DATA.forcast.report">
		<div ng-repeat="arr in data.data"  class="row">
			<div  class="col-xs-2" ><strong>{{arr.code_departure}} - {{arr.code_arrival}}</strong></div>
			<div  class="col-xs-6" ><strong>: {{arr.port_name_departure.port_name + ' - ' + arr.port_name_arrival.port_name }}</strong></div>
		</div>
		<hr />
	</div> -->

<!-- 	<hr />
	<a href="<?=site_url("home/print_page/#/print/report_forecast/")?>{{search.month}}/{{search.year}}/{{search.boat_id}}" target="_blank" >
		<i class="fa fa-print" aria-hidden="true"></i> Print
	</a>&nbsp; | &nbsp;
	<a href="<?=site_url("export_to_excel/report_forecast")?>?s[month]={{search.month}}&s[year]={{search.year}}&s[boat_id]={{search.boat_id}}" target="_blank"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export To Excel</a>
	
	 -->
			
	
	</div>
</div>
<style>
	.header td{vertical-align:middle !important}
</style>
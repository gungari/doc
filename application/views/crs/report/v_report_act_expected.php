<div class="sub-title">Expected Arrival Report</div>
<div class="products" ng-controller="report_expected_controller" ng-init="loadReportActExpected()">
		<div class="product">
			<form class="form-inline" id="formFilter" ng-submit="loadReportActExpectedData()" style="margin-bottom: 10px;">
				<div class="form-group">
					<label class="sr-only" for="view">View:</label>
					<select name="view" class="form-control input-sm" ng-model="search.view">
						<?php foreach(array("date"=>"Date", "month"=>"Month", "year"=>"Year") as $key => $value) : ?>
							<?php if($key==$view) : ?>
								<option value="<?=$key?>" selected><?=$value?></option>
							<?php else : ?>
								<option value="<?=$key?>"><?=$value?></option>
							<?php endif; ?>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="form-group" ng-show="search.view == 'date'">
					<input style="width:  100px;" type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='search.start_date' />
				</div>
				<div class="form-group" ng-show="search.view == 'date'">
					<input style="width:  100px;" type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='search.end_date' />
				</div>
				<div class="form-group" ng-show="search.view == 'month'">
					<label for="month" class="sr-only">Month:</label>
					<select name="month" class="form-control input-sm" ng-model="search.month">
						<?php foreach(array("1"=>"January","2"=>"February","3"=>"March","4"=>"April","5"=>"May","6"=>"June","7"=>"July","8"=>"August","9"=>"September","10"=>"October","11"=>"November","12"=>"December") as $key => $value) : ?>
							<?php if($month==$key) : ?>
								<option value="<?=$key?>" selected><?=$value?></option>
							<?php else : ?>
								<option value="<?=$key?>"><?=$value?></option>
							<?php endif; ?>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="form-group" ng-show="search.view == 'year' || search.view == 'month'">
					<label for="year" class="sr-only">Year:</label>
					<select name="year" class="form-control input-sm" ng-model="search.year">
						<?php for($i=date("Y") + 1;$i>=2014;$i--) : ?>
							<?php if($year==$i) : ?>
								<option value="<?=$i?>" selected><?=$i?></option>
							<?php else : ?>
								<option value="<?=$i?>"><?=$i?></option>
							<?php endif; ?>
						<?php endfor; ?>
					</select>
				</div>
				<!-- <div class="form-group">
					<span>&nbsp;&nbsp;&nbsp;<label><input style="margin: 0 !important;" type="checkbox" class="form-control input-sm" name="" ng-model="trips.isChecked" ng-change="trip_change(trips)" >&nbsp;&nbsp;Paid<label>&nbsp;&nbsp;</span>
				</div> -->
				<div class="form-group">
					<select class="form-control input-sm" ng-model='search.booking_source'>
						<option value="">All</option>
						<option value="{{booking_source.code}}" ng-repeat='booking_source in DATA_booking_source'>{{booking_source.name}}</option>
					</select>
				</div>
				<button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-menu-right"></span></button>
				<br />
				
				
		</form>
	</div>
  	<div class="text-center" >
  		<span ng-repeat="date in DATA.date" style="margin-right: 5px;"><a ng-click="getDateActExpected(date)"><input  type="button" class="btn btn-primary btn-xs btn-month" value="{{fn.formatDate(date, 'M yy')}}" style="font-size:0.9em;"></a></span>
  	</div>
	<hr />

	<div ng-show="!DATA.expected.data" class="alert alert-warning report-margin" id="tableBooking" role="alert">
			No data
	</div>
	<br />
	<div ng-show='is_loading'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div> 
	<div>
		<table class="table table-bordered report-margin" style="font-size:12px" ng-repeat="data in DATA.expected.data">
				
				<tr class="header">
					<td colspan="7"><span><b>{{data.name}}</b></span></td>
				</tr>
				<tr class="header">
					<td align="center" rowspan="2" width="20"><b>No.</b></td>
					<td align="center"><b>Voucher #</b></td>
					<td align="center"><b>Order #</b></td>
					<td align="center" colspan="4"><b>Customer</b></td>
				</tr>
				<tr class="header">
					
					<td align="center" width="150"><b>Arrival Date</b></td>
					<td align="center" width="150"><b>Booking Date</b></td>
					<td align="center"><b>Name</b></td>
					<td align="center" width="25"><b>Adult</b></td>
					<td align="center" width="25"><b>Child</b></td>
					<td align="center" width="25"><b>Infant</b></td>
				</tr>
				
				<tr ng-repeat="row in data.detail">
					<td align="center">{{$index+1}}</td>
					<td align="center">
						<span><b>{{row.voucher_code}}</b></span>
						<br />
						<span><small>{{fn.formatDate(row.arrival_date, "dd MM yy")}}<small></span>
					</td>
					<td align="center">
						<span><a ui-sref="reservation.detail({'booking_code':row.booking_code})"><b>{{row.booking_code}}</b></a></span>
						<br />
						<span><small>{{fn.formatDate(row.booking_date, "dd MM yy")}}</small></span>
					</td>
					<td align="center">
						<span><b>{{row.first_name}} {{row.last_name}} <span ng-show="row.booking_status == 'DEFINITE'"><strong>(DEF)</strong></span><span ng-show="row.booking_status == 'COMPLIMEN'"><strong>(COM)</strong></span></b></span><br />
						<span ng-hide="!row.email || row.email == '-'"><small>{{row.email}}</small><br /></span>
						<div ng-show="row.booking_source!='AGENT'"><small>{{row.booking_source}}</small></div>
						<div ng-show="row.booking_source=='AGENT'"><small>{{row.agent_name}}</small></div>
						<span><small>{{row.country}}</small></span>
					</td>
					<td align="center">{{row.qty1}}</td>
					<td align="center">{{row.qty2}}</td>
					<td align="center">{{row.qty3}}</td>
				</tr>
				<tr>
					<td colspan="4" align="right"><b>Total</b></td>
					<td align="center"><b>{{data.total_qty1}}</b></td>
					<td align="center"><b>{{data.total_qty2}}</b></td>
					<td align="center"><b>{{data.total_qty3}}</b></td>
				</tr>
		</table>
	</div>
	<div ng-show="DATA.expected.data" class="text-right" style="font-size: 14px; margin-top:50px;">
		<table class="table table-condensed table-borderless">
			<tr>
				<td><strong>Total Adult : </strong></td>
				<td width="80"><strong>{{DATA.expected.grand_total1}}</strong></td>
			</tr>
			<tr>
				<td><strong>Total Child : </strong></td>
				<td><strong>{{DATA.expected.grand_total2}}</strong></td>
			</tr>
			<tr>
				<td><strong>Total Infant : </strong></td>
				<td><strong>{{DATA.expected.grand_total3}}</strong></td>
			</tr>
		</table>
	</div>
	<a href="<?=site_url("export_to_excel/report_act_expected")?>?
		s[end_date]={{search.end_date}}&
		s[start_date]={{search.start_date}}&
		s[year]={{search.year}}&
		s[source]={{search.source}}&
		s[month]={{search.month}}&
		s[view]={{search.view}}
		" target="_blank">
			<i class="fa fa-file-excel-o" aria-hidden="true"></i> Export To Excel
	</a>
	&nbsp; | &nbsp;


		<span ng-hide="printDateExpected"><a href="<?=site_url("home/print_page/#/print/report_act_expected/")?>{{search.start_date}}/{{search.booking_source}}/{{search.month}}/{{search.year}}/{{search.view}}/{{search.end_date}}" target="_blank" >
			<i class="fa fa-print" aria-hidden="true"></i> Print 
		</a></span>
		<span ng-show="printDateExpected"><a href="<?=site_url("home/print_page/#/print/report_act_dateexpected/")?>{{search.date}}/{{search.paid}}" target="_blank" >
			<i class="fa fa-print" aria-hidden="true"></i> Print
		</a></span>
	</a>
</div>
<div ng-init="printReportSales()">
	<div class="no-print text-center" style="text-align:center !important">
		<br /><br />
	</div>
		<br />

		<div class="normal-paper">
		<div class="header">
			<div class="pull-right text-center" align="center">
				<div style="font-size:14px" class="title"><strong><?=$vendor["business_name"]?></strong></div>
				<div style="font-size:12px">SALES</div>

			</div>
			<div class="title">Report
				<span ng-show="view == 'month'">{{month}} - {{year}}</span>
				<span ng-show="view == 'year'">{{year}}</span>
				<span ng-show="view == 'date'">{{date}} - {{month}} - {{year}}</span>
			</div>

		</div>
		
			<div ng-show='!report.sales'>
					<img src="<?=base_url("public/images/loading_bar.gif")?>" />
			</div> 

			<div style="margin-top: 10px;" ng-show="report.sales && !date_data">
				<table class="table table-bordered report-margin" style="font-size:12px">
					<tr class="header">
						<th>Date</th>
						<th>Booking</th>
						<th class="text-right">Sales</th>
						<th class="text-right">Total</th>
					</tr>
					<tbody ng-repeat="data in report.sales">
						<tr>
							<td rowspan="3" ng-show="search.view!='year'"><strong><div ng-click="loadReportSalesData(data.date)">{{fn.formatDate(data.date, "dd MM yy")}}</div></strong></td>
							<td rowspan="3" ng-show="search.view=='year'"><strong><div ng-click="loadReportSalesMonth(data.date)">{{fn.formatDate(data.date, "MM yy")}}</div></strong></td>
							<td ng-class="{'tr-muted':(data.sources.OFFLINE.total==0)}"><strong>{{data.sources.OFFLINE.name}}</strong><hr /><span style="margin-right: 20px;">Booking : {{data.sources.OFFLINE.book}}</span><small>Guest(s) : {{data.sources.OFFLINE.guest}}</small></td>
							<td class="text-right" ng-class="{'tr-muted':(data.sources.OFFLINE.total==0)}">{{data.sources.currency}} {{fn.formatNumber(data.sources.OFFLINE.total, data.sources.currency)}}</td>
							<td rowspan="3" class="text-right" style="vertical-align: middle;" ng-class="{'tr-muted':(data.sources.total_book==0)}">{{data.sources.currency}} {{fn.formatNumber(data.sources.total, data.sources.currency)}}
								<hr style="margin:2px 0">
								<small>
									<span style="margin-right: 20px;">Booking(s): {{data.sources.total_book}}</span>
									<span>Guest(s): {{data.sources.total_guest}}</span>
								</small>
							</td>
						</tr>
						<tr >
							<td ng-show="search.view=='year'" ng-class="{'tr-muted':(data.sources.AGENT.total==0)}"><strong><div ng-show="data.sources.AGENT.total>0" data-toggle="modal" data-target="#checkin-popup-info" ng-click="loadAgentMonth(data.date)">{{data.sources.AGENT.name}}</div><span ng-show="data.sources.AGENT.total==0">{{data.sources.AGENT.name}}</span></strong><hr /><span style="margin-right: 20px;">Booking : {{data.sources.AGENT.book}}</span><small>Guest(s) : {{data.sources.AGENT.guest}}</small></td>
							<td ng-show="search.view!='year'" ng-class="{'tr-muted':(data.sources.AGENT.total==0)}"><strong><div ng-show="data.sources.AGENT.total>0" data-toggle="modal" data-target="#checkin-popup-info" ng-click="loadAgentDate(data.date)">{{data.sources.AGENT.name}}</div><span ng-show="data.sources.AGENT.total==0">{{data.sources.AGENT.name}}</span></strong><hr /><span style="margin-right: 20px;">Booking : {{data.sources.AGENT.book}}</span><small>Guest(s) : {{data.sources.AGENT.guest}}</small></td>
							<td class="text-right" ng-class="{'tr-muted':(data.sources.AGENT.total==0)}">{{data.sources.currency}} {{fn.formatNumber(data.sources.AGENT.total, data.sources.currency)}}</td>	
						</tr>
						<tr>
							<td ng-show="search.view=='year'"  ng-class="{'tr-muted':(data.sources.ONLINE.total==0)}"><strong>{{data.sources.ONLINE.name}}<span ng-show="data.sources.ONLINE.total==0">{{data.sources.ONLINE.name}}</span></strong><hr /><span style="margin-right: 20px;">Booking : {{data.sources.ONLINE.book}}</span><small>Guest(s) : {{data.sources.ONLINE.guest}} </small></td>
							<td ng-show="search.view!='year'"  ng-class="{'tr-muted':(data.sources.ONLINE.total==0)}"><strong>{{data.sources.ONLINE.name}}<span ng-show="data.sources.ONLINE.total==0">{{data.sources.ONLINE.name}}</span></strong><hr /><span style="margin-right: 20px;">Booking : {{data.sources.ONLINE.book}}</span><small>Guest(s) : {{data.sources.ONLINE.guest}} </small></td>
							<td class="text-right" ng-class="{'tr-muted':(data.sources.ONLINE.total==0)}">{{data.sources.currency}} {{fn.formatNumber(data.sources.ONLINE.total, data.sources.currency)}}</td>
						</tr>	
					</tbody>
					<tr class="header">
						<td rowspan="3" style="vertical-align: middle;"><strong>Summary</strong></td>
						<td>
							<strong>Offline Booking </strong><hr /><span style="margin-right: 20px;">Booking : {{total_book_offline}}</span><small>Guest(s) : {{total_guest_offline}}</small>
						</td>
						<td style="vertical-align: middle;" class="text-right" align="right"><strong>{{currency}} {{fn.formatNumber(sum_offline, currency)}}</strong></td>

						<td style="vertical-align: middle;" rowspan="3" class="text-right" align="left">
							<strong>{{currency}} {{fn.formatNumber(total_sales, currency)}}</strong>
							<hr style="margin:2px 0">
							<small>
								<span style="margin-right: 20px;"><strong>Booking(s): {{total_book}}</strong></span>
								<span><strong>Guest: {{total_guest}}</strong></span>
							</small>
						</td>
					</tr>
					<tr class="header">
						<td><strong>Agencies and Colleague</strong><hr /><span style="margin-right: 20px;">Booking : {{total_book_agent}}</span><small>Guest(s) : {{total_guest_agent}}</small></td>
						<td style="vertical-align: middle;" class="text-right" align="right"><strong>{{currency}} {{fn.formatNumber(sum_agent, currency)}}</strong></td>
					</tr>
					<tr class="header">
						<td><strong>Online Booking</strong><hr /><span style="margin-right: 20px;">Booking : {{total_book_online}}</span><small>Guest(s) :  {{total_guest_online}}</small></td>
						<td style="vertical-align: middle;" class="text-right" align="right"><strong>{{currency}} {{fn.formatNumber(sum_online, currency)}}</strong></td>
					</tr>
				</table>
			</div>
			<hr />

			<div style="margin-top: 10px;" ng-show="date_data && !show_error">
				<table class="table table-bordered report-margin" style="font-size:12px">
					<tr class="header">
						<th width="10">No</th>
						<th>Order #</th>
						<th>Customer Name</th>
						<th class="text-right">Amount</th>
						<th class="text-center">Total Trip</th>
					</tr>
					<tbody ng-repeat="data in report.sales">
						<tr>
							<td>{{$index+1}}</td>
							<td><strong>{{data.code}}</strong></td>
							<td>{{data.firs_name}} {{data.last_name}}<hr /><small>
								<span ng-show="data.qty1!=0" style="margin-left: 10px;">Adult(s): {{data.qty1}}</span>
								<span ng-show="data.qty2!=0" style="margin-left: 10px;">Child(s): {{data.qty2}}</span>
								<span ng-show="data.qty3!=0" style="margin-left: 10px;">Invant(s): {{data.qty3}}</span>
							</small></td>
							<td class="text-right">{{data.currency}} {{fn.formatNumber(data.total, data.currency)}}</td>
							<td class="text-center">{{data.book}}</td>
						</tr>
					</tbody>
					<tr class="header">
						<td class="text-right" colspan="3" align="right"><strong>Total</strong></td>
						
						<td class="text-right" align="left">
							<strong>{{currency}} {{fn.formatNumber(total_date, currency)}}</strong>
							<!-- <hr style="margin:2px 0">
							<small>
								<span style="margin-right: 20px;"><strong>Booking(s): {{total_book}}</strong></span>
								<span><strong>Guest(s): {{total_guest}}</strong></span>
							</small> -->
						</td>
						<td class="text-center"><strong>{{total_trip}}</strong></td>
					</tr>
				</table>
			</div>

		<!-- 	<div style="margin-top: 10px;" ng-show="date_data_agent && !show_error">
				<table class="table table-bordered report-margin" style="font-size:12px">
					<tr class="header">
						<th width="10">No</th>
						<th>Customer Name</th>
						<th class="text-right">Amount</th>
					</tr>
					<tbody ng-repeat="agent in agent.sales">
						<tr>
							<td>{{$index+1}}</td>
							<td>{{agent.agent_name}}</td>
							<td class="text-right">{{agent.currency}} {{fn.formatNumber(agent.total, agent.currency)}}</td>
						</tr>
						
					</tbody>
					<tr class="header">
							<td class="text-right" colspan="2" align="right"><strong>Total</strong></td>
							
							<td class="text-right" align="left">
								<strong>{{fn.formatNumber(total_agent)}}</strong>
							</td>
						</tr>
				</table>
			</div> -->
</div>	

<style type="text/css" media="print">
	
	 @media screen and (orientation:landscape) { … }
</style>
<style>


	.print_area{background:none !important; padding:0 !important;}
	.small_paper{width:8cm !important; margin:0 auto !important}
	.normal-paper{background:#FFF; padding:10px; font-size: 9px;}
	.small-paper{width:8cm; margin:auto; padding:10px; background:#FFF}
	.small-paper .header .title{font-size:18px !important}
	.landscape {width: 1030px; height: auto; margin-left: -139px;}
</style>
<?php
//pre($vendor)
?>

<style type="text/css">
	hr {
		margin: 0 !important;
		padding: 0 !important;
	}
</style>
<style type="text/css">
	.tr-muted {
		color: #ccc;
	}
	ul li.title {
		margin-top: 10px;
	}
	ul li.title a {
		padding: 5px;
		font-weight: bold;
	}
	a.muted-link {
		text-decoration: inherit;
		color: inherit;
	}
	a.muted-link:hover {
		text-decoration: inherit;
		color: #1B77C0;
	}
	.report-content table th, .report-content table td {
		vertical-align: middle !important;
	}
	.report-content .report-margin {
		margin:10px auto;
	}
	.report-content .report-border {
		border: #ccc solid 0.5px;;
	}
	.report-content table .header, .report-content table .footer {
		background-color: #eee;
		font-weight: bold;
	}
	.text-middle{
		vertical-align: middle;
	}
	[ng-click],
	[data-ng-click],
	[x-ng-click] {
	    cursor: pointer;
	}
</style>
<div class="sub-title">MOST ACTIVE AGENCIES REPORT</div>
<div class="products" ng-controller="report_revenue_controller" ng-init="loadReportAgent()">
		<div class="product">
			<form class="form-inline" id="formFilter" ng-submit="loadReportAgentData()" style="margin-bottom: 10px;">
				<div class="form-group">
					<label class="sr-only" for="view">View:</label>
					<select name="view" class="form-control input-sm" ng-model="search.view">
						<?php foreach(array("date"=>"Date", "month"=>"Month", "year"=>"Year") as $key => $value) : ?>
							<?php if($key==$view) : ?>
								<option value="<?=$key?>" selected><?=$value?></option>
							<?php else : ?>
								<option value="<?=$key?>"><?=$value?></option>
							<?php endif; ?>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="form-group" ng-show="search.view == 'date'">
					<input style="width:  100px;" type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='search.start_date' />
				</div>
				<div class="form-group" ng-show="search.view == 'date'">
					<input style="width:  100px;" type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='search.end_date' />
				</div>
				<div class="form-group" ng-show="search.view == 'month'">
					<label for="month" class="sr-only">Month:</label>
					<select name="month" class="form-control input-sm" ng-model="search.month">
						<?php foreach(array("1"=>"January","2"=>"February","3"=>"March","4"=>"April","5"=>"May","6"=>"June","7"=>"July","8"=>"August","9"=>"September","10"=>"October","11"=>"November","12"=>"December") as $key => $value) : ?>
							<?php if($month==$key) : ?>
								<option value="<?=$key?>" selected><?=$value?></option>
							<?php else : ?>
								<option value="<?=$key?>"><?=$value?></option>
							<?php endif; ?>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="form-group" ng-show="search.view == 'year' || search.view == 'month'">
					<label for="year" class="sr-only">Year:</label>
					<select name="year" class="form-control input-sm" ng-model="search.year">
						<?php for($i=date("Y") + 1;$i>=2014;$i--) : ?>
							<?php if($year==$i) : ?>
								<option value="<?=$i?>" selected><?=$i?></option>
							<?php else : ?>
								<option value="<?=$i?>"><?=$i?></option>
							<?php endif; ?>
						<?php endfor; ?>
					</select>
				</div>
				<button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-menu-right"></span></button>
		
		</form>
	</div>
  	<div ng-show="show_error" class="alert alert-warning report-margin" id="tableBooking" role="alert">
		No data agent
	</div>
	<hr />

	
	
<!-- <div>
    Onions: <input type="number" ng-model="onions[1].v" />
</div> -->
	

	<div class="panel-body panel-chart">
		<div ng-show='show_loading_DATA'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		</div> 
		<div class="report-content" style="display: block;">
			<div class="report-margin report-border">
				<div id="agentLine" style="width:100%; height:350px; margin: 0px auto;"></div>
			</div>
		</div>
	</div>
	
<!-- 
<iframe src=""<?=base_url("public/js/partials/generic.js")?>"" style="width:90%; height: 600px; margin: 1%; border: 1px solid black;" frameborder="0"></iframe> -->
	<div ng-show='show_loading_DATA'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div> 
	<div style="margin-top: 10px;">
		<table class="table table-bordered report-margin" style="font-size:12px" ng-show="!show_error && agent">
			<tr class="header">
				<th width="10" class="text-middle">No.</th>
				<th>Agent</th>
				<th class="text-right">Total</th>
				
			</tr>
			<tbody ng-repeat="data in agent">
				<tr >
					<td class="text-middle">{{$index+1}}</td>
					<td><strong><div ng-click="loadReportSalesMonth(data.date)">{{data.agent_name}}</div></strong></td>
					<td class="text-right" ng-class="{'tr-muted':(data.sources.OFFLINE.total==0)}"><strong>{{data.currency}} {{fn.formatNumber(data.total, data.currency)}}</strong><hr /><span style="margin-right: 20px;">Booking : {{data.book}}</span><small>Guest(s) : {{data.guest}}</small><br />
						<span style="color: red;" ng-show="data.cancel > 0"><small>Cancel : {{data.cancel}} pax</small></span>
					</td>
				</tr>
			</tbody>
			<tr class="header">
				<td style="vertical-align: middle;" colspan="2" class="text-right"><strong>Total</strong></td>
				<td style="vertical-align: middle;" class="text-right" align="left">
					<strong>{{currency}} {{fn.formatNumber(totalagent, currency)}}</strong>
					<hr style="margin:2px 0">
					<small>
						<span style="margin-right: 20px;"><strong>Booking(s): {{total_book}}</strong></span>
						<span><strong>Guest: {{total_guest}}</strong></span><br />
						<span style="color: red;" ng-show="total_cancel_agent > 0"><small>Cancel : {{total_cancel_agent}} pax</small></span>
					</small>
				</td>
			
			</tr>

		</table>
	</div>
	<br />
	<a href="<?=site_url("export_to_excel/agent")?>?
		s[year]={{search.year}}&
		s[start_date]={{search.start_date}}&
		s[finish_date]={{search.end_date}}&
		s[month]={{search.month}}&
		s[view]={{search.view}}
		" target="_blank">
			<i class="fa fa-file-excel-o" aria-hidden="true"></i> Export To Excel
	</a>
</div>

<script type="text/javascript">
	function setDateOption(){
		var year = $('#formFilter [name="year"]').val();
		var month = $('#formFilter [name="month"]').val();
		var date = $('#formFilter [name="date"]').val();
		var last_date = new Date(year, month, 0).getDate();
		$('#formFilter [name="date"]').empty();
		for(i=1;i<=last_date;i++) {
			if(i==date) {
				$('#formFilter [name="date"]').append('<option value='+i+' selected>'+i+'</option>');
			} else {
				$('#formFilter [name="date"]').append('<option value='+i+'>'+i+'</option>');
			}
		}
	}
	function hideFormElement() {
		$("#formFilter [name='month']").hide();
		$("#formFilter [name='date']").hide();
		var view = $("#formFilter [name='view']").val(); 
		switch(view) {
			case 'year':
				// tidak ada yang disembunyikan
				break;
			case 'date':
				$("#formFilter [name='month']").show();
				$("#formFilter [name='date']").show();
				break;
			default:
				$("#formFilter [name='month']").show();
				break;
		}
	}
	$("#formFilter").change(function(){
		// hideFormElement();
		// setDateOption();
	});
	

	
</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-110782943-1','auto');
    // ga('set', 'forceSSL', true);
    ga('set', 'userId', 'USER_ID');
    ga('send', 'pageview');
</script>
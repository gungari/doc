<div ng-init="printReportCheck()">
	<div class="no-print text-center" style="text-align:center !important">
		<br /><br />
	</div>
		<br />

		<div class="normal-paper">
		<div class="header">
			<div class="pull-right text-center" align="center">
				<div style="font-size:14px" class="title"><strong><?=$vendor["business_name"]?></strong></div>
				<div style="font-size:12px">CHECK-IN</div>

			</div>
			<div class="title">Report 
				<span ng-show="view == 'month'">{{month}} - {{year}}</span>
				<span ng-show="view == 'year'">{{year}}</span>
				<span ng-show="view == 'date'">{{date}} - {{month}} - {{year}}</span>
			</div>

		</div>
		
			
			<div style="margin-top: 10px;" ng-show="report.sales && !date_data && !show_error">
				<table class="table table-bordered report-margin" style="font-size:12px">
					<tr class="header">
						<th>Date</th>
						<th>Booking</th>
						
						<th class="text-left">Total</th>
					</tr>
					
					<tbody ng-repeat="data in report.sales">

						<tr >
							<td rowspan="3" ng-show="search.view!='year'"><strong><div ng-click="loadReportCheckDate(data.date)">{{fn.formatDate(data.date, "dd MM yy")}}</div></strong></td>
							<td rowspan="3" ng-show="search.view=='year'"><strong><div ng-click="loadReportCheckMonth(data.date)">{{fn.formatDate(data.date, "MM yy")}}</div></strong></td>
							<td ng-class="{'tr-muted':(data.OFFLINE.book==0)}"><strong>{{data.OFFLINE.name}}</strong><hr /><span style="margin-right: 20px;">Booking : {{data.OFFLINE.book}}</span><span style="margin-right: 20px;"><small>Check-in / Total : {{data.OFFLINE.checkin}} / {{data.OFFLINE.guest}} pax</small></span><span style="color: red;"><small>Not Check-In : {{data.OFFLINE.not_check}} pax</small></span></td>
							
							<td rowspan="3" class="text-left" style="vertical-align: middle;" ng-class="{'tr-muted':(data.sources.total_book==0)}">
									<span style="margin-right: 20px;">Booking: {{data.total_book}}</span>
									<hr style="margin:2px 0">
									<span><small>Check-in / Total: {{data.total_checkin}} / {{data.total_guest}} pax</small></span><br />
									<span style="color: red;"><small>Not Check-In: {{data.total_not_check}} pax</small></span>						
							</td>
						</tr>
						<tr >
							<td  ng-class="{'tr-muted':(data.AGENT.book==0)}"><strong>{{data.AGENT.name}}</strong><hr /><span style="margin-right: 20px;">Booking : {{data.AGENT.book}}</span><span style="margin-right: 20px;"><small>Check-in / Total : {{data.AGENT.checkin}} / {{data.AGENT.guest}} pax</small></span>
							
									<span style="color: red;"><small>Not Check-In: {{data.AGENT.not_check}} pax</small></span>	
							</td>
							
						</tr>
						<tr>
							<td ng-class="{'tr-muted':(data.ONLINE.book==0)}"><strong>{{data.ONLINE.name}}</strong><hr /><span style="margin-right: 20px;">Booking : {{data.ONLINE.book}}</span><span style="margin-right: 20px;"><small>Check-in / Total : {{data.ONLINE.checkin}} / {{data.ONLINE.guest}} pax</small></span>
							
									<span style="color: red;"><small>Not Check-In: {{data.ONLINE.not_check}} pax</small></span>	
							</td>
							
						</tr>
						
					</tbody>
				
					<tr class="header">
						
						<td style="vertical-align: middle;text-align: right;" colspan="2"><strong>Total</strong></td>
						<td class="text-left"><strong><span style="width: 50px;">Booking </span>: {{total_book}}<br /><span style="width: 50px;"><hr /><small>Check-in / Total</span>: {{ total_checkin  }} / {{total_guest}} pax</small><br /><span style="color: red;"><small>Not Check-In : {{total_not_check}} pax</small></span></strong></td>
					</tr>
				</table>
			</div>
			<hr />

			<div style="margin-top: 10px;" ng-show="date_data && !show_error">
				<table class="table table-bordered report-margin" style="font-size:12px">
					<tr class="header">
						<th width="10">No</th>
						<th>Order #</th>
						
						<th>Customer Name</th>
						<th>Trip</th> 
						<th class="text-right">Total</th>
					</tr>
					<tbody ng-repeat="data in report.sales">
						<tr>
							<td>{{$index+1}}</td>
							<td><strong>{{data.code}}</strong></td>
							
							<td>{{data.firs_name}} {{data.last_name}}<hr /><small>
								<span ng-show="data.qty1!=0" style="margin-left: 10px;">Adult(s): {{data.check1}} / {{data.qty1}}</span>
								<span ng-show="data.qty2!=0" style="margin-left: 10px;">Child(s): {{data.check2}} / {{data.qty2}}</span>
								<span ng-show="data.qty3!=0" style="margin-left: 10px;">Invant(s): {{data.check3}} / {{data.qty3}}</span>
							</small></td>
							<td><span ng-repeat="schedule in data.schedule">{{schedule.departure}} - {{schedule.arrival}}<hr style="margin:2px 0"></span></td>
							<td class="text-right">Check-in / Total : {{data.check1 + data.check2 + data.check3}} / {{data.qty1 + data.qty2 + data.qty3}}</td>
							<!-- <td class="text-right">{{data.currency}} {{fn.formatNumber(data.total, data.currency)}}</td> -->
							<!-- <td class="text-center">{{data.book}}</td> -->
						</tr>
					</tbody>
					<tr class="header">
						<td class="text-right" colspan="4" align="right"><strong>Total</strong></td>
						
						<td class="text-right" align="left">
							<strong>Check-in / Total : {{total_check}} / {{total_date}}</strong>
							
						</td>
						
					</tr>
				</table>
			</div>
		</div>

</div>	

<style type="text/css" media="print">
	
	 @media screen and (orientation:landscape) { … }
</style>
<style>


	.print_area{background:none !important; padding:0 !important;}
	.small_paper{width:8cm !important; margin:0 auto !important}
	.normal-paper{background:#FFF; padding:10px; font-size: 9px;}
	.small-paper{width:8cm; margin:auto; padding:10px; background:#FFF}
	.small-paper .header .title{font-size:18px !important}
	.landscape {width: 1030px; height: auto; margin-left: -139px;}
</style>
<?php
//pre($vendor)
?>
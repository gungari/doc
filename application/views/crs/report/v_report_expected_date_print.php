<div ng-init="printReportExpectedDate()">

	<div class="no-print text-center" style="text-align:center !important">
		
		<br /><br />
	</div>
	<br />
	<div ng-show='show_loading_DATA'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		</div>
	<div class="normal-paper">
	<div style="margin-top: 10px;">
		<div class="header" style="margin: 0 auto; height: auto;">
			<div class="pull-right text-center" align="center">
				<div style="font-size:14px" class="title"><strong><?=$vendor["business_name"]?></strong></div>
				<div style="font-size:12px">Expected Arrival</div>

			</div>
			<div class="title">Report 
				<span >{{fn.formatDate(date, "MM yy")}}</span>
			</div>

		</div>
		<table class="table table-bordered report-margin" style="font-size:12px; margin-top: 20px;" ng-repeat="data in DATA.expected.data">
				
				<tr class="header">
					<td colspan="7"><span><b>{{data.code}}</b></span></td>
				</tr>
				<tr class="header">
					<td align="center" rowspan="2" width="20"><b>No.</b></td>
					<td align="center"><b>Voucher #</b></td>
					<td align="center"><b>Order #</b></td>
					<td align="center" colspan="4"><b>Customer</b></td>
				</tr>
				<tr class="header">
					
					<td align="center"><b>Arrival Date</b></td>
					<td align="center"><b>Booking Date</b></td>
					<td align="center"><b>Name</b></td>
					<td align="center"><b>Adult</b></td>
					<td align="center"><b>Child</b></td>
					<td align="center"><b>Infant</b></td>
				</tr>
				
				<tr ng-repeat="row in data.detail">
					<td align="center">{{$index+1}}</td>
					<td align="center">
						<span><b>{{row.voucher_code}}</b></span>
						<br />
						<span><small>{{fn.formatDate(row.arrival_date, "dd MM yy")}}<small></span>
					</td>
					<td align="center">
						<span><b>{{row.booking_code}}</b></span>
						<br />
						<span><small>{{fn.formatDate(row.booking_date, "dd MM yy")}}</small></span>
					</td>
					<td align="center">
						<span><b>{{row.first_name}} {{row.last_name}} <span ng-show="row.booking_status == 'DEFINITE'"><strong>(DEF)</strong></span><span ng-show="row.booking_status == 'COMPLIMEN'"><strong>(COM)</strong></span></b></span><br />
						<span ng-hide="row.email == ''"><small>{{row.email}}</small></span><br />
						<span><small>{{row.country}}</small></span>
					</td>
					<td align="center">{{row.qty1}}</td>
					<td align="center">{{row.qty2}}</td>
					<td align="center">{{row.qty3}}</td>
				</tr>
				<tr>
					<td colspan="4" align="right"><b>Total</b></td>
					<td align="center"><b>{{data.total_qty1}}</b></td>
					<td align="center"><b>{{data.total_qty2}}</b></td>
					<td align="center"><b>{{data.total_qty3}}</b></td>
				</tr>
		</table>
	</div>
		<div ng-show="DATA.expected.data" class="text-right" style="font-size: 14px; margin-top:50px;">
			<table class="table table-condensed table-borderless">
				<tr>
					<td><strong>Total Adult : </strong></td>
					<td width="80"><strong>{{DATA.expected.grand_total1}}</strong></td>
				</tr>
				<tr>
					<td><strong>Total Child : </strong></td>
					<td><strong>{{DATA.expected.grand_total2}}</strong></td>
				</tr>
				<tr>
					<td><strong>Total Infant : </strong></td>
					<td><strong>{{DATA.expected.grand_total3}}</strong></td>
				</tr>
			</table>
		</div>
	</div>
</div>	

<style type="text/css" media="print">
	
	 @media screen and (orientation:landscape) { … }
</style>
<style>


	.print_area{background:none !important; padding:0 !important;}
	.small_paper{width:8cm !important; margin:0 auto !important}
	.normal-paper{background:#FFF; padding:10px; font-size: 9px;}
	.small-paper{width:8cm; margin:auto; padding:10px; background:#FFF}
	.small-paper .header .title{font-size:18px !important}
	.landscape {width: 1030px; height: auto; margin-left: -139px;}
</style>
<?php
//pre($vendor)
?>
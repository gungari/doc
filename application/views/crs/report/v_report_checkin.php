
<style type="text/css">
	hr {
		margin: 0 !important;
		padding: 0 !important;
	}
</style>
<style type="text/css">
	.tr-muted {
		color: #ccc;
	}
	ul li.title {
		margin-top: 10px;
	}
	ul li.title a {
		padding: 5px;
		font-weight: bold;
	}
	a.muted-link {
		text-decoration: inherit;
		color: inherit;
	}
	a.muted-link:hover {
		text-decoration: inherit;
		color: #1B77C0;
	}
	.report-content table th, .report-content table td {
		vertical-align: middle !important;
	}
	.report-content .report-margin {
		margin:10px auto;
	}
	.report-content .report-border {
		border: #ccc solid 0.5px;;
	}
	.report-content table .header, .report-content table .footer {
		background-color: #eee;
		font-weight: bold;
	}
	.text-middle{
		vertical-align: middle;
	}
	[ng-click],
	[data-ng-click],
	[x-ng-click] {
	    cursor: pointer;
	}
</style>
<div class="sub-title">Check-In</div>
<div class="products" ng-controller="report_controller" >
		<div class="product" ng-init="loadReportCheckin();">
			<form class="form-inline" id="formFilter" ng-submit="loadCheckin()" style="margin-bottom: 10px;">
				<div class="table-responsive">
					<table class="table table-condensed table-borderless" width="100%">
						<tr>
							<td width="70">View</td>
							<td width="70" ng-show="search.view == 'date'">Date</td>
							<td width="120" ng-show="search.view == 'date' || search.view == 'month'">Month</td>
							<td width="70">Year</td>
							<?php if($vendor['category'] == 'activities'){ ?>
								<td width="120">Product</td>
							<?php } ?>	
							<td></td>
						</tr>
						<tr>
							<td>
								<select name="view" class="form-control input-sm" ng-model="search.view" style="width: 80px;">
									<?php foreach(array("date"=>"Date", "month"=>"Month", "year"=>"Year") as $key => $value) : ?>
										<?php if($key==$view) : ?>
											<option value="<?=$key?>" selected><?=$value?></option>
										<?php else : ?>
											<option value="<?=$key?>"><?=$value?></option>
										<?php endif; ?>
									<?php endforeach; ?>
								</select>
							</td>
							<td ng-show="search.view == 'date'">
								<select name="date" class="form-control input-sm"  ng-model="search.date" style="width: 70px;">
									<?php for($i=1;$i<=date('t', strtotime("$year-$month-01"));$i++) : ?>
										<?php if($date==$i) : ?>
											<option value="<?=$i?>" selected><?=$i?></option>
										<?php else : ?>
											<option value="<?=$i?>"><?=$i?></option>
										<?php endif; ?>
									<?php endfor; ?>
								</select>
							</td>
							<td ng-show="search.view == 'date' || search.view == 'month'">
								<select name="month" class="form-control input-sm" ng-model="search.month" style="width: 120px;">
									<?php foreach(array("1"=>"January","2"=>"February","3"=>"March","4"=>"April","5"=>"May","6"=>"June","7"=>"July","8"=>"August","9"=>"September","10"=>"October","11"=>"November","12"=>"December") as $key => $value) : ?>
										<?php if($month==$key) : ?>
											<option value="<?=$key?>" selected><?=$value?></option>
										<?php else : ?>
											<option value="<?=$key?>"><?=$value?></option>
										<?php endif; ?>
									<?php endforeach; ?>
								</select>
							</td>
							<td>
								<select name="year" class="form-control input-sm" ng-model="search.year" style="width: 70px;">
									<?php for($i=date("Y") + 1;$i>=2014;$i--) : ?>
										<?php if($year==$i) : ?>
											<option value="<?=$i?>" selected><?=$i?></option>
										<?php else : ?>
											<option value="<?=$i?>"><?=$i?></option>
										<?php endif; ?>
									<?php endfor; ?>
								</select>
							</td>
							<?php if($vendor['category'] == 'activities'){ ?>
							<td ng-init="loadProduct()">
								<select class="form-control input-sm" placeholder="Product Name" ng-model='search.product'>
									<option value="">All Products</option>
									<option value="{{product.product_code}}" ng-repeat='product in DATA.products'>{{product.name}}</option>
								</select>
							</td>
							<?php } ?>	
							<td>
								<button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-menu-right"></span></button>
							</td>
						</tr>
					</table>
				</div>
		
		</form>
	</div>
  
	<hr />

	<div ng-show="show_error" class="alert alert-warning report-margin" id="tableBooking" role="alert">
			No data check-in
	</div>
	<div class="panel-body panel-chart">
		<div ng-show='show_loading_DATA'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		</div> 
		<div ng-show="show_graph && !date_data && !show_error" class="report-content" style="display: block;">
			<dir class="embed-responsive embed-responsive-16by9 report-margin report-border">
				
				<div id="checkLine" style="width:100%; height:400px; margin: 0px auto;"></div>
			</dir>
		</div>
	</div>
	


	<div style="margin-top: 10px;" ng-show="report.sales && !date_data && !show_error">
		<table class="table table-bordered report-margin" style="font-size:12px">
			<tr class="header">
				<th>Date</th>
				<th>Booking</th>
				
				<th class="text-left">Total</th>
			</tr>
			
			<tbody ng-repeat="data in report.sales">

				<tr >
					<td rowspan="3" ng-show="search.view!='year'"><strong><div ng-click="loadReportCheckDate(data.date)"><a>{{fn.formatDate(data.date, "dd MM yy")}}</a></div></strong></td>
					<td rowspan="3" ng-show="search.view=='year'"><strong><div ng-click="loadReportCheckMonth(data.date)"><a>{{data.date}}</a></div></strong></td>
					<td ng-class="{'tr-muted':(data.OFFLINE.book==0)}"><strong>{{data.OFFLINE.name}}</strong><hr /><span style="margin-right: 20px;">Booking : {{data.OFFLINE.book}}</span><span style="margin-right: 20px;"><small>Check-in / Total : {{data.OFFLINE.checkin}} / {{data.OFFLINE.guest}} pax</small></span><span ng-show="data.OFFLINE.not_check != 0" style="color: red;"><small>Not Check-In : {{data.OFFLINE.not_check}} pax</small></span></td>
					
					<td rowspan="3" class="text-left" style="vertical-align: middle;" ng-class="{'tr-muted':(data.sources.total_book==0)}">
							<span style="margin-right: 20px;">Booking: {{data.total_book}}</span>
							<hr style="margin:2px 0">
							<span><small>Check-in / Total: {{data.total_checkin}} / {{data.total_guest}} pax</small></span><br />
							<span ng-show="{data.total_not_check != 0" style="color: red;"><small>Not Check-In: {{data.total_not_check}} pax</small></span>						
					</td>
				</tr>
				<tr >
					<td  ng-class="{'tr-muted':(data.AGENT.book==0)}"><strong>{{data.AGENT.name}}</strong><hr /><span style="margin-right: 20px;">Booking : {{data.AGENT.book}}</span><span style="margin-right: 20px;"><small>Check-in / Total : {{data.AGENT.checkin}} / {{data.AGENT.guest}} pax</small></span>
					
							<span ng-show="data.AGENT.not_check != 0" style="color: red;"><small>Not Check-In: {{data.AGENT.not_check}} pax</small></span>	
					</td>
					
				</tr>
				<tr>
					<td ng-class="{'tr-muted':(data.ONLINE.book==0)}"><strong>{{data.ONLINE.name}}</strong><hr /><span style="margin-right: 20px;">Booking : {{data.ONLINE.book}}</span><span style="margin-right: 20px;"><small>Check-in / Total : {{data.ONLINE.checkin}} / {{data.ONLINE.guest}} pax</small></span>
					
							<span ng-show="data.ONLINE.not_check != 0"style="color: red;"><small>Not Check-In: {{data.ONLINE.not_check}} pax</small></span>	
					</td>
					
				</tr>
				
			</tbody>
		
			<tr class="header">
				
				<td style="vertical-align: middle;text-align: right;" colspan="2"><strong>Total</strong></td>
				<td class="text-left"><strong><span style="width: 50px;">Booking </span>: {{total_book}}<br /><span style="width: 50px;"><hr /><small>Check-in / Total</span>: {{ total_checkin  }} / {{total_guest}} pax</small><br /><span style="color: red;"><small>Not Check-In : {{total_not_check}} pax</small></span></strong></td>
			</tr>
		</table>
	</div>

	<div style="margin-top: 10px;" ng-show="date_data && !show_error">
		<table class="table table-bordered report-margin" style="font-size:12px">
			<tr class="header">
				<th width="10">No</th>
				<th>Order #</th>
				<th>Customer Name</th>
				<th>Trip</th> 
				<th class="text-right">Total</th>
			</tr>
			<tbody ng-repeat="data in report.sales">
				<tr>
					<td>{{$index+1}}</td>
					<td><strong><a ui-sref="trans_reservation.detail({'booking_code':data.code})">{{data.code}}</a></strong></td>
					<td>{{data.firs_name}} {{data.last_name}}<hr /><small>
						<span ng-show="data.qty1!=0" style="margin-left: 10px;">Adult(s): {{data.check1}} / {{data.qty1}}</span>
						<span ng-show="data.qty2!=0" style="margin-left: 10px;">Child(s): {{data.check2}} / {{data.qty2}}</span>
						<span ng-show="data.qty3!=0" style="margin-left: 10px;">Invant(s): {{data.check3}} / {{data.qty3}}</span>
					</small></td>
					<td><span ng-repeat="schedule in data.schedule">{{schedule.departure}} - {{schedule.arrival}}<hr style="margin:2px 0"></span></td>
					<td class="text-right">Check-in / Total : {{data.check1 + data.check2 + data.check3}} / {{data.qty1 + data.qty2 + data.qty3}}</td>
					<!-- <td class="text-right">{{data.currency}} {{fn.formatNumber(data.total, data.currency)}}</td> -->
					<!-- <td class="text-center">{{data.book}}</td> -->
				</tr>
			</tbody>
			<tr class="header">
				<td class="text-right" colspan="4" align="right"><strong>Total</strong></td>
				
				<td class="text-right" align="left">
					<strong>Check-in / Total : {{total_check}} / {{total_date}}</strong>
					<!-- <hr style="margin:2px 0">
					<small>
						<span style="margin-right: 20px;"><strong>Booking(s): {{total_book}}</strong></span>
						<span><strong>Guest(s): {{total_guest}}</strong></span>
					</small> -->
				</td>
				<!-- <td class="text-center">{{total_trip}}</td> -->
			</tr>
		</table>
	</div>
	<br />
	<span><a href="<?=site_url("home/print_page/#/print/report_check/")?>{{search.date}}/{{search.month}}/{{search.year}}/{{search.view}}/{{search.product}}" target="_blank" >
			<i class="fa fa-print" aria-hidden="true"></i> Print 
		</a></span>&nbsp;|&nbsp;
	<span><a href="<?=site_url("export_to_excel/check")?>?
		s[year]={{search.year}}&
		s[date]={{search.date}}&
		s[month]={{search.month}}&
		s[view]={{search.view}}
		" target="_blank">
			<i class="fa fa-file-excel-o" aria-hidden="true"></i> Export To Excel
	</a></span>
</div>

<script type="text/javascript">
	function setDateOption(){
		var year = $('#formFilter [name="year"]').val();
		var month = $('#formFilter [name="month"]').val();
		var date = $('#formFilter [name="date"]').val();
		var last_date = new Date(year, month, 0).getDate();
		$('#formFilter [name="date"]').empty();
		for(i=1;i<=last_date;i++) {
			if(i==date) {
				$('#formFilter [name="date"]').append('<option value='+i+' selected>'+i+'</option>');
			} else {
				$('#formFilter [name="date"]').append('<option value='+i+'>'+i+'</option>');
			}
		}
	}
	function hideFormElement() {
		$("#formFilter [name='month']").hide();
		$("#formFilter [name='date']").hide();
		var view = $("#formFilter [name='view']").val(); 
		switch(view) {
			case 'year':
				// tidak ada yang disembunyikan
				break;
			case 'date':
				$("#formFilter [name='month']").show();
				$("#formFilter [name='date']").show();
				break;
			default:
				$("#formFilter [name='month']").show();
				break;
		}
	}
	$("#formFilter").change(function(){
		//hideFormElement();
		//setDateOption();
	});
	

	
</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-110782943-2','auto');
   // ga('set', 'forceSSL', true);
    //ga('set', 'userId', 'UA-110782943-2');
    ga('send', 'pageview');

</script>
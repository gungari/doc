<?php $this->load->view("extranet_clean/reports/v_template_header"); ?>

<div class="panel panel-primary">
	<div class="panel-heading">
		<label>Visitor ( Last <?=$limit_day?> Days )</label>
	</div>
	<div class="panel-body panel-chart">
		<div class="loading-animation" style="width:10%;margin:0 auto;"><div class="sk sk-7" color="#1B77C0" speed="1"></div></div>
		<div class="report-content" style="display:none;">
			<div class="embed-responsive embed-responsive-16by9">
				<div class="embed-responsive-item" id="chartVisitor"></div>
			</div>
			<span style="margin:0 15px;">Total : <b><span id="totalVisitor"><?=$total_visitor?></span> Visitor(s)</b></span>
			<div class="pull-right">
				<a href="<?=site_url('extranet/reports/visitor');?>" class="btn btn-default btn-sm">More Detail</a>
			</div>
		</div>
	</div>
</div>
<div class="panel panel-primary">
	<div class="panel-heading">
		<label><?=ucwords($booking_text)?> ( Last <?=$limit_day?> Days )</label>
	</div>
	<div class="panel-body panel-chart">
		<div class="loading-animation" style="width:10%;margin:0 auto;"><div class="sk sk-7" color="#1B77C0" speed="1"></div></div>
		<div class="report-content" style="display:none;">
			<div class="embed-responsive embed-responsive-16by9">
				<div class="embed-responsive-item" id="chartBooking"></div>
			</div>
			<span style="margin:0 15px;">Total : <b><span id="totalBooking"><?=$total_booking?></span> <?=ucwords($booking_text)?>(s)</b></span>
			<div class="pull-right">
				<a href="<?=site_url("extranet/reports/$booking_text");?>" class="btn btn-default btn-sm">More Detail</a>
			</div>
		</div>
	</div>
</div>
<div class="panel panel-primary">
	<div class="panel-heading">
		<label>Revenue ( Last <?=$limit_day?> Days )</label>
	</div>
	<div class="panel-body panel-chart">
		<div class="loading-animation" style="width:10%;margin:0 auto;"><div class="sk sk-7" color="#1B77C0" speed="1"></div></div>
		<div class="report-content" style="display:none;">
			<div class="embed-responsive embed-responsive-16by9">
				<div class="embed-responsive-item" id="chartRevenue"></div>\
			</div>
			<span style="margin:0 15px;">Total : <b><?=$vendor['f_currency_filter']?> <span id="totalRevenue"><?=$total_revenue?></span></b></span>
			<div class="pull-right">
				<a href="<?=site_url('extranet/reports/revenue');?>" class="btn btn-default btn-sm">More Detail</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" id="taSpinkitJS" src="<?=site_url('public/plugin/ta-spinkit/ta-spinkit.js')?>"></script>
<script type="text/javascript" src="<?=base_url()?>public/plugin/accounting.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
	// load google chart
	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(function(){
		setLineChart($.parseJSON('<?=json_encode($visitors)?>'), '#chartVisitor');
		setLineChart($.parseJSON('<?=json_encode($bookings)?>'), '#chartBooking');
		setLineChart($.parseJSON('<?=json_encode($revenues)?>'), '#chartRevenue');
	});
	function loadingAnimated(revert) {
		if(revert===true) {
			$('.report-content').show();
			$(".loading-animation").hide();
		} else {
			$('.report-content').hide();
			$(".loading-animation").show();
		}
	}
	function setLineChart(paramData, element) {
		var data = new google.visualization.DataTable();
		var dataRowsArray = [];
		var maxNumber = 10;
		data.addColumn('string', 'Dates');
		data.addColumn('number', 'Visitor');
		data.addColumn({type: 'string', role: 'tooltip'});
		for(var i=0; i<paramData.length; i++){
			var dataValue = [paramData[i]['time'], paramData[i]['value'], paramData[i]['tooltip'].replace('\\n', '\n')];
			dataRowsArray.push(dataValue);
			if(paramData[i]['value']>maxNumber)
				maxNumber = paramData[i]['value'];
		}
		data.addRows(dataRowsArray);
		var options = {
			vAxis: {format: 'decimal', minValue: 0, maxValue: maxNumber, gridlines: {count: -1}, textStyle: {fontSize: 8}},
			hAxis: {textStyle: {fontSize: 8}},
			chartArea: {width: '85%', heigth: '100%'},
			colors: ['#337AB7'],
			legend: {position: 'none'},
			areaOpacity: 0.0
		};
		loadingAnimated(true);

		var chart = new google.visualization.AreaChart($(element)[0]);
		chart.draw(data, options);
	}
	$(document).ready(function(){
		loadingAnimated();
	})
</script>
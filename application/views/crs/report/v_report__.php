<div class="pull-right">
	<div class="dropdown">
		<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			<i class="fa fa-bars" aria-hidden="true"></i> Menu
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
		<li><a ui-sref="reports.revenue">Revenue</a></li>
		<li><a ui-sref="reports.forecast">Forecast</a></li>
		<li><a ui-sref="reports.sales">Sales</a></li>
		<li><a ui-sref="reports.checkin">Check-in</a></li>
		<li><a ui-sref="reports.agent">Most Active Agencies</a></li>
		<li><a ui-sref="reports.cancell">Cancellation</a></li>
		</ul>
	</div>
</div>

<h1>Report</h1>

<div ui-view ng-controller="report_home_controller" ng-init="loadReportHome()">
	<!-- <br /><br /> -->
	<hr />
<!-- 	<strong><em>Please select report menu above,,,</em></strong> -->
		<div class="panel panel-primary">
			<div class="panel-heading">
				<label>SALES REPORT ( Last 30 Days )</label>
			</div>
			<div class="panel-body panel-chart">
				<div ng-show='show_loading_DATA'>
					<img src="<?=base_url("public/images/loading_bar.gif")?>" />
				</div> 
				<div ng-show="show_graph && !date_data && !show_error" class="report-content" style="display: block;">
					<dir class="embed-responsive embed-responsive-16by9 report-margin report-border">
						<div google-chart chart="chartObject" ng-if="chartObject" style="height:400px; width:100%;font-size: 2px"></div>
					</dir>
					<span style="margin:0 15px;">Total Sales: <b>{{currency_home}} {{fn.formatNumber(total_home, currency_home)}}</b></span>
					<div class="pull-right">
						<a ui-sref="reports.sales"  target="_blank" class="btn btn-default btn-sm">More Detail</a>
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-primary">
			<div class="panel-heading">
				<label>CHECK-IN REPORT ( Last 30 Days )</label>
			</div>
			<div class="panel-body panel-chart">
				<div ng-show='show_loading_DATA'>
					<img src="<?=base_url("public/images/loading_bar.gif")?>" />
				</div> 
				<div ng-show="show_graph && !date_data && !show_error" class="report-content" style="display: block;">
					<dir class="embed-responsive embed-responsive-16by9 report-margin report-border">
						<div google-chart chart="chartObject2" ng-if="chartObject2" style="height:400px; width:100%;font-size: 2px"></div>
					</dir>
					<span style="margin:0 15px;">Check-in / Total : <b>{{total_checkin}} /  {{total_guest}}</b></span>
					<div class="pull-right">
						<a ui-sref="reports.checkin"  target="_blank" class="btn btn-default btn-sm">More Detail</a>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<label>REVENUE REPORT ( Last 30 Days )</label>
			</div>
			<div class="panel-body panel-chart">
				<div ng-show='show_loading_DATA'>
					<img src="<?=base_url("public/images/loading_bar.gif")?>" />
				</div> 
				<div ng-show="show_graph && !date_data && !show_error" class="report-content" style="display: block;">
					<dir class="embed-responsive embed-responsive-16by9 report-margin report-border">
						<div google-chart chart="chartObject3" ng-if="chartObject3" style="height:400px; width:100%;font-size: 2px"></div>
					</dir>
					<span style="margin:0 15px;">Total Revenue: <b>{{currency}} {{fn.formatNumber(total_revenue, currency)}}</b></span>
					<div class="pull-right">
						<a ui-sref="reports.revenue"  target="_blank" class="btn btn-default btn-sm">More Detail</a>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<label>MOST ACTIVE AGENCIES REPORT ( Last 30 Days )</label>
			</div>
			<div class="panel-body panel-chart">
				<div ng-show='show_loading_DATA'>
					<img src="<?=base_url("public/images/loading_bar.gif")?>" />
				</div> 
				<div ng-show="show_graph && !date_data && !show_error" class="report-content" style="display: block;">
					<dir class="embed-responsive embed-responsive-16by9 report-margin report-border">
						<div google-chart chart="chartObject_agent" ng-if="chartObject_agent" style="height:400px; width:100%;font-size: 2px"></div>
					</dir>
					<!-- <span style="margin:0 15px;">Total Gue: <b>{{currency}} {{fn.formatNumber(total_revenue, currency)}}</b></span> -->
					<div class="pull-right">
						<a ui-sref="reports.agent" target="_blank" class="btn btn-default btn-sm">More Detail</a>
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-primary">
			<div class="panel-heading">
				<label>CANCELLATION REPORT ( Last 30 Days )</label>
			</div>
			<div class="panel-body panel-chart">
				<div ng-show='show_loading_DATA'>
					<img src="<?=base_url("public/images/loading_bar.gif")?>" />
				</div> 
				<div ng-show="show_graph && !date_data && !show_error" class="report-content" style="display: block;">
					<dir class="embed-responsive embed-responsive-16by9 report-margin report-border">
						<div google-chart chart="chartObject_cancell" ng-if="chartObject_cancell" style="height:400px; width:100%;font-size: 2px"></div>
					</dir>
					<span style="margin:0 15px;">Total Cancell: <b>{{currency_cancell}} {{fn.formatNumber(total_cancell, currency_cancell)}}</b></span>
					<div class="pull-right">
						<a ui-sref="reports.cancell" target="_blank" class="btn btn-default btn-sm">More Detail</a>
					</div>
				</div>
			</div>
		</div>
</div>

<div class="pull-right">
	<div class="dropdown">
		<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			<i class="fa fa-bars" aria-hidden="true"></i> Menu
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
		<li><a ui-sref="reports.revenue">Revenue</a></li>
		<li><a ui-sref="reports.forecast">Forecast</a></li>
		<li><a ui-sref="reports.sales">Sales</a></li>
		<li><a ui-sref="reports.checkin">Check In</a></li>
		</ul>
	</div>
</div>

<h1>Report</h1>

<div ui-view>
	<br /><br />
	<hr />
	<strong><em>Please select report menu above,,,</em></strong>
</div>

<style type="text/css">
	hr {
		margin: 0 !important;
		padding: 0 !important;
	}
</style>
<style type="text/css">
	.tr-muted {
		color: #ccc;
	}
	ul li.title {
		margin-top: 10px;
	}
	ul li.title a {
		padding: 5px;
		font-weight: bold;
	}
	a.muted-link {
		text-decoration: inherit;
		color: inherit;
	}
	a.muted-link:hover {
		text-decoration: inherit;
		color: #1B77C0;
	}
	.report-content table th, .report-content table td {
		vertical-align: middle !important;
	}
	.report-content .report-margin {
		margin:10px auto;
	}
	.report-content .report-border {
		border: #ccc solid 0.5px;;
	}
	.report-content table .header, .report-content table .footer {
		background-color: #eee;
		font-weight: bold;
	}
	.text-middle{
		vertical-align: middle;
	}
	[ng-click],
	[data-ng-click],
	[x-ng-click] {
	    cursor: pointer;
	}
</style>
<div class="sub-title">REVENUE REPORT</div>

	<div class="products" ng-init='loadReportRevenue()'>
		<div class="product">
			<form ng-submit='loadReportRevenueTrans()' class="form-inline">
				<div class="table-responsive">
				<table class="table table-condensed table-borderless" width="100%">
					<tr>
						<td width="70">View</td>
						<td width="50" ng-show="search.view == 'date'">Date</td>
						<td width="100" ng-show="search.view == 'date' || search.view == 'month'">Month</td>
						<td width="70">Year</td>
						<td width="100">Source</td>
						<!-- <?php if($vendor['category'] == 'activities'){ ?>
							<td width="100">Product</td>
						<?php } ?> -->	
						<td></td>
					</tr>
					<tr>
						<td>
							<select name="view" class="form-control input-sm" ng-model="search.view" style="width: 80px;">
								<?php foreach(array("date"=>"Date", "month"=>"Month", "year"=>"Year") as $key => $value) : ?>
									<?php if($key==$view) : ?>
										<option value="<?=$key?>" selected><?=$value?></option>
									<?php else : ?>
										<option value="<?=$key?>"><?=$value?></option>
									<?php endif; ?>
								<?php endforeach; ?>
							</select>
						</td>
						<td ng-show="search.view == 'date'">
							<select name="date" class="form-control input-sm"  ng-model="search.date" style="width: 70px;">
								<?php for($i=1;$i<=date('t', strtotime("$year-$month-01"));$i++) : ?>
									<?php if($date==$i) : ?>
										<option value="<?=$i?>" selected><?=$i?></option>
									<?php else : ?>
										<option value="<?=$i?>"><?=$i?></option>
									<?php endif; ?>
								<?php endfor; ?>
							</select>
						</td>
						<td ng-show="search.view == 'date' || search.view == 'month'">
							<select name="month" class="form-control input-sm" ng-model="search.month" style="width: 120px;">
								<?php foreach(array("1"=>"January","2"=>"February","3"=>"March","4"=>"April","5"=>"May","6"=>"June","7"=>"July","8"=>"August","9"=>"September","10"=>"October","11"=>"November","12"=>"December") as $key => $value) : ?>
									<?php if($month==$key) : ?>
										<option value="<?=$key?>" selected><?=$value?></option>
									<?php else : ?>
										<option value="<?=$key?>"><?=$value?></option>
									<?php endif; ?>
								<?php endforeach; ?>
							</select>
						</td>
						<td>
							<select name="year" class="form-control input-sm" ng-model="search.year" style="width: 70px;">
								<?php for($i=date("Y") + 1;$i>=2014;$i--) : ?>
									<?php if($year==$i) : ?>
										<option value="<?=$i?>" selected><?=$i?></option>
									<?php else : ?>
										<option value="<?=$i?>"><?=$i?></option>
									<?php endif; ?>
								<?php endfor; ?>
							</select>
						</td>
						<td>
							<select class="form-control input-sm" ng-model='search.booking_source'>
								<option value="">All</option>
								<option value="{{booking_source.code}}" ng-repeat='booking_source in $root.DATA_booking_source'>{{booking_source.name}}</option>
							</select>
						</td>
						<!-- <?php if($vendor['category'] == 'activities'){ ?>
						<td ng-init="loadProduct()">
							<select class="form-control input-sm" placeholder="Product Name" ng-model='search.product'>
								<option value="">All Products</option>
								<option value="{{product.product_code}}" ng-repeat='product in DATA.products'>{{product.name}}</option>
							</select>
						</td>
						<?php } ?>	 -->
						<td>
							<button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-menu-right"></span></button>
						</td>
					</tr>
				</table>
			</div>
				
			</form>
		</div>
	<div ng-show="show_error" class="alert alert-warning report-margin" id="tableBooking" role="alert">
			No data revenue
	</div>
	<hr />
	<div class="panel-body panel-chart" style="padding: 0 !important;" ng-show="show_graph">
		<div ng-show='show_loading_graph'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		</div>
		<div ng-show='show_error'>
			<span class="label label-danger">No Data</span>
		</div>
		<div class="report-content" style="display: block;">
			<dir class="embed-responsive embed-responsive-16by9 report-margin report-border">
				<div id="revenueLine" style="width:100%; height:400px; margin: 0px auto;"></div>
			</dir>
		</div>
	</div>
	<div ng-show='show_loading_DATA'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>

	<div style="margin-top: 10px;" ng-show="month_data && DATA.sales">
		
		<table class="table table-bordered report-margin" style="font-size:12px">
			<tr class="header">
				<th>Activity Date</th>
				<!-- <th>Source</th> -->
				<th class="text-right">Total Revenue</th>
			</tr>
			<tbody ng-repeat="data in DATA.sales" style="border-top: 1px !important;">
				<tr ng-class="{'info':(data.date==today)}">
					<td rowspan="3" ng-show="search.view!='year'"><strong><div ng-click="loadReportSalesData(data.date)"><a>{{fn.formatDate(data.date, "dd MM yy")}}</a></div></strong></td>
					<td rowspan="3" ng-show="search.view=='year'"><strong><div>{{fn.formatDate(data.date, "MM yy")}}</div></strong></td>
					
					<!-- <td ng-class="{'tr-muted':(data.sources.OFFLINE.total==0)}"><strong>{{data.sources.OFFLINE.name}}</strong></td> -->
					<td ng-class="{'tr-muted':(data.sources.total==0)}" rowspan="3" class="text-right" style="vertical-align: middle">{{data.sources.currency}} {{fn.formatNumber(data.sources.total, data.sources.currency)}}</td>
					<!-- <td class="text-right" ng-class="{'tr-muted':(data.sources.OFFLINE.total==0)}">{{data.sources.currency}} {{fn.formatNumber(data.sources.OFFLINE.total)}}</td> -->

				</tr>
				<tr>
					<!-- <td ng-class="{'tr-muted':(data.sources.AGENT.total==0)}"><strong><a>{{data.sources.AGENT.name}}</a></td>
					<td class="text-right" ng-class="{'tr-muted':(data.sources.AGENT.total==0)}">{{data.sources.currency}} {{fn.formatNumber(data.sources.AGENT.total)}}</td> -->
				</tr>
				<tr>
					<!-- <td ng-class="{'tr-muted':(data.sources.ONLINE.total==0)}"><strong><a>{{data.sources.ONLINE.name}}</a></td>
					<td class="text-right" ng-class="{'tr-muted':(data.sources.ONLINE.total==0)}">{{data.sources.currency}} {{fn.formatNumber(data.sources.ONLINE.total)}}</td> -->
				</tr>
				
			</tbody>
			<tr class="header">
				<td class="text-right" align="right"><strong>Total</strong></td>
				<td class="text-right"><strong>{{currency}} {{fn.formatNumber(total, currency)}}</strong></td>
			</tr>
		</table>
		<hr />
		<div style="margin-top: 10px;" ng-show="data_summary">
			<table class="table table-borderless table-condensed" style="font-size:12px; font-weight:bold">
				<tr ng-repeat="data in data_summary">
					<td align="right">{{data.name}}</td>
					<td width="10">:</td>
					<td align="right" width="130">{{data.currency}} {{fn.formatNumber(data.total, data.currency)}}</td>
				</tr>
				
			</table>
		</div>
	</div>
	
	<div ng-show='date_data && DATA.revenue'>
		<div class="table-responsive" zzzstyle="overflow-x:scroll">
			<div style="width:1200px">
			<table class="table table-condensed table-bordered" style="font-size:11px">
				<tr class="header bold">
					<?php /*?><td rowspan="2" align="center" width="90">Order#<br />Booking Date</td><?php */?>
					<td rowspan="2" align="center" width="90">Booking#<br />Date</td>
					<td rowspan="2" align="center" width="90">Voucher#<br />Date</td>
					<td rowspan="2">Customer Name</td>
					<td rowspan="2" align="center" width="30" title="Nationality">Nat.</td>
					<?php if ($vendor['category'] == 'transport') { ?>
						<td rowspan="2" colspan="2" align="center">Trip</td>
					<?php } ?>
					<td rowspan="2" width="170">Booking Source</td>
					<td colspan="3" align="center" title="Passenger">Passenger</td>
					<td colspan="3" align="center" title="Passenger">Price</td>
					<td rowspan="2" align="center" width="60">Pickup</td>
					<td rowspan="2" align="center" width="60">Dropoff</td>
					<td rowspan="2" align="center" width="60">Other</td>
					<td rowspan="2" align="center" width="65">Sub Total</td>
					<td rowspan="2" align="center" width="60">Discount</td>
					<td rowspan="2" align="center" width="60">Fee</td>
					<td rowspan="2" align="center" width="60">TOTAL</td>
				</tr>
				<tr class="header bold">
					<td width="30" align="center"><small>Adl.</small></td>
					<td width="30" align="center"><small>Chi.</small></td>
					<td width="30" align="center"><small>Inf.</small></td>
					<td width="60" align="center"><small>Adl.</small></td>
					<td width="60" align="center"><small>Chi.</small></td>
					<td width="60" align="center"><small>Inf.</small></td>
				</tr>
				<tbody ng-repeat='revenue_data in DATA.revenue'>

					<tr style="background:#eeeeee">
						
						<?php if ($vendor['category'] != 'transport') { ?>

							<td colspan="18"><strong>{{revenue_data.product_name}}</strong></td>
						<?php }else{ ?>
							<td colspan="20"><strong>{{revenue_data.departure_port.name}} ({{revenue_data.departure_time}}) - {{revenue_data.arrival_port.name}} ({{revenue_data.arrival_time}})</strong></td>
						<?php } ?>
						
					</tr>
					
					<tr ng-repeat='revenue in revenue_data.revenue'>
						
						<td align="center">
							<a ui-sref="reservation.detail({'booking_code':revenue.booking.booking_code})" target="_blank"><strong>{{revenue.booking.booking_code}}</strong></a>
							<br />
							<small>{{fn.formatDate(revenue.booking.transaction_date, "dd M yy")}}</small>
						</td>

						<td align="center">
							<a href="" ng-click="openPopUpWindowVoucher('<?=site_url("home/print_page/#/print/voucher/")?>', revenue.booking.booking_code, revenue.detail.voucher_code)">
								<strong>{{revenue.detail.voucher_code}}</strong>
							</a><br />
							<small>{{fn.formatDate(revenue.detail.date, "dd M yy")}}</small>
						</td>
						<td>
							{{revenue.booking.customer.full_name}}
							<br>
							<span>({{revenue.booking.status_initial}})</span>
						</td>
						<td align="center" class="text-capitalize" title="{{revenue.booking.customer.country_name}}">{{revenue.booking.customer.country_code}}</td>
						<?php if ($vendor['category'] == 'transport') { ?>
							<td width="40" align="center">
								{{revenue.detail.departure.port.port_code}} <br /><small>{{revenue.detail.departure.time}}</small>
							</td>
							<td width="40" align="center">
								{{revenue.detail.arrival.port.port_code}} <br /><small>{{revenue.detail.arrival.time}}</small>
							</td>
						<?php } ?>
						<td>
							<div ng-show='!revenue.booking.agent' class="text-capitalize">{{revenue.booking.source.toLowerCase()}}</div>
							<div ng-show='revenue.booking.agent'>{{revenue.booking.agent.name}}</div>
						</td>
						<td align="center">{{(revenue.detail.qty_1>0?revenue.detail.qty_1:'-')}}</td>
						<td align="center">{{(revenue.detail.qty_2>0?revenue.detail.qty_2:'-')}}</td>
						<td align="center">{{(revenue.detail.qty_3>0?revenue.detail.qty_3:'-')}}</td>
						
						<td align="right">{{fn.formatNumber((revenue.detail.rates.rates_1), revenue.detail.rates.currency)}}</td>
						<td align="right">{{fn.formatNumber((revenue.detail.rates.rates_2), revenue.detail.rates.currency)}}</td>
						<td align="right">{{fn.formatNumber((revenue.detail.rates.rates_3), revenue.detail.rates.currency)}}</td>
						
						<td align="right">{{fn.formatNumber(revenue.detail.pickup_price, revenue.detail.rates.currency)}}</td>
						<td align="right">{{fn.formatNumber(revenue.detail.dropoff_price, revenue.detail.rates.currency)}}</td>
						<td align="right">{{fn.formatNumber(revenue.detail.additional_charge_price, revenue.detail.rates.currency)}}</td>
						<td align="right">{{fn.formatNumber(revenue.detail.subtotal, revenue.detail.rates.currency)}}</td>
						<td align="right">{{fn.formatNumber(revenue.detail.discount, revenue.detail.rates.currency)}}</td>
						<td align="right">{{fn.formatNumber(revenue.detail.hybrid_fee, revenue.detail.rates.currency)}}</td>
						
						<td align="right">{{fn.formatNumber(revenue.detail.total, revenue.detail.rates.currency)}}</td>
					</tr>
					<tr style="background:#FFFFFF">
						<?php if ($vendor['category'] != 'transport') { ?>

							<td colspan="5"><strong>TOTAL</strong></td>
						<?php }else{ ?>
							<td colspan="7"><strong>TOTAL</strong></td>
						<?php } ?>
						<td align="center"><b>{{(revenue_data.summary.qty_1>0?revenue_data.summary.qty_1:'-')}}</b></td>
						<td align="center"><b>{{(revenue_data.summary.qty_2>0?revenue_data.summary.qty_2:'-')}}</b></td>
						<td align="center"><b>{{(revenue_data.summary.qty_3>0?revenue_data.summary.qty_3:'-')}}</b></td>
						
						<td align="right"><b>{{fn.formatNumber((revenue_data.summary.rates_1), revenue_data.summary.currency)}}</b></td>
						<td align="right"><b>{{fn.formatNumber((revenue_data.summary.rates_2), revenue_data.summary.currency)}}</b></td>
						<td align="right"><b>{{fn.formatNumber((revenue_data.summary.rates_3), revenue_data.summary.currency)}}</b></td>
						
						<td align="right"><b>{{fn.formatNumber(revenue_data.summary.pickup_price, revenue_data.summary.currency)}}</b></td>
						<td align="right"><b>{{fn.formatNumber(revenue_data.summary.dropoff_price, revenue_data.summary.currency)}}</b></td>
						<td align="right"><b>{{fn.formatNumber(revenue_data.summary.additional_charge_price, revenue_data.summary.currency)}}</b></td>
						<td align="right"><b>{{fn.formatNumber(revenue_data.summary.subtotal, revenue_data.summary.currency)}}</b></td>
						<td align="right"><b>{{fn.formatNumber(revenue_data.summary.discount, revenue_data.summary.currency)}}</b></td>
						<td align="right"><b>{{fn.formatNumber(revenue_data.summary.hybrid_fee, revenue_data.summary.currency)}}</b></td>
						
						<td align="right"><b>{{fn.formatNumber(revenue_data.summary.fee, revenue_data.summary.currency)}}</b></td>
					</tr>
				</tbody>
			</table>
			</div>
		</div>
		<hr />
		<div ng-show='DATA.summary'>
			<table class="table table-borderless table-condensed" style="font-size:14px; font-weight:bold">
				<tr>
					<td align="right">TOTAL</td>
					<td width="10">:</td>
					<td align="right" width="130">{{DATA.summary.currency}} {{fn.formatNumber(DATA.summary.total_price, DATA.summary.currency)}}</td>
				</tr>
				<tr>
					<td align="right">Adult</td>
					<td width="10">:</td>
					<td align="right">{{DATA.summary.total_qty_1}}</td>
				</tr>
				<tr>
					<td align="right">Child</td>
					<td width="10">:</td>
					<td align="right">{{DATA.summary.total_qty_2}}</td>
				</tr>
				<tr>
					<td align="right">Infant</td>
					<td width="10">:</td>
					<td align="right">{{DATA.summary.total_qty_3}}</td>
				</tr>
			</table>
		</div>
		
		
	</div>
	<br />
	
	<span ng-show="search.view != 'year'">
		<a href="<?=site_url("home/print_page/#/print/report_revenue/")?>{{search.date}}/{{search.booking_source}}/{{search.month}}/{{search.year}}/{{search.view}}/{{search.date}}" target="_blank" >
			<i class="fa fa-print" aria-hidden="true"></i> Print
		</a>&nbsp;|&nbsp;
	</span>
	
	<a href="<?=site_url("export_to_excel/report_revenue")?>?s[date]={{search.date}}&s[booking_source]={{search.booking_source}}&s[month]={{search.month}}&s[year]={{search.year}}&s[view]={{search.view}}&s['start_date']={{search.date}}" target="_blank"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export To Excel</a>
	
	
</div>
<style>
	.header td{vertical-align:middle !important}
</style>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-110782943-1','auto');
    ga('set', 'forceSSL', true);
    ga('set', 'userId', 'USER_ID');
    ga('send', 'pageview');
</script>
<style type="text/css">
	/*.ui-datepicker-calendar {
    	display: none;
    }​*/
    .ui-datepicker-year
    {
    	color: black !important;
    }
    .ui-datepicker-month
    {
    	color: black !important;
    }
   

}
</style>
<div class="sub-title">Forecast Report</div>
<div ng-init='loadReportActForecast();loadProduct();'>
	<div class="products">
		<div class="product">
			<form ng-submit='loadReportActForecast()'>
				<div class="table-responsive">
					<table class="table table-condensed table-borderless" width="100%">
						<tr>
							<td width="150">Month</td>
							<td width="100">Year</td>
							<td width="200">Products</td>
							<td></td>
						</tr>
						<tr>
							<td>
								<select ng-model="search.month" class="form-control input-sm" style="width: 120px;">
									<option value="01">January</option>
									<option value="02">February</option>
									<option value="03">March</option>
									<option value="04">April</option>
									<option value="05">May</option>
									<option value="06">June</option>
									<option value="07">July</option>
									<option value="08">August</option>
									<option value="09">September</option>
									<option value="10">October</option>
									<option value="11">November</option>
									<option value="12">December</option>
								</select>
							</td>
							<td>
								<select ng-model="search.year" class="form-control input-sm" style="width: 70px;">
									<option ng-repeat="year in year" value="{{year}}">{{year}}</option>
								</select>
							</td>
							<td>
								<select class="form-control" placeholder="Product Name" ng-model='search.product'>
									<option value="">All Products</option>
									<option value="{{product.product_code}}" ng-repeat='product in DATA.products'>{{product.name}}</option>
								</select>
							</td>
							<td>
								<button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button>
							</td>
						</tr>
						
					</table>
				</div>
			</form>

		</div>
	</div>
	<div class="products">
	<div ng-show='show_loading_DATA'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	<div ng-show='not_found' class="alert alert-warning"><em>Data not found...</em></div>
	<div ng-show='!show_loading_DATA && !not_found' class="row" style="margin: 0 !important;">
		<div class="col-xs-3" style="margin:0 !important;padding:0 !important;">
			<table class="table table-striped table-bordered" style="font-size:11px">	
				<tr>
					<td><strong>PRODUCT</strong></td>
				</tr>
				<tr ng-repeat="data in report.forcast.report" style="height:50px; overflow:hidden;">
					<td style="vertical-align:middle"><span style="font-size: 10px;"><strong>{{data.product_name}}</strong></span></td>
				</tr>
				<tr>
					<td><strong>TOTAL</strong></td>
				</tr>	
			</table>
		</div>
		<div class="table-responsive col-xs-18" zzzstyle="overflow-x:scroll" style="margin:0 !important;padding:0 !important;">
			<div style="width:1200px">
				<table class="table table-striped table-bordered" style="font-size:11px">
					<tr>
						<th style="text-align: center;" ng-repeat="date in report.forcast.date">{{fn.formatDate(date, "dd")}}</th>
						<th>TOTAL</th>
					</tr>
					
					<tr ng-repeat="data in report.forcast.report" style="height:50px">
						<td style="text-align: center;vertical-align:middle;" ng-repeat="dates in data.dates">{{dates}}</td>
						<td style="text-align: center;vertical-align:middle;">{{data.total}}</td>
					</tr>
					<tr>
						
						<td style="text-align: center;" ng-repeat="total in report.total"><strong>{{total}}</strong></td>
						<td style="text-align: center;">{{total_forcst}}</td>
					</tr>
					
				</table>
			</div>
		</div>
				
	</div>
			
	</div>
	</div>

	<br />
	<!-- <div ng-repeat="data in report.forcast.report" class="row">
		<div class="col-md-2"><strong>{{data.code_gabung}}</strong></div><div class="col-md-6">: {{data.departure + ' - ' + data.arrival }}</div>
	</div> -->

	<br />
	<a href="<?=site_url("export_to_excel/report_act_forecast")?>?s[month]={{search.month}}&s[year]={{search.year}}&s[product_code]={{search.product}}" target="_blank"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export To Excel</a>
	&nbsp; | &nbsp;
	<a href="<?=site_url("home/print_page/#/print/report_act_forecast/")?>{{search.month}}/{{search.year}}/{{search.product}}" target="_blank" >
		<i class="fa fa-print" aria-hidden="true"></i> Print
	</a>
			
	
	</div>
</div>
<style>
	.header td{vertical-align:middle !important}
</style>
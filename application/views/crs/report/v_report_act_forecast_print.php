<div ng-init="printActReport()">
<style type="text/css" media="print">
  @page { size: landscape; }
</style>
	<div class="no-print text-center" style="text-align:center !important">
		
		<br /><br />
	</div>
	
	
	<br />
	<div class="normal-paper" >
		
		<div class="header" style="margin: 0 auto; width: 700px;height: auto;">
			<div class="pull-right text-center" align="center">
				<div style="font-size:14px" class="title"><strong><?=$vendor["business_name"]?></strong></div>
				<div style="font-size:12px">FORECAST</div>

			</div>
			<div class="title">{{month}} - {{year}}</div>

		</div>
		<div class="table-responsive" style="margin:0 auto; width:700px; height:auto;">
			<table  style="font-size:11px" border="1">
				<tr>
					<th></th>
					<th width="40" style="text-align: center;" ng-repeat="date in report.forcast.date">{{fn.formatDate(date, "dd")}}</th>
					<th width="40" style="text-align: center;">Total</th>
				</tr>
				<tr>
					<td width="100"><span style="padding-left: 5px;font-size: 8px;"><strong>PRODUCT</strong></span></td>
				</tr>
				<tr ng-repeat="data in report.forcast.report">
					<td><span style="padding-left: 5px;font-size: 8px;"><strong>{{data.product_name}}</strong></span></td>

					<td style="text-align: center;" ng-repeat="dates in data.dates">{{dates}}</td>
					<td style="text-align: center;">{{data.total}}</td>
				</tr>
				<!-- <tr>
					<td ><span style="padding-left: 5px;"><strong>Total</strong></span></td>
					<td height="40" style="text-align: center;" ng-repeat="total in report.forcast.total"><strong>{{total}}</strong></td>
				</tr> -->
			</table>
		<div>
		<br />
		
			</div>
			<br />
				

				<br />
		</div>

	</div>
<style>


	.print_area{background:none !important; padding:0 !important;}
	.small_paper{width:8cm !important; margin:0 auto !important}
	.normal-paper{background:#FFF; padding:10px}
	.small-paper{width:8cm; margin:auto; padding:10px; background:#FFF}
	.small-paper .header .title{font-size:18px !important}

</style>


<?php
//pre($vendor)
?>
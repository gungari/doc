<div ng-init="printReport()">
<style type="text/css" media="print">
  @page { size: landscape; }
</style>
	<div class="no-print text-center" style="text-align:center !important">
		
		<br /><br />
	</div>
	
	
	<br />
	<div class="normal-paper landscape" >
		
		<div class="header" style="margin: 0 auto; width: 1000px;height: auto;">
			<div class="pull-right text-center" align="center">
				<div style="font-size:14px" class="title"><strong><?=$vendor["business_name"]?></strong></div>
				<div style="font-size:12px">FORECAST</div>

			</div>
			<div class="title">{{month}} - {{year}}</div>

		</div>
		<div class="table-responsive col-md-18" zzzstyle="overflow-x:scroll" style="margin:0 !important;padding:0 !important;">
			<div style="width:1000px">
				<table class="table table-bordered" style="font-size:10px">
					<tr>
						<td></td>
						<td  ng-repeat="date in DATA.forcast.date">{{fn.formatDate(date, "dd")}}</td>
					</tr>

					<tbody ng-repeat="(index, data) in DATA.forcast.report">
						<tr style="background:#FAFAFA">

							<td colspan="{{DATA.forcast.date.length + 1}}"><strong>{{index}}</strong></td>
						</tr>
						
						<tr ng-show="data" ng-repeat="forcast in data.data">
							<td>{{forcast.code_gabung}}</td>
							<td ng-repeat="date in forcast.dates">{{date}}</td>
							<tr>
								<td><b>Total</b></td>
								<td ng-repeat="total in data.total"><b>{{total}}</b></td>
							</tr>
						</tr>
					</tbody>
					<tr>
						<th></th>
						<th style="text-align: center;" ng-repeat="date in report.forcast.date">{{fn.formatDate(date, "dd")}}</th>
						
					</tr>
					
				</table>
			</div>
		</div>	
		<hr />
		<div ng-repeat="data in DATA.forcast.report">
			<div ng-repeat="arr in data.data"  class="row">
				<div  class="col-xs-2" ><strong>{{arr.code_gabung}}</strong></div>
				<div  class="col-xs-6" ><strong>: {{arr.departure + ' - ' + arr.arrival }}</strong></div>
			</div>
		</div>
	</div>
<style>


	.print_area{background:none !important; padding:0 !important;}
	.small_paper{width:8cm !important; margin:0 auto !important}
	.normal-paper{background:#FFF; padding:10px}
	.small-paper{width:8cm; margin:auto; padding:10px; background:#FFF}
	.small-paper .header .title{font-size:18px !important}
	.landscape {width: 1020px; height: auto; margin-left: -129px;}
</style>


<?php
//pre($vendor)
?>
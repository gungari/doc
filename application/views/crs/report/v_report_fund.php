
<style type="text/css">
	hr {
		margin: 0 !important;
		padding: 0 !important;
	}
</style>
<style type="text/css">
	.tr-muted {
		color: #ccc;
	}
	ul li.title {
		margin-top: 10px;
	}
	ul li.title a {
		padding: 5px;
		font-weight: bold;
	}
	a.muted-link {
		text-decoration: inherit;
		color: inherit;
	}
	a.muted-link:hover {
		text-decoration: inherit;
		color: #1B77C0;
	}
	.report-content table th, .report-content table td {
		vertical-align: middle !important;
	}
	.report-content .report-margin {
		margin:10px auto;
	}
	.report-content .report-border {
		border: #ccc solid 0.5px;;
	}
	.report-content table .header, .report-content table .footer {
		background-color: #eee;
		font-weight: bold;
	}
	.text-middle{
		vertical-align: middle;
	}
	[ng-click],
	[data-ng-click],
	[x-ng-click] {
	    cursor: pointer;
	}
</style>
<div class="sub-title">FUND SOURCE REPORT</div>
	
		
	<div class="products" ng-init='loadReportFund()'>
		<br />
			<form ng-submit='loadReportFundData()' class="form-inline">
				<!-- <div class="table-responsive">
					<table class="table table-condensed table-borderless" width="100%">
						<tr>
							<td width="140">Date</td>
							<td width="200">Source</td>
							<td></td>
						</tr>
						<tr>
							<td>
								<input type="text" ng-model="search.date" class="form-control input-sm datepicker" placeholder="Date" style="width:140px; display:inline" />
							</td>
							<td>
								<select class="form-control input-sm" ng-model='search.booking_source'>
									<option value="">All</option>
									<option value="{{booking_source.code}}" ng-repeat='booking_source in $root.DATA_booking_source'>{{booking_source.name}}</option>
								</select>
							</td>
							<td>
								<button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button>
							</td>
						</tr>
					</table>
				</div> -->
				<div class="form-group">
					<label class="sr-only" for="view">View:</label>
					<select name="view" class="form-control input-sm" ng-model="search.view">
						<?php foreach(array("date"=>"Date", "month"=>"Month", "year"=>"Year") as $key => $value) : ?>
							<?php if($key==$view) : ?>
								<option value="<?=$key?>" selected><?=$value?></option>
							<?php else : ?>
								<option value="<?=$key?>"><?=$value?></option>
							<?php endif; ?>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="form-group" ng-show="search.view == 'date'">
					<label class="sr-only" for="date">Date:</label>
					<select name="date" class="form-control input-sm"  ng-model="search.date">
						<?php for($i=1;$i<=date('t', strtotime("$year-$month-01"));$i++) : ?>
							<?php if($date==$i) : ?>
								<option value="<?=$i?>" selected><?=$i?></option>
							<?php else : ?>
								<option value="<?=$i?>"><?=$i?></option>
							<?php endif; ?>
						<?php endfor; ?>
					</select>
				</div>
				<div class="form-group" ng-show="search.view == 'date' || search.view == 'month'">
					<label for="month" class="sr-only">Month:</label>
					<select name="month" class="form-control input-sm" ng-model="search.month">
						<?php foreach(array("1"=>"January","2"=>"February","3"=>"March","4"=>"April","5"=>"May","6"=>"June","7"=>"July","8"=>"August","9"=>"September","10"=>"October","11"=>"November","12"=>"December") as $key => $value) : ?>
							<?php if($month==$key) : ?>
								<option value="<?=$key?>" selected><?=$value?></option>
							<?php else : ?>
								<option value="<?=$key?>"><?=$value?></option>
							<?php endif; ?>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="form-group">
					<label for="year" class="sr-only">Year:</label>
					<select name="year" class="form-control input-sm" ng-model="search.year">
						<?php for($i=date("Y");$i>=2014;$i--) : ?>
							<?php if($year==$i) : ?>
								<option value="<?=$i?>" selected><?=$i?></option>
							<?php else : ?>
								<option value="<?=$i?>"><?=$i?></option>
							<?php endif; ?>
						<?php endfor; ?>
					</select>
				</div>
				
				<button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-menu-right"></span></button>
			</form>
			<br />
		</div>
	<div ng-show='show_loading_DATA'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	
	<div ng-show="show_error" class="alert alert-warning report-margin" id="tableBooking" role="alert">
			No data fund sources
	</div>
	<hr />
	<div  ng-show="DATA != ''">
	<div class="panel-body panel-chart" style="padding: 0 !important;">
		
		<div class="report-content" style="display: block;">
			<dir class="embed-responsive embed-responsive-16by9 report-margin report-border">
				<div id="agentLine" style="width:100%; height:400px; margin: 0px auto;"></div>
			</dir>
		</div>
	</div>
	</div>
	
	<div style="margin-top: 10px;" ng-show="DATA != ''">
		<table class="table table-bordered report-margin" style="font-size:12px">
			<tr class="header">
				
				<th>Fund Source</th>

				<th class="text-right">Amount</th>
				
			</tr>
			
			<tbody ng-repeat="data in DATA.fund" style="border-top: 1px !important;">
				<tr>
					
					<td ng-class="{'tr-muted':(data.sources.OFFLINE.total==0)}"><strong>{{data.name}}</strong></td>
					<td class="text-right">{{data.currency}} {{fn.formatNumber(data.total, data.currency)}}</td>
					
				</tr>
				
			</tbody>
				<tr class="header">
					<td class="text-right"><b>TOTAL</b></td>
					<td class="text-right"><b>{{currency_fund}} {{fn.formatNumber(total_fund, currency_fund)}}</b></td>
				</tr>
			
		</table>
	</div>
	<!-- <div ng-show='date_data'>
		<div class="table-responsive" zzzstyle="overflow-x:scroll">
			<div style="width:1200px">
			<table class="table table-condensed table-bordered" style="font-size:11px">
				<tr class="header bold">
					<?php /*?><td rowspan="2" align="center" width="90">Order#<br />Booking Date</td><?php */?>
					<td rowspan="2" align="center" width="90">Voucher#<br />Date</td>
					<td rowspan="2">Customer Name</td>
					<td rowspan="2" align="center" width="30" title="Nationality">Nat.</td>
					<td rowspan="2" colspan="2" align="center">Trip</td>
					<td rowspan="2" width="170">Booking Source</td>
					<td colspan="3" align="center" title="Passenger">Passenger</td>
					<td colspan="3" align="center" title="Passenger">Price</td>
					<td rowspan="2" align="center" width="60">Pickup</td>
					<td rowspan="2" align="center" width="60">Dropoff</td>
					<td rowspan="2" align="center" width="60">Other</td>
					<td rowspan="2" align="center" width="65">Sub Total</td>
					<td rowspan="2" align="center" width="60">Discount</td>
					<td rowspan="2" align="center" width="60">Fee</td>
					<td rowspan="2" align="center" width="60">TOTAL</td>
				</tr>
				<tr class="header bold">
					<td width="30" align="center"><small>Adl.</small></td>
					<td width="30" align="center"><small>Chi.</small></td>
					<td width="30" align="center"><small>Inf.</small></td>
					<td width="60" align="center"><small>Adl.</small></td>
					<td width="60" align="center"><small>Chi.</small></td>
					<td width="60" align="center"><small>Inf.</small></td>
				</tr>
				<tbody ng-repeat='revenue_data in DATA.fund'>
					<tr style="background:#FAFAFA">
						<td colspan="18"><strong>{{revenue_data.departure_port.name}} ({{revenue_data.departure_time}}) - {{revenue_data.arrival_port.name}} ({{revenue_data.arrival_time}})</strong></td>
					</tr>
					
					<tr ng-repeat='revenue in revenue_data.revenue'>
						<td align="center">
							<a href="" ng-click="openPopUpWindowVoucher('<?=site_url("home/print_page/#/print/voucher_trans/")?>', revenue.booking.booking_code, revenue.detail.voucher_code)">
								<strong>{{revenue.detail.voucher_code}}</strong>
							</a><br />
							<small>{{fn.formatDate(revenue.detail.date, "dd M yy")}}</small>
						</td>
						<td>{{revenue.booking.customer.full_name}}</td>
						<td align="center" class="text-capitalize" title="{{revenue.booking.customer.country_name}}">{{revenue.booking.customer.country_code}}</td>
						<td width="40" align="center">
							{{revenue.detail.departure.port.port_code}} <br /><small>{{revenue.detail.departure.time}}</small>
						</td>
						<td width="40" align="center">
							{{revenue.detail.arrival.port.port_code}} <br /><small>{{revenue.detail.arrival.time}}</small>
						</td>
						<td>
							<div ng-show='!revenue.booking.agent' class="text-capitalize">{{revenue.booking.source.toLowerCase()}}</div>
							<div ng-show='revenue.booking.agent'>{{revenue.booking.agent.name}}</div>
						</td>
						<td align="center">{{(revenue.detail.qty_1>0?revenue.detail.qty_1:'-')}}</td>
						<td align="center">{{(revenue.detail.qty_2>0?revenue.detail.qty_2:'-')}}</td>
						<td align="center">{{(revenue.detail.qty_3>0?revenue.detail.qty_3:'-')}}</td>
						
						<td align="right">{{fn.formatNumber((revenue.detail.rates.rates_1*revenue.detail.qty_1), revenue.detail.rates.currency)}}</td>
						<td align="right">{{fn.formatNumber((revenue.detail.rates.rates_2*revenue.detail.qty_2), revenue.detail.rates.currency)}}</td>
						<td align="right">{{fn.formatNumber((revenue.detail.rates.rates_3*revenue.detail.qty_3), revenue.detail.rates.currency)}}</td>
						
						<td align="right">{{fn.formatNumber(revenue.detail.pickup_price, revenue.detail.rates.currency)}}</td>
						<td align="right">{{fn.formatNumber(revenue.detail.dropoff_price, revenue.detail.rates.currency)}}</td>
						<td align="right">{{fn.formatNumber(revenue.detail.additional_charge_price, revenue.detail.rates.currency)}}</td>
						<td align="right">{{fn.formatNumber(revenue.detail.subtotal, revenue.detail.rates.currency)}}</td>
						<td align="right">{{fn.formatNumber(revenue.detail.discount, revenue.detail.rates.currency)}}</td>
						<td align="right">{{fn.formatNumber(revenue.detail.hybrid_fee, revenue.detail.rates.currency)}}</td>
						
						<td align="right">{{fn.formatNumber(revenue.detail.total, revenue.detail.rates.currency)}}</td>
					</tr>
				</tbody>
			</table>
			</div>
		</div>
		<hr />
		<div ng-show='DATA.summary'>
			<table class="table table-borderless table-condensed" style="font-size:14px; font-weight:bold">
				<tr>
					<td align="right">TOTAL</td>
					<td width="10">:</td>
					<td align="right" width="130">{{DATA.summary.currency}} {{fn.formatNumber(DATA.summary.total_price, DATA.summary.currency)}}</td>
				</tr>
				<tr>
					<td align="right">Adult</td>
					<td width="10">:</td>
					<td align="right">{{DATA.summary.total_qty_1}}</td>
				</tr>
				<tr>
					<td align="right">Child</td>
					<td width="10">:</td>
					<td align="right">{{DATA.summary.total_qty_2}}</td>
				</tr>
				<tr>
					<td align="right">Infant</td>
					<td width="10">:</td>
					<td align="right">{{DATA.summary.total_qty_3}}</td>
				</tr>
			</table>
		</div>
		
		
		
	</div> -->
</div>
<style>
	.header td{vertical-align:middle !important}
</style>

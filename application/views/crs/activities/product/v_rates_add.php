<div class="sub-title">
	<span ng-show='!DATA.current_rates.rates_code'><i class="fa fa-plus-square" aria-hidden="true"></i> Add New</span> 
	<span ng-show='DATA.current_rates.rates_code'><i class="fa fa-pencil" aria-hidden="true"></i> Edit</span>
	Rates
</div>
<br />
<div class="pull-right" ng-show="DATA.enable_copy_rates"><a href="" data-toggle="modal" ng-click="loadDataProducts();" data-target="#form-product-list"><i class="fa fa-copy" aria-hidden="true"></i> Copy Form Other Product</a></div><br /><br />

<div ng-init="ratesAddEdit();getContractRatesDetail();">
	<div class="products">
		<form ng-submit="saveDataProductRates($event)">
			<div ng-show='DATA.current_rates.error_desc.length>0' class="alert alert-danger show_error"><ul><li ng-repeat='err in DATA.current_rates.error_desc'>{{err}}</li></ul></div>
			<table width="100%" class="table td_center_align form-inline">
            	<tr>
                	<td style="vertical-align:top !important">Rates For*</td>
                    <td>
                    	<div ng-show="DATA.current_contract_rates">
                        	<h4 style="margin:0">{{DATA.current_contract_rates.name}}</h4>
                            Contract Rates Code : {{DATA.current_contract_rates.contract_rates_code}}<br />
                            Periode : {{fn.newDate(DATA.current_contract_rates.start_date) | date:'dd MMMM yyyy'}} -
            						  {{fn.newDate(DATA.current_contract_rates.end_date) | date:'dd MMMM yyyy'}}
                        </div>
                        <div ng-hide="DATA.current_contract_rates">
                            <label><input type="checkbox" ng-model='DATA.current_rates.rates_for.offline' ng-true-value="1" /> Offline Booking </label>
                            &nbsp;&nbsp;&nbsp;
                            <label><input type="checkbox" ng-model='DATA.current_rates.rates_for.walkin' ng-true-value="1" /> Walk In Booking </label>
                            &nbsp;&nbsp;&nbsp;
                            <label><input type="checkbox" ng-model='DATA.current_rates.rates_for.agent' ng-true-value="1" /> Agencies and Colleague </label>
                    	</div>
                    </td>
                </tr>
				<tr>
					<td width="150">Rates Name*</td>
					<td>
						<input required="required" placeholder="Rates Name" type="text" class="form-control input-sm txa_mxlen" data-mxlen="90" maxlength="90" style="width:90%" ng-model='DATA.current_rates.name' />
						<span class="dv_mxlen_remain">90/90</span>
					</td>
				</tr>
				<tr id="tr_rod" ng-hide="!(DATA.current_rates.rates_for.offline || DATA.current_rates.rates_for.walkin) || DATA.current_contract_rates">
					<td>Validity *</td>
					<td>
						<input required="required" type="text" class="form-control input-sm datepicker start_date datepicker" 	placeholder="Start Date" 	style="width:150px" ng-model='DATA.current_rates.start_date' 	ng-disabled="!(DATA.current_rates.rates_for.offline || DATA.current_rates.rates_for.walkin) || DATA.current_contract_rates" />
						-
						<input required="required" type="text" class="form-control input-sm datepicker end_date datepicker" 	placeholder="End Date" 		style="width:150px" ng-model='DATA.current_rates.end_date' 		ng-disabled="!(DATA.current_rates.rates_for.offline || DATA.current_rates.rates_for.walkin) || DATA.current_contract_rates" />
					</td>
				</tr>
				<tr>
					<td style="vertical-align:top !important">
                    	<span ng-hide="DATA.current_rates.is_packages">Sell Rates</span>
                        <span ng-show="DATA.current_rates.is_packages">Packages</span>
                    </td>
					<td>
                    	<?php //DEFAULT RATES ?>
                    	<div ng-show='!DATA.current_rates.is_custom_sell_rate && !DATA.current_rates.is_packages'>
                            <table width="100%" class="table-borderless table-condensed">
                                <tr>
                                    <td>Currency</td>
                                    <td>Adult*</td>
                                    <td>Child*</td>
                                    <td>Infant*</td>
                                </tr>
                                <tr>
                                    <td width="19%">
                                        <input type="text" disabled="disabled" class="form-control input-sm" style="width:100%; text-align:center" value="<?=@$vendor['default_currency']?>" />
                                    </td>
                                    <td width="27%">
                                        <input type="number" ng-required="!DATA.current_rates.is_custom_sell_rate && !DATA.current_rates.is_packages" min="0" step="any" class="form-control input-sm" placeholder="Sell Rate Adult " ng-model='DATA.current_rates.rates.rates_1' style="width:100%" />
                                    </td>
                                    <td width="27%">
                                        <input type="number" xxng-required="!DATA.current_rates.is_custom_sell_rate" min="0" step="any" class="form-control input-sm" placeholder="Sell Rate Child " ng-model='DATA.current_rates.rates.rates_2' style="width:100%" />
                                    </td>
                                    <td width="27%">
                                        <input type="number" xxng-required="!DATA.current_rates.is_custom_sell_rate" min="0" step="any" class="form-control input-sm" placeholder="Sell Rate Infant" ng-model='DATA.current_rates.rates.rates_3' style="width:100%" />
                                    </td>
                                </tr>
                            </table> 
                        </div>
                        <?php //--DEFAULT RATES ?>
                        
                        <?php //CUSTOM SELL RATES ?>
                        <div ng-show='DATA.current_rates.is_custom_sell_rate'>
                        	<div ng-repeat='custom_rates in DATA.current_rates.custom_rates' class="form-inline" style="margin-bottom:5px">
                            	<input type="text" class="form-control input-sm" placeholder="Caption" ng-model="custom_rates.rates_caption" style="width:150px" ng-required='DATA.current_rates.is_custom_sell_rate' />
                                <div class="input-group">
									<span class="input-group-addon input-sm"><?=@$vendor['default_currency']?></span>
									<input type="number" min="0" step="0.01" ng-model="custom_rates.rates" class="form-control input-sm" style="width:150px" placeholder="Price" ng-required='DATA.current_rates.is_custom_sell_rate' />
								</div>
                                /
                                <input type="text" class="form-control input-sm" placeholder="Caption" ng-model="custom_rates.rates_unit" style="width:100px" ng-required='DATA.current_rates.is_custom_sell_rate' />
                                &nbsp;&nbsp;
                                <a href="" ng-click="DATA.current_rates.custom_rates.splice($index,1)" title="Remove"><span class="glyphicon glyphicon-trash"></span></a>
                            </div>
                            <div ng-show='(DATA.current_rates.custom_rates.length < 5)'>
	                            <button type="button" class="btn btn-info btn-xs" ng-click="ratesAddCustomRatesName(DATA.current_rates.custom_rates)"><span class="glyphicon glyphicon-plus"></span> Add Item </button>
                            </div>
                        </div>
                        <?php //--CUSTOM SELL RATES ?>
                        
                        <?php //PACKAGES RATES?>
                        <div ng-show='DATA.current_rates.is_packages'>
                        	<ol style="padding-left:15px">
                            	<li ng-repeat='packages_option in DATA.current_rates.packages_option'>
                                    <div class="form-inline" style="margin-bottom:5px">
                                    	<div style="margin-bottom:10px">
                                        	<a href="" ng-click="DATA.current_rates.packages_option.splice($index,1)" title="Remove" class="pull-right" style="color:red"><span class="glyphicon glyphicon-trash"></span></a>
                                            
                                            <input type="text" class="form-control input-sm" placeholder="Packages Name" ng-model="packages_option.caption" style="width:250px" ng-required='DATA.current_rates.is_packages' />
                                            <div class="input-group">
                                                <span class="input-group-addon input-sm"><?=@$vendor['default_currency']?></span>
                                                <input type="number" min="0" step="0.01" ng-model="packages_option.rates" class="form-control input-sm" style="width:150px" placeholder="Price" xng-required='DATA.current_rates.is_packages' />
                                            </div>
                                            /
                                            <input type="text" class="form-control input-sm" placeholder="Caption" ng-model="packages_option.rates_unit" style="width:100px" ng-required='DATA.current_rates.is_packages' />
                                        </div>
                                        <div>
                                        	<table class="table table-condensed table-borderless">
                                            	<tr>
                                                	<td width="105">Include :</td>
                                                    <td width="40">Adult</td>
                                                    <td width="120"><input type="number" ng-model="packages_option.include_pax.opt_1" min="0" style="width:50px" /></td>
                                                    <td width="40">Child</td>
                                                    <td width="120"><input type="number" ng-model="packages_option.include_pax.opt_2" min="0" style="width:50px" /></td>
                                                    <td width="40">Infant</td>
                                                    <td width="120"><input type="number" ng-model="packages_option.include_pax.opt_3" min="0" style="width:50px" /></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                	<td width="90">Additional (<?=@$vendor['default_currency']?>) : </td>
                                                    <td width="40">Adult</td>
                                                    <td width="120"><input type="number" ng-model="packages_option.extra_rates.opt_1" min="0" style="width:100%" step="any" /></td>
                                                    <td width="40">Child</td>
                                                    <td width="120"><input type="number" ng-model="packages_option.extra_rates.opt_2" min="0" style="width:100%" step="any" /></td>
                                                    <td width="40">Infant</td>
                                                    <td width="120"><input type="number" ng-model="packages_option.extra_rates.opt_3" min="0" style="width:100%" step="any" /></td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                            <?php /*?>Include : Adult : <input type="number" ng-model="packages_option.include_pax.opt_1" min="0" style="width:50px" /> &nbsp;&nbsp;
                                                      Child : <input type="number" ng-model="packages_option.include_pax.opt_2" min="0" style="width:50px" /> &nbsp;&nbsp;
                                                      Infant : <input type="number" ng-model="packages_option.include_pax.opt_3" min="0" style="width:50px" /> <?php */?>
                                        </div>
                                        <hr style="margin:10px 0" />
                                    </div>
                            	</li>
                            </ol>
                            <div ng-show='(DATA.current_rates.custom_rates.length < 5)'>
	                            <button type="button" class="btn btn-info btn-xs" ng-click="ratesAddPackagesName(DATA.current_rates.packages_option)"><span class="glyphicon glyphicon-plus"></span> Add Item </button>
                            </div>
                        </div>
                        <?php //--PACKAGES RATES?>
                        
                        <hr style="margin:5px 0" />
                        <div class="pull-right">
	                        <?php /*?><label style="margin:0"><input type="checkbox" ng-true-value="1" ng-false-value="0" ng-model='DATA.current_rates.is_custom_sell_rate' /> Custom Rate</label><?php */?>
                            <label style="margin:0"><input type="checkbox" ng-true-value="1" ng-false-value="0" ng-model='DATA.current_rates.is_packages' /> Package Rates</label>
                        </div>
					</td>
				</tr>
				<tr>
					<td style="vertical-align:top !important">Pickup Service</td>
					<td>
						<label><input type="radio" value="no" class="pickup_service" ng-model='DATA.current_rates.pickup_service' /> Not Available </label> &nbsp;&nbsp;&nbsp;
						<label><input type="radio" value="yes" class="pickup_service" ng-model='DATA.current_rates.pickup_service' /> Available</label> &nbsp;&nbsp;&nbsp;
						<?php /*?><label><input name="dt[f_pickup_service]" type="radio" class="f_pickup_service" value="BUS" onclick="chk_tbl_pickup_service()"> Shuttle Bus</label> &nbsp;&nbsp;&nbsp;
						<label><input name="dt[f_pickup_service]" type="radio" class="f_pickup_service" value="CUST" onclick="chk_tbl_pickup_service()"> Custom</label><?php */?>
						
						<div ng-show='DATA.current_rates.pickup_service=="yes"'>
							<table class="table table-condensed table-hover" style="margin:0">
								<tr class="successs">
									<th width="200">Area</th>
									<th width="100">Time</th>
									<th colspan="2">Price (<?=@$vendor['default_currency']?>)</th>
									<th></th>
								</tr>
								<tr ng-repeat='area in DATA.current_rates.pickup_area'>
									<td><input ng-model='area.area' ng-required='DATA.current_rates.pickup_service=="yes"' maxlength="100" type="text" class="form-control  input-sm" placeholder="Area" style="width:100%" /></td>
									<td><input ng-model='area.time' ng-required='DATA.current_rates.pickup_service=="yes"' maxlength="25" type="text" class="form-control  input-sm" placeholder="Time" style="width:100%" /></td>
									<td width="150"><input ng-model='area.price' ng-required='DATA.current_rates.pickup_service=="yes"' maxlength="25" type="number" step="0.1" placeholder="Price" class="form-control input-sm" style="width:100%" /></td>
									<td width="90">
										<select ng-model='area.type' ng-required='DATA.current_rates.pickup_service=="yes"' class="form-control input-sm" style="width:100%">
											<option value="way">/ way</option>
											<option value="pax">/ pax</option>
										</select>
									</td>
									<td>
										<a href="" ng-click="DATA.current_rates.pickup_area.splice($index,1)" title="Remove"><span class="glyphicon glyphicon-trash"></span></a>
									</td>
								</tr>
							</table>
							<button type="button" class="btn btn-info btn-xs" ng-click="ratesAddPickUpOrDropOffArea(DATA.current_rates.pickup_area)"><span class="glyphicon glyphicon-plus"></span> Add Item </button>
						</div>
					</td>
				</tr>
				<tr>
					<td style="vertical-align:top !important">Dropoff Service</td>
					<td>
						<label><input type="radio" value="no" class="dropoff_service" ng-model='DATA.current_rates.dropoff_service' /> Not Available </label> &nbsp;&nbsp;&nbsp;
						<label><input type="radio" value="yes" class="dropoff_service" ng-model='DATA.current_rates.dropoff_service' /> Available</label> &nbsp;&nbsp;&nbsp;
						<?php /*?><label><input name="dt[f_pickup_service]" type="radio" class="f_pickup_service" value="BUS" onclick="chk_tbl_pickup_service()"> Shuttle Bus</label> &nbsp;&nbsp;&nbsp;
						<label><input name="dt[f_pickup_service]" type="radio" class="f_pickup_service" value="CUST" onclick="chk_tbl_pickup_service()"> Custom</label><?php */?>
						
						<div ng-show='DATA.current_rates.dropoff_service=="yes"'>
							<table class="table table-condensed table-hover" style="margin:0">
								<tr class="successs">
									<th width="200">Area</th>
									<th width="100">Time</th>
									<th colspan="2">Price (<?=@$vendor['default_currency']?>)</th>
									<th></th>
								</tr>
								<tr ng-repeat='area in DATA.current_rates.dropoff_area'>
									<td><input ng-model='area.area' ng-required='DATA.current_rates.dropoff_service=="yes"' maxlength="100" type="text" class="form-control  input-sm" placeholder="Area" style="width:100%" /></td>
									<td><input ng-model='area.time' ng-required='DATA.current_rates.dropoff_service=="yes"' maxlength="25" type="text" class="form-control  input-sm" placeholder="Time" style="width:100%" /></td>
									<td width="150"><input ng-model='area.price' ng-required='DATA.current_rates.dropoff_service=="yes"' maxlength="25" type="number" step="0.1" placeholder="Price" class="form-control input-sm" style="width:100%" /></td>
									<td width="90">
										<select ng-model='area.type' ng-required='DATA.current_rates.dropoff_service=="yes"' class="form-control input-sm" style="width:100%">
											<option value="way">/ way</option>
											<option value="pax">/ pax</option>
										</select>
									</td>
									<td>
										<a href="" ng-click="DATA.current_rates.dropoff_area.splice($index,1)" title="Remove"><span class="glyphicon glyphicon-trash"></span></a>
									</td>
								</tr>
							</table>
							<button type="button" class="btn btn-info btn-xs" ng-click="ratesAddPickUpOrDropOffArea(DATA.current_rates.dropoff_area)"><span class="glyphicon glyphicon-plus"></span> Add Item </button>
						</div>
					</td>
				</tr>
				<tr>
					<td>Duration*</td>
					<td>
						<div class="input-group">
							<input required="required" type="number" min="1" class="form-control input-sm" style="width:100px" ng-model='DATA.current_rates.duration' />
							<select required="required" class="form-control input-sm" style="width:100px" ng-model='DATA.current_rates.duration_unit'>
								<option value="MENIT" selected="selected">minute</option>
                                <option value="JAM" selected="selected">hour</option>
                                <option value="HARI" selected="selected">day</option>
                                <option value="KALI" selected="selected">time(s)</option>
							</select>
						</div>
					</td>
				</tr>
                <tr>
					<td>Minimum Order*</td>
					<td>
						<div class="input-group">
							<input required="required" type="number" min="1" class="form-control input-sm" style="width:100px" ng-model='DATA.current_rates.min_order' />
							<select required="required" name="pax_or_paket" id="pax_or_paket" class="form-control input-sm" style="width:100px">
								<option value="pax" selected="selected">{{(DATA.current_rates.is_packages?'Packages':'Person')}}</option>
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<td>Cut Off Booking*</td>
					<td>
						<input type="number" min="0" required="required" class="form-control input-sm" style="width:100px" maxlength="2" ng-model='DATA.current_rates.cut_of_booking' /> 
						<span>days in advance</span>
					</td>
				</tr>
				<?php /*?><tr>
					<td>Booking Handling</td>
					<td>
						<select class="form-control input-sm" style="width:50%" ng-model='DATA.current_rates.booking_handling'>
							<option value="INSTANT">Instant Booking</option>
							<option value="ENQUIRY">Enquiry Booking</option>
						</select>
					</td>
				</tr><?php */?>
				<tr>
					<td>Available On</td>
					<td class="form-inline">
						<div class="btn-group btn-days">
							<button type="button" class="btn btn-sm" ng-click="ratesCheckAvailableDayInWeek('mon')" ng-class="{'btn-info':DATA.current_rates.available_on.indexOf('mon') >= 0}">Mon</button>
							<button type="button" class="btn btn-sm" ng-click="ratesCheckAvailableDayInWeek('tue')" ng-class="{'btn-info':DATA.current_rates.available_on.indexOf('tue') >= 0}">Tue</button>
							<button type="button" class="btn btn-sm" ng-click="ratesCheckAvailableDayInWeek('wed')" ng-class="{'btn-info':DATA.current_rates.available_on.indexOf('wed') >= 0}">Wed</button>
							<button type="button" class="btn btn-sm" ng-click="ratesCheckAvailableDayInWeek('thu')" ng-class="{'btn-info':DATA.current_rates.available_on.indexOf('thu') >= 0}">Thu</button>
							<button type="button" class="btn btn-sm" ng-click="ratesCheckAvailableDayInWeek('fri')" ng-class="{'btn-info':DATA.current_rates.available_on.indexOf('fri') >= 0}">Fri</button>
							<button type="button" class="btn btn-sm" ng-click="ratesCheckAvailableDayInWeek('sat')" ng-class="{'btn-info':DATA.current_rates.available_on.indexOf('sat') >= 0}">Sat</button>
							<button type="button" class="btn btn-sm" ng-click="ratesCheckAvailableDayInWeek('sun')" ng-class="{'btn-info':DATA.current_rates.available_on.indexOf('sun') >= 0}">Sun</button>
						</div>
					</td>
				</tr>
                <tr>
					<td style="vertical-align:top !important">Inclusion</td>
					<td>
						<div class="form-inline important-infoEN" style="background:#FAFAFA; border:solid 1px #EEE; padding:10px">
							<div class="important-info-list" data-id="EN-1">
								<ul style="margin:5px 0 10px 15px; padding:0" class="sortable ui-sortable">
									<li style="margin-bottom: 5px;" ng-repeat='str in DATA.current_rates.inclusion'>
										<input type="text" required="required" class="form-control input-sm input-sm" ng-model='str.str' style="width:90%" maxlength="255"> 
										<a href="" ng-click="DATA.current_rates.inclusion.splice($index,1)" title="Remove"><span class="glyphicon glyphicon-trash"></span></a>
									</li>
								</ul>
								<button type="button" class="btn btn-info btn-xs" ng-click="ratesAddImportantInfo(DATA.current_rates.inclusion)"><span class="glyphicon glyphicon-plus"></span> Add Item </button>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td style="vertical-align:top !important">Exclusion</td>
					<td>
						<div class="form-inline important-infoEN" style="background:#FAFAFA; border:solid 1px #EEE; padding:10px">
							<div class="important-info-list" data-id="EN-1">
								<ul style="margin:5px 0 10px 15px; padding:0" class="sortable ui-sortable">
									<li style="margin-bottom: 5px;" ng-repeat='str in DATA.current_rates.exclusion'>
										<input type="text" required="required" class="form-control input-sm input-sm" ng-model='str.str' style="width:90%" maxlength="255"> 
										<a href="" ng-click="DATA.current_rates.exclusion.splice($index,1)" title="Remove"><span class="glyphicon glyphicon-trash"></span></a>
									</li>
								</ul>
								<button type="button" class="btn btn-info btn-xs" ng-click="ratesAddImportantInfo(DATA.current_rates.exclusion)"><span class="glyphicon glyphicon-plus"></span> Add Item </button>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td style="vertical-align:top !important">Additional Info</td>
					<td>
						<div class="form-inline important-infoEN" style="background:#FAFAFA; border:solid 1px #EEE; padding:10px">
							<div class="important-info-list" data-id="EN-1">
								<ul style="margin:5px 0 10px 15px; padding:0" class="sortable ui-sortable">
									<li style="margin-bottom: 5px;" ng-repeat='str in DATA.current_rates.additional_info'>
										<input type="text" required="required" class="form-control input-sm input-sm" ng-model='str.str' style="width:90%" maxlength="255"> 
										<a href="" ng-click="DATA.current_rates.additional_info.splice($index,1)" title="Remove"><span class="glyphicon glyphicon-trash"></span></a>
									</li>
								</ul>
								<button type="button" class="btn btn-info btn-xs" ng-click="ratesAddImportantInfo(DATA.current_rates.additional_info)"><span class="glyphicon glyphicon-plus"></span> Add Item </button>
							</div>
						</div>
					</td>
				</tr>
                
                <tr>
					<td style="vertical-align:top !important">Additional Charge</td>
					<td>
						<div class="form-inline important-infoEN" style="background:#FAFAFA; border:solid 1px #EEE; padding:10px">
							<div class="important-info-list" data-id="EN-1">
								<ul style="margin:5px 0 10px 15px; padding:0" class="sortable ui-sortable">
									<li style="margin-bottom: 5px;" ng-repeat='_item in DATA.current_rates.additional_charge'>
                                    	<p>
	                                        <a href="" ng-click="DATA.current_rates.additional_charge.splice($index,1)" title="Remove" class="pull-right"><span class="glyphicon glyphicon-trash"></span></a>
    	                                    <input type="text" required="required" class="form-control input-sm input-sm" ng-model='_item.name' style="width:90%" maxlength="255" placeholder="Name" />
                                        </p>
                                        <p>
                                            Price (<?=@$vendor['default_currency']?>) : 
                                            <input type="number" required="required" min="0" step="any" class="form-control input-sm input-sm" ng-model='_item.price' style="width:140px" placeholder="Price" />
											&nbsp;&nbsp;&nbsp;
                                            <label>
                                            	<input type="radio" ng-model="_item.payment_based_on" name="_item_payment_based_on_{{$index}}" value='PERPERSON' /> Per Pax
                                            </label>
                                            &nbsp;&nbsp;&nbsp;
                                            <label>
                                            	<input type="radio" ng-model="_item.payment_based_on" name="_item_payment_based_on_{{$index}}" value='PERBOOKING' /> 
                                                <span ng-show="_item.payment_based_on=='PERPERSON'">
                                                	Costum
                                                </span>
                                                <span ng-hide="_item.payment_based_on=='PERPERSON'">
	                                                Per <input type="text" class="form-control input-sm" ng-model='_item.unit' style="width:100px" ng-disabled="_item.payment_based_on=='PERPERSON'" />
                                            	</span>
                                            </label>
                                            
                                            
                                            <?php /*?><select ng-model='_item.payment_based_on' class="form-control input-sm" >
                                                <option value="PERPERSON">/ Pax</option>
                                                <option value="PERBOOKING">/ Packages</option>
                                            </select><?php */?>
                                        </p>
                                        <hr style="margin:7px 0" />
									</li>
								</ul>
								<button type="button" class="btn btn-info btn-xs" ng-click="ratesAddAdditionalCharge(DATA.current_rates.additional_charge)"><span class="glyphicon glyphicon-plus"></span> Add Item </button>
							</div>
						</div>
					</td>
				</tr>
                
				<tr>
					<td></td>
					<td>
						<button type="submit" class="btn btn-lg btn-primary">Save Rates</button>
						&nbsp;&nbsp;&nbsp;
						<a href="#/activities/product_detail/{{DATA.product_detail.product_code}}/rates"><strong>Cancel</strong></a>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>

<script>activate_sub_menu_schedule_detail("rates");</script>

<?php /*?>{{data_rates}}<?php */?>

<div class="modal fade" id="form-product-list" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">
            Copy Rates
        </h4>
      </div>
      <div class="modal-body">
      	
        <div ng-show='!DATA.copy_rates_from.product'>
            <ol style="padding-left:25px">
                <li ng-repeat='product in DATA.products.products'>
                    <a href="" ng-click='DATA.copy_rates_from.product = product;copyRatesGetProductRates()'>{{product.product_code}} - {{product.name}}</a>
                    <hr style="margin:10px 0" />
                </li>
            </ol>
      	</div>
        
        <div ng-show='DATA.copy_rates_from.product'>
        	<strong>Rates : {{DATA.copy_rates_from.product.name}}</strong><br /><br />
            <div ng-show='DATA.copy_rates_from.product.rates && DATA.copy_rates_from.product.rates.status=="ERROR"' class="alert alert-danger">
            	{{DATA.copy_rates_from.product.rates.error_desc[0]}}
            </div>
            <div ng-show='DATA.copy_rates_from.product.rates && DATA.copy_rates_from.product.rates.status=="SUCCESS"'>
            	<ol style="padding-left:25px">
                	<li ng-repeat='rates in DATA.copy_rates_from.product.rates.rates'>
                    	<a href="" ng-click='copySelectedRates(rates)'>{{rates.name}}</a>
                        <hr style="margin:10px 0" />
                    </li>
                </ol>
            </div>
            <a href="" ng-click='DATA.copy_rates_from.product=false'><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
        </div>
      </div>
      <div class="modal-footer" style="text-align:center">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
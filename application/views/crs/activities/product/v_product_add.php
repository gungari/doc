<div class="sub-title">
	<span ng-show='!DATA.current_product.product_code'><i class="fa fa-plus-square" aria-hidden="true"></i> Add New</span> 
	<span ng-show='DATA.current_product.product_code'><i class="fa fa-pencil" aria-hidden="true"></i> Edit</span>
	Product
</div>

<br />
<div ng-init="productAddEdit();">
	<div class="products">
		<div>
			<form ng-submit="saveDataProduct($event)">
				<div ng-show='DATA.current_product.error_desc.length>0' class="alert alert-danger show_error"><ul><li ng-repeat='err in DATA.current_product.error_desc'>{{err}}</li></ul></div>
				<table class="table table-borderlesss">
					<tr>
						<td width="150">Product Name</td>
						<td>
							<input type="text" class="form-control" placeholder="Product Name" required="required" ng-model='DATA.current_product.name' />
						</td>
					</tr>
                    <tr>
						<td>Product Highlight</td>
						<td>
							<input type="text" class="form-control" placeholder="Product Highlight 1" required="required" ng-model='DATA.current_product.highlight[0]' style="margin-bottom:5px" />
                            <input type="text" class="form-control" placeholder="Product Highlight 2" ng-model='DATA.current_product.highlight[1]' style="margin-bottom:5px" />
                            <input type="text" class="form-control" placeholder="Product Highlight 3" ng-model='DATA.current_product.highlight[2]' style="margin-bottom:5px" />
						</td>
					</tr>
					<tr>
						<td>Short Description</td>
						<td>
							<textarea class="form-control autoheight" placeholder="Short Description" ng-model='DATA.current_product.short_description' rows="3"></textarea>
						</td>
					</tr>
					<tr>
						<td>Full Description</td>
						<td>
							<textarea class="form-control autoheight" placeholder="Full Description" ng-model='DATA.current_product.full_description' rows="5"></textarea>
						</td>
					</tr>
                    <tr>
                    	<td>Show In</td>
                        <td>
                        	<select class="form-control" ng-model='DATA.current_product.show_in' style="width:150px">
                            	<option value="ALL">BES & CRS</option>
                                <option value="CRS">CRS Only</option>
                            </select>
                        </td>
                    </tr>
					<tr>
						<td></td>
						<td>
							<button type="submit" class="btn btn-primary">Save Product</button>
							&nbsp;&nbsp;&nbsp;
							<a ng-show='!DATA.current_product.product_code' ui-sref="activities.products_and_services"><strong>Cancel</strong></a>
							<a ng-show='DATA.current_product.product_code' ui-sref="activities.product_detail({'product_code':DATA.current_product.product_code})"><strong>Cancel</strong></a>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>

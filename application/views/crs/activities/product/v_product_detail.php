<div ng-show='DATA.products.products'>
    <div class="dropdown pull-right" style="margin-top:-50px">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            {{DATA.current_product.name}}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li ng-repeat="product in DATA.products.products" ng-show="product.publish_status == '1'" ng-class="{'active':(product.product_code == DATA.current_product.product_code)}">
            	<a ui-sref="activities.product_detail.rates({'product_code':product.product_code})">{{product.name}}</a>
            </li>
        </ul>
    </div>
</div>
<div class="sub-title"> Product Detail </div>

<br style="clear:both" />
<div ng-init="loadDataProductDetail();loadDataProducts();" class="schedule-detail">
	<div class="products">
		<div ng-show='!(DATA.current_product)'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		</div>
		
		<div ng-show='(DATA.current_product)'>
        	<div class="container lesspadding">
            	<div class="product row" style="border:none; background:none; padding:0">
                    <div class="col-xs-2 col">
                        <img ng-src="{{DATA.current_product.main_image}}" width="100%" />
                    </div>
                    <div class="col-xs-10 col">
                        <div class="desc">
                        	<div class="button-edit">
                                <div class="pull-right" style="padding-top:8px"> 
                                    <a href="" ng-click="publishUnpublishProduct(DATA.current_product, 0)" ng-show="DATA.current_product.publish_status == '1'">
                                        <div style="color:#090"><img src="<?=base_url("public/images/on-button.png")?>"></span> </div>
                                    </a> 
                                    <a href="" ng-click="publishUnpublishProduct(DATA.current_product, 1)" ng-show="DATA.current_product.publish_status != '1'">
                                        <div style="color:#F30"><img src="<?=base_url("public/images/off-button.png")?>"></span></div>
                                    </a> 
                                </div>
                            </div>
                            <div class="title">{{DATA.current_product.name}}</div>
                            <div class="code">Code : <strong>{{DATA.current_product.product_code}}</strong></div>
                            <div class="code">Show In : {{DATA.current_product.show_in_desc}}</div>
                            <?php /*?><div class="code">
                                Category : 
                                <span ng-show="DATA.current_product.category">
                                    <span ng-repeat='cat in DATA.current_product.category'><a href="">{{cat.name}}</a>, </span>
                                </span>
                                <span ng-show="!DATA.current_product.category"><em>-</em></span>
                            </div><?php */?>
                        </div>
                    </div>
        		</div>
            </div>
            <br />

			<ul class="nav nav-tabs sub-nav">
			  	<li role="presentation" class="description"><a ui-sref="activities.product_detail({'product_code':DATA.current_product.product_code})">Description</a></li>
			  	<li role="presentation" class="rates"><a ui-sref="activities.product_detail.rates">Rates</a></li>
			</ul>
			<br /><br />
			<div ui-view>
				<table class="table">
					<tr>
						<td width="120">Code</td>
						<td><strong>{{DATA.current_product.product_code}}</strong></td>
					</tr>
					<tr>
						<td>Product Name</td>
						<td><strong>{{DATA.current_product.name}}</strong></td>
					</tr>
					<?php /*?><tr>
						<td>Product Category</td>
						<td>
                        	<span ng-show="DATA.current_product.category">
	                        	<span ng-repeat='cat in DATA.current_product.category'><a href="">{{cat.name}}</a>, </span>
                            </span>
                            <span ng-show="!DATA.current_product.category">-</span>
                        </td>
					</tr><?php */?>
                    <tr>
						<td>Product Highlight</td>
						<td>
                        	<ol style="padding-left:20px">
                            	<li ng-repeat="highlight in DATA.current_product.highlight">{{highlight}}</li>
                            </ol>
                        </td>
					</tr>
                    <tr>
						<td>Short Description</td>
						<td>{{DATA.current_product.short_description}}</td>
					</tr>
                    <tr>
						<td>Full Description</td>
						<td>{{DATA.current_product.full_description}}</td>
					</tr>
                    <tr>
						<td>Show In</td>
						<td>{{DATA.current_product.show_in_desc}}</td>
					</tr>
				</table>
				<script>activate_sub_menu_schedule_detail("description");</script>
				<br /><hr />
				<a ui-sref="activities.product_edit({'product_code_edit':DATA.current_product.product_code})"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Product</a>
			</div>
		</div>
	</div>
</div>

<style>
	.schedule-detail .title{margin-bottom:20px}
	.schedule-detail .title h1{margin-bottom:10px !important;}
	.schedule-detail .title .code{margin-bottom:5px;}
</style>
<?php /*?><div class="sub-title"> Rates </div>
<br /><?php */?>

<div ng-init="loadMasterRates();loadInventoryProduct();" class="rates">

    <?php //INVENTORY----------------- ?>
    <div ng-show='!DATA.inventories'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		<br /><br />
	</div>
    
	<table class="table table-bordered table-condensed inventory-table" ng-show='DATA.inventories'>
    	<tr>
            <th colspan="{{DATA.inventories.calendar.length}}" style="text-align:left">
                <div class="pull-left" style="padding-top:7px" ng-show='DATA.inventories.inventory_model'>
                    <strong>Inventory : {{fn.newDate(DATA.inventories.start_date) | date:'dd MMMM yyyy'}} - {{fn.newDate(DATA.inventories.end_date) | date:'dd MMMM yyyy'}}</strong>
                </div>
                <div class="pull-right">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-sm" ng-click="prev_next_inventory('prev')"><span class="glyphicon glyphicon-backward"></span> Prev</button>
                        <button type="button" class="btn btn-default btn-sm" ng-click="prev_next_inventory('next')">Next <span class="glyphicon glyphicon-forward"></span></button>
                    </div>
                </div>
                
                <div class="input-group pull-right" style="width:200px; margin-right:10px">
                    <input type="text" class="form-control input-sm datepicker" style="text-align:center" ng-model='filter_date_search.str' />
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-success btn-sm" ng-click='loadInventoryProduct()'>OK</button>
                    </span>
                </div>
            </th>
        </tr>
        <tr style="background: #EEE;font-size: 11px;">
        	<td width="10%" ng-repeat='inventory in DATA.inventories.calendar' align="center">
            	{{fn.newDate(inventory.date) | date:'dd/MM'}}<br />
                {{fn.newDate(inventory.date) | date:'EEE'}}
            </td>
        </tr>
        <tr>
        	<td width="10%" ng-repeat='inventory in DATA.inventories.calendar' align="center" ng-class="{'danger':inventory.is_closed}" class="allotment"
            	ng-dblclick="addInventoryDblClickCellTable(inventory)">
            	<span ng-show="inventory.allotment">{{inventory.allotment}}</span>
                <span ng-show="!inventory.allotment">-</span>
            </td>
        </tr>
        <tr>
        	<td colspan="{{DATA.inventories.calendar.length}}" style="padding:0 0 10px 0">
            
                <table class="table table-condensed table-borderless" style="background:#F5F5F5; padding:10px">
                    <tbody>
                        <tr>
                            <td width="110">Inventory Model</td>
                            <td width="10">:</td>
                            <td>
                            	<div class="allotment-header">
                                    <div class="pull-right"> <a href="#" ng-click='change_inventory_model()' onClick="$('.allotment-header').hide(); $('.change-allotment-header').show(); return false;"><span class="glyphicon glyphicon-pencil"></span> Change Inventory Model</a> </div>
                                    
                                    <div style="margin-bottom:10px">
                                    	<div ng-show='DATA.inventories.inventory_model && !DATA.inventories.inventory_model_2'>
	                                    	<strong>{{DATA.inventories.inventory_model.name}}</strong>
                                        </div>
                                        
                                        
                                        <div ng-show='DATA.inventories.inventory_model_2'>
                                        	<ol>
		                                    	<li><strong>{{DATA.inventories.inventory_model.name}}</strong></li>
                                                <li><strong>{{DATA.inventories.inventory_model_2.name}}</strong></li>
                                                <li ng-show='DATA.inventories.inventory_model_3'><strong>{{DATA.inventories.inventory_model_3.name}}</strong></li>
                                            </ol>
                                        </div>
                                        
                                        
                                        <div ng-show='!DATA.inventories.inventory_model'>
                                        	<br /><br />
                                        	<div class="alert alert-warning">
                                            	<em><strong>Inventory</strong> not found, please select or create new inventory...</em>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div ng-show="DATA.inventories.affected_roduct">
                                        <hr style="margin:10px 0">
                                        <div style="margin-bottom:10px;"> <strong>Inventory Affiliate Products</strong>
                                            <ol>
                                                <li ng-repeat='pr in DATA.inventories.affected_roduct'>{{pr.name}}</li>
                                            </ol>
                                        </div>
                                	</div>
                                </div>
                                
                                <div class="change-allotment-header hidden-field">
                                    <form method="post" ng-submit='product_change_inventory_model($event)'>
                                        <div class="show_error"></div>
                                        
                                        <table class="table table-condensed table-borderless" style="background:none">
                                        	<tr>
                                            	<td>
                                                	<span ng-show='DATA.inventory_model_update.qty>1'>Inventory 1</span>
                                                	<select style="width:100%; margin-bottom:0" class="form-control input-sm inventory_name" ng-model='DATA.inventory_model_update.inventory_id' required>
                                                        <option ng-repeat='model in DATA.inventory_models.inventory_types' value="{{model.id}}">{{model.name}}</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr ng-hide='DATA.inventory_model_update.qty<2'>
                                            	<td>
                                                	<hr style="margin:0px 0 5px 0" />
                                                	Inventory 2 <a href="" title="Remove" style="color:red" class="pull-right" ng-show='DATA.inventory_model_update.qty==2' ng-click='DATA.inventory_model_update.qty=DATA.inventory_model_update.qty-1'><span class="glyphicon glyphicon-trash"></span></a>
                                                	<select style="width:100%; margin-bottom:0" class="form-control input-sm inventory_name" ng-model='DATA.inventory_model_update.inventory_id_2' ng-disabled="DATA.inventory_model_update.qty<2" required>
                                                    	<option value=""></option>
                                                        <option ng-repeat='model in DATA.inventory_models.inventory_types' value="{{model.id}}">{{model.name}}</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr ng-hide='DATA.inventory_model_update.qty<3'>
                                            	<td>
                                                	<hr style="margin:0px 0 5px 0" />
                                                	Inventory 3 <a href="" title="Remove" style="color:red" class="pull-right" ng-show='DATA.inventory_model_update.qty==3' ng-click='DATA.inventory_model_update.qty=DATA.inventory_model_update.qty-1'><span class="glyphicon glyphicon-trash"></span></a>
                                                	<select style="width:100%; margin-bottom:0" class="form-control input-sm inventory_name" ng-model='DATA.inventory_model_update.inventory_id_3' ng-disabled="DATA.inventory_model_update.qty<3" required>
                                                    	<option value=""></option>
                                                        <option ng-repeat='model in DATA.inventory_models.inventory_types' value="{{model.id}}">{{model.name}}</option>
                                                    </select>
                                                </td>
                                            </tr>
                                        </table>
                                        <div ng-hide='DATA.inventory_model_update.qty>=3'>
                                            <br />
                                            <a href="" ng-click='DATA.inventory_model_update.qty=DATA.inventory_model_update.qty+1'><span class="glyphicon glyphicon-plus"></span>Add Inventory</a>
                                        </div>
                                        <hr />
                                        <button type="submit" class="btn btn-sm btn-info">Save</button>
                                        &nbsp;&nbsp; <a href="#" onClick="$('.allotment-header').show(); $('.change-allotment-header').hide(); return false;">Cancel</a> <br>
                                        <hr>
                                        <a href="" data-toggle="modal" ng-click='addInventoryModel()' data-target="#inventory-new-inventory-model"><span class="glyphicon glyphicon-plus"></span> New Inventory Model</a>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
				
                <div align="center" style="margin-top: 10px; display: block;" class="allotment-header" ng-show='DATA.inventories.inventory_model && !DATA.inventories.inventory_model_2'>
                    <?php /*?><button type="button" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-calendar"></span> Inventories Calendar</button><?php */?>
                    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" ng-click='addInventoryUpdateAllotment()' data-target="#inventory-update-allotment"><span class="glyphicon glyphicon-plus"></span> Setup Inventory</button>
                    <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" ng-click='addInventoryOpenCloseDate()' data-target="#inventory-open-close-date"><span class="glyphicon glyphicon-plus"></span> Close/Open Date</button>
                </div>
            
            </td>
        </tr>
    </table>
	<?php //-- END INVENTORY----------------- ?>
	
    <br /><hr /><br />

    <?php //RATES----------------- ?>
	<div ng-show='!DATA.rates'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		<br /><br />
	</div>

	<div ng-show='DATA.rates && !DATA.rates.rates'>
    	<div class="alert alert-warning">
            <em><strong>Rates</strong> not found, please setup new rates...</em>
        </div>
    </div>
	<div ng-show='DATA.rates.rates'>
        <table class="table table-bordered table-condensed product-rates-list">
            <tr style="background:#F5F5F5">
                <td width="50%"><strong>Rates Variation</strong></td>
                <td width="50%"><strong>Rates</strong></td>
            </tr>
        </table>
        <form id="sortable-rates" ng-submit="saveReorderRates();" onSubmit="return false;">
            <div ng-repeat="rates in DATA.rates.rates">
            <input type='hidden' class='rates_codes' name='rates_codes[]' ng-value='rates.rates_code' />
                <table class="table table-bordered table-condensed product-rates-list">
                    <tr ng-class="{'danger':rates.publish_status!='1'}" style="background:#FFF">
                        <td width="50%">
                            <a href="" onclick="$(this).closest('tr').next().toggle();" ng-click='loadMasterRatesDetailForRatesList(rates);'><strong>{{rates.rates_code}} - {{rates.name}}</strong></a><br />
                            <div ng-show="rates.rates_for.walkin || rates.rates_for.offline">
                                <small>From <strong>{{fn.formatDate(rates.start_date, "dd M yy")}}</strong> to <strong>{{fn.formatDate(rates.end_date, "dd M yy")}}</strong></small>
                            </div>
                            
                            <div>
                                <span class="label label-danger" ng-show='rates.rates_for.offline'>Offline Booking</span>
                                <span class="label label-warning" ng-show='rates.rates_for.walkin'>Walk In Booking</span>
                                <span class="label label-info" ng-show='rates.rates_for.agent'>Agencies and Colleague</span>
                            </div>
                            
                            <div class="pull-right" ng-show="rates.use_smart_pricing == 'YES'">
                                <small title="Smart Pricing">
                                    <span class="label label-success">
                                        <span class="glyphicon glyphicon-signal"></span> Smart Pricing
                                    </span>
                                </small>
                            </div>
                        </td>
                        <td class="hover-to-show" width="50%">
                            <div ng-show="!rates.is_packages">
                                <div ng-show="rates.rates_edit">
                                    <form ng-submit='ratesEditInlineSave($event, rates)'>
                                        <div ng-show='rates.error_desc.length>0' class="alert alert-danger"><ul><li ng-repeat='err in rates.error_desc'>{{err}}</li></ul></div>
                                        <table class="xtable xtable-condensed xtable-borderless" width="100%">
                                            <tr>
                                                <td width="100">{{rates.rates_caption.rates_1}}</td>
                                                <td>
                                                    <div class="input-group" style="width:200px">
                                                        <span class="input-group-addon" id="basic-addon1">{{rates.currency}}</span>
                                                        <input min="0" type="number" class="form-control input-sm" placeholder="Rates {{rates.rates_caption.rates_1}}" aria-describedby="basic-addon1" 
                                                            ng-model='rates.rates_edit.rates_1' />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr ng-show='rates.rates_caption.rates_2'>
                                                <td>{{rates.rates_caption.rates_2}}</td>
                                                <td>
                                                    <div class="input-group" style="width:200px">
                                                        <span class="input-group-addon" id="basic-addon1">{{rates.currency}}</span>
                                                        <input min="0" type="number" class="form-control input-sm" placeholder="Rates {{rates.rates_caption.rates_2}}" aria-describedby="basic-addon1" 
                                                            ng-model='rates.rates_edit.rates_2' ng-disabled="!rates.rates_caption.rates_2" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr ng-show='rates.rates_caption.rates_3'>
                                                <td>{{rates.rates_caption.rates_3}}</td>
                                                <td>
                                                    <div class="input-group" style="width:200px">
                                                        <span class="input-group-addon" id="basic-addon1">{{rates.currency}}</span>
                                                        <input min="0" type="number" class="form-control input-sm" placeholder="Rates {{rates.rates_caption.rates_3}}" aria-describedby="basic-addon1" 
                                                            ng-model='rates.rates_edit.rates_3' ng-disabled="!rates.rates_caption.rates_3" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <button type="submit" class="btn btn-sm btn-primary">Save</button>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="" style="color:red" ng-click='rates.rates_edit=false'><span class="glyphicon glyphicon-remove"></span> Cancel</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </form>
                                </div>
                                <div ng-show="!rates.rates_edit">
                                    <div class="pull-right hover-to-show-div">
                                        <a href="" ng-click='ratesEditInline(rates)'><span class="glyphicon glyphicon-pencil"></span> Edit</a>
                                        &nbsp;&nbsp;&nbsp; <span class="glyphicon glyphicon-sort handle"></span>
                                    </div>
                                
                                    <div>
                                        {{rates.rates_caption.rates_1}} : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.rates_1, rates.currency)}}</strong>
                                    </div>
                                    <div ng-show='rates.rates_caption.rates_2'>
                                        {{rates.rates_caption.rates_2}} : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.rates_2, rates.currency)}}</strong>
                                    </div>
                                    <div ng-show='rates.rates_caption.rates_3'>
                                        {{rates.rates_caption.rates_3}} : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.rates_3, rates.currency)}}</strong>
                                    </div>
                                </div>
                            </div>
                            <div ng-show='rates.is_packages'>
                                <div class="pull-right hover-to-show-div">
                                    <span class="glyphicon glyphicon-sort handle"></span>
                                </div>
                                <strong>Packages:</strong>
                                <ul ng-show='rates.packages_option' style="padding-left:20px">
                                    <li ng-repeat="packages_option in rates.packages_option" style="margin-bottom:5px">
                                        <div>
                                            <strong>{{packages_option.caption}}</strong>
                                        </div>
                                        <div>
                                            Price : <strong>{{rates.currency}} {{fn.formatNumber(packages_option.rates, rates.currency)}}</strong> 
                                            <?php /*?><small>/{{packages_option.rates_unit}}</small><?php */?>
                                            <span class="glyphicon glyphicon-menu-right"></span>
                                            <span ng-show="packages_option.include_pax.opt_1">{{packages_option.include_pax.opt_1}} Adult &nbsp;</span>
                                            <span ng-show="packages_option.include_pax.opt_2">{{packages_option.include_pax.opt_2}} Child &nbsp;</span>
                                            <span ng-show="packages_option.include_pax.opt_3">{{packages_option.include_pax.opt_3}} infant &nbsp;</span>
                                        </div>
                                        <div ng-show="packages_option.extra_rates.opt_1 || packages_option.extra_rates.opt_2 || packages_option.extra_rates.opt_3">
                                            Extra :
                                            <span ng-show="packages_option.extra_rates.opt_1">Adult : <strong>{{rates.currency}} {{fn.formatNumber(packages_option.extra_rates.opt_1, rates.currency)}}</strong> &nbsp;</span>
                                            <span ng-show="packages_option.extra_rates.opt_2">Child : <strong>{{rates.currency}} {{fn.formatNumber(packages_option.extra_rates.opt_2, rates.currency)}}</strong> &nbsp;</span>
                                            <span ng-show="packages_option.extra_rates.opt_3">Infant : <strong>{{rates.currency}} {{fn.formatNumber(packages_option.extra_rates.opt_3, rates.currency)}}</strong> &nbsp;</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <tr ng-class="{'danger':rates.publish_status!='1'}" class="hidden-field">
                        <td colspan="2">
                            <table class="table table-borderless table-condensed" style="background:none">
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <div class="pull-right"> 
                                                <a href="" ng-click="publishUnpublishProductRates(rates, 0)" ng-show="rates.publish_status == '1'">
                                                    <div style="color:#090"><img src="<?=base_url("public/images/on-button.png")?>"></span> </div>
                                                </a> 
                                                <a href="" ng-click="publishUnpublishProductRates(rates, 1)" ng-show="rates.publish_status != '1'">
                                                    <div style="color:#F30"><img src="<?=base_url("public/images/off-button.png")?>"></span></div>
                                                </a> 
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100">Code</td>
                                        <td><strong>{{rates.rates_code}}</strong></td>
                                    </tr>
                                    <?php /*?><tr>
                                        <td>Booking Handling</td>
                                        <td>{{rates.booking_handling}}</td>
                                    </tr><?php */?>
                                    <tr>
                                        <td width="140">Available On</td>
                                        <td>
                                            <div class="btn-group btn-days">
                                                <button type="button" class="btn btn-xs" ng-class="{'btn-info':rates.available_on == 'all_day' || rates.available_on.indexOf('mon') >= 0}">Mon</button>
                                                <button type="button" class="btn btn-xs" ng-class="{'btn-info':rates.available_on == 'all_day' || rates.available_on.indexOf('tue') >= 0}">Tue</button>
                                                <button type="button" class="btn btn-xs" ng-class="{'btn-info':rates.available_on == 'all_day' || rates.available_on.indexOf('wed') >= 0}">Wed</button>
                                                <button type="button" class="btn btn-xs" ng-class="{'btn-info':rates.available_on == 'all_day' || rates.available_on.indexOf('thu') >= 0}">Thu</button>
                                                <button type="button" class="btn btn-xs" ng-class="{'btn-info':rates.available_on == 'all_day' || rates.available_on.indexOf('fri') >= 0}">Fri</button>
                                                <button type="button" class="btn btn-xs" ng-class="{'btn-info':rates.available_on == 'all_day' || rates.available_on.indexOf('sat') >= 0}">Sat</button>
                                                <button type="button" class="btn btn-xs" ng-class="{'btn-info':rates.available_on == 'all_day' || rates.available_on.indexOf('sun') >= 0}">Sun</button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr ng-show="rates.rates_for.walkin || rates.rates_for.offline">
                                        <td>Range of dates</td>
                                        <td><strong>{{fn.formatDate(rates.start_date, "d MM yy")}}</strong> to <strong>{{fn.formatDate(rates.end_date, "d MM yy")}}</strong></td>
                                    </tr>
                                    <?php /*?><tr>
                                        <td>Auto Discount</td>
                                        <td> IDR 100.000 </td>
                                    </tr><?php */?>
                                    <tr>
                                        <td>Duration</td>
                                        <td>{{rates.duration}} {{rates.duration_unit_desc}}</td>
                                    </tr>
                                    <tr>
                                        <td>Cut Off Booking</td>
                                        <td>{{rates.cut_of_booking}} days in advance</td>
                                    </tr>
                                    <tr>
                                        <td>Minimum Order</td>
                                        <td>{{rates.min_order}} Pax</td>
                                    </tr>
                                    <tr>
                                        <td> Pickup Service </td>
                                        <td> <span class="text-uppercase">{{rates.pickup_service}}</span> </td>
                                    </tr>
                                    <tr ng-show="rates.pickup_service=='yes' && rates.pickup_area">
                                        <td> Pick Up Area </td>
                                        <td><ul style="margin:0; padding-left:15px">
                                                <li ng-repeat='area in rates.pickup_area'> 
                                                    {{area.area}} - {{area.time}} <strong ng-show='area.price > 0'> - {{area.currency}} {{fn.formatNumber(area.price, area.currency)}} / {{area.type}}</strong> 
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td> Drop Off Service </td>
                                        <td> <span class="text-uppercase">{{rates.dropoff_service}}</span> </td>
                                    </tr>
                                    <tr ng-show="rates.dropoff_service=='yes' && rates.dropoff_area">
                                        <td> Dropoff Area </td>
                                        <td>
                                            <ul style="margin:0; padding-left:15px">
                                                <li ng-repeat='area in rates.dropoff_area'> 
                                                    {{area.area}} - {{area.time}} <strong ng-show='area.price > 0'> - {{area.currency}} {{fn.formatNumber(area.price, area.currency)}} / {{area.type}}</strong> 
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td>Inclusions</td>
                                        <td>
                                            <ul style="margin:0; padding-left:15px" ng-show='rates.inclusion'>
                                                <li ng-repeat='str in rates.inclusion'>{{str}}</li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Exclusions</td>
                                        <td>
                                            <ul style="margin:0; padding-left:15px" ng-show='rates.exclusion'>
                                                <li ng-repeat='str in rates.exclusion'>{{str}}</li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Additional info</td>
                                        <td>
                                            <ul style="margin:0; padding-left:15px" ng-show='rates.additional_info'>
                                                <li ng-repeat='str in rates.additional_info'>{{str}}</li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr ng-show='rates.additional_charge'>
                                        <td>Additional Charge</td>
                                        <td>
                                            <ul style="margin:0; padding-left:15px" ng-show='rates.additional_charge'>
                                                <li ng-repeat='item in rates.additional_charge'>
                                                    {{item.name}} - <strong>{{item.currency}} {{fn.formatNumber(item.price, item.currency)}} </strong>
                                                    / {{item.unit}}
                                                    <?php /*?><div ng-show='item.description'>
                                                        <small>{{item.description}}</small>
                                                    </div><?php */?>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr style="margin:10px 0">
                            <div align="right">
                                <a ui-sref="activities.product_detail.edit_rates({'rates_code':rates.rates_code})"><span class="glyphicon glyphicon-pencil"></span> Edit Rates</a>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <?php /*?><a ui-sref="transport.trips_schedule_detail.smart_pricing({'rates_code':rates.rates_code})"><span class="glyphicon glyphicon-signal"></span> Smart Pricing</a>
                                &nbsp;&nbsp;&nbsp;&nbsp;<?php */?>
                                <a href="" ng-click="duplicateProductRates(rates)"><span class="glyphicon glyphicon-duplicate"></span>  Duplicate Rates</a>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="" style="color:red" ng-click="deleteProductRates(rates)"><span class="glyphicon glyphicon-trash"></span> </a>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </form>
	</div>
	
    <br /><br />
	<div class="add-product-button"> <a ui-sref="activities.product_detail.new_rates" class="btn btn-success btn-lg btn-block" > <span class="glyphicon glyphicon-plus"></span> New Rates </a> </div>
	
	<?php //-- END RATES----------------- ?>
    
	<script>
		activate_sub_menu_schedule_detail("rates");
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
    </script>
    
    <style>
    	table.inventory-table .allotment.danger{background:red !important; color:#FFF !important}
		table.inventory-table .allotment:hover{background:#1B77C0 !important; color:#FFF !important; cursor:pointer}
    </style>
    
    <?php //================================================================================================== ?>
    
    <?php //MODAL UNTUK CLOSE & OPEN DATE ?>
    <div class="modal fade" id="inventory-open-close-date" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <form ng-submit='saveInventoryOpenCloseDate($event)'>
	  <div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">
				Open Close Date
			</h4>
		  </div>
		  <div class="modal-body">
		  	<div ng-show='DATA.inv_open_close_date.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.inv_open_close_date.error_msg'>{{err}}</li></ul></div>
			<table class="table table-borderless table-condensed">
				<tr>
					<td width="90">Start date*</td>
					<td><input placeholder="Start Date" required="required" type="text" class="form-control input-md openCloseDateDatepicker" ng-model='DATA.inv_open_close_date.start_date' /></td>
				</tr>
				<tr>
					<td>End date*</td>
					<td><input placeholder="End Date" required="required" type="text" class="form-control input-md openCloseDateDatepicker" ng-model='DATA.inv_open_close_date.end_date' /></td>
				</tr>
                <tr>
					<td>Status*</td>
					<td>
                    	<select required="required" class="form-control input-md" ng-model='DATA.inv_open_close_date.status'>
                        	<option value="" disabled="disabled">--Status--</option>
                        	<option value="close">Close Date</option>
                            <option value="open">Open Date</option>
                        </select>
                    </td>
				</tr>
			</table>
            
            <div ng-show="DATA.inventories.affected_roduct">
                <hr style="margin:10px 0">
                <div style="margin-bottom:10px;"> <strong>Inventory Affiliate Products</strong>
                    <ol>
                        <li ng-repeat='pr in DATA.inventories.affected_roduct'>{{pr.name}}</li>
                    </ol>
                </div>
            </div>
		  </div>
		  <div class="modal-footer" style="text-align:center">
		  	<button type="submit" class="btn btn-primary">Save</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
	  </div>
	  </form>
	</div>
    <?php //--- ?>
    
    <?php //MODAL UNTUK UPDATE ALLOTMENT ?>
    <div class="modal fade" id="inventory-update-allotment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <form ng-submit='saveInventoryUpdateAllotment($event)'>
	  <div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">
				Inventory Model
			</h4>
		  </div>
		  <div class="modal-body">
		  	<div ng-show='DATA.inv_update_allotment.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.inv_update_allotment.error_msg'>{{err}}</li></ul></div>
			<table class="table table-borderless table-condensed">
				<tr>
                	<td>Inventory Model</td>
                    <td><strong>{{DATA.inventories.inventory_model.name}}</strong></td>
                </tr>
                <tr>
					<td width="120">Start date*</td>
					<td><input placeholder="Start Date" required="required" type="text" class="form-control input-md updateAllotmentDatepicker" ng-model='DATA.inv_update_allotment.start_date' style="width:150px;" /></td>
				</tr>
				<tr>
					<td>End date*</td>
					<td><input placeholder="End Date" required="required" type="text" class="form-control input-md updateAllotmentDatepicker" ng-model='DATA.inv_update_allotment.end_date' style="width:150px;" /></td>
				</tr>
                <tr>
					<td>Inventory*</td>
					<td><input placeholder="Inventory" type="number" min="0" step="1" required="required" class="form-control input-md" ng-model='DATA.inv_update_allotment.inventory' style="width:100px;" /></td>
				</tr>
			</table>
            
            <div ng-show="DATA.inventories.affected_roduct">
                <hr style="margin:10px 0">
                <div style="margin-bottom:10px;"> <strong>Inventory Affiliate Products</strong>
                    <ol>
                        <li ng-repeat='pr in DATA.inventories.affected_roduct'>{{pr.name}}</li>
                    </ol>
                </div>
            </div>
		  </div>
		  <div class="modal-footer" style="text-align:center">
		  	<button type="submit" class="btn btn-primary">Save</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
	  </div>
	  </form>
	</div>
    <?php //--- ?>
    
    <?php //MODAL UNTUK NEW INVENTORY ?>
    <div class="modal fade" id="inventory-new-inventory-model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <form ng-submit='saveInventoryModel($event)'>
	  <div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">
				Inventory Model
			</h4>
		  </div>
		  <div class="modal-body">
		  	<div ng-show='DATA.inv_inventory_model.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.inv_inventory_model.error_msg'>{{err}}</li></ul></div>
			<table class="table table-borderless table-condensed">
				<tr>
                	<td>Inventory Model</td>
                    <td><input placeholder="Inventory Name" required="required" type="text" class="form-control input-md" ng-model='DATA.inv_inventory_model.inventory_name' /></td>
                </tr>
                <tr>
					<td width="120">Start date*</td>
					<td><input placeholder="Start Date" required="required" type="text" class="form-control input-md inventoryModelDatepicker" ng-model='DATA.inv_inventory_model.start_date' style="width:150px;" /></td>
				</tr>
				<tr>
					<td>End date*</td>
					<td><input placeholder="End Date" required="required" type="text" class="form-control input-md inventoryModelDatepicker" ng-model='DATA.inv_inventory_model.end_date' style="width:150px;" /></td>
				</tr>
                <tr>
					<td>Inventory*</td>
					<td><input placeholder="Inventory" type="number" min="0" step="1" required="required" class="form-control input-md" ng-model='DATA.inv_inventory_model.inventory' style="width:100px;" /></td>
				</tr>
			</table>
            
		  </div>
		  <div class="modal-footer" style="text-align:center">
		  	<button type="submit" class="btn btn-primary">Save</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
	  </div>
	  </form>
	</div>
    <?php //--- ?>
    
</div>

<style>
	.hover-to-show .hover-to-show-div{display:none}
	.hover-to-show:hover .hover-to-show-div{display:inline}
    span.handle{cursor:pointer}
</style>
<script>
    $( function() {
        $( "#sortable-rates" ).sortable({
            handle: ".handle",
			revert:false,
			stop: function(){
                $( "#sortable-rates" ).submit();
				//var form = $("#sortable-rates");
                //console.log(form.serializeArray());
				//$.ajax({url : form.attr("action"),  data: form.serialize(), type: "POST", success: function(e) {/*alert(e);*/} });
			}
        });
    } );
</script>
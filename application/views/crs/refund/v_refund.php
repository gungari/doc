<h1>Refund</h1> 

<div ui-view>
	<div ng-init="loadDataTefund()">
		
        <div class="products">
			<div class="product">
				<form ng-submit='loadDataTefund()'>
					<div class="table-responsive">
						<table class="table table-condensed table-borderless" width="100%">
							<tr>
								<td width="100">From</td>
								<td width="100">To</td>
								<td width="200">Search</td>
								<?php /*?><td width="130">Source</td>
								<td width="130">Payment</td><?php */?>
                                <td width="130">Status</td>
								<td></td>
							</tr>
							<tr>
								<td><input type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='search.start_date' /></td>
								<td><input type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='search.end_date' /></td>
								<td><input type="text" class="form-control input-sm" placeholder="Search" ng-model='search.q' /></td>
								<?php /*?><td>
									<select class="form-control input-sm" ng-model='search.booking_source'>
										<option value="">All</option>
										<option value="{{booking_source.code}}" ng-repeat='booking_source in $root.DATA_booking_source'>{{booking_source.name}}</option>
									</select>
								</td>
								<td>
									<select class="form-control input-sm" ng-model='search.payment_method'>
										<option value="">All</option>
										<option value="{{payment_method.code}}" ng-repeat='payment_method in $root.DATA_payment_method' ng-show="payment_method.code!='OPENVOUCHER'">{{payment_method.name}}</option>
									</select>
								</td><?php */?>
								<td>
									<select class="form-control input-sm" ng-model='search.refund_status'>
										<option value="">All</option>
										<option value="1">Refunded</option>
                                        <option value="0">Pending</option>
									</select>
								</td>
								<td><button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button></td>
							</tr>
						</table>
					</div>	
				</form>
			</div>
		</div>
		
		<div ng-show='show_loading_DATA_bookings'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		</div>
        
		<?php /*?><div class="products">
			<div class="product">
				Filter : <input type="text" ng-model="filter_refund" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
			</div>
		</div>
		
		<div ng-show='show_loading_DATA_bookings'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		</div><?php */?>
		
		<div ng-show='DATA.refund'>
			<div ng-show='!show_loading_DATA_bookings'>
				<div class="table-responsive">
					<table class="table table-condensed table-bordered">
						<tr class="header bold">
							<td width="110" align="center">Refund Code</td>
							<td width="90" align="center">Refund Date</td>
							<td width="90" align="center">Order#</td>
							<td>Description</td>
							<td width="70" align="center">Status</td>
							<td width="110" align="right">Refund Amount</td>
                            <td width="110" align="right">Funds Resource</td>
						</tr>
						<tbody ng-repeat="refund in DATA.refund.refunds | filter : filter_refund">
							<tr>
								<td align="center">
									<span ng-show="refund.status=='1'">
										<strong>{{refund.refund_code}}</strong>
									</span>
									<span ng-show="refund.status!='1'">
										<a href="" data-toggle="modal" data-target="#refund-detail-form" ng-click="editDataRefund(refund)">
											<strong>{{refund.refund_code}}</strong>
										</a>
									</span>
								</td>
								<td align="center">{{fn.formatDate(refund.request_date, "dd M yy")}}</td>
								<td align="center">
									<?php /*?><a ui-sref="trans_reservation.detail({'booking_code':refund.booking_code})" target="_blank"><strong>{{refund.booking_code}}</strong></a><?php */?>
                                    <a ui-sref="reservation.detail({'booking_code':refund.booking_code})" target="_blank"><strong>{{refund.booking_code}}</strong></a>
								</td>
								<td>{{refund.description}}</td>
								<td align="center">
									<span class="label label-warning" ng-show="refund.status!='1'">Pending</span>
									<span class="label label-success" ng-show="refund.status=='1'">Refunded</span>
								</td>
								<td align="right" width="110">
									{{refund.currency}}
									{{fn.formatNumber(refund.refund_amount, refund.currency)}}
								</td>
                                <td>{{refund.source_of_funds}}</td>
							</tr>
							<tr>
								<td colspan="7" style="background:#FAFAFA"></td>
							</tr>
						</tbody>
					</table>
				</div>
                
                <nav aria-label="Page navigation" class="pull-right">
                  <ul class="pagination pagination-sm">
                    <li ng-class="{'disabled':DATA.refund.search.page <= 1}">
                      <a href="" ng-click='loadDataTefund(DATA.refund.search.page-1, true)' aria-label="Prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                    </li>
                    <li ng-repeat='pagination in DATA.refund.search.pagination' ng-class="{'active':DATA.refund.search.page == pagination}">
                        <a href="" ng-click='loadDataTefund(($index+1), true)'>{{($index+1)}}</a>
                    </li>
                    <li ng-class="{'disabled':DATA.refund.search.page >= DATA.refund.search.number_of_pages}">
                      <a href="" ng-click='loadDataTefund(DATA.refund.search.page+1, true)' aria-label="Next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                    </li>
                  </ul>
                </nav>
                <div class="clearfix"></div>
                
			</div>
			
			<div class="modal fade" id="refund-detail-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  	<form ng-submit='saveRefunds($event)'>
					<div class="modal-dialog" role="document">
						<div class="modal-content">
						  <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">
								Refund Booking #{{DATA.myRefund.booking_code}}
							</h4>
						  </div>
						  <div class="modal-body">
							<div ng-show='DATA.myRefund.error_desc.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myRefund.error_desc'>{{err}}</li></ul></div>
							<table class="table table-borderless table-condensed">
                            	<tbody ng-show="DATA.myRefund.booking_info" style="border:none">
                                    <tr>
                                        <td>Order#</td>
                                        <td>
                                        	<a ui-sref="reservation.detail({'booking_code':DATA.myRefund.booking_code})" target="_blank"><strong>#{{DATA.myRefund.booking_code}}</strong></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Booking Date</td>
                                        <td><strong>{{fn.newDate(DATA.myRefund.booking_info.transaction_date) | date : 'dd MMMM yyyy HH:mm'}}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>Booking Status</td>
                                        <td class="text-capitalize">
                                            <strong>
                                                <span ng-class="{'label label-danger':DATA.myRefund.booking_info.status_code == 'CANCEL', 'label label-warning':DATA.myRefund.booking_info.status_code == 'UNDEFINITE', 'label label-default':DATA.myRefund.booking_info.status_code == 'VOID'}">
                                                    {{DATA.myRefund.booking_info.status.toLowerCase()}}
                                                </span>
                                            </strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Booking Source</td>
                                        <td class="text-capitalize"><strong>{{DATA.myRefund.booking_info.source.toLowerCase()}}</strong></td>
                                    </tr>
                                    <tr ng-show="DATA.myRefund.booking_info.agent">
                                        <td>Agent</td>
                                        <td class="text-capitalize">
                                            <a ui-sref="agent.detail({'agent_code':DATA.myRefund.booking_info.agent.agent_code})" target="_blank">
                                                <strong>{{DATA.myRefund.booking_info.agent.name}}</strong>
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                                <tbody style="border:none">
                                    <tr>
                                        <td>Refund Code</td>
                                        <td>{{DATA.myRefund.refund_code}}</td>
                                    </tr>
                                    <tr>
                                        <td width="130">Refund Amount</td>
                                        <td>
                                            <strong>{{DATA.myRefund.currency}} {{fn.formatNumber(DATA.myRefund.refund_amount,DATA.myRefund.currency)}}</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Remarks</td>
                                        <td>{{DATA.myRefund.description}}</td>
                                    </tr>
                                    <tr>
                                        <td>Status*</td>
                                        <td>
                                            <select class="form-control input-md" ng-model="DATA.myRefund.approval_status" style="width:150px">
                                                <option value="1">Approve</option>
                                                <option value="0">Reject</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr ng-show="DATA.myRefund.approval_status=='1' && DATA.myRefund.booking_info.agent">
                                        <td></td>
                                        <td>
                                            <label>
                                                <input type="checkbox" ng-true-value='1' ng-false-value='0' ng-model='DATA.myRefund.refund_to_deposit' ng-disabled="!DATA.myRefund.booking_info.agent" /> 
                                                Refund to Agent Deposit ({{DATA.myRefund.booking_info.agent.name}})
                                            </label>
                                        </td>
                                    </tr>
                                    <tr ng-show="DATA.myRefund.approval_status=='1' && DATA.myRefund.refund_to_deposit != '1'">
                                        <td>Funds Resource*</td>
                                        <td>
                                            <select ng-required="DATA.myRefund.approval_status=='1' && DATA.myRefund.refund_to_deposit != '1'" class="form-control input-md" ng-model='DATA.myRefund.payment_method_code'>
                                                <option value="" disabled="disabled"> -- Funds Resource --</option>
                                                <option ng-repeat="payment_method in DATA_payment_method_out | orderBy : 'name'" value="{{payment_method.code}}" ng-show="payment_method.code != 'OPENVOUCHER' && payment_method.code != 'ONLINE'">
                                                    {{payment_method.name}}
                                                </option>
                                            </select>
                                            <div ng-hide='DATA_payment_method_out.length>0' class="alert alert-warning" style="margin-top:10px">
                                                Commission Redeem Cannot processed, 
                                                there are no cash out account set up, 
                                                please refer to following <a href="#/setting/cash_and_bank" target="_blank">link</a> to setup cash out account.
                                            </div>
                                            <?php /*?><input type="text" ng-required="DATA.myRefund.approval_status=='1'" class="form-control input-md" placeholder="Ex.: Account# | Bank BCA" ng-model='DATA.myRefund.source_of_funds' /><?php */?>
                                        </td>
                                    </tr>
                                </tbody>
								<?php /*?><tr>
									<td width="130">Remarks</td>
									<td>
										<textarea placeholder="Description" required="required" type="text" class="form-control input-md autoheight" ng-model='DATA.myRefund.remarks' rows="3"></textarea>
									</td>
								</tr><?php */?>
							</table>
                            
						  </div>
						  <div class="modal-footer" style="text-align:center">
							<button type="submit" class="btn btn-primary">Save</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						  </div>
						</div>
					</div>
				</form>
			</div>
		</div>
		
	</div>
</div>
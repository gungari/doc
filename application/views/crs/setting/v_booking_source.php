<div class="sub-title">Booking Source</div>

<div ng-init="loadDataBookingSource()">
 	<div ng-show="DATA.status=='SUCCESS'">
		<div class="products">
			<div class="product">
				Filter : <input type="text" ng-model="filter_booking_source" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
			</div>
			
			<div ng-show='!DATA.booking_source'>
				<img src="<?=base_url("public/images/loading_bar.gif")?>" />
			</div>
			<table class="table table-bordered" xng-show='DATA.booking_source'>
				<tr class="info" style="font-weight:bold">
					<td colspan="2">Name</td>
					<td width="90"></td>
				</tr>
                <tbody ng-repeat="booking_source in DATA.booking_source | filter : filter_booking_source">
                    <tr>
                        <td colspan="2">
                        	<div class="pull-right" ng-show="booking_source.code == 'OFFLINE'">
                                <a href="" data-toggle="modal" ng-click='addEditBookingSource()' data-target="#add-edit-booking-source">
                                <button type="button" class="btn btn-xs btn-default">
                                    <span class="glyphicon glyphicon-plus"></span> Add
                                </button>
                                </a>
                            </div>
                        	<strong>{{booking_source.name}}</strong>
                        </td>
                        <td align="center"></td>
                    </tr>
                    <tr ng-show='booking_source.sub' ng-repeat="booking_source_sub in booking_source.sub | filter : filter_booking_source" ng-class="{'danger':booking_source_sub.publish_status=='0'}">
                    	<td width="40" align="right">{{($index+1)}}</td>
                        <td>
                            <a href="" data-toggle="modal" ng-click='addEditBookingSource(booking_source_sub)' data-target="#add-edit-booking-source">
                                {{booking_source_sub.name}}
                            </a>
                        </td>
                        <td align="center">
                        	<a href="" ng-click="publishUnpublishBookingSource(booking_source_sub, 0)" ng-show="booking_source_sub.publish_status == '1'">
                                <div style="color:#090"><img src="<?=base_url("public/images/on-button.png")?>" width="100%"></span> </div>
                            </a> 
                            <a href="" ng-click="publishUnpublishBookingSource(booking_source_sub, 1)" ng-show="booking_source_sub.publish_status != '1'">
                                <div style="color:#F30"><img src="<?=base_url("public/images/off-button.png")?>" width="100%"></span></div>
                            </a> 
                        </td>
                    </tr>
                </tbody>
			</table>
		</div>
	</div>
	<?php /*?><hr>
	<div class="add-product-button"> <a href="" class="btn btn-success btn-lg btn-block" data-toggle="modal" ng-click='addEditAccessRole()' data-target="#add-edit-access-role"> <span class="glyphicon glyphicon-plus"></span> New Access Role </a> </div><?php */?>
	<br>
	<br>
	
	<div class="modal fade" id="add-edit-booking-source" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <form ng-submit='saveDataBookingSource($event)'>
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">
				<span ng-show='!DATA.myBookingSource.code'>Add</span><span ng-show='DATA.myBookingSource.code'>Edit</span> Booking Source
			</h4>
		  </div>
		  <div class="modal-body">
			<div ng-show='DATA.myBookingSource.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myBookingSource.error_msg'>{{err}}</li></ul></div>
			<table class="table table-borderless table-condensed">
				<tr>
					<td width="110">Name*</td>
					<td><input placeholder="Name" required="required" type="text" class="form-control input-md" ng-model='DATA.myBookingSource.name' /></td>
				</tr>
				<?php /*?><tr>
					<td>Description</td>
					<td><input placeholder="Description" type="text" class="form-control input-md" ng-model='DATA.myAccessRole.description' /></td>
				</tr><?php */?>
			</table>
		  </div>
		  <div class="modal-footer" style="text-align:center">
			<button type="submit" class="btn btn-primary">Save</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
	  </div>
	  </form>
	</div>
	
</div>
</div>
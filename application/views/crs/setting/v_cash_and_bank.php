<div class="sub-title">Cash And Bank</div>

<div ui-view>
	<div ng-init="loadDataCashBank()">
		
		<div class="products">
			<div class="product">
				Filter : <input type="text" ng-model="filter_cashbank" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
			</div>
		</div>

		<div ng-show='show_loading_DATA_bookings'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		</div>
		
		<div ng-show='DATA.account'>
			<div ng-show='!show_loading_DATA_bookings'>
				<div class="table-responsive">
					<table class="table table-condensed table-bordered">
						<tr class="header bold">
							<td width="50" align="center">No.</td>
							<td width="300">Name</td>
                            <?php /*?><td width="130">Type</td><?php */?>
							<td >Type</td>
							<td align="center" width="50">In</td>
							<td align="center" width="50">Out</td>
							<td width="80" align="center">Status</td>
							
						</tr>
						<tbody>
							<tr ng-class="{'danger':account.publish_status!='1'}" ng-repeat="account in DATA.account.account | filter : filter_cashbank | orderBy : '-publish_status'">
								<td align="center">{{($index+1)}}</td>
								<td>
									<a ng-show="account.default == '0'" href="" data-toggle="modal" ng-click='addEditCashBank(account)' data-target="#add-edit-cash-bank">
										{{account.name}}
									</a>
									<span ng-show="account.default == '1'">
										{{account.name}}
									</span>
								</td>
								<td>{{account.type_desc}}</td>
								<td align="center"><span ng-show="account.cash_in == '1'"><span class="fa fa-check" style="color: green;"></span></span><span ng-show="account.cash_in == '0'"> - </span></td>
								<td align="center"><span ng-show="account.cash_out == '1'"><span class="fa fa-check" style="color: green;"></span></span><span ng-show="account.cash_out == '0'"> - </span></td>
                                <?php /*?><td>{{account.description}}</td><?php */?>
								<td align="center">
								<a href="" ng-click="publishUnpublishAccount(account, 0)" ng-show="account.publish_status == '1'">
								<!-- kode CC,ONLINE,OPENVOUCER tidak bisa disisable -->
									<div ng-class="{'hidden':account.default=='1'}" 
									style="color:#090;"><img src="<?=base_url("public/images/on-button.png")?>"></span> </div>
								</a> 
								<a href="" ng-click="publishUnpublishAccount(account, 1)" ng-show="account.publish_status != '1'">
									<div ng-class="{'hidden':account.default=='1'}"
									style="color:#F30"><img src="<?=base_url("public/images/off-button.png")?>"></span></div>
								</a> 

								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<!-- pagination -->
			<nav aria-label="Page navigation" class="pull-right" ng-show="DATA.account.search.number_of_pages>1">
			  <ul class="pagination pagination-sm">
				<li ng-class="{'disabled':DATA.account.search.page <= 1}">
				  <a href="" ng-click='loadDataCashBank(DATA.account.search.page-1)' aria-label="Prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
				</li>
				<li ng-repeat='pagination in DATA.account.search.pagination' ng-class="{'active':DATA.account.search.page == pagination}">
					<a href="" ng-click='loadDataCashBank(($index+1))'>{{($index+1)}}</a>
				</li>
				<li ng-class="{'disabled':DATA.account.search.page >= DATA.account.search.number_of_pages}">
				  <a href="" ng-click='loadDataCashBank(DATA.account.search.page+1)' aria-label="Next"><span class="glyphicon glyphicon-chevron-right"></span></a>
				</li>
			  </ul>
			</nav>
			<div class="clearfix"></div>


			<hr>
			<div class="add-product-button"> <a href="" class="btn btn-success btn-lg btn-block" data-toggle="modal" ng-click='addEditCashBank()' data-target="#add-edit-cash-bank"> <span class="glyphicon glyphicon-plus"></span> Add </a> </div>
			<br>
			<br>
			<div class="modal fade" id="add-edit-cash-bank" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  	<form ng-submit='saveDataCashBank($event)'>
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">
							<span ng-show='!DATA.CBAccount.id'>Add</span><span ng-show='DATA.CBAccount.id'>Edit</span> Cash and Bank
						</h4>
					  </div>
					  <div class="modal-body">
						<div ng-show='DATA.CBAccount.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.CBAccount.error_msg'>{{err}}</li></ul></div>
						
						<table class="table table-borderless table-condensed">
							<tr>
								<td width="140">Name*</td>
								<td><input placeholder="Name" required="required" type="text" class="form-control input-md" ng-model='DATA.CBAccount.name' /></td>
							</tr>
                            <tr>
                            	<td>Type*</td>
                                <td>
                                	<select class="form-control input-md" ng-model='DATA.CBAccount.type' required="required">
                                    	<option value="" disabled="disabled">-- Type --</option>
                                        <option value="CC">EDC Machine</option>
                                        <option value="TRANSFER">Bank Transfer</option>
                                        <option value="CASH">Cash</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                            	<td>Cash</td>
                            	<td>
                            		<input style="margin-left:3px;margin-bottom:15px" type="radio" class="chk-prior-cancellation" ng-model="DATA.CBAccount.cash_in" ng-change="change_check(check)" value="1"/>
									In 
									&nbsp;&nbsp;
									<input style="margin-left:3px;margin-bottom:15px" type="radio" class="chk-prior-cancellation" ng-model="DATA.CBAccount.cash_in" value="0" ng-change="change_check(check)" />
									Out
                            	</td>
                            </tr>
                            <tr ng-show="DATA.CBAccount.type == 'TRANSFER'" class="header">
              					<td>Bank Name</td>
              					<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.CBAccount.bank_name' /></td>
              				</tr>
              				<tr ng-show="DATA.CBAccount.type == 'TRANSFER'" class="header">
              					<td>Account Number</td>
              					<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.CBAccount.account_number' /></td>
              				</tr>
              				<tr ng-show="DATA.CBAccount.type == 'TRANSFER'" class="header">
              					<td>Name On Card</td>
              					<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.CBAccount.name_on_card' /></td>
              				</tr>
              				<tr ng-show="DATA.CBAccount.type == 'TRANSFER'" class="header">
              					<td>Branch</td>
              					<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.CBAccount.branch' /></td>
              				</tr>
							<?php /*?><tr>
								<td>Description</td>
								<td><input placeholder="Description" type="text" class="form-control input-md" ng-model='DATA.CBAccount.description' /></td>
							</tr><?php */?>
						</table>
					  </div>
					  <div class="modal-footer" style="text-align:center">
						<button type="submit" class="btn btn-primary">Save</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					  </div>
					</div>
				  </div>
				  </form>
			</div>
		</div>
		
	</div>
</div>
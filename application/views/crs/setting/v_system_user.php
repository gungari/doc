<?php //Sync : 180806 ?>
<div class="sub-title">System User</div>

<div ng-init="loadDataSystemUser()">
	<div ng-show="DATA.user.status=='SUCCESS'">
		<div class="products">
			<div class="product">
				Filter : <input type="text" ng-model="filter_user" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
			</div>
			
			<div ng-show='!DATA.user'>
				<img src="<?=base_url("public/images/loading_bar.gif")?>" />
			</div>
			<table class="table table-bordered" ng-show='DATA.user.user'>
				<tr class="info" style="font-weight:bold">
					<td>Email / Username</td>
					<td width="200">Name</td>
					<td width="150">Role</td>
					<td width="100"></td>
				</tr>
				<tr ng-class="{'danger':user.publish_status!='1'}" ng-repeat="user in DATA.user.user | filter : filter_user ">
					<td>
						<span ng-show='user.access_role.is_main_user'>
							{{user.email}}
						</span>
						<span ng-show='!user.access_role.is_main_user'>
                        	<div>
                                <a href="" data-toggle="modal" ng-click='addEditSystemUser(user)' data-target="#add-edit-user">
                                    {{user.email}}
                                </a>
                            </div>
                            <div ng-show='user.username'>
                            	Username : 
                                <a href="" data-toggle="modal" ng-click='addEditSystemUser(user)' data-target="#add-edit-user">
                                    {{user.username}}
                                </a>
                            </div>
						</span>
					</td>
					<td>{{user.name}}</td>
					<td ng-class="{'danger':user.access_role.publish_status!='1'}">{{user.access_role.name}}</td>
					<td align="center">
						<div ng-show='!user.access_role.is_main_user'>
							<a href="" ng-click="publishUnpublishSystemUser(user, 0)" ng-show="user.publish_status == '1'">
								<div style="color:#090"><img src="<?=base_url("public/images/on-button.png")?>"></span> </div>
							</a> 
							<a href="" ng-click="publishUnpublishSystemUser(user, 1)" ng-show="user.publish_status != '1'">
								<div style="color:#F30"><img src="<?=base_url("public/images/off-button.png")?>"></span></div>
							</a> 
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<hr>
	<div class="add-product-button"> <a href="" class="btn btn-success btn-lg btn-block" data-toggle="modal" ng-click='addEditSystemUser()' data-target="#add-edit-user"> <span class="glyphicon glyphicon-plus"></span> New System User </a> </div>
	<br>
	<br>
	
	<div class="modal fade" id="add-edit-user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <form ng-submit='saveDataSystemUser($event)'>
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">
				<span ng-show='!DATA.myUser.id'>Add</span><span ng-show='DATA.myUser.id'>Edit</span> System User
			</h4>
		  </div>
		  <div class="modal-body">
			<div ng-show='DATA.myUser.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myUser.error_msg'>{{err}}</li></ul></div>
			
			<table class="table table-borderless table-condensed">
				<tr>
					<td width="140">Name*</td>
					<td><input placeholder="Name" required="required" type="text" class="form-control input-md" ng-model='DATA.myUser.name' /></td>
				</tr>
				<tr>
					<td>Email</td>
					<td><input placeholder="Email" type="text" class="form-control input-md" ng-model='DATA.myUser.email' /></td>
				</tr>
                <tr>
					<td>Username</td>
					<td><input placeholder="Username" type="text" class="form-control input-md" ng-model='DATA.myUser.username' /></td>
				</tr>
				<tr>
					<td>Access Role*</td>
					<td>
						<select class="form-control" ng-model='DATA.myUser.access_role.id'>
							<option value="" disabled="">--Select Access Role--</option>
							<option value="{{access_role.id}}" ng-repeat='access_role in DATA.access_role.access_role'>{{access_role.name}}</option>
						</select>
					</td>
				</tr>
				
				<tr ng-show='DATA.myUser.id'>
					<td></td>
					<td><a href="" ng-click='DATA.myUser.change_password=!DATA.myUser.change_password'>Change Password</a></td>
				</tr>
				
				<tr ng-show='!DATA.myUser.id || DATA.myUser.change_password'>
					<td>Password*</td>
					<td><input placeholder="Password" ng-required="!DATA.myUser.id || DATA.myUser.change_password" type="password" class="form-control input-md" ng-model='DATA.myUser.password' /></td>
				</tr>
				<tr ng-show='!DATA.myUser.id || DATA.myUser.change_password'>
					<td>Retype Password*</td>
					<td><input placeholder="Retype Password" ng-required="!DATA.myUser.id || DATA.myUser.change_password" type="password" class="form-control input-md" ng-model='DATA.myUser.password_2' /></td>
				</tr>
			</table>

		  </div>
		  <div class="modal-footer" style="text-align:center">
			<button type="submit" class="btn btn-primary">Save</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
	  </div>
	  </form>
	</div>
	
</div>
</div>
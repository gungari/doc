<div class="sub-title">Access Role</div>

<div ng-init="loadDataAccessRole()">
	<div ng-show="DATA.access_role.status=='SUCCESS'">
		<div class="products">
			<div class="product">
				Filter : <input type="text" ng-model="filter_access_role" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
			</div>
			
			<div ng-show='!DATA.access_role'>
				<img src="<?=base_url("public/images/loading_bar.gif")?>" />
			</div>
			<table class="table table-bordered" xng-show='DATA.access_role.access_role'>
				<tr class="info" style="font-weight:bold">
					<td width="200">Name</td>
					<td>Description</td>
					<td width="100"></td>
				</tr>
				<tr ng-class="{'danger':access_role.publish_status!='1'}" ng-repeat="access_role in DATA.access_role.access_role| filter : filter_access_role">
					<td>
						<a href="" data-toggle="modal" ng-click='addEditAccessRole(access_role)' data-target="#add-edit-access-role">
							{{access_role.name}}
						</a>
					</td>
					<td>
						<div class="pull-right"><a href="" ng-click='viewAccessRoleDetail(access_role)'>Detail</a></div>
						<div>{{access_role.description}}</div>
						<div ng-show='access_role.view_detail_menu_link'>
							<hr style="margin:10px 0" />
							<p><strong>Access Menu</strong></p>
							<ol>
								<li ng-repeat='menu_link in access_role.menu_link'>
									<label>{{menu_link.name}}</label>
								</li>
							</ol>
						</div>
					</td>
					<td align="center">
						<a href="" ng-click="publishUnpublishAccessRole(access_role, 0)" ng-show="access_role.publish_status == '1'">
							<div style="color:#090"><img src="<?=base_url("public/images/on-button.png")?>"></span> </div>
						</a> 
						<a href="" ng-click="publishUnpublishAccessRole(access_role, 1)" ng-show="access_role.publish_status != '1'">
							<div style="color:#F30"><img src="<?=base_url("public/images/off-button.png")?>"></span></div>
						</a> 
					</td>
				</tr>
			</table>
		</div>
	</div>
	<hr>
	<div class="add-product-button"> <a href="" class="btn btn-success btn-lg btn-block" data-toggle="modal" ng-click='addEditAccessRole()' data-target="#add-edit-access-role"> <span class="glyphicon glyphicon-plus"></span> New Access Role </a> </div>
	<br>
	<br>
	
	<div class="modal fade" id="add-edit-access-role" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <form ng-submit='saveDataAccessRole($event)'>
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">
				<span ng-show='!DATA.myUser.id'>Add</span><span ng-show='DATA.myUser.id'>Edit</span> Access Role
			</h4>
		  </div>
		  <div class="modal-body">
			<div ng-show='DATA.myAccessRole.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myAccessRole.error_msg'>{{err}}</li></ul></div>
			
			<table class="table table-borderless table-condensed">
				<tr>
					<td width="140">Name*</td>
					<td><input placeholder="Name" required="required" type="text" class="form-control input-md" ng-model='DATA.myAccessRole.name' /></td>
				</tr>
				<tr>
					<td>Description</td>
					<td><input placeholder="Description" type="text" class="form-control input-md" ng-model='DATA.myAccessRole.description' /></td>
				</tr>
			</table>
			<hr />
			<h4>Access Menu</h4>
			<ol>
				<li ng-repeat='menu_link in DATA.menu_link.menu_link'>
					<label>
						<input type="checkbox" ng-model='DATA.myAccessRole.detail[menu_link.id]' ng-true-value="'1'" ng-false-value="'0'"
							ng-checked="DATA.myAccessRole.detail[menu_link.id] =='1'"  />
						{{menu_link.name}}
					</label>
				</li>
			</ol>
		  </div>
		  <div class="modal-footer" style="text-align:center">
			<button type="submit" class="btn btn-primary">Save</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
	  </div>
	  </form>
	</div>
	
</div>
</div>
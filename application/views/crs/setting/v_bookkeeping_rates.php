<div class="sub-title">Bookkeeping Rates</div>

<div ng-init="loadDataBookkeepingRates()">
	
    <div ng-show='DATA.bookkeeping_rates.status == "SUCCESS"'>
    	<br />
        <form ng-submit="saveDataBookkeepingRates($event)">
        <table class="table table-condensed">
			<tr ng-repeat="bookkeeping_rates in DATA.bookkeeping_rates.bookkeeping_rates">
            	<td width="30">{{($index+1)}}</td>
            	<td width="180">
                	<div class="input-group" style="width:170px; float:left">
                        <span class="input-group-addon input-sm">{{bookkeeping_rates.from.currency}}</span>
                        <input type="number" class="form-control input-sm" required="required" min="0" step="0.01" ng-model="bookkeeping_rates.from.amount" />
                    </div>
                </td>
                <td width="5">=</td>
                <td width="200">
                	<div class="input-group" style="width:200px; float:left">
                        <span class="input-group-btn">
                            <select class="form-control input-sm" required="required" style="width:80px; border-radius:3px 0 0 3px" ng-model="bookkeeping_rates.to.currency">
                                <option value="IDR">IDR</option>
                                <option value="AUD">AUD</option>
                                <option value="USD">USD</option>
                                <option value="CNY">CNY</option>
                                <option value="EUR">EUR</option>
                                <option value="JPY">JPY</option>
                                <option value="NZD">NZD</option>
                                <option value="BRL">BRL</option>
                                <option value="GBP">GBP</option>
                                <option value="CAD">CAD</option>
                                <option value="DKK">DKK</option>
                                <option value="HKD">HKD</option>
                                <option value="INR">INR</option>
                                <option value="MYR">MYR</option>
                                <option value="NOK">NOK</option>
                                <option value="PHP">PHP</option>
                                <option value="RUB">RUB</option>
                                <option value="SGD">SGD</option>
                                <option value="ZAR">ZAR</option>
                                <option value="KRW">KRW</option>
                                <option value="THB">THB</option>
                           	</select>
                        </span>
                        <input type="number" class="form-control input-sm" required="required" min="0" step="0.01" ng-model="bookkeeping_rates.to.amount" />
                    </div>
                </td>
                <td>
                	<a href="" style="color:red" class="remove-currency" ng-click="DATA.bookkeeping_rates.bookkeeping_rates.splice($index,1)"><span class="glyphicon glyphicon-remove"></span> Del</a>
                </td>
            </tr>  
            <tr>
            	<td></td>
            	<td colspan="4">
                	<a href="" ng-click='addBookkeepingRates()'><span class="glyphicon glyphicon-plus"></span> Add Currency</a>
                </td>
            </tr>
            <tr>
            	<td></td>
            	<td colspan="4">
                	<div ng-show='DATA.bookkeeping_rates.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.bookkeeping_rates.error_msg'>{{err}}</li></ul></div>
                	<button type="submit" class="btn btn-md btn-info">Update Data</button>
                </td>
            </tr>
        </table>
        </form>
	</div>

</div>
<div class="pull-right">
	<div class="dropdown">
		<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			<i class="fa fa-bars" aria-hidden="true"></i> Menu
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
		<li><a ui-sref="setting.access_role">Access Role</a></li>
		<li><a ui-sref="setting.system_user">System User</a></li>
		<li><a ui-sref="setting.cash_and_bank">Cash and Bank</a></li>
		<li><a ui-sref="setting.bookkeeping_rates">Bookkeeping Rates</a></li>
		<li><a ui-sref="setting.template">Layout & Template</a></li>
		<!-- <li><a ui-sref="setting.email_bcc">CC/BCC Email Invoices</a></li> -->
		</ul>
	</div>
</div>

<h1>Setting</h1>

<div ui-view>
	<br /><br />
	<hr />
	<strong><em>Please select setting menu above,,,</em></strong>
</div>
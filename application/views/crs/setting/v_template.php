<div class="sub-title">Layout & Template</div>
<div ng-init="loadDataVendor()">
	<div ng-show="DATA.vendor">
			<table class="table" width="100%" cellspacing="0" cellpadding="7">
				<tbody>
					<tr>
				        <td width="140px">Template</td>
				        <td>
<!-- 				            <select class="form-control input-sm cb-template" name="template" style="width:300px">
				                <option value="000">Default Template</option>
				                 <option value="1">Template - {{DATA.vendor.code}}</option>
				           </select> -->
				           <div ng-show='DATA.vendor.setting.active_template_crs != "" '>
								Template - {{DATA.vendor.setting.active_template_crs}}
							</div>
							<div ng-show='DATA.vendor.setting.active_template_crs == "" '>
								Default Template 
							</div>
				        </td>
				    </tr>
				    <tr>
				        <td></td>
				        <td>  
				            	<div class="add-product-button"> 
				            		<a href="" class="btn btn-info btn-sm btn-block" data-toggle="modal" ng-click='updateTemplate()' data-target="#update_template"> 
				            		Update Template 
				            		</a> 
				            	</div>
				        </td>
				    </tr>
				</tbody>

			</table>
	</div>

	<!-- MODAL TEMPLATE -->
	<div class="modal fade" id="update_template" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <form ng-submit='saveUpdateTemplate($event)'>
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- {{DATA.vendor.code}} -->
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">
				Update Template
			</h4>
		  </div>
		  <div class="modal-body">			
			<table class="table table-borderless table-condensed">
				<tr>
					<td width="140">Template</td>
					<td>
						<select class="form-control input-sm cb-template" name="template" style="width:300px" ng-model="code_vendor">
			                <option value="">Default Template</option>
			                <option value="{{DATA.vendor.code}}">Template - {{DATA.vendor.code}}</option>
			           </select>
					</td>
				</tr>
			</table>
			<hr />
		  </div>
		  <div class="modal-footer" style="text-align:center">
			<button type="submit" class="btn btn-primary">Save</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
	  </div>
	  </form>
	</div>
	
</div>


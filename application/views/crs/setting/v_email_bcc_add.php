<!-- <div class="sub-title">
	<span ng-show='!DATA.current_schedule.schedule_code'><i class="fa fa-plus-square" aria-hidden="true"></i> Add New</span> 
	<span ng-show='DATA.current_schedule.schedule_code'><i class="fa fa-pencil" aria-hidden="true"></i> Edit</span>
	Email CC
</div>

<br /> -->
<div ng-init="loadDataEmailBcc();">

	<div class="products">
		<!-- <div ng-show='!(DATA.email_bcc && DATA.port_list)'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		</div> -->
		
		<div >
			<form ng-submit="saveDataSchedule($event)">
				<div ng-show='DATA.email_bcc.error_desc.length>0' class="alert alert-danger show_error"><ul><li ng-repeat='err in DATA.email_bcc.error_desc'>{{err}}</li></ul></div>

				<table class="table table-borderlesss">
					
					<tr>
						
						<td>
						<!-- <div ng-show='(DATA.email_bcc)'> -->
							<table class="table table-bordered">
								<tr class="info">
									<th>No.</th>
									<th>Name</th>
									<th width="75%">Email Address</th>
									<th width="5%"></th>
								</tr>
								
								<tr ng-repeat="email in DATA.email_bcc">
									<td>{{$index+1}}</td>
									<td width="20%">
										<input required="required" class="form-control input-sm" type="text" name="" ng-model="email.name">
									</td>
									<td width="75%">
										<input required="required" class="form-control input-sm" type="email" name="" ng-model="email.email">
										<input type="hidden" name="" value="{{agent_code}}" ng-model="email.agent_code">
										<!-- <div class="inline-edit">
											<div class="main-text">
												<span class="inline-edit-icon pull-right"><a style="cursor: pointer;"><span class="glyphicon glyphicon-pencil"></span>Edit</a></span>
												<strong>{{email.email}}</strong>
											</div>
											<div class="edit-text hidden-field">
												
													<div class="cancel pull-right"><a style="cursor: pointer;">Cancel</a></div>
													<div class="input-group">
														<input required="required" class="form-control input-sm" type="text" name="" ng-model="email.email">
														<span class="input-group-btn">
															<button class="btn btn-info btn-sm" type="submit">Save</button>
														</span>
													</div>
												
											</div>
										</div> -->
									</td>
									
									<td width="5%">
										<a href="" style="color:red" ng-click="scheduleDetailRemoveLastItem()" ng-show='($index+1)==DATA.email_bcc.length'><i class="fa fa-remove" aria-hidden="true"></i></a>
										
									</td>
								</tr>
							</table>
							<!-- </div> -->
							<button style="margin-top: 20px;" type="button" class="btn btn-info btn-xs" ng-click='emailDetailAddItem()'>Add email</button>
						</td>
					</tr>
					<tr>
						
						<td>
							<button type="submit" class="btn btn-primary">Save Email</button>
							&nbsp;&nbsp;&nbsp;
							<!-- <a ui-sref="transport.trips_schedule"><strong>Cancel</strong></a> -->
						</td>
					</tr>
				</table>
			</form>
		</div>
		
	</div>
</div>
<script>activate_sub_menu_agent_detail("email_cc");</script>
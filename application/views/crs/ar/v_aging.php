<?php $this->load->view("crs/ar/v_ar_header_tab_menu.php") ?>
<script>activate_sub_menu_invoice("aging");</script>

<div ui-view>
	<div ng-init="loadDataAgentARAging()">
		<div>
			<div class="products">
				<div class="product">
                    <form ng-submit='loadDataAgentARAging(1, true)'>
                        Search : 
                        <input type="text" class="form-control input-sm" placeholder="Search" ng-model='search.q' style="display:inline; width:250px" />
                        <button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button>
                    </form>
                </div>
            
				<?php /*?><div class="product">
					Filter : <input type="text" ng-model="filter_agents" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
				</div><?php */?>
				
				<div ng-show='show_loading_DATA_bookings'>
                    <img src="<?=base_url("public/images/loading_bar.gif")?>" />
                </div>
				
                <div ng-show='!show_loading_DATA_bookings'>
                    <table class="table table-bordered table-condensed" ng-show='DATA.agents.agents'>
                        <tr class="info" style="font-weight:bold">
                            <td width="30">No.</td>
                            <td>Name</td>
                            <td width="10"></td>
                            <td width="80" align="right">Future</td>
                            <td width="80" align="right">Current</td>
                            <td width="80" align="right">30+ Days</td>
                            <td width="80" align="right">60+ Days</td>
                            <td width="80" align="right">90+ Days</td>
                            <td width="80" align="right">Balance</td>
                        </tr>
                        <tr xxng-class="{'danger':agent.publish_status!='1'}" 
                            ng-repeat="agent in DATA.agents.agents | filter : filter_agents "
                            style="font-size:11.5px">
                            <td>{{($index+1)}}</td>
                            <td>
                                <a ui-sref="agent.detail.invoice({'agent_code':agent.agent_code})" target="_blank"><strong>{{agent.name}}</strong></a><br />
                                {{agent.agent_code}}
                            </td>
                            <td align="center"><?=$vendor["default_currency"]?></td>
                            <td align="right">{{fn.formatNumber(agent.ar_aging.uninvoicing, '<?=$vendor["default_currency"]?>')}}</td>
                            <td align="right">{{fn.formatNumber(agent.ar_aging.days_0_30, '<?=$vendor["default_currency"]?>')}}</td>
                            <td align="right">{{fn.formatNumber(agent.ar_aging.days_31_60, '<?=$vendor["default_currency"]?>')}}</td>
                            <td align="right">{{fn.formatNumber(agent.ar_aging.days_61_90, '<?=$vendor["default_currency"]?>')}}</td>
                            <td align="right">{{fn.formatNumber(agent.ar_aging.days_91_plus, '<?=$vendor["default_currency"]?>')}}</td>
                            <td align="right">{{fn.formatNumber(agent.ar_aging.balance, '<?=$vendor["default_currency"]?>')}}</td>
                        </tr>
                    </table>
                    
                    <nav aria-label="Page navigation" class="pull-right">
                      <ul class="pagination pagination-sm">
                        <li ng-class="{'disabled':DATA.agents.search.page <= 1}">
                          <a href="" ng-click='loadDataAgentARAging(DATA.agents.search.page-1, true)' aria-label="Prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                        </li>
                        <li ng-repeat='pagination in DATA.agents.search.pagination' ng-class="{'active':DATA.agents.search.page == pagination}">
                            <a href="" ng-click='loadDataAgentARAging(($index+1), true)'>{{($index+1)}}</a>
                        </li>
                        <li ng-class="{'disabled':DATA.agents.search.page >= DATA.agents.search.number_of_pages}">
                          <a href="" ng-click='loadDataAgentARAging(DATA.agents.search.page+1, true)' aria-label="Next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                        </li>
                      </ul>
                    </nav>
                    <div class="clearfix"></div>
                </div>
                
                <div ng-show="DATA.ar_aging_summary">
                    <table class="table table-condensed table-borderless">
                        <tr>
                            <td align="right"><strong style="font-size:14px">Summary : </strong></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td align="right">Future : </td>
                            <td align="right" width="130"><strong><?=$vendor["default_currency"]?> {{fn.formatNumber(DATA.ar_aging_summary.uninvoicing, '<?=$vendor["default_currency"]?>')}}</strong></td>
                        </tr>
                        <tr>
                            <td align="right">Current : </td>
                            <td align="right"><strong><?=$vendor["default_currency"]?> {{fn.formatNumber(DATA.ar_aging_summary.days_0_30, '<?=$vendor["default_currency"]?>')}}</strong></td>
                        </tr>
                        <tr>
                            <td align="right">30+ Days : </td>
                            <td align="right"><strong><?=$vendor["default_currency"]?> {{fn.formatNumber(DATA.ar_aging_summary.days_31_60, '<?=$vendor["default_currency"]?>')}}</strong></td>
                        </tr>
                        <tr>
                            <td align="right">60+ Days : </td>
                            <td align="right"><strong><?=$vendor["default_currency"]?> {{fn.formatNumber(DATA.ar_aging_summary.days_61_90, '<?=$vendor["default_currency"]?>')}}</strong></td>
                        </tr>
                        <tr>
                            <td align="right">90+ Days : </td>
                            <td align="right"><strong><?=$vendor["default_currency"]?> {{fn.formatNumber(DATA.ar_aging_summary.days_91_plus, '<?=$vendor["default_currency"]?>')}}</strong></td>
                        </tr>
                        <tr style="font-size:14px">
                            <td align="right"><strong>Balance :</strong> </td>
                            <td align="right"><strong><?=$vendor["default_currency"]?> {{fn.formatNumber(DATA.ar_aging_summary.balance, '<?=$vendor["default_currency"]?>')}}</strong></td>
                        </tr>
                    </table>
                </div>
                
                <div ng-show="DATA.ar_aging_summary">
                    <hr />
                    <table width="100%" class="table table-condensed table-borderless">
                        <tr>
                            <td width="100"><strong>Future</strong></td>
                            <td>Total amount of money owed by customer and invoice has not been issued</td>
                        </tr>
                        <tr>
                            <td><strong>Current</strong></td>
                            <td>Total outstanding invoices ready to collect or billed and should be settled by customer</td>
                        </tr>
                        <tr>
                            <td><strong>30+ Days</strong></td>
                            <td>Total outstanding payments period owed by customer over 30 days</td>
                        </tr>
                        <tr>
                            <td><strong>60+ Days</strong></td>
                            <td>Total outstanding payments period owed by customer over 60 days</td>
                        </tr>
                        <tr>
                            <td><strong>90+ Days</strong></td>
                            <td>Total outstanding payments period owed by customer over 90 days</td>
                        </tr>
                        <tr>
                            <td><strong>Balance</strong></td>
                            <td>Total outstanding from future, current and over periods</td>
                        </tr>
                    </table>
                </div>
                
			</div>
		</div>
	</div>
</div>
<div class="sub-title">
	<span ng-show='!DATA.current_invoice.invoice_code'><i class="fa fa-plus-square" aria-hidden="true"></i> Create New</span> 
	<span ng-show='DATA.current_invoice.invoice_code'><i class="fa fa-pencil" aria-hidden="true"></i> Edit</span>
	Proforma Invoice {{DATA.current_invoice.invoice_code}}
</div>
<div ng-init="createProformaInvoice()">
	<div class="products">
		<div>
        	<div class="products">
                <div class="product">
                    <form ng-submit="loadDataUnpaidBooking(1,true)">
                    <div class="table-responsive">
                        <table class="table table-condensed table-borderless" width="100%">
                            <tr>
                                <td width="120">Filter By</td>
                                <td width="100">From</td>
                                <td width="100">To</td>
                                <td width="200">Search</td>
                                <td width="130">Source</td>
                                <td width="120">Type</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <select class="form-control input-sm " ng-model='DATA.search.filter_by'>
                                        <option value="BOOKINGDATE">Booking Date</option>
                                        <option value="TRIPDATE">Activity Date</option>
                                    </select>
                                </td>
                                <td><input type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='DATA.search.start_date' /></td>
                                <td><input type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='DATA.search.end_date' /></td>
                                <td><input type="text" class="form-control input-sm" placeholder="Search" ng-model='DATA.search.q' /></td>
                                <td>
                                    <select class="form-control input-sm" ng-model='DATA.search.agent_code' ng-change="ProformaInvoiceSearchAgentPopUp();">
                                        <option value="">All</option>
                                        <option ng-show='DATA.selected_agents && DATA.selected_agents.length > 0' value="-------" disabled="disabled">-------</option>
                                    	<option value="{{agent.agent_code}}" ng-repeat='agent in DATA.selected_agents'>{{agent.name}} - {{agent.agent_code}}</option>
                                        <option disabled="disabled" value="--------">-------</option>
                                        <option value="SELECTAGENT">Select Agencies and Colleague</option>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control input-sm " ng-model='DATA.search.type'>
                                        <option value="RESERVATION">Reservation</option>
                                        <option value="OPENVOUCHER">Pre Sold Ticket</option>
                                    </select>
                                </td>
                                <td><button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button></td>
                            </tr>
                        </table>
                        
                        <p style="color:green; margin:10px 0 0 6px;"><em>Please select or click Business Source name to continue to create proforma invoice.</em></p>
                        
                        <?php /*?>Filter : <input type="text" ng-model="DATA.filter_booking" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
                        <button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button><?php */?>
                    </div>	
                    </form>
                </div>
            </div>
        
			<?php { /*?><form class="form-inline" ng-submit="loadDataUnpaidBooking(1,true)">
                <div class="products">
                    <div class="product">
                        <div class="table-responsive">
                            <table class="table table-condensed table-borderless" width="100%">
                                <tr>
                                    <td width="160">Filter By</td>
                                    <td width="110">From</td>
                                    <td width="110">To</td>
                                    <td width="200">Business Source</td>
                                    <td width="150">Type</td>
                                    <td width="130"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                	<td>
                                    	<select style="width: 100%; margin-top:5px;margin-bottom: 10px;" class="form-control input-sm " ng-model='DATA.search.filter_by'>
                                            <option value="BOOKINGDATE">Booking Date</option>
                                            <option value="TRIPDATE">Activity Date</option>
                                        </select>
                                    </td>
                                    <td><input style="width: 100%;margin-top:5px;margin-bottom: 10px;" type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='DATA.search.start_date' /></td>
                                    <td><input style="width: 100%;margin-top:5px;margin-bottom: 10px;" type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='DATA.search.end_date' /></td>
                                    <td>
                                    	<select class="form-control input-sm" ng-model='DATA.search.agent_code' style="width:100%;margin-top:5px;margin-bottom: 10px;">
                                            <option value="">All</option>
                                            <option disabled="disabled">---</option>
                                            <option ng-show="agent.agent_code" value="{{agent.agent_code}}" ng-repeat="agent in DATA.agents | orderBy : 'name'">{{agent.name}} {{(agent.real_category.name?' - '+agent.real_category.name:'')}}</option>
                                        </select>
                                    </td>
                                    <td>
                                    	<select style="width: 100%; margin-top:5px;margin-bottom: 10px;" class="form-control input-sm " ng-model='DATA.search.type'>
                                            <option value="RESERVATION">Reservation</option>
                                            <option value="OPENVOUCHER">Pre Sold Ticket</option>
                                        </select>
                                    </td>
                                    <td>
                                    	<button type="submit" style="margin-top:5px;margin-bottom: 10px;" class="btn btn-sm btn-info form-control input-sm"><i class="fa fa-check"></i> Ok</button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                	</div>
                </div>
            </form><?php */ } ?>
            
            <div ng-show='!DATA.UnpaidBookings.ready'>
				<img src="<?=base_url("public/images/loading_bar.gif")?>" />
			</div>
            
			<div ng-show='DATA.UnpaidBookings.ready && DATA.search.type =="RESERVATION"'>
            	<div ng-show="DATA.UnpaidBookings.ready_to_create_invoice">
                	<div class="sub-title">Agent Information</div>
                    <table class="table">
                    	<tr>
                        	<td width="100">Agent Code</td>
                            <td width="10">:</td>
                            <td><strong>{{DATA.AGENT.agent_code}}</strong></td>
                        </tr>
                        <tr>
                        	<td>Name</td>
                            <td>:</td>
                            <td><strong>{{DATA.AGENT.name}}</strong></td>
                        </tr>
                        <tr>
                        	<td>Email</td>
                            <td>:</td>
                            <td><strong>{{DATA.AGENT.email}}</strong></td>
                        </tr>
                        <tr ng-show='DATA.AGENT.address'>
                        	<td>Adress</td>
                            <td>:</td>
                            <td><strong>{{DATA.AGENT.address}}</strong></td>
                        </tr>
                        <tr ng-show='DATA.AGENT.real_category'>
                        	<td>Category</td>
                            <td>:</td>
                            <td><strong>{{DATA.AGENT.real_category.name}}</strong></td>
                        </tr>
                        <tr>
                        	<td colspan="3">&nbsp;</td>
                        </tr>
                    </table>
                </div>
            	
            
            	<div class="sub-title">Reservation List</div>
                
                <div ng-show='!DATA.UnpaidBookings.bookings'>
                	<div class="alert alert-warning">
                    	<em><strong>Sorry,</strong> Data not found...</em>
                    </div>
                </div>
                <div ng-show='DATA.UnpaidBookings.bookings'>
                    <table class="table table-condensed table-bordered table-hover">
                        <tr class="info">
                            <th width="30" ng-show="DATA.UnpaidBookings.ready_to_create_invoice" align="center" style="font-size:16px">
                            	<a href="" ng-click="chk_booking_list_uncheck_all();_check_all = false;" ng-show='_check_all'>
	                            	<span class="glyphicon glyphicon-check"></span>
                                </a>
                                <a href="" ng-click="chk_booking_list_check_all();_check_all = true;" ng-show='!_check_all'>
	                                <span class="glyphicon glyphicon-unchecked"></span>
                                </a>
                            </th>
                            <td align="center" width="90">Order#</td>
                            <td align="center" width="110">Booking Date</td>
                            <td>Customer Name</td>
                            <td align="center" width="40" title="Nationality">Nat.</td>
                            <td width="200" ng-hide="DATA.UnpaidBookings.ready_to_create_invoice">Booking Source</td>
                            <td align="right">Amount</td>
                        </tr>
                        <tbody ng-repeat="booking in DATA.UnpaidBookings.bookings" ng-hide="!booking.booking_code">
                        	<tr ng-class="{'danger':(booking.status_code=='CANCEL'), 'warning':(booking.status_code=='UNDEFINITE')}">
                            
                            	<td rowspan="2" ng-show="DATA.UnpaidBookings.ready_to_create_invoice" align="center">
                                    <input ng-disabled="!DATA.UnpaidBookings.ready_to_create_invoice" type="checkbox" ng-click="chk_booking_list($event, booking);" ng-checked="DATA.selected_booking_list[booking.booking_code]" />
                                </td>
                                <td align="center" rowspan="2">
                                    <a ui-sref="reservation.detail({'booking_code':booking.booking_code})" target="_blank"><strong>{{booking.booking_code}}</strong></a>
                                </td>
                                <td align="center">{{fn.newDate(booking.transaction_date) | date:'dd MMM yyyy'}} <small>{{fn.newDate(booking.transaction_date) | date:'HH:mm'}}</small> </td>
                                <td>
                                    <strong>{{booking.customer.full_name}}</strong>
                                </td>
                                <td align="center" class="text-capitalize" title="{{booking.customer.country_name}}">{{booking.customer.country_code}}</td>
                                <td ng-hide="DATA.UnpaidBookings.ready_to_create_invoice">
                                    <div ng-show='!booking.agent' class="text-capitalize">{{booking.source.toLowerCase()}}</div>
                                    <div ng-show='booking.agent'>
                                        <a href="" ng-click='<?php /*?>DATA.search.agent_code = booking.agent.agent_code<?php */?>ProformaInvoiceSearchAgentPopUpSelectAgent(booking.agent);loadDataUnpaidBooking();'>
                                            {{booking.agent.name}}
                                        </a>
                                    </div>
                                </td>
                                <td align="right">
                                    {{booking.currency}} {{fn.formatNumber(booking.balance, booking.currency)}}
                                </td>
                            </tr>
                            <tr ng-class="{'danger':(booking.status_code=='CANCEL'), 'warning':(booking.status_code=='UNDEFINITE')}">
                                <td colspan="5" style="padding:0 !important">
                                    
                                    <table class="table table-bordered table-condensed" style="margin:-1px !important; background:none">
                                        <tr ng-show='booking.remarks'>
                                            <td style="border-left:solid 5px #FFF; font-size:11px">
                                                <em>Remarks : {{booking.remarks}}</em>
                                            </td>
                                        </tr>
                                        <tr ng-repeat="voucher in booking.detail | orderBy : '-booking_detail_status_code'" ng-class="{'danger':(voucher.booking_detail_status_code=='CANCEL')}">
                                            <td style="border-left:solid 5px #EEE; font-size:11px">
                                                {{voucher.voucher_code}} - 
                                                {{fn.formatDate(voucher.date, "dd / M / y")}} - 
                                                
                                                <span ng-show="voucher.product_type == 'ACT'">
                                                    {{voucher.product.name}}
                                                </span>
                                                <span ng-show="voucher.product_type == 'TRANS'">
                                                    {{voucher.departure.port.port_code}} ({{voucher.departure.time}})
                                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                                    {{voucher.arrival.port.port_code}} ({{voucher.arrival.time}})
                                                </span>
                                                
                                                <span ng-show="voucher.qty_1 > 0"> - {{voucher.qty_1}} Adl. </span>
                                                <span ng-show="voucher.qty_2 > 0"> - {{voucher.qty_2}} Chi. </span>
                                                <span ng-show="voucher.qty_3 > 0"> - {{voucher.qty_3}} Inf. </span>
                                                
                                                <span ng-show="voucher.is_pickup=='YES'"> - Pickup </span>
                                                <span ng-show="voucher.is_dropoff=='YES'"> - Dropoff </span>
                                                <span class="pull-right" style="color: red;" ng-show="booking.status_code=='CANCEL'"><i>Cancelation fee may aplied</i></span>
                                            </td>
                                        </tr>
                                    </table>
                                
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6" style="background:#FAFAFA"></td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <div ng-show="DATA.UnpaidBookings.ready_to_create_invoice">
                    	<br />
                    	<a href="" ng-click="chk_booking_list_uncheck_all();_check_all = false;" ng-show='_check_all'><i class="fa fa-square-o" style="font-size: 16px;"></i> Uncheck All</a>
                        <a href="" ng-click="chk_booking_list_check_all();_check_all = true;" ng-show='!_check_all'><i class="fa fa-check-square-o" style="font-size: 16px;"></i> Check All</a>
                    </div>
                    
                    <nav aria-label="Page navigation" class="pull-right" ng-show='DATA.search.number_of_pages > 1'>
                      <ul class="pagination pagination-sm">
                        <li ng-class="{'disabled':DATA.search.page <= 1}">
                          <a href="" ng-click='loadDataUnpaidBooking(DATA.search.page-1)' aria-label="Prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                        </li>
                        <li ng-repeat='pagination in DATA.search.pagination' ng-class="{'active':DATA.search.page == pagination}">
                            <a href="" ng-click='loadDataUnpaidBooking(($index+1))'>{{($index+1)}}</a>
                        </li>
                        <li ng-class="{'disabled':DATA.search.page >= DATA.search.number_of_pages}">
                          <a href="" ng-click='loadDataUnpaidBooking(DATA.search.page+1)' aria-label="Next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                        </li>
                      </ul>
                    </nav>
                    <div class="clearfix"></div>
                    
                </div>
            </div>
            
            <div ng-show="DATA.UnpaidBookings.ready_to_create_invoice">
            	<br />
                <div class="sub-title">Other Information</div>
                <div class="row">
                    <div class="col-sm-6">
                    	<br />
                        Remarks :<br />
                        <textarea placeholder="Remarks" ng-model="DATA.UnpaidBookings.remarks" class="form-control" style="width: 100%" rows="3"></textarea>
                        <br />
                        Bank Account :<br />
                        <select class="form-control input-sm" ng-model="DATA.UnpaidBookings.bank_id">
                            <option value="">-- Select Bank Account --</option>
                            <option ng-show="bank.id" ng-repeat="bank in DATA.vendor_account_number" value="{{bank.id}}">{{bank.bank_name+" - "+bank.account_number}}</option>
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <table class="table table-borderless">
                            <tr>
                                <td colspan="3" align="right">
                                    
                                </td>
                            </tr>
                            <tr>
                                <td width="90"><strong>Checked</strong></td>
                                <td width="10">:</td>
                                <td align="right">
                                    <span style="color: green;" class="g_total">{{DATA.UnpaidBookings.bookings[0].currency}} {{fn.formatNumber(DATA.GrandTotalSelectedBooking, DATA.UnpaidBookings.bookings[0].currency)}}</span>
                                    &nbsp; from &nbsp;
                                    <strong style="color: red;">{{DATA.uninvoicing.currency}} {{fn.formatNumber(DATA.uninvoicing.uninvoicing, DATA.uninvoicing.currency)}}</strong>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <hr />
                <div align="center">
                	<button ng-show='!DATA.UnpaidBookings.is_submit_data' type="submit" class="btn btn-lg btn-info" ng-click='save_proforma_invoice()' ng-disabled="(!DATA.GrandTotalSelectedBooking || DATA.GrandTotalSelectedBooking == 0)">Create Proforma Invoice</button>
                    <button ng-show='DATA.UnpaidBookings.is_submit_data' type="submit" class="btn btn-lg btn-info" disabled="disabled">Loading...</button>
                </div>
            </div>
            
		</div>
	</div>
    
    <div class="modal fade" id="modal-search-agent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Search Agent</h4>
                </div>
                <div class="modal-body">
                    <div class="form-inline">
                        Search :
                        <input type="text" class="form-control input-md" ng-model="modal_agent_search.name" style="width:250px" placeholder="Search..." />
                        <select class="form-control input-md" ng-model="modal_agent_search.real_category.id" style="width:200px">
                            <option value="">All Category</option>
                            <option value="" disabled="disabled">---</option>
                            <option value="{{real_categories.id}}" ng-repeat="real_categories in DATA.agent_real_categories">{{real_categories.name}}</option>
                        </select>
                    </div>
                    <hr />
                    <table class="table table-bordered tbl-search-agent">
                        <tr class="success">
                            <td width="40"><strong>#</strong></td>
                            <td><strong>Agent Name</strong></td>
                            <td width="180"><strong>Category</strong></td>
                        </tr>
                        <tr ng-repeat="agent in DATA.agents.agents | filter : modal_agent_search | limitTo:50" class="agent-list"
                            ng-click="ProformaInvoiceSearchAgentPopUpSelectAgent(agent)" data-dismiss="modal">
                            <td>{{($index+1)}}</td>
                            <td>
                                <strong>{{agent.name}}</strong><br />
                                <small>{{agent.agent_code}}</small>
                            </td>
                            <td><span ng-repeat='_cat in agent.real_categories'> {{_cat.name}}<span ng-hide="agent.real_categories.length==($index+1)">,</span> </span></td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer" style="text-align:center">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    
</div>
<script>$("#invoice_title").html("PROFORMA INVOICE")</script>
<style type="text/css">
	.booking td label{font-weight: normal;}
	.pagination>.active>a{background-color: #337ab7 !important; color:#FFF !important;}
</style>
<style>
	table.tbl-search-agent tr.agent-list{cursor:pointer;}
	table.tbl-search-agent tr.agent-list:hover{background:#d9edf7}
</style>
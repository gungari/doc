			<div class="sub-title">Reservation List</div>
            <table class="table table-bordered table-condensed">
                
                <tr class="info bold">
                    <td align="center" width="90">Order#</td>
                    <td align="center" width="150">Booking Date</td>
                    <td>Customer Name</td>
                    <td align="center" width="60" title="Nationality">Nat.</td>
                    <?php /*?><td width="200">Booking Source</td><?php */?>
                    <td align="right" width="140">Amount</td>
                </tr>
                <tbody ng-repeat="booking in DATA.current_invoice.invoice.detail">
                    <tr ng-class="{'danger':(booking.cancel_status=='CAN' || booking.cancel_status=='VOID'), 'warning':(booking.status_code=='UNDEFINITE')}">
                        <td align="center" rowspan="2">
                            <a ui-sref="reservation.detail({'booking_code':booking.booking_code})"><strong>{{booking.booking_code}}</strong></a>
                        </td>
                        <td align="center">{{fn.newDate(booking.transaction_date) | date:'dd MMM yyyy'}} <small>{{fn.newDate(booking.transaction_date) | date:'HH:mm'}}</small> </td>
                        <td>
                            <strong>{{booking.customer.full_name}}</strong>
                        </td>
                        <td align="center" class="text-capitalize" title="{{booking.customer.country_name}}">{{booking.customer.country_code}}</td>
                        <?php /*?><td>
                            <div ng-show='!booking.agent' class="text-capitalize">{{booking.source.toLowerCase()}}</div>
                            <div ng-show='booking.agent'><a ui-sref="agent.detail({'agent_code':booking.agent.agent_code})" target="_blank">{{booking.agent.name}}</a></div>
                        </td><?php */?>
                        <td align="right">
                            {{booking.currency}} {{fn.formatNumber(booking.grand_total, booking.currency)}}
                        </td>
                    </tr>
                    <tr ng-class="{'danger':(booking.cancel_status=='CAN' || booking.cancel_status=='VOID'), 'warning':(booking.status_code=='UNDEFINITE')}">
                        <td colspan="4" style="padding:0 !important">
                            
                            <table class="table table-bordered table-condensed" style="margin:-1px !important; background:none">
                                <?php /*?><tr ng-show="booking.remarks || booking.agent && booking.voucher_reff_number != ''">
                                    <td style="border-left:solid 5px #FFF; font-size:11px">
                                        <em>
                                            Remarks : {{booking.remarks}}
                                            <span ng-show="booking.agent && booking.voucher_reff_number != ''">(Voucher# Reff. : {{booking.voucher_reff_number}})</span>
                                        </em>
                                    </td>
                                </tr><?php */?>
                                <tr ng-repeat="voucher in booking.detail | orderBy : '-booking_detail_status_code'" 
                                    ng-class="{'danger':(booking.cancel_status=='CAN')}">
                                    <td style="border-left:solid 5px #EEE; font-size:11px">
                                        {{voucher.voucher_code}} - 
                                        <strong style="font-size:12px">{{fn.formatDate(voucher.date, "dd / M / y")}}</strong> - 
                                        <span ng-show="voucher.product_type == 'ACT'">
                                            {{voucher.product.name}}
                                        </span>
                                        <span ng-show="voucher.product_type == 'TRANS'">
                                            {{voucher.departure.port.port_code}} ({{voucher.departure.time}})
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                            {{voucher.arrival.port.port_code}} ({{voucher.arrival.time}})
                                        </span>
                                        
                                        <span ng-show="voucher.qty_1 > 0"> - {{voucher.qty_1}} Adl. @ {{booking.currency}} {{fn.formatNumber(voucher.rates.rates_1, booking.currency)}}</span>
                                        <span ng-show="voucher.qty_2 > 0"> - {{voucher.qty_2}} Chi. @ {{booking.currency}} {{fn.formatNumber(voucher.rates.rates_1, booking.currency)}}</span>
                                        <span ng-show="voucher.qty_3 > 0"> - {{voucher.qty_3}} Inf. @ {{booking.currency}} {{fn.formatNumber(voucher.rates.rates_1, booking.currency)}}</span>
                                        
                                        <span ng-show="voucher.is_pickup=='YES'"> - Pickup </span>
                                        <span ng-show="voucher.is_dropoff=='YES'"> - Dropoff </span>
                                    </td>
                                </tr>
                            </table>
                            
                        </td>
                    </tr>
                    <?php /*?><tr>
                        <td colspan="5" style="background:#FAFAFA"></td>
                    </tr><?php */?>
                </tbody>
                
                <tfoot>
                    <tr class="success">
                        <td colspan="4" align="right"><strong>Grand Total</strong></td>
                        <td align="right"><strong>{{DATA.current_invoice.invoice.currency}} {{fn.formatNumber(DATA.current_invoice.invoice.grand_total, DATA.current_invoice.invoice.currency)}}</strong></td>
                    </tr>
                    <tr ng-class="{'danger':(DATA.current_invoice.invoice.balance>0), 'info':(DATA.current_invoice.invoice.balance<=0)}">
                        <td colspan="4" align="right"><strong>Total Payment</strong></td>
                        <td align="right">
                            <strong>{{DATA.current_invoice.invoice.currency}} ({{fn.formatNumber(DATA.current_invoice.invoice.total_payment, DATA.current_invoice.invoice.currency)}})</strong>
                        </td>
                    </tr>
                    <tr ng-class="{'danger':(DATA.current_invoice.invoice.balance>0), 'info':(DATA.current_invoice.invoice.balance<=0)}">
                        <td colspan="4" align="right"><strong>Outstanding Invoice</strong></td>
                        <td align="right"><strong>{{DATA.current_invoice.invoice.currency}} {{fn.formatNumber(DATA.current_invoice.invoice.balance, DATA.current_invoice.invoice.currency)}}</strong></td>
                    </tr>
                </tfoot>
            </table>
            
            <br />
            <div class="sub-title">Remarks</div>
            <br />
            <div ng-show="DATA.current_invoice.invoice.remarks">
                <pre style='font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;font-size: 13px;padding: 0;margin: 0;border: 0;background-color: transparent;line-height: 1;padding-bottom: 10px'>{{DATA.current_invoice.invoice.remarks}}</pre>
            </div>
            <div ng-show="!DATA.current_invoice.invoice.remarks">-</div>
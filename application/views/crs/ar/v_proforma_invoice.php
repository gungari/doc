<?php $this->load->view("crs/ar/v_ar_header_tab_menu.php") ?>
<script>activate_sub_menu_invoice("proforma_invoice");</script>

<div ng-init="loadDataInvoices(1, true)">
    <div class="products">
        <?php /*?><div class="product">
            <form ng-submit="loadDataInvoices(1, true)">
                Filter : <input type="text" ng-model="search.q" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
                <button type="submit" class="btn btn-sm btn-info"><span class="glyphicon glyphicon-search"></span></button>
            </form>
        </div><?php */?>
        
        <div class="product">
			<form ng-submit='loadDataInvoices(1,true)'>
				<div class="table-responsive">
					<table class="table table-condensed table-borderless" width="100%">
						<tr>
                            <td width="100">From</td>
							<td width="100">To</td>
							<td width="200">Search</td>
							<td width="150">Source</td>
							<td></td>
						</tr>
						<tr>
							<td><input type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='search.start_date' /></td>
							<td><input type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='search.end_date' /></td>
							<td><input type="text" class="form-control input-sm" placeholder="Search" ng-model='search.q' /></td>
							<td>
								<select class="form-control input-sm" ng-model='search.booking_source' ng-change="InvoiceListSearchAgentPopUp();">
									<option value="">All</option>
									<option value="{{booking_source.code}}" ng-repeat='booking_source in $root.DATA_booking_source'>{{booking_source.name}}</option>
                                    <option ng-show='DATA.selected_agents && DATA.selected_agents.length > 0' value="-------" disabled="disabled">-------</option>
                                    <option value="{{agent.agent_code}}" ng-repeat='agent in DATA.selected_agents'>{{agent.name}} - {{agent.agent_code}}</option>
                                    <option value="-------" disabled="disabled">-------</option>
                                    <option value="SELECTAGENT">Select Agencies and Colleague</option>
								</select>
							</td>
							<td><button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button></td>
						</tr>
					</table>
				</div>	
			</form>
		</div>
        
        <div ng-show='!DATA.invoice'>
            <img src="<?=base_url("public/images/loading_bar.gif")?>" />
        </div>
        <div ng-show="DATA.invoice && DATA.invoice.status!='SUCCESS'">
            <div class="alert alert-warning">
                <strong>Data Not Founds.</strong>
            </div>
        </div>
        
        <div ng-show="DATA.invoice.status=='SUCCESS'">
            <div class="table-responsive" ng-show='DATA.invoice.invoices'>
                <table class="table table-bordered table-condensed">
                    <tr class="info" style="font-weight:bold">
                        <td width="30" align="center">No.</td>
                        <td width="160" align="center">Code #</td>
                        <td align="center">Business Source</td>
                        <td width="100" align="center">Invoice Date</td>
                        <td width="100" align="center">Type</td>
                        <td width="100" align="center">Total</td>
                        <td width="90" align="center">Status</td>
                    </tr>
                    <tbody ng-repeat="invoice in DATA.invoice.invoices | filter : filter_invoices" style="border-top:none">
                        <tr ng-class="{success : invoice.invoice_total == invoice.total_payment, warning : invoice.invoice_due < DATA.invoice.now && invoice.balance > 0 }">
                            <td rowspan="{{(invoice.remarks?2:1)}}" align="right">{{($index+1)}}</td>
                            <td rowspan="{{(invoice.remarks?2:1)}}">
                                <a ui-sref="ar.proforma_invoice_detail({'invoice_code':invoice.invoice_code})" ng-show="invoice.type=='RESERVATION'"><strong>{{invoice.invoice_code}}</strong></a>
                                <a ui-sref="invoice.openvoucher({'invoice_code':invoice.invoice_code})" ng-show="invoice.type=='OPENVOUCHER'"><strong>{{invoice.invoice_code}}</strong></a>
                            </td>
                            <td>
                                <strong><a ui-sref="agent.detail.invoice({'agent_code':invoice.agent.agent_code})" target="_blank">{{invoice.agent.name}}</a></strong>
                            </td>
                            <td align="center">{{fn.formatDate(invoice.invoice_date, "d M yy")}}</td>
                            <td>
                                <span ng-show="invoice.type=='OPENVOUCHER'">Pre Sold Ticket</span>
                                <span ng-show="invoice.type=='RESERVATION'">Reservation</span>
                            </td>
                            <td align="right">{{invoice.currency}} {{fn.formatNumber(invoice.grand_total, invoice.currency)}}</td>
                            <td align="center"><span class="label label-default">{{invoice.proforma_invoice_status_desc}}</span></td>
                        </tr>
                        <tr ng-show='invoice.remarks'>
                            <td colspan="5">
                            	<em>Remarks : {{invoice.remarks || '-'}}</em>
                            </td>
                        </tr>
                     </tbody>
                </table>
            </div>
        </div>

        <!-- pagination -->
        <nav aria-label="Page navigation" class="pull-right" ng-show="DATA.invoice.search.number_of_pages>1">
          <ul class="pagination pagination-sm">
            <li ng-class="{'disabled':DATA.invoice.search.page <= 1}">
              <a href="" ng-click='loadDataInvoices(DATA.invoice.search.page-1, true)' aria-label="Prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
            </li>
            <li	ng-show="DATA.invoice.search.page-3 > 1">
                <a href="" ng-click='loadDataInvoices(1, true)'>1</a>
            </li>
            <li class="disabled" ng-show="DATA.invoice.search.page-4 > 1">
                <a>...</a>
            </li>
            <li ng-repeat='pagination in DATA.invoice.search.pagination' ng-class="{'active':DATA.invoice.search.page == pagination}" ng-show="$index+1 > DATA.invoice.search.page-4 && $index+1 < DATA.invoice.search.page+4">
                <a href="" ng-click='loadDataInvoices(($index+1), true)'>{{($index+1)}}</a>
            </li>
            <li class="disabled" ng-show="DATA.invoice.search.page+4 < DATA.invoice.search.number_of_pages">
                <a>...</a>
            </li>
            <li	ng-show="DATA.invoice.search.page+3 < DATA.invoice.search.number_of_pages">
                <a href="" ng-click='loadDataInvoices(DATA.invoice.search.number_of_pages, true)'>{{DATA.invoice.search.number_of_pages}}</a>
            </li>
            <li ng-class="{'disabled':DATA.invoice.search.page >= DATA.invoice.search.number_of_pages}">
              <a href="" ng-click='loadDataInvoices(DATA.invoice.search.page+1, true)' aria-label="Next"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </li>
          </ul>
        </nav>
        <div class="clearfix"></div>
        
    </div>
    <hr>
    <div class="add-product-button"> <a ui-sref="ar.proforma_invoice_add" class="btn btn-success btn-lg btn-block"> <span class="glyphicon glyphicon-plus"></span> New Proforma Invoice </a> </div>
    <br>
    <br>
    <?php $this->load->view("crs/ar/v_ar_search_agent_popup"); ?>
</div>

<script>$("#invoice_title").html("INVOICES")</script>
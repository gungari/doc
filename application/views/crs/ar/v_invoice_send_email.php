<div class="modal fade" id="frm-invoice-send-email" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <form ng-submit='sendEmailInvoiceSubmit($event)'>
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="myModalLabel">
			Send Email {{(DATA.current_invoice.invoice.is_proforma_invoice?"Proforma":"")}} Invoice #{{DATA.current_invoice.invoice.invoice_code}}
		</h4>
	  </div>
	  <div class="modal-body">
      	<div>
            <table class="table table-borderless table-condensed">
                <tr>
                    <td colspan="2"><strong>BUSINESS SOURCE</strong></td>
                </tr>
                <tr>
                    <td width="100">Name</td>
                    <td><strong>{{DATA.current_invoice.invoice.agent.name}}</strong></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><strong>{{DATA.current_invoice.invoice.agent.email}}</strong></td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td><strong>{{DATA.current_invoice.invoice.agent.address||'-'}}</strong></td>
                </tr>
                <tr>
                    <td>Telephone</td>
                    <td><strong>{{DATA.current_invoice.invoice.agent.phone||'-'}}</strong></td>
                </tr>
                <tr ng-show='DATA.current_invoice.email_list_agent'>
                    <td colspan="2"><hr /><strong>SELECT EMAIL</strong></td>
                </tr>
                <tr ng-show='DATA.current_invoice.email_list_agent'>
                	<td colspan="2">
                    	<ol>
                        	<li ng-repeat='email in DATA.current_invoice.email_list_agent'>
                            	<label style="font-weight:{{(email.selected?'bold':'normal')}}">
                                	<input type="checkbox" ng-model='email.selected' ng-true-value='1' ng-false-value='0' /> &nbsp;&nbsp; 
                                	<span>{{email.name}} - {{email.email}}</span>
                                </label>
                            </li>
                        </ol>
                    </td>
                </tr>
            </table>
		</div>
		<div class="modal-footer" style="text-align:center !important">
            <button type="submit" class="btn btn-primary">Send Now</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
		</div>
        
      </div>
	</div>
  </div>
  </form>
</div>
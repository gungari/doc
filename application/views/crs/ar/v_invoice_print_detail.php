<div ng-init="printInvoiceDetail()">
	
	<?php /*?><div class="no-print text-center" style="text-align:center !important">
		<select class="form-control input-md change-paper-size" ng-model='print_type' style="width:auto; display:inline">
			<option value="">Normal Paper</option>
			<option value="small"> Small Paper</option>
		</select>
		<br /><br />
	</div><?php */?>
    
	<div class="small-paper" ng-show="print_type == 'small'">
		<div class="header">
			<div class="title"><?=$vendor["business_name"]?></div>
			<div><?=$vendor["address"]?>, <?=$vendor["regency"]?>, <?=$vendor["province"]?>, <?=$vendor["country"]?></div>
			<div>[P] : <?=$vendor["telephone"]?></div>
			<div>[E] : <?=$vendor["email"]?></div>
			<div><?=$vendor["website"]?></div>
		</div>
		
		<div class="text-center">
			<strong>{{(DATA.current_invoice.invoice.is_proforma_invoice?"PROFORMA":"")}} INVOICE</strong><br />
			<div>
				<img src="<?=base_url()?>public/plugin/barcode.php?text={{DATA.current_invoice.invoice.invoice_code}}&size=40" />
			</div>
			<div style="font-size:16px">
				{{DATA.current_invoice.invoice.invoice_code}}
			</div>
		</div>
		
		<hr />
		<strong>{{(DATA.current_invoice.invoice.is_proforma_invoice?"PROFORMA":"")}} INVOICE INFORMATION</strong><br />
		<table width="100%" class="print-table">
			<tr>
				<td width="100">{{(DATA.current_invoice.invoice.is_proforma_invoice?"Pr.":"")}} Invoice Code</td>
				<td><strong>#{{DATA.current_invoice.invoice.invoice_code}}</strong></td>
			</tr>
			<tr>
				<td>{{(DATA.current_invoice.invoice.is_proforma_invoice?"Pr.":"")}} Invoice Date</td>
				<td><strong>{{fn.formatDate(DATA.current_invoice.invoice.invoice_date, "d MM yy")}}</strong></td>
			</tr>
			<tr ng-show='!DATA.current_invoice.invoice.is_proforma_invoice'>
				<td>Due Date</td>
				<td><strong>{{fn.formatDate(DATA.current_invoice.invoice.invoice_due_date, "d MM yy")}}</strong></td>
			</tr>
			<tr>
				<td>Status</td>
				<td>
                    <span ng-show='DATA.current_invoice.invoice.is_proforma_invoice'>
                        <strong>{{DATA.current_invoice.invoice.proforma_invoice_status_desc}}</strong>
                    </span>
                    <span ng-show='!DATA.current_invoice.invoice.is_proforma_invoice'>
                        <strong ng-show="DATA.current_invoice.invoice.invoice_due_date >= DATA.current_invoice.now && DATA.current_invoice.invoice.invoice_total == DATA.current_invoice.invoice.balance">UNPAID</strong>
                        <strong ng-show="DATA.current_invoice.invoice.balance == 0">PAID</strong>
                        <strong ng-show="DATA.current_invoice.invoice.invoice_due_date >= DATA.current_invoice.now && DATA.current_invoice.invoice.invoice_total > DATA.current_invoice.invoice.balance && DATA.current_invoice.invoice.balance > 0">PENDING</strong>
                        <strong ng-show="DATA.current_invoice.invoice.invoice_due_date < DATA.current_invoice.now && DATA.current_invoice.invoice.balance > 0">EXPIRED</strong>
                    </span>
				</td>
			</tr>
		</table>

		<hr/>
		<strong>BUSINESS SOURCE</strong><br />
		<table width="100%" class="print-table" ng-show="DATA.current_invoice.invoice.agent">
			<tr>
				<td width="90">Name</td>
				<td><strong>{{DATA.current_invoice.invoice.agent.name}}</strong></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><strong>{{DATA.current_invoice.invoice.agent.email}}</strong></td>
			</tr>
			<tr>
				<td>Address</td>
				<td><strong>{{DATA.current_invoice.invoice.agent.address||"-"}}</strong></td>
			</tr>
			<tr>
				<td>Telephone</td>
				<td><strong>{{DATA.current_invoice.invoice.agent.phone||"-"}}</strong></td>
			</tr>
		</table>
		<hr />
		
		<hr />
		<strong>RESERVATION</strong><br />
		<table class="table table-bordered table-condensed" width="100%" cellspacing="1" cellpadding="8">
			<tr ng-repeat="invoice_detail in DATA.current_invoice.invoice.detail">
				<td colspan="2">
					<table width="100%" class="print-table">
						<tr>
							<td colspan="2"><strong>{{invoice_detail.booking_code}}</strong></td>
						</tr>
						<tr>
							<td>Customer</td>
							<td align="right">{{invoice_detail.customer.first_name}} {{invoice_detail.customer.last_name}}</td>
						</tr>
                        <tr ng-repeat="booking_detail in invoice_detail.detail">
                        	<td>
                            	{{booking_detail.voucher_code}}<br />
                            	<small>{{fn.formatDate(invoice_detail.detail[0].date, "d M yy")}}</small>
                            </td>
                            <td align="right">
                                <div ng-show="invoice_detail.detail[0].product_type == 'ACT'">
                                    {{invoice_detail.detail[0].product.name}}
                                </div>
                                <div ng-show="invoice_detail.detail[0].product_type != 'ACT'">
                                    {{invoice_detail.detail[0].departure.port.port_code}} <small>({{invoice_detail.detail[0].departure.time}})</small>
                                    -
                                    {{invoice_detail.detail[0].arrival.port.port_code}} <small>({{invoice_detail.detail[0].arrival.time}})</small>
                                </div>
                            </td>
                        </tr>
                        
						<?php /*?><tr>
							<td>Subtotal</td>
							<td align="right">{{invoice_detail.currency}} {{fn.formatNumber(invoice_detail.subtotal, invoice_detail.currency)}}</td>
						</tr>
						<tr ng-show="invoice_detail.discount>-1">
							<td>Discount</td>
							<td align="right">{{invoice_detail.currency}} ({{fn.formatNumber(invoice_detail.discount, invoice_detail.currency)}})</td>
						</tr>
						<tr>
							<td>Already paid</td>
							<td align="right">{{invoice_detail.currency}} ({{fn.formatNumber(invoice_detail.total_payment, invoice_detail.currency)}})</td>
						</tr><?php */?>
						<tr>
							<td><strong>Total Amount</strong></td>
							<td align="right"><strong>{{invoice_detail.currency}} {{fn.formatNumber(invoice_detail.grand_total, invoice_detail.currency)}}</strong></td>
						</tr>
					</table>
				</td>
			</tr>
			<tbody>
				<tr class="info table-header">
					<td align="right"><strong>Grand Total</strong></td>
					<td align="right">
						<strong class="ng-binding">
							{{DATA.current_invoice.invoice.currency}} {{fn.formatNumber(DATA.current_invoice.invoice.grand_total, DATA.current_invoice.invoice.currency)}}
						</strong>
					</td>
				</tr>
				<tr class="info table-header">
					<td align="right"><strong>Total Payment</strong></td>
					<td align="right">
						<strong class="ng-binding">
							{{DATA.current_invoice.invoice.currency}} ({{fn.formatNumber(DATA.current_invoice.invoice.total_payment, DATA.current_invoice.invoice.currency)}})
						</strong>
					</td>
				</tr>
				<tr class="info table-header">
					<td align="right"><strong>Outstanding Invoice</strong></td>
					<td align="right">
						<strong class="ng-binding">
							{{DATA.current_invoice.invoice.currency}} {{fn.formatNumber(DATA.current_invoice.invoice.balance, DATA.current_invoice.invoice.currency)}}
						</strong>
					</td>
				</tr>
			</tbody>
		</table>
		<br/>
		<table class="table" ng-show="DATA.current_invoice.invoice.remarks" width="100%">
			<tbody>
				<tr class="info table-header">
					<td><strong>Remarks</strong></td>
				</tr>
				<tr>
					<td>{{DATA.current_invoice.invoice.remarks}}</td>
				</tr>
			</tbody>
		</table>
	</div>
	
	<br />
	<div class="normal-paper" ng-show="print_type != 'small'">
		
		<div class="header">
			<div class="pull-right text-center" align="center">
				<div style="font-size:16px">{{(DATA.current_invoice.invoice.is_proforma_invoice?"PROFORMA":"")}} INVOICE</div>
				<div>
					<img src="<?=base_url()?>public/plugin/barcode.php?text={{DATA.current_invoice.invoice.invoice_code}}&size=40" />
				</div>
				<div style="font-size:16px">
					{{DATA.current_invoice.invoice.invoice_code}}
				</div>
			</div>
			
			<div class="title"><?=$vendor["business_name"]?></div>
			<div><?=$vendor["address"]?>, <?=$vendor["regency"]?>, <?=$vendor["province"]?>, <?=$vendor["country"]?></div>
			<div>[P] : <?=$vendor["telephone"]?>, [E] : <?=$vendor["email"]?></div>
			<div><?=$vendor["website"]?></div>
		</div>
		
		<table width="100%">
			<tr>
				<td>
					<table width="100%" class="print-table" ng-show="DATA.current_invoice.invoice.agent">
						<tr>
							<td colspan="2">
								<strong>BUSINESS SOURCE</strong>
							</td>
						</tr>
						<tr>
							<td width="100">Name</td>
							<td><strong>{{DATA.current_invoice.invoice.agent.name}}</strong></td>
						</tr>
						<tr>
							<td>Email</td>
							<td><strong>{{DATA.current_invoice.invoice.agent.email}}</strong></td>
						</tr>
						<tr>
							<td>Address</td>
							<td><strong>{{DATA.current_invoice.invoice.agent.address||'-'}}</strong></td>
						</tr>
						<tr>
							<td>Telephone</td>
							<td><strong>{{DATA.current_invoice.invoice.agent.phone||'-'}}</strong></td>
						</tr>
					</table>
				</td>
				<td width="250">
					<table class="print-table">
						<tr>
							<td colspan="2">
								<strong>{{(DATA.current_invoice.invoice.is_proforma_invoice?"PROFORMA":"")}} INVOICE INFORMATION</strong>
							</td>
						</tr>
						<tr>
							<td width="100">{{(DATA.current_invoice.invoice.is_proforma_invoice?"Pr.":"")}} Invoice Code</td>
							<td width="150"><strong>#{{DATA.current_invoice.invoice.invoice_code}}</strong></td>
						</tr>
						<tr>
							<td>{{(DATA.current_invoice.invoice.is_proforma_invoice?"Pr.":"")}} Invoice Date</td>
							<td><strong>{{fn.formatDate(DATA.current_invoice.invoice.invoice_date, "d MM yy")}}</strong></td>
						</tr>
						<tr ng-hide='DATA.current_invoice.invoice.is_proforma_invoice'>
							<td>Due Date</td>
							<td><strong>{{fn.formatDate(DATA.current_invoice.invoice.invoice_due_date, "d MM yy")}}</strong></td>
						</tr>
						<tr>
							<td>Status</td>
							<td>
                            	<span ng-show='DATA.current_invoice.invoice.is_proforma_invoice'>
                                	<strong>{{DATA.current_invoice.invoice.proforma_invoice_status_desc}}</strong>
                                </span>
                            	<span ng-show='!DATA.current_invoice.invoice.is_proforma_invoice'>
                                    <strong ng-show="DATA.current_invoice.invoice.invoice_due_date >= DATA.current_invoice.now && DATA.current_invoice.invoice.invoice_total == DATA.current_invoice.invoice.balance">UNPAID</strong>
                                    <strong ng-show="DATA.current_invoice.invoice.balance == 0">PAID</strong>
                                    <strong ng-show="DATA.current_invoice.invoice.invoice_due_date >= DATA.current_invoice.now && DATA.current_invoice.invoice.invoice_total > DATA.current_invoice.invoice.balance && DATA.current_invoice.invoice.balance > 0">PENDING</strong>
                                    <strong ng-show="DATA.current_invoice.invoice.invoice_due_date < DATA.current_invoice.now && DATA.current_invoice.invoice.balance > 0">EXPIRED</strong>
								</span>
                            </td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
		<br />
		
		<?php $this->load->view("crs/ar/v_invoice_detail_order_list") ?>
        
		<?php /*?><table class="table table-bordered table-condensed" width="100%" cellspacing="1" cellpadding="8">
			<tr class="info table-header">
				<td colspan="6"><strong>RESERVATION</strong></td>
			</tr>
			<tr class="info table-header">
                <td align="center" width="100">Booking#</td>
                <td>Customer</td>
                <td align="center" width="110">Voucher</td>
                <td align="center" width="100">Date</td>
                <td width="150">Trip</td>
                <td width="110" align="right">Total</td>
            </tr>
			<tbody ng-repeat="invoice_detail in DATA.current_invoice.invoice.detail">
                <tr>
                    <td align="center" rowspan="{{invoice_detail.detail.length}}">
                        <a ui-sref="trans_reservation.detail({'booking_code':invoice_detail.booking_code})" target="_blank"><strong>{{invoice_detail.booking_code}}</strong></a>
                    </td>
                    <td rowspan="{{invoice_detail.detail.length}}">{{invoice_detail.customer.first_name}} {{invoice_detail.customer.last_name}}</td>
                    <td align="center">{{invoice_detail.detail[0].voucher_code}}</td>
                    <td align="center" width="100">{{fn.formatDate(invoice_detail.detail[0].date, "d M yy")}}</td>
                    <td>
                    	<div ng-show="invoice_detail.detail[0].product_type == 'ACT'">
                            {{invoice_detail.detail[0].product.name}}
                        </div>
                        <div ng-show="invoice_detail.detail[0].product_type != 'ACT'">
                            {{invoice_detail.detail[0].departure.port.port_code}} <small>({{invoice_detail.detail[0].departure.time}})</small>
                            -
                            {{invoice_detail.detail[0].arrival.port.port_code}} <small>({{invoice_detail.detail[0].arrival.time}})</small>
                        </div>
                    </td>
                    <td align="right" rowspan="{{invoice_detail.detail.length}}">{{invoice_detail.currency}} {{fn.formatNumber(invoice_detail.grandtotal, invoice_detail.currency)}}</td>
                </tr>
                <tr ng-repeat="booking_detail in invoice_detail.detail" ng-if="$index >= 1">
                    <td align="center">{{booking_detail.voucher_code}}</td>
                    <td align="center" width="100">{{fn.formatDate(booking_detail.date, "d M yy")}}</td>
                    <td width="100">
                        <div ng-show="invoice_detail.detail[0].product_type == 'ACT'">
                            {{booking_detail.product.name}}
                        </div>
                        <div ng-show="invoice_detail.detail[0].product_type != 'ACT'">
                            {{booking_detail.departure.port.port_code}} <small>({{invoice_detail.detail[0].departure.time}})</small>
                            -
                            {{booking_detail.arrival.port.port_code}} <small>({{invoice_detail.detail[0].arrival.time}})</small>
                        </div>
                    </td>
                </tr>
            </tbody>
			<tbody>
				<tr class="info table-header">
					<td colspan="5" align="right"><strong>Grand Total</strong></td>
					<td align="right">
						<strong class="ng-binding">
							{{DATA.current_invoice.invoice.invoice_curr}} {{fn.formatNumber(DATA.current_invoice.invoice.invoice_total, DATA.current_invoice.invoice.invoice_curr)}}
						</strong>
					</td>
				</tr>
				<tr class="info table-header">
					<td colspan="5" align="right"><strong>Total Payment</strong></td>
					<td align="right">
						<strong class="ng-binding">
							{{DATA.current_invoice.invoice.invoice_curr}} ({{fn.formatNumber(DATA.current_invoice.invoice.total_payment, DATA.current_invoice.invoice.invoice_curr)}})
						</strong>
					</td>
				</tr>
				<tr class="info table-header">
					<td colspan="5" align="right"><strong>Balance</strong></td>
					<td align="right">
						<strong class="ng-binding">
							{{DATA.current_invoice.invoice.invoice_curr}} {{fn.formatNumber(DATA.current_invoice.invoice.balance, DATA.current_invoice.invoice.invoice_curr)}}
						</strong>
					</td>
				</tr>
			</tbody>
		</table>
		<br/>
		<table class="table" ng-show="DATA.current_invoice.invoice.remarks" width="100%">
			<tbody>
				<tr class="info table-header">
					<td><strong>Remarks</strong></td>
				</tr>
				<tr>
					<td><pre style='font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;font-size: 13px;padding: 0;margin: 0;border: 0;background-color: transparent;line-height: 1;padding-bottom: 10px'>{{DATA.current_invoice.invoice.remarks}}</pre></td>
				</tr>
			</tbody>
		</table><?php */?>
		
        <hr />
        
		<div ng-show="DATA.current_invoice.invoice.bank_account">
            You may pay the invoices as following method :<br /><br />
            <strong>Wire Transfer</strong><br />
            {{DATA.current_invoice.invoice.bank_account.account_name}}<br />
            Account : {{DATA.current_invoice.invoice.bank_account.account_number}}<br />
            {{DATA.current_invoice.invoice.bank_account.bank_name}}<br />
            {{DATA.current_invoice.invoice.bank_account.branch}}<br />
        </div>
        
        <div class="no-print" style="margin-top:5px">
        	<a href="" data-toggle="modal" data-target="#edit_bank_account">
            	<span class="glyphicon glyphicon-pencil"></span> Edit Bank Account
            </a>
        </div>
	</div>
</div>

<?php $this->load->view("crs/ar/v_edit_invoice_form_edit_bank_account") ?>
<?php $this->load->view("crs/ar/v_invoice_send_email") ?>

<div class="print-toolbox no-print" ng-show='show_print_button'>
    <div class="pull-right">
    	<a href="" data-toggle="modal" ng-click='sendEmailInvoice()' data-target="#frm-invoice-send-email">
        	<button type="button" class="btn btn-md btn-info">
            	<i class="fa fa-send" aria-hidden="true"></i> Send Email
            </button>
        </a>
    
        <button onclick="window.print()" ng-disabled="!show_print_button" type="button" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-print"></span> Print Now</button>
    </div>
    <div>
        <select class="form-control input-md change-paper-size" ng-model='print_type' style="width:auto; display:inline">
            <option value="">Normal Paper</option>
            <option value="small"> Small Paper</option>
        </select>
	</div>
    <div style="clear:both"></div>
</div>

<script>
	$(".change-paper-size").change(function(){
		var _this = $(this);
		
		if (_this.val() == 'small'){
			$(".print_area").addClass("small_paper");
		}else{
			$(".print_area").removeClass("small_paper");
		}
	});
</script>

<style>
	.print_area{background:none !important; padding:0 !important;}
	.small_paper{width:8cm !important; margin:0 auto !important}
	.normal-paper{background:#FFF; padding:10px}
	.small-paper{width:8cm; margin:auto; padding:10px; background:#FFF}
	.small-paper .header .title{font-size:18px !important}
	#btn-print-top{display:none}
	tr.success td, tr.info td, tr.warning td, tr.danger td{background:none !important}
	tr.success, tr.info, tr.warning, tr.danger{background:#EEE !important}
</style>


<?php
//pre($vendor)
?>
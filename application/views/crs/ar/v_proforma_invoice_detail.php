<div class="sub-title">Proforma Invoice Detail </div>
<br />
<div ng-init="loadDataInvoiceDetail(true)" class="invoice-detail">

	<div ng-show='!(DATA.current_invoice)'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>

	<div ng-show='(DATA.current_invoice)'>
		<div class="pull-right" xxng-show='DATA.current_invoice.invoice.proforma_invoice_status == "NEW" || DATA.current_invoice.invoice.proforma_invoice_status == "REVISED"'>
        
        	<div class="btn-group">
			  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fa fa-print" aria-hidden="true"></i> Print <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu">
				<li>
                	<a href="<?=site_url("home/print_page/#/print/ar_invoice/")?>{{DATA.current_invoice.invoice.invoice_code}}" target="_blank" >#{{DATA.current_invoice.invoice.invoice_code}}</a>
                </li>
			  </ul>
			</div>
        
			<?php /*?><div class="btn-group">
			  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fa fa-bars" aria-hidden="true"></i> Menu <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu">
				<li>
					<a href="<?=site_url("home/print_page/#/print/invoice_agent/")?>{{DATA.current_invoice.invoice.invoice_code}}" target="_blank" >
					<i class="fa fa-print" aria-hidden="true"></i> Print
					</a>
				</li>
				<li ng-show="DATA.current_invoice.invoice.bank_account.id">
					<a href="<?=site_url("home/resend_email/#/email/invoice_agent/")?>{{DATA.current_invoice.invoice.invoice_code}}" target="_blank" >
						<i class="fa fa-send" aria-hidden="true"></i> Send Email
					</a>
				</li>
				<li ng-show="!DATA.current_invoice.invoice.bank_account.id">
					<a href="" data-toggle="modal" ng-click='EditRekNumber(true)' data-target="#edit_bank_account">
						<i class="fa fa-send" aria-hidden="true"></i> Send Email
					</a>
				</li>
                
                <li ng-show='DATA.current_invoice.invoice.proforma_invoice_status == "NEW" || DATA.current_invoice.invoice.proforma_invoice_status == "REVISED"' role="separator" class="divider"></li>
                <li ng-show='DATA.current_invoice.invoice.proforma_invoice_status == "NEW" || DATA.current_invoice.invoice.proforma_invoice_status == "REVISED"'>
                	<a href="" data-toggle="modal" ng-click='createInvoiceFromProformaInvoice()' data-target="#modal-create-invoice">
						<i class="fa fa-plus" aria-hidden="true"></i> Create Invoice
					</a>
                </li>
			  </ul>
			</div><?php */?>
		</div>
		<div class="title">
			<h1>#{{DATA.current_invoice.invoice.invoice_code}} - {{DATA.current_invoice.invoice.agent.name}}</h1>
		</div>
		<div class="row">
        	<div class="col-sm-6">
                <div class="sub-title">Detail </div>
                <table class="table">
                    <tr>
                        <td width="100">Code</td>
                        <td width="10">:</td>
                        <td><strong>{{DATA.current_invoice.invoice.invoice_code}}</strong></td>
                    </tr>
                    <tr>
                        <td>Created date</td>
                        <td>:</td>
                        <td><strong>{{fn.formatDate(DATA.current_invoice.invoice.invoice_date, "d MM yy")}}</strong></td>
                    </tr>
                    <tr>
                    	<td>Status</td>
                        <td>:</td>
                        <td><span class="label label-default">{{DATA.current_invoice.invoice.proforma_invoice_status_desc}}</span></td>
                    </tr>
                    <tr>
                        <td>Created by</td>
                        <td>:</td>
                        <td>
                            <strong>{{DATA.current_invoice.invoice.created_by}}</strong><br />
                            <small>{{fn.newDate(DATA.current_invoice.invoice.created_on) | date : 'dd MMMM yyyy HH:mm'}}</small>
                        </td>
                    </tr>
                </table>
			</div>
            <div class="col-sm-6">
                <div class="sub-title">AGENT INFORMATION</div>
                <table class="table">
                    <tr>
                        <td width="100">Agent Code</td>
                        <td width="10">:</td>
                        <td><strong>{{DATA.current_invoice.invoice.agent.agent_code}}</strong></td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>:</td>
                        <td><strong><a ui-sref="agent.detail({'agent_code':DATA.current_invoice.invoice.agent.agent_code})" target="_blank">{{DATA.current_invoice.invoice.agent.name}}</strong></a></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>:</td>
                        <td><strong>{{DATA.current_invoice.invoice.agent.email}}</strong></td>
                    </tr>
                    <tr ng-show='DATA.current_invoice.invoice.agent.address'>
                        <td>Adress</td>
                        <td>:</td>
                        <td><strong>{{DATA.current_invoice.invoice.agent.address}}</strong></td>
                    </tr>
                    <tr ng-show='DATA.current_invoice.invoice.agent.real_category'>
                        <td>Category</td>
                        <td>:</td>
                        <td><strong>{{DATA.current_invoice.invoice.agent.real_category.name}}</strong></td>
                    </tr>
                    <tr>
                        <td colspan="3">&nbsp;</td>
                    </tr>
                </table>
        	</div>
		</div>
        
        <?php $this->load->view("crs/ar/v_invoice_detail_order_list") ?>
        
        <hr />
        <div align="center" ng-show='DATA.current_invoice.invoice.proforma_invoice_status == "NEW" || DATA.current_invoice.invoice.proforma_invoice_status == "REVISED"'>
        	<div class="add-product-button"> 
            	<a  data-toggle="modal" ng-click='createInvoiceFromProformaInvoice()' data-target="#modal-create-invoice" class="btn btn-success btn-lg btn-block"> <span class="glyphicon glyphicon-saved"></span> Convert To Invoice </a> 
            </div>
            <hr />
            
            <div class="add-product-button">
            	<a ui-sref="ar.proforma_invoice_add" class="btn btn-default btn-lg btn-block"> <span class="glyphicon glyphicon-plus"></span> Create Another Proforma Invoice </a>
            </div>
            <hr />
        </div>
        <a ui-sref="ar.proforma_invoice"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back to list</a>
        <div class="pull-right" align="right">
            <a data-ng-click="loadDataInvoiceDetail(true)" style="cursor: pointer"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh</a>
            &nbsp;&nbsp;
            <a href="" ng-click="deleteInvoice(DATA.current_invoice.invoice)" style="color:#F00"><i class="fa fa-trash" aria-hidden="true"></i> Delete Pro. Invoice</a>
        </div>
        <script>GeneralJS.activateLeftMenu("invoice");</script>
        <script>$("#invoice_title").html("PROFORMA INVOICE")</script>
	</div>
</div>




<!-- modal add payment -->	
<div class="modal fade" id="modal-create-invoice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <form ng-submit='saveCreateInvoice($event)'>
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="myModalLabel">
			Create Invoice
		</h4>
	  </div>
	  <div class="modal-body">
	  	<div ng-show='DATA.myInvoice.error_desc.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myInvoice.error_desc'>{{err}}</li></ul></div>
		<table class="table table-borderless table-condenseds">
			<tr>
				<td width="150">Proforma Invoice Code</td>
                <td><strong>{{DATA.current_invoice.invoice.invoice_code}}</strong></td>
			</tr>
            <tr>
				<td>Grand Total</td>
                <td><strong>{{DATA.current_invoice.invoice.currency}} {{fn.formatNumber(DATA.current_invoice.invoice.grand_total, DATA.current_invoice.invoice.currency)}}</strong></td>
			</tr>
            <tr>
				<td>Agent Name</td>
                <td><strong>{{DATA.current_invoice.invoice.agent.agent_code}} - {{DATA.current_invoice.invoice.agent.name}}</strong></td>
			</tr>
            <tr>
                <td>Bank Account</td>
                <td>
                    <select class="form-control input-sm" ng-model="DATA.myInvoice.bank_id" ng-required="DATA.vendor_account_number">
                        <option value="">-- Select Bank Account --</option>
                        <option ng-show="bank.id" ng-repeat="bank in DATA.vendor_account_number" value="{{bank.id}}">{{bank.bank_name+" - "+bank.account_number}}</option>
                    </select>
                </td>
            </tr>
            <tr>
            	<td>Due Date</td>
                <td>
	            	<input type="text" ng-model='DATA.myInvoice.due_date' class="form-control input-sm datepicker" style="width:150px" placeholder="Due Date" />
            	</td>
            </tr>
            <tr>
            	<td>Remarks</td>
                <td>
                	<textarea class="form-control input-sm" rows="3" ng-model='DATA.myInvoice.remarks'></textarea>
            	</td>
            </tr>
		</table>
	  </div>
      <div class="modal-footer" style="text-align:center">
        <button type="submit" class="btn btn-primary">Create Invoice</button>
        <button tabindex="101" type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
	</div>
  </div>
  </form>
</div>
<style>
	.invoice-detail .title{margin-bottom:20px}
	.invoice-detail .title h1{margin-bottom:10px !important;}
	.invoice-detail .title .code{margin-bottom:5px;}
</style>

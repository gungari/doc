	<div class="modal fade" id="modal-search-agent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Search Agent</h4>
                </div>
                <div class="modal-body">
                    <div class="form-inline">
                        Search :
                        <input type="text" class="form-control input-md" ng-model="modal_agent_search.name" style="width:250px" placeholder="Search..." />
                        <select class="form-control input-md" ng-model="modal_agent_search.real_category.id" style="width:200px">
                            <option value="">All Category</option>
                            <option value="" disabled="disabled">---</option>
                            <option value="{{real_categories.id}}" ng-repeat="real_categories in DATA.agent_real_categories">{{real_categories.name}}</option>
                        </select>
                    </div>
                    <hr />
                    <table class="table table-bordered tbl-search-agent">
                        <tr class="success">
                            <td width="40"><strong>#</strong></td>
                            <td><strong>Agent Name</strong></td>
                            <td width="180"><strong>Category</strong></td>
                        </tr>
                        <tr ng-repeat="agent in DATA.agents.agents | filter : modal_agent_search | limitTo:50" class="agent-list"
                            ng-click="InvoiceListSearchAgentPopUpSelectAgent(agent)" data-dismiss="modal">
                            <td>{{($index+1)}}</td>
                            <td>
                                <strong>{{agent.name}}</strong><br />
                                <small>{{agent.agent_code}}</small>
                            </td>
                            <td><span ng-repeat='_cat in agent.real_categories'> {{_cat.name}}<span ng-hide="agent.real_categories.length==($index+1)">,</span> </span></td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer" style="text-align:center">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <style>
        table.tbl-search-agent tr.agent-list{cursor:pointer;}
        table.tbl-search-agent tr.agent-list:hover{background:#d9edf7}
    </style>
<div class="sub-title">Proforma Invoice Detail </div>
<br />
<div ng-init="loadDataInvoiceDetail(true)" class="invoice-detail">

	<div ng-show='!(DATA.current_invoice)'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>

	<div ng-show='(DATA.current_invoice)'>
		<div class="pull-right" ng-show='DATA.current_invoice.invoice.proforma_invoice_status == "NEW" || DATA.current_invoice.invoice.proforma_invoice_status == "REVISED"'>
			<div class="btn-group">
			  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fa fa-bars" aria-hidden="true"></i> Menu <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu">
				<li>
					<a href="<?=site_url("home/print_page/#/print/invoice_agent/")?>{{DATA.current_invoice.invoice.invoice_code}}" target="_blank" >
					<i class="fa fa-print" aria-hidden="true"></i> Print
					</a>
				</li>
				<li ng-show="DATA.current_invoice.invoice.bank_account.id">
					<a href="<?=site_url("home/resend_email/#/email/invoice_agent/")?>{{DATA.current_invoice.invoice.invoice_code}}" target="_blank" >
						<i class="fa fa-send" aria-hidden="true"></i> Send Email
					</a>
				</li>
				<li ng-show="!DATA.current_invoice.invoice.bank_account.id">
					<a href="" data-toggle="modal" ng-click='EditRekNumber(true)' data-target="#edit_bank_account">
						<i class="fa fa-send" aria-hidden="true"></i> Send Email
					</a>
				</li>
                
                <li ng-show='DATA.current_invoice.invoice.proforma_invoice_status == "NEW" || DATA.current_invoice.invoice.proforma_invoice_status == "REVISED"' role="separator" class="divider"></li>
                <li ng-show='DATA.current_invoice.invoice.proforma_invoice_status == "NEW" || DATA.current_invoice.invoice.proforma_invoice_status == "REVISED"'>
                	<a href="" data-toggle="modal" ng-click='createInvoiceFromProformaInvoice()' data-target="#modal-create-invoice">
						<i class="fa fa-plus" aria-hidden="true"></i> Create Invoice
					</a>
                </li>
			  </ul>
			</div>
		</div>
		<div class="title">
			<h1>#{{DATA.current_invoice.invoice.invoice_code}} - {{DATA.current_invoice.invoice.agent.name}}</h1>
		</div>
		<div class="row">
        	<div class="col-sm-6">
                <div class="sub-title">Detail </div>
                <table class="table">
                    <tr>
                        <td width="100">Code</td>
                        <td width="10">:</td>
                        <td><strong>{{DATA.current_invoice.invoice.invoice_code}}</strong></td>
                    </tr>
                    <tr>
                        <td>Created date</td>
                        <td>:</td>
                        <td><strong>{{fn.formatDate(DATA.current_invoice.invoice.invoice_date, "d MM yy")}}</strong></td>
                    </tr>
                    <tr>
                    	<td>Status</td>
                        <td>:</td>
                        <td><span class="label label-default">{{DATA.current_invoice.invoice.proforma_invoice_status_desc}}</span></td>
                    </tr>
                    <tr>
                        <td>Created by</td>
                        <td>:</td>
                        <td>
                            <strong>{{DATA.current_invoice.invoice.created_by}}</strong><br />
                            <small>{{fn.newDate(DATA.current_invoice.invoice.created_on) | date : 'dd MMMM yyyy HH:mm'}}</small>
                        </td>
                    </tr>
                </table>
			</div>
            <div class="col-sm-6">
                <div class="sub-title">AGENT INFORMATION</div>
                <table class="table">
                    <tr>
                        <td width="100">Agent Code</td>
                        <td width="10">:</td>
                        <td><strong>{{DATA.current_invoice.invoice.agent.agent_code}}</strong></td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>:</td>
                        <td><strong>{{DATA.current_invoice.invoice.agent.name}}</strong></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>:</td>
                        <td><strong>{{DATA.current_invoice.invoice.agent.email}}</strong></td>
                    </tr>
                    <tr ng-show='DATA.current_invoice.invoice.agent.address'>
                        <td>Adress</td>
                        <td>:</td>
                        <td><strong>{{DATA.current_invoice.invoice.agent.address}}</strong></td>
                    </tr>
                    <tr ng-show='DATA.current_invoice.invoice.agent.real_category'>
                        <td>Category</td>
                        <td>:</td>
                        <td><strong>{{DATA.current_invoice.invoice.agent.real_category.name}}</strong></td>
                    </tr>
                    <tr>
                        <td colspan="3">&nbsp;</td>
                    </tr>
                </table>
        	</div>
		</div>
        
        <div class="sub-title">Reservation List</div>
        <table class="table table-bordered table-condensed">
            <tr class="info bold">
                <td align="center" width="100">Booking#</td>
                <td>Customer</td>
                <td align="center" width="120">Voucher</td>
                <td align="center" width="120">Date</td>
                <td width="150">Trip</td>
                <?php /*?><td width="130" align="right">Subtotal</td>
                <td width="120" align="right">Discount</td>
                <td width="120" align="right">Already paid</td><?php */?>
                <td width="120" align="right">Total</td>
            </tr>
            <tbody ng-repeat="invoice_detail in DATA.current_invoice.invoice.detail">
                <tr>
                    <td align="center" rowspan="{{invoice_detail.detail.length}}">
                        <?php /*?><a ui-sref="trans_reservation.detail({'booking_code':invoice_detail.booking_code})" target="_blank"><strong>{{invoice_detail.booking_code}}</strong></a><?php */?>
                        <a ui-sref="reservation.detail({'booking_code':invoice_detail.booking_code})" target="_blank"><strong>{{invoice_detail.booking_code}}</strong></a>
                        <?php /*?><br />
                        {{fn.formatDate(invoice_detail.reservation_date, "d M yy")}}<?php */?>
                    </td>
                    <td rowspan="{{invoice_detail.detail.length}}">{{invoice_detail.customer.first_name}} {{invoice_detail.customer.last_name}}</td>
                    <td align="center">{{invoice_detail.detail[0].voucher_code}}</td>
                    <td align="center" width="100">{{fn.formatDate(invoice_detail.detail[0].date, "d M yy")}}</td>
                    <td>
                        <div ng-show="invoice_detail.detail[0].product_type == 'ACT'">
                            {{invoice_detail.detail[0].product.name}}
                        </div>
                        <div ng-show="invoice_detail.detail[0].product_type != 'ACT'">
                            {{invoice_detail.detail[0].departure.port.port_code}} <small>({{invoice_detail.detail[0].departure.time}})</small>
                            -
                            {{invoice_detail.detail[0].arrival.port.port_code}} <small>({{invoice_detail.detail[0].arrival.time}})</small>
                        </div>
                    </td>
                    <?php /*?><td align="right">{{invoice_detail.currency}} {{fn.formatNumber(invoice_detail.subtotal, invoice_detail.currency)}}</td>
                    <td align="right">{{invoice_detail.currency}} {{fn.formatNumber(invoice_detail.discount, invoice_detail.currency)}}</td>
                    <td align="right">{{invoice_detail.currency}} {{fn.formatNumber(invoice_detail.total_payment, invoice_detail.currency)}}</td><?php */?>
                    <td align="right" rowspan="{{invoice_detail.detail.length}}">{{invoice_detail.currency}} {{fn.formatNumber(invoice_detail.grand_total, invoice_detail.currency)}}</td>
                </tr>
                <tr ng-repeat="booking_detail in invoice_detail.detail" ng-if="$index >= 1">
                    <td align="center">{{booking_detail.voucher_code}}</td>
                    <td align="center" width="100">{{fn.formatDate(booking_detail.date, "d M yy")}}</td>
                    <td width="100">
                        <div ng-show="invoice_detail.detail[0].product_type == 'ACT'">
                            {{booking_detail.product.name}}
                        </div>
                        <div ng-show="invoice_detail.detail[0].product_type != 'ACT'">
                            {{booking_detail.departure.port.port_code}} <small>({{invoice_detail.detail[0].departure.time}})</small>
                            -
                            {{booking_detail.arrival.port.port_code}} <small>({{invoice_detail.detail[0].arrival.time}})</small>
                        </div>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr class="success">
                    <td colspan="5" align="right"><strong>Grand Total</strong></td>
                    <td align="right"><strong>{{DATA.current_invoice.invoice.currency}} {{fn.formatNumber(DATA.current_invoice.invoice.grand_total, DATA.current_invoice.invoice.currency)}}</strong></td>
                </tr>
            </tfoot>
        </table>
        
        <br />
        <table class="table">
            <tr class="header bold">
                <td>Remarks</td>
            </tr>
            <tr>
                <td>
                	<div ng-show="DATA.current_invoice.invoice.remarks">
	                	<pre style='font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;font-size: 13px;padding: 0;margin: 0;border: 0;background-color: transparent;line-height: 1;padding-bottom: 10px'>{{DATA.current_invoice.invoice.remarks}}</pre>
                	</div>
                    <div ng-show="!DATA.current_invoice.invoice.remarks">-</div>
                </td>
            </tr>
        </table>
        <hr />
        <a ui-sref="ar.proforma_invoice"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back to list</a>
        <a class="pull-right" data-ng-click="loadDataInvoiceDetail(true)" style="cursor: pointer"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh</a>
        <script>GeneralJS.activateLeftMenu("invoice");</script>
        <script>$("#invoice_title").html("PROFORMA INVOICE")</script>
	</div>
</div>




<!-- modal add payment -->	
<div class="modal fade" id="modal-create-invoice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <form ng-submit='saveCreateInvoice($event)'>
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="myModalLabel">
			Create Invoice
		</h4>
	  </div>
	  <div class="modal-body">
	  	<div ng-show='DATA.myInvoice.error_desc.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myInvoice.error_desc'>{{err}}</li></ul></div>
		<table class="table table-borderless table-condenseds">
			<tr>
				<td width="150">Proforma Invoice Code</td>
                <td><strong>{{DATA.current_invoice.invoice.invoice_code}}</strong></td>
			</tr>
            <tr>
				<td>Grand Total</td>
                <td><strong>{{DATA.current_invoice.invoice.currency}} {{fn.formatNumber(DATA.current_invoice.invoice.grand_total, DATA.current_invoice.invoice.currency)}}</strong></td>
			</tr>
            <tr>
				<td>Agent Name</td>
                <td><strong>{{DATA.current_invoice.invoice.agent.agent_code}} - {{DATA.current_invoice.invoice.agent.name}}</strong></td>
			</tr>
            <tr>
                <td>Bank Name</td>
                <td>
                    <select class="form-control input-sm" ng-model="DATA.myInvoice.bank_id" ng-required="DATA.vendor_account_number">
                        <option value="">-- Select Bank Account --</option>
                        <option ng-show="bank.id" ng-repeat="bank in DATA.vendor_account_number" value="{{bank.id}}">{{bank.bank_name+" - "+bank.account_number}}</option>
                    </select>
                </td>
            </tr>
            <tr>
            	<td>Due Date</td>
                <td>
	            	<input type="text" ng-model='DATA.myInvoice.due_date' class="form-control input-sm datepicker" style="width:150px" placeholder="Due Date" />
            	</td>
            </tr>
            <tr>
            	<td>Remarks</td>
                <td>
                	<textarea class="form-control input-sm" rows="3" ng-model='DATA.myInvoice.remarks'></textarea>
            	</td>
            </tr>
		</table>
	  </div>
      <div class="modal-footer" style="text-align:center">
        <button type="submit" class="btn btn-primary">Create Invoice</button>
        <button tabindex="101" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
	</div>
  </div>
  </form>
</div>
<style>
	.invoice-detail .title{margin-bottom:20px}
	.invoice-detail .title h1{margin-bottom:10px !important;}
	.invoice-detail .title .code{margin-bottom:5px;}
</style>

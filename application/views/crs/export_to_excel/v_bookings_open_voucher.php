<?php

if (!isset($_GET["TEST"])){

	$file="Open Voucher Bookings - ".$vendor["business_name"].".xls";
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=$file");

}
//pre($data["vendor"]);
?>

<?php if (isset($bookings)){ ?>

	<table border="1" cellspacing="1">
		<tr style="background:#DDD">
			<td>NO</td>
			<td>ORDER #	</td>
			<td>RESV. DATE</td>	
			<td>CUSTOMER NAME</td>
			<td>NATIONALITY</td>
			<td>SOURCE</td>
			<td>QTY</td>
			<td colspan="2">PRICE</td>
			<td>TOTAL</td>
			<td>USED</td>
			<td>REMAINS</td>
			<td>STATUS</td>
		</tr>
		<?php foreach ($bookings as $index => $booking){ ?>
		
		<?php
		$bg_status = "none";
		if ($booking["status_code"] == 'CANCEL') $bg_status = 'red';
		?>
		
		<tr style="background:<?=$bg_status?>">
			<td><?=($index+1)?></td>
			<td><?=$booking["booking_code"]?></td>
			<td><?=date("Y-m-d", strtotime($booking["transaction_date"]))?></td>
			<td><?=$booking["customer"]["full_name"]?></td>
			<td><?=$booking["customer"]["country_code"]?></td>
			<td><?=$booking["source"]?></td>
			<td><?=$booking["open_voucher"]["qty"]?></td>
			<td><?=$booking["open_voucher"]["currency"]?></td>
			<td><?=$booking["open_voucher"]["price"]?></td>
			<td><?=$booking["grand_total"]?></td>
			<td><?=$booking["open_voucher"]["used"]?></td>
			<td><?=$booking["open_voucher"]["qty"] - $booking["open_voucher"]["used"]?></td>
			<td><?=$booking["status_initial"]?></td>
		</tr>
		<?php } ?>
	</table>
<?php
//pre($bookings);
?>

<?php } ?>


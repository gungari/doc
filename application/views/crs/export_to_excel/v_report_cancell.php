
<?php
if (!isset($_GET["TEST"])){

	$file="Cancel - ".$vendor["business_name"].".xls";
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=$file");

}
//pre($data["vendor"]);

?>

<?php if (isset($sales)){ ?>
	<?php if ($search['view'] != 'date') { ?>
		<table border="1" cellspacing="1">
		<tr class="header">
			<td ><b>Date</b></td>
			<td align="right"><b>Booking</b></td>
			<th class="text-right">Sales</th>
				<th class="text-right">Total</th>
		</tr>
		
		
		<?php foreach ($sales as $revenue_data){ ?>
			<tr >
				<td rowspan="3" width="180"><?=$revenue_data['date']?></td>
				<td>
					<strong><?=$revenue_data['sources']['OFFLINE']['name'] ?></strong><hr /><span style="margin-right: 20px;">Booking : <?=$revenue_data['sources']['OFFLINE']['book'] ?></span><small> Guest(s) : <?=$revenue_data['sources']['OFFLINE']['guest'] ?></small>
				</td>
				<td class="text-right"><?=$revenue_data['sources']['currency'] ?> <?=$revenue_data['sources']['OFFLINE']['total'] ?></td>
				<td rowspan="3" width="250" align="right" >
					<?=$revenue_data['sources']['currency']?> <?=$revenue_data['sources']['total']?>
						<hr style="margin:2px 0">
						<small>
							<span style="margin-right: 20px;">Booking(s): <?=$revenue_data['sources']['total_book']?></span>
							<span>Guest(s): <?=$revenue_data['sources']['total_guest']?></span>
						</small>
				</td>
			</tr>
			<tr>
				<td>
					<strong><?=$revenue_data['sources']['ONLINE']['name'] ?></strong><hr /><span style="margin-right: 20px;">Booking : <?=$revenue_data['sources']['ONLINE']['book'] ?></span><small> Guest(s) : <?=$revenue_data['sources']['ONLINE']['guest'] ?></small>
				</td>
				<td class="text-right"><?=$revenue_data['sources']['currency'] ?> <?=$revenue_data['sources']['ONLINE']['total'] ?></td>
			</tr>
			<tr>
				<td>
					<strong><?=$revenue_data['sources']['AGENT']['name'] ?></strong><hr /><span style="margin-right: 20px;">Booking : <?=$revenue_data['sources']['AGENT']['book'] ?></span><small> Guest(s) : <?=$revenue_data['sources']['AGENT']['guest'] ?></small>
				</td>
				<td class="text-right"><?=$revenue_data['sources']['currency'] ?> <?=$revenue_data['sources']['AGENT']['total'] ?></td>
			</tr>
			
		<?php } ?>
	</table>
	<?php }else{ ?>
		<table border="1" cellspacing="1">
			<tr class="header">
				
				<td><b>Order#</b></td>
				<td><b>Customer Name</b></td>
				<td align="right"><b>Amount</b></td>
				<td><b>Total Trip</b></td>
			</tr>
			<?php foreach ($sales as $value) { ?>
				<tr>
					
						<td><strong><?=$value['code']?></strong></td>
						<td><?=$value['firs_name']?> <?=$value['last_name']?><hr /><small>
							<span style="margin-left: 10px;">Adult(s): <?=$value['qty1']?></span>
							<span style="margin-left: 10px;">Child(s): <?=$value['qty2']?></span>
							<span style="margin-left: 10px;">Invant(s): <?=$value['qty3']?></span>
						</small></td>
						<td class="text-right"><?=$value['currency']?> <?=$value['total']?> </td>
						<td class="text-center"><?=$value['book']?></td>
				</tr>
			<?php } ?>
		</table>
	<?php } ?>
<?php
//pre($revenue);
?>

<?php } ?>

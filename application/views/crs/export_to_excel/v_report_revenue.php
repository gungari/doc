
<?php
if (!isset($_GET["TEST"])){

	$file="Revenue - ".$vendor["business_name"].".xls";
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=$file");

}
//pre($data["vendor"]);

?>

<?php if (isset($revenue)){ ?>

	<table border="1" cellspacing="1">
		<tr style="background:#DDD">
			<td rowspan="2">No</td>
			<td rowspan="2" align="center">Booking#
				<br>
				Date
			</td>
			<td rowspan="2" align="center">Voucher#
				<br>
				Date
			</td>
			
			<!-- <td rowspan="2" align="center">Date</td> -->
			<td rowspan="2">Customer Name</td>
			<td rowspan="2" align="center" title="Nationality">Nationality</td>
			<?php if ($vendor['category'] == 'transport') { ?>
				<td rowspan="2" colspan="2" align="center">Trip</td>
			<?php } ?>
			<td rowspan="2">Booking Source</td>
			<td colspan="3" align="center" title="Passenger">Passenger</td>
			<td colspan="3" align="center" title="Passenger">Price</td>
			<td rowspan="2" align="center">Pickup</td>
			<td rowspan="2" align="center">Dropoff</td>
			<td rowspan="2" align="center">Other</td>
			<td rowspan="2" align="center">Sub Total</td>
			<td rowspan="2" align="center">Discount</td>
			<td rowspan="2" align="center">Fee</td>
			<td rowspan="2" align="center">TOTAL</td>
			<td rowspan="2">Passengers</td>
		</tr>
		<tr style="background:#DDD">
			<td align="center">Adl.</td>
			<td align="center">Chi.</td>
			<td align="center">Inf.</td>
			<td align="center">Adl.</td>
			<td align="center">Chi.</td>
			<td align="center">Inf.</td>
		</tr>
		
		<?php foreach ($revenue as $revenue_data){ ?>
		<tr style="background:#FAFAFA">
			<td colspan="22">
				<?php if ($vendor['category'] != 'activities') { ?>
					<strong>
					<?=$revenue_data["departure_port"]["name"]?> (<?=$revenue_data["departure_time"]?>)
					-
					<?=$revenue_data["arrival_port"]["name"]?> (<?=$revenue_data["arrival_time"]?>)
				</strong>
				<?php }else{ ?>
						<?=$revenue_data["product_name"]?>
				<?php } ?>
				
			</td>
		</tr>
		<?php foreach ($revenue_data["revenue"] as $index => $vcr){ ?>
		
		<tr>
			<td><?=($index+1)?></td>
		
				<td align="center">
					<strong><?=$vcr["booking"]["booking_code"]?></strong>
					<br />
					<small><?=date("Y-m-d", strtotime($vcr["booking"]["transaction_date"]))?></small>
				</td>
		
			<td align="center">
				<strong><?=$vcr["detail"]["voucher_code"]?></strong>
				<br>
				<small><?=date("Y-m-d", strtotime($vcr["detail"]["date"]))?></small>
			</td>
			<!-- <td></td> -->
			<td><?=$vcr["booking"]["customer"]["full_name"]?></td>
			<td><?=$vcr["booking"]["customer"]["country_code"]?></td>
			<?php if ($vendor['category'] == 'transport') { ?>
				<td><?=$vcr["detail"]["departure"]["port"]["port_code"]?> (<?=$vcr["detail"]["departure"]["time"]?>)</td>
				<td><?=$vcr["detail"]["arrival"]["port"]["port_code"]?> (<?=$vcr["detail"]["arrival"]["time"]?>)</td>
			<?php } ?>
			<td>
				<?php if (isset($vcr["booking"]["agent"]["name"])){ ?>
					<?=$vcr["booking"]["agent"]["name"]?>
				<?php }else{ ?>
					<?=$vcr["booking"]["source"]?>
				<?php } ?>
			</td>
			<td align="center"><?=$vcr["detail"]["qty_1"]?></td>
			<td align="center"><?=$vcr["detail"]["qty_2"]?></td>
			<td align="center"><?=$vcr["detail"]["qty_3"]?></td>
			<td align="right"><?=$vcr["detail"]["rates"]["rates_1"]?></td>
			<td align="right"><?=$vcr["detail"]["rates"]["rates_2"]?></td>
			<td align="right"><?=$vcr["detail"]["rates"]["rates_3"]?></td>
			<td align="right"><?=$vcr["detail"]["pickup_price"]?></td>
			<td align="right"><?=$vcr["detail"]["dropoff_price"]?></td>
			<td align="right"><?=(isset($vcr["detail"]["additional_charge_price"])?number_format($vcr["detail"]["additional_charge_price"],0,'',''):0)?></td>
			<td align="right"><?=$vcr["detail"]["subtotal"]?></td>
			<td align="right"><?=(isset($vcr["detail"]["discount"])?number_format($vcr["detail"]["discount"],0,'',''):0)?></td>
			<td align="right"><?=(isset($vcr["detail"]["hybrid_fee"])?number_format($vcr["detail"]["hybrid_fee"],0,'',''):0)?></td>
			<td align="right"><?=number_format($vcr["detail"]["total"],0,'','')?></td>
			<td>
				<?php
				$arr_passenger = array();
				foreach ($vcr["detail"]["passengers"] as $passenger){
					$arr_passenger[] = $passenger["first_name"]." ".$passenger["last_name"];
				}
				?>
				<?=implode(", ", $arr_passenger)?>
			</td>
		</tr>
		
		<?php } ?>
		<tr style="background:#FFFFFF">
			<?php if ($vendor['category'] != 'transport') { ?>

				<td colspan="6"><strong>TOTAL</strong></td>
			<?php }else{ ?>
				<td colspan="8"><strong>TOTAL</strong></td>
			<?php } ?>
			<td align="center"><b><?=$revenue_data["summary"]["qty_1"]?></b></td>
			<td align="center"><b><?=$revenue_data["summary"]["qty_2"]?></b></td>
			<td align="center"><b><?=$revenue_data["summary"]["qty_3"]?></b></td>
			
			<td align="right"><b><?=$revenue_data["summary"]["rates_1"]?></b></td>
			<td align="right"><b><?=$revenue_data["summary"]["rates_2"]?></b></td>
			<td align="right"><b><?=$revenue_data["summary"]["rates_3"]?></b></td>
			
			<td align="right"><b><?=$revenue_data["summary"]["pickup_price"]?></b></td>
			<td align="right"><b><?=$revenue_data["summary"]["dropoff_price"]?></b></td>
			<td align="right"><b><?=(isset($revenue_data["summary"]["additional_charge_price"])?number_format($revenue_data["summary"]["additional_charge_price"],0,'',''):0)?></b></td>
			<td align="right"><b><?=(isset($revenue_data["summary"]["subtotal"])?number_format($revenue_data["summary"]["subtotal"],0,'',''):0)?></b></td>
			<td align="right"><b><?=(isset($revenue_data["summary"]["discount"])?number_format($revenue_data["summary"]["discount"],0,'',''):0)?></b></td>
			<td align="right"><b><?=(isset($revenue_data["summary"]["hybrid_fee"])?number_format($revenue_data["summary"]["hybrid_fee"],0,'',''):0)?></b></td>
			
			<td align="right"><b><?=number_format($revenue_data["summary"]["fee"],0,'','')?></b></td>
			<td></td>
		</tr>
		<?php } ?>
	</table>
<?php
//pre($revenue);
?>

<?php }else if(isset($sales)){ ?>
	<table border="1" cellspacing="1">
			<tr class="header">
				<th>Date</th>
				
				<th class="text-right">Total Revenue</th>
			</tr>
			<?php foreach ($sales as $value) { ?>
				<tr>
					<td><strong><?=$value['date'] ?></strong></td>
					

					<td class="text-right" style="vertical-align: middle"><?=$value['sources']['currency'] ?> <?=$value['sources']['total'] ?> </td>

				</tr>
			<?php } ?>
		</table>
<?php } ?>
<?php

if (!isset($_GET["TEST"])){

	$file="Passenger List (".$search['date'].") - ".$vendor["business_name"].".xls";
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=$file");
}
?>

<?php if (isset($pickup_dropoff)){ ?>
	<h4 class="text-capitalize">
	<?=$search['type'] ?> - <?=date("d M y", strtotime($search["date"]))?></h4>
	<table border="1" cellspacing="1">
	
	<tr style="background:#FAFAFA;vertical-align: top;" class="header bold">
			<td width="30"><strong>No</strong></td></strong>
			<td style="text-align: center" width="100"><strong>Booking Number</strong></td>
			<td style="text-align: center" width="100"><strong>Voucher#</strong></td>
			<td width="120"><strong>Guest Name</strong></td>
			<td style="text-align: center" colspan="3"><strong>Pax(S)</strong></td>
			<td width="160"><strong>Pick Up Address</strong></td>
			<td width="80"><strong>Area</strong></td>
			<td width="120"><strong>Dept Date</strong></td>
			<td width="60"><strong>Dept Time</strong></td>
			<?php if($vendor['category'] == 'transport'){ ?>
					<td style="text-align: center;" colspan="2">Trip</td>
				<?php }else{ ?>
					<td style="text-align: center;">Product</td>
				<?php } ?>
		</tr>
		<tr style="text-align: center;background:#FAFAFA;" class="header bold">
			<td rowspan="1"></td>
			<td rowspan="1"></td>
			<td rowspan="1"></td>
			<td rowspan="1"></td>
			<td rowspan="1"><strong>Adl</strong></td>
			<td rowspan="1"><strong>Chi</strong></td>
			<td rowspan="1"><strong>Inf</strong></td>
			<td rowspan="1"></td>
			<td rowspan="1"></td>
			<td rowspan="1"></td>
			<td rowspan="1"></td>
			<?php if($vendor['category'] == 'transport'){ ?>
					<td rowspan="1">Dept</td>
					<td rowspan="1">Arrv</td>
			<?php }else{ ?>
				<td></td>
			<?php } ?>
		</tr>
		<?php foreach ($pickup_dropoff as $data){ ?>
		<tr style="background:#FAFAFA">
			<?php if($vendor['category'] == 'transport'){ ?>
			<td colspan="11">
				<strong><?=$data["departure_port"]["name"]?> <?=$data["departure_time"]?></strong>
				-
				<strong><?=$data["arrival_port"]["name"]?> <?=$data["arrival_time"]?></strong>
			</td>
			<?php } ?>
		</tr>
		<?php foreach ($data["passenger_list"] as $index => $pickup){ ?>
			<tr style="vertical-align: top;">
			<td style="text-align: center"><?=($index+1)?></td>
			<td style="text-align: center">
				<?=$pickup["booking_code"]?>
			</td>
			<td style="text-align: center">
				<?=$pickup["voucher_code"]?>
			</td>
			<td>
				<?=$pickup["customer"]["first_name"]?> <?=$pickup["customer"]["last_name"]?>
			</td>
			<td>
				<?php if ($pickup["qty_opt_1"]>0){ ?>
					<?=$pickup["qty_opt_1"]?>
				<?php }else{ ?>
					-
				<?php } ?>
			</td>
			<td>
				<?php if ($pickup["qty_opt_2"]>0){ ?>
					<?=$pickup["qty_opt_2"]?>
				<?php }else{ ?>
					-
				<?php } ?>
			</td>
			<td>
				<?php if ($pickup["qty_opt_3"]>0){ ?>
					<?=$pickup["qty_opt_3"]?>
				<?php }else{ ?>
					-
				<?php } ?>
			</td>
			<td>
				<?=$pickup["hotel_name"]?><br> <?=$pickup["hotel_address"]?>
			</td>
			<td>
				<?=$pickup["area"]?>
			</td>
			<td style="text-align: center">
				<?=date("Y-m-d", strtotime($pickup["date"]))?>
			</td>
			<td style="text-align: center">
				<?=$pickup["time"]?>
			</td>
			<?php if($vendor['category'] == 'transport'){ ?>
			<td>
				<?=$pickup["departure"]['port']['name']?>
			</td>
			<td>
				<?=$pickup["arrival"]['port']['name']?>
			</td>
			<?php }else{ ?>
				<td>
					<?=$data["product_name"]?>
				</td>
			<?php } ?>
		</tr>
		<?php } ?>
		
		
		<?php } ?>
	</table>

<?php } ?>

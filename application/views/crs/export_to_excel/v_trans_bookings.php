<?php

if (!isset($_GET["TEST"])){

	$file="Bookings - ".$data["vendor"]["business_name"].".xls";
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=$file");

}
//pre($data["vendor"]);
?>

<?php if (isset($data["data_bookings"])){ ?>

	<table border="1" cellspacing="1">
		<tr style="background:#DDD">
			<td rowspan="2">NO</td>
			<td rowspan="2">ORDER #	</td>
			<td rowspan="2">VOUCHER CODE</td>	
			<td rowspan="2">BOOKING DATE</td>
			<td rowspan="2">CUSTOMER NAME</td>
			<td rowspan="2">EMAIL ADDRESS</td>
			<td rowspan="2">ARRIVAL DATE</td>
			<td rowspan="2" colspan="2">TRIP</td>
			<td rowspan="2">RATES NAME</td>
			<td colspan="3" align="center">PASSENGER</td>
			<td rowspan="2">TOTAL GUEST(S)</td>
			<td colspan="5" align="center">PRICE</td>
			<td rowspan="2">SUB TOTAL</td>
			<td rowspan="2">DISCOUNT (%)</td>
			<td rowspan="2">GRAND TOTAL</td>
			<td rowspan="2">STATUS</td>
		</tr>
		<tr style="background:#DDD">
			<td>ADULT</td>
			<td>CHILD</td>
			<td>INFANT</td>
			<td>ADULT</td>
			<td>CHILD</td>
			<td>INFANT</td>
			<td>PICKUP</td>
			<td>DROPOFF</td>
		</tr>
		<?php foreach ($data["data_bookings"] as $index => $booking){ ?>
		
		<?php
		$bg_status = "none";
		if ($booking["cancel_status"] == 'CAN') $bg_status = 'red';
		?>
		
		<tr style="background:<?=$bg_status?>">
			<td><?=($index+1)?></td>
			<td><?=$booking["booking_code"]?></td>
			<td><?=$booking["detail"]["voucher_code"]?></td>
			<td><?=date("Y-m-d", strtotime($booking["transaction_date"]))?></td>
			<td><?=$booking["customer"]["full_name"]?></td>
			<td><?=$booking["customer"]["email"]?></td>
			<td><?=$booking["detail"]["date"]?></td>
			<td><?=$booking["detail"]["departure"]["port"]["port_code"]?> (<?=$booking["detail"]["departure"]["time"]?>)</td>
			<td><?=$booking["detail"]["arrival"]["port"]["port_code"]?> (<?=$booking["detail"]["arrival"]["time"]?>)</td>
			<td><?=$booking["detail"]["rates"]["name"]?></td>
			
			<td><?=($booking["detail"]["qty_1"]>0)?$booking["detail"]["qty_1"]:""?></td>
			<td><?=($booking["detail"]["qty_2"]>0)?$booking["detail"]["qty_2"]:""?></td>
			<td><?=($booking["detail"]["qty_3"]>0)?$booking["detail"]["qty_3"]:""?></td>

			<td><?=($booking["detail"]["qty_1"]+$booking["detail"]["qty_2"]+$booking["detail"]["qty_3"])?></td>

			<?php
			$sub_total_rates_1 = ($booking["detail"]["rates"]["rates_1"]*$booking["detail"]["qty_1"]);
			$sub_total_rates_2 = ($booking["detail"]["rates"]["rates_2"]*$booking["detail"]["qty_2"]);
			$sub_total_rates_3 = ($booking["detail"]["rates"]["rates_3"]*$booking["detail"]["qty_3"]);
			$pickup_price = ($booking["detail"]["pickup_price"]);
			$dropiff_price = ($booking["detail"]["dropoff_price"]);
			?>
			
			<td><?=$sub_total_rates_1?></td>
			<td><?=$sub_total_rates_2?></td>
			<td><?=$sub_total_rates_3?></td>
			
			<td><?=$pickup_price?></td>
			<td><?=$dropiff_price?></td>
			
			<td><?=$booking["detail"]["subtotal"]?></td>
			<td><?=$booking["detail"]["percent_discount"]?></td>
			
			<td><?=($booking["detail"]["subtotal"] - ($booking["detail"]["subtotal"]*$booking["detail"]["percent_discount"]/100))?></td>
			<td><?=$booking["status"]?></td>
		</tr>
		<?php } ?>
	</table>
<?php
//pre($data);
?>

<?php } ?>


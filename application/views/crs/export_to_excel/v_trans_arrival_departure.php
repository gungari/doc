<?php

if (!isset($_GET["TEST"])){

	$file="Passenger List ($search[date]) - ".$vendor["business_name"].".xls";
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=$file");

}
//pre($data["vendor"]);
?>

<?php if (isset($passenger_list)){ ?>

	<table border="1" cellspacing="1">
		<tr style="background:#DDD">
			<td>No</td>
			<td>Voucher#</td>
			<td>Name</td>
			<td align="center" title="Nationality">Nat.</td>
			<td>ID Number</td>
			<td>Phone</td>
			<td>Booking Source</td>
			<td colspan="2" align="center">Trip</td>
			<td>Pickup / Dropoff</td>
			<td align="center">Check In</td>
		</tr>
		<?php foreach ($passenger_list as $passenger){ ?>
		
		<tr style="background:#FAFAFA">
			<td colspan="11">
				<strong><?=$passenger["departure_port"]["name"]?> (<?=$passenger["departure_time"]?>) </strong>
				-
				<strong><?=$passenger["arrival_port"]["name"]?> (<?=$passenger["arrival_time"]?>) </strong>
			</td>
		</tr>
		
		<?php foreach ($passenger["passenger_list"] as $index => $arr_dept){ ?>
		
		<?php
		$bg_status = "none";
		if ($arr_dept["checkin_status"] != '1') $bg_status = '#EEE';
		?>
		
		<tr style="background:<?=$bg_status?>">
			<td><?=($index+1)?></td>
			<td><?=$arr_dept["voucher_code"]?></td>
			<td><?=$arr_dept["first_name"]?> <?=$arr_dept["last_name"]?></td>
			<td title="<?=$arr_dept["country_name"]?>" align="center"><?=$arr_dept["country_code"]?></td>
			<td><?=$arr_dept["passport_number"]?></td>
			<td><?=$arr_dept["phone"]?></td>
			<td>
				<?php if (!isset($arr_dept["agent"])){ ?>
					<?=$arr_dept["source"]?>
				<?php }else{ ?>
					<?=$arr_dept["agent"]["name"]?>
				<?php } ?>
			</td>
			<td align="center"><?=$arr_dept["departure"]["port_code"]?> (<?=$arr_dept["departure"]["time"]?>)</td>
			<td align="center"><?=$arr_dept["arrival"]["port_code"]?> (<?=$arr_dept["arrival"]["time"]?>)</td>
			<td>
				<?php if (isset($arr_dept["pickup"])){ ?>
					<?=$arr_dept["pickup"]["hotel_name"]?>
				<?php }elseif (isset($arr_dept["dropoff"])){ ?>
					<?=$arr_dept["dropoff"]["hotel_name"]?>
				<?php } ?>
			</td>
			<td align="center">
				<?php if ($arr_dept["checkin_status"] == "1"){ echo "YES"; } ?>
			</td>
		</tr>
		<?php } ?>
		
		<?php } ?>
	</table>
<?php
//pre($passenger_list);
?>

<?php } ?>


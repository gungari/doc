<?php

if (!isset($_GET["TEST"])){

	$file="Expected";
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=$file");
	
}
?>
<?php if(isset($expected)){ ?>
 	<strong><?=$vendor['business_name']?> Expected Report</strong>
	<div>
		<?php foreach ($expected['data']['data'] as $key => $data) { ?>
			
		<table class="table table-bordered report-margin" style="font-size:12px">
			
				<tr class="header">
					<?php if($vendor['category'] == 'activities'){ ?>
						<td colspan="7"><span><b><?=$data['name']?></b></span></td>
					<?php }else{ ?>
						<td colspan="7"><span><b><?=$data['code']?></b></span></td>
					<?php } ?>
						
				</tr>
				<tr class="header">
					<td align="center" rowspan="2" width="20"><b>No.</b></td>
					<td align="center"><b>Voucher #</b></td>
					<td align="center"><b>Order #</b></td>
					<td align="center" colspan="4"><b>Customer</b></td>
				</tr>
				<tr class="header">
					
					<td align="center"><b>Arrival Date</b></td>
					<td align="center"><b>Booking Date</b></td>
					<td align="center"><b>Name</b></td>
					<td align="center"><b>Adult</b></td>
					<td align="center"><b>Child</b></td>
					<td align="center"><b>Infant</b></td>
				</tr>
				<?php foreach ($data['detail'] as $key1 => $exp) { ?>
					<tr>
						<td align="center"><?=($key1 + 1)?></td>
						<td align="center">
							<span><b><?=$exp['voucher_code']?></b></span>
							<br />
							<span><small><?=$exp['arrival_date']?><small></span>
						</td>
						<td align="center">
							<span><b><?=$exp['booking_code']?></b></span>
							<br />
							<span><small><?=$exp['booking_date']?></small></span>
						</td>
						<td align="center">
							<span><?=$exp['first_name']?> <?=$exp['last_name']?></span><br />
							<span ng-hide="exp.email == ''"><small><?=$exp['email']?></small></span><br />
							<span><small><?=$exp['country']?></small></span>
						</td>
						<td align="center"><?=$exp['qty1']?></td>
						<td align="center"><?=$exp['qty2']?></td>
						<td align="center"><?=$exp['qty3']?></td>
					</tr>
						
				<?php } ?>
					<tr>
						<td colspan="4" align="right"><b>Total</b></td>
						<td align="center"><b><?=$data['total_qty1']?></b></td>
						<td align="center"><b><?=$data['total_qty1']?></b></td>
						<td align="center"><b><?=$data['total_qty1']?></b></td>
					</tr>
		</table>
		<?php } ?>
	</div>
	<div ng-show="DATA.expected.data" class="text-right" style="font-size: 14px; margin-top:50px;">
		<table class="table table-condensed table-borderless">
			<tr>
				<td><strong>Total Adult : </strong></td>
				<td width="80"><strong><?=$expected['data']['grand_total1']?></strong></td>
			</tr>
			<tr>
				<td><strong>Total Child : </strong></td>
				<td><strong><?=$expected['data']['grand_total2']?></strong></td>
			</tr>
			<tr>
				<td><strong>Total Infant : </strong></td>
				<td><strong><?=$expected['data']['grand_total3']?></strong></td>
			</tr>
		</table>
	</div>
<?php } ?>



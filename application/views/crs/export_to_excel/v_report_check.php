
<?php
if (!isset($_GET["TEST"])){

	$file="Check-in - ".$vendor["business_name"].".xls";
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=$file");

}
//pre($data["vendor"]);

?>

<?php if (isset($sales)){ ?>
	<?php if ($search['view'] != 'date') { ?>
		<table border="1" cellspacing="1">
		<tr class="header">
			<td ><b>Date</b></td>
			<td align="right"><b>Booking</b></td>
			
				<th class="text-right">Total</th>
		</tr>
		<?php foreach ($sales as $revenue_data){ ?>

			<tr >
				<td rowspan="3" width="180"><?=$revenue_data['date']?></td>
				<td>
					<strong><?=$revenue_data['OFFLINE']['name'] ?></strong><hr /><span style="margin-right: 20px;">Booking : <?=$revenue_data['OFFLINE']['book'] ?>
						
					</span><small>
						<span> Check-in / Total: <?=$revenue_data['OFFLINE']['checkin']?> / <?=$revenue_data['OFFLINE']['guest']?>
					</span></small>
				</td>
				
				<td rowspan="3" width="250" align="right" >
					<span style="margin-right: 20px;">Booking: <?=$revenue_data['total_book']?></span>
						<hr style="margin:2px 0">
						<small>
							
							<span> Check-in / Total: <?=$revenue_data['total_checkin']?> / <?=$revenue_data['total_guest']?></span>
						</small>
				</td>
			</tr>
			<tr>
				<td>
					<strong><?=$revenue_data['ONLINE']['name'] ?></strong><hr /><span style="margin-right: 20px;">Booking : <?=$revenue_data['ONLINE']['book'] ?></span><small> Check-in / Total : <?=$revenue_data['ONLINE']['checkin']?> / <?=$revenue_data['ONLINE']['guest']?> </small>
				</td>
				
			</tr>
			<tr>
				<td>
					<strong><?=$revenue_data['AGENT']['name'] ?></strong><hr /><span style="margin-right: 20px;">Booking : <?=$revenue_data['AGENT']['book'] ?></span><small> Check-in / Total :<?=$revenue_data['AGENT']['checkin']?> / <?=$revenue_data['AGENT']['guest']?></small>
				</td>
				
			</tr>
		<?php } ?>
	</table>
	<?php }else{ ?>
		<table border="1" cellspacing="1">
			<tr class="header">
				
				<td><b>Order#</b></td>
				<td><b>Customer Name</b></td>
				
				<td><b>Total Trip</b></td>
			</tr>
			<?php foreach ($sales as $value) { ?>
				<tr>
					
						<td><strong><?=$value['code']?></strong></td>
						<td><?=$value['firs_name']?> <?=$value['last_name']?><hr /><small>
							<span style="margin-left: 10px;">Adult(s): <?=$value['qty1']?></span>
							<span style="margin-left: 10px;">Child(s): <?=$value['qty2']?></span>
							<span style="margin-left: 10px;">Invant(s): <?=$value['qty3']?></span>
						</small></td>
						
						<td class="text-right">Check-in / Total : <?=$value['check1']+$value['check2']+$value['check3']?> / <?=$value['qty1']+$value['qty2']+$value['qty3']?></td>
				</tr>
			<?php } ?>
		</table>
	<?php } ?>
<?php
//pre($revenue);
?>

<?php } ?>

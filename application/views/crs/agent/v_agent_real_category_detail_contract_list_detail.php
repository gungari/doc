<br />
<div ng-init="loadDataContractRatesDetail()">
	<div class="title" ng-show='DATA.current_contract_rates.contract_rates_code'>
        <div class="pull-right">
        	<a href="" ng-click="publishUnpublishContractRates(DATA.current_contract_rates, 0)" ng-show="DATA.current_contract_rates.publish_status == '1'">
                <div style="color:#090"><img src="<?=base_url("public/images/on-button.png")?>" /></span> </div>
            </a> 
            <a href="" ng-click="publishUnpublishContractRates(DATA.current_contract_rates, 1)" ng-show="DATA.current_contract_rates.publish_status != '1'">
                <div style="color:#F30"><img src="<?=base_url("public/images/off-button.png")?>" width="100%" /></span></div>
            </a> 
        </div>
        <h1>{{DATA.current_contract_rates.name}}</h1>
        <div class="code">Contract Rates Code : {{DATA.current_contract_rates.contract_rates_code}}</div>
        <div class="code">
        	Periode : 	{{fn.newDate(DATA.current_contract_rates.start_date) | date:'dd MMMM yyyy'}} -
            			{{fn.newDate(DATA.current_contract_rates.end_date) | date:'dd MMMM yyyy'}}
		</div>
        <div class="desc" ng-show="DATA.current_contract_rates.description">
            <em>{{DATA.current_contract_rates.description}}</em>
        </div>
    </div>
	
    <hr />
    
    <div ng-show='DATA.current_contract_rates.show_loading'><em>Loading...</em></div>
    
    <?php $this->load->view("crs/agent/v_agent_real_category_detail_contract_list_detail_act"); ?>
    <?php $this->load->view("crs/agent/v_agent_real_category_detail_contract_list_detail_trans"); ?>
    
</div>



<style>
	.contract-select-rates-list ol{padding:0}
	.contract-select-rates-list ol li{padding:10px; list-style-position:inside; border:solid 1px #efeaea; margin:0 0 5px 0; cursor:pointer}
	.contract-select-rates-list ol li:hover{border:solid 1px #4DB6AC}
	.contract-select-rates-list ol li.selected{border:solid 2px #4DB6AC; background:#E8EAF6}
</style>
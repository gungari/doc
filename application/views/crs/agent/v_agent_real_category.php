<?php //sync:180827 ?>
<div class="sub-title">Rates Category</div> 
<br />
<div ui-view>

	<div ng-init="loadDataRealCategory()">
		<div ng-show="DATA.real_categories.status=='SUCCESS'">
			<div class="products">
				<div class="product ">
					Filter : <input type="text" ng-model="filter_categories" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
				</div>
				
				<div ng-show='!DATA.real_categories.real_categories'>
					<img src="<?=base_url("public/images/loading_bar.gif")?>" />
				</div>
				
				

				<table class="table table-bordered table-condensed">
					<tr class="info" style="font-weight:bold">
						<td width="30">No.</td>
            <td width="100">Code</td>
						<td>Rates Category Name</td>
						<td width="80"></td>
						<td width="80"></td>
					</tr>
				</table>
				<form id="sortable-rates" ng-submit="saveReorderRealCategory();" onSubmit="return false;"> 
					<div ng-repeat="category in DATA.real_categories.real_categories | filter : filter_categories " style='background:#FFF'>
					
					<input type='hidden' class='real_category_codes' name='real_category_codes[]' ng-value='category.real_category_code' /> 

						<table class="table table-bordered table-condensed">
							<tr ng-class="{'danger':category.publish_status!='1'}">
								<td width="30">{{($index+1)}}</td>
								<td width="100" align="center">{{category.real_category_code}}</td>
								<td>
									<a ui-sref="agent.real_category.detail.contract_rates({'real_category_code':category.real_category_code})">
										{{category.name}}
									</a>
								</td>
								<td  width="80" align="center">
									<a href="" ng-click="publishUnpublishRealCategory(category, 0)" ng-show="category.publish_status == '1'">
										<div style="color:#090"><img src="<?=base_url("public/images/on-button.png")?>" width="100%" /></span> </div>
									</a> 
									<a href="" ng-click="publishUnpublishRealCategory(category, 1)" ng-show="category.publish_status != '1'">
										<div style="color:#F30"><img src="<?=base_url("public/images/off-button.png")?>" width="100%" /></span></div>
									</a> 
								</td>
								<td  width="80" align="center">
									<a href="" data-toggle="modal" ng-click='addEditRealCategory(category)' data-target="#add-edit-category">
										<span class="glyphicon glyphicon-pencil"></span>
									</a>
									&nbsp;
									<a href="" data-toggle="modal" ng-click='deleteRealCategory(category)' style="color:red">
										<span class="glyphicon glyphicon-remove"></span>
									</a>
									&nbsp;
									<span class="glyphicon glyphicon-sort handle"></span> 
								</td>
							</tr>
						</table>

					</div>
				</form>
			</div>
		</div>
		
		<hr>
		<div class="add-product-button"> <a href="" class="btn btn-success btn-lg btn-block" data-toggle="modal" ng-click='addEditRealCategory()' data-target="#add-edit-category"> <span class="glyphicon glyphicon-plus"></span> Rates Category</a> </div>
		<br>
		<br>
		
		<div class="modal fade" id="add-edit-category" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <form ng-submit='saveDataRealCategory($event)'>
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">
					<span ng-show='!DATA.myCategory.id'>Add</span><span ng-show='DATA.myCategory.id'>Edit</span> Rates Category
				</h4>
			  </div>
			  <div class="modal-body">
				<div ng-show='DATA.myCategory.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myCategory.error_msg'>{{err}}</li></ul></div>
				<table class="table table-borderless table-condensed">
					<tr>
						<td width="120">Name*</td>
						<td><input placeholder="Name" required="required" type="text" class="form-control input-md" ng-model='DATA.myCategory.name' /></td>
					</tr>
					<tr>
						<td>Description</td>
						<td>
							<textarea placeholder="Description" class="form-control input-md" ng-model='DATA.myCategory.description' rows="5"></textarea>
						</td>
					</tr>
				</table>
			  </div>
			  <div class="modal-footer" style="text-align:center">
				<button type="submit" class="btn btn-primary">Save</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			  </div>
			</div>
		  </div>
		  </form>
		</div>
		
	</div>
	<style>span.handle{cursor:pointer} </style>
	<script>GeneralJS.activateSubMenu(".nav-pills", "li", ".real_category");</script>
	<script> 
    $( function() { 
        $( "#sortable-rates" ).sortable({ 
          handle: ".handle", 
					revert:false, 
					stop: function(){ 
               $( "#sortable-rates" ).submit(); 
				//var form = $("#sortable-rates"); 
                //console.log(form.serializeArray()); 
				//$.ajax({url : form.attr("action"),  data: form.serialize(), type: "POST", success: function(e) {/*alert(e);*/} }); 
			} 
        }); 
    } ); 
</script>

</div>
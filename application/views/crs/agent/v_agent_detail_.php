<div class="sub-title"> Business Source Detail </div>
<br />
<div ng-init="loadDataAgentDetail();" class="agent-detail">

	<div ng-show='!(DATA.current_agent)'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>

	<div ng-show='(DATA.current_agent)'>
		<div class="title">
			<h1>{{DATA.current_agent.name}}</h1>
			<div class="code"> Code : {{DATA.current_agent.agent_code}}</div>			
		</div>

		<ul class="nav nav-tabs sub-nav">
			<li role="presentation" class="detail"><a ui-sref="agent.detail({'agent_code':DATA.current_agent.agent_code})">Detail</a></li>
            <li role="presentation" class="contract-rates"><a ui-sref="agent.detail.contract_rates">Contract Rates</a></li>
			<li role="presentation" class="reservation"><a ui-sref="agent.detail.reservation">Reservation</a></li>
			<li role="presentation" class="transaction"><a ui-sref="agent.detail.transaction">Transaction</a></li>
			<li role="presentation" class="invoice"><a ui-sref="agent.detail.invoice">Invoices</a></li>
			<li role="presentation" class="email_cc"><a ui-sref="agent.detail.email_cc">Email Setting</a></li>
		</ul>
		<br /><br />
		<div ui-view>
			<div class="sub-title">Profile</div>
			<table class="table">
				<tr>
					<td width="150">Name</td>
					<td><strong>{{DATA.current_agent.name}}</strong></td>
				</tr>
				<tr>
					<td>Code</td>
					<td><strong>{{DATA.current_agent.agent_code}}</strong></td>
				</tr>
				<tr>
					<td>Email</td>
					<td><strong>{{DATA.current_agent.email}}</strong></td>
				</tr>
				<tr>
					<td>Phone</td>
					<td><strong>{{DATA.current_agent.phone}}</strong></td>
				</tr>
				<tr>
					<td>Website</td>
					<td><strong>{{DATA.current_agent.website}}</strong></td>
				</tr>
				<tr>
					<td>Address</td>
					<td><strong>{{DATA.current_agent.address}}</strong></td>
				</tr>
				<tr>
					<td>Country</td>
					<td><strong>{{DATA.current_agent.country.name}}</strong></td>
				</tr>
				<tr>
					<td>PIC</td>
					<td><strong>{{DATA.current_agent.contact_person.name}}</strong></td>
				</tr>
			</table>	
			<br />
			
			<?php /*?><div class="sub-title">Category</div>	
			<table class="table">
				<tr>
					<td width="150">Category</td>
					<td><strong>{{DATA.current_agent.category.name}}</strong></td>
				</tr>
				<tr ng-show="DATA.current_agent.category.description != ''">
					<td>Description</td>
					<td><strong>{{DATA.current_agent.category.description}}</strong></td>
				</tr>
				<tr ng-show="DATA.current_agent.category.percentage">
					<td>Contract Rates</td>
					<td><strong>{{DATA.current_agent.category.percentage}}%</strong></td>
				</tr>
			</table>
			<br /><?php */?>
			
			<div class="sub-title">Agent Payment Type</div>
			<table class="table table-borderlesss">
				<tr>
					<td width="150">Agent Payment Type</td>
					<td><strong>{{DATA.current_agent.payment_method.description}}</strong></td>
				</tr>
				<tr ng-show="DATA.current_agent.payment_method.payment_code == 'ACL'">
					<td>Credit Limit</td>
					<td><strong><?=$vendor["default_currency"]?> {{fn.formatNumber(DATA.current_agent.credit_limit, '<?=$vendor["default_currency"]?>')}}</strong></td>
				</tr>
			</table>
			
			<?php /*?><div class="sub-title">Contact Person</div>	
			<table class="table">
				<tr>
					<td width="150">Name</td>
					<td><strong>{{DATA.current_agent.contact_person.name}}</strong></td>
				</tr>
				<tr>
					<td>Phone</td>
					<td><strong>{{DATA.current_agent.contact_person.phone}}</strong></td>
				</tr>
				<tr>
					<td>Email</td>
					<td><strong>{{DATA.current_agent.contact_person.email}}</strong></td>
				</tr>
			</table>
			<br /><?php */?>
			
			<div class="sub-title">Remarks</div>		
			<table class="table">
				<tr>
					<td width="150"></td>
					<td><span ng-show="!DATA.current_agent.remarks">-</span>{{DATA.current_agent.remarks}}</td>
				</tr>
			</table>

			<div class="sub-title">Account Information</div>
			<table class="table">
				<tr>
					<td width="150">Username</td>
					<td><span>
						<b>{{DATA.current_agent.email}}</b>
					</span></td>
				</tr>
				<tr>
					<td width="150">Password</td>
					<td><span>
						******
					</span></td>
				</tr>
				<tr>
					<td width="150"></td>
					<td><span>
						<div class="add-product-button"> <a href="" class="btn btn-success btn-sm" data-toggle="modal" data-target="#checkin-popup-info" ng-click="loadDataAccount()"> <span class="glyphicon glyphicon-letter"></span> Send Account Information </a> </div>
					</span></td>
				</tr>
			</table>
			
			<br /><hr />
			<a ui-sref="agent.edit({'agent_code':DATA.current_agent.agent_code})"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Business Source</a>
			<script>activate_sub_menu_agent_detail("detail");</script>
		</div>
	</div>

	<div class="modal fade" id="checkin-popup-info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel" >
				Agent's Account Information
			</h4>
		  </div>
		  <div class="modal-body">
				<!--  <div ng-hide=''>
					<img src="<?=base_url("public/images/loading_bar.gif")?>" />
				</div>  -->
				<div class="form-group">
						<div class="alert alert-info">
							<p>This login credentials will be sent to your agent.</p><br /> <p>Password can be changed later by your side and a new password notification email will be sent back to the agent.</p>
						</div>
					</div>
				<div ng-show='account_error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in account_error_msg'>{{err}}</li></ul></div>
				<form ng-submit="sendEmailAgentAcc(DATA.current_agent.agent_code)">
					<div class="form-group row">
						<div class="col-md-2">Email</div>
						<div class="col-md-6">{{DATA.current_agent.email}}</div>
					</div>
					<div class="form-group row">
						<div class="col-md-2">Password</div>
						<div class="col-md-6"> <input class="form-control input-sm" type="password" name="" ng-model="random_pwd">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-2">Retype Password</div>
						<div class="col-md-6"> <input class="form-control input-sm" type="password" name="" ng-model="random_pwd2">
						</div>
					</div>
					 <div class="modal-footer" style="text-align:center">
						<button type="submit" class="btn btn-primary">Send</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					  </div>
				</form>
		  		
		  </div>
		</div>
	</div>
	</div>
	
</div>

<style>
	.agent-detail .title{margin-bottom:20px}
	.agent-detail .title h1{margin-bottom:10px !important;}
	.agent-detail .title .code{margin-bottom:5px;}
</style>
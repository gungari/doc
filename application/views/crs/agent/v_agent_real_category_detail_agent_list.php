<div ng-init="loadDataAgent()">
    <div>
        <div class="products">
            <div class="product">
                <form ng-submit='loadDataAgent(1, true)'>
                <div class="table-responsive">
                    <table class="table table-condensed table-borderless" width="100%">
                        <tr>
                            <td width="230">Search</td>
                            <?php /*?><td width="200">Category</td><?php */?>
                            <td width="130">Status</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><input type="text" class="form-control input-sm" placeholder="Search" ng-model='search.q' /></td>
                            <?php /*?><td>
                                <select class="form-control input-sm" ng-model='search.real_category_code'>
                                    <option value="">-- All Category --</option>
                                    <option value="{{category.real_category_code}}" ng-repeat="category in $root.DATA_real_categories | orderBy : 'name'">{{category.name}}</option>
                                </select>
                            </td><?php */?>
                            <td>
                                <select class="form-control input-sm" ng-model='search.publish_status'>
                                    <option value="all">All</option>
                                    <option value="1">Published</option>
                                    <option value="0">Unpublished</option>
                                </select>
                            </td>
                            <td><button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button></td>
                        </tr>
                    </table>
                    
                    <?php /*?>Filter : <input type="text" ng-model="DATA.filter_booking" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
                    <button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button><?php */?>
                </div>	
                </form>
            </div>
        
            <?php /*?><div class="product">
                Filter : <input type="text" ng-model="filter_agents" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
            </div><?php */?>
            
            <div ng-show='show_loading_DATA_bookings'>
                <img src="<?=base_url("public/images/loading_bar.gif")?>" />
            </div>
            
            <div ng-show='!show_loading_DATA_bookings'>
                <table class="table table-bordered table-condensed" ng-show='DATA.agents.agents'>
                    <tr class="info" style="font-weight:bold">
                        <td width="30">No.</td>
                        <td>Name</td>
                        <?php /*?><td align="center" width="100">Code</td><?php */?>
                        <td width="150">Type</td>
                        <?php /*?><td width="150">Payment Type</td><?php */?>
                        <td width="200">Rates Category</td>
                        <td width="80"></td>
                        <td width="40"></td>
                    </tr>
                    <tr ng-class="{'danger':agent.publish_status!='1'}" ng-repeat="agent in DATA.agents.agents | orderBy:['name'] | filter : filter_agents ">
                        <td>{{($index+1)}}</td>
                        <td>
                            <a ui-sref="agent.detail({'agent_code':agent.agent_code})" target="_blank"><strong>{{agent.name}}</strong></a><br />
                            {{agent.agent_code}}<br />
                            {{agent.email}}<br />
                            {{agent.phone}}
                        </td>
                        <?php /*?><td align="center">{{agent.agent_code}}</td><?php */?>
                        <?php /*?><td>{{agent.phone}}</td><?php */?>
                        <td>{{agent.payment_method.description}}</td>
                        <td>
                            <ul ng-show='agent.real_categories' style="padding-left:20px">
                                <li ng-repeat="_cat in agent.real_categories">{{_cat.name}}</li>
                            </ul>
                            <?php /*?>{{agent.real_category.name}}
                            <div ng-show="agent.last_contract_rates && agent.last_contract_rates.name !=''">
                                <hr style="margin:5px 0" />
                                <small>Contract Rates :</small><br />
                                {{agent.last_contract_rates.name}}<br />
                                <a ui-sref="agent.detail.contract_rates({'agent_code':agent.agent_code})">More...</a>
                            </div><?php */?>
                        </td>
                        <td align="center">
                            <a href="" ng-click="publishUnpublishAgent(agent, 0)" ng-show="agent.publish_status == '1'">
                                <div style="color:#090"><img src="<?=base_url("public/images/on-button.png")?>" width="100%"></span> </div>
                            </a> 
                            <a href="" ng-click="publishUnpublishAgent(agent, 1)" ng-show="agent.publish_status != '1'">
                                <div style="color:#F30"><img src="<?=base_url("public/images/off-button.png")?>" width="100%"></span></div>
                            </a> 
                        </td>
                        <td align="center">
                            <div ng-show="agent.payment_method.payment_code == 'DEPOSIT' || agent.payment_method.payment_code == 'ACL'" class="btn-group">
                             <!-- <button type="button" class="btn btn-info btn-xs dropdown-toggle" id="link_auto" title="Auto Login"><span class="glyphicon glyphicon-log-in"></span></button> -->
                              <div class="btn-group pull-right">
                                <button type="button" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="<?=site_url("login_agent/autologin")?>?
                                        s[id]={{agent.id}}&
                                        s[code]=<?=md5($vendor['code']);?>
                                        " target="_blank">Autologin Agent</a>
                                    </li>
                                </ul>
                              </div>
                            </div>
                        </td>
                    </tr>
                </table>
                
                <nav aria-label="Page navigation" class="pull-right">
                  <ul class="pagination pagination-sm">
                    <li ng-class="{'disabled':DATA.agents.search.page <= 1}">
                      <a href="" ng-click='loadDataAgent(DATA.agents.search.page-1, true)' aria-label="Prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                    </li>
                    <li ng-repeat='pagination in DATA.agents.search.pagination' ng-class="{'active':DATA.agents.search.page == pagination}">
                        <a href="" ng-click='loadDataAgent(($index+1), true)'>{{($index+1)}}</a>
                    </li>
                    <li ng-class="{'disabled':DATA.agents.search.page >= DATA.agents.search.number_of_pages}">
                      <a href="" ng-click='loadDataAgent(DATA.agents.search.page+1, true)' aria-label="Next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                    </li>
                  </ul>
                </nav>
                <div class="clearfix"></div>
                
                <div class="add-product-button"> <a href="" class="btn btn-success btn-lg btn-block" data-toggle="modal" ng-click='popUpRealCategoryAddAgentLoad()' data-target="#add-category"> <span class="glyphicon glyphicon-plus"></span> Add Agent</a> </div>
                
            </div>
            
        </div>
    </div>
    <br>
    <br>
</div>

<div class="modal fade" id="add-category" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <form ng-submit='saveDataRealCategory($event)'>
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-click='loadDataAgent(1)'><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">
            <span ng-show='!DATA.myCategory.id'>Add Agent
        </h4>
      </div>
      <div class="modal-body">
        <div>
        	<table class="table table-condensed table-borderless">
            	<tr>
                	<td width="60" style="vertical-align:middle !important">Search :</td>
                    <td><input type="text" class="form-control input-md" ng-model="modal_agent_search.name" placeholder="Search..." /></td>
                </tr>
            </table>
            <?php /*?><select class="form-control input-md" ng-model="modal_agent_search.real_category.id" style="width:200px">
                <option value="">All Category</option>
                <option value="" disabled="disabled">---</option>
                <option value="{{real_categories.id}}" ng-repeat="real_categories in DATA.agent_real_categories">{{real_categories.name}}</option>
            </select><?php */?>
        </div>
        <hr />
        <table class="table table-bordered tbl-search-agent">
            <tr class="success">
                <td width="40"><strong>#</strong></td>
                <td><strong>Agent Name</strong></td>
                <td width="100"></td>
            </tr>
            <tr ng-repeat="agent in DATA.myAgents.agents | filter : modal_agent_search | limitTo:100" 
            	class="agent-list" ng-class="{'selected':(agent.selected)}">
                <td>{{($index+1)}}</td>
                <td>
                    <strong>{{agent.agent_code}} - {{agent.name}}</strong><br />
                    Category : 
                    <span ng-show='!agent.real_categories'>-</span>
                    <span ng-repeat='_cat in agent.real_categories'> {{_cat.name}}<span ng-hide="agent.real_categories.length==($index+1)">,</span> </span>
                </td>
                <td align="center">
                	<div ng-show='agent.selected'>
                    	<button type="button" class="btn btn-sm btn-danger" style="width:100%" ng-click="popUpRealCategoryRemoveAgentSubmit(agent, $parent.$parent.DATA.current_real_category)">Remove</button>
                    </div>
                    <div ng-hide='agent.selected'>
                		<button type="button" class="btn btn-sm btn-success" style="width:100%" ng-click="popUpRealCategoryAddAgentSubmit(agent, $parent.$parent.DATA.current_real_category)">Add Agent</button>
                	</div>
                </td>
            </tr>
        </table>
      </div>
      <div class="modal-footer" style="text-align:center">
        <button type="button" class="btn btn-default" data-dismiss="modal" ng-click='loadDataAgent(1)'>Cancel</button>
      </div>
    </div>
  </div>
  </form>
</div>

<style>
table.tbl-search-agent tr.agent-list{cursor:pointer;}
table.tbl-search-agent tr.agent-list:hover{background:#d9edf7;}
table.tbl-search-agent tr.agent-list.selected{border:solid 1px #4DB6AC; background:#E8EAF6}
</style>

<script>activate_sub_menu_agent_detail("agent");</script>
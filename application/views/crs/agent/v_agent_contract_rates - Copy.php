<div class="sub-title">Contract Rates</div> 

<div ng-init="loadDataAgentContracRatesAssign()">
	<div ng-show="DATA.contract_rates_assign.status=='SUCCESS'">
        <div class="products">
            <div class="product ">
                Filter : <input type="text" ng-model="filter_categories" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
            </div>
            
            <div ng-show='!DATA.contract_rates_assign.contract_rates_assign'>
                <img src="<?=base_url("public/images/loading_bar.gif")?>" />
            </div>
            
            <table class="table table-bordered table-condensed">
                <tr class="info" style="font-weight:bold">
                    <td width="40">No.</td>
                    <td width="100">Code</td>
                    <td>Contract Rates Name</td>
                    <td width="150">Type</td>
                    <td width="160" align="center">Periode</td>
                    <td width="60"></td>
                </tr>
                <tr ng-class="{'danger':category.publish_status!='1'}" ng-repeat="category in DATA.contract_rates_assign.contract_rates_assign | orderBy:['name'] | filter : filter_categories ">
                    <td>{{($index+1)}}</td>
                    <td align="center">{{category.contract_rate_code}}</td>
                    <td>
                        <a href="" data-toggle="modal" ng-click='assignAgentContractRates(category, 1)' data-target="#add-edit-contract-rates">
                            {{category.name}}
                        </a>
                    </td>
                    
                    <td width="100">
                        {{category.contract_rate_type_desc}}
                        <?php /*?><span ng-show="category.contract_rate_type == 'PERCENT'"> 
                        	{{category.percentage}}%
                        </span><?php */?>
                    </td>
                    <td align="center">
                        {{fn.newDate(category.start_date) | date:'dd MMM yyyy'}} -
                        {{fn.newDate(category.end_date) | date:'dd MMM yyyy'}}
                    </td>
                    <td align="center">
                    	<a href="" data-toggle="modal" ng-click='assignAgentContractRates(category)' data-target="#add-edit-contract-rates">
                        	<span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        &nbsp;
                        <a href="" data-toggle="modal" ng-click='deleteAssignAgentContractRates(category)' data-target="#add-edit-category" style="color:red">
                            <span class="glyphicon glyphicon-trash"></span>
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div ng-show='!DATA.contract_rates_assign.contract_rates_assign' class="alert alert-warning"><em>Data not found...</em></div>
    <hr>
    <div class="add-product-button"> <a href="" class="btn btn-success btn-lg btn-block" data-toggle="modal" ng-click='assignAgentContractRates()' data-target="#add-edit-contract-rates"> <span class="glyphicon glyphicon-plus"></span> Contract Rates </a> </div>
    <br>
    <br>
    
    <div class="modal fade" id="add-edit-contract-rates" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <form ng-submit='saveAssignAgentContractRates($event)'>
      <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">
            	<span ng-show='!DATA.myContractRates.is_edit'>Add</span> Contract Rates - {{$parent.DATA.current_agent.name}}
            </h4>
          </div>
          <div class="modal-body">
            <div ng-show='DATA.myContractRates.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myContractRates.error_msg'>{{err}}</li></ul></div>
            <table class="table table-borderless table-condensed">
                <tr ng-show='!DATA.myContractRates.is_edit'>
                    <td width="150">Contract Rates Templates</td>
                    <td>
                    	<select ng-disabled='DATA.myContractRates.is_edit' ng-model='DATA.myContractRates' ng-change='selectContractRatesGetDetail()' 
                        	ng-options="item.name + ' (' + item.category_code + ') ' for item in DATA.agent_contract_rates | orderBy:'name'" class="form-control input-md" required style="max-width:500px">
                        	<option value="" disabled="disabled"> -- Contract Rates -- </option>
                        </select>
                    </td>
                </tr>
                <tr ng-show='DATA.myContractRates'>
                	<td width="120">Name</td>
                    <td><strong>{{DATA.myContractRates.name}}</strong></td>
                </tr>
                <tr ng-show='DATA.myContractRates'>
                	<td>Code</td>
                    <td>{{DATA.myContractRates.category_code}}</td>
                </tr>
                <tr ng-show='DATA.myContractRates'>
                	<td>Type</td>
                    <td ng-show="!edit_contract">
                    	{{DATA.myContractRates.contract_rate_type_desc}}
                        
                        <span ng-show='DATA.myContractRates.contract_rate_type == "PERCENT"'>
                        	: 
                            <span ng-show='!DATA.myContractRates.is_percentage_edit'>
                            	{{DATA.myContractRates.percentage}} %
                                &nbsp;&nbsp;&nbsp;
                                <a href="" title="Edit Percentage" ng-click='DATA.myContractRates.is_percentage_edit = true'><span class="glyphicon glyphicon-pencil"></span></a>
                            </span>
                            <span ng-show='DATA.myContractRates.is_percentage_edit'>
                                <input ng-model='DATA.myContractRates.percentage' type="number" 
                                    min="0" max="100" step="0.01" class="form-control input-sm" 
                                    style="display:inline; width:70px"
                                    ng-blur="myContractRatesChangePercentage();DATA.myContractRates.is_percentage_edit = false" /> %
                        	</span>
                        </span>
                    </td>
                    <td ng-show="edit_contract">
                        {{DATA.myContractRates.contract_rate_type_desc}}
                        <span ng-show='DATA.myContractRates.contract_rate_type == "PERCENT"'>
                            : 
                             {{DATA.myContractRates.percentage}} %
                        </span>
                    </td>
                </tr>
                <tr ng-show='DATA.myContractRates'>
                	<td>Periode</td>
                    <td ng-show="!edit_contract">
                    	<input type="text" class="form-control input-sm datepicker" ng-model='DATA.myContractRates.start_date' style="display:inline; width:120px" />
                        -
                        <input type="text" class="form-control input-sm datepicker" ng-model='DATA.myContractRates.end_date' style="display:inline; width:120px" />
                    	<?php /*?>{{fn.newDate(DATA.myContractRates.start_date) | date:'dd MMMM yyyy'}} -
                        {{fn.newDate(DATA.myContractRates.end_date) | date:'dd MMMM yyyy'}}<?php */?>
                    </td>
                    <td ng-show="edit_contract">
                        
                        {{DATA.myContractRates.start_date}} - {{DATA.myContractRates.end_date}}
                        
                        <?php /*?>{{fn.newDate(DATA.myContractRates.start_date) | date:'dd MMMM yyyy'}} -
                        {{fn.newDate(DATA.myContractRates.end_date) | date:'dd MMMM yyyy'}}<?php */?>
                    </td>
                </tr>
                <tr ng-show='DATA.myContractRates'>
                	<td>Description</td>
                    <td ng-show="!edit_contract">
                    	<textarea class="form-control input-sm" rows="5" ng-model="DATA.myContractRates.description"></textarea>
                    </td>
                    <td ng-show="edit_contract">
                        {{DATA.myContractRates.description}}
                    </td>
                </tr>
                <?php /*?><tr ng-show='DATA.myContractRates'>
                    <td width="150">Agent Payment Type</td>
                    <td ng-show="!edit_contract">
                        <select class="form-control" ng-model='DATA.myContractRates.agent_payment_type' required>
                            <option value="" disabled="disabled">-- Agent Payment Type --</option>
                            <option value="ACL">Agent Credit Limit</option>
                            <option value="REGULAR">Regular</option>
                        </select>
                    </td>
                    <td ng-show="edit_contract">
                        {{DATA.myContractRates.agent_payment_type}}
                    </td>
            	</tr><?php */?>
            </table>
            
            <div ng-show="DATA.myContractRates.contract_rates || DATA.myContractRates.contract_rates_act">
            	<hr />
            	<h4>Detail Contract Rates</h4>
                
                <table class="table table-bordered table-condensed">
                    <tbody>
                        <tr class="header">
                            <td></td>
                            <td ng-show="DATA.myContractRates.contract_rate_type == 'PERCENT'"><strong>Publish</strong></td>
                            <td ng-show="DATA.myContractRates.contract_rate_type == 'PERCENT'"></td>
                            <td ><strong>Nett</strong></td>
                            <td ng-show="DATA.myContractRates.contract_rate_type == 'PERCENT'" width="55" align="center"></td>
                        </tr>
                        <?php //RATES TRANSPORT ===============================  ?>
                        <tr ng-repeat='rates in DATA.myContractRates.contract_rates'>
                            <td>
                                <strong>{{rates.departure.port.name}} ({{rates.departure.time}}) 
                                <i class="fa fa-chevron-right"></i>
                                {{rates.arrival.port.name}} ({{rates.arrival.time}})</strong>
                                <br />
                                <span style="color:blue">
                                    <strong>{{rates.rates_code}}</strong> - <strong><em>{{rates.name}}</em></strong>
                                </span>
                            </td>
                            <td width="120">
                                <strong>One Way</strong><br />
                                <small>
                                    <div>
                                        Adult : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.one_way_rates.rates_1, rates.currency)}}</strong>
                                    </div>
                                    <div ng-show='rates.rates.one_way_rates.rates_2 > 0'>
                                        Child : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.one_way_rates.rates_2, rates.currency)}}</strong>
                                    </div>
                                    <div ng-show='rates.rates.one_way_rates.rates_3 > 0'>
                                        Infant : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.one_way_rates.rates_3, rates.currency)}}</strong>
                                    </div>
                                </small>
                                <strong>Return</strong><br />
                                <small>
                                    <div ng-show='rates.rates.return_rates.rates_1 > 0'>
                                        Adult : <strong>{{rates.currency}} {{fn.formatNumber((rates.rates.return_rates.rates_1*2), rates.currency)}}</strong>
                                    </div>
                                    <div ng-show='rates.rates.return_rates.rates_2 > 0'>
                                        Child : <strong>{{rates.currency}} {{fn.formatNumber((rates.rates.return_rates.rates_2*2), rates.currency)}}</strong>
                                    </div>
                                    <div ng-show='rates.rates.return_rates.rates_3 > 0'>
                                        Infant : <strong>{{rates.currency}} {{fn.formatNumber((rates.rates.return_rates.rates_3*2), rates.currency)}}</strong>
                                    </div>
                                </small>
                            </td>
                            <td width="30" style="vertical-align:middle; font-size:18px; text-align:center" ng-show="DATA.myContractRates.contract_rate_type == 'PERCENT'">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </td>
                            <td width="120" ng-show="DATA.myContractRates.contract_rate_type == 'PERCENT'">
                                <strong>One Way</strong><br />
                                <small>
                                    <div>
                                        Adult : <strong>{{rates.currency}} {{fn.formatNumber(rates.nett_rates.one_way_rates.rates_1, rates.currency)}}</strong>
                                    </div>
                                    <div ng-show='rates.rates.one_way_rates.rates_2 > 0'>
                                        Child : <strong>{{rates.currency}} {{fn.formatNumber(rates.nett_rates.one_way_rates.rates_2, rates.currency)}}</strong>
                                    </div>
                                    <div ng-show='rates.rates.one_way_rates.rates_3 > 0'>
                                        Infant : <strong>{{rates.currency}} {{fn.formatNumber(rates.nett_rates.one_way_rates.rates_3, rates.currency)}}</strong>
                                    </div>
                                </small>
                                <strong>Return</strong><br />
                                <small>
                                    <div ng-show='rates.rates.return_rates.rates_1 > 0'>
                                        Adult : <strong>{{rates.currency}} {{fn.formatNumber((rates.nett_rates.return_rates.rates_1*2), rates.currency)}}</strong>
                                    </div>
                                    <div ng-show='rates.rates.return_rates.rates_2 > 0'>
                                        Child : <strong>{{rates.currency}} {{fn.formatNumber((rates.nett_rates.return_rates.rates_2*2), rates.currency)}}</strong>
                                    </div>
                                    <div ng-show='rates.rates.return_rates.rates_3 > 0'>
                                        Infant : <strong>{{rates.currency}} {{fn.formatNumber((rates.nett_rates.return_rates.rates_3*2), rates.currency)}}</strong>
                                    </div>
                                </small>
                            </td>
                            <td align="center" ng-show="DATA.myContractRates.contract_rate_type == 'PERCENT'">
                                {{rates.percentage}} %
                            </td>
                        </tr>
                        <?php //-- END RATES TRANSPORT ===============================  ?>
                        
                        <?php //RATES ACTIVITIES ===============================  ?>
                        <tr ng-repeat='rates in DATA.myContractRates.contract_rates_act'>
                            <td>
                                <?php /*?><strong>{{rates.departure.port.name}} ({{rates.departure.time}}) 
                                <i class="fa fa-chevron-right"></i>
                                {{rates.arrival.port.name}} ({{rates.arrival.time}})</strong>
                                <br /><?php */?>
                                <strong>{{rates.product.name}}</strong><br />
                                <span style="color:blue">
                                    <strong>{{rates.rates_code}}</strong> - <strong><em>{{rates.name}}</em></strong>
                                </span>
                            </td>
                            <td width="120">
                            	<small>
                                    <div>
                                        {{rates.rates_caption.rates_1}} : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.rates_1, rates.currency)}}</strong>
                                    </div>
                                    <div ng-show='rates.rates.rates_2 > 0'>
                                        {{rates.rates_caption.rates_2}} : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.rates_2, rates.currency)}}</strong>
                                    </div>
                                    <div ng-show='rates.rates.rates_3 > 0'>
                                        {{rates.rates_caption.rates_3}} : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.rates_3, rates.currency)}}</strong>
                                    </div>
                                </small>
                            </td>
                            <td width="30" style="vertical-align:middle; font-size:18px; text-align:center" ng-show="DATA.myContractRates.contract_rate_type == 'PERCENT'">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </td>
                            <td width="120" ng-show="DATA.myContractRates.contract_rate_type == 'PERCENT'">
                            	<small>
                                    <div>
                                        {{rates.rates_caption.rates_1}} : <strong>{{rates.currency}} {{fn.formatNumber(rates.nett_rates.rates_1, rates.currency)}}</strong>
                                    </div>
                                    <div ng-show='rates.rates.rates_2 > 0'>
                                        {{rates.rates_caption.rates_2}} : <strong>{{rates.currency}} {{fn.formatNumber(rates.nett_rates.rates_2, rates.currency)}}</strong>
                                    </div>
                                    <div ng-show='rates.rates.rates_3 > 0'>
                                        {{rates.rates_caption.rates_3}} : <strong>{{rates.currency}} {{fn.formatNumber(rates.nett_rates.rates_3, rates.currency)}}</strong>
                                    </div>
                                </small>
                            </td>
                            <td align="center" ng-show="DATA.myContractRates.contract_rate_type == 'PERCENT'">
                                {{rates.percentage}} %
                            </td>
                        </tr>
                        <?php //-- END RATES ACTIVITIES ===============================  ?>
                    </tbody>
                </table>
                
                
            </div>
            
          </div>
          <div class="modal-footer" style="text-align:center">
            <button ng-show="!edit_contract" type="submit" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
      </form>
    </div>
    
    
    
    
    
    
</div>

<script>activate_sub_menu_agent_detail("contract-rates");</script>
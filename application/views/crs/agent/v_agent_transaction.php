<div ng-init="loadDataTransactionTransport()">
	<div class="products">
		<div class="product">
			<form ng-submit='loadDataTransactionTransport()'>
				<div class="table-responsive">
					<table class="table table-condensed table-borderless" width="100%">
						<tr>
							<td width="100">From</td>
							<td width="100">To</td>
							<td width="200">Search</td>
							<td width="130">Source</td>
							<td width="130">Payment</td>
							<td></td>
						</tr>
						<tr>
							<td><input type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='search.start_date' /></td>
							<td><input type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='search.end_date' /></td>
							<td><input type="text" class="form-control input-sm" placeholder="Search" ng-model='search.q' /></td>
							<td>
								<select class="form-control input-sm" ng-model='search.booking_source'>
									<option value="">All</option>
									<option value="{{booking_source.code}}" ng-repeat='booking_source in $root.DATA_booking_source'>{{booking_source.name}}</option>
								</select>
							</td>
							<td>
								<select class="form-control input-sm" ng-model='search.payment_method'>
									<option value="">All</option>
									<option value="{{payment_method.code}}" ng-repeat='payment_method in $root.DATA_payment_method'>{{payment_method.name}}</option>
								</select>
							</td>
							<?php /*?><td>
								<select class="form-control input-sm" ng-model='search.booking_status'>
									<option value="">All</option>
									<option value="{{booking_status.code}}" ng-repeat='booking_status in DATA.booking_status.booking_status'>{{booking_status.name}}</option>
								</select>
							</td><?php */?>
							<td><button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button></td>
						</tr>
					</table>
				</div>	
			</form>
		</div>
	</div>
	
	<div ng-show='show_loading_DATA_bookings'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	
	<div ng-show='DATA.transaction'>
		<div ng-show='!show_loading_DATA_bookings'>
			<div class="table-responsive">
				<table class="table table-condensed table-bordered">
					<tr class="header bold">
						<td width="120" align="center">Trx. ID#</td>
						<td width="120" align="center">Order#</td>
						<td>Booking Source</td>
						<td width="150">Payment</td>
						<!-- <td width="100" align="center">Status</td> -->
						<td width="120" align="right">Total Amount</td>
					</tr>
					<tbody ng-repeat="transaction in DATA.transaction.transactions.data">
						<tr ng-class="{'danger':(transaction.trx_type == 'REFUND')}">
							<td rowspan="2" align="center">
								{{transaction.trx_code}}
								<hr style="margin:2px" />
								{{fn.newDate(transaction.trx_date) | date:'dd MMM yyyy'}} <small>{{fn.newDate(transaction.trx_date) | date:'HH:mm'}}</small>
							</td>
							<td ng-show="transaction.booking_code != 'DEPOSIT' && transaction.trx_type != 'INVOICE'" align="center">
								<a ui-sref="trans_reservation.detail({'booking_code':transaction.booking_code})" target="_blank"><strong>{{transaction.booking_code}}</strong></a>
							</td>
							<td ng-show="transaction.booking_code == 'DEPOSIT'" align="center">
								<a ui-sref="agent.detail.deposit" target="_blank"><strong>{{transaction.booking_code}}</strong></a>
							</td>
							
							<td ng-show="transaction.trx_type == 'INVOICE'" align="center">
								<a ui-sref="invoice.detail({'invoice_code':transaction.booking_code})" target="_blank"><strong>{{transaction.booking_code}}</strong></a>
							</td>
							<td>
								<div ng-show="transaction.trx_type == 'REFUND'">REFUND</div>
								<div ng-show="!transaction.agent">
									{{transaction.booking_source}}<br />
									<strong>{{transaction.customer.full_name}}</strong>
								</div>
								<div ng-show="transaction.agent">
									<strong>{{transaction.agent.name}}</strong>
								</div>
							</td>
							<td>
								<div ng-show="transaction.trx_type == 'REFUND' && !transaction.payment_type"><span class="label label-warning">PENDING</span></div>
								{{transaction.payment_type}}
							</td>
							<!-- <td align="center">{{transaction.trx_type}}</td> -->
							<td align="right">
								{{transaction.currency}} {{fn.formatNumber(transaction.amount, transaction.currency)}}
							</td>
						</tr>
						<tr ng-class="{'danger':(transaction.trx_type == 'REFUND')}">
							<td colspan="7" style="font-size:11px">
								<em>Remarks : {{transaction.description}}</em>
							</td>
						</tr>
						<tr>
							<td colspan="8" style="background:#FAFAFA"></td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<nav aria-label="Page navigation" class="pull-right">
			  <ul class="pagination pagination-sm">
				<li ng-class="{'disabled':DATA.transaction.search.page <= 1}">
				  <a href="" ng-click='loadDataTransactionTransport(DATA.transaction.search.page-1)' aria-label="Prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
				</li>
				<li ng-repeat='pagination in DATA.transaction.search.pagination' ng-class="{'active':DATA.transaction.search.page == pagination}">
					<a href="" ng-click='loadDataTransactionTransport(($index+1))'>{{($index+1)}}</a>
				</li>
				<li ng-class="{'disabled':DATA.transaction.search.page >= DATA.transaction.search.number_of_pages}">
				  <a href="" ng-click='loadDataTransactionTransport(DATA.transaction.search.page+1)' aria-label="Next"><span class="glyphicon glyphicon-chevron-right"></span></a>
				</li>
			  </ul>
			</nav>
			<div class="clearfix"></div>
			<div class="text-right" ng-show="DATA.summary">
				<table class="table table-condensed table-borderless">
					<tr align="right">
						<td colspan="2" ><strong><span style="font-size: 14px;">Transaction Summary </span></strong></td>
						
					</tr>
					<tr>
						<td>Total Paid : </td>
						<td width="130"><strong>{{DATA.transaction.transactions.data[0].currency}} {{fn.formatNumber(DATA.summary.total_paid, DATA.transaction.transactions.data[0].currency)}}</strong></td>
					</tr>
					<tr>
						<td class="color-red">Total Refund : </td>
						<td class="color-red"><strong>{{DATA.transaction.transactions.data[0].currency}} {{fn.formatNumber(DATA.summary.total_refund,DATA.transaction.transactions.data[0].currency)}}</strong></td>
						
					</tr>
					<tr ng-repeat="(index,data) in DATA.summary.total_acc" ng-show="data.total!=0">
						<td ng-class="{'color-red':(data.name == 'Total Refund' || data.name == 'Commission Payment')}">{{data.name}} : </td>
						<td ng-class="{'color-red':(data.name == 'Total Refund' || data.name == 'Commission Payment')}"><strong>{{DATA.transaction.transactions.data[0].currency}} {{fn.formatNumber(data.total,DATA.transaction.transactions.data[0].currency)}}</strong></td>
					</tr>
				</table>
			</div>
			<style type="text/css">
				td.color-red {
					color: red;
				}
			</style>
		</div>
	</div>
	
</div>

<script>activate_sub_menu_agent_detail("transaction");</script>


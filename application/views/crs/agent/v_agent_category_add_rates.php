<div ng-init="loadCategoryDetailAddMasterRates()" class="agent-detail"> 
	<div class="title">
		<h1>{{DATA.category_detail.name}}</h1>
		<div class="code">Code : <a href="" ui-sref="agent.category.detail({'category_code':DATA.category_detail.category_code})">{{DATA.category_detail.category_code}}</a> </div>
		<div class="code">
			Type : {{DATA.category_detail.contract_rate_type_desc}} 
			<span ng-show="DATA.category_detail.contract_rate_type == 'PERCENT'"> {{DATA.category_detail.percentage}}%</span>
		</div>
		<div class="code">
        	Periode : {{fn.newDate(DATA.category_detail.start_date) | date:'dd MMMM yyyy'}} - {{fn.newDate(DATA.category_detail.end_date) | date:'dd MMMM yyyy'}}
        </div>
        <div class="desc" ng-show="DATA.category_detail.description">
        	{{DATA.category_detail.description}}
        </div>
	</div>
	<br />

    <div class="products">
        <div class="product ">
            Filter : <input type="text" ng-model="filter_rates" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
        </div>
    </div>
	<form ng-submit='saveDataContractRatesCategory($event)'>
		<?php // RATES TRANSPORT =========================================================== ?>
        <h4 ng-show='DATA.schedules.schedules'>Trips & Schedules Rates</h4>
        <table class="table table-bordered table-condensed">
			<tbody ng-repeat='schedules in DATA.schedules.schedules'>
				<tr class="header">
					<td>
						<strong>{{schedules.schedule_code}}</strong> - <strong>{{schedules.boat.name}}</strong><br />
					</td>
                    <td ng-show="DATA.category_detail.contract_rate_type == 'PERCENT'"><strong>Publish</strong></td>
                    <td ng-show="DATA.category_detail.contract_rate_type == 'PERCENT'"></td>
                    <td ><strong>Nett</strong></td>
                    <td width="80"></td>
				</tr>
				<tr ng-repeat='rates in DATA.rates[schedules.id] | filter:filter_rates' ng-class="{'info':(rates.selected_agent_rates == '1' || DATA.contract_rates[rates.id])}">
					<td>
						<strong>{{rates.departure.port.name}} ({{rates.departure.time}}) 
						<i class="fa fa-chevron-right"></i>
						{{rates.arrival.port.name}} ({{rates.arrival.time}})</strong>
						<br />
						<span style="color:blue">
							<strong>{{rates.rates_code}}</strong> - <strong><em>{{rates.name}}</em></strong>
						</span>
					</td>
					<td width="180">
						<strong>One Way</strong><br />
						<small>
							<div>
								Adult : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.one_way_rates.rates_1, rates.currency)}}</strong>
							</div>
							<div ng-show='rates.rates.one_way_rates.rates_2 > 0'>
								Child : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.one_way_rates.rates_2, rates.currency)}}</strong>
							</div>
							<div ng-show='rates.rates.one_way_rates.rates_3 > 0'>
								Infant : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.one_way_rates.rates_3, rates.currency)}}</strong>
							</div>
						</small>
						<strong>Return</strong><br />
						<small>
							<div ng-show='rates.rates.return_rates.rates_1 > 0'>
								Adult : <strong>{{rates.currency}} {{fn.formatNumber((rates.rates.return_rates.rates_1*2), rates.currency)}}</strong>
							</div>
							<div ng-show='rates.rates.return_rates.rates_2 > 0'>
								Child : <strong>{{rates.currency}} {{fn.formatNumber((rates.rates.return_rates.rates_2*2), rates.currency)}}</strong>
							</div>
							<div ng-show='rates.rates.return_rates.rates_3 > 0'>
								Infant : <strong>{{rates.currency}} {{fn.formatNumber((rates.rates.return_rates.rates_3*2), rates.currency)}}</strong>
							</div>
						</small>
					</td>
					<td width="30" style="vertical-align:middle; font-size:18px; text-align:center" ng-show="DATA.category_detail.contract_rate_type == 'PERCENT'">
						<span class="glyphicon glyphicon-chevron-right"></span>
					</td>
					<td width="180" ng-show="DATA.category_detail.contract_rate_type == 'PERCENT'">
						<strong>One Way</strong><br />
						<small>
							<div>
								Adult : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.one_way_rates.rates_1 - (rates.rates.one_way_rates.rates_1 * DATA.category_detail.percentage/100), rates.currency)}}</strong>
							</div>
							<div ng-show='rates.rates.one_way_rates.rates_2 > 0'>
								Child : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.one_way_rates.rates_2 - (rates.rates.one_way_rates.rates_2 * DATA.category_detail.percentage/100), rates.currency)}}</strong>
							</div>
							<div ng-show='rates.rates.one_way_rates.rates_3 > 0'>
								Infant : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.one_way_rates.rates_3 - (rates.rates.one_way_rates.rates_3 * DATA.category_detail.percentage/100), rates.currency)}}</strong>
							</div>
						</small>
						<strong>Return</strong><br />
						<small>
							<div ng-show='rates.rates.return_rates.rates_1 > 0'>
								Adult : <strong>{{rates.currency}} {{fn.formatNumber((rates.rates.return_rates.rates_1*2)-(rates.rates.return_rates.rates_1*2*DATA.category_detail.percentage/100), rates.currency)}}</strong>
							</div>
							<div ng-show='rates.rates.return_rates.rates_2 > 0'>
								Child : <strong>{{rates.currency}} {{fn.formatNumber((rates.rates.return_rates.rates_2*2)-(rates.rates.return_rates.rates_2*2*DATA.category_detail.percentage/100), rates.currency)}}</strong>
							</div>
							<div ng-show='rates.rates.return_rates.rates_3 > 0'>
								Infant : <strong>{{rates.currency}} {{fn.formatNumber((rates.rates.return_rates.rates_3*2)-(rates.rates.return_rates.rates_3*2*DATA.category_detail.percentage/100), rates.currency)}}</strong>
							</div>
						</small>
					</td>
                    <td align="center">
                    	<div ng-show="DATA.category_detail.contract_rate_type == 'PERCENT'">
                        	<div ng-show='rates.custom_rates_is_edit'>
                            	<input type="number" step="0.01" min="0" max="100" style="width:100%" placeholder="... %" ng-model='rates.custom_percentage' ng-blur='rates.custom_rates_is_edit=false' />
                            </div>
                            <div ng-hide='rates.custom_rates_is_edit'>
                            	<span ng-hide="DATA.contract_rates[rates.id].custom_percentage">
                                    <span ng-hide="rates.custom_percentage">{{DATA.category_detail.percentage}}</span>
                                    <span ng-show="rates.custom_percentage">{{rates.custom_percentage}}</span>
                                </span>
                                <span ng-show="DATA.contract_rates[rates.id].custom_percentage">{{DATA.contract_rates[rates.id].custom_percentage}}</span>
                                %
                                <small ng-hide="(rates.selected_agent_rates == '1' || DATA.contract_rates[rates.id])"><a href="" ng-click='rates.custom_rates_is_edit=true'><span class="glyphicon glyphicon-pencil"></span></a></small>
                            </div>
                            <hr style="margin:10px 0" />
                        </div>
                        <div ng-hide="(rates.selected_agent_rates == '1' || DATA.contract_rates[rates.id])">
	                    	<button type="button" class="btn btn-xs btn-info" ng-click='saveSelectedRatesToCategory(rates)'><span class="glyphicon glyphicon-plus"></span> Add</button>
                    	</div>
                        <div ng-show="(rates.selected_agent_rates == '1' || DATA.contract_rates[rates.id])">
                        	<button type="button" class="btn btn-xs btn-danger" ng-click='removeSelectedRatesToCategory(rates)'><span class="glyphicon glyphicon-remove"></span> Del</button>
                        </div>
                    </td>
				</tr>
			</tbody>
		</table>
		<?php // --END RATES TRANSPORT =========================================================== ?>
        
        
        <?php // RATES ACTIVITIES =========================================================== ?>
        <br />
        <h4 ng-show='DATA.products.products'>Products & Services Rates</h4>
        
        <table class="table table-bordered table-condensed">
			<tbody ng-repeat='products in DATA.products.products'>
				<tr class="header">
					<td>
						<strong>{{products.product_code}}</strong> - <strong>{{products.name}}</strong><br />
					</td>
                    <td ng-show="DATA.category_detail.contract_rate_type == 'PERCENT'"><strong>Publish</strong></td>
                    <td ng-show="DATA.category_detail.contract_rate_type == 'PERCENT'"></td>
                    <td ><strong>Nett</strong></td>
                    <td width="80"></td>
				</tr>
                <tr ng-repeat='rates in DATA.rates_act[products.id] | filter:filter_rates' ng-class="{'info':(rates.selected_agent_rates == '1' || DATA.contract_rates_act[rates.id])}">
					<td>
						<?php /*?><strong>{{rates.departure.port.name}} ({{rates.departure.time}}) 
						<i class="fa fa-chevron-right"></i>
						{{rates.arrival.port.name}} ({{rates.arrival.time}})</strong>
						<br /><?php */?>
						<span style="color:blue">
							<strong>{{rates.rates_code}}</strong> - <strong><em>{{rates.name}}</em></strong>
						</span>
					</td>
					<td width="180">
						<div>
                            {{rates.rates_caption.rates_1}} : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.rates_1, rates.currency)}}</strong>
                        </div>
                        <div ng-show='rates.rates.rates_2 > 0'>
                            {{rates.rates_caption.rates_2}} : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.rates_2, rates.currency)}}</strong>
                        </div>
                        <div ng-show='rates.rates.rates_3 > 0'>
                            {{rates.rates_caption.rates_3}} : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.rates_3, rates.currency)}}</strong>
                        </div>
					</td>
					<td width="30" style="vertical-align:middle; font-size:18px; text-align:center" ng-show="DATA.category_detail.contract_rate_type == 'PERCENT'">
						<span class="glyphicon glyphicon-chevron-right"></span>
					</td>
					<td width="180" ng-show="DATA.category_detail.contract_rate_type == 'PERCENT'">
                    	<div>
							{{rates.rates_caption.rates_1}} : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.rates_1 - (rates.rates.rates_1 * (rates.custom_percentage?rates.custom_percentage:DATA.category_detail.percentage)/100), rates.currency)}}</strong>
                        </div>
                        <div ng-show='rates.rates.rates_2 > 0'>
							{{rates.rates_caption.rates_2}} : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.rates_2 - (rates.rates.rates_2 * (rates.custom_percentage?rates.custom_percentage:DATA.category_detail.percentage)/100), rates.currency)}}</strong>
                        </div>
                        <div ng-show='rates.rates.rates_3 > 0'>
							{{rates.rates_caption.rates_3}} : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.rates_3 - (rates.rates.rates_3 * (rates.custom_percentage?rates.custom_percentage:DATA.category_detail.percentage)/100), rates.currency)}}</strong>
                        </div>
					</td>
                    <td align="center">
                    	<div ng-show="DATA.category_detail.contract_rate_type == 'PERCENT'">
                        	<div ng-show='rates.custom_rates_is_edit'>
                            	<input type="number" step="0.01" min="0" max="100" style="width:100%" placeholder="... %" ng-model='rates.custom_percentage' ng-blur='rates.custom_rates_is_edit=false' />
                            </div>
                            <div ng-hide='rates.custom_rates_is_edit'>
                            	<span ng-hide="DATA.contract_rates[rates.id].custom_percentage">
                                    <span ng-hide="rates.custom_percentage">{{DATA.category_detail.percentage}}</span>
                                    <span ng-show="rates.custom_percentage">{{rates.custom_percentage}}</span>
                                </span>
                                <span ng-show="DATA.contract_rates[rates.id].custom_percentage">{{DATA.contract_rates[rates.id].custom_percentage}}</span>
                                %
                                <small ng-hide="(rates.selected_agent_rates == '1' || DATA.contract_rates[rates.id])"><a href="" ng-click='rates.custom_rates_is_edit=true'><span class="glyphicon glyphicon-pencil"></span></a></small>
                            </div>
                            <hr style="margin:10px 0" />
                        </div>
                        <div ng-hide="(rates.selected_agent_rates == '1' || DATA.contract_rates_act[rates.id])">
	                    	<button type="button" class="btn btn-xs btn-info" ng-click="saveSelectedRatesToCategory(rates, 'ACT')"><span class="glyphicon glyphicon-plus"></span> Add</button>
                    	</div>
                        <div ng-show="(rates.selected_agent_rates == '1' || DATA.contract_rates_act[rates.id])">
                        	<button type="button" class="btn btn-xs btn-danger" ng-click="removeSelectedRatesToCategory(rates, 'ACT')"><span class="glyphicon glyphicon-remove"></span> Del</button>
                        </div>
                    </td>
				</tr>
			</tbody>
        </table>
        <?php // -- END RATES ACTIVITIES =========================================================== ?>
        
		<hr />
		
		<div>
			<?php /*?><button type="submit" class="btn btn-lg btn-primary">Submit Contract Rates</button><?php */?>
			<a href="" ui-sref="agent.category.detail({'category_code':DATA.category_detail.category_code})">
            	<span class="glyphicon glyphicon-chevron-left"></span>
            	Back
            </a>
		</div>
	</form>
</div>
<script>GeneralJS.activateSubMenu(".nav-pills", "li", ".category");</script>

<style>
	.agent-detail .title{margin-bottom:20px}
	.agent-detail .title h1{margin-bottom:10px !important;}
	.agent-detail .title .code{margin-bottom:5px;}
</style>
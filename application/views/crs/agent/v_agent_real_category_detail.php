<div ng-init="loadDataRealCategoryDetail()" class="agent-detail"> 
    <div class="title">
        <h1>{{DATA.current_real_category.name}}</h1>
        <div class="code">Category Code : {{DATA.current_real_category.real_category_code}}</div>
        <div class="desc" ng-show="DATA.current_real_category.description">
            {{DATA.current_real_category.description}}
        </div>
    </div>
    
    <ul class="nav nav-tabs sub-nav">
        <li role="presentation" class="contract-rates"><a ui-sref="agent.real_category.detail.contract_rates">Contract Rates</a></li>
        <li role="presentation" class="agent"><a ui-sref="agent.real_category.detail.agent">Agents</a></li>
    </ul>
    <br />
    
    <div ui-view>
    	<?php $this->load->view("crs/agent/v_agent_real_category_detail_agent_list") ?>
    </div>
</div>

<style>
	.agent-detail .title{margin-bottom:20px}
	.agent-detail .title h1{margin-bottom:10px !important;}
	.agent-detail .title .code{margin-bottom:5px;}
</style>
<div ui-view>
	<div ng-init="loadDataInvoice()">
		<div class="products">
			<div class="product">
				Filter : <input type="text" ng-model="filter_invoices" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
			</div>
			<div ng-show='!DATA.invoice'>
				<img src="<?=base_url("public/images/loading_bar.gif")?>" />
			</div>
			<div ng-show="DATA.invoice && DATA.invoice.status!='SUCCESS'">
				<div class="alert alert-warning">
					<strong>Data Not Founds.</strong>
				</div>
			</div>
			<div ng-show="DATA.invoice.status=='SUCCESS'">
				<div class="table-responsive" ng-show='DATA.invoice.invoices'>
					<table class="table table-bordered table-condensed">
						<tr class="info" style="font-weight:bold">
							<td width="30" align="center">No.</td>
							<td width="100" align="center">Invoice code #</td>
							<td width="150" align="center">Business Source</td>
							<td width="90" align="center">Invoice Date</td>
							<td width="90" align="center">Type</td>
							<td width="90" align="center">Total</td>
							<td width="90" align="center">Total Payment</td>
							<td width="90" align="center">Balance</td>
						</tr>
						<tr ng-repeat="invoice in DATA.invoice.invoices | filter : filter_invoices " ng-class="{success : invoice.invoice_total == invoice.total_payment, warning : invoice.invoice_due < DATA.invoice.now && invoice.balance > 0 }">
							<td align="right">{{($index+1)}}</td>
							<td>
								<a ui-sref="invoice.detail({'invoice_code':invoice.invoice_code})" ng-show="invoice.invoice_type=='RESERVATION'"><strong>{{invoice.invoice_code}}</strong></a>
								<a ui-sref="invoice.openvoucher({'invoice_code':invoice.invoice_code})" ng-show="invoice.invoice_type=='OPENVOUCHER'"><strong>{{invoice.invoice_code}}</strong></a>
							</td>
							<td>{{invoice.agent_name}}</td>
							<td align="center">{{fn.formatDate(invoice.invoice_date, "d M yy")}}</td>
							<td>
								<span ng-show="invoice.invoice_type=='OPENVOUCHER'">Pre Sold Ticket</span>
								<span ng-show="invoice.invoice_type=='RESERVATION'">Reservation</span>
							</td>
							<td align="right">{{invoice.invoice_curr}} {{fn.formatNumber(invoice.invoice_total, invoice.invoice_curr)}}</td>
							<td align="right">{{invoice.invoice_curr}} {{fn.formatNumber(invoice.total_payment, invoice.invoice_curr)}}</td>
							<td align="right">{{invoice.invoice_curr}} {{fn.formatNumber(invoice.balance, invoice.invoice_curr)}}</td>
						</tr>
					</table>
				</div>
				<br />
                <div>
                	<p>
                    	<div class="label" style="border:solid 1px #CCCCCC">&nbsp;&nbsp;</div> New Invoice : ({{DATA.invoice.summary.currency}} {{fn.formatNumber(DATA.invoice.summary.new, DATA.invoice.summary.currency)}})
                    </p>
                    <p>
    	                <div class="label label-warning" style="border:solid 1px #CCCCCC">&nbsp;&nbsp;</div> Outstanding Invoice : ({{DATA.invoice.summary.currency}} {{fn.formatNumber(DATA.invoice.summary.outstanding, DATA.invoice.summary.currency)}})
                    </p>
                    <p>
	                    <div class="label label-success" style="border:solid 1px #CCCCCC">&nbsp;&nbsp;</div> Paid : ({{DATA.invoice.summary.currency}} {{fn.formatNumber(DATA.invoice.summary.paid, DATA.invoice.summary.currency)}})
                    </p>
                </div>
			</div>

			<!-- pagination -->
			<nav aria-label="Page navigation" class="pull-right" ng-show="DATA.invoice.search.number_of_pages>1">
			  <ul class="pagination pagination-sm">
				<li ng-class="{'disabled':DATA.invoice.search.page <= 1}">
				  <a href="" ng-click='loadDataInvoice(DATA.invoice.search.page-1)' aria-label="Prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
				</li>
				<li	ng-show="DATA.invoice.search.page-3 > 1">
  					<a href="" ng-click='loadDataInvoice(1)'>1</a>
  				</li>
  				<li class="disabled" ng-show="DATA.invoice.search.page-4 > 1">
  					<a>...</a>
  				</li>
				<li ng-repeat='pagination in DATA.invoice.search.pagination' ng-class="{'active':DATA.invoice.search.page == pagination}">
					<a href="" ng-click='loadDataInvoice(($index+1))'>{{($index+1)}}</a>
				</li>
				<li class="disabled" ng-show="DATA.invoice.search.page+4 < DATA.invoice.search.number_of_pages">
  					<a>...</a>
  				</li>
  				<li	ng-show="DATA.invoice.search.page+3 < DATA.invoice.search.number_of_pages">
  					<a href="" ng-click='loadDataInvoice(DATA.invoice.search.number_of_pages)'>{{DATA.invoice.search.number_of_pages}}</a>
  				</li>
				<li ng-class="{'disabled':DATA.invoice.search.page >= DATA.invoice.search.number_of_pages}">
				  <a href="" ng-click='loadDataInvoice(DATA.invoice.search.page+1)' aria-label="Next"><span class="glyphicon glyphicon-chevron-right"></span></a>
				</li>
			  </ul>
			</nav>
			<div class="clearfix"></div>
			
		</div>

		<br>
	</div>
</div>

<script>activate_sub_menu_agent_detail("invoice");</script>
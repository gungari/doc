<div ui-view>
    <div ng-init="loadDataContractRates()">
    	<div ng-show='!DATA.contract_rates'>
            <img src="<?=base_url("public/images/loading_bar.gif")?>" />
        </div>
        
        <div ng-show="DATA.contract_rates.status=='SUCCESS'">
            <div class="products">
                <div class="product ">
                    Filter : <input type="text" ng-model="filter_categories" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
                </div>
                
                <div ng-hide="DATA.contract_rates.contract_rates" class="alert alert-warning"><strong>Sorry.</strong> Data not found...</div>
                
                <table class="table table-bordered table-condensed" ng-show='DATA.contract_rates.contract_rates'>
                    <tr class="info" style="font-weight:bold">
                        <td width="30">No.</td>
                        <td width="120" align="center">Code</td>
                        <td>Contract Rates Name</td>
                        <td width="180" align="center">Periode</td>
                        <td width="80"></td>
                        <td width="30"></td>
                    </tr>
                    <tbody ng-repeat="contract in DATA.contract_rates.contract_rates | filter : filter_categories ">
                        <tr ng-class="{'danger':contract.publish_status!='1'}">
                            <td rowspan="3">{{($index+1)}}</td>
                            <td rowspan="3" align="center">{{contract.contract_rates_code}}</td>
                            <td>
                                <a href="" ng-click="contract.show_detail=!contract.show_detail; loadDataContractRatesDetail(contract);">
                                    <strong>{{contract.name}}</strong>
                                </a>
                            </td>
                            <td align="center">
                                {{fn.newDate(contract.start_date) | date:'dd MMM yyyy'}} -
                                {{fn.newDate(contract.end_date) | date:'dd MMM yyyy'}}
                            </td>
                            <td rowspan="2" align="center">
                                <a href="" ng-click="publishUnpublishContractRates(contract, 0)" ng-show="contract.publish_status == '1'">
                                    <div style="color:#090"><img src="<?=base_url("public/images/on-button.png")?>" width="100%" /></span> </div>
                                </a> 
                                <a href="" ng-click="publishUnpublishContractRates(contract, 1)" ng-show="contract.publish_status != '1'">
                                    <div style="color:#F30"><img src="<?=base_url("public/images/off-button.png")?>" width="100%" /></span></div>
                                </a> 
                            </td>
                            <td rowspan="2" align="center">
                                <a href="" data-toggle="modal" ng-click='addEditContractRates(contract)' data-target="#add-edit-contract-rates">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                            </td>
                        </tr>
                        <tr ng-class="{'danger':contract.publish_status!='1'}">
                            <td colspan="2"><em ng-show='contract.description'>{{contract.description}}</em><em ng-hide="contract.description">-</em></td>
                        </tr>
                        
                        <tr ng-class="{'danger':contract.publish_status!='1'}">
                            <td colspan="4" ng-show='contract.show_detail'>
                                <div ng-show='contract.show_loading'><em>Loading...</em></div>
                                <table class="table table-bordered">
                                    <tbody ng-repeat="product in DATA.products" ng-show='contract.rates_act_by_id_product[product.id]'>
                                        <tr class="header">
                                            <td><strong>{{product.product_code}} - {{product.name}}</strong></td>
                                        </tr>
                                        <tr ng-repeat="rates in contract.rates_act_by_id_product[product.id]" ng-class="{'danger':rates.publish_status!='1'}">
                                            <td>
                                            	<div class="pull-right"> 
                                                    <a href="" ng-click="publishUnpublishProductRatesACT(rates, 0)" ng-show="rates.publish_status == '1'">
                                                        <div style="color:#090"><img src="<?=base_url("public/images/on-button.png")?>" width="60" /></span> </div>
                                                    </a> 
                                                    <a href="" ng-click="publishUnpublishProductRatesACT(rates, 1)" ng-show="rates.publish_status != '1'">
                                                        <div style="color:#F30"><img src="<?=base_url("public/images/off-button.png")?>"  width="60" /></span></div>
                                                    </a> 
                                                </div>
                                                <div><strong>{{rates.rates_code}} - {{rates.name}}</strong></div>
                                                <div style="font-size:11px">
                                                	<div ng-show="!rates.is_packages">
                                                        <span class="glyphicon glyphicon glyphicon-chevron-right"></span>
                                                        <span>
                                                            {{rates.rates_caption.rates_1}} : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.rates_1, rates.currency)}}</strong>
                                                        </span>
                                                        <span ng-show='rates.rates_caption.rates_2'>
                                                            &nbsp;&nbsp;
                                                            <span class="glyphicon glyphicon glyphicon-chevron-right"></span>
                                                            {{rates.rates_caption.rates_2}} : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.rates_2, rates.currency)}}</strong>
                                                        </span>
                                                        <span ng-show='rates.rates_caption.rates_3'>
                                                            &nbsp;&nbsp;
                                                            <span class="glyphicon glyphicon glyphicon-chevron-right"></span>
                                                            {{rates.rates_caption.rates_3}} : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.rates_3, rates.currency)}}</strong>
                                                        </span>
                                                	</div>
                                                    <div ng-show='rates.is_packages'>
                                                        <strong>Packages:</strong>
                                                        <ul ng-show='rates.packages_option' style="padding-left:20px; margin-bottom:0">
                                                            <li ng-repeat="packages_option in rates.packages_option" style="margin-bottom:5px">
                                                                <div>
                                                                    <strong>{{packages_option.caption}}</strong>
                                                                </div>
                                                                <div>
                                                                    Price : <strong>{{rates.currency}} {{fn.formatNumber(packages_option.rates, rates.currency)}}</strong> 
                                                                    <?php /*?><small>/{{packages_option.rates_unit}}</small><?php */?>
                                                                    <span class="glyphicon glyphicon-menu-right"></span>
                                                                    <span ng-show="packages_option.include_pax.opt_1">{{packages_option.include_pax.opt_1}} Adult &nbsp;</span>
                                                                    <span ng-show="packages_option.include_pax.opt_2">{{packages_option.include_pax.opt_2}} Child &nbsp;</span>
                                                                    <span ng-show="packages_option.include_pax.opt_3">{{packages_option.include_pax.opt_3}} infant &nbsp;</span>
                                                                </div>
                                                                <div ng-show="packages_option.extra_rates.opt_1 || packages_option.extra_rates.opt_2 || packages_option.extra_rates.opt_3">
                                                                    Extra :
                                                                    <span ng-show="packages_option.extra_rates.opt_1">Adult : <strong>{{rates.currency}} {{fn.formatNumber(packages_option.extra_rates.opt_1, rates.currency)}}</strong> &nbsp;</span>
                                                                    <span ng-show="packages_option.extra_rates.opt_2">Child : <strong>{{rates.currency}} {{fn.formatNumber(packages_option.extra_rates.opt_2, rates.currency)}}</strong> &nbsp;</span>
                                                                    <span ng-show="packages_option.extra_rates.opt_3">Infant : <strong>{{rates.currency}} {{fn.formatNumber(packages_option.extra_rates.opt_3, rates.currency)}}</strong> &nbsp;</span>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div>
                                                    <small>
                                                        <i class="fa fa-check"  aria-hidden="true" ng-show="rates.pickup_service == 'yes'"></i> 
                                                        <i class="fa fa-remove" aria-hidden="true" ng-show="rates.pickup_service != 'yes'" style="color:red"></i> Pickup 
                                                        
                                                        <i class="fa fa-check"  aria-hidden="true" ng-show="rates.dropoff_service == 'yes'"></i> 
                                                        <i class="fa fa-remove" aria-hidden="true" ng-show="rates.dropoff_service != 'yes'" style="color:red"></i> Dropoff
                                                        
                                                        <span ng-show="rates.additional_charge"><i class="fa fa-check"></i> Additional Charge</span>
                                                    </small>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tbody ng-repeat="schedule in DATA.schedules" ng-showxx='contract.rates_trans_by_id_schedule[schedule.id]'>
                                        <tr class="header">
                                            <td>
                                            	<div><strong>{{schedule.schedule_code}} - <i class="fa fa-ship" aria-hidden="true"></i> {{schedule.boat.name}}</strong></div>
                                                <div style="font-size:11px">{{schedule.description}}</div>
                                            </td>
                                        </tr>
                                        <tr ng-repeat="rates in contract.rates_trans_by_id_schedule[schedule.id]" ng-class="{'danger':rates.publish_status!='1'}">
                                        	<td>
                                            	<div class="pull-right"> 
                                                    <a href="" ng-click="publishUnpublishScheduleRatesTRANS(rates, 0)" ng-show="rates.publish_status == '1'">
                                                        <div style="color:#090"><img src="<?=base_url("public/images/on-button.png")?>" width="60" /></span> </div>
                                                    </a> 
                                                    <a href="" ng-click="publishUnpublishScheduleRatesTRANS(rates, 1)" ng-show="rates.publish_status != '1'">
                                                        <div style="color:#F30"><img src="<?=base_url("public/images/off-button.png")?>" width="60" /></span></div>
                                                    </a> 
                                                </div>
                                            	<div>
                                                    <strong>
                                                        {{rates.departure.port.name}} ({{rates.departure.port.port_code}})
                                                        : <span style="color:#337ab7">{{rates.departure.time}}</span>
                                                        &nbsp; <i class="fa fa-chevron-right"></i> &nbsp;
                                                        {{rates.arrival.port.name}} ({{rates.arrival.port.port_code}})
                                                        : <span style="color:#337ab7">{{rates.arrival.time}}</span>
                                                    </strong>
                                                </div>
                                                <hr style="margin:5px 0" />
                                                <div><strong>{{rates.rates_code}} - {{rates.name}}</strong></div>
                                                
                                                <div style="font-size:11px">
                                                	One Way : 
                                                    <span>
                                                    	<span class="glyphicon glyphicon glyphicon-chevron-right"></span>
                                                        Adult : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.one_way_rates.rates_1, rates.currency)}}</strong>
                                                    </span>
                                                    <span ng-show='rates.rates.one_way_rates.rates_2 > 0'>
                                                    	&nbsp;&nbsp;
                                                    	<span class="glyphicon glyphicon glyphicon-chevron-right"></span>
                                                        Child : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.one_way_rates.rates_2, rates.currency)}}</strong>
                                                    </span>
                                                    <span ng-show='rates.rates.one_way_rates.rates_3 > 0'>
                                                    	&nbsp;&nbsp;
                                                    	<span class="glyphicon glyphicon glyphicon-chevron-right"></span>
                                                        Infant : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.one_way_rates.rates_3, rates.currency)}}</strong>
                                                    </span>
                                                </div>
                                                
                                                <div style="font-size:11px" ng-show='rates.rates.return_rates'>
                                                	Return : &nbsp;&nbsp;&nbsp;
                                                    <span ng-show='rates.rates.return_rates.rates_1 > 0'>
                                                    	<span class="glyphicon glyphicon glyphicon-chevron-right"></span>
                                                        Adult : <strong>{{rates.currency}} {{fn.formatNumber((rates.rates.return_rates.rates_1*2), rates.currency)}}</strong>
                                                    </span>
                                                    <span ng-show='rates.rates.return_rates.rates_2 > 0'>
                                                    	&nbsp;&nbsp;
                                                    	<span class="glyphicon glyphicon glyphicon-chevron-right"></span>
                                                        Child : <strong>{{rates.currency}} {{fn.formatNumber((rates.rates.return_rates.rates_2*2), rates.currency)}}</strong>
                                                    </span>
                                                    <span ng-show='rates.rates.return_rates.rates_3 > 0'>
                                                    	&nbsp;&nbsp;
                                                    	<span class="glyphicon glyphicon glyphicon-chevron-right"></span>
                                                        Infant : <strong>{{rates.currency}} {{fn.formatNumber((rates.rates.return_rates.rates_3*2), rates.currency)}}</strong>
                                                    </span>
                                                </div>
                                                
                                                <div>
                                                    <small>
                                                        <i class="fa fa-check"  aria-hidden="true" ng-show="rates.pickup_service == 'yes'"></i> 
                                                        <i class="fa fa-remove" aria-hidden="true" ng-show="rates.pickup_service != 'yes'" style="color:red"></i> Pickup 
                                                        
                                                        <i class="fa fa-check"  aria-hidden="true" ng-show="rates.dropoff_service == 'yes'"></i> 
                                                        <i class="fa fa-remove" aria-hidden="true" ng-show="rates.dropoff_service != 'yes'" style="color:red"></i> Dropoff
                                                        
                                                        <span ng-show="rates.additional_charge"><i class="fa fa-check"></i> Additional Charge</span>
                                                    </small>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <hr style="margin:10px 0">
                                <div align="right">
                                    <a ui-sref="agent.real_category.detail.contract_rates.detail({'contract_rates_code':contract.contract_rates_code})"><span class="glyphicon glyphicon-pencil"></span> Edit Rates</a>
                                    <?php /*?>&nbsp;&nbsp;&nbsp;&nbsp;
                                     <a href="" ng-click="duplicateProductRates(rates)"><span class="glyphicon glyphicon-duplicate"></span>  Duplicate Rates</a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="" style="color:red" ng-click="deleteProductRates(rates)"><span class="glyphicon glyphicon-trash"></span> </a><?php */?>
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td colspan="6" style="background:#FAFAFA"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        
        <hr>
        <div class="add-product-button"> <a href="" class="btn btn-success btn-lg btn-block" data-toggle="modal" ng-click='addEditContractRates()' data-target="#add-edit-contract-rates"> <span class="glyphicon glyphicon-plus"></span> Add Contract Rates</a> </div>
        <br>
        <br>
        
        <div class="modal fade" id="add-edit-contract-rates" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <form ng-submit='saveDataContractRates($event)'>
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <span ng-show='!DATA.myContractRates.id'>Add</span><span ng-show='DATA.myContractRates.id'>Edit</span> Contract Rates
                </h4>
              </div>
              <div class="modal-body">
                <div ng-show='DATA.myContractRates.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myContractRates.error_msg'>{{err}}</li></ul></div>
                <table class="table table-borderless table-condensed">
                    <tr>
                        <td width="120">Name*</td>
                        <td><input placeholder="Name" required="required" type="text" class="form-control input-md" ng-model='DATA.myContractRates.name' /></td>
                    </tr>
                    <tr>
                        <td>Periode*</td>
                        <td>
                            <input type="text" required="required" ng-model='DATA.myContractRates.start_date' class="form-control input-md datepicker" style="display:inline; width:150px" placeholder="Start Date" />
                            -
                            <input type="text" required="required" ng-model='DATA.myContractRates.end_date' class="form-control input-md datepicker" style="display:inline; width:150px" placeholder="End Date" />
                        </td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td>
                            <textarea placeholder="Description" class="form-control input-md" ng-model='DATA.myContractRates.description' rows="5"></textarea>
                        </td>
                    </tr>
                </table>
              </div>
              <div class="modal-footer" style="text-align:center">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              </div>
            </div>
          </div>
          </form>
        </div>
        
    </div>
    
</div>	
<script>activate_sub_menu_agent_detail("contract-rates");</script>

<?php //Sync : 180801 ?>
<div ng-init="loadDataBookingTransport()">
	<div class="products">
		<div class="product">
			<form ng-submit='loadDataBookingTransport()'>
			<div class="table-responsive">
				<table class="table table-condensed table-borderless" width="100%">
					<tr>
                    	<td width="120">Filter By</td>
						<td width="110">From</td>
						<td width="110">To</td>
						<td width="200">Search</td>
						<td width="130">Status</td>
						<td></td>
					</tr>
					<tr>
                    	<td>
                            <select class="form-control input-sm " ng-model='search_booking.filter_by'>
                                <option value="BOOKINGDATE">Booking Date</option>
                                <option value="TRIPDATE">Activity Date</option>
                            </select>
                        </td>
						<td><input type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='search_booking.start_date' /></td>
						<td><input type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='search_booking.end_date' /></td>
						<td><input type="text" class="form-control input-sm" placeholder="Search" ng-model='search_booking.q' /></td>
						<td>
							<select class="form-control input-sm" ng-model='search_booking.booking_status'>
								<option value="">All</option>
								<option value="{{booking_status.code}}" ng-repeat='booking_status in $root.DATA_booking_status'>{{booking_status.name}}</option>
							</select>
							</td>
						<td><button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button></td>
					</tr>
				</table>
			</div>	
			</form>
		</div>
	</div>

	<div ng-show='show_loading_DATA_bookings'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	
	<div ng-show='!show_loading_DATA_bookings'>
		<div class="table-responsive">
			<table class="table table-condensed table-bordered">
				<tr class="header bold">
					<td align="center" width="90">Order#</td>
					<td align="center" width="110">Resv. Date</td>
					<td>Customer Name</td>
					<td align="center" width="40" title="Nationality">Nat.</td>
					<td width="200">Booking Source</td>
					<td align="center" width="50">Status</td>
					<td align="right" width="100">Amount</td>
				</tr>
				<tbody ng-repeat="booking in DATA.bookings.bookings | filter:DATA.filter_booking">
					<tr ng-class="{'danger':(booking.status_code=='CANCEL'), 'warning':(booking.status_code=='UNDEFINITE')}">
					<?php /* <tr ng-class="{'danger':(booking.cancel_status=='CAN' || booking.cancel_status=='VOID'), 'warning':(booking.status_code=='UNDEFINITE')}"> */ ?>
						<td align="center" rowspan="2">
							<?php /*?><a ui-sref="trans_reservation.detail({'booking_code':booking.booking_code})" target="_blank"><strong>{{booking.booking_code}}</strong></a><?php */?>
                            <a ui-sref="reservation.detail({'booking_code':booking.booking_code})" target="_blank"><strong>{{booking.booking_code}}</strong></a>
						</td>
						<td align="center">{{fn.newDate(booking.transaction_date) | date:'dd MMM yyyy'}} <small>{{fn.newDate(booking.transaction_date) | date:'HH:mm'}}</small> </td>
						<?php /*?><td align="center">{{fn.formatDate(booking.transaction_date, "dd M yy")}}</td><?php */?>
						<td>
							<strong>{{booking.customer.full_name}}</strong>
							<?php /*?><br /> {{booking.customer.email}}<?php */?>
						</td>
						<td align="center" class="text-capitalize" title="{{booking.customer.country_name}}">{{booking.customer.country_code}}</td>
						<td>
							<div ng-show='!booking.agent' class="text-capitalize">{{booking.source.toLowerCase()}}</div>
							<div ng-show='booking.agent'>{{booking.agent.name}}</div>
						</td>
						<td align="center" title="{{booking.status}}">{{booking.status_initial}}</td>
                        <?php /*?><td align="center" title="{{booking.status}}">
                        	<div ng-show="booking.cancel_status!=''">
                                <span>{{booking.cancel_status}}</span>
                            </div>
                            <div ng-show="booking.cancel_status==''">
                                {{((booking.status_initial=="CAN" || booking.status_initial=="VOID")?"DEF":booking.status_initial)}}
                            </div>
                        </td><?php */?>
						<?php /* <!-- <td align="right">
							<div ng-show='booking.cancel_status=="CAN"'>
                            	{{booking.currency}} {{fn.formatNumber(booking.grand_total_before_cancel, booking.currency)}}<br />
                                <small>Cancelation Fee:</small>
                                {{booking.currency}} {{fn.formatNumber(booking.grand_total, booking.currency)}}
                            </div>
                            <div ng-show='booking.cancel_status!="CAN"'>
                                <div ng-show='booking.status_initial=="CAN"'>
                                    {{booking.currency}} {{fn.formatNumber(booking.grand_total_before_cancel, booking.currency)}}
                                </div>
                                <div ng-show='booking.status_initial!="CAN"'>
                                    {{booking.currency}} {{fn.formatNumber(booking.grand_total, booking.currency)}}
                                </div>
                            </div>
						</td> --> */ ?>
						<td align="right">
								<?php /*?><div ng-show='booking.cancel_status=="CAN"'><?php */?>
                                <div ng-show='booking.status_code=="CANCEL"'>
                                	<div ng-show='booking.detail.length == 1'>
                                    	{{booking.currency}} {{fn.formatNumber(booking.detail[0].subtotal_before_cancel, booking.currency)}}<br />
                                        <small>Cancelation Fee:</small>
                                        {{booking.currency}} {{fn.formatNumber(booking.detail[0].subtotal, booking.currency)}}
                                    </div>
                                    <div ng-show='booking.detail.length != 1'>
                                        {{booking.currency}} {{fn.formatNumber(booking.grand_total_before_cancel, booking.currency)}}<br />
                                        <small>Cancelation Fee:</small>
                                        {{booking.currency}} {{fn.formatNumber(booking.grand_total, booking.currency)}}
                                    </div>
                                </div>
                                <?php /*?><div ng-show='booking.cancel_status=="CAN"'><?php */?>
                                <div ng-show='booking.status_code!="CANCEL"'>
                                	<?php /*?><div ng-show='booking.status_initial=="CAN"'>
										{{booking.currency}} {{fn.formatNumber(booking.grand_total_before_cancel, booking.currency)}}
                                    </div><?php */?>
                                    <div ng-show='booking.status_initial!="CAN"'>
										{{booking.currency}} {{fn.formatNumber(booking.grand_total, booking.currency)}}
                                    </div>
                                    <div>
	                                    <hr style="margin:3px 0" />
    	                                <small style="color:green">Paid : {{booking.currency}} {{fn.formatNumber(booking.total_payment, booking.currency)}}</small>
                                	</div>
                                </div>
							</td>
					</tr>
					<tr ng-class="{'danger':(booking.status_code=='CANCEL'), 'warning':(booking.status_code=='UNDEFINITE')}">
					<?php /* <tr ng-class="{'danger':(booking.cancel_status=='CAN' || booking.cancel_status=='VOID'), 'warning':(booking.status_code=='UNDEFINITE')}"> <?php */?>
						<td colspan="6" style="padding:0 !important">
							
							<table class="table table-bordered table-condensed" style="margin:-1px !important; background:none">
								<tr ng-show='booking.remarks'>
									<td style="border-left:solid 5px #FFF; font-size:11px">
										<em>Remarks : {{booking.remarks}}</em>
									</td>
								</tr>
								<tr ng-repeat="voucher in booking.detail | orderBy : '-booking_detail_status_code'" 
									ng-class="{'danger':(voucher.booking_detail_status_code=='CANCEL' || voucher.booking_detail_status_code == 'CANCEL')}">
									<td style="border-left:solid 5px #EEE; font-size:11px">
										{{voucher.voucher_code}} - 
										<strong style="font-size:12px">{{fn.formatDate(voucher.date, "dd / M / y")}}</strong> - 
                                        <span ng-show="voucher.product_type == 'ACT'">
                                            {{voucher.product.name}}
                                        </span>
                                        <span ng-show="voucher.product_type == 'TRANS'">
                                            {{voucher.departure.port.port_code}} ({{voucher.departure.time}})
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                            {{voucher.arrival.port.port_code}} ({{voucher.arrival.time}})
                                        </span>
										
                                        <span ng-hide='voucher.is_packages'>
                                            <span ng-show="voucher.qty_1 > 0"> - {{voucher.qty_1}} Adl. </span>
                                            <span ng-show="voucher.qty_2 > 0"> - {{voucher.qty_2}} Chi. </span>
                                            <span ng-show="voucher.qty_3 > 0"> - {{voucher.qty_3}} Inf. </span>
										</span>
                                        <span ng-show='voucher.is_packages'>
                                            - {{voucher.packages.qty}} x {{voucher.packages.name}}
                                        </span>
                                        
                                        
										<span ng-show="voucher.is_pickup=='YES'"> - Pickup </span>
										<span ng-show="voucher.is_dropoff=='YES'"> - Dropoff </span>
									</td>
								</tr>
							</table>
							
							<?php /*?><table class="table table-bordered table-condensed" style="margin:-1px !important">
								<tr ng-repeat='voucher in booking.detail' ng-class="{'danger':(booking.status_code=='CANCEL'), 'warning':(booking.status_code=='UNDEFINITE')}">
									<td width="100">{{voucher.voucher_code}}</td>
									<td width="90">{{fn.formatDate(voucher.date, "dd M yy")}}</td>
									<td width="90">{{voucher.departure.port.port_code}} ({{voucher.departure.time}})</td>
									<td width="90">{{voucher.arrival.port.port_code}} ({{voucher.arrival.time}})</td>
									<td width="45" align="center" title="Pickup">{{(voucher.is_pickup=='YES')?'PU':'-'}}</td>
									<td width="45" align="center" title="Dropoff">{{(voucher.is_dropoff=='YES')?'DO':'-'}}</td>
									<td></td>
								</tr>
							</table><?php */?>
							
						</td>
					</tr>
					<tr>
						<td colspan="7" style="background:#FAFAFA"></td>
					</tr>
				</tbody>
			</table>
		</div>
        
        <div ng-show='DATA.bookings.summary'>
			<table class="table table-borderless table-condensed" style="font-size:14px; font-weight:bold">
				<tr>
					<td align="right">TOTAL</td>
					<td width="10">:</td>
					<td align="right" width="130">{{DATA.bookings.summary.currency}} {{fn.formatNumber(DATA.bookings.summary.total_price, DATA.bookings.summary.currency)}}</td>
				</tr>
				<tr>
					<td align="right">Adult</td>
					<td width="10">:</td>
					<td align="right">{{DATA.bookings.summary.total_qty_1}}</td>
				</tr>
				<tr>
					<td align="right">Child</td>
					<td width="10">:</td>
					<td align="right">{{DATA.bookings.summary.total_qty_2}}</td>
				</tr>
				<tr>
					<td align="right">Infant</td>
					<td width="10">:</td>
					<td align="right">{{DATA.bookings.summary.total_qty_3}}</td>
				</tr>
			</table>
		</div>
		
		<nav aria-label="Page navigation" class="pull-right">
		  <ul class="pagination pagination-sm">
			<li ng-class="{'disabled':DATA.bookings.search.page <= 1}">
			  <a href="" ng-click='loadDataBookingTransport(DATA.bookings.search.page-1)' aria-label="Prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
			</li>
			<li ng-repeat='pagination in DATA.bookings.search.pagination' ng-class="{'active':DATA.bookings.search.page == pagination}">
				<a href="" ng-click='loadDataBookingTransport(($index+1))'>{{($index+1)}}</a>
			</li>
			<li ng-class="{'disabled':DATA.bookings.search.page >= DATA.bookings.search.number_of_pages}">
			  <a href="" ng-click='loadDataBookingTransport(DATA.bookings.search.page+1)' aria-label="Next"><span class="glyphicon glyphicon-chevron-right"></span></a>
			</li>
		  </ul>
		</nav>
		<div class="clearfix"></div>
		
		<hr />
		<a href="<?=site_url("export_to_excel/trans_bookings")?>?
			s[start_date]={{DATA.bookings.search.start_date}}&
			s[end_date]={{DATA.bookings.search.end_date}}&
			s[q]={{DATA.bookings.search.q}}&
			s[booking_status]={{DATA.bookings.search.booking_status}}&
			s[booking_source]={{DATA.bookings.search.booking_source}}&
			s[agent_code]={{DATA.bookings.search.agent_code}}&
			s[page]={{DATA.bookings.search.page}}" target="_blank">
				<i class="fa fa-file-excel-o" aria-hidden="true"></i> Export To Excel
		</a>
	</div>
	<br />
</div>

<script>activate_sub_menu_agent_detail("reservation");</script>

<?php // Trips & Schedules ------------------------------------------- ?>
<div ng-show='DATA.schedules'>
    <h4>Trips & Schedules</h4>
    <div ng-repeat="schedule in DATA.schedules">
        <table class="table table-bordered" style="border:solid 1px #DDD !important">
            <tbody>
                <tr class="header" ng-class="{'warning':(!DATA.current_contract_rates.rates_trans_by_id_schedule[schedule.id])}">
                    <td>
                        <div><strong>{{schedule.schedule_code}} - <i class="fa fa-ship" aria-hidden="true"></i> {{schedule.boat.name}}</strong></div>
                        <div style="font-size:11px">{{schedule.description}}</div>
                    </td>
                    <td width="200"><strong>One Way</strong></td>
                    <td width="200"><strong>Return</strong></td>
                </tr>
                <tr ng-hide="DATA.current_contract_rates.rates_trans_by_id_schedule[schedule.id].length>0" ng-class="{'warning':(!DATA.current_contract_rates.rates_trans_by_id_schedule[schedule.id])}">
                    <td colspan="3">
                        <em>Rates not available...</em>
                    </td>
                </tr>
            </tbody>
            <tbody ng-repeat="rates in DATA.current_contract_rates.rates_trans_by_id_schedule[schedule.id]">
                <tr>
                    <td colspan="3">
                        <strong>
                            {{rates.departure.port.name}} ({{rates.departure.port.port_code}})
                            : <span style="color:#337ab7">{{rates.departure.time}}</span>
                            &nbsp; <i class="fa fa-chevron-right"></i> &nbsp;
                            {{rates.arrival.port.name}} ({{rates.arrival.port.port_code}})
                            : <span style="color:#337ab7">{{rates.arrival.time}}</span>
                        </strong>
                    </td>
                </tr>
                <tr ng-class="{'danger':rates.publish_status!='1'}">
                    <td>
                        <a href="" onclick="$(this).closest('tr').next().toggle();" ng-click='loadMasterRatesDetailForRatesListTRANS(rates);'><strong>{{rates.rates_code}} - {{rates.name}}</strong></a><br />
                    </td>
                    <td>
                        <div>
                            Adult : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.one_way_rates.rates_1, rates.currency)}}</strong>
                        </div>
                        <div ng-show='rates.rates.one_way_rates.rates_2 > 0'>
                            Child : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.one_way_rates.rates_2, rates.currency)}}</strong>
                        </div>
                        <div ng-show='rates.rates.one_way_rates.rates_3 > 0'>
                            Infant : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.one_way_rates.rates_3, rates.currency)}}</strong>
                        </div>
                    </td>
                    <td>
                        <div ng-show='rates.rates.return_rates.rates_1 > 0'>
                            Adult : <strong>{{rates.currency}} {{fn.formatNumber((rates.rates.return_rates.rates_1*2), rates.currency)}}</strong>
                        </div>
                        <div ng-show='rates.rates.return_rates.rates_2 > 0'>
                            Child : <strong>{{rates.currency}} {{fn.formatNumber((rates.rates.return_rates.rates_2*2), rates.currency)}}</strong>
                        </div>
                        <div ng-show='rates.rates.return_rates.rates_3 > 0'>
                            Infant : <strong>{{rates.currency}} {{fn.formatNumber((rates.rates.return_rates.rates_3*2), rates.currency)}}</strong>
                        </div>
                    </td>
                </tr>
            
                <tr ng-class="{'danger':rates.publish_status!='1'}" class="hidden-field">
                    <td colspan="3">
                        <table class="table table-borderless table-condensed" style="background:none">
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td>
                                        <div class="pull-right"> 
                                            <a href="" ng-click="publishUnpublishScheduleRatesTRANS(rates, 0)" ng-show="rates.publish_status == '1'">
                                                <div style="color:#090"><img src="<?=base_url("public/images/on-button.png")?>"></span> </div>
                                            </a> 
                                            <a href="" ng-click="publishUnpublishScheduleRatesTRANS(rates, 1)" ng-show="rates.publish_status != '1'">
                                                <div style="color:#F30"><img src="<?=base_url("public/images/off-button.png")?>"></span></div>
                                            </a> 
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100">Code</td>
                                    <td><strong>{{rates.rates_code}}</strong></td>
                                </tr>
                                <tr>
                                    <td>Booking Handling</td>
                                    <td>{{rates.booking_handling}}</td>
                                </tr>
                                <tr>
                                    <td width="140">Available On</td>
                                    <td>
                                        <div class="btn-group btn-days">
                                            <button type="button" class="btn btn-xs" ng-class="{'btn-info':rates.available_on == 'all_day' || rates.available_on.indexOf('mon') >= 0}">Mon</button>
                                            <button type="button" class="btn btn-xs" ng-class="{'btn-info':rates.available_on == 'all_day' || rates.available_on.indexOf('tue') >= 0}">Tue</button>
                                            <button type="button" class="btn btn-xs" ng-class="{'btn-info':rates.available_on == 'all_day' || rates.available_on.indexOf('wed') >= 0}">Wed</button>
                                            <button type="button" class="btn btn-xs" ng-class="{'btn-info':rates.available_on == 'all_day' || rates.available_on.indexOf('thu') >= 0}">Thu</button>
                                            <button type="button" class="btn btn-xs" ng-class="{'btn-info':rates.available_on == 'all_day' || rates.available_on.indexOf('fri') >= 0}">Fri</button>
                                            <button type="button" class="btn btn-xs" ng-class="{'btn-info':rates.available_on == 'all_day' || rates.available_on.indexOf('sat') >= 0}">Sat</button>
                                            <button type="button" class="btn btn-xs" ng-class="{'btn-info':rates.available_on == 'all_day' || rates.available_on.indexOf('sun') >= 0}">Sun</button>
                                        </div>
                                    </td>
                                </tr>
                                <tr ng-show="rates.rates_for.walkin || rates.rates_for.offline">
                                    <td>Range of dates</td>
                                    <td><strong>{{fn.formatDate(rates.start_date, "d MM yy")}}</strong> to <strong>{{fn.formatDate(rates.end_date, "d MM yy")}}</strong></td>
                                </tr>
                                <?php /*?><tr>
                                    <td>Auto Discount</td>
                                    <td> IDR 100.000 </td>
                                </tr><?php */?>
                                <tr>
                                    <td>Cut Off Booking</td>
                                    <td>{{rates.cut_of_booking}} days in advance</td>
                                </tr>
                                <tr>
                                    <td>Minimum Order</td>
                                    <td>{{rates.min_order}} Pax</td>
                                </tr>
                                <tr>
                                    <td> Pickup Service </td>
                                    <td> <span class="text-uppercase">{{rates.pickup_service}}</span> </td>
                                </tr>
                                <tr ng-show="rates.pickup_service=='yes' && rates.pickup_area">
                                    <td> Pick Up Area </td>
                                    <td><ul style="margin:0; padding-left:15px">
                                            <li ng-repeat='area in rates.pickup_area'> 
                                                {{area.area}} - {{area.time}} <strong ng-show='area.price > 0'> - {{area.currency}} {{fn.formatNumber(area.price, area.currency)}} / {{area.type}}</strong> 
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td> Drop Off Service </td>
                                    <td> <span class="text-uppercase">{{rates.dropoff_service}}</span> </td>
                                </tr>
                                <tr ng-show="rates.dropoff_service=='yes' && rates.dropoff_area">
                                    <td> Dropoff Area </td>
                                    <td>
                                        <ul style="margin:0; padding-left:15px">
                                            <li ng-repeat='area in rates.dropoff_area'> 
                                                {{area.area}} - {{area.time}} <strong ng-show='area.price > 0'> - {{area.currency}} {{fn.formatNumber(area.price, area.currency)}} / {{area.type}}</strong> 
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td>Inclusions</td>
                                    <td>
                                        <ul style="margin:0; padding-left:15px" ng-show='rates.inclusion'>
                                            <li ng-repeat='str in rates.inclusion'>{{str}}</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Exclusions</td>
                                    <td>
                                        <ul style="margin:0; padding-left:15px" ng-show='rates.exclusion'>
                                            <li ng-repeat='str in rates.exclusion'>{{str}}</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Additional info</td>
                                    <td>
                                        <ul style="margin:0; padding-left:15px" ng-show='rates.additional_info'>
                                            <li ng-repeat='str in rates.additional_info'>{{str}}</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr ng-show='rates.close_date'>
                                    <td>Exception Date</td>
                                    <td>
                                        <ul style="margin:0; padding-left:15px">
                                            <li ng-repeat='str in rates.close_date'>{{fn.formatDate(str.date, "dd M yy")}}</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr ng-show='rates.additional_charge'>
                                    <td>Additional Charge</td>
                                    <td>
                                        <ul style="margin:0; padding-left:15px" ng-show='rates.additional_charge'>
                                            <li ng-repeat='item in rates.additional_charge'>
                                                {{item.name}} - <strong>{{item.currency}} {{fn.formatNumber(item.price, item.currency)}} </strong>
                                                 / {{item.unit}}
                                                <div ng-show='item.description'>
                                                    <small>{{item.description}}</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <hr style="margin:10px 0">
                        <div align="right">
                            <a ui-sref="transport.trips_schedule_detail.edit_rates_for_contract({'schedule_code':schedule.schedule_code,'contract_rates_code':DATA.current_contract_rates.contract_rates_code,'rates_code':rates.rates_code})"><span class="glyphicon glyphicon-pencil"></span> Edit Rates</a>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="" style="color:red" ng-click="removeScheduleRatesTRANS(rates)"><span class="glyphicon glyphicon-trash"></span> Remove Rates</a>
                            
                            <?php /*?><a ui-sref="transport.trips_schedule_detail.edit_rates({'rates_code':rates.rates_code})"><span class="glyphicon glyphicon-pencil"></span> Edit Rates</a>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <a ui-sref="transport.trips_schedule_detail.smart_pricing({'rates_code':rates.rates_code})"><span class="glyphicon glyphicon-signal"></span> Smart Pricing</a>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="" ng-click="duplicateScheduleRates(rates)"><span class="glyphicon glyphicon-duplicate"></span>  Duplicate Rates</a>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="" style="color:red" ng-click="deleteScheduleRates(rates)"><span class="glyphicon glyphicon-trash"></span> </a><?php */?>
                        </div>
                    </td>
                </tr>
                <tr ng-hide="($index+1 == DATA.current_contract_rates.rates_trans_by_id_schedule[schedule.id].length)">
                    <td colspan="3" style="background:#F5F5F5; padding:3px"></td>
                </tr>
            </tbody>
            <tbody>
                <tr style="background:#F5F5F5">
                    <td colspan="3" align="center" style="padding:2px">
                        <?php /*?><a ui-sref="transport.trips_schedule_detail.new_rates_for_contract({'schedule_code':schedule.schedule_code,'contract_rates_code':DATA.current_contract_rates.contract_rates_code})" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-plus"></span> Add Rates</a><?php */?>
                        
                        <div class="btn-group">
							<button type="button" class="btn btn-sm btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="glyphicon glyphicon-plus"></span> Add Rates <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a ui-sref="transport.trips_schedule_detail.new_rates_for_contract({'schedule_code':schedule.schedule_code,'contract_rates_code':DATA.current_contract_rates.contract_rates_code})"><span class="glyphicon glyphicon-plus"></span> Create New Rates</a></li>
								<li><a href="" data-toggle="modal" data-target="#popUpSelectRatesProductTrans" ng-click='popUpSelectMasterRatesProductTRANS(schedule)'><span class="glyphicon glyphicon-ok"></span> Add from existing Rates</a></li>
							</ul>
						</div>
                    </td>
                </tr>
            </tbody>
        </table>
        <br />
    </div>
    <hr />
</div>
<?php //--END Trips & Schedules ------------------------------------------- ?>

<div class="modal fade" id="popUpSelectRatesProductTrans" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form ng-submit="popUpSubmitSelectedMasterRatesProductTRANS($event)">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">{{DATA.myMasterRatesProductTRANS.schedule.schedule_code}} <i class="fa fa-ship" aria-hidden="true"></i> Boat : {{DATA.myMasterRatesProductTRANS.schedule.boat.name}}</h4>
      </div>
      <div class="modal-body">
      	<div ng-show="DATA.myMasterRatesProductTRANS.rates.status != 'ERROR'" class="contract-select-rates-list">
        	<div class="pull-right" style="margin-top:-10px">
            	Search : 
                <input type="text" class="form-control input-sm" placeholder="Search..." ng-model="search_rates_pop_up_trans" style="display:inline; width:200px" />
            </div>
	      	<h4>Select Rates</h4>
            <ol style="clear:both">
				<li ng-repeat="rates in DATA.myMasterRatesProductTRANS.rates | filter:search_rates_pop_up_trans" 
                	ng-class="{'selected':(rates.selected)}" ng-click='popUpSelectThisMasterRatesProductTRANS(rates)'>
                	{{rates.departure.port.name}} ({{rates.departure.port.port_code}}) : {{rates.departure.time}}
                    <i class="fa fa-chevron-right"></i>
                    {{rates.arrival.port.name}} ({{rates.arrival.port.port_code}}) : {{rates.arrival.time}}
                    <br />
                   	 {{rates.rates_code}} - {{rates.name}}
                </li>
            </ol>
            <div ng-show='DATA.myMasterRatesProductTRANS.qty_selected_rates'>
                <hr />
                <em><strong>{{DATA.myMasterRatesProductTRANS.qty_selected_rates}}</strong> rates selected.</em>
      		</div>
        </div>
        <div ng-show="DATA.myMasterRatesProductTRANS.rates.status == 'ERROR'" class="alert alert-danger">
        	<strong>Sorry</strong>... Rates not found.
        </div>
      </div>
      <div class="modal-footer" style="text-align:center">
      	<button type="submit" class="btn btn-primary" ng-disabled='!DATA.myMasterRatesProductTRANS.qty_selected_rates'>Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </form>
    </div>
  </div>
</div>
<?php //sync ?>
<div class="sub-title">Contract Rates Templates</div> 
<br />
<div ui-view>

	<div ng-init="loadDataCategory()">
		<div ng-show="DATA.categories.status=='SUCCESS'">
			<div class="products">
				<div class="product ">
					Filter : <input type="text" ng-model="filter_categories" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
				</div>
				
				<div ng-show='!DATA.categories.categories'>
					<img src="<?=base_url("public/images/loading_bar.gif")?>" />
				</div>
				
				<table class="table table-bordered table-condensed">
					<tr class="info" style="font-weight:bold">
						<td width="30">No.</td>
                        <td width="100">Template Code</td>
						<td>Contract Rates Name</td>
						<td width="120">Type</td>
                        <td width="160" align="center">Periode</td>
						<td width="80"></td>
						<td width="30"></td>
					</tr>
					<tr ng-class="{'danger':category.publish_status!='1'}" ng-repeat="category in DATA.categories.categories | orderBy:['name'] | filter : filter_categories ">
						<td>{{($index+1)}}</td>
                        <td align="center">{{category.category_code}}</td>
						<td>
							<a ui-sref="agent.category.detail({'category_code':category.category_code})">
								{{category.name}}
							</a>
						</td>
						
						<td width="100">
							{{category.contract_rate_type_desc}}
							<span ng-show="category.contract_rate_type == 'PERCENT'"> {{category.percentage}}%</span>
						</td>
						<td align="center">
                        	{{fn.newDate(category.start_date) | date:'dd MMM yyyy'}} -
                            {{fn.newDate(category.end_date) | date:'dd MMM yyyy'}}
                        </td>
						<td align="center">
							<a href="" ng-click="publishUnpublishCategory(category, 0)" ng-show="category.publish_status == '1'">
								<div style="color:#090"><img src="<?=base_url("public/images/on-button.png")?>" width="100%" /></span> </div>
							</a> 
							<a href="" ng-click="publishUnpublishCategory(category, 1)" ng-show="category.publish_status != '1'">
								<div style="color:#F30"><img src="<?=base_url("public/images/off-button.png")?>" width="100%" /></span></div>
							</a> 
						</td>
						<td align="center">
							<a href="" data-toggle="modal" ng-click='addEditCategory(category)' data-target="#add-edit-category">
								<span class="glyphicon glyphicon-pencil"></span>
							</a>
						</td>
					</tr>
				</table>
			</div>
		</div>
		
		<hr>
		<div class="add-product-button"> <a href="" class="btn btn-success btn-lg btn-block" data-toggle="modal" ng-click='addEditCategory()' data-target="#add-edit-category"> <span class="glyphicon glyphicon-plus"></span> Contract Rates Templates</a> </div>
		<br>
		<br>
		
		<div class="modal fade" id="add-edit-category" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <form ng-submit='saveDataCategory($event)'>
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">
					<span ng-show='!DATA.myCategory.id'>Add</span><span ng-show='DATA.myCategory.id'>Edit</span> Contract Rates
				</h4>
			  </div>
			  <div class="modal-body">
				<div ng-show='DATA.myCategory.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myCategory.error_msg'>{{err}}</li></ul></div>
				<table class="table table-borderless table-condensed">
					<tr>
						<td width="120">Name*</td>
						<td><input placeholder="Name" required="required" type="text" class="form-control input-md" ng-model='DATA.myCategory.name' /></td>
					</tr>
					<tr>
						<td>Rates Type*</td>
						<td class="form-inline">
							<select class="form-control input-md" ng-model='DATA.myCategory.contract_rate_type' style="display:inline; width:150px">
								<option value="PERCENT">Percentage</option>
								<option value="NETTRATES">Nett Rates</option>
							</select>
							<span ng-show="DATA.myCategory.contract_rate_type == 'PERCENT'">
								&nbsp;&nbsp;&nbsp;&nbsp;
								Percentage : 
								<input placeholder="Percentage" ng-required="DATA.myCategory.contract_rate_type == 'PERCENT'" type="number" min="0" step="0.01" max="100" class="form-control input-md" ng-model='DATA.myCategory.percentage' style="width:80px" />
								%
							</span>
						</td>
					</tr>
                    <tr>
                    	<td>Periode*</td>
                        <td>
                        	<input type="text" required="required" ng-model='DATA.myCategory.start_date' class="form-control input-md datepicker" style="display:inline; width:150px" placeholder="Start Date" />
                            -
                            <input type="text" required="required" ng-model='DATA.myCategory.end_date' class="form-control input-md datepicker" style="display:inline; width:150px" placeholder="End Date" />
                        </td>
                    </tr>
					<tr>
						<td>Description</td>
						<td>
							<textarea placeholder="Description" class="form-control input-md" ng-model='DATA.myCategory.description' rows="5"></textarea>
						</td>
					</tr>
				</table>
			  </div>
			  <div class="modal-footer" style="text-align:center">
				<button type="submit" class="btn btn-primary">Save</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			  </div>
			</div>
		  </div>
		  </form>
		</div>
		
	</div>
	<script>GeneralJS.activateSubMenu(".nav-pills", "li", ".category");</script>

</div>
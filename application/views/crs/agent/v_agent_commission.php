<div ng-init="loadDataCommission()">
	<div ng-show="DATA.commissions">
    	<div class="products">
            <div class="product ">
                Filter : <input type="text" ng-model="filter_commissions" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
            </div>
    	</div>
		<div class="table-responsive">
			<table class="table table-condensed table-bordered">
				<tr class="header bold">
					<td width="120" align="center">Code</td>
					<td width="90" align="center">Trx. Date</td>
					<td width="90" align="center">Order#</td>
                    <td width="110" align="right">Grand Total</td>
                    <td width="110" align="right">Commission</td>
					<td align="left">Payment</td>
				</tr>
				<tbody ng-repeat="commission in DATA.commissions | filter : filter_commissions">

					<tr ng-class="{'warning':(!commission.payment)}">
						<td align="center" rowspan="2">
                            <strong>{{commission.commission_code}}</strong>
						</td>
						<td align="center">
							{{fn.formatDate(commission.transaction_date, "dd M yy")}}
						</td>
						<td align="center">
                        	<a ui-sref="reservation.detail({'booking_code':commission.booking_code})" target="_blank"><strong>{{commission.booking_code}}</strong></a>
                        </td>
                        <td align="right">
							{{commission.currency}} {{fn.formatNumber(commission.grand_total, commission.currency)}}
						</td>
                        <td align="right">
							{{commission.currency}} {{fn.formatNumber(commission.commission_amount, commission.currency)}}
						</td>
						<td>
                        	<div ng-show='!commission.payment'>
                                <a href="" data-toggle="modal" ng-click="editCommission(commission)" data-target="#add-edit-commission">
                                    <span class="glyphicon glyphicon-plus"></span> Redeem Commission
                                </a>
                            </div>
                            <div ng-show='commission.payment'>
                            	<span class="glyphicon glyphicon-ok"></span>
								{{fn.formatDate(commission.payment.payment_date, "dd M yy")}} - {{commission.payment.source_of_funds}}
							</div>
                        </td>
					</tr>
                    <tr ng-class="{'warning':(!commission.payment)}">
                    	<td colspan="5">
                        	<em>Note : <span ng-show="commission.remarks">{{commission.remarks}}</span> <span ng-show="!commission.remarks">-</span></em>
                        </td>
                    </tr>
                    <tr>
                    	<td colspan="6" style="background:#FAFAFA"></td>
                    </tr>
				</tbody>
			</table>
		</div>
		
		<?php /*?><nav aria-label="Page navigation" class="pull-right">
		  <ul class="pagination pagination-sm">
			<li ng-class="{'disabled':DATA.transaction.search.page <= 1}">
			  <a href="" ng-click='loadDataTransactionTransport(DATA.transaction.search.page-1)' aria-label="Prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
			</li>
			<li ng-repeat='pagination in DATA.transaction.search.pagination' ng-class="{'active':DATA.transaction.search.page == pagination}">
				<a href="" ng-click='loadDataTransactionTransport(($index+1))'>{{($index+1)}}</a>
			</li>
			<li ng-class="{'disabled':DATA.transaction.search.page >= DATA.transaction.search.number_of_pages}">
			  <a href="" ng-click='loadDataTransactionTransport(DATA.transaction.search.page+1)' aria-label="Next"><span class="glyphicon glyphicon-chevron-right"></span></a>
			</li>
		  </ul>
		</nav><?php */?>
		<div class="clearfix"></div>
		<?php /*?><div class="text-right">
			<table class="table table-condensed table-borderless">
				<tr>
					<td>Total : </td>
					<td><strong>{{DATA.deposit.currency}} {{fn.formatNumber(DATA.deposit.deposit, DATA.deposit.currency)}}</strong></td>
				</tr>
				<tr>
					<td>Used : </td>
					<td width="130"><strong>{{DATA.deposit.currency}} {{fn.formatNumber(DATA.deposit.transaction, DATA.deposit.currency)}}</strong></td>
				</tr>
				<tr>
					<td>Deposit Remaining : </td>
					<td><strong>{{DATA.deposit.currency}} {{fn.formatNumber(DATA.deposit.current_deposit, DATA.deposit.currency)}}</strong></td>
				</tr>
			</table>
		</div><?php */?>
	</div>
	
	<div ng-show="show_loading">
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	<br />
	<div ng-show='!DATA.commissions' class="alert alert-warning"><em>Data not found...</em></div>
    <hr>
	<?php /*?><div class="add-product-button"> 
		<a href="" class="btn btn-success btn-lg btn-block" data-toggle="modal" ng-click='addEditDeposit()' data-target="#add-edit-payment"> <span class="glyphicon glyphicon-plus"></span> Add Deposit </a> 
	</div><?php */?>
	<br>

	<!-- modal add payment -->	
	<div class="modal fade" id="add-edit-commission" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  	<form ng-submit='saveCommission($event)'>
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">
                        Commission Booking #{{DATA.myCommission.booking_code}}
                    </h4>
                  </div>
                  <div class="modal-body">
                    <div ng-show='DATA.myCommission.error_desc.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myCommission.error_desc'>{{err}}</li></ul></div>
                    <table class="table table-borderless table-condensed">
                        <tr>
                            <td>Commission Code</td>
                            <td>{{DATA.myCommission.commission_code}}</td>
                        </tr>
                        <tr>
                        	<td>Trx. Date</td>
                            <td>{{fn.formatDate(DATA.myCommission.transaction_date, "dd M yy")}}</td>
                        </tr>
                        <tr>
                        	<td>Order#</td>
                            <td>{{DATA.myCommission.booking_code}}</td>
                        </tr>
                        <tr>
                            <td>Grand Total</td>
                            <td>
                                {{DATA.myCommission.currency}} {{fn.formatNumber(DATA.myCommission.grand_total,DATA.myCommission.currency)}}
                            </td>
                        </tr>
                        <tr>
                            <td width="130">Commission Amount</td>
                            <td>
                                <strong>{{DATA.myCommission.currency}} {{fn.formatNumber(DATA.myCommission.commission_amount,DATA.myCommission.currency)}}</strong>
                            </td>
                        </tr>
                        <tr>
                        	<td>Agent</td>
                            <td><strong>{{DATA.myCommission.agent.name}}</strong></td>
                        </tr>
                        <tr>
                            <td>Funds Resource*</td>
                            <td>
                                <select class="form-control input-md" ng-model='DATA.myCommission.payment_method_code' required>
                                    <option value="" disabled="disabled"> -- Funds Resource --</option>
                                    <option ng-repeat="payment_method in DATA_payment_method_out | orderBy : 'name'" value="{{payment_method.code}}" ng-show="payment_method.code != 'OPENVOUCHER' && payment_method.code != 'ONLINE'">
                                        {{payment_method.name}}
                                    </option>
                                </select>
                                
                                <div ng-hide='DATA_payment_method_out.length>0' class="alert alert-warning" style="margin-top:10px">
                                	Commission Redeem Cannot processed, 
                                    there are no cash out account set up, 
                                    please refer to following <a href="#/setting/cash_and_bank" target="_blank">link</a> to setup cash out account.
                                </div>
                                <?php /*?><input type="text" ng-required="DATA.myRefund.approval_status=='1'" class="form-control input-md" placeholder="Ex.: Account# | Bank BCA" ng-model='DATA.myRefund.source_of_funds' /><?php */?>
                            </td>
                        </tr>
                        <?php /*?><tr>
                            <td width="130">Remarks</td>
                            <td>
                                <textarea placeholder="Description" required="required" type="text" class="form-control input-md autoheight" ng-model='DATA.myRefund.remarks' rows="3"></textarea>
                            </td>
                        </tr><?php */?>
                    </table>
                    
                  </div>
                  <div class="modal-footer" style="text-align:center">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                  </div>
                </div>
            </div>
        </form>
	</div>
</div>

<script>activate_sub_menu_agent_detail("commission");</script>
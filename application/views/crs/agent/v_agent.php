<?php //sync:180827 ?>
<div class="pull-right">
    <ul class="nav nav-pills">
        <?php /*?><li role="presentation" class="category"><a ui-sref="agent.category"><i class="fa fa-address-book-o" aria-hidden="true"></i> Contract Rates</a></li><?php */?>
        <li role="presentation" class="real_category"><a ui-sref="agent.real_category"><i class="fa fa-th-large" aria-hidden="true"></i> Rates Category</a></li>
        <li role="presentation" class="agent"><a ui-sref="agent"><i class="fa fa-users" aria-hidden="true"></i> Business Source</a></li>
    </ul>
</div>

<h1>Business Source & Contracts</h1>

<div ui-view>

    <div ng-init="loadDataAgent();">
        <div>
            <div class="products">
                <div class="product">
                    <form ng-submit='loadDataAgent(1, true)'>
                    <div class="table-responsive">
                        <table class="table table-condensed table-borderless" width="100%">
                            <tr>
                                <td width="230">Search</td>
                                <td width="200">Rates Category</td>
                                <td width="130">Status</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><input type="text" class="form-control input-sm" placeholder="Search" ng-model='search.q' /></td>
                                <td>
                                    <select class="form-control input-sm" ng-model='search.real_category_code'>
                                        <option value="">-- All Category --</option>
                                        <option value="{{category.real_category_code}}" ng-repeat="category in $root.DATA_real_categories | orderBy : 'name'">{{category.name}}</option>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control input-sm" ng-model='search.publish_status'>
                                        <option value="all">All</option>
                                        <option value="1">Published</option>
                                        <option value="0">Unpublished</option>
                                    </select>
                                </td>
                                <td><button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button></td>
                            </tr>
                        </table>
                        
                        <?php /*?>Filter : <input type="text" ng-model="DATA.filter_booking" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
                        <button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button><?php */?>
                    </div>  
                    </form>
                </div>
            
                <?php /*?><div class="product">
                    Filter : <input type="text" ng-model="filter_agents" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
                </div><?php */?>
                
                <div ng-show='show_loading_DATA_bookings'>
                    <img src="<?=base_url("public/images/loading_bar.gif")?>" />
                </div>
                 <!-- <div class="pull-right" style="margin-bottom: 10px;"> <a ui-sref="agent.add" class="btn btn-success btn-md"> <span class="glyphicon glyphicon-plus"></span> New Business Source </a> </div>
                 <div class="pull-left" style="margin-bottom: 10px;"> <a  data-toggle="modal" data-target="#upload-excell" class="btn btn-info btn-sm"> <span class="glyphicon glyphicon-upload"></span> Import Excell Data </a> </div>
                <div ng-show='!show_loading_DATA_bookings'> -->
                 <div class="pull-right" style="margin-bottom: 10px;">
                    <div class="dropdown">
                        <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="fa fa-plus" aria-hidden="true"></i> New Business Source
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="" ui-sref="agent.add" target="_blank"> New Business Source </a> </li>
                             <li><a href="" data-toggle="modal" data-target="#upload-excell"> Import Excell Data </a> </li>
                        </ul>
                     
                    </div>
                </div>
                <br>
                <table class="table table-bordered table-condensed" ng-show='DATA.agents.agents'>
                    <tr class="info" style="font-weight:bold">
                        <td width="30">No.</td>
                        <td>Name</td>
                        <?php /*?><td align="center" width="100">Code</td><?php */?>
                        <td width="150">Type</td>
                        <?php /*?><td width="150">Payment Type</td><?php */?>
                        <td width="200">Rates Category</td>
                        <td width="80"></td>
                        <td width="40"></td>
                    </tr>
                    <tr ng-class="{'danger':agent.publish_status!='1'}" ng-repeat="agent in DATA.agents.agents | orderBy:['name'] | filter : filter_agents ">
                        <td>{{($index+1)}}</td>
                        <td>
                            <a ui-sref="agent.detail({'agent_code':agent.agent_code})"><strong>{{agent.name}}</strong></a><br />
                            {{agent.agent_code}}<br />
                            {{agent.email}}<br />
                            {{agent.phone}}
                        </td>
                        <?php /*?><td align="center">{{agent.agent_code}}</td><?php */?>
                        <?php /*?><td>{{agent.phone}}</td><?php */?>
                        <td>{{agent.payment_method.description}}</td>
                        <td>
                            <ul ng-show='agent.real_categories' style="padding-left:20px">
                                <li ng-repeat="_cat in agent.real_categories">{{_cat.name}}</li>
                            </ul>
                            <?php /*?>{{agent.real_category.name}}
                            <div ng-show="agent.last_contract_rates && agent.last_contract_rates.name !=''">
                                <hr style="margin:5px 0" />
                                <small>Contract Rates :</small><br />
                                {{agent.last_contract_rates.name}}<br />
                                <a ui-sref="agent.detail.contract_rates({'agent_code':agent.agent_code})">More...</a>
                            </div><?php */?>
                        </td>
                        <td align="center">
                            <a href="" ng-click="publishUnpublishAgent(agent, 0)" ng-show="agent.publish_status == '1'">
                                <div style="color:#090"><img src="<?=base_url("public/images/on-button.png")?>" width="100%"></span> </div>
                            </a> 
                            <a href="" ng-click="publishUnpublishAgent(agent, 1)" ng-show="agent.publish_status != '1'">
                                <div style="color:#F30"><img src="<?=base_url("public/images/off-button.png")?>" width="100%"></span></div>
                            </a> 
                        </td>
                        <td align="center">
                            <div ng-show="agent.payment_method.payment_code == 'DEPOSIT' || agent.payment_method.payment_code == 'ACL'" class="btn-group">
                             <!-- <button type="button" class="btn btn-info btn-xs dropdown-toggle" id="link_auto" title="Auto Login"><span class="glyphicon glyphicon-log-in"></span></button> -->
                              <div class="btn-group pull-right">
                                <button type="button" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="<?=site_url("login_agent/autologin")?>?
                                        s[id]={{agent.id}}&
                                        s[code]=<?=md5($vendor['code']);?>
                                        " target="_blank">Autologin Agent</a>
                                    </li>
                                </ul>
                              </div>
                            </div>
                        </td>
                    </tr>
                </table>
                
                <nav aria-label="Page navigation" class="pull-right">
                  <ul class="pagination pagination-sm">
                    <li ng-class="{'disabled':DATA.agents.search.page <= 1}">
                      <a href="" ng-click='loadDataAgent(DATA.agents.search.page-1, true)' aria-label="Prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                    </li>
                    <li ng-repeat='pagination in DATA.agents.search.pagination' ng-class="{'active':DATA.agents.search.page == pagination}">
                        <a href="" ng-click='loadDataAgent(($index+1), true)'>{{($index+1)}}</a>
                    </li>
                    <li ng-class="{'disabled':DATA.agents.search.page >= DATA.agents.search.number_of_pages}">
                      <a href="" ng-click='loadDataAgent(DATA.agents.search.page+1, true)' aria-label="Next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                    </li>
                  </ul>
                </nav>
                <div class="clearfix"></div>
                
                </div>
                
            </div>
        </div>
        <hr>
        <div class="add-product-button"> <a ui-sref="agent.add" class="btn btn-success btn-lg btn-block"> <span class="glyphicon glyphicon-plus"></span> New Business Source </a> </div>
        <br>
        <br>
    </div>
    <script>GeneralJS.activateSubMenu(".nav-pills", "li", ".agent");</script>

    <div ng-init="loadDataRealCategory()" class="modal fade" id="upload-excell" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" style="width: 1000px !important;" role="document">
        <div class="modal-content">
          <form ng-submit="submitUploadExcell()">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">
                 
                <span>Import Excell</span> Data 
            </h4>
            </div>
            <div class="modal-body">

                <div style="padding: 15px;">
                    <label>
                        Select Rate CAtegory
                    </label>
                    <select class="form-control input-sm" ng-model="DATA.real_category" required>
                        <option value="">-- Select Category --</option>
                        <option value="{{cat.id}}" ng-repeat="cat in DATA.real_categories.real_categories">{{cat.name}}</option>
                    </select>
                 </div>
                <div class="btn btn-sm-success" style="margin-top: 10px;">
                    <input required type="file" id="file" placeholder='Choose a file...' ng-model="csvFile" onchange="angular.element(this).scope().ExcelExport(event)"/>    
                 </div>
                <div style="margin-top: 10px;padding: 15px;">
                    <span style="color: red;"><i>Please upload *xls or *xlsx format. Maximum only 500 data. Download excel format<a href="<?=base_url("public/document/import_data_agent.xlsx")?>" style="cursor: pointer;"> here</a></i></span>
                </div> 
                <div ng-show='show_loading'>
                    <span>Loading preview</span><img src="<?=base_url("public/images/loading_new.gif")?>" />
                </div>
                <div class="table-responsive" ng-show="DATA.excell_data" style="height: 400px;overflow-y: auto;padding: 15px;">
                    <table class="table table-condensed table-borderless" width="100%">
                        <tr>
                            <th>
                                No.
                            </th>
                            <th width="150">
                                Name
                            </th>
                            <th width="150">
                                Email
                            </th>
                            <th width="150">
                                Phone
                            </th>
                            <th width="150">
                                Website
                            </th>
                            <th width="150">
                                Address
                            </th>
                            <th width="100">
                                Contry
                            </th>
                            <th width="150">PIC</th>
                        </tr>
                        <tr  ng-repeat="data in DATA.excell_data">
                            <td>
                                {{data.no}}
                            </td>
                            <td>
                                {{data.name}}
                            </td>
                            <td>
                                {{data.email}}
                            </td>
                            <td>
                                {{data.phone}}
                            </td>
                            <td>
                                {{data.website}}
                            </td>
                            <td>
                                {{data.address}}
                            </td>
                            <td>
                                {{data.country}}
                            </td>
                            <td>
                                {{data.pic}}
                            </td>
                        </tr>
                    </table>
                </div>

            </div>
            <div class="modal-footer" style="text-align:center">
                <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#data-excell">Upload</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </form>
    </div>
        </div>
    </div>

</div>

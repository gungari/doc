<div ng-init="loadDataCategoryDetail()" class="agent-detail"> 
	<div class="title">
		<h1>{{DATA.category_detail.name}}</h1>
		<div class="code">Template Code : {{DATA.category_detail.category_code}}</div>
		<div class="code">
			Type : {{DATA.category_detail.contract_rate_type_desc}} 
			<span ng-show="DATA.category_detail.contract_rate_type == 'PERCENT'"> {{DATA.category_detail.percentage}}%</span>
		</div>
		<div class="code">
        	Periode : {{fn.newDate(DATA.category_detail.start_date) | date:'dd MMMM yyyy'}} - {{fn.newDate(DATA.category_detail.end_date) | date:'dd MMMM yyyy'}}
        </div>
        <div class="desc" ng-show="DATA.category_detail.description">
        	{{DATA.category_detail.description}}
        </div>
	</div>
	<?php /*?><div class="sub-title">
		<span ng-show='contract_rates_edit_mode'>Edit</span> Contract Rate
	</div><?php */?>
	<br />
    
	<form ng-submit='saveDataContractRatesCategory($event)'>
    	<div ng-show='!DATA.category_detail.contract_rates && !DATA.category_detail.contract_rates_act'>
        	<div class="alert alert-warning">
            	<strong>Rates</strong> not found...
            </div>
        </div>
        <?php // RATES TRANSPORT =========================================================== ?>
        <div ng-show='DATA.category_detail.contract_rates'>
        	<h4 ng-show='DATA.schedules.schedules'>Trips & Schedules Rates</h4>
            <table class="table table-bordered table-condensed">
                <tbody ng-repeat='schedules in DATA.schedules.schedules'>
                    <tr class="header">
                        <td><strong>{{schedules.schedule_code}}</strong> - <strong>{{schedules.boat.name}}</strong><br /></td>
                        <td ng-show="DATA.category_detail.contract_rate_type == 'PERCENT'"><strong>Publish</strong></td>
                        <td ng-show="DATA.category_detail.contract_rate_type == 'PERCENT'"></td>
                        <td ><strong>Nett</strong></td>
                        <td ng-show="DATA.category_detail.contract_rate_type == 'PERCENT'" width="80" align="center"></td>
                    </tr>
                    <tr ng-repeat='rates in DATA.rates[schedules.id].rates' ng-class="{'info':(rates.selected_agent_rates == '1' || DATA.contract_rates[rates.id])}"
                        ng-show="DATA.contract_rates[rates.id] || contract_rates_edit_mode">
                        <?php /*?><td width="40" align="center">
                            <input type="checkbox" ng-model="rates.selected_agent_rates" ng-true-value='1' ng-false-value='0' ng-checked="DATA.contract_rates[rates.id]" ng-disabled="!contract_rates_edit_mode" />	
                        </td><?php */?>
                        <td>
                            <strong>{{rates.departure.port.name}} ({{rates.departure.time}}) 
                            <i class="fa fa-chevron-right"></i>
                            {{rates.arrival.port.name}} ({{rates.arrival.time}})</strong>
                            <br />
                            <span style="color:blue">
                                <strong>{{rates.rates_code}}</strong> - <strong><em>{{rates.name}}</em></strong>
                            </span>
                        </td>
                        <td width="180">
                            <strong>One Way</strong><br />
                            <small>
                                <div>
                                    Adult : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.one_way_rates.rates_1, rates.currency)}}</strong>
                                </div>
                                <div ng-show='rates.rates.one_way_rates.rates_2 > 0'>
                                    Child : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.one_way_rates.rates_2, rates.currency)}}</strong>
                                </div>
                                <div ng-show='rates.rates.one_way_rates.rates_3 > 0'>
                                    Infant : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.one_way_rates.rates_3, rates.currency)}}</strong>
                                </div>
                            </small>
                            <strong>Return</strong><br />
                            <small>
                                <div ng-show='rates.rates.return_rates.rates_1 > 0'>
                                    Adult : <strong>{{rates.currency}} {{fn.formatNumber((rates.rates.return_rates.rates_1*2), rates.currency)}}</strong>
                                </div>
                                <div ng-show='rates.rates.return_rates.rates_2 > 0'>
                                    Child : <strong>{{rates.currency}} {{fn.formatNumber((rates.rates.return_rates.rates_2*2), rates.currency)}}</strong>
                                </div>
                                <div ng-show='rates.rates.return_rates.rates_3 > 0'>
                                    Infant : <strong>{{rates.currency}} {{fn.formatNumber((rates.rates.return_rates.rates_3*2), rates.currency)}}</strong>
                                </div>
                            </small>
                        </td>
                        <td width="30" style="vertical-align:middle; font-size:18px; text-align:center" ng-show="DATA.category_detail.contract_rate_type == 'PERCENT'">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </td>
                        <td width="180" ng-show="DATA.category_detail.contract_rate_type == 'PERCENT'">
                            <strong>One Way</strong><br />
                            <small>
                                <div>
                                    Adult : <strong>{{rates.currency}} {{fn.formatNumber(rates.nett_rates.one_way_rates.rates_1, rates.currency)}}</strong>
                                </div>
                                <div ng-show='rates.rates.one_way_rates.rates_2 > 0'>
                                    Child : <strong>{{rates.currency}} {{fn.formatNumber(rates.nett_rates.one_way_rates.rates_2, rates.currency)}}</strong>
                                </div>
                                <div ng-show='rates.rates.one_way_rates.rates_3 > 0'>
                                    Infant : <strong>{{rates.currency}} {{fn.formatNumber(rates.nett_rates.one_way_rates.rates_3, rates.currency)}}</strong>
                                </div>
                            </small>
                            <strong>Return</strong><br />
                            <small>
                                <div ng-show='rates.rates.return_rates.rates_1 > 0'>
                                    Adult : <strong>{{rates.currency}} {{fn.formatNumber((rates.nett_rates.return_rates.rates_1*2), rates.currency)}}</strong>
                                </div>
                                <div ng-show='rates.rates.return_rates.rates_2 > 0'>
                                    Child : <strong>{{rates.currency}} {{fn.formatNumber((rates.nett_rates.return_rates.rates_2*2), rates.currency)}}</strong>
                                </div>
                                <div ng-show='rates.rates.return_rates.rates_3 > 0'>
                                    Infant : <strong>{{rates.currency}} {{fn.formatNumber((rates.nett_rates.return_rates.rates_3*2), rates.currency)}}</strong>
                                </div>
                            </small>
                        
                            <?php /*?><strong>One Way</strong><br />
                            <small>
                                <div>
                                    Adult : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.one_way_rates.rates_1 - (rates.rates.one_way_rates.rates_1 * rates.percentage/100), rates.currency)}}</strong>
                                </div>
                                <div ng-show='rates.rates.one_way_rates.rates_2 > 0'>
                                    Child : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.one_way_rates.rates_2 - (rates.rates.one_way_rates.rates_2 * rates.percentage/100), rates.currency)}}</strong>
                                </div>
                                <div ng-show='rates.rates.one_way_rates.rates_3 > 0'>
                                    Infant : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.one_way_rates.rates_3 - (rates.rates.one_way_rates.rates_3 * rates.percentage/100), rates.currency)}}</strong>
                                </div>
                            </small>
                            <strong>Return</strong><br />
                            <small>
                                <div ng-show='rates.rates.return_rates.rates_1 > 0'>
                                    Adult : <strong>{{rates.currency}} {{fn.formatNumber((rates.rates.return_rates.rates_1*2)-(rates.rates.return_rates.rates_1*2*rates.percentage/100), rates.currency)}}</strong>
                                </div>
                                <div ng-show='rates.rates.return_rates.rates_2 > 0'>
                                    Child : <strong>{{rates.currency}} {{fn.formatNumber((rates.rates.return_rates.rates_2*2)-(rates.rates.return_rates.rates_2*2*rates.percentage/100), rates.currency)}}</strong>
                                </div>
                                <div ng-show='rates.rates.return_rates.rates_3 > 0'>
                                    Infant : <strong>{{rates.currency}} {{fn.formatNumber((rates.rates.return_rates.rates_3*2)-(rates.rates.return_rates.rates_3*2*rates.percentage/100), rates.currency)}}</strong>
                                </div>
                            </small><?php */?>
                        </td>
                        <td align="center" ng-show="DATA.category_detail.contract_rate_type == 'PERCENT'">
                            {{rates.percentage}} %
                        </td>
                    </tr>
                </tbody>
            </table>
        </div> 
        <?php // --END RATES TRANSPORT =========================================================== ?>
        
        
        <?php // RATES ACTIVITIES =========================================================== ?>
        <br />
        <div ng-show='DATA.category_detail.contract_rates_act'>
        	<h4 ng-show='DATA.products.products'>Products & Services Rates</h4>
            <table class="table table-bordered table-condensed">
                <tbody ng-repeat='products in DATA.products.products'>
                    <tr class="header">
                        <td>
                        	<a href="#activities/product_detail/{{products.product_code}}/rates" target="_blank">
                        		<strong>{{products.product_code}}</strong> - <strong>{{products.name}}</strong>
                            </a>
                            <br />
                        </td>
                        <td ng-show="DATA.category_detail.contract_rate_type == 'PERCENT'"><strong>Publish</strong></td>
                        <td ng-show="DATA.category_detail.contract_rate_type == 'PERCENT'"></td>
                        <td ><strong>Nett</strong></td>
                        <td ng-show="DATA.category_detail.contract_rate_type == 'PERCENT'" width="80" align="center"></td>
                    </tr>
                    <tr ng-repeat='rates in DATA.rates_act[products.id].rates' ng-class="{'info':(rates.selected_agent_rates == '1' || DATA.contract_rates_act[rates.id])}"
                        ng-show="DATA.contract_rates_act[rates.id] || contract_rates_edit_mode">
                        <td>
                            <?php /*?><strong>{{rates.departure.port.name}} ({{rates.departure.time}}) 
                            <i class="fa fa-chevron-right"></i>
                            {{rates.arrival.port.name}} ({{rates.arrival.time}})</strong>
                            <br /><?php */?>
                            <span style="color:blue">
                                <strong>{{rates.rates_code}}</strong> - <strong><em>{{rates.name}}</em></strong>
                            </span>
                        </td>
                        <td width="180">
                            <div>
                                {{rates.rates_caption.rates_1}} : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.rates_1, rates.currency)}}</strong>
                            </div>
                            <div ng-show='rates.rates.rates_2 > 0'>
                                {{rates.rates_caption.rates_2}} : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.rates_2, rates.currency)}}</strong>
                            </div>
                            <div ng-show='rates.rates.rates_3 > 0'>
                                {{rates.rates_caption.rates_3}} : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.rates_3, rates.currency)}}</strong>
                            </div>
                        </td>
                        <td width="30" style="vertical-align:middle; font-size:18px; text-align:center" ng-show="DATA.category_detail.contract_rate_type == 'PERCENT'">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </td>
                        <td width="180" ng-show="DATA.category_detail.contract_rate_type == 'PERCENT'">
                            <div>
                                {{rates.rates_caption.rates_1}} : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.rates_1 - (rates.rates.rates_1 * DATA.category_detail.percentage/100), rates.currency)}}</strong>
                            </div>
                            <div ng-show='rates.rates.rates_2 > 0'>
                                {{rates.rates_caption.rates_2}} : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.rates_2 - (rates.rates.rates_2 * DATA.category_detail.percentage/100), rates.currency)}}</strong>
                            </div>
                            <div ng-show='rates.rates.rates_3 > 0'>
                                {{rates.rates_caption.rates_3}} : <strong>{{rates.currency}} {{fn.formatNumber(rates.rates.rates_3 - (rates.rates.rates_3 * DATA.category_detail.percentage/100), rates.currency)}}</strong>
                            </div>
                        </td>
                        <td align="center" ng-show="DATA.category_detail.contract_rate_type == 'PERCENT'">
                            {{rates.percentage}} %
                        </td>
                    </tr>
                </tbody>
            </table>
        </div> 
        <?php // --END RATES ACTIVITIES =========================================================== ?>
        
		<hr />
		
		<div ng-show='!contract_rates_edit_mode'>
        	<a href="" ui-sref="agent.category.add_rates({'category_code':DATA.category_detail.category_code})">
            	<span class="glyphicon glyphicon-pencil"></span>
                Edit Contract Rates
            </a>
			<?php /*?><a href="" ng-click='contract_rates_edit_mode = true'>Edit Contract Rates</a><?php */?>
		</div>
		<div ng-show='contract_rates_edit_mode'>
			<button type="submit" class="btn btn-lg btn-primary">Submit Contract Rates</button>
			<a href="" ng-click='contract_rates_edit_mode = false'>Cancel</a>
		</div>
	</form>
</div>
<script>GeneralJS.activateSubMenu(".nav-pills", "li", ".category");</script>

<style>
	.agent-detail .title{margin-bottom:20px}
	.agent-detail .title h1{margin-bottom:10px !important;}
	.agent-detail .title .code{margin-bottom:5px;}
</style>
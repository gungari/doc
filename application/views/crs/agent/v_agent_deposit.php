<div ng-init="loadDataDeposit()">

	
	<div ng-show="DATA.deposit">
		<div class="table-responsive">
			<table class="table table-condensed table-bordered">
				<tr class="header bold">
					<td width="120" align="center">Code</td>
					<td width="130" align="center">Date</td>
					<td align="center">Type</td>
					<td width="150" align="right">Total Amount</td>
					<?php /*?><td width="30"></td><?php */?>
				</tr>
				<tbody ng-repeat="data in DATA.deposit.data">

					<tr ng-class="{'danger':(transaction.trx_type == 'REFUND')}">
						<td rowspan="2">
							<span>
								<a href="" data-toggle="modal" ng-click="paymentDetail(data)" data-target="#payment-detail">
									{{data.deposit_code}}
								</a>
							</span>
						</td>
						<td align="center">
							{{fn.formatDate(data.date, "dd M yy")}}
						</td>
						
						<td>
							{{data.payment_type}}
						</td>
						<td align="right">
							{{data.currency}} {{fn.formatNumber(data.amount, data.currency)}}
						</td>
						<?php /*?><td rowspan="2" align="center" valign="middle">
							<a href="" ng-click="paymentDetail(data, true)" data-toggle="modal" data-target="#payment-detail" class="delete-icon">
								<span class="glyphicon glyphicon-trash"></span>
							</a>
						</td><?php */?>
					</tr>
					<tr ng-class="{'danger':(transaction.trx_type == 'REFUND')}">
						<td colspan="3" style="font-size:11px">
							<em>Remarks : {{data.description}}</em>
						</td>
					</tr>
					<tr>
						<td colspan="8" style="background:#FAFAFA"></td>
					</tr>
				</tbody>
			</table>
		</div>
		
		<nav aria-label="Page navigation" class="pull-right">
		  <ul class="pagination pagination-sm">
			<li ng-class="{'disabled':DATA.transaction.search.page <= 1}">
			  <a href="" ng-click='loadDataTransactionTransport(DATA.transaction.search.page-1)' aria-label="Prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
			</li>
			<li ng-repeat='pagination in DATA.transaction.search.pagination' ng-class="{'active':DATA.transaction.search.page == pagination}">
				<a href="" ng-click='loadDataTransactionTransport(($index+1))'>{{($index+1)}}</a>
			</li>
			<li ng-class="{'disabled':DATA.transaction.search.page >= DATA.transaction.search.number_of_pages}">
			  <a href="" ng-click='loadDataTransactionTransport(DATA.transaction.search.page+1)' aria-label="Next"><span class="glyphicon glyphicon-chevron-right"></span></a>
			</li>
		  </ul>
		</nav>
		<div class="clearfix"></div>
		<div class="text-right">
			<table class="table table-condensed table-borderless">
				<tr>
					<td>Total Deposit: </td>
					<td><strong>{{DATA.deposit.currency}} {{fn.formatNumber(DATA.deposit.deposit, DATA.deposit.currency)}}</strong></td>
				</tr>
				<tr>
					<td>Used Deposit: </td>
					<td width="130"><strong><a href="" data-toggle="modal" data-target="#tranc-detail">{{DATA.deposit.currency}} {{fn.formatNumber(DATA.deposit.transaction.total, DATA.deposit.currency)}}</a></strong></td>
				</tr>
				<tr>
					<td>Remaining Deposit : </td>
					<td><strong>{{DATA.deposit.currency}} {{fn.formatNumber((DATA.deposit.current_deposit), DATA.deposit.currency)}}</strong></td>
				</tr>
			</table>
		</div>
	</div>
	
	<div ng-show="show_loading">
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	<br />
	<div ng-show='!DATA.deposit.data' class="alert alert-warning"><em>Data not found...</em></div>
    <hr>
	<div class="add-product-button"> 
		<a href="" class="btn btn-success btn-lg btn-block" data-toggle="modal" ng-click='addEditDeposit()' data-target="#add-edit-payment"> <span class="glyphicon glyphicon-plus"></span> Add Deposit </a> 
	</div>
	<br>

	<!-- modal add payment -->	
	<div class="modal fade" id="add-edit-payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <form ng-submit='saveDataDeposit($event)'>
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">
				<span ng-show='!DATA.myDeposit.id'>Add</span><span ng-show='DATA.myDeposit.id'>Edit</span> Deposit
			</h4>
		  </div>
		  <div class="modal-body">
		  	<div ng-show='DATA.myDeposit.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myDeposit.error_msg'>{{err}}</li></ul></div>
			<table class="table table-borderless table-condenseds">
				
				<tr>
					<td>Deposit Date*</td>
					<td><input placeholder="Deposit Date" required="required" type="text" class="form-control input-md datepicker" ng-model='DATA.myDeposit.date' style="width:150px" /></td>
				</tr>
				<tr>
					<td>Payment Type*</td>
					<td>
						<select required="required" class="form-control input-md" ng-model='DATA.myDeposit.payment_type' ng-change='changePaymentType()'>
							<option value="" disabled="disabled">-- Select Payment Type --</option>
							<option value="{{payment_method.code}}" ng-repeat='payment_method in $root.DATA_payment_method'
                                ng-hide="payment_method.code=='OPENVOUCHER' || payment_method.code=='ONLINE'">
								{{payment_method.name}}
							</option>
						</select>
					</td>
				</tr>
				
				<?php /*?><tr ng-show="DATA.myDeposit.payment_type=='CC' || DATA.myDeposit.payment_type=='ATM'" class="header"><?php */?>
                <tr ng-show="DATA.payment_is_cc" class="header">
					<td>
                    	<span ng-show="DATA.payment_is_cc">Card Number</span>
                        <span ng-show="DATA.payment_is_atm">Account Number</span>
                    </td>
					<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.myDeposit.account_number' /></td>
				</tr>
				<?php /*?><tr ng-show="DATA.myDeposit.payment_type=='CC' || DATA.myDeposit.payment_type=='ATM'" class="header"><?php */?>
                <tr ng-show="DATA.payment_is_cc" class="header">
					<td>Name On Card</td>
					<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.myDeposit.name_on_card' /></td>
				</tr>
				<?php /*?><tr ng-show="DATA.myDeposit.payment_type=='CC' || DATA.myDeposit.payment_type=='ATM'" class="header"><?php */?>
                <tr ng-show="DATA.payment_is_cc" class="header">
					<td>Bank Name</td>
					<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.myDeposit.bank_name' /></td>
				</tr>
				<?php /*?><tr ng-show="DATA.myDeposit.payment_type=='CC' || DATA.myDeposit.payment_type=='ATM'" class="header"><?php */?>
                <tr ng-show="DATA.payment_is_cc" class="header">
					<td>Approval Number</td>
					<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.myDeposit.payment_reff_number' /></td>
				</tr>
                
				<tr>
					<td>Remarks*</td>
					<td><input placeholder="Description" type="text" class="form-control input-md" ng-model='DATA.myDeposit.description' /></td>
				</tr>
				<tr ng-show="DATA.myDeposit.payment_type!='OPENVOUCHER'">
					<td>Deposit Amount*</td>
					<td>

						<div class="input-group">
							<span class="input-group-addon"><?=$vendor["default_currency"]?></span>
							<input placeholder="Payment Amount" required="required" type="number" min="0" class="form-control input-md" ng-model='DATA.myDeposit.amount' style="width:160px" />
						</div>
					</td>
				</tr>
			</table>
			
		  </div>
		  <div class="modal-footer" style="text-align:center">
		  	<button type="submit" class="btn btn-primary">Save</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
	  </div>
	  </form>
	</div>
	<!--/ modal add payment -->
	<div class="modal fade" id="payment-detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">
				 
				<span>Deposit</span> Detail #{{myPaymentDetail.deposit_code}}
			</h4>
		  </div>
		  <div class="modal-body">
		  	<div ng-show='DATA.myPayment.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myPayment.error_msg'>{{err}}</li></ul></div>
			<table class="table table-borderless table-condensed">
				<tr>
					<td width="130">Code</td>
					<td><strong>#{{myPaymentDetail.deposit_code}}</strong></td>
				</tr>
				<tr>
					<td width="130">Deposit Date</td>
					<td>{{fn.formatDate(myPaymentDetail.date, "d MM yy")}}</td>
				</tr>
				<tr>
					<td>Type</td>
					<td>{{myPaymentDetail.payment_type}}</td>
				</tr>
				<tr ng-show="myPaymentDetail.payment_type=='CC' || myPaymentDetail.payment_type=='TRANSFER'">
					<td><span ng-show="myPaymentDetail.payment_type=='CC'">Credit Card Number</span><span ng-show="myPaymentDetail.payment_type=='TRANSFER'">Account Number</span></td>
					<td>{{myPaymentDetail.account_number}}</td>
				</tr>
				<tr ng-show="myPaymentDetail.payment_type=='CC' || myPaymentDetail.payment_type=='TRANSFER'">
					<td>Name On Card</td>
					<td>{{myPaymentDetail.name_on_card}}</td>
				</tr>
				<tr ng-show="myPaymentDetail.payment_type=='CC' || myPaymentDetail.payment_type=='TRANSFER'">
					<td><span ng-show="myPaymentDetail.payment_type=='CC'">EDC Machine</span><span ng-show="myPaymentDetail.payment_type=='TRANSFER'">Bank Name</span></td>
					<td>{{myPaymentDetail.bank_name}}</td>
				</tr>
				<tr ng-show="myPaymentDetail.payment_type=='CC' || myPaymentDetail.payment_type=='TRANSFER'">
					<td>Approval Number</td>
					<td>{{myPaymentDetail.payment_reff_number}}</td>
				</tr>
				<tr ng-show="myPaymentDetail.payment_type_code=='OPENVOUCHER'">
					<td>Pre Paid Ticket Code</td>
					<td>{{myPaymentDetail.payment_reff_number}}</td>
				</tr>
				<tr>
					<td>Description</td>
					<td>{{myPaymentDetail.description}}</td>
				</tr>
				<tr>
					<td>Amount</td>
					<td>{{myPaymentDetail.currency}} {{fn.formatNumber(myPaymentDetail.amount, myPaymentDetail.currency)}}</td>
				</tr>
				
			</table>
			  	<div ng-show='myPaymentDetail.delete_payment' class="alert alert-warning">
					<form ng-submit='submitDeletePayment($event)'>
						<strong>Are you sure to delete this deposit?</strong><br />
						<label><input type="radio" name="delete_payment_confirmation" value="1" ng-model='myPaymentDetail.sure_to_delete' /> Yes</label>
						&nbsp;&nbsp;
						<label><input type="radio" name="delete_payment_confirmation" value="0" ng-model='myPaymentDetail.sure_to_delete' /> No</label>
						<div ng-show="myPaymentDetail.sure_to_delete=='1'">
							<hr style="margin:5px 0" />
							
							<div ng-show='myPaymentDetail.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in myPaymentDetail.error_msg'>{{err}}</li></ul></div>
							
							<strong>Remarks:</strong>
							<textarea rows='2' placeholder="Remarks" class="form-control input-md autoheight" ng-model='myPaymentDetail.delete_remarks' required="required"></textarea>
							<br />
							<button type="submit" class="btn btn-primary">Delete Payment</button>
						</div>
					</form>
				</div>
		  </div>
		  <?php /* 
		  <!-- <div class="modal-footer" style="text-align:center">
			<span ng-show="myPaymentDetail.payment_type_code">
				<a style="cursor: pointer;" href="<?=site_url("home/print_page/#/print/receipt_trans_payment/")?>{{myPaymentDetail.payment_code}}" target="_blank" ng-show="payment.id != ''">
					<button type="button"  class="btn btn-primary">
					<small><span class="glyphicon glyphicon-print"></span>
					 &nbsp;Print Receipt</small>
					</button>
				</a>
			</span> 
			<span ng-show="!myPaymentDetail.payment_type_code">
				<a href="<?=site_url("home/print_page/#/print/receipt_trans/")?>{{DATA.current_booking.booking.booking_code}}" target="_blank">
					<button type="button"  class="btn btn-primary">
					<small><span class="glyphicon glyphicon-print"></span>
					 &nbsp;Print <span ng-show="DATA.current_booking.booking.status_code == 'UNDEFINITE'">Proforma</span> Invoice</small>
					</button>
				</a>
			</span>
			

			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div> --> */ ?>
	  </div>
	</div>
</div>

<!--/ modal detail transaction -->
	<div class="modal fade" id="tranc-detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">
				 
				<span>Transaction</span> Detail 
			</h4>
		  </div>
		  <div class="modal-body">
			<div class="table-responsive">
				<table class="table table-condensed table-bordered">
					<tr class="header bold">
						<td width="120" align="center">Trx. ID#</td>
						<td width="120" align="center">Order# / Invoice#</td>
						<td>Booking Source</td>
						<td width="130">Payment</td>
						<!-- <td width="120" align="center">Status</td> -->
						<td width="120" align="right">Total Amount</td>
					</tr>
					<tbody ng-repeat="transaction in DATA.deposit.transaction.data">
						<tr ng-class="{'danger':(transaction.trx_type == 'REFUND')}">
							<td rowspan="2" align="center">
								{{transaction.trx_code}}
								<hr style="margin:2px" />
								{{fn.newDate(transaction.trx_date) | date:'dd MMM yyyy'}} <small>{{fn.newDate(transaction.trx_date) | date:'HH:mm'}}</small>
							</td>
							<td align="center">
								<a ng-show="transaction.trx_type != 'INVOICE'" ui-sref="trans_reservation.detail({'booking_code':transaction.booking_code})" target="_blank"><strong>{{transaction.booking_code}}</strong></a>
								<a ng-show="transaction.trx_type == 'INVOICE'" ui-sref="invoice.detail({'invoice_code':transaction.booking_code})" target="_blank"><strong>{{transaction.booking_code}}</strong></a>
							</td>
							<td>
                            	<div ng-show="transaction.trx_type == 'REFUND'">REFUND</div>
								<div ng-show="!transaction.agent">
									{{transaction.booking_source}}<br />
									<strong>{{transaction.customer.full_name}}</strong>
								</div>
								<div ng-show="transaction.agent">
									<strong>{{transaction.agent.name}}</strong>
								</div>
							</td>
							<td>
                            	<div ng-show="transaction.trx_type == 'REFUND' && !transaction.payment_type"><span class="label label-warning">PENDING</span></div>
								{{transaction.payment_type}}
							</td>
							<!-- <td align="center">{{transaction.trx_type}}</td> -->
							<td align="right">
								{{transaction.currency}} {{fn.formatNumber(transaction.amount, transaction.currency)}}
							</td>
						</tr>
						<tr ng-class="{'danger':(transaction.trx_type == 'REFUND')}">
							<td colspan="7" style="font-size:11px">
								<em>Remarks : {{transaction.description}}</em>
							</td>
						</tr>
						<tr>
							<td colspan="8" style="background:#FAFAFA"></td>
						</tr>
					</tbody>
				</table>
			</div>
			  	
		  </div>

	  </div>
	</div>
</div>

<script>activate_sub_menu_agent_detail("deposit");</script>
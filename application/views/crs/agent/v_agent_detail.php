<?php //sync:180827 ?>
<div class="sub-title"> Business Source Detail </div> 
<br />
<div ng-init="loadDataAgentDetail();loadDataRealCategory();" class="agent-detail">

	<div ng-show='!(DATA.current_agent)'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>

	<div ng-show='(DATA.current_agent)'>
		<div class="title">
			<h1>{{DATA.current_agent.name}}</h1>
			<div class="code"> Code : {{DATA.current_agent.agent_code}}</div>			
		</div>

		<ul class="nav nav-tabs sub-nav">
			<li role="presentation" class="detail"><a ui-sref="agent.detail({'agent_code':DATA.current_agent.agent_code})">Detail</a></li>
            <li role="presentation" class="contract-rates"><a ui-sref="agent.detail.contract_rates">Rates Category</a></li>
			<li role="presentation" class="reservation"><a ui-sref="agent.detail.reservation">Reservation</a></li>
			<li role="presentation" class="transaction"><a ui-sref="agent.detail.transaction">Transaction</a></li>
			<li role="presentation" class="invoice"><a ui-sref="agent.detail.invoice">Invoices</a></li>
			<li role="presentation" class="email_cc"><a ui-sref="agent.detail.email_cc">Email Setting</a></li>
			<li role="presentation" class="deposit"><a ui-sref="agent.detail.deposit">Deposit</a></li>
			<li role="presentation" class="commission"><a ui-sref="agent.detail.commission">Commission</a></li>
		</ul>
		<br /><br />
		<div ui-view>
			<div class="sub-title">Profile</div>
			<table class="table">
                <tr>
					<td width="150">Name</td>
					<td><strong>{{DATA.current_agent.name}}</strong></td>
				</tr>
				<tr>
					<td>Code</td>
					<td><strong>{{DATA.current_agent.agent_code}}</strong></td>
				</tr>
                <tr>
					<td>Rates Category</td>
					<td>
                    	<ul ng-show='DATA.current_agent.real_categories' style="padding-left:15px; margin-bottom:0">
                           <li ng-repeat="_cat in DATA.current_agent.real_categories"> <a ui-sref="agent.real_category.detail.contract_rates({'real_category_code':_cat.real_category_code})" target="_blank">{{_cat.name}}</a>
                           	<span><a ng-click="delCategory(DATA.current_agent.agent_code , _cat.real_category_code)"><i style="color: red;cursor: pointer;" class="fa fa-close"></i></a></span>
                           </li>

                        </ul>
                        <div style="margin-top: 10px;">
                        	<button class="btn btn-info btn-xs" data-toggle="modal" data-target="#add-category"><span class="glyphicon glyphicon-plus"></span> rates category</button>
                        </div>
                    </td>
				</tr>
                <tr>
					<td>Email</td>
					<td><strong>{{DATA.current_agent.email}}</strong></td>
				</tr>
				<tr>
					<td>Phone</td>
					<td><strong>{{DATA.current_agent.phone}}</strong></td>
				</tr>
				<tr>
					<td>Website</td>
					<td><strong>{{DATA.current_agent.website}}</strong></td>
				</tr>
				<tr>
					<td>Address</td>
					<td><strong>{{DATA.current_agent.address}}</strong></td>
				</tr>
				<tr>
					<td>Country</td>
					<td><strong>{{DATA.current_agent.country.name}}</strong></td>
				</tr>
				<tr>
					<td>PIC</td>
					<td><strong>{{DATA.current_agent.contact_person.name}}</strong></td>
				</tr>
			</table>	
			<br />
			
			<?php /*?><div class="sub-title">Category</div>	
			<table class="table">
				<tr>
					<td width="150">Category</td>
					<td><strong>{{DATA.current_agent.category.name}}</strong></td>
				</tr>
				<tr ng-show="DATA.current_agent.category.description != ''">
					<td>Description</td>
					<td><strong>{{DATA.current_agent.category.description}}</strong></td>
				</tr>
				<tr ng-show="DATA.current_agent.category.percentage">
					<td>Contract Rates</td>
					<td><strong>{{DATA.current_agent.category.percentage}}%</strong></td>
				</tr>
			</table>
			<br /><?php */?>
			
			<div class="sub-title">Agent Payment Type</div>
			<table class="table table-borderlesss">
				<tr>
					<td width="150">Agent Payment Type</td>
					<td><strong>{{DATA.current_agent.payment_method.description}}</strong></td>
				</tr>
				<tr ng-show='DATA.deposit.current_deposit'>
					<td>Deposit Remaining </td>
					<td><strong>{{DATA.deposit.currency}} {{fn.formatNumber(DATA.deposit.current_deposit, DATA.deposit.currency)}}</strong></td>
				</tr>
				<tr ng-show='DATA.uninvoicing.credit_limit'>
					<td>Credit Limit  </td>
					<td><strong>{{DATA.uninvoicing.currency}} {{fn.formatNumber(DATA.uninvoicing.credit_limit, DATA.uninvoicing.currency)}}</strong></td>
				</tr>
				<tr>
					<td>Uninvoicing  </td>
					<td><a ui-sref="ar.proforma_invoice_add_agent({'agent_code':DATA.current_agent.agent_code})" target="_blank"><strong>{{DATA.uninvoicing.currency}} {{fn.formatNumber(DATA.uninvoicing.uninvoicing, DATA.uninvoicing.currency)}}</strong></a></td>
				</tr>
				<tr ng-show='DATA.uninvoicing.credit_limit'>
					<td>Credit Remaining  </td>
					<td><strong>{{DATA.uninvoicing.currency}} {{fn.formatNumber(DATA.uninvoicing.current_limit, DATA.uninvoicing.currency)}}</strong></td>
				</tr>
				<?php /* <tr ng-show='DATA.deposit.current_deposit'>
					<td>Total Deposit  </td>
					<td><strong>{{DATA.deposit.currency}} {{fn.formatNumber(DATA.deposit.deposit, DATA.deposit.currency)}}</strong></td>
				</tr>
				<tr ng-show='DATA.deposit.current_deposit'>
					<td>Used Deposit  </td>
					<td><strong>{{DATA.deposit.currency}} {{fn.formatNumber(DATA.deposit.transaction, DATA.deposit.currency)}}</strong></td>
				</tr>
				*/?>
				
				<!-- <tr ng-show='!DATA.deposit.current_deposit'>
					<strong><em>Loading...</em></strong>
				</tr> -->
			</table>
			<?php /*?><div class="sub-title">Contact Person</div>	
			<table class="table">
				<tr>
					<td width="150">Name</td>
					<td><strong>{{DATA.current_agent.contact_person.name}}</strong></td>
				</tr>
				<tr>
					<td>Phone</td>
					<td><strong>{{DATA.current_agent.contact_person.phone}}</strong></td>
				</tr>
				<tr>
					<td>Email</td>
					<td><strong>{{DATA.current_agent.contact_person.email}}</strong></td>
				</tr>
			</table>
			<br /><?php */?>
			
			<div class="sub-title">Remarks</div>		
			<table class="table">
				<tr>
					<td width="150"></td>
					<td><span ng-show="!DATA.current_agent.remarks">-</span>{{DATA.current_agent.remarks}}</td>
				</tr>
			</table>

			<div class="sub-title">Account Information</div>
			<table class="table">
				<tr>
					<td width="150">Username</td>
					<td><span>
						<b>{{DATA.current_agent.email}}</b>
					</span></td>
				</tr>
				<tr>
					<td width="150">Password</td>
					<td><span>
						******
					</span></td>
				</tr>
				<tr>
					<td width="150"></td>
					<td><span>
						<div class="add-product-button"> <a href="" class="btn btn-success btn-sm" data-toggle="modal" data-target="#checkin-popup-info" ng-click="loadDataAccount()"> <span class="glyphicon glyphicon-letter"></span> Send Account Information </a> </div>
					</span></td>
				</tr>
			</table>
			
			<br /><hr />
			<a ui-sref="agent.edit({'agent_code':DATA.current_agent.agent_code})"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Business Source</a>

			<script>activate_sub_menu_agent_detail("detail");</script>
		</div>
	</div>

	<div class="modal fade" id="checkin-popup-info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel" >
				Agent's Account Information
			</h4>
		  </div>
		  <div class="modal-body">
				<!--  <div ng-hide=''>
					<img src="<?=base_url("public/images/loading_bar.gif")?>" />
				</div>  -->
				<div ng-show='account_error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in account_error_msg'>{{err}}</li></ul></div>
				<form ng-submit="sendEmailAgentAcc(DATA.current_agent.agent_code)">
					<div class="form-group">
						<div class="alert alert-info">
							<p>This login credentials will be sent to your agent.</p><br /> <p>Password can be changed later by your side and a new password notification email will be sent back to the agent.</p>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-2">Email</div>
						<div class="col-md-6">{{DATA.current_agent.email}}</div>
					</div>
					
				
					 <div class="modal-footer" style="text-align:center">
						<button type="submit" class="btn btn-primary">Send</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					  </div>
				</form>
		  		
		  </div>
		</div>
	</div>
	</div>

	<div class="modal fade" id="add-category" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel" >
				Add Agent Category
			</h4>
		  </div>
		  <div class="modal-body">
				<!--  <div ng-hide=''>
					<img src="<?=base_url("public/images/loading_bar.gif")?>" />
				</div>  -->
				<div ng-show='account_error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in account_error_msg'>{{err}}</li></ul></div>
				<form ng-submit="submitAgentCat()">
					<div class="form-group">
						<div class="alert alert-info">
							<p>Please choose the category.</p>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-2">Category</div>
						<div class="col-md-6">
							<select ng-model="DATA.real_category"  class="form-control input-md" style="width:200px">
								<option ng-repeat="cat in DATA.real_categories.real_categories" value="{{cat.real_category_code}}">{{cat.name}}</option>
							</select>
						</div>
					</div>
				
				
					 <div class="modal-footer" style="text-align:center">
						<button type="submit" class="btn btn-primary">Send</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					  </div>
				</form>
		  		
		  </div>
		</div>
	</div>
	</div>

</div>

<style>
	.agent-detail .title{margin-bottom:20px}
	.agent-detail .title h1{margin-bottom:10px !important;}
	.agent-detail .title .code{margin-bottom:5px;}
</style>

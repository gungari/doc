<div class="modal fade" id="edit_bank_account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <form ng-submit='bookingDetailUpdateBankAccount($event)'>
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="myModalLabel">
			Edit Bank Account
		</h4>
	  </div>
	  <div class="modal-body">
      	<div ng-show='!DATA.current_booking.booking.edit_bank_account'>
            <table class="table table-borderless table-condenseds" ng-show='DATA.current_booking.booking.bank_account'>
                <tr>
                    <td colspan="2"><strong>Current Bank Account</strong></td>
                </tr>
                <tr>
                    <td width="130">Bank Name</td>
                    <td><strong>{{DATA.current_booking.booking.bank_account.bank_name}}</strong></td>
                </tr>
                <tr>
                    <td width="130">Account Number</td>
                    <td><strong>{{DATA.current_booking.booking.bank_account.account_number}}</strong></td>
                </tr>
                <tr>
                    <td width="130">Account Name</td>
                    <td><strong>{{DATA.current_booking.booking.bank_account.account_name}}</strong></td>
                </tr>
                <tr>
                    <td width="130">Branch</td>
                    <td><strong>{{DATA.current_booking.booking.bank_account.branch}}</strong></td>
                </tr>
            </table>
            
            <div class="alert alert-danger" ng-show='!DATA.current_booking.booking.bank_account'>
                Bank Account Not Set!
            </div>
            
            <hr />
            
            <div align="center">
                <button type="button" class="btn btn-primary" ng-click='bookingDetailEditBankAccount()'><i class="fa fa-pencil"></i> Change Bank account</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
		</div>

        <div ng-show='DATA.current_booking.booking.edit_bank_account'>
            <div ng-show='DATA.current_booking.booking.edit_bank_account.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.current_booking.booking.edit_bank_account.error_msg'>{{err}}</li></ul></div>
            <table class="table table-borderless table-condenseds">    
                <tr>
                    <td colspan="2"><strong>Change Bank Account</strong></td>
                </tr>
                <tr>
                    <td width="130">Bank Name</td>
                    <td>
                        <select class="form-control input-sm" ng-model="DATA.current_booking.booking.edit_bank_account.id" required="required">
                            <option value="">-- Select Bank Account --</option>
                            <option ng-show="bank.id" ng-repeat="bank in DATA.bank_account" value="{{bank.id}}">{{bank.bank_name+" - "+bank.account_number}}</option>
                        </select>
                    </td>
                </tr>
            </table>
            <hr />
            <div align="center">
            	<button class="btn btn-primary" type="submit">Save</button> 
                 <button class="btn btn-default" type="button" ng-click='DATA.current_booking.booking.edit_bank_account=false'>Cancel</button>
            </div>
	  	</div>
        
      </div>
	</div>
  </div>
  </form>
</div>
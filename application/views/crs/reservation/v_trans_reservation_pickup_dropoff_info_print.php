<div ng-init='pickupDropofTransPrint();'>
	
	<div ng-show='pickup_dropoff'>
		
		<table class="table table-bordered table-condensed" id="data">
			<tr>
				<td colspan="14">
					
					<h4 class="text-capitalize">{{search.type}} - {{fn.formatDate(search.date,"d MM yy")}}</h4>
				</td>

			</tr>
			<tr class="header bold">
				<td width="30">No</td>
				<td style="text-align: center;" width="100">Booking Number</td>
				<td style="text-align: center;" width="100">Voucher#</td>
				<td width="120">Guest Name</td>
				<td style="text-align: center;" colspan="3">Pax(S)</td>
				
				<td width="160">Pick Up Address</td>
				<td width="80">Area</td>
				<td width="120">Dept Date</td>
				<td width="60">Dept Time</td>
				<?php if($vendor['category'] == 'transport'){ ?>
					<td style="text-align: center;" colspan="2">Trip</td>
				<?php }else{ ?>
					<td style="text-align: center;">Product</td>
				<?php } ?>
			</tr>
			<tr style="text-align: center;" class="header bold">
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<td rowspan="1">Adl</td>
				<td rowspan="1">Chi</td>
				<td rowspan="1">Inf</td>
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<?php if($vendor['category'] == 'transport'){ ?>
					<td rowspan="1">Dept</td>
					<td rowspan="1">Arrv</td>
				<?php }else{ ?>
					<td></td>
				<?php } ?>
			</tr>
			<tbody  ng-repeat="pickup in  pickup_dropoff">
			<tr style="background:#FAFAFA">
				<?php if($vendor['category'] == 'transport'){ ?>
				<td colspan="11">
					<strong>{{pickup.departure_port.name}} ({{pickup.departure_time}}) </strong>
					-
					<strong>{{pickup.arrival_port.name}} ({{pickup.arrival_time}}) </strong>
				</td>
				<?php } ?>
			</tr>
			<tr ng-repeat="data in pickup.passenger_list">
				<td style="text-align: center;">{{($index+1)}}</td>
				<td style="text-align: center;">{{data.booking_code}}</td>
				<td style="text-align: center;">
					{{data.voucher_code}}
					
				</td>
				<td>{{data.customer.first_name}} {{data.customer.last_name}}</td>
				<td  style="text-align: center;">
					<span ng-show='data.qty_opt_1>0'>{{data.qty_opt_1}}</span>
					<span ng-show='data.qty_opt_1==0'>-</span>
				</td>
				<td  style="text-align: center;">
					<span ng-show='data.qty_opt_2>0'>{{data.qty_opt_2}}</span>
					<span ng-show='data.qty_opt_2==0'>-</span>
				</td>
				<td  style="text-align: center;">
					<span ng-show='data.qty_opt_3>0'>{{data.qty_opt_3}}</span>
					<span ng-show='data.qty_opt_3==0'>-</span>
				</td>
				<td>{{data.hotel_name}} <br> {{data.hotel_address}}</td>
				<td>{{data.area}}</td>
				<td  style="text-align: center;">{{fn.formatDate(data.date,"d MM yy")}}</td>
				<td>{{data.time}}</td>
				<?php if($vendor['category'] == 'transport'){ ?>
					<td>{{data.departure.port.name}}</td>
					<td>{{data.arrival.port.name}}</td>
				<?php }else{ ?>
					<td>{{pickup.product_name}}</td>
				<?php } ?>
			</tr>

			</tbody>
		</table>
	</div>
	
	<style>
		table tr.not_checkin{color:#666; background:#FAFAFA}
	</style>
	<script>GeneralJS.activateSubMenu(".nav-pills", "li", ".pickup-dropoff");</script>
	<?php /*?>{{dept}}
	<br />
	{{DATA_R}}<?php */?>
</div>
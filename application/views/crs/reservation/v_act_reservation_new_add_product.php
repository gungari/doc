<div class="modal fade" id="modal-add-product" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Select Product</h4>
		  </div>
		  <div class="modal-body products">
          	
          	<div class="alert alert-warning" ng-show="DATA.products.status == 'ERROR'">
            	<strong>Sorry,</strong> product not found...
            </div>
            <div ng-show='!DATA.products.status'>
                <img src="<?=base_url("public/images/loading_bar.gif")?>" />
            </div>
            <div ng-show='DATA.products.products' class="search-trip-result">
            	<div ng-repeat='product in DATA.products.products' ng-show='!add_product.product || product.product_code == add_product.product.product_code'>
                	<div class="trip" ng-click='add_new_product_select_this_product(product)'>
                        <table class="table table-borderless table-condensed" style="margin:0">
                            <tr>
                                <td width="100">
                                    <img ng-src="{{product.main_image}}" width="100%" />
                                </td>
                                <td>
                                    <div>
                                        <h4>{{product.name}}</h4>
                                    </div>
                                    <div>
                                        Code : <strong>{{product.product_code}}</strong>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <hr style="margin:5px 0" />
                </div>
            </div>
            
            <div class="bg-info form-inline" style="padding:10px; border:solid 1px #99cbe4; text-align:center" ng-show='add_product.product'>
            	<form ng-submit='check_availabilities($event)'>
                    <strong>Select Date : </strong>
                    <input tabindex="5" type="text" class="form-control input-sm datepicker" required="required" placeholder="Select Date" ng-model='add_product.date' style="width:150px; text-align:center" />
                    <button type="submit" tabindex="10" class="btn btn-success">Check Availability</button>
            	</form>
            </div>
            <br />
			<div class="text-center" ng-show='check_availabilities_show_loading'>
				<strong><em>Loading...</em></strong>
			</div>
			
            <div ng-show="check_availabilities_data.status == 'ERROR'">
				<div class="alert alert-warning">
					<strong>Sorry</strong>, Product not available, please select another date...
				</div>
			</div>
            
            <div ng-show="check_availabilities_data.status == 'SUCCESS'" class="product search-trip-result">
            	<div class="pull-right">
					<strong>{{fn.formatDate(check_availabilities_data.check.date, "dd MM yy")}}</strong>
				</div>
				<div ng-show='DATA.data_rsv.booking_source == "AGENT" && DATA.data_rsv.agent'>
					Rates for : <strong>{{DATA.data_rsv.agent.name}}</strong>
				</div>
                <div ng-show='DATA.data_rsv.booking_source != "AGENT"'>
					Rates for : <strong>{{DATA.data_rsv.booking_source}} BOOKING</strong>
				</div>
                <hr style="margin:5px 0; clear:both" />
                
				<div class="trip" ng-class="{'selected':availabilities.is_selected}" ng-show='!availabilities.is_hidden' 
                	ng-repeat="availabilities in check_availabilities_data.availabilities"
                	ng-click="check_availabilities_select_this_product(availabilities, check_availabilities_data)"
                	style="border-bottom:solid 1px #eee">
					<div class="media">
					  <div class="media-left">
						<a href="#">
						  <img class="media-object" ng-src="{{add_product.product.main_image}}" width="80px" />
						</a>
					  </div>
					  <div class="media-body">
					  	<?php /*?><p class="pull-right text-right" ng-show='!availabilities.smart_pricing'>
							<small>Adult:</small> <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_1, availabilities.currency)}}</strong><br />
							<small>Child:</small> <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_2, availabilities.currency)}}</strong><br />
							<small>Infant:</small> <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_3, availabilities.currency)}}</strong><br />
						</p><?php */?>
					  	
                      	<strong><span ng-show="availabilities.contract_rates">{{availabilities.contract_rates.name}} - </span>{{availabilities.name}}</strong> <br />
						<span style='color:#5cb85c'>({{availabilities.calendar.allotment}} space left)</span><br />
						<small><i class="fa fa-car" aria-hidden="true"></i> 
							<i class="fa fa-check" aria-hidden="true" ng-show="availabilities.pickup_service == 'yes'"></i> <i class="fa fa-remove" aria-hidden="true" ng-show="availabilities.pickup_service != 'yes'" style="color:red"></i> Pickup 
							&nbsp;
							<i class="fa fa-check" aria-hidden="true" ng-show="availabilities.dropoff_service == 'yes'"></i> <i class="fa fa-remove" aria-hidden="true" ng-show="availabilities.dropoff_service != 'yes'" style="color:red"></i> Dropoff
						</small><br />
                        
                        <?php //NORMAL RATES ?>
                        <div ng-show='!availabilities.is_packages'>
                        	
                        	<table class="table table-condensed table-borderless">
                            	<?php for($i=1;$i<=5;$i++){ ?>
                            	<tr ng-show='availabilities.rates_caption.rates_<?=$i?>'>
                                	<td width="100">{{availabilities.rates_caption.rates_<?=$i?>}}</td>
                                    <td width="10">:</td>
                                    <td width="150"><strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.rates.rates_<?=$i?>, availabilities.currency)}}</strong> <small>/{{availabilities.rates_unit.rates_<?=$i?>}}</small> </td>
                                    <td width="10"  ng-show='availabilities.is_selected'>x</td>
                                    <td width="100" ng-show='availabilities.is_selected' style="padding:0">
                                    	<input <?=(($i==1)?'required="required"':'')?> type="number" step="1" min='0' max="{{availabilities.calendar.allotment}}" 
                                        	class="form-control input-sm" placeholder="Qty." style="height:25px !important" ng-disabled="!availabilities.is_selected"
                                            ng-model='check_availabilities_data.selected_rates.selected_qty_<?=$i?>' />
                                            
										<?php /*?><select <?=(($i==1)?'required="required"':'')?> class="form-control input-sm" placeholder="Qty." 
                                        	style="height:25px !important" ng-disabled="!availabilities.is_selected"
                                            ng-model='check_availabilities_data.selected_rates.selected_qty_<?=$i?>'>
                                            <option value="{{x.qty}}" ng-repeat="x in add_product.product.arr_qty.qty_<?=$i?>">{{x.qty}}</option>
                                        </select><?php */?>
                                    </td>
                                    <td></td>
                                </tr>
                                <?php } ?>
                            </table>
                        </div>
                        
                        <?php //PACKAGES ?>
                        <div ng-show='availabilities.is_packages'>
                        	<table class="table table-condensed table-borderless tbl-packages-popup">
                            	<tbody ng-repeat="packages_option in availabilities.packages_option">
                                	<tr>
                                    	<td colspan="4">
                                        	<div>
                                                <strong>{{packages_option.caption}}</strong>
                                                -
                                                <strong>{{availabilities.currency}} {{fn.formatNumber(packages_option.rates, availabilities.currency)}}</strong> <small>/{{packages_option.rates_unit}}</small>
                                        	</div>
                                            <div>
                                            	Inc. : 	<span ng-show='packages_option.include_pax.opt_1'><strong><span xclass="glyphicon glyphicon-menu-right"></span> {{packages_option.include_pax.opt_1}}</strong> Adult(s) &nbsp;|&nbsp; </span>
														<span ng-show='packages_option.include_pax.opt_2'><strong><span xclass="glyphicon glyphicon-menu-right"></span> {{packages_option.include_pax.opt_2}}</strong> Child(s) &nbsp;|&nbsp; </span>
														<span ng-show='packages_option.include_pax.opt_3'><strong><span xclass="glyphicon glyphicon-menu-right"></span> {{packages_option.include_pax.opt_3}}</strong> Infant(s) &nbsp;|&nbsp; </span>
                                            </div>
                                            <div>
                                            	Additional : <span ng-show='packages_option.extra_rates.opt_1'><strong><span xclass="glyphicon glyphicon-menu-right"></span> {{availabilities.currency}} {{fn.formatNumber(packages_option.extra_rates.opt_1, availabilities.currency)}}</strong>/Adult &nbsp;|&nbsp; </span>
															 <span ng-show='packages_option.extra_rates.opt_2'><strong><span xclass="glyphicon glyphicon-menu-right"></span> {{availabilities.currency}} {{fn.formatNumber(packages_option.extra_rates.opt_2, availabilities.currency)}}</strong>/Child &nbsp;|&nbsp; </span>&nbsp;
															 <span ng-show='packages_option.extra_rates.opt_3'><strong><span xclass="glyphicon glyphicon-menu-right"></span> {{availabilities.currency}} {{fn.formatNumber(packages_option.extra_rates.opt_3, availabilities.currency)}}</strong>/Infant &nbsp;|&nbsp; </span>&nbsp;
                                            </div>
                                        </td>
                                    </tr>
                                    <tr ng-show='availabilities.is_selected'>
                                        <td width="50">Qty.</td>
                                        <td width="10">:</td>
                                        <td ng-show='availabilities.is_selected' style="padding:0">
                                            <input <?=(($i==1)?'required="required"':'')?> type="number" step="1" min='0' max="{{availabilities.calendar.allotment}}" 
                                                class="form-control input-sm" placeholder="Qty." style="height:25px !important; display:inline; width:75px" ng-disabled="!availabilities.is_selected"
                                                ng-model='packages_option.qty' /> {{packages_option.rates_unit}}

												<small>
													|
													Inc. : 	<span ng-show='packages_option.include_pax.opt_1'><strong><span xclass="glyphicon glyphicon-menu-right"></span> {{packages_option.include_pax.opt_1}}</strong> Adult(s) | </span>
															<span ng-show='packages_option.include_pax.opt_2'><strong><span xclass="glyphicon glyphicon-menu-right"></span> {{packages_option.include_pax.opt_2}}</strong> Child(s) | </span>
															<span ng-show='packages_option.include_pax.opt_3'><strong><span xclass="glyphicon glyphicon-menu-right"></span> {{packages_option.include_pax.opt_3}}</strong> Infant(s) | </span>
													
												</small>
                                        </td>
                                        <td></td>
                                	</tr>
                                    <tr ng-show='availabilities.is_selected && packages_option.extra_rates.opt_1'>
                                    	<td colspan="4">
                                        	<strong>Additional</strong>
                                        </td>
                                    </tr>
                                    <tr ng-show='availabilities.is_selected && packages_option.extra_rates.opt_1'>
                                        <td width="50">Adult</td>
                                        <td width="10">:</td>
                                        <td ng-show='availabilities.is_selected' style="padding:0">
                                            <input <?=(($i==1)?'required="required"':'')?> type="number" step="1" min='0' max="{{availabilities.calendar.allotment}}" 
                                                class="form-control input-sm" placeholder="Adult" style="height:25px !important; display:inline; width:75px" 
                                                ng-disabled="!(availabilities.is_selected && packages_option.extra_rates.opt_1 && packages_option.qty)"
                                                ng-model='packages_option.additional_qty_opt_1' /> Pax
												<small>| {{availabilities.currency}} {{fn.formatNumber(packages_option.extra_rates.opt_1, availabilities.currency)}} / Adult</small>
                                        </td>
                                        <td></td>
                                	</tr>
                                    <tr ng-show='availabilities.is_selected && packages_option.extra_rates.opt_2'>
                                        <td width="50">Child</td>
                                        <td width="10">:</td>
                                        <td ng-show='availabilities.is_selected' style="padding:0">
                                            <input <?=(($i==1)?'required="required"':'')?> type="number" step="1" min='0' max="{{availabilities.calendar.allotment}}" 
                                                class="form-control input-sm" placeholder="Child" style="height:25px !important; display:inline; width:75px" 
                                                ng-disabled="!(availabilities.is_selected && packages_option.extra_rates.opt_2 && packages_option.qty)"
                                                ng-model='packages_option.additional_qty_opt_2' /> Pax
												<small>| {{availabilities.currency}} {{fn.formatNumber(packages_option.extra_rates.opt_2, availabilities.currency)}} / Child</small>
                                        </td>
                                        <td></td>
                                	</tr>
                                    <tr ng-show='availabilities.is_selected && packages_option.extra_rates.opt_3'>
                                        <td width="50">Infant</td>
                                        <td width="10">:</td>
                                        <td ng-show='availabilities.is_selected' style="padding:0">
                                            <input <?=(($i==1)?'required="required"':'')?> type="number" step="1" min='0' max="{{availabilities.calendar.allotment}}" 
                                                class="form-control input-sm" placeholder="Infant" style="height:25px !important; display:inline; width:75px" 
                                                ng-disabled="!(availabilities.is_selected && packages_option.extra_rates.opt_3 && packages_option.qty)"
                                                ng-model='packages_option.additional_qty_opt_3' /> Pax
												<small>| {{availabilities.currency}} {{fn.formatNumber(packages_option.extra_rates.opt_3, availabilities.currency)}} / Infant</small>
                                        </td>
                                        <td></td>
                                	</tr>
                                    <tr>
                                    	<td colspan="4"><hr style="margin:0" /></td>
                                    </tr>
                                </tbody>
                            </table>
                            <style>
                            	.tbl-packages-popup tbody, .tbl-packages-popup tbody tr{border:none !important}
                            </style>
                        </div>
                        
					  </div>
					</div>
				</div>
            </div>
            

    		<?php /*?><div class="bg-info" style="padding:10px; border:solid 1px #99cbe4">
				 <form ng-submit='check_availabilities($event)'>
					<div ng-show='add_trip.error_desc.length>0' class="alert alert-danger"><ul><li ng-repeat='err in add_trip.error_desc'>{{err}}</li></ul></div>
					<table class="table table-borderless table-condensed">
						<tr>
							<td colspan="2">
								<label><input tabindex="1" type="radio" name="trip_model" ng-model='add_trip.trip_model' required="required" value="ONEWAY" /> One Way</label> &nbsp;&nbsp;&nbsp;&nbsp;
								<label><input tabindex="1" type="radio" name="trip_model" ng-model='add_trip.trip_model' required="required" value="RETURN" /> Return</label>
							</td>
						</tr>
						<tr>
							<td width="50%">
								<strong>From</strong><br />
								<select tabindex="3" class="form-control input-sm departure-port" ng-model='add_trip.departure_port_id' required="required">
									<option value="">-- Departure Port --</option>
									<option value="{{port.id}}" ng-repeat='port in DATA.available_port.ports'>{{port.port_code}} - {{port.name}}</option>
								</select>
							</td>
							<td>
								<div style="padding:0 5px; width:50%" class="pull-left">
									<strong>Departure</strong><br />
									<input tabindex="5" type="text" class="form-control input-sm datepicker" required="required" placeholder="Departure Date" ng-model='add_trip.departure_date' />
								</div>
								<div style="padding:0 5px; width:50%" class="pull-left" ng-show="add_trip.trip_model=='RETURN'">
									<strong>Return</strong><br />
									<input tabindex="6" type="text" class="form-control input-sm datepicker" ng-required="add_trip.trip_model=='RETURN'" placeholder="Return Date" ng-model='add_trip.return_date' />
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<strong>To</strong><br />
								<select tabindex="3" class="form-control input-sm arrival-port" ng-model='add_trip.arrival_port_id' required="required">
									<option value="">-- Destination Port --</option>
									<option value="{{port.id}}" ng-repeat='port in DATA.available_port.ports'>{{port.port_code}} - {{port.name}}</option>
								</select>
							</td>
							<td>
								<div style="padding:0 5px; width:33.333%" class="pull-left">
									<strong>Adult</strong><br />
									<input tabindex="7" type="number" class="form-control input-sm" placeholder="Adult" required="required" ng-model='add_trip.adult' min="0" />
								</div>
								<div style="padding:0 5px; width:33.333%" class="pull-left">
									<strong>Child</strong><br />
									<input tabindex="8" type="number" class="form-control input-sm" placeholder="Child" required="required" ng-model='add_trip.child' min="0" />
								</div>
								<div style="padding:0 5px; width:33.333%" class="pull-left">
									<strong>Infant</strong><br />
									<input tabindex="9" type="number" class="form-control input-sm" placeholder="Infant" required="required" ng-model='add_trip.infant' min="0" />
								</div>
							</td>
						</tr>
					</table>
					<hr style="margin:5px 0" />
					<div class="text-center">
						<button type="submit" tabindex="10" class="btn btn-success">Check Availability</button>
					</div>
				</form>
			</div><?php */?>
			<?php /*?><div class="product search-trip-result" ng-show='check_availabilities_data.departure.availabilities'>
				<h4 style="margin-top:0">Departure</h4>
				<div class="pull-right">
					<strong>{{fn.formatDate(check_availabilities_data.departure.check.date, "dd M yy")}}</strong>
				</div>
				<div>
					<strong>{{check_availabilities_data.departure.check.departure.port_code}} - {{check_availabilities_data.departure.check.departure.name}}</strong>
					&nbsp; <i class="fa fa-chevron-right"></i> &nbsp;
					<strong>{{check_availabilities_data.departure.check.arrival.port_code}} - {{check_availabilities_data.departure.check.arrival.name}}</strong>
				</div>
                
                
				<hr style="margin:5px 0" />
				<div class="trip" ng-class="{'selected':availabilities.is_selected}" ng-show='!availabilities.is_hidden' ng-repeat="availabilities in check_availabilities_data.departure.availabilities | orderBy : 'departure.time'" ng-click="check_availabilities_select_this_trip(availabilities, check_availabilities_data.departure)">
					<div class="media">
					  <div class="media-left">
						<a href="#">
						  <img class="media-object" ng-src="{{availabilities.schedule.boat.main_image}}" width="80px" />
						</a>
					  </div>
					  <div class="media-body">
					  	<p class="pull-right text-right" ng-show='!availabilities.smart_pricing'>
							<small>Adult:</small> <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_1, availabilities.currency)}}</strong><br />
							<small>Child:</small> <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_2, availabilities.currency)}}</strong><br />
							<small>Infant:</small> <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_3, availabilities.currency)}}</strong><br />
						</p>
					  
					  	<i class="fa fa-clock-o"></i> <strong>{{availabilities.departure.time}} - {{availabilities.arrival.time}}</strong> ({{availabilities.schedule.boat.available_space}} space left)<br />
						<small><i class="fa fa-ship"></i> 	<strong>{{availabilities.schedule.boat.name}} ({{availabilities.schedule.boat.capacity}} seats)</strong></small><br />
						<small><i class="fa fa-dollar"></i> &nbsp; {{availabilities.name}}</small><br />
						<small><i class="fa fa-car" aria-hidden="true"></i> 
							<i class="fa fa-check" aria-hidden="true" ng-show="availabilities.pickup_service == 'yes'"></i> <i class="fa fa-remove" aria-hidden="true" ng-show="availabilities.pickup_service != 'yes'" style="color:red"></i> Pickup 
							&nbsp;
							<i class="fa fa-check" aria-hidden="true" ng-show="availabilities.dropoff_service == 'yes'"></i> <i class="fa fa-remove" aria-hidden="true" ng-show="availabilities.dropoff_service != 'yes'" style="color:red"></i> Dropoff
						</small><br />
						<div ng-show='availabilities.smart_pricing'>
							<div ng-repeat='pricing_level in availabilities.smart_pricing.applicable_rates'>
								<hr style="margin:5px 0" />
								<div class="pull-right">{{pricing_level.allotment}} <small>pax</small> <span class="glyphicon glyphicon-ok" ng-show='pricing_level.allotment > 0' style="color:green"></span></div>
								{{($index+1)}}.
								<small>Adult:</small> <strong>{{pricing_level.currency}} {{fn.formatNumber(pricing_level.rates.rates_1, pricing_level.currency)}}</strong>
								<small>Child:</small> <strong>{{pricing_level.currency}} {{fn.formatNumber(pricing_level.rates.rates_2, pricing_level.currency)}}</strong>
								<small>Infant:</small> <strong>{{pricing_level.currency}} {{fn.formatNumber(pricing_level.rates.rates_3, pricing_level.currency)}}</strong>
								<div class="clearfix"></div>
							</div>
							<hr style="margin:5px 0" /><br />
						</div>
					  </div>
					</div>
				</div>
			</div>
			
			<div class="product search-trip-result" ng-show='check_availabilities_data.return.availabilities'>
				<h4 style="margin-top:0">Return</h4>
				<div class="pull-right">
					<strong>{{fn.formatDate(check_availabilities_data.return.check.date, "dd M yy")}}</strong>
				</div>
				<div>
					<strong>{{check_availabilities_data.return.check.departure.port_code}} - {{check_availabilities_data.return.check.departure.name}}</strong>
					&nbsp; <i class="fa fa-chevron-right"></i> &nbsp;
					<strong>{{check_availabilities_data.return.check.arrival.port_code}} - {{check_availabilities_data.return.check.arrival.name}}</strong>
				</div>
				<hr style="margin:5px 0" />
				<div class="trip" ng-class="{'selected':availabilities.is_selected}" ng-show='!availabilities.is_hidden' ng-repeat="availabilities in check_availabilities_data.return.availabilities | orderBy : 'departure.time'" ng-click="check_availabilities_select_this_trip(availabilities, check_availabilities_data.return)">
					<div class="media">
					  <div class="media-left">
						<a href="#">
						  <img class="media-object" ng-src="{{availabilities.schedule.boat.main_image}}" width="80px" />
						</a>
					  </div>
					  <div class="media-body">
					  	<p class="pull-right text-right" ng-show='!availabilities.smart_pricing'>
							<small>Adult:</small> <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_1, availabilities.currency)}}</strong><br />
							<small>Child:</small> <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_2, availabilities.currency)}}</strong><br />
							<small>Infant:</small> <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_3, availabilities.currency)}}</strong><br />
						</p>
					  
					  	<i class="fa fa-clock-o"></i> <strong>{{availabilities.departure.time}} - {{availabilities.arrival.time}}</strong> ({{availabilities.schedule.boat.available_space}} space left)<br />
						<small><i class="fa fa-ship"></i> 	<strong>{{availabilities.schedule.boat.name}} ({{availabilities.schedule.boat.capacity}} seats)</strong></small><br />
						<small><i class="fa fa-dollar"></i> &nbsp; {{availabilities.name}}</small><br />
						<small><i class="fa fa-car" aria-hidden="true"></i> 
							<i class="fa fa-check" aria-hidden="true" ng-show="availabilities.pickup_service == 'yes'"></i> <i class="fa fa-remove" aria-hidden="true" ng-show="availabilities.pickup_service != 'yes'" style="color:red"></i> Pickup 
							&nbsp;
							<i class="fa fa-check" aria-hidden="true" ng-show="availabilities.dropoff_service == 'yes'"></i> <i class="fa fa-remove" aria-hidden="true" ng-show="availabilities.dropoff_service != 'yes'" style="color:red"></i> Dropoff
						</small><br />
						<div ng-show='availabilities.smart_pricing'>
							<div ng-repeat='pricing_level in availabilities.smart_pricing.applicable_rates'>
								<hr style="margin:5px 0" />
								<div class="pull-right">{{pricing_level.allotment}} <small>pax</small> <span class="glyphicon glyphicon-ok" ng-show='pricing_level.allotment > 0' style="color:green"></span></div>
								{{($index+1)}}.
								<small>Adult:</small> <strong>{{pricing_level.currency}} {{fn.formatNumber(pricing_level.rates.rates_1, pricing_level.currency)}}</strong>
								<small>Child:</small> <strong>{{pricing_level.currency}} {{fn.formatNumber(pricing_level.rates.rates_2, pricing_level.currency)}}</strong>
								<small>Infant:</small> <strong>{{pricing_level.currency}} {{fn.formatNumber(pricing_level.rates.rates_3, pricing_level.currency)}}</strong>
								<div class="clearfix"></div>
							</div>
							<hr style="margin:5px 0" /><br />
						</div>
					  </div>
					</div>
				</div>
                
			</div><?php */?>
		  
          <div class="modal-footer" style="text-align:center">
			<button tabindex="100" type="button" class="btn btn-primary" ng-show='check_availabilities_show_OK_button' ng-click='check_availabilities_add_to_data_rsv()'>OK</button>
			<button tabindex="101" type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
	  </div>
</div>
<script>//$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});</script>
<style>
.search-trip-result .trip{padding:5px}
.search-trip-result .trip:hover{background:#d9edf7; cursor:pointer}
.search-trip-result .trip.selected{background:#a3cce0}
</style>
<div class="modal fade" id="void-booking-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <form ng-submit='saveVoidBooking($event)'>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    Void Booking #{{DATA.current_booking.booking.booking_code}}
                </h4> 
              </div>
              <div class="modal-body">
                <div ng-show='DATA.void.error_desc.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.void.error_desc'>{{err}}</li></ul></div>
                
                <div ng-show="DATA.current_booking.booking.can_be_void != '1'" class="alert-danger">
                    <div style="padding: 5px;">
                        <em>Void instruction only valid for 24 hours after transaction is made. Reservations has made will cancelled and refund will requested.</em>
                    </div>
                </div>
                <div ng-show="DATA.current_booking.booking.can_be_void == '1'">
                    <table class="table table-borderless table-condensed">
                        <tr>
                            <td></td>
                            <td>
                                <h4><input style="display:none" type="checkbox" ng-model='DATA.void.void_all_trip' ng-true-value='1' ng-false-value='0' ng-click='chkVoidAllBookingClick()' /> Void All Trips</h4>
                            </td>
                        </tr>
                        <tr>
                            <td>Trips</td>
                            <td>
                                <hr style="margin:5px 0" />
                                <div ng-repeat='detail in DATA.current_booking.booking.detail' ng-show="detail.booking_detail_status_code != 'VOID'">
                                    <div ng-class="{'alert alert-info':DATA.void.voucher[detail.voucher_code].is_void}" style="margin:0 !important">
                                        <h4><input style="display:none" type="checkbox" ng-model='DATA.void.voucher[detail.voucher_code].is_void' ng-true-value='1' ng-false-value='0' ng-click='chkVoidAllBookingClick(true)' /> {{detail.voucher_code}}</h4>
                                        <div ng-show="detail.product_type == 'ACT'">
                                        	<strong>{{detail.product.name}}</strong>
                                        </div>
                                        <div ng-show="detail.product_type == 'TRANS'">
                                            <strong>{{detail.departure.port.name}} ({{detail.departure.port.port_code}}) : {{detail.departure.time}}</strong>
                                            &nbsp;&nbsp;<i class="fa fa-chevron-right"></i>&nbsp;&nbsp;
                                            <strong>{{detail.arrival.port.name}} ({{detail.arrival.port.port_code}}) : {{detail.arrival.time}}</strong>
                                        </div>
                                        <div><small>({{detail.rates.name}})</small></div>
                                        <div><strong>{{fn.formatDate(detail.date, "dd MM yy")}}</strong></div>
                                        <div>
                                            <span ng-show='detail.qty_1 > 0'>{{detail.qty_1}} Adult</span>
                                            <span ng-show='detail.qty_2 > 0'>{{detail.qty_2}} Child</span>
                                            <span ng-show='detail.qty_3 > 0'>{{detail.qty_3}} Infant</span>
                                        </div>
                                    </div>
                                    <hr style="margin:5px 0" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td width="130">Void Reason</td>
                            <td>
                                <textarea placeholder="Description" required="required" type="text" class="form-control input-md autoheight" ng-model='DATA.void.void_reason' rows="3"></textarea>
                            </td>
                        </tr>
                    </table>
                    <div class="modal-footer" style="text-align:center">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                      </div>
                  </div>
                  
                </div>
            </div>
        </div>
    </form>
</div>
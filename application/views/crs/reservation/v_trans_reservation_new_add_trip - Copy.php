<div class="modal fade" id="modal-add-trip" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Check Availability</h4>
		  </div>
		  <div class="modal-body products">
    
    		<div class="bg-info" style="padding:10px; border:solid 1px #99cbe4">
				 <form ng-submit='check_availabilities($event)'>
					<div ng-show='add_trip.error_desc.length>0' class="alert alert-danger"><ul><li ng-repeat='err in add_trip.error_desc'>{{err}}</li></ul></div>
					<table class="table table-borderless table-condensed">
						<tr>
							<td colspan="2">
								<label><input tabindex="1" type="radio" name="trip_model" ng-model='add_trip.trip_model' required="required" value="ONEWAY" /> One Way</label> &nbsp;&nbsp;&nbsp;&nbsp;
								<label><input tabindex="1" type="radio" name="trip_model" ng-model='add_trip.trip_model' required="required" value="RETURN" /> Return</label>
							</td>
						</tr>
						<tr>
							<td width="50%">
								<strong>From</strong><br />
								<select tabindex="3" class="form-control input-sm departure-port" ng-model='add_trip.departure_port_id' required="required">
									<option value="">-- Departure Port --</option>
									<option value="{{port.id}}" ng-repeat='port in DATA.available_port.ports'>{{port.port_code}} - {{port.name}}</option>
								</select>
							</td>
							<td>
								<div style="padding:0 5px; width:50%" class="pull-left">
									<strong>Departure</strong><br />
									<input tabindex="5" type="text" class="form-control input-sm datepicker" required="required" placeholder="Departure Date" ng-model='add_trip.departure_date' />
								</div>
								<div style="padding:0 5px; width:50%" class="pull-left" ng-show="add_trip.trip_model=='RETURN'">
									<strong>Return</strong><br />
									<input tabindex="6" type="text" class="form-control input-sm datepicker" ng-required="add_trip.trip_model=='RETURN'" placeholder="Return Date" ng-model='add_trip.return_date' />
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<strong>To</strong><br />
								<select tabindex="3" class="form-control input-sm arrival-port" ng-model='add_trip.arrival_port_id' required="required">
									<option value="">-- Destination Port --</option>
									<option value="{{port.id}}" ng-repeat='port in DATA.available_port.ports'>{{port.port_code}} - {{port.name}}</option>
								</select>
							</td>
							<td>
								<div style="padding:0 5px; width:33.333%" class="pull-left">
									<strong>Adult</strong><br />
									<input tabindex="7" type="number" class="form-control input-sm" placeholder="Adult" required="required" ng-model='add_trip.adult' min="0" />
								</div>
								<div style="padding:0 5px; width:33.333%" class="pull-left">
									<strong>Child</strong><br />
									<input tabindex="8" type="number" class="form-control input-sm" placeholder="Child" required="required" ng-model='add_trip.child' min="0" />
								</div>
								<div style="padding:0 5px; width:33.333%" class="pull-left">
									<strong>Infant</strong><br />
									<input tabindex="9" type="number" class="form-control input-sm" placeholder="Infant" required="required" ng-model='add_trip.infant' min="0" />
								</div>
							</td>
						</tr>
					</table>
					<hr style="margin:5px 0" />
					<div class="text-center">
						<button type="submit" tabindex="10" class="btn btn-success">Check Availability</button>
					</div>
				</form>
			</div>
			<br />
			<div class="text-center" ng-show='check_availabilities_show_loading'>
				<strong><em>Loading...</em></strong>
			</div>
			
			<div ng-show='check_availabilities_data.departure && !check_availabilities_data.departure.availabilities'>
				<div class="alert alert-warning">
					<strong>Sorry</strong>, Trip not available, please select another date...
				</div>
			</div>
			
			<div class="product search-trip-result" ng-show='check_availabilities_data.departure.availabilities'>
				<h4 style="margin-top:0">Departure</h4>
				<div ng-show="DATA.data_rsv.booking_source=='AGENT'" style="font-size: 14px">
					<strong>Rates</strong> for ({{DATA.data_rsv.agent.name}})
				</div>
				<br />
				<div class="pull-right">
					<strong>{{fn.formatDate(check_availabilities_data.departure.check.date, "dd M yy")}}</strong>
				</div>
				<div>
					<strong>{{check_availabilities_data.departure.check.departure.port_code}} - {{check_availabilities_data.departure.check.departure.name}}</strong>
					&nbsp; <i class="fa fa-chevron-right"></i> &nbsp;
					<strong>{{check_availabilities_data.departure.check.arrival.port_code}} - {{check_availabilities_data.departure.check.arrival.name}}</strong>
				</div>
				<hr style="margin:5px 0" />
				<div class="trip" ng-class="{'selected':availabilities.is_selected}" ng-show='!availabilities.is_hidden' ng-repeat="availabilities in check_availabilities_data.departure.availabilities | orderBy : 'departure.time'" ng-click="check_availabilities_select_this_trip(availabilities, check_availabilities_data.departure)">
					<div class="media">
					  <div class="media-left">
						<a href="#">
						  <img class="media-object" ng-src="{{availabilities.schedule.boat.main_image}}" width="80px" />
						</a>
					  </div>
					  <div class="media-body">
					  	<p class="pull-right text-right" ng-show='!availabilities.smart_pricing'>
							<small>Adult:</small> <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_1, availabilities.currency)}}</strong><br />
							<small>Child:</small> <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_2, availabilities.currency)}}</strong><br />
							<small>Infant:</small> <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_3, availabilities.currency)}}</strong><br />
						</p>
					  
					  	<i class="fa fa-clock-o"></i> <strong>{{availabilities.departure.time}} - {{availabilities.arrival.time}}</strong> ({{availabilities.schedule.boat.available_space}} space left)<br />
						<small><i class="fa fa-ship"></i> 	<strong>{{availabilities.schedule.boat.name}} ({{availabilities.schedule.boat.capacity}} seats)</strong></small><br />
						<small><i class="fa fa-dollar"></i> &nbsp; {{availabilities.name}}</small><br />
						<small><i class="fa fa-car" aria-hidden="true"></i> 
							<i class="fa fa-check" aria-hidden="true" ng-show="availabilities.pickup_service == 'yes'"></i> <i class="fa fa-remove" aria-hidden="true" ng-show="availabilities.pickup_service != 'yes'" style="color:red"></i> Pickup 
							&nbsp;
							<i class="fa fa-check" aria-hidden="true" ng-show="availabilities.dropoff_service == 'yes'"></i> <i class="fa fa-remove" aria-hidden="true" ng-show="availabilities.dropoff_service != 'yes'" style="color:red"></i> Dropoff
						</small><br />
						<div ng-show='availabilities.smart_pricing'>
							<div ng-repeat='pricing_level in availabilities.smart_pricing.applicable_rates'>
								<hr style="margin:5px 0" />
								<div class="pull-right">{{pricing_level.allotment}} <small>pax</small> <span class="glyphicon glyphicon-ok" ng-show='pricing_level.allotment > 0' style="color:green"></span></div>
								{{($index+1)}}.
								<small>Adult:</small> <strong>{{pricing_level.currency}} {{fn.formatNumber(pricing_level.rates.rates_1, pricing_level.currency)}}</strong>
								<small>Child:</small> <strong>{{pricing_level.currency}} {{fn.formatNumber(pricing_level.rates.rates_2, pricing_level.currency)}}</strong>
								<small>Infant:</small> <strong>{{pricing_level.currency}} {{fn.formatNumber(pricing_level.rates.rates_3, pricing_level.currency)}}</strong>
								<div class="clearfix"></div>
							</div>
							<hr style="margin:5px 0" /><br />
						</div>
					  </div>
					</div>
				</div>
			</div>
			
			<div class="product search-trip-result" ng-show='check_availabilities_data.return.availabilities'>
				<h4 style="margin-top:0">Return</h4>
				<div class="pull-right">
					<strong>{{fn.formatDate(check_availabilities_data.return.check.date, "dd M yy")}}</strong>
				</div>
				<div>
					<strong>{{check_availabilities_data.return.check.departure.port_code}} - {{check_availabilities_data.return.check.departure.name}}</strong>
					&nbsp; <i class="fa fa-chevron-right"></i> &nbsp;
					<strong>{{check_availabilities_data.return.check.arrival.port_code}} - {{check_availabilities_data.return.check.arrival.name}}</strong>
				</div>
				<hr style="margin:5px 0" />
				<div class="trip" ng-class="{'selected':availabilities.is_selected}" 
					xxng-show='!availabilities.is_hidden' 
                    ng-hide='availabilities.is_hidden || (check_availabilities_data.departure.selected_trip && check_availabilities_data.departure.selected_trip.aplicable_rates.rates_1 != availabilities.aplicable_rates.rates_1)'
					ng-repeat="availabilities in check_availabilities_data.return.availabilities | orderBy : 'departure.time'" 
					ng-click="check_availabilities_select_this_trip(availabilities, check_availabilities_data.return)">
					<div class="media">
					  <div class="media-left">
						<a href="#">
						  <img class="media-object" ng-src="{{availabilities.schedule.boat.main_image}}" width="80px" />
						</a>
					  </div>
					  <div class="media-body">
					  	<p class="pull-right text-right" ng-show='!availabilities.smart_pricing'>
							<small>Adult:</small> <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_1, availabilities.currency)}}</strong><br />
							<small>Child:</small> <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_2, availabilities.currency)}}</strong><br />
							<small>Infant:</small> <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_3, availabilities.currency)}}</strong><br />
						</p>
					  
					  	<i class="fa fa-clock-o"></i> <strong>{{availabilities.departure.time}} - {{availabilities.arrival.time}}</strong> ({{availabilities.schedule.boat.available_space}} space left)<br />
						<small><i class="fa fa-ship"></i> 	<strong>{{availabilities.schedule.boat.name}} ({{availabilities.schedule.boat.capacity}} seats)</strong></small><br />
						<small><i class="fa fa-dollar"></i> &nbsp; {{availabilities.name}}</small><br />
						<small><i class="fa fa-car" aria-hidden="true"></i> 
							<i class="fa fa-check" aria-hidden="true" ng-show="availabilities.pickup_service == 'yes'"></i> <i class="fa fa-remove" aria-hidden="true" ng-show="availabilities.pickup_service != 'yes'" style="color:red"></i> Pickup 
							&nbsp;
							<i class="fa fa-check" aria-hidden="true" ng-show="availabilities.dropoff_service == 'yes'"></i> <i class="fa fa-remove" aria-hidden="true" ng-show="availabilities.dropoff_service != 'yes'" style="color:red"></i> Dropoff
						</small><br />
						<div ng-show='availabilities.smart_pricing'>
							<div ng-repeat='pricing_level in availabilities.smart_pricing.applicable_rates'>
								<hr style="margin:5px 0" />
								<div class="pull-right">{{pricing_level.allotment}} <small>pax</small> <span class="glyphicon glyphicon-ok" ng-show='pricing_level.allotment > 0' style="color:green"></span></div>
								{{($index+1)}}.
								<small>Adult:</small> <strong>{{pricing_level.currency}} {{fn.formatNumber(pricing_level.rates.rates_1, pricing_level.currency)}}</strong>
								<small>Child:</small> <strong>{{pricing_level.currency}} {{fn.formatNumber(pricing_level.rates.rates_2, pricing_level.currency)}}</strong>
								<small>Infant:</small> <strong>{{pricing_level.currency}} {{fn.formatNumber(pricing_level.rates.rates_3, pricing_level.currency)}}</strong>
								<div class="clearfix"></div>
							</div>
							<hr style="margin:5px 0" /><br />
						</div>
					  </div>
					</div>
				</div>
                
			</div>
		  <div class="modal-footer" style="text-align:center">
			<button tabindex="100" type="button" class="btn btn-primary" ng-show='check_availabilities_show_OK_button' ng-click='check_availabilities_add_to_data_rsv()'>OK</button>
			<button tabindex="101" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
	  </div>
</div>
<style>
.search-trip-result .trip{padding:5px}
.search-trip-result .trip:hover{background:#d9edf7; cursor:pointer}
.search-trip-result .trip.selected{background:#a3cce0}
</style>
	
<div class="sub-title"> Reservation Detail </div> 
<br />
<div ng-init="loadDataBookingTransportDetail();loadDataTermsAndConditions();" class="reservation-detail">

	<div ng-show='!(DATA.current_booking)'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	
	<div ng-show='(DATA.current_booking)'>
		<div class="pull-right">
			<div class="btn-group">
			  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fa fa-print" aria-hidden="true"></i> Print <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu">
				<li ng-show="DATA.current_booking.booking.status_code != 'UNDEFINITE'"><a href="<?=site_url("home/print_page/#/print/receipt_trans/")?>{{DATA.current_booking.booking.booking_code}}" target="_blank">Invoice #{{DATA.current_booking.booking.booking_code}}</a></li>
                <li ng-show="DATA.current_booking.booking.status_code == 'UNDEFINITE'"><a href="<?=site_url("home/print_page/#/print/receipt_trans/")?>{{DATA.current_booking.booking.booking_code}}" target="_blank">Proforma Invoice #{{DATA.current_booking.booking.booking_code}}</a></li>
				<li role="separator" class="divider"></li>

				<li ng-repeat='detail in DATA.current_booking.booking.detail'>
					<a href="<?=site_url("home/print_page/#/print/voucher_trans/")?>{{DATA.current_booking.booking.booking_code}}/{{detail.voucher_code}}" target="_blank">Voucher {{detail.voucher_code}}</a>
				</li>
			  </ul>
			</div>
			<div class="btn-group">
			  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fa fa-bars" aria-hidden="true"></i> Menu <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu">
				<li ng-show="DATA.current_booking.booking.status_code != 'CANCEL'">
					<a href="" data-toggle="modal" data-target="#cancel-booking-form" ng-click="cancelBooking(DATA.current_booking.booking)">
						<i class="fa fa-remove" aria-hidden="true"></i> Cancel Booking
					</a>
				</li>
                <li ng-show="DATA.current_booking.booking.status_code != 'VOID'">
					<a href="" data-toggle="modal" data-target="#void-booking-form" ng-click="voidBooking(DATA.current_booking.booking)">
						<i class="fa fa-minus-circle" aria-hidden="true"></i> Void Booking
					</a>
				</li>
				<li ng-show='DATA.current_booking.booking.total_payment!=0'>
					<a href="" data-toggle="modal" data-target="#refund-booking-form" ng-click="refundBooking(DATA.current_booking.booking)">
						<i class="fa fa-reply" aria-hidden="true"></i> Refund
					</a>
				</li>
				<li>
					<a ui-sref="trans_reservation.edit({'booking_code':DATA.current_booking.booking.booking_code})">
						<i class="fa fa-pencil" aria-hidden="true"></i> Edit Booking
					</a>
				</li>
			  </ul>
			</div>
		</div>
		<div class="title">
			<h1>#{{DATA.current_booking.booking.booking_code}} - {{DATA.current_booking.booking.customer.first_name}} {{DATA.current_booking.booking.customer.last_name}}</h1>
			<?php /*?><div class="code"> Date : {{fn.formatDate(DATA.current_booking.booking.transaction_date, "dd M yy")}}</div><?php */?>
		</div>

		<ul class="nav nav-tabs sub-nav">
			<li role="presentation" class="detail"><a ui-sref="act_reservation.detail({'booking_code':DATA.current_booking.booking.booking_code})">Detail</a></li>
			<li role="presentation" class="passenger"><a ui-sref="act_reservation.detail.passenger">Passenger</a></li>
			<li role="presentation" class="payment"><a ui-sref="act_reservation.detail.payment">Billing Statement</a></li>
			<?php /*?><li role="presentation" class="contract-rates"><a ui-sref="agent.detail.contract_rates">Contract Rates</a></li><?php */?>
		</ul>
		<br />
		<div ui-view>
			<div class="sub-title"> Booking Information </div>
			<table class="table">
				<tr>
					<td width="130">Order</td>
					<td><strong>#{{DATA.current_booking.booking.booking_code}}</strong></td>
				</tr>
				<tr>
					<td>Booking Date</td>
					<td><strong>{{fn.newDate(DATA.current_booking.booking.transaction_date) | date : 'dd MMMM yyyy HH:mm'}}</strong></td>
					<?php /*?><td><strong>{{fn.formatDate(DATA.current_booking.booking.transaction_date, "dd MM yy")}}</strong></td><?php */?>
				</tr>
				<tr>
					<td>Booking Status</td>
					<td class="text-capitalize">
						<strong>
                            <span ng-class="{'label label-danger':DATA.current_booking.booking.status_code == 'CANCEL', 'label label-warning':DATA.current_booking.booking.status_code == 'UNDEFINITE', 'label label-default':DATA.current_booking.booking.status_code == 'VOID'}">
                                {{DATA.current_booking.booking.status.toLowerCase()}}
                            </span>
                        </strong>
					</td>
				</tr>
                <tr ng-show='DATA.current_booking.booking.undefinite_cut_off_date'>
                	<td>Valid Until</td>
                    <td><strong>{{fn.newDate(DATA.current_booking.booking.undefinite_cut_off_date) | date : 'dd MMMM yyyy HH:mm'}}</strong></td>
                </tr>
				<tr ng-show='DATA.current_booking.booking.cancelation'>
					<td>Cancelation Date</td>
					<td><strong>{{fn.formatDate(DATA.current_booking.booking.cancelation.date, "dd MM yy")}}</strong></td>
				</tr>
				<tr ng-show='DATA.current_booking.booking.cancelation'>
					<td>Cancelation Reason</td>
					<td><strong>{{DATA.current_booking.booking.cancelation.cancelation_reason}}</strong></td>
				</tr>
                <tr ng-show='DATA.current_booking.booking.void'>
					<td>Void Date</td>
					<td><strong>{{fn.formatDate(DATA.current_booking.booking.void.date, "dd MM yy")}}</strong></td>
				</tr>
				<tr ng-show='DATA.current_booking.booking.void'>
					<td>Void Reason</td>
					<td><strong>{{DATA.current_booking.booking.void.void_reason}}</strong></td>
				</tr>
				<tr>
					<td>Booking Source</td>
					<td class="text-capitalize"><strong>{{DATA.current_booking.booking.source.toLowerCase()}}</strong></td>
				</tr>
				<tr ng-show="DATA.current_booking.booking.agent">
					<td>Agent</td>
					<td class="text-capitalize"><strong>{{DATA.current_booking.booking.agent.name}}</strong></td>
				</tr>
                <tr ng-show="DATA.current_booking.booking.agent && DATA.current_booking.booking.voucher_reff_number != ''">
					<td>Voucher# Reff.</td>
					<td class="text-capitalize"><strong>{{DATA.current_booking.booking.voucher_reff_number}}</strong></td>
				</tr>
			</table>	
			<br />
			<div class="sub-title"> Customer Information </div>
			<table class="table">
				<tr>
					<td width="130">Full Name</td>
					<td><strong>{{DATA.current_booking.booking.customer.first_name}} {{DATA.current_booking.booking.customer.last_name}}</strong></td>
				</tr>
				<tr>
					<td>Email</td>
					<td><strong>{{DATA.current_booking.booking.customer.email}}</strong></td>
				</tr>
				<tr>
					<td>Country</td>
					<td><strong>{{DATA.current_booking.booking.customer.country_name}}</strong></td>
				</tr>
				<tr>
					<td>Telephone</td>
					<td><strong>{{DATA.current_booking.booking.customer.phone}}</strong></td>
				</tr>
			</table>
			<br />
			
			<div class="sub-title">Remarks / Special Request</div>
			<table class="table">
				<tr>
					<td width="130">Remarks</td>
					<td>
						<span ng-show="!DATA.current_booking.booking.remarks">-</span>
						<div ng-show="DATA.current_booking.booking.remarks" class="">
							<strong>{{DATA.current_booking.booking.remarks}}</strong>
						</div>
					</td>
				</tr>
			</table>
			<br />
			
			<div class="sub-title"> Booking Details </div>
			<table class="table table-bordered">
				<tr class="info">
					<td><strong>Description</strong></td>
					<td width="150" align="right"><strong>Sub Total</strong></td>
				</tr>
				<tbody ng-repeat="detail in DATA.current_booking.booking.detail | orderBy : '-booking_detail_status_code'">
					<tr ng-class="{'danger':detail.booking_detail_status_code == 'CANCEL'}">
						<td>
							<div class="pull-right text-right">
								Voucher#
								<div style="font-size:20px">
									<a href="<?=site_url("home/print_page/#/print/voucher_trans/")?>{{DATA.current_booking.booking.booking_code}}/{{detail.voucher_code}}" target="_blank">{{detail.voucher_code}}</a>
								</div>
							</div>
							<div>
                            	<strong>{{detail.product.name}}</strong>
								<?php /*?><strong>{{detail.departure.port.name}} ({{detail.departure.port.port_code}}) : {{detail.departure.time}}</strong>
								&nbsp;&nbsp;<i class="fa fa-chevron-right"></i>&nbsp;&nbsp;
								<strong>{{detail.arrival.port.name}} ({{detail.arrival.port.port_code}}) : {{detail.arrival.time}}</strong><?php */?>
							</div>
							<div><small>({{detail.rates.name}})</small></div>
							<div><strong>{{fn.newDate(detail.date) | date : 'dd MMMM yyyy'}}</strong></div>
							<?php /*?><div><strong>{{fn.formatDate(detail.date, "dd MM yy")}}</strong></div><?php */?>
							<div>
								<span ng-show='detail.qty_1 > 0'>{{detail.qty_1}} Adult</span>
								<span ng-show='detail.qty_2 > 0'>{{detail.qty_2}} Child</span>
								<span ng-show='detail.qty_3 > 0'>{{detail.qty_3}} Infant</span>
							</div>
							<div ng-show="detail.booking_detail_status_code == 'CANCEL'">
								<span class="label label-danger">{{detail.booking_detail_status}}</span>
							</div>
                            <div ng-show="detail.booking_detail_status_code == 'VOID'">
								<span class="label label-default">{{detail.booking_detail_status}}</span>
							</div>
						</td>
						<td align="right">
							<strong ng-show='detail.subtotal == detail.subtotal_before_cancel'>
								{{detail.rates.currency}}
								{{fn.formatNumber((detail.rates.rates_1 * detail.qty_1) + (detail.rates.rates_2 * detail.qty_2) + (detail.rates.rates_3 * detail.qty_3), detail.rates.currency)}}
							</strong>
                            <strong ng-show='detail.subtotal != detail.subtotal_before_cancel'>
                            	{{detail.rates.currency}}
								{{fn.formatNumber(detail.subtotal, detail.rates.currency)}}
                            </strong>
						</td>
					</tr>
					<tr ng-show='detail.pickup' ng-class="{'danger':detail.booking_detail_status_code == 'CANCEL'}">
						<td>
							<a href="" onclick="$(this).parent('td').find('table').toggle()">Pickup Service ({{detail.pickup.area}} - {{detail.pickup.time}})</a>
							<table class="table table-borderless table-condensed hidden-field" style="background:none">
								<tr>
									<td width="130">Hotel Name</td>
									<td><strong>{{detail.pickup.hotel_name}}</strong></td>
								</tr>
								<tr>
									<td>Hotel Addres</td>
									<td><strong>{{detail.pickup.hotel_address}}</strong></td>
								</tr>
								<tr>
									<td>Hotel Phone Number</td>
									<td><strong>{{detail.pickup.hotel_phone_number}}</strong></td>
								</tr>
							</table>
						</td>
						<td align="right">
							<strong ng-show='detail.pickup.price > 0 && detail.subtotal == detail.subtotal_before_cancel'>
								{{detail.rates.currency}}
								{{fn.formatNumber(detail.pickup.price,detail.rates.currency)}}
							</strong>
						</td>
					</tr>
					<tr ng-show='detail.dropoff' ng-class="{'danger':detail.booking_detail_status_code == 'CANCEL'}">
						<td>
							<a href="" onclick="$(this).parent('td').find('table').toggle()">Dropoff Service ({{detail.dropoff.area}} - {{detail.dropoff.time}})</a>
							<table class="table table-borderless table-condensed hidden-field" style="background:none">
								<tr>
									<td width="130">Hotel Name</td>
									<td><strong>{{detail.dropoff.hotel_name}}</strong></td>
								</tr>
								<tr>
									<td>Hotel Addres</td>
									<td><strong>{{detail.dropoff.hotel_address}}</strong></td>
								</tr>
								<tr>
									<td>Hotel Phone Number</td>
									<td><strong>{{detail.dropoff.hotel_phone_number}}</strong></td>
								</tr>
							</table>
						</td>
						<td align="right">
							<strong ng-show='detail.dropoff.price > 0 && detail.subtotal == detail.subtotal_before_cancel'>
								{{detail.rates.currency}}
								{{fn.formatNumber(detail.dropoff.price,detail.rates.currency)}}
							</strong>
						</td>
					</tr>
					<tr ng-show='detail.additional_service' ng-repeat='additional_service in detail.additional_service'>
						<td>{{additional_service.name}} ({{additional_service.qty}}x @ {{detail.rates.currency}} {{fn.formatNumber(additional_service.price,detail.rates.currency)}})</td>
						<td align="right">
							<strong>
							{{detail.rates.currency}} {{fn.formatNumber((additional_service.price * additional_service.qty),detail.rates.currency)}}
							</strong>
						</td>
					</tr>
				</tbody>
				<tr class="success">
					<td align="right"><strong>Total</strong></td>
					<td align="right">
						<strong>
							{{DATA.current_booking.booking.currency}}
							{{fn.formatNumber(DATA.current_booking.booking.total_before_discount,DATA.current_booking.booking.currency)}}
						</strong>
					</td>
				</tr>
				<tr class="success">
					<td align="right">
						<strong>
							Discount 
							<span ng-show='DATA.current_booking.booking.discount.amount > 0'>
								&nbsp;&nbsp;
								<span ng-show="DATA.current_booking.booking.discount.type == '%'">({{DATA.current_booking.booking.discount.value}}%)</span>
								<span ng-show="DATA.current_booking.booking.discount.type != '%'">({{fn.formatNumber(DATA.current_booking.booking.discount.value,DATA.current_booking.booking.currency)}})</span>
							</span>
						</strong>
					</td>
					<td align="right">
						<strong>
							{{DATA.current_booking.booking.currency}}
							{{fn.formatNumber(DATA.current_booking.booking.discount.amount,DATA.current_booking.booking.currency)}}
						</strong>
					</td>
				</tr>
                <tr class="success" ng-show='DATA.current_booking.booking.tax_service.amount > 0'>
					<td align="right">
						<strong>
							Tax & Service 
							<span ng-show='DATA.current_booking.booking.tax_service.amount > 0'>
								&nbsp;&nbsp;
								<span ng-show="DATA.current_booking.booking.tax_service.type == '%'">({{DATA.current_booking.booking.tax_service.value}}%)</span>
								<span ng-show="DATA.current_booking.booking.tax_service.type != '%'">({{fn.formatNumber(DATA.current_booking.booking.tax_service.value,DATA.current_booking.booking.currency)}})</span>
							</span>
						</strong>
					</td>
					<td align="right">
						<strong>
							{{DATA.current_booking.booking.currency}}
							{{fn.formatNumber(DATA.current_booking.booking.tax_service.amount,DATA.current_booking.booking.currency)}}
						</strong>
					</td>
				</tr>
				<tr class="success">
					<td align="right"><strong>Grand Total</strong></td>
					<td align="right">
						<strong>
							{{DATA.current_booking.booking.currency}}
							{{fn.formatNumber(DATA.current_booking.booking.grand_total,DATA.current_booking.booking.currency)}}
						</strong>
					</td>
				</tr>
				<tr class="info">
					<td align="right"><strong>Total Payment</strong></td>
					<td align="right">
						<strong>
							{{DATA.current_booking.booking.currency}}
							({{fn.formatNumber(DATA.current_booking.booking.total_payment,DATA.current_booking.booking.currency)}})
						</strong>
					</td>
				</tr>
				<tr style="font-weight:bold" ng-class="{'danger':(DATA.current_booking.booking.balance>0), 'info':(DATA.current_booking.booking.balance<=0)}">
					<td align="right"><strong>Outstanding Invoice</strong></td>
					<td align="right">
						<strong>
							{{DATA.current_booking.booking.currency}}
							{{fn.formatNumber(DATA.current_booking.booking.balance,DATA.current_booking.booking.currency)}}
						</strong>
					</td>
				</tr>
				<tr style="font-weight:bold" class="warning" ng-show='DATA.current_booking.booking.total_refund'>
					<td align="right"><strong>Refund</strong></td>
					<td align="right">
						<strong>
							{{DATA.current_booking.booking.currency}}
							{{fn.formatNumber(DATA.current_booking.booking.total_refund, DATA.current_booking.booking.currency)}}
						</strong>
					</td>
				</tr>
			</table>
			
            
			<?php /*?><br />
            <div style="color:red">
				<em>Void instruction only valid for 24 hours after transaction is made. Reseervations has made will cancelled and refund will requested.</em>
            </div><?php */?>
            
			<br /><hr />
            
            <div ng-show="DATA.current_booking.booking.inserted_by" class="pull-right">
                <em>Created By : <strong>{{DATA.current_booking.booking.inserted_by.name}}</strong></em>
            </div>
			
			<?php $this->load->view("crs/reservation/v_trans_reservation_detail_form_cancelation") ?>
            <?php $this->load->view("crs/reservation/v_trans_reservation_detail_form_void") ?>
            <?php $this->load->view("crs/reservation/v_trans_reservation_detail_form_refund") ?>
			
			<?php /*?><a ui-sref="agent.edit({'agent_code':DATA.current_agent.agent_code})"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Agent</a><?php */?>
			<script>activate_sub_menu_agent_detail("detail");</script>
		</div>
	</div>
</div>
<style>
	.reservation-detail .title{margin-bottom:20px}
	.reservation-detail .title h1{margin-bottom:10px !important;}
	.reservation-detail .title .code{margin-bottom:5px;}
</style>
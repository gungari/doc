<div class="pull-right">
	<ul class="nav nav-pills">
		<li role="presentation" class="pickup-dropoff"><a ui-sref="act_arrival_departure.pickup_dropoff"><i class="fa fa-users" aria-hidden="true"></i> Pickup / Dropoff</a></li>
		<li role="presentation" class="arrival-departure"><a ui-sref="act_arrival_departure"><i class="fa fa-address-book-o" aria-hidden="true"></i> Arrival / Departure</a></li>
	</ul>
</div>

<div ui-view>
	<h1>Participant List</h1>
	
	<div ng-init='arrivalDepartureAct();'>
		<div class="products">
			<div class="product">
			<form ng-submit="arrivalDepartureActSearch()" class="form-inline">
				<div class="table-responsive">
					<table class="table table-condensed table-borderless" width="100%">
						<tr>
							<td width="110">Activity Date</td>
							<!-- <td width="110">Status</td> -->
							<td width="200">Product</td>
							
							<td></td>
						</tr>
						<tr>
							<td><input type="text" class="form-control input-md datepicker" style="width:120px" placeholder="Select Date" ng-model='dept.date' required /></td>
							
							<td>
								<select class="form-control input-md" ng-model='dept.product'>
									<option value="">All</option>
									<option value="{{product.product_code}}" ng-repeat='product in DATA.products'>{{product.name}}</option>
				                   
								</select>
							</td>
							<td><button type="submit" class="btn btn-md btn-primary">Search</button></td>
						</tr>
					</table>
				</div>
            	
			</form>
			</div>
		</div>
		
		<div ng-show='show_arrivalDepartureTransSearch_loading'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		</div>
		
		<div ng-show='!show_arrivalDepartureTransSearch_loading && arrival_departure'>
			
			
				
				<a href="<?=site_url("home/print_page/#/print/act_arrival_departure/")?>{{search.date}}" target="_blank" class="pull-right">
					<button type="button" class="btn btn-default"><span class="glyphicon glyphicon-print"></span> Print</button>
				</a>
				
				<h4 class="text-capitalize">{{search.type}}</h4>
				<table class="table table-borderless table-condensed">
					<tr>
						<td width="100">Activity Date</td>
						<td width="150"><strong>{{fn.formatDate(search.date,"d MM yy")}}</strong></td>
                        
						<td width="60">Total</td>
						<td width="150"><strong>{{summary.total_all}} Pax</strong></td>
                        
                        <td width="60">Check In</td>
						<td><strong>{{summary.total_checked_in}} Pax</strong></td>
                        
						<td></td>
					</tr>
				</table>
			
			<br>
			
				<div style="margin-bottom: 20px !important;padding:0 !important;">
				<table class="table table-bordered table-condensed">
					<tr class="success" style="font-weight:bold">
						<td width="40">No</td>
						<td width="90">Voucher#</td>
						<td>Name</td>
						<!-- <td align="center" title="Nationality">Nat.</td>
						<td >ID Number</td>
						<td >Phone</td> -->
						<td >Booking Source</td>
						<td >Pickup / Dropoff</td>
						<td align="center">Check In</td>
					</tr>
					<tbody ng-repeat='passenger_list in  arrival_departure'>
						<tr  style="background:#FAFAFA">
							<td colspan="11">
								<strong>{{passenger_list.product.product_code}} - {{passenger_list.product.name}}</strong>
							</td>
						</tr>
						<tr  ng-repeat="arr_dept in  passenger_list.passenger_list" ng-class="{'not_checkin':arr_dept.checkin_status != '1'}" style="font-size:12px !important">
							<td>{{($index+1)}}</td>
							<td>
								<a href="" ng-click="openPopUpWindowVoucher('<?=site_url("home/print_page/#/print/voucher/")?>', arr_dept.booking_code, arr_dept.voucher_code)">
									{{arr_dept.voucher_code}}
								</a>
								
							</td>
							<td>
                            	<div>{{arr_dept.first_name}} {{arr_dept.last_name}}</div>
								<div title="{{arr_dept.country_name}}">{{arr_dept.country_name}}</div>
								<div ng-show="arr_dept.passport_number">ID#:{{arr_dept.passport_number}}</div>
								<div ng-show="arr_dept.phone">Ph.:{{arr_dept.phone}}</div>
								<div ng-show="arr_dept.pass_code" style="font-size: 8px;">
									<b>Passcode: {{arr_dept.pass_code}}</b>
								</div>
							</td>
							<?php /* <!-- <td title="{{arr_dept.country_name}}" align="center">{{arr_dept.country_code}}</td>
							<td>{{arr_dept.passport_number}}</td>
							<td>{{arr_dept.phone}}</td> --> */ ?>
							<td>
								<div ng-show='!arr_dept.agent' class="text-capitalize">{{arr_dept.source.toLowerCase()}}</div>
								<div ng-show='arr_dept.agent'>{{arr_dept.agent.name}}</div>
								
								<div style="font-size: 8px;">
									<b><i>Remarks: {{arr_dept.remarks}}</i></b>
								</div>
							</td>
							<td>
								<div ng-show='arr_dept.pickup'>{{arr_dept.pickup.hotel_name}}</div>
								<div ng-show='arr_dept.dropoff'>{{arr_dept.dropoff.hotel_name}}</div>
							</td>
							<td align="center"><span class="glyphicon glyphicon-ok" ng-show="arr_dept.checkin_status == '1'"></span> </td>
						</tr>
					</tbody>
				</table>
				</div>
			<?php  
			/*<div  class="table-responsive col-md-8 col-xs-6" zzzstyle="overflow-x:scroll" style="margin:0 !important;padding:0 !important;">
			<div style="width:1000px">
			<table class="table table-bordered table-condensed">
				<tr class="success" style="font-weight:bold">
					
					<td align="center" title="Nationality">Nat.</td>
					<td >ID Number</td>
					<td >Phone</td>
					<td >Booking Source</td>
					<td >Pickup / Dropoff</td>
					<td align="center">Check In</td>
				</tr>
				<tbody ng-repeat='passenger_list in  arrival_departure'>
					<tr height="50" style="background:#FAFAFA">
						<td colspan="8">&nbsp;</td>
					</tr>
					<tr height="50" ng-repeat="arr_dept in  passenger_list.passenger_list" ng-class="{'not_checkin':arr_dept.checkin_status != '1'}" style="font-size:12px !important">
						<td title="{{arr_dept.country_name}}" align="center">{{arr_dept.country_code}}</td>
						<td>{{arr_dept.passport_number}}</td>
						<td>{{arr_dept.phone}}</td>
						<td>
							<div ng-show='!arr_dept.agent' class="text-capitalize">{{arr_dept.source.toLowerCase()}}</div>
							<div ng-show='arr_dept.agent'>{{arr_dept.agent.name}}</div>
						</td>
						<td>
							<div ng-show='arr_dept.pickup'>{{arr_dept.pickup.hotel_name}}</div>
							<div ng-show='arr_dept.dropoff'>{{arr_dept.dropoff.hotel_name}}</div>
						</td>
						<td align="center"><span class="glyphicon glyphicon-ok" ng-show="arr_dept.checkin_status == '1'"></span> </td>
					</tr>
				</tbody>
			
			</table>
			</div>
			</div> */
			?>
			
			
			<br>
			<a href="<?=site_url("export_to_excel/act_arrival_departure")?>?s[date]={{dept.date}}&s[product]={{dept.product}}" target="_blank"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export To Excel</a>
			&nbsp; | &nbsp;
			<a href="<?=site_url("home/print_page/#/print/act_arrival_departure/")?>{{dept.date}}/{{dept.product}}" target="_blank"><i class="fa fa-print" aria-hidden="true"></i> Print</a>
			
			</div>
			
			
		
		
		<style>
			table tr.not_checkin{color:#666; background:#EEE}
		</style>
		<script>GeneralJS.activateSubMenu(".nav-pills", "li", ".arrival-departure");</script>
		<?php /*?>{{dept}}
		<br />
		{{DATA_R}}<?php */?>
</div>
</div>
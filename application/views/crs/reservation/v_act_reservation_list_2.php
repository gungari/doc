<?php if (is_array($vendor["category"])){ ?>
    <div class="pull-right">
        <div class="btn-group">
            <button type="button" class="btn btn-success btn-md dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="glyphicon glyphicon-plus"></span> New Reservation </a> <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a ui-sref="act_reservation.new">Activities</a></li>
                <li><a ui-sref="trans_reservation.new">Transport</a></li>
            </ul>
        </div>
	</div>
<?php }else{ ?>
	<div class="pull-right"> <a ui-sref="trans_reservation.new" class="btn btn-success btn-md"> <span class="glyphicon glyphicon-plus"></span> New Reservation </a> </div>
<?php } ?>

<h1>Reservation</h1>
<div ui-view>
	<div ng-init="loadDataBookingTransport_2()">
		<div class="products">
			<div class="product">
				<form ng-submit='loadDataBookingTransport_2(1, true)'>
				<div class="table-responsive">
					<table class="table table-condensed table-borderless" width="100%">
						<tr>
							<td width="110">From</td>
							<td width="110">To</td>
							<td width="200">Search</td>
							<td width="130">Source</td>
							<td width="130">Status</td>
							<td></td>
						</tr>
						<tr>
							<td><input type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='search.start_date' /></td>
							<td><input type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='search.end_date' /></td>
							<td><input type="text" class="form-control input-sm" placeholder="Search" ng-model='search.q' /></td>
							<td>
								<select class="form-control input-sm" ng-model='search.booking_source'>
									<option value="">All</option>
									<option value="{{booking_source.code}}" ng-repeat='booking_source in $root.DATA_booking_source'>{{booking_source.name}}</option>
								</select>
							</td>
							<td>
								<select class="form-control input-sm" ng-model='search.booking_status'>
									<option value="">All</option>
									<option value="{{booking_status.code}}" ng-repeat='booking_status in $root.DATA_booking_status'>{{booking_status.name}}</option>
								</select>
							</td>
							<td><button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button></td>
						</tr>
					</table>
					
					<?php /*?>Filter : <input type="text" ng-model="DATA.filter_booking" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
					<button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button><?php */?>
				</div>	
				</form>
			</div>
		</div>

		<div ng-show='show_loading_DATA_bookings'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		</div>
		
		<div ng-show='!show_loading_DATA_bookings'>
			<div class="table-responsive">
				<table class="table table-condensed table-bordered">
					<tr class="header bold">
						<td align="center" width="90">Order#</td>
						<td align="center" width="110">Resv. Date</td>
						<td>Customer Name</td>
						<td align="center" width="40" title="Nationality">Nat.</td>
						<td width="200">Booking Source</td>
						<td align="center" width="50">Status</td>
						<td align="right" width="100">Amount</td>
					</tr>
					<tbody ng-repeat="booking in DATA.bookings.bookings | filter:DATA.filter_booking">
						<?php /*?><tr ng-class="{'danger':(booking.status_code=='CANCEL'), 'warning':(booking.status_code=='UNDEFINITE')}"><?php */?>
						<tr ng-class="{'danger':(booking.cancel_status=='CAN' || booking.cancel_status=='VOID'), 'warning':(booking.status_code=='UNDEFINITE')}">
							<td align="center" rowspan="2">
								<a ui-sref="act_reservation.detail({'booking_code':booking.booking_code})"><strong>{{booking.booking_code}}</strong></a>
							</td>
							<td align="center">{{fn.newDate(booking.transaction_date) | date:'dd MMM yyyy'}} <small>{{fn.newDate(booking.transaction_date) | date:'HH:mm'}}</small> </td>
							<?php /*?><td align="center">{{fn.formatDate(booking.transaction_date, "dd M yy")}}</td><?php */?>
							<td>
								<strong>{{booking.customer.full_name}}</strong>
								<?php /*?><br /> {{booking.customer.email}}<?php */?>
							</td>
							<td align="center" class="text-capitalize" title="{{booking.customer.country_name}}">{{booking.customer.country_code}}</td>
							<td>
								<div ng-show='!booking.agent' class="text-capitalize">{{booking.source.toLowerCase()}}</div>
								<div ng-show='booking.agent'>{{booking.agent.name}}</div>
							</td>
							<td align="center" title="{{booking.status}}">
                            	<?php /*?><span ng-show="booking.cancel_status == ''">DEF</span>
                                <span ng-show="booking.cancel_status != ''">{{booking.cancel_status}}</span><?php */?>
                                <span ng-show="booking.cancel_status=='CAN'">{{booking.cancel_status}}</span>
								<span ng-show="booking.cancel_status!='CAN'">{{((booking.status_initial=="CAN")?"DEF":booking.status_initial)}}</span>
                                
								<?php /*?><span ng-show="booking.cancel_status=='CAN'">{{booking.cancel_status}}</span>
								<span ng-show="booking.cancel_status!='CAN'">{{((booking.status_initial=="CAN" || booking.status_initial=="VOID")?"DEF":booking.status_initial)}}</span><?php */?>
							</td>
							<td align="right">
								{{booking.currency}} {{fn.formatNumber(booking.grand_total, booking.currency)}}
							</td>
						</tr>
						<?php /*?><tr ng-class="{'danger':(booking.status_code=='CANCEL'), 'warning':(booking.status_code=='UNDEFINITE')}"><?php */?>
						<tr ng-class="{'danger':(booking.cancel_status=='CAN' || booking.cancel_status=='VOID'), 'warning':(booking.status_code=='UNDEFINITE')}">
							<td colspan="6" style="padding:0 !important">
								
								<table class="table table-bordered table-condensed" style="margin:-1px !important; background:none">
									<tr ng-show="booking.remarks || booking.agent && booking.voucher_reff_number != ''">
										<td style="border-left:solid 5px #FFF; font-size:11px">
											<em>
                                            	Remarks : {{booking.remarks}}
                                                <span ng-show="booking.agent && booking.voucher_reff_number != ''">(Voucher# Reff. : {{booking.voucher_reff_number}})</span>
                                        	</em>
										</td>
									</tr>
									<?php /*?><tr ng-repeat="voucher in booking.detail | orderBy : '-booking_detail_status_code'" ng-class="{'danger':(voucher.booking_detail_status_code=='CANCEL')}"><?php */?>
									<tr ng-repeat="voucher in booking.detail | orderBy : '-booking_detail_status_code'" 
										ng-class="{'danger':(booking.cancel_status=='CAN')}">
										<td style="border-left:solid 5px #EEE; font-size:11px">
											{{voucher.voucher_code}} - 
											{{fn.formatDate(voucher.date, "dd / M / y")}} - 
                                            
                                            {{voucher.product.name}}
                                            
											<?php /*?>{{voucher.departure.port.port_code}} ({{voucher.departure.time}})
											<span class="glyphicon glyphicon-chevron-right"></span>
											{{voucher.arrival.port.port_code}} ({{voucher.arrival.time}})<?php */?>
											
											<span ng-show="voucher.qty_1 > 0"> - {{voucher.qty_1}} Adl. </span>
											<span ng-show="voucher.qty_2 > 0"> - {{voucher.qty_2}} Chi. </span>
											<span ng-show="voucher.qty_3 > 0"> - {{voucher.qty_3}} Inf. </span>
											
											<span ng-show="voucher.is_pickup=='YES'"> - Pickup </span>
											<span ng-show="voucher.is_dropoff=='YES'"> - Dropoff </span>
										</td>
									</tr>
								</table>
								
								<?php /*?><table class="table table-bordered table-condensed" style="margin:-1px !important">
									<tr ng-repeat='voucher in booking.detail' ng-class="{'danger':(booking.status_code=='CANCEL'), 'warning':(booking.status_code=='UNDEFINITE')}">
										<td width="100">{{voucher.voucher_code}}</td>
										<td width="90">{{fn.formatDate(voucher.date, "dd M yy")}}</td>
										<td width="90">{{voucher.departure.port.port_code}} ({{voucher.departure.time}})</td>
										<td width="90">{{voucher.arrival.port.port_code}} ({{voucher.arrival.time}})</td>
										<td width="45" align="center" title="Pickup">{{(voucher.is_pickup=='YES')?'PU':'-'}}</td>
										<td width="45" align="center" title="Dropoff">{{(voucher.is_dropoff=='YES')?'DO':'-'}}</td>
										<td></td>
									</tr>
								</table><?php */?>
								
							</td>
						</tr>
						<tr>
							<td colspan="7" style="background:#FAFAFA"></td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<nav aria-label="Page navigation" class="pull-right">
			  <ul class="pagination pagination-sm">
				<li ng-class="{'disabled':DATA.bookings.search.page <= 1}">
				  <a href="" ng-click='loadDataBookingTransport_2(DATA.bookings.search.page-1, true)' aria-label="Prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
				</li>
				<li ng-repeat='pagination in DATA.bookings.search.pagination' ng-class="{'active':DATA.bookings.search.page == pagination}">
					<a href="" ng-click='loadDataBookingTransport_2(($index+1), true)'>{{($index+1)}}</a>
				</li>
				<li ng-class="{'disabled':DATA.bookings.search.page >= DATA.bookings.search.number_of_pages}">
				  <a href="" ng-click='loadDataBookingTransport_2(DATA.bookings.search.page+1, true)' aria-label="Next"><span class="glyphicon glyphicon-chevron-right"></span></a>
				</li>
			  </ul>
			</nav>
			<div class="clearfix"></div>
			
			<hr />
			<a href="<?=site_url("export_to_excel/act_bookings")?>?s[start_date]={{DATA.bookings.search.start_date}}&s[end_date]={{DATA.bookings.search.end_date}}&s[q]={{DATA.bookings.search.q}}&s[booking_status]={{DATA.bookings.search.booking_status}}&s[booking_source]={{DATA.bookings.search.booking_source}}&s[page]={{DATA.bookings.search.page}}" target="_blank"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export To Excel</a>
		</div>
		<br />
		
		<div class="add-product-button"> <a ui-sref="act_reservation.new" class="btn btn-success btn-lg btn-block"> <span class="glyphicon glyphicon-plus"></span> New Reservation </a> </div>
	</div>
</div>

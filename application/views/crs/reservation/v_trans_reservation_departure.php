
<div class="pull-right">
	<ul class="nav nav-pills">
		<li role="presentation" class="pickup-dropoff"><a ui-sref="trans_arrival_departure.pickup_dropoff"><i class="fa fa-users" aria-hidden="true"></i> Pickup / Dropoff</a></li>
		<li role="presentation" class="arrival-departure"><a ui-sref="trans_arrival_departure"><i class="fa fa-address-book-o" aria-hidden="true"></i> Arrival / Departure</a></li>
	</ul>
</div>
<div ui-view>
	<style>
		.open{
			background:#fdffbe
		}
	</style>
	<h1>Passenger List</h1>
	
	<div ng-init='arrivalDepartureTrans();'>
		<div class="products">
			<div class="product">
			<form ng-submit="arrivalDepartureTransSearch()">
				<table class="table table-borderless table-condensed">
					<tr>
						<td width="50"></td>
						<td width="200">
							<label><input ng-change='arrivalDepartureTime();selectArrivalDepartureType();' type="radio" ng-model='dept.type' value="departure" /> Departure</label>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<label><input ng-change='arrivalDepartureTime();selectArrivalDepartureType();' type="radio" ng-model='dept.type' value="arrival" /> Arrival</label>
						</td>
						<td width="50"></td>
						<td width="200"></td>
						<td width="50"></td>
						<td></td>
					</tr>
					<tr>
						<td>Date</td>
						<td>
							<input type="text" ng-change='arrivalDepartureTime()' class="form-control input-md datepicker" style="width:200px" placeholder="Select Date" ng-model='dept.date' required />
						</td>
						<td>Boat</td>
						<td>
							<select ng-change='arrivalDepartureTime()' class="form-control input-md" ng-model='dept.boat' required
								ng-options="(boats.name +' - '+boats.boat_code) for boats in DATA.boats.boats">
								<option value="" disabled>-- Select Boat --</option>
							</select>
						</td>
						<td></td>
						<td></td>
					</tr>
					
					<tr>
						<td>From</td>
						<td>
							<select ng-change='arrivalDepartureTime()' class="form-control input-md" ng-model='dept.departure' required style="width:200px"
								ng-options="(port.name) for port in DATA.ports_arrival.ports">
								<option value="" disabled="disabled">-- Departure --</option>
							</select>
						</td>
						<td> To </td>
						<td>
							<select ng-change='arrivalDepartureTime()' class="form-control input-md" ng-model='dept.destination' required style="width:200px"
								ng-options="(port.name) for port in DATA.ports_departure.ports">
								<option value="" disabled="disabled">-- Arrival --</option>
							</select>
						</td>
						<td>Time</td>
						<td class="form-inline">
							<select class="form-control input-md" ng-model='dept.time' style="width:150px"
								ngxx-options="tm.time for tm in DATA.time">
								<option value="">All Time</option>
							</select>
						</td>
					</tr>
					
					<?php /*?><tr>
						<td width="100">Schedule</td>
						<td>
							<select class="form-control input-md" ng-model='dept.schedule' required ng-change='departureTransGetScheduleDetail()'
								ng-options="(schedules.name +' - '+schedules.schedule_code) for schedules in DATA.schedules.schedules">
								<option value="" disabled>-- Select Schedule --</option>
							</select>
						</td>
					</tr><?php */?>
					
					<tr>
						<td></td>
						<td><button type="submit" class="btn btn-md btn-primary">Search</button></td>
					</tr>
				</table>
			</form>
			</div>
		</div>
		
		<div ng-show='show_arrivalDepartureTransSearch_loading'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		</div>
		
		<div ng-show='!show_arrivalDepartureTransSearch_loading && arrival_departure'>
			
				<a href="<?=site_url("home/print_page/#/print/trans_arrival_departure/")?>{{search.type}}/{{search.date}}/{{search.boat.id}}/{{(search.departure_port=='ALL')?'ALL':search.departure_port.id}}/{{(search.destination_port=='ALL')?'ALL':search.destination_port.id}}/{{(search.time)?search.time:'ALL'}}" target="_blank" class="pull-right">
					<button type="button" class="btn btn-default"><span class="glyphicon glyphicon-print"></span> Print</button>
				</a>
				
				<h4 class="text-capitalize">{{search.type}}</h4>
				<table class="table table-borderless table-condensed">
					<tr>
						<td width="50">Date</td>
						<td width="200"><strong>{{fn.formatDate(search.date,"d MM yy")}}</strong></td>
						<td width="70">Boat</td>
						<td width="200"><strong>{{search.boat.name}} ({{search.boat.capacity}} seats)</strong></td>
						<td width="50"></td>
						<td></td>
					</tr>
					<tr>
						<td>From</td>
						<td><strong>{{(search.departure_port=='ALL')?'All':search.departure_port.name}}</strong></td>
						<td>To</td>
						<td><strong>{{(search.destination_port=='ALL')?'All':search.destination_port.name}}</strong></td>
						<td><?php /*?>Time<?php */?></td>
						<td><?php /*?><strong>{{(search.time=='ALL')?'All':search.time}}</strong><?php */?></td>
					</tr>
					<tr>
						<td>Total</td>
						<td><strong>{{summary.total_all}} Pax</strong></td>
						<td>Check In</td>
						<td><strong>{{summary.total_checked_in}} Pax</strong></td>
						<td></td>
						<td></td>
					</tr>
				</table>
			
			<br>
			
				<div class="col-md-12 col-xs-12" style="margin-bottom: 20px !important;padding:0 !important;">
				<table class="table table-bordered table-condensed">
					<tr class="success" style="font-weight:bold">
						<td width="40">No</td>
						<td width="90">Voucher#</td>
						<td width="150">Booking Source</td>
						<td colspan="2" align="center">
							Trip
							<?php /*?><span ng-show="search.type =='departure'">Destination</span>
							<span ng-show="search.type =='arrival'">Departure</span><?php */?>
						</td>
						<td>Pickup</td>
						<td colspan="2">Remark</td>
					</tr>
					<tbody ng-repeat='passenger_list in arrival_departure'>
						<tr style="background:#FAFAFA">
							<td colspan="8">
								<strong>{{passenger_list.departure_port.name}} ({{passenger_list.departure_time}}) </strong>
								to								
								<strong>{{passenger_list.arrival_port.name}} ({{passenger_list.arrival_time}}) </strong>
							</td>
						</tr>
						<tr ng-class="{'open':voucher_[passenger_list.voucher_code]}" height="40" class="not_checkin" >
							<td>
								<span ng-show="voucher_[passenger_list.voucher_code]" style="font-size: 8px;" class="glyphicon glyphicon-triangle-bottom"></span>
								<span ng-show="!voucher_[passenger_list.voucher_code]" style="font-size: 8px;" class="glyphicon glyphicon-triangle-right"></span>
							
								&nbsp;{{$index+1}}
							</td>
							<td>
								<a style="cursor: pointer;" ng-click="show_collapes(passenger_list.voucher_code)">
									{{passenger_list.voucher_code}}
								</a>
							</td>
							<td>
								<div ng-show='!passenger_list.agent' class="text-capitalize">{{passenger_list.source.toLowerCase()}}</div>
								<div ng-show='passenger_list.agent'>{{passenger_list.agent.name}}</div>
							</td>
							<td width="90">
								<strong>{{passenger_list.departure_port.port_code}} ({{passenger_list.departure_time}}) </strong>
							</td>
							<td width="90">
								<strong>{{passenger_list.arrival_port.port_code}} ({{passenger_list.arrival_time}}) </strong>
							</td>
							<td>
								<div ng-show='arr_dept.pickup'>{{arr_dept.pickup.hotel_name}}</div>
								<div ng-show='arr_dept.dropoff'>{{arr_dept.dropoff.hotel_name}}</div>
							</td>
							<td colspan="2">
								{{passenger_list.remarks}}
							</td>
							
						</tr>
						
							<tr ng-show="voucher_[passenger_list.voucher_code]" style="background:#FAFAFA;">
								<td></td>
								<td align="center">#</td>
								<td>Names</td>
								<td>Email</td>
								<td>Phone</td>
								<td align="center">Country</td>
								<td>Checkin</td>
								<td>User ID</td>
							</tr>
							<tr ng-show="voucher_[passenger_list.voucher_code]" ng-repeat="arr_dept in  passenger_list.passenger_list" style="font-size:11px !important">
								<td></td>
								
								<td align="center">{{($index+1)}}</td>
								
								<td>
									<div>{{arr_dept.first_name}} {{arr_dept.last_name}}</div>
									<div title="{{arr_dept.country_name}}">{{arr_dept.country_name}}</div>
									<div ng-show="arr_dept.passport_number">ID#:{{arr_dept.passport_number}}</div>
									<div ng-show="arr_dept.phone">Ph.:{{arr_dept.phone}}</div>
								</td>

								<td>{{arr_dept.email}}</td>
								<td>{{arr_dept.phone}}</td>
								<td align="center">{{arr_dept.country_code}}</td>

								
								<td align="center"><span class="glyphicon glyphicon-ok" ng-show="arr_dept.checkin_status == '1'"></span> </td>
								<td></td>
							</tr>
						
					</tbody>
				</table>
				</div>
			
			<hr />
			<a href="<?=site_url("export_to_excel/trans_arrival_departure")?>?s[type]={{search.type}}&s[date]={{search.date}}&s[schedule_code]={{search.schedule_code}}&s[boat_id]={{search.boat.id}}&s[departure_port_id]={{(search.departure_port=='ALL')?'ALL':search.departure_port.id}}&s[arrival_port_id]={{(search.destination_port=='ALL')?'ALL':search.destination_port.id}}&s[time]={{search.time}}" target="_blank"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export To Excel</a>
			&nbsp; | &nbsp;
			<a href="<?=site_url("home/print_page/#/print/trans_arrival_departure/")?>{{search.type}}/{{search.date}}/{{search.boat.id}}/{{(search.departure_port=='ALL')?'ALL':search.departure_port.id}}/{{(search.destination_port=='ALL')?'ALL':search.destination_port.id}}/{{(search.time)?search.time:'ALL'}}" target="_blank"><i class="fa fa-print" aria-hidden="true"></i> Print</a>
		</div>
	
		<script>
			GeneralJS.activateSubMenu(".nav-pills", "li", ".arrival-departure");
		</script>
	</div>
</div>


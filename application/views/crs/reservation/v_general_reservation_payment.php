<!-- <div class="sub-title"> Billing Statement</div>  -->
<br />
<div ng-init="loadDataBookingPayment();" class="reservation-detail">
	<div ng-show='!(DATA.payment)'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	<div ng-show='(DATA.payment)'>
		<div class="table-responsive">
			<table class="table table-bordered table-payment">
				<tr class="info" style="font-weight:bold">
					<td align="center" width="90">Date</td>
					<td align="center" width="120">Order#</td>
					<td>Description</td>
					<td width="150">Payment Type</td>
					<td align="right" colspan="2">Amount</td>
					<td width="30"></td>
				</tr>
				<tr ng-repeat='payment in DATA.payment.payment.detail' ng-hide="payment.payment_type_code == 'INVOICE' && payment.total_payment == 0">
					<td align="center">{{fn.formatDate(payment.date, "dd M yy")}}</td>
					<td align="center">
						<span>
							<a href="" data-toggle="modal" ng-click="paymentDetail(payment)" data-target="#payment-detail">
								{{payment.payment_code}}
							</a>
						</span>
					</td>
					<td>
                    	{{payment.description}}
                    </td>
					<td>{{payment.payment_type}}</td>
					<td align="center" width="50">{{payment.currency}}</td>
					<td align="right" width="110">
						<div>
                            <strong ng-show='payment.amount>=0'>{{fn.formatNumber(payment.amount, payment.currency)}}</strong>
                            <strong ng-show='payment.amount<0'>({{fn.formatNumber(payment.amount*-1, payment.currency)}})</strong>
                        </div>
                        <div ng-show="payment.payment_currency && payment.payment_currency != ''">
                        	<small><em>
                            	Paid in : <br />
                                ({{payment.payment_currency}} {{fn.formatNumber(payment.payment_amount*-1, payment.payment_currency)}})
                            </em></small>
                        </div>
					</td>

					<td align="center">
                    	<div ng-hide="payment.payment_type_code == 'ONLINE'">
                            <a href="" ng-click="paymentDetail(payment, true)" data-toggle="modal" data-target="#payment-detail" class="delete-icon" ng-show="payment.id != ''">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </div>
						<?php /* <!-- &nbsp;&nbsp;
						<a href="<?=site_url("home/print_page/#/print/receipt_trans/")?>{{booking_code}}" target="_blank" ng-show="payment.id == ''">
							<span class="glyphicon glyphicon-print"></span>
						</a>
						<a href="<?=site_url("home/print_page/#/print/receipt_trans_payment/")?>{{payment.payment_code}}" target="_blank" ng-show="payment.id != ''">
							<span class="glyphicon glyphicon-print"></span>
						</a> -->
						*/?>
					</td>
				</tr>
				<tr>
					<td colspan="7"></td>
				</tr>
				<tr class="success" style="font-weight:bold">
					<td colspan="4" align="right">Total Transaction</td>
					<td>{{DATA.payment.payment.currency}}</td>
					<td align="right">{{fn.formatNumber(DATA.payment.payment.total_transaction, DATA.payment.payment.currency)}}</td>
					<td></td>
				</tr>
				<tr class="info" style="font-weight:bold; color:green"
                	ng-hide="DATA.current_booking.booking.status_code == 'VOID'">
					<td colspan="4" align="right">Paid</td>
					<td>{{DATA.payment.payment.currency}}</td>
					<td align="right">({{fn.formatNumber(DATA.payment.payment.total_real_payment_with_invoice, DATA.payment.payment.currency)}})</td>
					<td></td>
				</tr>
				<tr style="font-weight:bold" ng-class="{'danger':(DATA.payment.payment.balance_real_payment_with_invoice>0), 'info':(DATA.payment.payment.balance_real_payment_with_invoice<=0)}"
                	ng-hide="DATA.current_booking.booking.status_code == 'VOID'">
					<td colspan="4" align="right">Outstanding Order</td>
					<td>{{DATA.payment.payment.currency}}</td>
					<td align="right">{{fn.formatNumber(DATA.payment.payment.balance_real_payment_with_invoice, DATA.payment.payment.currency)}}</td>
					<td></td>
				</tr>
				
			</table>
		</div>
		<br />
		<div class="pull-right">
			<a style="cursor: pointer;" href="<?=site_url("home/print_page/#/print/receipt_billing_statement/")?>{{booking_code}}" target="_blank" ng-show="payment.id != ''">
				<button type="button" class="btn btn-primary btn-sm">
					<span class="glyphicon glyphicon-print"></span>
					&nbsp;Print Billing Statement
				</button>
			</a>
		</div>
        
        <?php // SHOW DATA UNPAID INVOICE  ?>
        <div ng-show='DATA.payment.payment.invoices && DATA.current_booking.booking'>
        	<div ng-repeat="invoice in DATA.payment.payment.invoices" 
                ng-show="invoice.total_payment == 0">
                <em>
                	Order <strong>#{{DATA.current_booking.booking.booking_code}}</strong> has invoiced with Invoice 
                	<strong><a ng-show='invoice.is_proforma_invoice' ui-sref="ar.proforma_invoice_detail({'invoice_code':invoice.payment_code})" target="_blank">{{invoice.payment_code}}</a></strong> 
                    <strong><a ng-show='!invoice.is_proforma_invoice' ui-sref="ar.invoice_detail({'invoice_code':invoice.payment_code})" target="_blank">{{invoice.payment_code}}</a></strong> 
                </em>
            </div>
        </div>
        
		<hr />
		<div class="add-product-button" ng-show="DATA.payment.payment.balance>0 && DATA.current_booking.booking.status_code != 'CANCEL'"> 
			<a href="" class="btn btn-success btn-lg btn-block" data-toggle="modal" ng-click='addEditPayment()' data-target="#add-edit-payment"> <span class="glyphicon glyphicon-plus"></span> Add Payment </a> 
		</div>
		<br>
	</div>
	
	<div ng-show='DATA.refund'>
		<div class="sub-title"> Refund Information</div>
		<br />
		<div class="table-responsive">
			<table class="table table-bordered table-payment">
				<tr class="info" style="font-weight:bold">
					<td align="center" width="100">Date</td>
					<td align="center" width="130">Order#</td>
					<td>Description</td>
					<td width="100" align="center">Status</td>
					<td align="right" colspan="2">Amount</td>
					<?php /*?><td width="40"></td><?php */?>
				</tr>
				<tr ng-repeat='refund in DATA.refund.refund.detail'>
					<td align="center">{{fn.formatDate(refund.request_date, "dd M yy")}}</td>
					<td align="center">{{refund.refund_code}}</td>
					<td>{{refund.description}}</td>
					<td align="center">
						<span class="label label-warning" ng-show="refund.status!='1'">Pending</span>
						<span class="label label-success" ng-show="refund.status=='1'">Refunded</span>
					</td>
					<td align="center" width="50">{{refund.currency}}</td>
					<td align="right" width="110">
						{{fn.formatNumber(refund.refund_amount, refund.currency)}}
					</td>
					<?php /*?><td align="center">
						<a href="" ng-click="paymentDetail(payment, true)" data-toggle="modal" data-target="#payment-detail" class="delete-icon" ng-show="payment.id != ''">
							<span class="glyphicon glyphicon-trash"></span>
						</a>
					</td><?php */?>
				</tr>
				<tr>
					<td colspan="6"></td>
				</tr>
				<tr class="warning" style="font-weight:bold">
					<td colspan="4" align="right">Total Refund</td>
					<td>{{DATA.refund.refund.currency}}</td>
					<td align="right">{{fn.formatNumber(DATA.refund.refund.total_refund, DATA.refund.refund.currency)}}</td>
					<?php /*?><td></td><?php */?>
				</tr>
			</table>
		</div>
		
	</div>
		
	<div class="modal fade" id="add-edit-payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	  <form ng-submit='saveDataPayment($event)'>
	  <div class="modal-dialog" role="document">

		<div class="modal-content">

		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">
				<span ng-show='!DATA.myPayment.id'>Add</span><span ng-show='DATA.myPayment.id'>Edit</span> Payment
			</h4>
		  </div>
		  <div class="modal-body">
		  	<div ng-show='DATA.myPayment.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myPayment.error_msg'>{{err}}</li></ul></div>
			<table class="table table-borderless table-condenseds">
				<tr>
					<td width="130">Order</td>
					<td><strong>#{{DATA.current_booking.booking.booking_code}}</strong></td>
				</tr>
				<tr>
					<td>Payment Date*</td>
					<td><input placeholder="Payment Date" required="required" type="text" class="form-control input-md datepicker" ng-model='DATA.myPayment.date' style="width:150px" /></td>
				</tr>
                
                <?php //Payment With Deposit --------------------- ?>
				<tbody style="border-top:none" ng-show="DATA.myPayment.payment_type == 'DEPOSIT'">
                    <tr>
                        <td>Current Deposit</td>
                        <td>
                            <div class="input-group">
                                <span class="input-group-addon" style="width:60px">{{DATA.myPayment.deposit.currency}}</span>
                                <input placeholder="Current Deposit" disabled="disabled" class="form-control input-md payment_amount" value="{{fn.formatNumber(DATA.myPayment.deposit.current_deposit, DATA.myPayment.deposit.currency)}}" style="width:130px; text-align:right" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Payment Amount</td>
                        <td>
                            <div class="input-group">
                                <span class="input-group-addon" style="width:60px">{{DATA.current_booking.booking.currency}}</span>
                                <input placeholder="Current Deposit" disabled="disabled" class="form-control input-md payment_amount" value="{{fn.formatNumber(DATA.myPayment.amount, DATA.current_booking.booking.currency)}}" style="width:130px; text-align:right" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Deposit Remaining </td>
                        <td>
                            <div class="input-group">
                                <span class="input-group-addon" style="width:60px">{{DATA.myPayment.deposit.currency}}</span>
                                <input placeholder="Current Deposit" disabled="disabled" class="form-control input-md payment_amount" value="{{fn.formatNumber((DATA.myPayment.deposit.current_deposit - DATA.myPayment.amount), DATA.myPayment.deposit.currency)}}" style="width:130px; text-align:right" />
                            </div>
                            <div style="margin-top:10px">
	                            <a href="" ng-click="DATA.myPayment.payment_type = ''"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
                        	</div>
                        </td>
                    </tr>
                    <tr>
                        <td>Remarks*</td>
                        <td><input placeholder="Description" required="required" type="text" class="form-control input-md" ng-model='DATA.myPayment.description' /></td>
                    </tr>
                </tbody>
                
                <?php //Payment With Other than Doposit --------------------- ?>
                <tbody style="border-top:none" ng-hide="DATA.myPayment.payment_type == 'DEPOSIT'">
                	<tr ng-show='DATA.myPayment.deposit'>
                        <td></td>
                        <td>
                            <button type="button" class="btn btn-danger" ng-click="addEditPaymentPayWithDeposit()">
                                <i class="fa fa-plus"></i> Pay with Deposit ({{DATA.myPayment.deposit.currency}} {{fn.formatNumber(DATA.myPayment.deposit.current_deposit, DATA.myPayment.deposit.currency)}})
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>Payment Type*</td>
                        <td>
                            <select required="required" class="form-control input-md" ng-model='DATA.myPayment.payment_type' ng-change='changePaymentType()'>
                                <option value="" disabled="disabled">-- Select Payment Type --</option>
                                <option value="DEPOSIT" ng-show='DATA.myPayment.deposit'>
                                    Pay with Deposit ({{DATA.myPayment.deposit.currency}} {{fn.formatNumber(DATA.myPayment.deposit.current_deposit, DATA.myPayment.deposit.currency)}})
                                </option>
                                <option value="{{payment_method.code}}" ng-repeat='payment_method in $root.DATA_payment_method'
                                    <?php /*?>ng-hide="payment_method.code=='OPENVOUCHER' && DATA.current_booking.booking.booking_type == 'OPENVOUCHER'"><?php */?>
                                    ng-hide="payment_method.code=='OPENVOUCHER' || payment_method.code=='ONLINE'">
                                    <div ng-hide="payment_method.name == 'Deposit'">{{payment_method.name}}</div>
                                </option>
                                <?php /*<option value="ACL" ng-show='agent_allow_to_use_acl' ng-disabled="DATA.myPayment.amount > DATA.current_booking.booking.agent.out_standing_invoice.current_limit">
                                    Agent Credit Limit
                                    (Maksimum : {{DATA.current_booking.booking.agent.out_standing_invoice.currency}} {{fn.formatNumber(DATA.current_booking.booking.agent.out_standing_invoice.current_limit,DATA.current_booking.booking.agent.out_standing_invoice.currency)}})
                                    {{(DATA.myPayment.amount > DATA.current_booking.booking.agent.out_standing_invoice.current_limit)?<?="'<-- Cannot use ACL (Over Limit)'"?>:""}}
                                </option> */ ?>
                            </select>
                            <?php /*?><div ng-show='error_deposit' class="alert alert-danger">Insufficient Deposit</div>
                            <div ng-show="DATA.myPayment.payment_type == 'DEPOSIT'" class="text-left" style="padding: 10px 0;">
                                <table class="table table-bordered table-payment" style="padding: 10px;">
                                    <tr ng-show="DATA.myPayment.deposit.current_deposit" ng-class="{'danger':(DATA.myPayment.deposit.current_deposit<DATA.myPayment.amount), 'info':(DATA.myPayment.deposit.current_deposit>=DATA.myPayment.amount)}">
                                        <td width="130"><strong>Deposit Remaining</strong></td>
                                        <td><strong> : {{DATA.myPayment.deposit.currency}} {{fn.formatNumber(DATA.myPayment.deposit.current_deposit, DATA.myPayment.deposit.currency)}}</strong></td>
                                    </tr>
                                    <tr ng-hide="DATA.myPayment.deposit.current_deposit">
                                        <div ng-hide="DATA.myPayment.deposit.current_deposit">
                                            <img src="<?=base_url("public/images/loading_bar.gif")?>" />
                                        </div>
                                    </tr>
                                </table>
                            </div><?php */?>
                        </td>
                    </tr>
                    <tr ng-show="DATA.myPayment.payment_type=='OPENVOUCHER' && DATA.current_booking.booking.booking_type != 'OPENVOUCHER'" class="header">
                        <td>Open Voucher Code*</td>
                        <td>
                            <table width="100%">
                                <tr ng-repeat='ov_code in DATA.myPayment.openvoucher_code' class="form-inline">
                                    <td width="30">{{($index+1)}}.</td>
                                    <td>
                                        <input ng-required="DATA.myPayment.payment_type=='OPENVOUCHER'" placeholder="Open Voucher Code {{($index+1)}}" type="text" class="form-control input-sm" ng-model='ov_code.code' style="width:200px" ng-blur="checkValidateOpenVoucherCode(ov_code)" />
                                        <span ng-show="ov_code.valid" style="color:green" class="glyphicon glyphicon-ok"></span>
                                        <span ng-show="ov_code.invalid" style="color:red" class="glyphicon glyphicon-remove"></span>
                                        <span ng-show="ov_code.loading"><small><em>Loading...</em></small></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <?php /*?><tr ng-show="DATA.myPayment.payment_type=='CC' || DATA.myPayment.payment_type=='ATM'" class="header"><?php */?>
                    <tr ng-show="DATA.payment_is_cc" class="header">
                        <td>
                            <span ng-show="DATA.payment_is_cc">Card Number</span>
                            <span ng-show="DATA.payment_is_atm">Account Number</span>
                        </td>
                        <td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.myPayment.account_number' /></td>
                    </tr>
                    <?php /*?><tr ng-show="DATA.myPayment.payment_type=='CC' || DATA.myPayment.payment_type=='ATM'" class="header"><?php */?>
                    <tr ng-show="DATA.payment_is_cc" class="header">
                        <td>Name On Card</td>
                        <td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.myPayment.name_on_card' /></td>
                    </tr>
                    <?php /*?><tr ng-show="DATA.myPayment.payment_type=='CC' || DATA.myPayment.payment_type=='ATM'" class="header"><?php */?>
                    <tr ng-show="DATA.payment_is_cc" class="header">
                        <td>Bank Name</td>
                        <td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.myPayment.bank_name' /></td>
                    </tr>
                    <?php /*?><tr ng-show="DATA.myPayment.payment_type=='CC' || DATA.myPayment.payment_type=='ATM'" class="header"><?php */?>
                    <tr ng-show="DATA.payment_is_cc" class="header">
                        <td>Approval Number</td>
                        <td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.myPayment.payment_reff_number' /></td>
                    </tr>
                    <tr>
                        <td>Remarks*</td>
                        <td><input placeholder="Description" required="required" type="text" class="form-control input-md" ng-model='DATA.myPayment.description' /></td>
                    </tr>
                    <tr ng-show="DATA.myPayment.payment_type!='OPENVOUCHER'">
                        <td>Payment Amount*</td>
                        <td>
                            <div class="input-group">
                                <span class="input-group-addon" style="width:80px">{{DATA.current_booking.booking.currency}}</span>
                                <input placeholder="Payment Amount" required="required" type="number" min="0" step='any' class="form-control input-md payment_amount" ng-model='DATA.myPayment.amount' style="width:160px" xxng-disabled="DATA.current_booking.booking.agent.payment_type_code == 'ACL'" ng-blur="convert_currency()" />
                            </div>
                        </td>
                    </tr>
                    <tr ng-show="DATA.myPayment.payment_type!='OPENVOUCHER' && $root.DATA_available_currency.currency">
                        <td>Paid In*</td>
                        <td>
                            <div class="input-group" ng-show='$root.DATA_available_currency.currency'>
                                <select class="form-control input-md" ng-model='DATA.myPayment.payment_currency' style="width:80px" ng-change="convert_currency()" ng-disabled="!$root.DATA_available_currency.currency">
                                    <option ng-repeat="crr in $root.DATA_available_currency.currency" value="{{crr}}">{{crr}}</option>
                                </select>
                                <input placeholder="Payment Amount" disabled="disabled" type="number" min="0" step="0.1" class="form-control input-md payment_amount" ng-model='DATA.myPayment.payment_amount' style="width:160px" />
                            </div>
                            <div ng-show='DATA.currency_converter.bookkeeping_rates' style="margin:10px 0">
                                {{DATA.currency_converter.bookkeeping_rates.from.currency}} {{DATA.currency_converter.bookkeeping_rates.from.amount}} = 
                                {{DATA.currency_converter.bookkeeping_rates.to.currency}} {{DATA.currency_converter.bookkeeping_rates.to.amount}}
                            </div>
                            <em ng-show='DATA.myPayment.payment_currency_loading'>Loading...</em>
                        </td>
                    </tr>
                </tbody>
                
			</table>
			
		  </div>
		  <div class="modal-footer" style="text-align:center">
		  	<button type="submit" class="btn btn-primary">Save</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
	  </div>
	  </form>
	</div>
	
	<div class="modal fade" id="payment-detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">
				<span ng-show="myPaymentDetail.payment_type_code">Payment</span> 
				<span ng-show="!myPaymentDetail.payment_type_code">Reservation</span> Detail #{{myPaymentDetail.payment_code}}
			</h4>
		  </div>
		  <div class="modal-body">
		  	
			<table class="table table-borderless table-condensed">
				<tr>
					<td width="130">Order</td>
					<td><strong>#{{DATA.current_booking.booking.booking_code}}</strong></td>
				</tr>
				<tr>
					<td>Payment Date</td>
					<td>{{fn.formatDate(myPaymentDetail.date, "d MM yy")}}</td>
				</tr>
				<tr>
					<td>Payment Type</td>
					<td>{{myPaymentDetail.payment_type}}</td>
				</tr>
				<tr ng-show="myPaymentDetail.payment_type=='CC' || myPaymentDetail.payment_type=='ATM'">
					<td><span ng-show="myPaymentDetail.payment_type=='CC'">Credit Card Number</span><span ng-show="myPaymentDetail.payment_type=='ATM'">Account Number</span></td>
					<td>{{myPaymentDetail.account_number}}</td>
				</tr>
				<tr ng-show="myPaymentDetail.payment_type=='CC' || myPaymentDetail.payment_type=='ATM'">
					<td>Name On Card</td>
					<td>{{myPaymentDetail.name_on_card}}</td>
				</tr>
				<tr ng-show="myPaymentDetail.payment_type=='CC' || myPaymentDetail.payment_type=='ATM'">
					<td><span ng-show="myPaymentDetail.payment_type=='CC'">EDC Machine</span><span ng-show="myPaymentDetail.payment_type=='ATM'">Bank Name</span></td>
					<td>{{myPaymentDetail.bank_name}}</td>
				</tr>
				<tr ng-show="myPaymentDetail.payment_type=='CC' || myPaymentDetail.payment_type=='ATM'">
					<td>Approval Number</td>
					<td>{{myPaymentDetail.payment_reff_number}}</td>
				</tr>
				<tr ng-show="myPaymentDetail.payment_type_code=='OPENVOUCHER'">
					<td>Pre Paid Ticket Code</td>
					<td>{{myPaymentDetail.payment_reff_number}}</td>
				</tr>
				<tr>
					<td>Description</td>
					<td>{{myPaymentDetail.description}}</td>
				</tr>
				<tr>
					<td>Payment Amount</td>
					<td>{{myPaymentDetail.currency}} {{fn.formatNumber(myPaymentDetail.amount, myPaymentDetail.currency)}}</td>
				</tr>
				
			</table>
			  <div ng-show='myPaymentDetail.delete_payment' class="alert alert-warning">
					<form ng-submit='submitDeletePayment($event)'>
						<strong>Are you sure to delete this payment?</strong><br />
						<label><input type="radio" name="delete_payment_confirmation" value="1" ng-model='myPaymentDetail.sure_to_delete' /> Yes</label>
						&nbsp;&nbsp;
						<label><input type="radio" name="delete_payment_confirmation" value="0" ng-model='myPaymentDetail.sure_to_delete' /> No</label>
						<div ng-show="myPaymentDetail.sure_to_delete=='1'">
							<hr style="margin:5px 0" />
							
							<div ng-show='myPaymentDetail.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in myPaymentDetail.error_msg'>{{err}}</li></ul></div>
							
							<strong>Remarks:</strong>
							<textarea rows='2' placeholder="Remarks" class="form-control input-md autoheight" ng-model='myPaymentDetail.delete_remarks' required="required"></textarea>
							<br />
							<button type="submit" class="btn btn-primary">Delete Payment</button>
						</div>
					</form>
				</div>
		  </div>
		  
		  <div class="modal-footer" style="text-align:center">
			<span ng-show="myPaymentDetail.payment_type_code">
				<a style="cursor: pointer;" href="<?=site_url("home/print_page/#/print/receipt_trans_payment/")?>{{myPaymentDetail.payment_code}}" target="_blank" ng-show="payment.id != ''">
					<button type="button"  class="btn btn-primary">
					<small><span class="glyphicon glyphicon-print"></span>
					 &nbsp;Print Receipt</small>
					</button>
				</a>
			</span> 
			<span ng-show="!myPaymentDetail.payment_type_code">
				<a href="<?=site_url("home/print_page/#/print/receipt_trans/")?>{{DATA.current_booking.booking.booking_code}}" target="_blank">
					<button type="button"  class="btn btn-primary">
					<small><span class="glyphicon glyphicon-print"></span>
					 &nbsp;Print <span ng-show="DATA.current_booking.booking.status_code == 'UNDEFINITE'">Proforma</span> Invoice</small>
					</button>
				</a>
			</span>
			

			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
	  </div>
	</div>
	
</div>
<script>activate_sub_menu_agent_detail("payment");</script>
<style>
	table.table-payment .delete-icon{opacity:0.3; color:red}
	table.table-payment a:hover.delete-icon{opacity:1;}
</style>

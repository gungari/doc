<div class="modal fade" id="modal-add-trip" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Edit Voucher</h4>
		  </div>
		  <div class="modal-body products">
			<div class="product">
				<div class="pull-right text-right" ng-hide='current_edit_trip.is_packages'>
					<small>Adult:</small> <strong>{{current_edit_trip.rates.currency}} {{fn.formatNumber(current_edit_trip.rates.rates_1, current_edit_trip.rates.currency)}}</strong><br />
					<small>Child:</small> <strong>{{current_edit_trip.rates.currency}} {{fn.formatNumber(current_edit_trip.rates.rates_2, current_edit_trip.rates.currency)}}</strong><br />
					<small>Infant:</small> <strong>{{current_edit_trip.rates.currency}} {{fn.formatNumber(current_edit_trip.rates.rates_3, current_edit_trip.rates.currency)}}</strong><br />
				</div>
				<div>
                    <strong>{{current_edit_trip.product.name}}</strong>
                </div>
				<div><small>({{current_edit_trip.rates.name}})</small></div>
				<div><strong>{{fn.formatDate(current_edit_trip.date, "dd MM yy")}}</strong></div>
                
                <div ng-show='current_edit_trip.is_packages'>
                    <div>
                    	{{current_edit_trip.packages.qty}} x {{current_edit_trip.packages.name}} 
                        (
                        <span ng-show='current_edit_trip.packages.include_pax.opt_1'>{{current_edit_trip.packages.include_pax.opt_1}} Adult</span>
                        <span ng-show='current_edit_trip.packages.include_pax.opt_2'>{{current_edit_trip.packages.include_pax.opt_2}} Child</span>
                        <span ng-show='current_edit_trip.packages.include_pax.opt_3'>{{current_edit_trip.packages.include_pax.opt_3}} Infant</span>
                        )
                        @{{DATA.current_booking.booking.currency}} {{fn.formatNumber(current_edit_trip.packages.rates,DATA.current_booking.booking.currency)}}
                    </div>
                    <div ng-show='current_edit_trip.packages.additional.opt_1 || current_edit_trip.packages.additional.opt_2 || current_edit_trip.packages.additional.opt_3'>
                        <strong>Additional:</strong><br />
                        <span ng-show='current_edit_trip.packages.additional.opt_1'>Adult : {{current_edit_trip.packages.additional.opt_1}} @{{DATA.current_booking.booking.currency}} {{fn.formatNumber(current_edit_trip.packages.extra_rates.opt_1,DATA.current_booking.booking.currency)}}</span><br />
                        <span ng-show='current_edit_trip.packages.additional.opt_2'>Child : {{current_edit_trip.packages.additional.opt_2}} @{{DATA.current_booking.booking.currency}} {{fn.formatNumber(current_edit_trip.packages.extra_rates.opt_2,DATA.current_booking.booking.currency)}}</span><br />
                        <span ng-show='current_edit_trip.packages.additional.opt_3'>Infant : {{current_edit_trip.packages.additional.opt_3}} @{{DATA.current_booking.booking.currency}} {{fn.formatNumber(current_edit_trip.packages.extra_rates.opt_3,DATA.current_booking.booking.currency)}}</span>
                    </div>
                </div>

				<div ng-hide='current_edit_trip.is_packages'>
					<span ng-show='current_edit_trip.qty_1 > 0'>{{current_edit_trip.qty_1}} Adult</span>
					<span ng-show='current_edit_trip.qty_2 > 0'>{{current_edit_trip.qty_2}} Child</span>
					<span ng-show='current_edit_trip.qty_3 > 0'>{{current_edit_trip.qty_3}} Infant</span>
				</div>
				
                <hr />
				<form ng-submit='check_availabilities_edit_trip($event)'>
					<?php /*?><div ng-show='add_trip.error_desc.length>0' class="alert alert-danger"><ul><li ng-repeat='err in add_trip.error_desc'>{{err}}</li></ul></div><?php */?>
                    <table class="table table-borderless table-condensed">
                    	<tr>
                        	<td width="110">Change Schedule</td>
                            <td>
                            	<input type="text" class="form-control input-sm datepicker" required="required" placeholder="Select Date" ng-model='add_trip.date' style="display:inline; width:150px" />
                                <span ng-show='!add_trip.change_trip && !add_trip.is_change_participant'>
                                	&nbsp;&nbsp;
                                	<button type="submit" tabindex="10" class="btn btn-success btn-sm">Check Availability</button>
                                </span>
                            </td>
                        </tr>
                        <tr>
                        	<td colspan="2">
                            	<hr style="margin:10px 0" />
                                <label>
                            		<input type="checkbox" ng-true-value='1' ng-false-value='0' ng-model='add_trip.is_change_participant' />
                            		Update Participant
                                </label>
                                <br />
                                <em style="color:red">To reduce participant quantity, plesae use <a ui-sref="reservation.detail({'booking_code':DATA.current_booking.booking.booking_code})" target="_blank">cancellation feature</a></em>
                            </td>
                        </tr>
                        <tr ng-show='add_trip.is_change_participant && !current_edit_trip.is_packages'>
                            <td class="text-right">Adult</td>
                            <td><input type="number" class="form-control input-sm" min="{{add_trip.min_participant.qty_1}}" ng-model='add_trip.participant.qty_1' min="0" style="width:100px" /></td>
                        </tr>
                        <tr ng-show='add_trip.is_change_participant && !current_edit_trip.is_packages'>
                            <td class="text-right">Child</td>
                            <td><input type="number" class="form-control input-sm" min="{{add_trip.min_participant.qty_2}}" ng-model='add_trip.participant.qty_2' min="0" style="width:100px" /></td>
                        </tr>
                        <tr ng-show='add_trip.is_change_participant && !current_edit_trip.is_packages'>
                            <td class="text-right">Infant</td>
                            <td><input type="number" class="form-control input-sm" min="{{add_trip.min_participant.qty_3}}" ng-model='add_trip.participant.qty_3' min="0" style="width:100px" /></td>
                        </tr>
                        <tr ng-show='add_trip.is_change_participant && !current_edit_trip.is_packages'>
                        	<td colspan="2" align="center">
                            	<hr style="margin:10px 0" />
                            </td>
                        </tr>
                        <tr ng-show='add_trip.is_change_participant && !current_edit_trip.is_packages'>
                        	<td></td>
                            <td>
                            	<span ng-show='!add_trip.change_trip && add_trip.is_change_participant'>
                                	&nbsp;&nbsp;
                                	<button type="submit" class="btn btn-success btn-sm">Check Availability</button>
                                </span>
                            </td>
                        </tr>
                        
                        <tr ng-show='add_trip.is_change_participant && current_edit_trip.is_packages'>
                        	<td colspan="2" class="form-inline">
                            	<hr style="margin:10px 0" />
                                
                                <table class="table table-condensed table-borderless" style="background:none">
                                	<tr>
                                    	<td colspan="2">
                                            {{current_edit_trip.packages.name}} 
                                            (
                                            <span ng-show='current_edit_trip.packages.include_pax.opt_1'>{{current_edit_trip.packages.include_pax.opt_1}} Adult</span>
                                            <span ng-show='current_edit_trip.packages.include_pax.opt_2'>{{current_edit_trip.packages.include_pax.opt_2}} Child</span>
                                            <span ng-show='current_edit_trip.packages.include_pax.opt_3'>{{current_edit_trip.packages.include_pax.opt_3}} Infant</span>
                                            )
                                            :
                                            <input type="number" class="form-control input-sm" min="{{add_trip.participant_packages.min_participant.qty}}" ng-model='add_trip.participant_packages.qty' min="0" style="width:100px" />
                                        </td>
                                    </tr>
                                	<tr>
                                    	<td colspan="2"><strong>Additional:</strong></td>
                                    </tr>
                                    <tr>
                                    	<td width="70" align="right">Adult</td>
                                        <td>
                                        	<input type="number" class="form-control input-sm" min="{{add_trip.participant_packages.min_participant.additional.opt_1}}" ng-model='add_trip.participant_packages.additional.opt_1' min="0" style="width:100px" />
                                        </td>
                                    </tr>
                                    <tr>
                                    	<td align="right">Child</td>
                                        <td>
                                        	<input type="number" class="form-control input-sm" min="{{add_trip.participant_packages.min_participant.additional.opt_2}}" ng-model='add_trip.participant_packages.additional.opt_2' min="0" style="width:100px" />
                                        </td>
                                    </tr>
                                    <tr>
                                    	<td align="right">Infant</td>
                                        <td>
                                        	<input type="number" class="form-control input-sm" min="{{add_trip.participant_packages.min_participant.additional.opt_3}}" ng-model='add_trip.participant_packages.additional.opt_3' min="0" style="width:100px" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr ng-show='add_trip.is_change_participant && current_edit_trip.is_packages'>
                        	<td colspan="2" align="center">
                            	<hr style="margin:10px 0" />
                            	<span ng-show='!add_trip.change_trip && add_trip.is_change_participant'>
                                	&nbsp;&nbsp;
                                	<button type="submit" class="btn btn-success btn-sm">Check Availability</button>
                                </span>
                            </td>
                        </tr>
                        
                        <?php /*?><tr ng-show='add_trip.change_trip'>
                        	<td>Change Trip</td>
                            <td>
                            	<div style="margin-bottom:10px">
                                    <strong>From</strong><br />
                                    <select tabindex="3" class="form-control input-sm departure-port" ng-model='add_trip.departure_port_id'>
                                        <option value="{{port.id}}" ng-repeat='port in $root.DATA_available_port'>{{port.port_code}} - {{port.name}}</option>
                                    </select>
                                </div>
                                
                                <div>
                                    <strong>To</strong><br />
                                    <select tabindex="3" class="form-control input-sm arrival-port" ng-model='add_trip.arrival_port_id'>
                                        <option value="{{port.id}}" ng-repeat='port in $root.DATA_available_port'>{{port.port_code}} - {{port.name}}</option>
                                    </select>
                            	</div>
                            </td>
                        </tr>
                        <tr ng-show='add_trip.change_trip'>
                        	<td></td>
                            <td>
                            	<button type="submit" tabindex="10" class="btn btn-success btn-sm">Check Availability</button>
                                <a href="" ng-click='add_trip.change_trip = !add_trip.change_trip'>Cancel</a>
                            </td>
                        </tr>
                        <tr ng-show='!add_trip.change_trip'>
                        	<td></td>
                            <td>
                            	<a href="" ng-click='add_trip.change_trip = !add_trip.change_trip'>Change Trip</a>
                            </td>
                        </tr><?php */?>
                    </table>
				</form>
			</div>

			<div class="text-center" ng-show='check_availabilities_show_loading'>
				<strong><em>Loading...</em></strong>
			</div>
			<div ng-show='add_trip.error_desc.length'>
				<div class="alert alert-warning">
					<strong>Sorry</strong>, Not available, please select another date...
				</div>
			</div>
			
			<div class="product search-trip-result" ng-show='check_availabilities_data.availabilities'>
            
				<div class="pull-right">
					<strong>{{fn.formatDate(check_availabilities_data.check.date, "dd MM yy")}}</strong>
				</div>
                <div>
					<strong></strong>
				</div>
                <hr style="margin:5px 0; clear:both" />
				<div class="trip" 
                	ng-class="{'selected':availabilities.is_selected,
                    			'availability-rates-highlighted':(	availabilities.rates.rates_1==current_edit_trip.rates.rates_1 &&
                                									availabilities.rates.rates_2==current_edit_trip.rates.rates_2 &&
                                                                    availabilities.rates.rates_3==current_edit_trip.rates.rates_3)}" 
                	ng-show='!availabilities.is_hidden' 
                	ng-repeat="availabilities in check_availabilities_data.availabilities" 
                    ng-click="check_availabilities_select_this_product(availabilities, check_availabilities_data)">
					<div class="media">
					  <div class="media-left">
						<a href="#">
						  <img class="media-object" ng-src="{{add_trip.products.main_image}}" width="80px" />
						</a>
					  </div>
					  <div class="media-body">
					  	<?php /*?><p class="pull-right text-right">
							<small>Adult:</small> <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_1, availabilities.currency)}}</strong><br />
							<small>Child:</small> <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_2, availabilities.currency)}}</strong><br />
							<small>Infant:</small> <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_3, availabilities.currency)}}</strong><br />
						</p><?php */?>
                        
                        <strong>{{availabilities.name}}</strong> ({{availabilities.calendar.allotment}} space left)<br />
						<small><i class="fa fa-car" aria-hidden="true"></i> 
							<i class="fa fa-check" aria-hidden="true" ng-show="availabilities.pickup_service == 'yes'"></i> <i class="fa fa-remove" aria-hidden="true" ng-show="availabilities.pickup_service != 'yes'" style="color:red"></i> Pickup 
							&nbsp;
							<i class="fa fa-check" aria-hidden="true" ng-show="availabilities.dropoff_service == 'yes'"></i> <i class="fa fa-remove" aria-hidden="true" ng-show="availabilities.dropoff_service != 'yes'" style="color:red"></i> Dropoff
						</small><br />
					  	
                        <?php //NORMAL RATES ?>
                        <div ng-show='!availabilities.is_packages'>
                        	<table class="table table-condensed table-borderless">
                            	<?php for($i=1;$i<=5;$i++){ ?>
                            	<tr ng-show='availabilities.rates.rates_<?=$i?> > 0'>
                                	<td width="100">{{availabilities.rates_caption.rates_<?=$i?>}}</td>
                                    <td width="10">:</td>
                                    <td width="150"><strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.rates.rates_<?=$i?>, availabilities.currency)}}</strong> <small>/{{availabilities.rates_unit.rates_<?=$i?>}}</small> </td>
                                    <?php /*?><td width="10"  ng-show='availabilities.is_selected'>x</td>
                                    <td width="100" ng-show='availabilities.is_selected' style="padding:0">
										<select <?=(($i==1)?'required="required"':'')?> class="form-control input-sm" placeholder="Qty." 
                                        	style="height:25px !important" ng-disabled="!availabilities.is_selected"
                                            ng-model='check_availabilities_data.selected_rates.selected_qty_<?=$i?>'>
                                            <option value="{{x.qty}}" ng-repeat="x in add_product.product.arr_qty.qty_<?=$i?>">{{x.qty}}</option>
                                        </select>
                                    </td><?php */?>
                                    <td></td>
                                </tr>
                                <?php } ?>
                            </table>
                        </div>
                        
                        <?php //PACKAGES ?>
                        <div ng-show='availabilities.is_packages'>
                        	<table class="table table-condensed table-borderless tbl-packages-popup">
                            	<tbody ng-repeat="packages_option in availabilities.packages_option">
                                	<tr>
                                    	<td colspan="4">
                                        	<div>
                                                <strong>{{packages_option.caption}}</strong>
                                                -
                                                <strong>{{availabilities.currency}} {{fn.formatNumber(packages_option.rates, availabilities.currency)}}</strong> <small>/{{packages_option.rates_unit}}</small>
                                        	</div>
                                            <div>
                                            	Inc. : 	<span ng-show='packages_option.include_pax.opt_1'><strong><span class="glyphicon glyphicon-menu-right"></span> {{packages_option.include_pax.opt_1}}</strong> Adult(s)</span>
														<span ng-show='packages_option.include_pax.opt_2'><strong><span class="glyphicon glyphicon-menu-right"></span> {{packages_option.include_pax.opt_2}}</strong> Child(s)</span>
														<span ng-show='packages_option.include_pax.opt_3'><strong><span class="glyphicon glyphicon-menu-right"></span> {{packages_option.include_pax.opt_3}}</strong> Infant(s)</span>
                                            </div>
                                            <div>
                                            	Additional : <span ng-show='packages_option.extra_rates.opt_1'><strong><span class="glyphicon glyphicon-menu-right"></span> {{availabilities.currency}} {{fn.formatNumber(packages_option.extra_rates.opt_1, availabilities.currency)}}</strong>/Adult</span>
															 <span ng-show='packages_option.extra_rates.opt_2'><strong><span class="glyphicon glyphicon-menu-right"></span> {{availabilities.currency}} {{fn.formatNumber(packages_option.extra_rates.opt_2, availabilities.currency)}}</strong>/Child</span>&nbsp;
															 <span ng-show='packages_option.extra_rates.opt_3'><strong><span class="glyphicon glyphicon-menu-right"></span> {{availabilities.currency}} {{fn.formatNumber(packages_option.extra_rates.opt_3, availabilities.currency)}}</strong>/Infant</span>&nbsp;
                                            </div>
                                        </td>
                                    </tr>
                                    <?php /*?><tr ng-show='availabilities.is_selected'>
                                        <td width="50">Qty.</td>
                                        <td width="10">:</td>
                                        <td ng-show='availabilities.is_selected' style="padding:0">
                                            <input <?=(($i==1)?'required="required"':'')?> type="number" step="1" min='0' max="{{availabilities.calendar.allotment}}" 
                                                class="form-control input-sm" placeholder="Qty." style="height:25px !important; display:inline; width:75px" ng-disabled="!availabilities.is_selected"
                                                ng-model='packages_option.qty' /> {{packages_option.rates_unit}}
                                        </td>
                                        <td></td>
                                	</tr>
                                    <tr ng-show='availabilities.is_selected && packages_option.extra_rates.opt_1'>
                                    	<td colspan="4">
                                        	<strong>Additional</strong>
                                        </td>
                                    </tr>
                                    <tr ng-show='availabilities.is_selected && packages_option.extra_rates.opt_1'>
                                        <td width="50">Adult</td>
                                        <td width="10">:</td>
                                        <td ng-show='availabilities.is_selected' style="padding:0">
                                            <input <?=(($i==1)?'required="required"':'')?> type="number" step="1" min='0' max="{{availabilities.calendar.allotment}}" 
                                                class="form-control input-sm" placeholder="Adult" style="height:25px !important; display:inline; width:75px" 
                                                ng-disabled="!(availabilities.is_selected && packages_option.extra_rates.opt_1 && packages_option.qty)"
                                                ng-model='packages_option.additional_qty_opt_1' /> Pax
                                        </td>
                                        <td></td>
                                	</tr>
                                    <tr ng-show='availabilities.is_selected && packages_option.extra_rates.opt_2'>
                                        <td width="50">Child</td>
                                        <td width="10">:</td>
                                        <td ng-show='availabilities.is_selected' style="padding:0">
                                            <input <?=(($i==1)?'required="required"':'')?> type="number" step="1" min='0' max="{{availabilities.calendar.allotment}}" 
                                                class="form-control input-sm" placeholder="Child" style="height:25px !important; display:inline; width:75px" 
                                                ng-disabled="!(availabilities.is_selected && packages_option.extra_rates.opt_2 && packages_option.qty)"
                                                ng-model='packages_option.additional_qty_opt_2' /> Pax
                                        </td>
                                        <td></td>
                                	</tr>
                                    <tr ng-show='availabilities.is_selected && packages_option.extra_rates.opt_3'>
                                        <td width="50">Infant</td>
                                        <td width="10">:</td>
                                        <td ng-show='availabilities.is_selected' style="padding:0">
                                            <input <?=(($i==1)?'required="required"':'')?> type="number" step="1" min='0' max="{{availabilities.calendar.allotment}}" 
                                                class="form-control input-sm" placeholder="Infant" style="height:25px !important; display:inline; width:75px" 
                                                ng-disabled="!(availabilities.is_selected && packages_option.extra_rates.opt_3 && packages_option.qty)"
                                                ng-model='packages_option.additional_qty_opt_3' /> Pax
                                        </td>
                                        <td></td>
                                	</tr><?php */?>
                                    <tr>
                                    	<td colspan="4"><hr style="margin:0" /></td>
                                    </tr>
                                </tbody>
                            </table>
                            <style>
                            	.tbl-packages-popup tbody, .tbl-packages-popup tbody tr{border:none !important}
                            </style>
                        </div>
                        
					  	<?php /*?><i class="fa fa-clock-o"></i> <strong>{{availabilities.departure.time}} - {{availabilities.arrival.time}}</strong> ({{availabilities.schedule.boat.available_space}} space left)<br />
						<small><i class="fa fa-ship"></i> 	<strong>{{availabilities.schedule.boat.name}} ({{availabilities.schedule.boat.capacity}} seats)</strong></small><br />
						<small><i class="fa fa-dollar"></i> &nbsp; {{availabilities.name}}</small><br />
						<small><i class="fa fa-car" aria-hidden="true"></i> 
							<i class="fa fa-check" aria-hidden="true" ng-show="availabilities.pickup_service == 'yes'"></i> <i class="fa fa-remove" aria-hidden="true" ng-show="availabilities.pickup_service != 'yes'" style="color:red"></i> Pickup 
							&nbsp;
							<i class="fa fa-check" aria-hidden="true" ng-show="availabilities.dropoff_service == 'yes'"></i> <i class="fa fa-remove" aria-hidden="true" ng-show="availabilities.dropoff_service != 'yes'" style="color:red"></i> Dropoff
						</small><br /><?php */?>
					  </div>
					</div>
				</div>
			</div>
			<div class="product" ng-show='check_availabilities_data.selected_rates && !check_availabilities_data.selected_rates.is_packages'>
                <strong>Rates ({{check_availabilities_data.selected_rates.currency}})</strong><br />
                Adult : <input type="number" min="0" step="any" class="text-right" style="width:100px" ng-model='check_availabilities_data.selected_rates.rates.rates_1' />
                Child : <input type="number" min="0" step="any" class="text-right" style="width:100px" ng-model='check_availabilities_data.selected_rates.rates.rates_2' />
                Infant : <input type="number" min="0" step="any" class="text-right"  style="width:100px" ng-model='check_availabilities_data.selected_rates.rates.rates_3' />
            </div>
            
            <div ng-show="check_availabilities_data.selected_rates && !check_availabilities_data.selected_rates.is_packages &&
            			!(	check_availabilities_data.selected_rates.rates.rates_1==current_edit_trip.rates.rates_1 &&
                            check_availabilities_data.selected_rates.rates.rates_2==current_edit_trip.rates.rates_2 &&
                            check_availabilities_data.selected_rates.rates.rates_3==current_edit_trip.rates.rates_3)"
				class='alert alert-warning'>
            	<strong>Different price are selected, do you want to proceed?</strong>
            </div>
			
			<div ng-show='update_voucher_trip_error_desc.error_desc.length>0' class="alert alert-danger"><ul><li ng-repeat='err in update_voucher_trip_error_desc.error_desc'>{{err}}</li></ul></div>
		  </div>	
		  <div class="modal-footer" style="text-align:center">
			<button type="button" class="btn btn-primary btn-submit-edit-trip" ng-show='check_availabilities_show_OK_button' ng-click='update_voucher_trip()'>Save</button>
			<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
	  </div>
</div>
<style>
.search-trip-result .trip{padding:5px}
.search-trip-result .trip.availability-rates-highlighted{background:#fff4bb;border:solid 1px #ffda8d;}
.search-trip-result .trip:hover{background:#d9edf7; cursor:pointer}
.search-trip-result .trip.selected{background:#a3cce0; border:none;}
</style>
<div class="pull-right"> <a ui-sref="trans_reservation.new" class="btn btn-success btn-md"> <span class="glyphicon glyphicon-plus"></span> New Reservation </a> </div>
<h1>Reservation</h1>

<div ui-view>
	<div ng-init="loadDataBookingTransport()">
		<div class="products">
			<div class="product">
				Filter : <input type="text" ng-model="filter_booking" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
			</div>
		</div>
		
		<div ng-show='!DATA.bookings'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		</div>
		
		<div ng-show='DATA.bookings.bookings'>
			<div class="table-responsive">
				<table class="table table-condensed table-bordered">
					<tr class="header bold">
						<td align="center" width="130">Order#</td>
						<td align="center" width="100">Date</td>
						<td>Customer Name</td>
						<td align="center" width="100">Source</td>
						<td align="center" width="100">Status</td>
						<td align="right" width="100">Amount</td>
					</tr>
					<tr ng-repeat='booking in DATA.bookings.bookings | filter:filter_booking' ng-class="{'danger':(booking.status_code=='CANCEL'), 'warning':(booking.status_code=='UNDEFINITE')}">
						<td align="center"><a ui-sref="trans_reservation.detail({'booking_code':booking.booking_code})"><strong>{{booking.booking_code}}</strong></a></td>
						<td align="center">{{fn.formatDate(booking.transaction_date, "dd M yy")}}</td>
						<td>
							<strong>{{booking.customer.first_name}} {{booking.customer.last_name}}</strong>
							<br />
							{{booking.customer.email}}
						</td>
						<td align="center" class="text-capitalize">{{booking.source.toLowerCase()}}</td>
						<td align="center" class="text-capitalize">{{booking.status.toLowerCase()}}</td>
						<td align="right">{{booking.currency}} {{fn.formatNumber(booking.grand_total, booking.currency)}}</td>
					</tr>
				</table>
			</div>
			
			<hr />
			<a href="<?=site_url("export_to_excel/trans_bookings")?>" target="_blank"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export To Excel</a>
			
		</div>
		<br />
		
		<div class="add-product-button"> <a ui-sref="trans_reservation.new" class="btn btn-success btn-lg btn-block"> <span class="glyphicon glyphicon-plus"></span> New Reservation </a> </div>
	</div>
</div>

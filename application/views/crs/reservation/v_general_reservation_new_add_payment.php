<!-- Modal -->
<div class="modal fade" id="mdl-add-payment-rsv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        	<form ng-submit="newRsvAddPaymentsToArray()">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Payment</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-borderless table-condenseds">
                        <tr>
                            <td width="150">Payment Type* </td>
                            <td>
                                <select class="form-control input-md" required='required' ng-model='DATA.myPayment.payment_type' ng-change='changePaymentTypeInNewBookingForm(DATA.myPayment)'>
                                    <option value="" disabled="disabled">-- Select Payment Type --</option>
                                    <option value="{{payment_method.code}}" ng-repeat='payment_method in $root.DATA_payment_method' ng-show="payment_method.code != 'ONLINE' && payment_method.code != 'OPENVOUCHER'">
                                        {{payment_method.name}}
                                    </option>
                                </select>
                            </td>
                        </tr>
                        <tr ng-show="DATA.payment_is_cc" class="header">
                            <td>
                                <span ng-show="DATA.payment_is_cc">Card Number</span>
                                <span ng-show="DATA.payment_is_atm">Account Number</span>
                            </td>
                            <td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.myPayment.account_number' /></td>
                        </tr>
                        <tr ng-show="DATA.myPayment.payment_is_cc" class="header">
                            <td>Name On Card</td>
                            <td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.myPayment.name_on_card' /></td>
                        </tr>
                        <?php /*?><tr ng-show="DATA.payment_is_cc" class="header">
                            <td>Bank Name</td>
                            <td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.data_rsv.payment.bank_name' /></td>
                        </tr><?php */?>
                        <tr ng-show="DATA.myPayment.payment_is_cc" class="header">
                            <td>Approval Number</td>
                            <td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.myPayment.payment_reff_number' /></td>
                        </tr>
                        <tr ng-show="DATA.myPayment.payment_type!='ACL' && DATA.myPayment.payment_type!='DEPOSIT'">
                            <td>Remarks</td>
                            <td><input placeholder="Remarks" type="text" class="form-control input-md" ng-model='DATA.myPayment.description' /></td>
                        </tr>
                        <tr>
                            <td>Payment Amount*</td>
                            <td>
                                <div class="input-group">
                                    <span class="input-group-addon" style="width:80px"><?=$vendor["default_currency"]?></span>
                                    <input placeholder="Payment Amount" min='0' step="any" type="number" min="0" class="form-control input-md" ng-model='DATA.myPayment.payment_amount' style="width:160px" ng-blur="convert_currency(DATA.myPayment)"
                                        ng-disabled="DATA.myPayment.payment_type == 'ACL' || DATA.myPayment.payment_type == 'DEPOSIT'" />
                                </div>
                            </td>
                        </tr>
                        <tr ng-show="$root.DATA_available_currency.currency">
                            <td>Paid In*</td>
                            <td>
                                <div class="input-group" ng-show='$root.DATA_available_currency.currency'>
                                    <select class="form-control input-md" ng-model='DATA.myPayment.payment_currency' style="width:80px" ng-change="convert_currency(DATA.myPayment)" ng-disabled="!$root.DATA_available_currency.currency">
                                        <option ng-repeat="crr in $root.DATA_available_currency.currency" value="{{crr}}">{{crr}}</option>
                                    </select>
                                    <input placeholder="Payment Amount" disabled="disabled" type="number" min="0" step="any" class="form-control input-md payment_amount" ng-model='DATA.myPayment.payment_amount_convertion' style="width:160px" />
                                </div>
                                <div ng-show='DATA.myPayment.currency_converter.bookkeeping_rates && DATA.myPayment.currency_converter.bookkeeping_rates.from.currency != DATA.myPayment.currency_converter.bookkeeping_rates.to.currency' 
                                    style="margin:10px 0">
                                    {{DATA.myPayment.currency_converter.bookkeeping_rates.from.currency}} {{DATA.myPayment.currency_converter.bookkeeping_rates.from.amount}} = 
                                    {{DATA.myPayment.currency_converter.bookkeeping_rates.to.currency}} {{DATA.myPayment.currency_converter.bookkeeping_rates.to.amount}}
                                </div>
                                <em ng-show='DATA.myPayment.payment_currency_loading'>Loading...</em>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer" style="text-align:center">
                    <button type="submit" class="btn btn-primary">Add</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
        	</form>
        </div>
    </div>
</div>
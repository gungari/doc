<div ng-init='checkInVoucherTrans(); <?php /*?>check_voucher_code="TDQ170678535"; getVoucherInformation()<?php */?> '>

	<div class="products">
		<div class="product form-inline text-center" style="font-size:18px">
			<form ng-submit='getVoucherInformation()'>
				Voucher Code : <input id="check_voucher_code" type="text" required="required" placeholder="Voucher Code" class="form-control input-lg" style="width:250px" ng-model='check_voucher_code' />
				<button type="submit" class="btn btn-lg btn-primary">OK</button>
				<button type="button" class="btn btn-lg btn-success" data-toggle="modal" data-target="#checkin-popup-info"><i class="fa fa-search"></i></button>
				<button type="button" class="btn btn-lg btn-default" onclick='$("#check_voucher_code").val("").focus();' ng-click="DATA.voucher = {}">Clear</button>
			</form>
		</div>
	</div>
	<br />

	<div ng-show='show_getVoucherInformation' xxng-show='!DATA.voucher.voucher.voucher && show_getVoucherInformation' align="center">
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	
	<div ng-show='DATA.voucher.voucher.voucher'>
    	<div class="sub-title"> Booking Information </div>
        <table class="table">
            <tr>
                <td width="130">Order#</td>
                <td><strong>#{{DATA.voucher.voucher.booking.booking_code}}</strong></td>
            </tr>
            <tr>
                <td>Booking Date</td>
                <td><strong>{{fn.newDate(DATA.voucher.voucher.booking.transaction_date) | date : 'dd MMMM yyyy HH:mm'}}</strong></td>
            </tr>
            <tr>
                <td>Booking Status</td>
                <td class="text-capitalize">
                    <strong>
                        <span ng-class="{'label label-danger':DATA.voucher.voucher.booking.status_code == 'CANCEL', 'label label-warning':DATA.voucher.voucher.booking.status_code == 'UNDEFINITE', 'label label-default':DATA.voucher.voucher.booking.status_code == 'VOID'}">
                            {{DATA.voucher.voucher.booking.status.toLowerCase()}}
                        </span>
                    </strong>
                </td>
            </tr>
            <tr>
                <td>Booking Source</td>
                <td class="text-capitalize"><strong>{{DATA.voucher.voucher.booking.source.toLowerCase()}}</strong></td>
            </tr>
            <tr ng-show="DATA.voucher.voucher.booking.agent">
                <td>Agent</td>
                <td class="text-capitalize">
                    <a ui-sref="agent.detail({'agent_code':DATA.voucher.voucher.booking.agent.agent_code})" target="_blank">
                        <strong>{{DATA.voucher.voucher.booking.agent.name}}</strong>
                    </a>
                </td>
            </tr>
            <tr ng-show="DATA.voucher.voucher.booking.agent && DATA.voucher.voucher.booking.voucher_reff_number != ''">
                <td>Voucher# Reff.</td>
                <td class="text-capitalize"><strong>{{DATA.voucher.voucher.booking.voucher_reff_number}}</strong></td>
            </tr>
        </table>
    
    
		<div class="sub-title"><strong>CUSTOMER INFORMATION</strong></div>
	
		<table width="100%" class="table">
			<tr>
				<td width="130">Full Name</td>
				<td><strong>{{DATA.voucher.voucher.booking.customer.first_name}} {{DATA.voucher.voucher.booking.customer.last_name}}</strong></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><strong>{{DATA.voucher.voucher.booking.customer.email}}</strong></td>
			</tr>
			<tr>
				<td>Country</td>
				<td><strong>{{DATA.voucher.voucher.booking.customer.country_name}}</strong></td>
			</tr>
			<tr>
				<td>Telephone</td>
				<td><strong>{{DATA.voucher.voucher.booking.customer.phone}}</strong></td>
			</tr>
		</table>
		
		<br />
		<div class="sub-title"><strong>VOUCHER INFORMATION</strong></div>
		<div ng-show='loading_print' align="center">
			Printing receipt <img src="<?=base_url("public/images/loading.gif")?>" />
		</div>
		<table width="100%" class="table">
			<tr>
				<td width="130">Bookings Code</td>
				<td><strong>#{{DATA.voucher.voucher.booking.booking_code}}</strong></td>
			</tr>
			<tr>
				<td>Voucher Code</td>
				<td><strong>{{DATA.voucher.voucher.voucher.voucher_code}}</strong></td>
			</tr>
			<tr>
				<td>Date</td>
				<td><strong>{{fn.formatDate(DATA.voucher.voucher.voucher.date, "dd MM yy")}}</strong></td>
			</tr>
			<tr ng-show="DATA.voucher.voucher.voucher.product_type == 'TRANS'">
				<td>Trip</td>
				<td style="font-size:16px">
                    <strong>{{DATA.voucher.voucher.voucher.departure.port.name}} ({{DATA.voucher.voucher.voucher.departure.port.port_code}}) - {{DATA.voucher.voucher.voucher.departure.time}}</strong>
                    &nbsp;&nbsp;<i class="fa fa-chevron-right"></i>&nbsp;&nbsp;
                    <strong>{{DATA.voucher.voucher.voucher.arrival.port.name}} ({{DATA.voucher.voucher.voucher.arrival.port.port_code}}) - {{DATA.voucher.voucher.voucher.arrival.time}}</strong>
                </td>
			</tr>
            <tr ng-show="DATA.voucher.voucher.voucher.product_type == 'ACT'">
				<td>Product</td>
				<td style="font-size:16px">
                    <strong>{{DATA.voucher.voucher.voucher.product.name}}</strong>
                </td>
			</tr>
			<tr>
				<td>Participants</td>
				<td>
					<strong>
						<span ng-show='DATA.voucher.voucher.voucher.qty_1 > 0'>{{DATA.voucher.voucher.voucher.qty_1}} Adult</span>
						<span ng-show='DATA.voucher.voucher.voucher.qty_2 > 0'>{{DATA.voucher.voucher.voucher.qty_2}} Child</span>
						<span ng-show='DATA.voucher.voucher.voucher.qty_3 > 0'>{{DATA.voucher.voucher.voucher.qty_3}} Infant</span>
					</strong>
				</td>
			</tr>
		</table>
		
		<div ng-show='DATA.voucher.voucher.voucher.pickup'>
			<br />
			<div class="sub-title"><strong>PICKUP INFORMATION</strong></div>
			<table width="100%" class="table">
				<tr>
					<td width="130">Area</td>
					<td><strong>{{DATA.voucher.voucher.voucher.pickup.area}} - {{DATA.voucher.voucher.voucher.pickup.time}}</strong></td>
				</tr>
				<tr>
					<td>Hotel Name</td>
					<td><strong>{{DATA.voucher.voucher.voucher.pickup.hotel_name}}</strong></td>
				</tr>
				<tr>
					<td>Hotel Address</td>
					<td><strong>{{DATA.voucher.voucher.voucher.pickup.hotel_address}}</strong></td>
				</tr>
				<tr>
					<td>Phone Number</td>
					<td><strong>{{DATA.voucher.voucher.voucher.pickup.hotel_phone_number}}</strong></td>
				</tr>
			</table>
		</div>
		<div ng-show='DATA.voucher.voucher.voucher.dropoff'>
			<br />
			<div class="sub-title"><strong>DROPOFF INFORMATION</strong></div>
			<table width="100%" class="table">
				<tr>
					<td width="130">Area</td>
					<td><strong>{{DATA.voucher.voucher.voucher.dropoff.area}} - {{DATA.voucher.voucher.voucher.dropoff.time}}</strong></td>
				</tr>
				<tr>
					<td>Hotel Name</td>
					<td><strong>{{DATA.voucher.voucher.voucher.dropoff.hotel_name}}</strong></td>
				</tr>
				<tr>
					<td>Hotel Address</td>
					<td><strong>{{DATA.voucher.voucher.voucher.dropoff.hotel_address}}</strong></td>
				</tr>
				<tr>
					<td>Phone Number</td>
					<td><strong>{{DATA.voucher.voucher.voucher.dropoff.hotel_phone_number}}</strong></td>
				</tr>
			</table>
		</div>

		<br />
        
        <div ng-show="DATA.voucher.voucher.booking.balance">
            <div class="sub-title"><strong>PAYMENT INFORMATION</strong></div>
            <div ng-show="DATA.voucher.voucher.booking.balance != 0">
                <table width="100%" class="table">
                    <tr xclass="success" xstyle="font-weight:bold">
                        <td width="130">Total Transaction</td>
                        <td>
                            {{DATA.voucher.voucher.booking.currency}} {{fn.formatNumber(DATA.voucher.voucher.booking.grand_total, DATA.voucher.voucher.booking.currency)}}
                        </td>
                    </tr>
                    <tr xclass="info" xstyle="font-weight:bold; color:green">
                        <td>Paid</td>
                        <td>{{DATA.voucher.voucher.booking.currency}} {{fn.formatNumber(DATA.voucher.voucher.booking.total_payment, DATA.voucher.voucher.booking.currency)}}</td>
                    </tr>
                    <tr style="font-weight:bold" ng-class="{'danger':(DATA.voucher.voucher.booking.balance>0), 'info':(DATA.voucher.voucher.booking.balance<=0)}">
                        <td>Outstanding Order</td>
                        <td>{{DATA.voucher.voucher.booking.currency}} {{fn.formatNumber(DATA.voucher.voucher.booking.balance, DATA.voucher.voucher.booking.currency)}}</td>
                    </tr>
                    <tr>
                    	<td></td>
                        <td>
                            <div class="add-product-button"> 
                                <a href="" class="btn btn-success btn-xs" ui-sref="reservation.detail.payment({booking_code:DATA.voucher.voucher.booking.booking_code})" target="_blank"> <span class="glyphicon glyphicon-plus"></span> Add Payment </a> 
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        
		<div class="sub-title"><strong>PARTICIPANT</strong></div>
		<br />
		<div class="table-responsive">
			<table width="100%" class="table table-bordered table-hover table-condensed" cellspacing="1">
				<tr class="header bold">
					<td width="40">#</td>
					<td>Full Name</td>
					<?php /*?><td width="150">Email</td><?php */?>
					<td width="130">Phone</td>
					<td width="130">ID Number</td>
					<td width="130">Nationality</td>
					<td width="80"></td>
				</tr>
				<tr ng-repeat='passenger in DATA.voucher.voucher.voucher.passenger' ng-class="{'success':(passenger.checkin_status==1 && DATA.voucher.allow_to_checkin)}"
					ng-click="selectToCheckIn(passenger)" style="cursor:pointer">
					<td width="30">{{($index+1)}}</td>
					<td>{{passenger.first_name}} {{passenger.last_name}}</td>
					<?php /*?><td width="150">{{passenger.email}}</td><?php */?>
					<td width="150">{{passenger.phone}}</td>
					<td width="150">{{passenger.passport_number}}</td>
					<td width="150">{{passenger.country_name}}</td>
					<?php /*?><td width="100" style="padding:0;">
						<input type="text" class="form-control input-sm" ng-click='passenger.checkin_status=1' placeholder="Code" ng-model='passenger.pass_code' />
					</td><?php */?>
					<td width="50" align="center">
						<div ng-show='passenger.already_checkin'><small style="color:green"><em>Checked In</em></small></div>
						<div ng-show='DATA.voucher.allow_to_checkin && !passenger.already_checkin'>
							<input type="checkbox" ng-disabled="is_checkedin_proccessed" ng-true-value="1" ng-false-value="0" ng-checked="(passenger.checkin_status==1)" ng-model="passenger.checkin_status" />
						</div>
					</td>
				</tr>
			</table>
		</div>
		<br /><br />
		<div ng-show='DATA.voucher.allow_to_checkin'>
			<div class="text-right" ng-show='!is_checkedin_proccessed'>
				<a href="" ng-click="checkUncheckParticipant(1, DATA.voucher.voucher.voucher.passenger)"><span class="glyphicon glyphicon-ok"></span> Check All</a> 
				&nbsp;&nbsp; | &nbsp;&nbsp; 
				<a href="" ng-click="checkUncheckParticipant(0, DATA.voucher.voucher.voucher.passenger)"><span class="glyphicon glyphicon-remove"></span> Uncheck All</a>
				<br /><br />
				<hr />
				<button type="button" class="btn btn-primary btn-lg" ng-click='checkInParticipantNow()'>Check In Now</button>
			</div>
			<div class="text-right" ng-show='is_checkedin_proccessed'>
				<strong><em>Processing...</em></strong>
			</div>
		</div>
		
		<div ng-show="!DATA.voucher.allow_to_checkin && DATA.voucher.voucher.booking.balance">
			<div class="alert alert-danger">
				<div class="pull-right text-right">
					<strong>Outstanding Order</strong>
					<div style="font-size:18px">
						{{DATA.voucher.voucher.booking.currency}}
						{{fn.formatNumber(DATA.voucher.voucher.booking.balance, DATA.voucher.voucher.booking.currency)}}
					</div>
				</div>
				<div>
                	<br />
					<?php /*?><strong>Please</strong> check on balance in order to continue Check In.<?php */?>
                    <strong>Please</strong> settle a payment in order to continue check in.
				</div>
                <div class="clearfix"></div>
			</div>
		</div>
	</div>
	
	<!-- modal Search reservation-->
	<div class="modal fade" id="checkin-popup-info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" ng-init="loadDataBookingTransport_2();">
	  <div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">
				Search Reservation
			</h4>
		  </div>
		  <div class="modal-body">
		  	<div>
		  		<div class="products">
		  			<div class="product">
		  				<form ng-submit='loadDataBookingTransport_2(1, true)'>
		  					<!-- <input type="text" class="form-control input-lg" placeholder="Search" ng-model='search.q' /> 
		  					<button type="submit" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-search"></span></button> -->
		  				<div class="table-responsive">
		  				<table class="table table-condensed table-borderless" width="100%">
							<tr>
								<td width="130">Filter By</td>
	                            <td width="100">From</td>
								<td width="100">To</td>
								<td width="200">Search</td>
								<td width="130">Source</td>
								
								<td></td>
							</tr>
							<tr>
	                        	<td>
	                            	<select class="form-control input-sm " ng-model='search.filter_by'>
	                                    <option value="BOOKINGDATE">Booking Date</option>
	                                    <option value="TRIPDATE">Activity Date</option>
	                                </select>
	                            </td>
								<td><input type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='search.start_date' /></td>
								<td><input type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='search.end_date' /></td>
								<td><input type="text" class="form-control input-sm" placeholder="Search" ng-model='search.q' /></td>
								<td>
									<select class="form-control input-sm" ng-model='search.booking_source' ng-change="ReservationListSearchAgentPopUp();">
										<option value="">All</option>
										<option value="{{booking_source.code}}" ng-repeat='booking_source in $root.DATA_booking_source'>{{booking_source.name}}</option>
	                                    <option ng-show='DATA.selected_agents && DATA.selected_agents.length > 0' value="-------" disabled="disabled">-------</option>
	                                    <option value="AGENT--{{agent.agent_code}}" ng-repeat='agent in DATA.selected_agents'>{{agent.name}} - {{agent.agent_code}}</option>
	                                    <option value="-------" disabled="disabled">-------</option>
	                                    <option value="SELECTAGENT">Select Agencies and Colleague</option>
									</select>
								</td>
								
								<td><button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button></td>
							</tr>
						</table>
						</div>
		  					<?php /*?>Filter : <input type="text" ng-model="DATA.filter_booking" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
		  					<button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button><?php */?>
		  				</form>
		  			</div>
		  		</div>

		  		<div ng-show='show_loading_DATA_bookings'>
		  			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		  		</div>
		  		<div ng-show='DATA.bookings.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.bookings.error_msg'>{{err}}</li></ul></div>
		  		<div ng-show='!show_loading_DATA_bookings && DATA.bookings.bookings'>
		  			<div class="table-responsive">
		  				<table class="table table-condensed table-bordered table-hover">
		  					<tr class="header bold">
		  						<td align="center" width="110">Voucher Code#</td>
		  						<td align="center">Customer</td>
		  						<td align="center" width="50">Adult</td>
		  						<td align="center" width="50">Child</td>
		  						<td align="center" width="50">infant</td>
								<?php /*?><td align="center" colspan="2" width="200">Trip</td><?php */?>
		  						<td align="center" width="200">Booking Source</td>
		  						<td width="50"></td>
		  					</tr>
		  					<tbody ng-repeat="booking in DATA.bookings.bookings | filter:DATA.filter_booking"  >
		  						<tr ng-class="{'danger':(booking.cancel_status=='CAN'), 'warning':(booking.status_code=='UNDEFINITE')}" >
		  							<td style="text-align: center;">
		  								<a href="" ng-click="openPopUpWindowVoucher('<?=base_url('home/print_page')?>#/print/voucher/', booking.booking_code, booking.detail.voucher_code)">
		  									<strong>{{booking.detail.voucher_code}}</strong>
		  								</a><br/>
		  								{{fn.formatDate(booking.detail.date, "dd  M  yy")}}
		  							</td>
		  							<td>{{booking.customer.first_name + " " +booking.customer.last_name}}</td>
		  							<td align="center">{{booking.detail.qty_1}}</td>
		  							<td align="center">{{booking.detail.qty_2}}</td>
		  							<td align="center">{{booking.detail.qty_3}}</td>
		  							<?php /*?><td width="70" align="center">{{booking.detail.departure.port.port_code}} <br />({{booking.detail.departure.time}})</td>
									<td width="70" align="center">{{booking.detail.arrival.port.port_code}} <br />({{booking.detail.arrival.time}})</td><?php */?>
		  							<td class="text-capitalize">
                                    	<div ng-show='!booking.agent' class="text-capitalize">{{booking.source.toLowerCase()}}</div>
										<div ng-show='booking.agent'>
                                        	{{booking.agent.name}}
                                            <div ng-show="booking.voucher_reff_number != ''">(Voucher# : {{booking.voucher_reff_number}})</div>
                                        </div>
		  							</td>
		  							<td>
		  								<button type="button" class="btn btn-primary btn-sm" data-ng-click="getClickVoucherInformation(booking.detail.voucher_code)">Checkin</button>
		  							</td>	  							
		  						</tr>
		  					</tbody>
		  				</table>
		  			</div>
		  			<nav aria-label="Page navigation" class="pull-right">
		  			  <ul class="pagination pagination-sm">
		  				<li ng-class="{'disabled':DATA.bookings.search.page <= 1}">
		  				  <a href="" ng-click='loadDataBookingTransport_2(DATA.bookings.search.page-1)' aria-label="Prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
		  				</li>
		  				<li	ng-show="DATA.bookings.search.page-3 > 1">
		  					<a href="" ng-click='loadDataBookingTransport_2(1)'>1</a>
		  				</li>
		  				<li class="disabled" ng-show="DATA.bookings.search.page-4 > 1">
		  					<a>...</a>
		  				</li>
		  				<li ng-repeat='pagination in DATA.bookings.search.pagination' ng-class="{'active':DATA.bookings.search.page == pagination}"
		  					ng-show="($index +1) <= DATA.bookings.search.page+3 && ($index+1) >= DATA.bookings.search.page-3">
		  					<a href="" ng-click='loadDataBookingTransport_2(($index+1))'>{{($index+1)}}</a>
		  				</li>
		  				<li class="disabled" ng-show="DATA.bookings.search.page+4 < DATA.bookings.search.number_of_pages">
		  					<a>...</a>
		  				</li>
		  				<li	ng-show="DATA.bookings.search.page+3 < DATA.bookings.search.number_of_pages">
		  					<a href="" ng-click='loadDataBookingTransport_2(DATA.bookings.search.number_of_pages)'>{{DATA.bookings.search.number_of_pages}}</a>
		  				</li>
		  				<li ng-class="{'disabled':DATA.bookings.search.page >= DATA.bookings.search.number_of_pages}">
		  				  <a href="" ng-click='loadDataBookingTransport_2(DATA.bookings.search.page+1)' aria-label="Next"><span class="glyphicon glyphicon-chevron-right"></span></a>
		  				</li>
		  			  </ul>
		  			</nav>
		  			<div class="clearfix"></div>
		  		</div>
		  	</div>
		  </div>
		  <div class="modal-footer" style="text-align:center">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
	  </div>
	</div>
	<!--/ modal Search reservation-->
    <!-- modal search agent -->
    <div class="modal fade" id="modal-search-agent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Search Agent</h4>
                </div>
                <div class="modal-body">
                    <div class="form-inline">
                        Search :
                        <input type="text" class="form-control input-md" ng-model="modal_agent_search.name" style="width:250px" placeholder="Search..." />
                        <select class="form-control input-md" ng-model="modal_agent_search.real_category.id" style="width:200px">
                            <option value="">All Category</option>
                            <option value="" disabled="disabled">---</option>
                            <option value="{{real_categories.id}}" ng-repeat="real_categories in DATA.agent_real_categories">{{real_categories.name}}</option>
                        </select>
                    </div>
                    <hr />
                    <table class="table table-bordered tbl-search-agent">
                        <tr class="success">
                            <td width="40"><strong>#</strong></td>
                            <td><strong>Agent Name</strong></td>
                            <td width="180"><strong>Category</strong></td>
                        </tr>
                        <tr ng-repeat="agent in DATA.agents.agents | filter : modal_agent_search | limitTo:50" class="agent-list"
                            ng-click="ReservationListSearchAgentPopUpSelectAgent(agent)" data-dismiss="modal">
                            <td>{{($index+1)}}</td>
                            <td>
                                <strong>{{agent.name}}</strong><br />
                                <small>{{agent.agent_code}}</small>
                            </td>
                            <td>{{agent.real_category.name}}</td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer" style="text-align:center">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <style>
        table.tbl-search-agent tr.agent-list{cursor:pointer;}
        table.tbl-search-agent tr.agent-list:hover{background:#d9edf7}
    </style>
    <!-- /modal search agent -->
    <!-- modal Open ticket checkin-->
	<?php $this->load->view("crs/checkin/v_modal_presoldticket_checkin")?>
	<!--/ modal Open ticket checkin-->
    
	<script>$("#check_voucher_code").focus();</script>
</div>
<script>activate_sub_menu_agent_detail("checkin");</script>

<div ng-init='printVoucher()'>
	<div class="no-print text-center" style="text-align:center !important">
		<select class="form-control input-md change-paper-size" ng-model='print_type' style="width:auto; display:inline">
			<option value="">Normal Paper</option>
			<option value="small"> Small Paper</option>
		</select>
		<br /><br />
	</div>
	
	<div class="small-paper" ng-show="print_type == 'small'">
		<div class="header">
			<div class="title"><?=$vendor["business_name"]?></div>
			<div><?=$vendor["address"]?>, <?=$vendor["regency"]?>, <?=$vendor["province"]?>, <?=$vendor["country"]?></div>
			<div>[P] : <?=$vendor["telephone"]?></div>
			<div>[E] : <?=((@$vendor["contact"]["email"]=='')?$vendor["email"]:$vendor["contact"]["email"])?></div>
			<div><?=$vendor["website"]?></div>
		</div>
		
		<div class="text-center">
			<strong>VOUCHER</strong><br />
			<div>
				<img src="<?=base_url()?>public/plugin/barcode.php?text={{DATA.voucher.voucher_code}}&size=40" />
			</div>
			<div style="font-size:16px">
				{{DATA.voucher.voucher_code}}
			</div>
			<div ng-show="DATA.current_booking.booking.status_code=='CANCEL'" style='color:red'>[CANCELLED]</div>
		</div>
		
		<hr />
		<strong>CUSTOMER INFORMATION</strong><br />
		<table width="100%" >
			<tr>
				<td width="90">Full Name</td>
				<td><strong>{{DATA.current_booking.booking.customer.first_name}} {{DATA.current_booking.booking.customer.last_name}}</strong></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><strong>{{DATA.current_booking.booking.customer.email}}</strong></td>
			</tr>
			<tr>
				<td>Country</td>
				<td><strong>{{DATA.current_booking.booking.customer.country_name}}</strong></td>
			</tr>
			<tr>
				<td>Telephone</td>
				<td><strong>{{DATA.current_booking.booking.customer.phone}}</strong></td>
			</tr>
		</table>
		<hr />

		<strong>VOUCHER INFORMATION</strong><br />
		<table width="100%" class="print-table">
			<tr>
				<td width="90">Booking Code</td>
				<td><strong>#{{DATA.current_booking.booking.booking_code}}</strong></td>
			</tr>
			<tr>
				<td>Voucher Code</td>
				<td><strong>{{DATA.voucher.voucher_code}}</strong></td>
			</tr>
			<tr>
				<td>Date</td>
				<td><strong>{{fn.formatDate(DATA.voucher.date, "dd MM yy")}}</strong></td>
			</tr>
			<tr ng-show="DATA.voucher.product_type == 'TRANS'"> 
				<td>Boat Name</td> 
				<td> 
					<strong>{{DATA.voucher.schedule.boat.name}}</strong> 
				</td> 
			</tr> 
			<tr ng-show="DATA.voucher.product_type == 'TRANS'">
				<td>Trip</td>
				<td></td>
			</tr>
			<tr ng-show="DATA.voucher.product_type == 'TRANS'">
				<td style="font-size:14px" colspan="2">
					<strong>{{DATA.voucher.departure.port.name}} ({{DATA.voucher.departure.port.port_code}}) - {{DATA.voucher.departure.time}}</strong>
					&nbsp;&nbsp;<i class="fa fa-chevron-right"></i>&nbsp;&nbsp;<br />
					<strong>{{DATA.voucher.arrival.port.name}} ({{DATA.voucher.arrival.port.port_code}}) - {{DATA.voucher.arrival.time}}</strong>
				</td>
			</tr>
            <tr ng-show="DATA.voucher.product_type == 'ACT'">
            	<td>Product</td>
                <td></td>
            </tr>
            <tr ng-show="DATA.voucher.product_type == 'ACT'">
				<td style="font-size:14px" colspan="2">
					<strong>{{DATA.voucher.product.name}}</strong>
				</td>
            </tr>
			<tr ng-show="DATA.voucher.is_packages">
            	<td>Packages</td>
				<td>
					{{DATA.voucher.packages.qty}} x {{DATA.voucher.packages.name}}
				</td>
            </tr>

			<tr>
				<td>Participants</td>
				<td>
					<strong>
						<span ng-show='DATA.voucher.qty_1 > 0'>{{DATA.voucher.qty_1}} Adult</span>
						<span ng-show='DATA.voucher.qty_2 > 0'>{{DATA.voucher.qty_2}} Child</span>
						<span ng-show='DATA.voucher.qty_3 > 0'>{{DATA.voucher.qty_3}} Infant</span>
                        <span ng-show='DATA.voucher.qty_4 > 0'>{{DATA.voucher.qty_4}} {{DATA.voucher.caption_opt_4}}</span>
                        <span ng-show='DATA.voucher.qty_5 > 0'>{{DATA.voucher.qty_5}} {{DATA.voucher.caption_opt_5}}</span>
					</strong>
				</td>
			</tr>
		</table>
		
		<div ng-show='DATA.voucher.pickup'>
			<hr />
			<strong>PICKUP INFORMATION</strong><br />
			<table width="100%" class="print-table">
				<tr>
					<td width="90">Area</td>
					<td><strong>{{DATA.voucher.pickup.area}} - {{DATA.voucher.pickup.time}}</strong></td>
				</tr>
				<tr>
					<td>Hotel Name</td>
					<td><strong>{{DATA.voucher.pickup.hotel_name}}</strong></td>
				</tr>
				<tr>
					<td>Hotel Address</td>
					<td><strong>{{DATA.voucher.pickup.hotel_address}}</strong></td>
				</tr>
				<tr>
					<td>Phone Number</td>
					<td><strong>{{DATA.voucher.pickup.hotel_phone_number}}</strong></td>
				</tr>
			</table>
		</div>
		<div ng-show='DATA.voucher.dropoff'>
			<hr />
			<strong>DROPOFF INFORMATION</strong><br />
			<table width="100%" class="print-table">
				<tr>
					<td width="90">Area</td>
					<td><strong>{{DATA.voucher.dropoff.area}} - {{DATA.voucher.dropoff.time}}</strong></td>
				</tr>
				<tr>
					<td>Hotel Name</td>
					<td><strong>{{DATA.voucher.dropoff.hotel_name}}</strong></td>
				</tr>
				<tr>
					<td>Hotel Address</td>
					<td><strong>{{DATA.voucher.dropoff.hotel_address}}</strong></td>
				</tr>
				<tr>
					<td>Hotel Phone Number</td>
					<td><strong>{{DATA.voucher.dropoff.hotel_phone_number}}</strong></td>
				</tr>
			</table>
		</div>
		
		<div ng-show='DATA.voucher.additional_service'>
			<hr />
			<strong>ADDITIONAL CHARGE</strong>
            <ul>
                <li ng-repeat="_item in DATA.voucher.additional_service">{{_item.name}} ({{_item.qty}}x)</li>
            </ul>
		</div>
		
		<hr />
		<div class="text-center">
			<strong>VOUCHER</strong><br />
			<div>
				<img src="<?=base_url()?>public/plugin/barcode.php?text={{DATA.voucher.voucher_code}}&size=40" />
			</div>
			<div style="font-size:16px">
				{{DATA.voucher.voucher_code}}
			</div>
			<div ng-show="DATA.current_booking.booking.status_code=='CANCEL'" style='color:red'>[CANCELLED]</div>
		</div>
		<hr />
		<strong>TERMS AND CONDITIONS</strong><br />
		<ol style="margin:0; padding-left:15px">
			<li ng-repeat='tac in terms_and_conditions'>{{tac}}</li>
		</ol>
		<br />
	</div>
	
	<br />
	<div class="normal-paper" ng-show="print_type != 'small'">
		
		<div class="header">
			<div class="pull-right text-center" align="center">
				<div style="font-size:16px">Voucher</div>
				<div>
					<img src="<?=base_url()?>public/plugin/barcode.php?text={{DATA.voucher.voucher_code}}&size=40" />
				</div>
				<div style="font-size:16px">
					{{DATA.voucher.voucher_code}}
				</div>
			</div>
			
			<div class="title"><?=$vendor["business_name"]?></div>
			<div><?=$vendor["address"]?>, <?=$vendor["regency"]?>, <?=$vendor["province"]?>, <?=$vendor["country"]?></div>
			<div>[P] : <?=$vendor["telephone"]?>, [E] : <?=((@$vendor["contact"]["email"]=='')?$vendor["email"]:$vendor["contact"]["email"])?></div>
			<div><?=$vendor["website"]?></div>
		</div>
		
		<div class="sub-title"><strong>CUSTOMER INFORMATION</strong></div>
		
		<table width="100%" class="print-table">
			<tr>
				<td width="100">Full Name</td>
				<td><strong>{{DATA.current_booking.booking.customer.first_name}} {{DATA.current_booking.booking.customer.last_name}}</strong></td>
				<td width="200" rowspan='4'>
					<div ng-show="DATA.current_booking.booking.status_code=='CANCEL'">
						<img src="<?=base_url("public/images/canceled_small.png")?>" width='200px' />
					</div>
				</td>
			</tr>
			<tr>
				<td>Email</td>
				<td><strong>{{DATA.current_booking.booking.customer.email}}</strong></td>
			</tr>
			<tr>
				<td>Country</td>
				<td><strong>{{DATA.current_booking.booking.customer.country_name}}</strong></td>
			</tr>
			<tr>
				<td>Telephone</td>
				<td><strong>{{DATA.current_booking.booking.customer.phone}}</strong></td>
			</tr>
		</table>
		
		<br />
		<div class="sub-title"><strong>VOUCHER INFORMATION</strong></div>
		<table width="100%" class="print-table">
			<tr>
				<td width="100">Booking Code</td>
				<td><strong>#{{DATA.current_booking.booking.booking_code}}</strong></td>
			</tr>
			<tr>
				<td>Voucher Code</td>
				<td><strong>{{DATA.voucher.voucher_code}}</strong></td>
			</tr>
			<tr>
				<td>Date</td>
				<td><strong>{{fn.formatDate(DATA.voucher.date, "dd MM yy")}}</strong></td>
			</tr>
			<tr ng-show="DATA.voucher.product_type == 'TRANS'"> 
				<td>Boat Name</td> 
				<td> 
					<strong>{{DATA.voucher.schedule.boat.name}}</strong> 
				</td> 
			</tr> 
			<tr ng-show="DATA.voucher.product_type == 'TRANS'">
				<td>Trip</td>
				<td style="font-size:16px">
					<strong>{{DATA.voucher.departure.port.name}} ({{DATA.voucher.departure.port.port_code}}) - {{DATA.voucher.departure.time}}</strong>
					&nbsp;&nbsp;<i class="fa fa-chevron-right"></i>&nbsp;&nbsp;
					<strong>{{DATA.voucher.arrival.port.name}} ({{DATA.voucher.arrival.port.port_code}}) - {{DATA.voucher.arrival.time}}</strong>
				</td>
			</tr>
            <tr ng-show="DATA.voucher.product_type == 'ACT'">
            	<td>Product</td>
				<td style="font-size:16px">
					<strong>{{DATA.voucher.product.name}}</strong>
				</td>
            </tr>
            <tr ng-show="DATA.voucher.is_packages">
            	<td>Packages</td>
				<td>
					{{DATA.voucher.packages.qty}} x {{DATA.voucher.packages.name}}
				</td>
            </tr>
			<tr>
				<td>Participants</td>
				<td>
					<strong>
						<span ng-show='DATA.voucher.qty_1 > 0'>{{DATA.voucher.qty_1}} Adult</span>
						<span ng-show='DATA.voucher.qty_2 > 0'>{{DATA.voucher.qty_2}} Child</span>
						<span ng-show='DATA.voucher.qty_3 > 0'>{{DATA.voucher.qty_3}} Infant</span>
                        <span ng-show='DATA.voucher.qty_4 > 0'>{{DATA.voucher.qty_4}} {{DATA.voucher.caption_opt_4}}</span>
                        <span ng-show='DATA.voucher.qty_5 > 0'>{{DATA.voucher.qty_5}} {{DATA.voucher.caption_opt_5}}</span>
					</strong>
				</td>
			</tr>
		</table>
		
		<div ng-show='DATA.voucher.pickup'>
			<br />
			<div class="sub-title"><strong>PICKUP INFORMATION</strong></div>
			<table width="100%" class="print-table">
				<tr>
					<td width="100">Area</td>
					<td><strong>{{DATA.voucher.pickup.area}} - {{DATA.voucher.pickup.time}}</strong></td>
				</tr>
				<tr>
					<td>Hotel Name</td>
					<td><strong>{{DATA.voucher.pickup.hotel_name}}</strong></td>
				</tr>
				<tr>
					<td>Hotel Address</td>
					<td><strong>{{DATA.voucher.pickup.hotel_address}}</strong></td>
				</tr>
				<tr>
					<td>Hotel Phone Number</td>
					<td><strong>{{DATA.voucher.pickup.hotel_phone_number}}</strong></td>
				</tr>
			</table>
		</div>
		<div ng-show='DATA.voucher.dropoff'>
			<br />
			<div class="sub-title"><strong>DROPOFF INFORMATION</strong></div>
			<table width="100%" class="print-table">
				<tr>
					<td width="100">Area</td>
					<td><strong>{{DATA.voucher.dropoff.area}} - {{DATA.voucher.dropoff.time}}</strong></td>
				</tr>
				<tr>
					<td>Hotel Name</td>
					<td><strong>{{DATA.voucher.dropoff.hotel_name}}</strong></td>
				</tr>
				<tr>
					<td>Hotel Address</td>
					<td><strong>{{DATA.voucher.dropoff.hotel_address}}</strong></td>
				</tr>
				<tr>
					<td>Hotel Phone Number</td>
					<td><strong>{{DATA.voucher.dropoff.hotel_phone_number}}</strong></td>
				</tr>
			</table>
		</div>
		
		<div ng-show='DATA.voucher.additional_service'>
			<br />
			<div class="sub-title"><strong>Additional Charge</strong></div>
			<table width="100%" class="print-table">
				<tr>
					<td width="100"></td>
					<td>
                    	<ul style="padding-left:5px">
                        	<li ng-repeat="_item in DATA.voucher.additional_service">{{_item.name}} ({{_item.qty}}x)</li>
                        </ul>
                    </td>
				</tr>
			</table>
		</div>
		
		<br />
		<div class="sub-title"><strong>PARTICIPANT</strong></div>
		<table width="100%" class="table table-bordered table-condensed">
			<tr class="table-header bold">
				<td width="40">#</td>
				<td>Full Name</td>
				<?php /*?><td width="150">Email</td><?php */?>
				<td width="140">Phone</td>
				<td width="140">ID Number</td>
				<td width="150">Nationality</td>
			</tr>
			<tr ng-repeat='passenger in DATA.voucher.passenger'>
				<td width="30">{{($index+1)}}</td>
				<td>{{passenger.first_name}} {{passenger.last_name}}</td>
				<?php /*?><td>{{passenger.email}}</td><?php */?>
				<td>{{passenger.phone}}</td>
				<td>{{passenger.passport_number}}</td>
				<td>{{passenger.country_name}}</td>
			</tr>
		</table>
		
		<div class="sub-title"><strong>TERMS AND CONDITIONS</strong></div>
		<ol style="margin:0; padding-left:20px">
			<li ng-repeat='tac in terms_and_conditions'>{{tac}}</li>
		</ol>
        <div ng-show='voucher_notes' style="margin-top:15px;">
	        <div class="sub-title"><strong>NOTES</strong></div>
            <div style="background:#ffffd3; padding:10px">
                <ol ng-show="voucher_notes.list" style="margin:0; padding-left:15px">
                    <li ng-repeat='note in voucher_notes.list'>{{note}}</li>
                </ol>
                <div ng-show="voucher_notes.text">{{voucher_notes.text}}</div> 
            </div>
        </div>
	</div>
	<br>
	<div class="no-print">
		<div class="row">
			<div class="dropdown col-md-6 text-left" style="margin-top: 10px;">
				<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
		    		<span class="glyphicon glyphicon-envelope"></span>&nbsp;Send Email
				    <span class="caret"></span>
				</button>
				
					<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					  <li style="cursor: pointer;" ng-show="DATA.current_booking.booking.agent"><a ng-click="send_email_voucher(DATA.current_booking.booking.booking_code, '1')">To Agent</a></li>
					  <li ng-show="!DATA.current_booking.booking.agent" style="cursor: pointer;"><a data-toggle="modal" data-target="#payment-detail" class="delete-icon" ng-show="payment.id != ''">To Customer</a></li>
					</ul>
			</div>
		</div>
	</div>
	<div class="modal fade" id="payment-detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	  <form ng-submit="update_email_customer($event, 'voucher')">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">
				<span>Customer Email</span> 
			</h4>
		  </div>
		  <div class="modal-body">
		  <div ng-show='error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in error_msg'>{{err}}</li></ul></div>
			<table class="table table-borderless table-condensed" style="margin-bottom: 0 !important;">
				<tr>
					<td>Email</td>
					<td><input placeholder="Email" type="email" class="form-control input-md" ng-model='DATA.current_booking.booking.customer.email' /></td>
				</tr>
			</table>
		  </div>
		  <div class="modal-footer" style="text-align:center">
			<button type="submit"  class="btn btn-primary">
			<small><span class="glyphicon glyphicon-envelope"></span>
			 &nbsp;Send Receipt</small>
			</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
		</form>
	  </div>
	</div>
</div>

<script>
	$(".change-paper-size").change(function(){
		var _this = $(this);
		
		if (_this.val() == 'small'){
			$(".print_area").addClass("small_paper");
		}else{
			$(".print_area").removeClass("small_paper");
		}
	});
</script>

<style>


	.print_area{background:none !important; padding:0 !important;}
	.small_paper{width:8cm !important; margin:0 auto !important}
	.normal-paper{background:#FFF; padding:10px}
	.small-paper{width:8cm; margin:auto; padding:10px; background:#FFF}
	.small-paper .header .title{font-size:18px !important}

</style>


<?php
//pre($vendor)
?>
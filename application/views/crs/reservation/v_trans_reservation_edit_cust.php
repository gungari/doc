<div class="modal" id="modal-add-cust" tabindex="-1" role="dialog" aria-labelledby="ModalEditCustLabel">
  <div class="modal-dialog" role="document">
  <form ng-submit='update_customer($event)'>
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title">Edit Customer Information</span></h4>
	  </div>
	  <div class="modal-body products">
        <div ng-show='DATA.myCustomer.error_desc.length>0' class="alert alert-danger"><ul><li ng-repeat='error in DATA.myCustomer.error_desc'>{{error}}</li></ul></div>
        <table class="table table-borderless table-condensed">
            <tr>
                <td width="150">Name*</td>
                <td>					
                    <input type="text" placeholder="First Name" class="form-control input-md" ng-model='DATA.myCustomer.first_name' style="display:inline; width: 46.5%;" />
                    &nbsp;&nbsp;&nbsp;
                    <input type="text" placeholder="Last Name" class="form-control input-md" ng-model='DATA.myCustomer.last_name' style="display:inline; width:48.5%" />
                </td>
            </tr>
            <tr>
                <td>Email</td>
                <td><input type="text" placeholder="Email" class="form-control input-md" ng-model='DATA.myCustomer.email' /></td>
            </tr>
            <tr>
                <td>Phone / Mobile*</td>
                <td><input type="text" placeholder="Phone / Mobile" class="form-control input-md" ng-model='DATA.myCustomer.phone' /></td>
            </tr>
            <tr>
                <td>Country*</td>
                <td>
                    <select ng-model='DATA.myCustomer.country_code' class="form-control input-md">
                        <option value="">-- Select Country --</option>
                        <option value="{{country.code}}" ng-repeat='country in $root.DATA_country_list'>{{country.name}}</option>
                    </select>
                </td>
            </tr>
        </table>
	  </div>	
	  <div class="modal-footer" style="text-align:center">
		<button type="submit" class="btn btn-primary btn-submit-edit-cust">Save</button>
		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	  </div>
	</div>
  </form>
  </div>
</div>
<?php $this->load->view("crs/reservation/v_general_reservation_page_header") ?>

<div ui-view>
	<div ng-init="loadDataBooking_2()">
		<div class="products">
			<div class="product">
				<form ng-submit='loadDataBooking_2(1, true)'>
				<div class="table-responsive">
					<table class="table table-condensed table-borderless" width="100%">
						<tr>
							<td width="120">Filter By</td>
                            <td width="100">From</td>
							<td width="100">To</td>
							<td width="200">Search</td>
							<td width="130">Source</td>
							<td width="120">Status</td>
							<td></td>
						</tr>
						<tr>
                        	<td>
                            	<select class="form-control input-sm " ng-model='search.filter_by'>
                                    <option value="BOOKINGDATE">Booking Date</option>
                                    <option value="TRIPDATE">Activity Date</option>
                                </select>
                            </td>
							<td><input type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='search.start_date' /></td>
							<td><input type="text" class="form-control input-sm datepicker" placeholder="yyyy-mm-dd" ng-model='search.end_date' /></td>
							<td><input type="text" class="form-control input-sm" placeholder="Search" ng-model='search.q' /></td>
							<td>
								<select class="form-control input-sm" ng-model='search.booking_source' ng-change="ReservationListSearchAgentPopUp();">
									<option value="">All</option>
									<option value="{{booking_source.code}}" ng-repeat='booking_source in $root.DATA_booking_source'>{{booking_source.name}}</option>
                                    <option ng-show='DATA.selected_agents && DATA.selected_agents.length > 0' value="-------" disabled="disabled">-------</option>
                                    <option value="AGENT--{{agent.agent_code}}" ng-repeat='agent in DATA.selected_agents'>{{agent.name}} - {{agent.agent_code}}</option>
                                    <option value="-------" disabled="disabled">-------</option>
                                    <option value="SELECTAGENT">Select Agencies and Colleague</option>
								</select>
							</td>
							<td>
								<select class="form-control input-sm" ng-model='search.booking_status'>
									<option value="">All</option>
									<option value="{{booking_status.code}}" ng-repeat='booking_status in $root.DATA_booking_status'>{{booking_status.name}}</option>
								</select>
							</td>
							<td><button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button></td>
						</tr>
					</table>
					
					<?php /*?>Filter : <input type="text" ng-model="DATA.filter_booking" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
					<button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button><?php */?>
				</div>	
				</form>
			</div>
		</div>

		<div ng-show='show_loading_DATA_bookings'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		</div>
		
		<div ng-show='!show_loading_DATA_bookings'>
			<div class="table-responsive">
				<table class="table table-condensed table-bordered">
					<tr class="header bold">
						<td align="center" width="90">Order#</td>
						<td align="center" width="110">Booking Date</td>
						<td>Customer Name</td>
						<td align="center" width="40" title="Nationality">Nat.</td>
						<td width="200">Booking Source</td>
						<td align="center" width="50">Status</td>
						<td align="right" width="130">Amount</td>
					</tr>
					<tbody ng-repeat="booking in DATA.bookings.bookings | filter:DATA.filter_booking">
						<tr ng-class="{'danger':(booking.status_code=='CANCEL'), 'warning':(booking.status_code=='UNDEFINITE')}">
						<?php /* <tr ng-class="{'danger':(booking.cancel_status=='CAN' || booking.cancel_status=='VOID'), 'warning':(booking.status_code=='UNDEFINITE')}"> */ ?>
							<td align="center" rowspan="2">
								<a ui-sref="reservation.detail({'booking_code':booking.booking_code})"><strong>{{booking.booking_code}}</strong></a>
							</td>
							<td align="center">{{fn.newDate(booking.transaction_date) | date:'dd MMM yyyy'}} <small>{{fn.newDate(booking.transaction_date) | date:'HH:mm'}}</small> </td>
							<?php /*?><td align="center">{{fn.formatDate(booking.transaction_date, "dd M yy")}}</td><?php */?>
							<td>
								<strong>{{booking.customer.full_name}}</strong>
								<?php /*?><br /> {{booking.customer.email}}<?php */?>
							</td>
							<td align="center" class="text-capitalize" title="{{booking.customer.country_name}}">{{booking.customer.country_code}}</td>
							<td>
								<div ng-show='!booking.agent' class="text-capitalize">{{booking.source.toLowerCase()}}</div>
								<div ng-show='booking.agent'>
                                	<a ui-sref="agent.detail.reservation({'agent_code':booking.agent.agent_code})" target="_blank">{{booking.agent.name}}</a><br />
                                    <small>{{booking.agent.payment_method.description}}</small>
								</div>
							</td>
							<td align="center" title="{{booking.status}}">
                            	<?php /*?><span ng-show="booking.cancel_status == ''">DEF</span>
                                <span ng-show="booking.cancel_status != ''">{{booking.cancel_status}}</span><?php */?>
                                <?php /*?><span ng-show="booking.cancel_status=='CAN'">{{booking.cancel_status}}</span>
								<span ng-show="booking.cancel_status!='CAN'">{{((booking.status_initial=="CAN")?"DEF":booking.status_initial)}}</span><?php */?>
                                
                                <?php /*
								<div ng-show="booking.cancel_status!=''">
	                                <span>{{booking.cancel_status}}</span>
                                </div>
                                <div ng-show="booking.cancel_status==''">
                                	{{((booking.status_initial=="CAN" || booking.status_initial=="VOID")?"DEF":booking.status_initial)}}
                                </div>
                                */ ?>

								<div><span>{{booking.status_initial}}</span></div>

                                <div ng-show="booking.status_code == 'UNDEFINITE'" title="To confirm booking please click this button">
                                    <span ng-show='!booking.saving_ConfirmTentativeBooking'>
                                        <button type="button" class="btn btn-info btn-xs" ng-click='ConfirmTentativeBooking(booking, "loadDataBooking_2")'>Confirm</button>
                                    </span>
                                    <span ng-show='booking.saving_ConfirmTentativeBooking'>
                                        <button type="button" class="btn btn-info btn-xs" disabled="disabled"><em>Loading...</em></button>
                                    </span>
                                </div>
                                
								<?php /*?><span ng-show="booking.cancel_status=='CAN'">{{booking.cancel_status}}</span>
								<span ng-show="booking.cancel_status!='CAN'">{{((booking.status_initial=="CAN" || booking.status_initial=="VOID")?"DEF":booking.status_initial)}}</span><?php */?>
							</td>
							<td align="right">
								<?php /*?><div ng-show='booking.cancel_status=="CAN"'><?php */?>
                                <div ng-show='booking.status_code=="CANCEL"'>
									<div ng-show='booking.detail.length == 1'>
                                    	{{booking.currency}} {{fn.formatNumber(booking.detail[0].subtotal_before_cancel, booking.currency)}}<br />
                                        <small>Cancelation Fee:</small>
                                        {{booking.currency}} {{fn.formatNumber(booking.detail[0].subtotal, booking.currency)}}
                                    </div>
                                    <div ng-show='booking.detail.length != 1'>
                                        {{booking.currency}} {{fn.formatNumber(booking.grand_total_before_cancel, booking.currency)}}<br />
                                        <small>Cancelation Fee:</small>
                                        {{booking.currency}} {{fn.formatNumber(booking.grand_total, booking.currency)}}
                                    </div>
                                </div>
                                <?php /*?><div ng-show='booking.cancel_status!="CAN"'><?php */?>
                                <div ng-show='booking.status_code!="CANCEL"'>
									<?php /*?><div ng-show='booking.status_initial=="CAN"'>
										{{booking.currency}} {{fn.formatNumber(booking.grand_total_before_cancel, booking.currency)}}
                                    </div><?php */?>
                                    <div ng-show='booking.status_initial!="CAN"'>
										{{booking.currency}} {{fn.formatNumber(booking.grand_total, booking.currency)}}
                                    </div>
                                    <div>
	                                    <hr style="margin:3px 0" />
    	                                <small style="color:green">Paid : {{booking.currency}} {{fn.formatNumber(booking.total_payment, booking.currency)}}</small>
                                	</div>
                                </div>
							</td>
						</tr>
						<tr ng-class="{'danger':(booking.status_code=='CANCEL'), 'warning':(booking.status_code=='UNDEFINITE')}">
						<?php /* <tr ng-class="{'danger':(booking.cancel_status=='CAN' || booking.cancel_status=='VOID'), 'warning':(booking.status_code=='UNDEFINITE')}"> <?php */?>
							<td colspan="6" style="padding:0 !important">
								
								<table class="table table-bordered table-condensed" style="margin:-1px !important; background:none">
									<tr ng-show="booking.remarks || booking.undefinite_confirm_to_definite || (booking.agent && booking.voucher_reff_number != '')">
										<td style="border-left:solid 5px #FFF; font-size:11px; font-style:italic">
                                        	<div ng-show='booking.remarks'>
                                            	Remarks : {{booking.remarks}}
                                            </div>
                                            <div ng-show="booking.agent && booking.voucher_reff_number != ''">
                                            	Voucher# Reff. : {{booking.voucher_reff_number}}
                                            </div>
                                            <div ng-show="booking.undefinite_confirm_to_definite">
                                                Confirm By : <strong>{{booking.undefinite_confirm_to_definite.name}}</strong> - 
                                                             <strong>{{fn.newDate(booking.undefinite_confirm_to_definite.date) | date : 'dd MMM yyyy HH:mm'}}</strong>
                                            </div>
										</td>
									</tr>
									<?php /*?><tr ng-repeat="voucher in booking.detail | orderBy : '-booking_detail_status_code'" ng-class="{'danger':(voucher.booking_detail_status_code=='CANCEL')}"><?php */?>
									<tr ng-repeat="voucher in booking.detail | orderBy : '-booking_detail_status_code'" 
										ng-class="{'danger':(booking.cancel_status=='CAN' || voucher.booking_detail_status_code == 'CANCEL')}">
										<td style="border-left:solid 5px #EEE; font-size:11px">
											{{voucher.voucher_code}} - 
											<strong style="font-size:12px">{{fn.formatDate(voucher.date, "dd / M / y")}}</strong> - 
                                            <span ng-show="voucher.product_type == 'ACT'">
                                            	{{voucher.product.name}}
                                            </span>
                                            <span ng-show="voucher.product_type == 'TRANS'">
                                                {{voucher.departure.port.port_code}} ({{voucher.departure.time}})
                                                <span class="glyphicon glyphicon-chevron-right"></span>
                                                {{voucher.arrival.port.port_code}} ({{voucher.arrival.time}})
											</span>
                                            <span ng-hide='voucher.is_packages'>
                                                <span ng-show="voucher.qty_1 > 0"> - {{voucher.qty_1}} Adl. </span>
                                                <span ng-show="voucher.qty_2 > 0"> - {{voucher.qty_2}} Chi. </span>
                                                <span ng-show="voucher.qty_3 > 0"> - {{voucher.qty_3}} Inf. </span>
											</span>
                                            <span ng-show='voucher.is_packages'>
                                            	- {{voucher.packages.qty}} x {{voucher.packages.name}}
                                            </span>
                                            
											<span ng-show="voucher.is_pickup=='YES'"> - Pickup </span>
											<span ng-show="voucher.is_dropoff=='YES'"> - Dropoff </span>
										</td>
									</tr>
								</table>
								
								<?php /*?><table class="table table-bordered table-condensed" style="margin:-1px !important">
									<tr ng-repeat='voucher in booking.detail' ng-class="{'danger':(booking.status_code=='CANCEL'), 'warning':(booking.status_code=='UNDEFINITE')}">
										<td width="100">{{voucher.voucher_code}}</td>
										<td width="90">{{fn.formatDate(voucher.date, "dd M yy")}}</td>
										<td width="90">{{voucher.departure.port.port_code}} ({{voucher.departure.time}})</td>
										<td width="90">{{voucher.arrival.port.port_code}} ({{voucher.arrival.time}})</td>
										<td width="45" align="center" title="Pickup">{{(voucher.is_pickup=='YES')?'PU':'-'}}</td>
										<td width="45" align="center" title="Dropoff">{{(voucher.is_dropoff=='YES')?'DO':'-'}}</td>
										<td></td>
									</tr>
								</table><?php */?>
								
							</td>
						</tr>
						<tr>
							<td colspan="7" style="background:#FAFAFA"></td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<nav aria-label="Page navigation" class="pull-right">
			  <ul class="pagination pagination-sm">
				<li ng-class="{'disabled':DATA.bookings.search.page <= 1}">
				  <a href="" ng-click='loadDataBooking_2(DATA.bookings.search.page-1, true)' aria-label="Prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
				</li>
				<li ng-repeat='pagination in DATA.bookings.search.pagination' ng-class="{'active':DATA.bookings.search.page == pagination}">
					<a href="" ng-click='loadDataBooking_2(($index+1), true)'>{{($index+1)}}</a>
				</li>
				<li ng-class="{'disabled':DATA.bookings.search.page >= DATA.bookings.search.number_of_pages}">
				  <a href="" ng-click='loadDataBooking_2(DATA.bookings.search.page+1, true)' aria-label="Next"><span class="glyphicon glyphicon-chevron-right"></span></a>
				</li>
			  </ul>
			</nav>
			<div class="clearfix"></div>
            
			<hr />
			<a href="<?=site_url("export_to_excel/trans_bookings")?>?s[start_date]={{DATA.bookings.search.start_date}}&s[end_date]={{DATA.bookings.search.end_date}}&s[q]={{DATA.bookings.search.q}}&s[booking_status]={{DATA.bookings.search.booking_status}}&s[booking_source]={{DATA.bookings.search.booking_source}}&s[page]={{DATA.bookings.search.page}}" target="_blank"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export To Excel</a>
		</div>
		<br />
		
		<div class="add-product-button">
			<?php if (is_array($vendor["category"])){ ?>
                <div class="pull-right">
                    <div class="btn-group">
                        <button type="button" class="btn btn-success btn-md dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="glyphicon glyphicon-plus"></span> New Reservation </a> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a ui-sref="act_reservation.new">Activities</a></li>
                            <li><a ui-sref="trans_reservation.new">Transport</a></li>
                        </ul>
                    </div>
                </div>
            <?php }elseif ($this->fn->isVendorCRS_TRANS($vendor)){ ?>
                <a ui-sref="trans_reservation.new" class="btn btn-success btn-lg btn-block"> <span class="glyphicon glyphicon-plus"></span> New Reservation </a>
            <?php }elseif ($this->fn->isVendorCRS_ACT($vendor)){ ?>
                <a ui-sref="act_reservation.new" class="btn btn-success btn-lg btn-block"> <span class="glyphicon glyphicon-plus"></span> New Reservation </a>
            <?php } ?>
        </div>
	</div>
    
    <div class="modal fade" id="modal-search-agent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Search Agent</h4>
                </div>
                <div class="modal-body">
                    <div class="form-inline">
                        Search :
                        <input type="text" class="form-control input-md" ng-model="modal_agent_search.name" style="width:250px" placeholder="Search..." />
                        <select class="form-control input-md" ng-model="modal_agent_search.real_category.id" style="width:200px">
                            <option value="">All Category</option>
                            <option value="" disabled="disabled">---</option>
                            <option value="{{real_categories.id}}" ng-repeat="real_categories in DATA.agent_real_categories">{{real_categories.name}}</option>
                        </select>
                    </div>
                    <hr />
                    <table class="table table-bordered tbl-search-agent">
                        <tr class="success">
                            <td width="40"><strong>#</strong></td>
                            <td><strong>Agent Name</strong></td>
                            <td width="180"><strong>Category</strong></td>
                        </tr>
                        <tr ng-repeat="agent in DATA.agents.agents | filter : modal_agent_search | limitTo:50" class="agent-list"
                            ng-click="ReservationListSearchAgentPopUpSelectAgent(agent)" data-dismiss="modal">
                            <td>{{($index+1)}}</td>
                            <td>
                                <strong>{{agent.name}}</strong><br />
                                <small>{{agent.agent_code}}</small>
                            </td>
                            <td><span ng-repeat='_cat in agent.real_categories'> {{_cat.name}}<span ng-hide="agent.real_categories.length==($index+1)">,</span> </span></td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer" style="text-align:center">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <style>
        table.tbl-search-agent tr.agent-list{cursor:pointer;}
        table.tbl-search-agent tr.agent-list:hover{background:#d9edf7}
    </style>

    
</div>

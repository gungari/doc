<style type="text/css">
	.danger{
		color: red;
	}
</style>
<form ng-submit='submit_booking($event)'>

	<div class="sub-title" ng-init='newReservationActivities()'>
		<span ng-show='!DATA.data_rsv.reservation_code'><i class="fa fa-plus-square" aria-hidden="true"></i> Add New</span> 
		<span ng-show='DATA.data_rsv.reservation_code'><i class="fa fa-pencil" aria-hidden="true"></i> Edit</span>
		Reservation
	</div>
	
	<br />
	
	<div ng-show='DATA.data_rsv.error_desc.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.data_rsv.error_desc'>{{err}}</li></ul></div>
	
	<table class="table table-borderless">
		<tr>
			<td width="150">Booking Source*</td>
			<td class="form-inline">
				<select class="form-control input-md" style="width:200px" ng-model='DATA.data_rsv.booking_source' ng-change='aplicable_rates_for_agent();check_booking_source_sub();newReservationResetSelectedProduct();'>
					<option value="{{booking_source.code}}" ng-repeat='booking_source in $root.DATA_booking_source' ng-show="booking_source.code!='ONLINE'">{{booking_source.name}}</option>
				</select>
                &nbsp;&nbsp;
                <select ng-disabled="!DATA.booking_source_sub" ng-show='DATA.booking_source_sub' class="form-control input-md" style="width:200px" ng-model='DATA.data_rsv.booking_source_sub'>
					<option value="{{booking_source.code}}" ng-repeat='booking_source in DATA.booking_source_sub'>{{booking_source.name}}</option>
				</select>
			</td>
		</tr>
		<tr ng-show='DATA.data_rsv.booking_source == "AGENT"'>
			<td>Agent Name*</td>
			<td>
				<select class="form-control input-md" ng-model='DATA.data_rsv.agent' ng-change='aplicable_rates_for_agent();newReservationResetSelectedProduct();' 
					ng-options="(agents.name +' - '+agents.agent_code) for agents in DATA.agents.agents" id="cmb-agent-name">
					<option value="">-- Select Agent --</option>
					<?php /*?><option value="{{agent.agent_code}}" ng-repeat='agent in DATA.agents.agents'>{{agent.name}} - {{agent.agent_code}}</option><?php */?>
				</select>
			</td>
		</tr>
        <tr ng-show='DATA.data_rsv.booking_source == "AGENT"'>
			<td>Voucher# Reff.</td>
			<td>
            	<input type="text" class="form-control input-md" style="width:200px" ng-model='DATA.data_rsv.voucher_reff_number' placeholder="Voucher# Reff." />
			</td>
		</tr>
		<tr>
			<td>Booking Status*</td>
			<td>
				<select class="form-control input-md" style="width:200px; display:inline" ng-model='DATA.data_rsv.booking_status'>
					<option value="{{booking_status.code}}" ng-repeat='booking_status in $root.DATA_booking_status' ng-show="booking_status.code!='CANCEL' && booking_status.code!='VOID'">
                    	{{booking_status.name}}
                    </option>
				</select>
                <?php /*?><span ng-show="DATA.data_rsv.booking_status == 'UNDEFINITE'">&nbsp;&nbsp;&nbsp;<em>Valid for 24 hours only.</em></span><?php */?>
			</td>
		</tr>
        <tr ng-show="DATA.data_rsv.booking_status == 'UNDEFINITE'">
        	<td>Valid For*</td>
            <td>
            	<div class="input-group" style="width:200px">
                  <input placeholder="Valid For" type="number" ng-disabled="DATA.data_rsv.booking_status!='UNDEFINITE' || DATA.data_rsv.undefinite_valid_until_type != 'HOUR'" ng-required="DATA.data_rsv.booking_status=='UNDEFINITE'" step="0.1" class="form-control input-md" ng-model='DATA.data_rsv.undefinite_valid_until_hour' ng-show="DATA.data_rsv.undefinite_valid_until_type == 'HOUR'" required="required" />
                  <input placeholder="Valid For" type="text" ng-disabled="DATA.data_rsv.booking_status!='UNDEFINITE' || DATA.data_rsv.undefinite_valid_until_type != 'DATE'" ng-required="DATA.data_rsv.booking_status=='UNDEFINITE'" class="form-control input-md" ng-model='DATA.data_rsv.undefinite_valid_until_date' ng-show="DATA.data_rsv.undefinite_valid_until_type == 'DATE'" required="required" id="undefinite_valid_until_date" />
                  <div class="input-group-btn">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{DATA.data_rsv.undefinite_valid_until_type}} <span class="caret"></span></button>
                    <ul class="dropdown-menu dropdown-menu-right">
                      <li><a href="" ng-click="DATA.data_rsv.undefinite_valid_until_type='HOUR'">Hour</a></li>
                      <li><a href="" ng-click="DATA.data_rsv.undefinite_valid_until_type='DATE'">Date</a></li>
                    </ul>
                  </div>
                </div>
            	<?php /*?><div class="input-group" style="width:200px">
	            	<input placeholder="Valid For" type="number" ng-disabled="DATA.data_rsv.booking_status!='UNDEFINITE'" ng-required="DATA.data_rsv.booking_status=='UNDEFINITE'" step="0.1" class="form-control input-md" ng-model='DATA.data_rsv.undefinite_valid_until_hour' />
                    <span class="input-group-addon">Hour</span>
                </div><?php */?>
			</td>
        </tr>
	</table>
	
	<br />
	
	<div class="sub-title">Product</div>
	<div class="products">
		<div class="product">
			<div ng-show="DATA.data_rsv.selected_products && DATA.data_rsv.selected_products.length > 0">
				<table class="table table-bordered table-condensed">
					<tr class="success" style="font-weight:bold">
						<td width="30">#</td>
						<td width="90">Date</td>
						<td>Product</td>
						<td width="150">Participant</td>
						<td align="right" width="100">Sub Total (<?=$vendor["default_currency"]?>)</td>
						<td width="25"></td>
						<td width="25"></td>
					</tr>
					<tbody ng-repeat="product in DATA.data_rsv.selected_products">
					<tr>
						<td>{{$index+1}}</td>
						<td align="center">
							<strong>{{fn.formatDate(product.date, "dd M yy")}}</strong>
						</td>
                        <td><strong>{{product.product.product_code}} - {{product.product.name}}</strong></td>
						<td width="100">
                        	<?php for($i=1;$i<=5;$i++){ ?>
                        	<div ng-show='product.qty_<?=$i?> && product.qty_<?=$i?> > 0'>
                            	{{product.rates.rates_caption.rates_<?=$i?>}} : <strong>{{product.qty_<?=$i?>}}</strong> {{product.rates.rates_unit.rates_<?=$i?>}}
                            </div>
                            <?php } ?>
                        </td>
						<td align="right">{{fn.formatNumber(product.sub_total, product.rates.currency)}}</td>
						<td><a href="" style="color:blue" ng-click="product.show_custom_price = (!product.show_custom_price)"><span class="glyphicon glyphicon-pencil"></span></a></td>
						<td><a href="" style="color:red" ng-click='remove_product($index)'><span class="glyphicon glyphicon-trash"></span></a></td>
					</tr>
					<tr ng-show='product.show_custom_price' class="warning">
						<td>&nbsp;</td>
						<td colspan="3" class="form-inline">
                        	<?php for($i=1;$i<=5;$i++){ ?>
                            	<span ng-show='product.qty_<?=$i?> && product.qty_<?=$i?> > 0'>
                                    {{product.rates.rates_caption.rates_<?=$i?>}} : 
                                    <input type="number" min="0" step="any" class="text-right" style="width:100px" ng-model='product.rates.rates.rates_<?=$i?>' ng-change="calculate_subtotal(product)" />
                                </span>
                            <?php } ?>
							<?php /*?>Adult : <input type="number" min="0" step="0.5" class="text-right" style="width:100px" ng-model='product.rates.rates.rates_1' ng-change="calculate_subtotal(product)" />
							Child : <input type="number" min="0" step="0.5" class="text-right" style="width:100px" ng-model='product.rates.rates.rates_2' ng-change="calculate_subtotal(product)" />
							Infant : <input type="number" min="0" step="0.5" class="text-right"  style="width:100px" ng-model='product.rates.rates.rates_3' ng-change="calculate_subtotal(product)" /><?php */?>
							<button type="button" ng-click="product.show_custom_price = (!product.show_custom_price)">OK</button>
						</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr ng-show="product.rates.pickup_area">
						<td>&nbsp;</td>
						<td colspan="3">
							<table class="table table-condensed table-borderless" style="background:none">
								<tr>
									<td width="120px"><strong>Pickup Service</strong></td>
									<td ng-init="area={'text':'Own Transport','nopickup':true, 'currency':trips.trips.currency}; product.rates.pickup_area.unshift(area); product.pickup.area = area">
										<select class="form-control input-sm" ng-model='product.pickup.area' ng-change='calculate_total()' 
											ng-options="((area.text)?area.text:(area.area+' - '+area.time+' - '+area.currency+' '+fn.formatNumber(area.price, area.currency))) for area in product.rates.pickup_area">
										</select>
									</td>
								</tr>
								<tr ng-show='!product.pickup.area.nopickup'>
									<td>Hotel/Villa Name*</td>
									<td><input ng-required="!product.pickup.area.nopickup" ng-disabled='product.pickup.area.nopickup' type="text" class="form-control input-sm" ng-model='product.pickup.hotel_name' placeholder="Hotel/Villa Name" /></td>
								</tr>
								<tr ng-show='!product.pickup.area.nopickup'>
									<td>Address*</td>
									<td><input ng-required="!product.pickup.area.nopickup" ng-disabled='product.pickup.area.nopickup' type="text" class="form-control input-sm" ng-model='product.pickup.hotel_address' placeholder="Hotel/Villa Address" /></td>
								</tr>
								<tr ng-show='!product.pickup.area.nopickup'>
									<td>Phone Number*</td>
									<td><input ng-required="!product.pickup.area.nopickup" ng-disabled='product.pickup.area.nopickup' type="text" class="form-control input-sm" ng-model='product.pickup.hotel_phone' placeholder="Hotel/Villa Phone Number" /></td>
								</tr>
							</table>
						</td>
						<td align="right">
							<span ng-show='!product.pickup.area.nopickup'>
								<span class="pickup_price" ng-show="product.pickup.area.type == 'way'">
									{{fn.formatNumber(product.pickup.area.price, product.rates.currency)}}
								</span>
								<span class="pickup_price" ng-show="product.pickup.area.type == 'pax'">
									{{fn.formatNumber((product.pickup.area.price * (product.qty_1 + trips.qty_2 + product.qty_3 + trips.qty_4 +  + trips.qty_5)), product.rates.currency)}}
								</span>
								<span class="pickup_price hidden-field">
									<input type="number" min="0" onblur="$(this).closest('tr').find('.pickup_price').toggle()" step="0.5" class="text-right"  style="width:100px" ng-model='product.pickup.area.price' ng-change="calculate_subtotal(product)" />
									/ {{product.pickup.area.type}}
								</span>
							</span>
						</td>
						<td align="center">
							<span ng-show='!product.pickup.area.nopickup'>
								<a href="" style="color:blue" onclick="$(this).closest('tr').find('.pickup_price').toggle()"><span class="glyphicon glyphicon-pencil"></span></a>
							</span>
						</td>
						<td></td>
					</tr>
					<tr ng-show="product.rates.dropoff_area">
						<td>&nbsp;</td>
						<td colspan="3">
							<table class="table table-condensed table-borderless" style="background:none">
								<tr>
									<td width="120px"><strong>Dropoff Service</strong></td>
									<td ng-init="area={'text':'Own Transport','nopickup':true, 'currency':product.rates.currency}; product.rates.dropoff_area.unshift(area); product.dropoff.area = area">
										<select class="form-control input-sm" ng-model='product.dropoff.area' ng-change='calculate_total()' 
											ng-options="((area.text)?area.text:(area.area+' - '+area.time+' - '+area.currency+' '+fn.formatNumber(area.price, area.currency))) for area in product.rates.dropoff_area">
										</select>
									</td>
								</tr>
								<tr ng-show='!product.dropoff.area.nopickup'>
									<td>Hotel/Villa Name*</td>
									<td><input ng-required="!product.dropoff.area.nopickup" ng-disabled='product.dropoff.area.nopickup' type="text" class="form-control input-sm" ng-model='product.dropoff.hotel_name' placeholder="Hotel/Villa Name" /></td>
								</tr>
								<tr ng-show='!product.dropoff.area.nopickup'>
									<td>Address*</td>
									<td><input ng-required="!product.dropoff.area.nopickup" ng-disabled='product.dropoff.area.nopickup' type="text" class="form-control input-sm" ng-model='product.dropoff.hotel_address' placeholder="Hotel/Villa Address" /></td>
								</tr>
								<tr ng-show='!product.dropoff.area.nopickup'>
									<td>Phone Number*</td>
									<td><input ng-required="!product.dropoff.area.nopickup" ng-disabled='product.dropoff.area.nopickup' type="text" class="form-control input-sm" ng-model='product.dropoff.hotel_phone' placeholder="Hotel/Villa Phone Number" /></td>
								</tr>
							</table>
						</td>
						<td align="right">
							<span ng-show='!product.dropoff.area.nopickup'>
								<span class="pickup_price" ng-show="product.dropoff.area.type == 'way'">
									{{fn.formatNumber(product.dropoff.area.price, product.rates.currency)}}
								</span>
								<span class="pickup_price" ng-show="product.dropoff.area.type == 'pax'">
                                	{{fn.formatNumber((product.dropoff.area.price * (product.qty_1 + trips.qty_2 + product.qty_3 + trips.qty_4 +  + trips.qty_5)), product.rates.currency)}}
								</span>
								<span class="pickup_price hidden-field">
									<input type="number" min="0" onblur="$(this).closest('tr').find('.pickup_price').toggle()" step="0.5" class="text-right"  style="width:100px" ng-model='product.dropoff.area.price' ng-change="calculate_subtotal(product)" />
									/ {{product.pickup.area.type}}
								</span>
							</span>
							<?php /*?><span ng-show='trips.dropoff.area.price > 0'>
								{{fn.formatNumber(trips.dropoff.area.price, trips.trips.currency)}}
							</span><?php */?>
						</td>
						<td>
							<span ng-show='!product.dropoff.area.nopickup'>
								<a href="" style="color:blue" onclick="$(this).closest('tr').find('.pickup_price').toggle()"><span class="glyphicon glyphicon-pencil"></span></a>
							</span>
						</td>
						<td></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan="6">
							<strong>Additional Service</strong>
							<table class="table table-condensed" style="background:none;">
								<tr ng-repeat='add_srv in product.additional_service'>
									<td><input required="required" type="text" class="form-control input-sm" ng-model='add_srv.name' placeholder="Additional Service Name" title="Additional Service Name" /></td>
									<td width="60"><input type="number" ng-change='calculate_total()' class="form-control input-sm text-right" ng-model='add_srv.qty' min="1" step="1" title="Qty." /></td>
									<td width="180">
                                    	<div class="input-group">
                                        	<span class="input-group-addon" id="basic-addon1"><?=$vendor["default_currency"]?></span>
                                        	<input type="number" ng-change='calculate_total()' class="form-control input-sm text-right" ng-model='add_srv.price' min="0" step="0.5" title="Price" />
                                        </div>
                                    </td>
									<td width="100" align="right">
										{{fn.formatNumber((add_srv.qty*add_srv.price), product.rates.currency)}}
									</td>
									<td></td>
									<td width="20" align="right"><a href="" style="color:red" ng-click='removeAdditionalService(product, $index)'><span class="glyphicon glyphicon-trash"></span></a></td>
								</tr>
							</table>
							<a href="" ng-click='addAdditionalService(product)'><span class="glyphicon glyphicon-plus"></span> Add</a>
						</td>
					</tr>
					</tbody>
					<tr>
						<td colspan="7"></td>
					</tr>
					<tr>
						<td colspan="4" align="right">Total (<?=$vendor["default_currency"]?>)</td>
						<td align="right">{{fn.formatNumber(DATA.data_rsv.TOTAL.total, "<?=$vendor["default_currency"]?>")}}</td>
						<td></td>
						<td></td>
					</tr>
                    <tr>
						<td colspan="4"class="form-inline">
							<strong>Discount :</strong>
							<div class="pull-right">
								Type:
								<select style="width:auto" class="form-control input-sm" ng-model='DATA.data_rsv.discount_type' ng-change='calculate_total()'>
									<option value="%">%</option>
									<option value="FIX"><?=$vendor["default_currency"]?></option>
								</select>
								<span>
									&nbsp;&nbsp;&nbsp;
									Amount:
									<div class="input-group">
										<span class="input-group-addon" ng-show="DATA.data_rsv.discount_type!='%'"><?=$vendor["default_currency"]?></span>
										<input class="form-control input-sm" type="number" min="0" max="{{(DATA.data_rsv.discount_type=='%')?100:DATA.data_rsv.TOTAL.total}}" step="0.1" style="width:{{(DATA.data_rsv.discount_type=='%')?80:150}}px; text-align:right" ng-model='DATA.data_rsv.discount_amount' ng-change='calculate_total()' />
										<span class="input-group-addon" ng-show="DATA.data_rsv.discount_type=='%'">%</span>
									</div>
								</span>
							</div>
						</td>
						<td align="right">
							{{fn.formatNumber(DATA.data_rsv.TOTAL.discount, "<?=$vendor["default_currency"]?>")}}
						</td>
						<td></td>
						<td></td>
					</tr>
                    <tr>
						<td colspan="4"class="form-inline">
							<strong>Tax & Service :</strong>
							<div class="pull-right">
								Type:
								<select style="width:auto" class="form-control input-sm" ng-model='DATA.data_rsv.tax_service_type' ng-change='calculate_total()'>
									<option value="%">%</option>
									<option value="FIX"><?=$vendor["default_currency"]?></option>
								</select>
								<span>
									&nbsp;&nbsp;&nbsp;
									Amount:
									<div class="input-group">
										<span class="input-group-addon" ng-show="DATA.data_rsv.tax_service_type!='%'"><?=$vendor["default_currency"]?></span>
										<input class="form-control input-sm" type="number" min="0" max="{{(DATA.data_rsv.tax_service_type=='%')?100:DATA.data_rsv.TOTAL.total}}" step="0.1" style="width:{{(DATA.data_rsv.tax_service_type=='%')?80:150}}px; text-align:right" ng-model='DATA.data_rsv.tax_service_amount' ng-change='calculate_total()' />
										<span class="input-group-addon" ng-show="DATA.data_rsv.tax_service_type=='%'">%</span>
									</div>
								</span>
							</div>
						</td>
						<td align="right">
							{{fn.formatNumber(DATA.data_rsv.TOTAL.tax_service, "<?=$vendor["default_currency"]?>")}}
						</td>
						<td></td>
						<td></td>
					</tr>
					<tr ng-show="DATA.data_rsv.booking_status!='COMPLIMEN'" class="info" style="font-weight:bold">
						<td colspan="4" align="right">GRAND TOTAL (<?=$vendor["default_currency"]?>)</td>
						<td align="right">{{fn.formatNumber(DATA.data_rsv.TOTAL.grand_total, "<?=$vendor["default_currency"]?>")}}</td>
						<td></td>
						<td></td>
					</tr>
                    <tr ng-show="DATA.data_rsv.booking_status=='COMPLIMEN'" class="success" style="font-weight:bold;color:green;">
						<td colspan="4" align="right">COMPLIMENTARY GRAND TOTAL (<?=$vendor["default_currency"]?>)</td>
						<td align="right">{{fn.formatNumber(DATA.data_rsv.TOTAL.grand_total, "<?=$vendor["default_currency"]?>")}}</td>
						<td></td>
						<td></td>
					</tr>
                    
                    <?php //COMMISSION FOR AGENT ?>
                    <tr ng-show='DATA.data_rsv.booking_source == "AGENT" && DATA.data_rsv.booking_status!="COMPLIMEN"'>
						<td colspan="7">&nbsp;</td>
					</tr>
                    <tr class="warning" ng-show='DATA.data_rsv.booking_source == "AGENT" && DATA.data_rsv.booking_status!="COMPLIMEN"'>
                    	<td colspan="4"class="form-inline">
                        	<div ng-show="DATA.data_rsv.add_commission_for_agent != '1'">
                            	<label><input type="checkbox" ng-model='DATA.data_rsv.add_commission_for_agent' ng-true="1" ng-false='0' /> Additional Commission</label>
                            </div>
                        	<div ng-show="DATA.data_rsv.add_commission_for_agent == '1'">
                                <div class="pull-right">
                                    Type:
                                    <select style="width:auto" class="form-control input-sm" ng-model='DATA.data_rsv.commission_type' ng-change='calculate_total()' 
                                    	ng-disabled="DATA.data_rsv.add_commission_for_agent != '1' || DATA.data_rsv.booking_status=='COMPLIMEN'">
                                        <option value="%">%</option>
                                        <option value="FIX"><?=$vendor["default_currency"]?></option>
                                    </select>
                                    <span>
                                        &nbsp;&nbsp;&nbsp;
                                        Amount:
                                        <div class="input-group">
                                            <span class="input-group-addon" ng-show="DATA.data_rsv.commission_type!='%'"><?=$vendor["default_currency"]?></span>
                                            <input class="form-control input-sm" type="number" min="0" max="{{(DATA.data_rsv.commission_type=='%')?100:DATA.data_rsv.TOTAL.total}}" step="0.1" style="width:{{(DATA.data_rsv.commission_type=='%')?80:150}}px; text-align:right" ng-disabled="DATA.data_rsv.add_commission_for_agent != '1'" ng-model='DATA.data_rsv.commission_amount' ng-change='calculate_total()' />
                                            <span class="input-group-addon" ng-show="DATA.data_rsv.commission_type=='%'">%</span>
                                        </div>
                                    </span>
                                </div>
                                <div>
                                    Additional Commission : <br />
                                    <strong>{{DATA.data_rsv.agent.name}}</strong><hr style="margin:5px 0" />
                                    &nbsp;Note :
                                    <input type="text" class="form-control input-sm" maxlength="200" placeholder="Note" ng-model='DATA.data_rsv.commission_remarks' 
                                    	ng-disabled="DATA.data_rsv.add_commission_for_agent != '1' || DATA.data_rsv.booking_status=='COMPLIMEN'" style="display:block; width:100%" />
                                </div>
                            </div>
						</td>
						<td align="right">
                        	<div ng-show="DATA.data_rsv.add_commission_for_agent == '1'">
								{{fn.formatNumber(DATA.data_rsv.total_commission, "<?=$vendor["default_currency"]?>")}}
                            </div>
						</td>
						<td></td>
						<td>
                        	<div ng-show="DATA.data_rsv.add_commission_for_agent == '1'">
                        		<a href="" style="color:red" ng-click="DATA.data_rsv.add_commission_for_agent='0'"><span class="glyphicon glyphicon-trash"></span></a>
                        	</div>
                        </td>
                    </tr>
                    <?php //--END COMMISSION FOR AGENT ?>
                    
				</table>
				<hr />
			</div>
			<div class="text-center">
				<a href="" xdata-toggle="modal" xdata-target="#modal-add-product" ng-click="add_new_product()">
					<button type="button" class="btn btn-info"><i class="fa fa-plus"></i> Add Product</button>
				</a>
			</div>
		</div>
	</div>
	
	<br />
	<div ng-show="DATA.data_rsv.selected_products && DATA.data_rsv.selected_products.length > 0"> 
	<!-- jika sudah memilih trip -->
	<div class="sub-title">Guest Information</div>
	<table class="table">
		<tr>
			<td width="150">Name*</td>
			<td>
			
				<input type="text" placeholder="First Name" class="form-control input-md" ng-model='DATA.data_rsv.customer.first_name' style="display:inline; width:48.5%" />
				&nbsp;&nbsp;&nbsp;
				<input type="text" placeholder="Last Name" class="form-control input-md" ng-model='DATA.data_rsv.customer.last_name' style="display:inline; width:48.5%" />
			</td>
		</tr>
		<tr>
			<td>Email</td>
			<td><input type="text" placeholder="Email" class="form-control input-md" ng-model='DATA.data_rsv.customer.email' /></td>
		</tr>
		<tr>
			<td>Phone / Mobile</td>
			<td><input type="text" placeholder="Phone / Mobile" class="form-control input-md" ng-model='DATA.data_rsv.customer.phone' /></td>
		</tr>
		<tr>
			<td>Country*</td>
			<td>
				<select class="form-control input-md" ng-model="DATA.data_rsv.customer.country_code">
					<option value="">-- Select Country --</option>
					<option value="{{country.code}}" ng-repeat="country in DATA.country_list.country_list | orderBy : 'name'">{{country.name}}</option>
				</select>
			</td>
		</tr>
	</table>
	
	<br />
	
	<div class="sub-title">Remarks / Special Request</div>
	<table class="table">
		<tr>
			<td width="150"></td>
			<td><textarea placeholder="Description" class="form-control input-md autoheight" ng-model='DATA.data_rsv.remarks' rows="3"></textarea></td>
		</tr>
	</table>
	
    
    
	<div ng-show="DATA.data_rsv.booking_status != 'COMPLIMEN'">
		<br />
		<div class="sub-title">
			Payment
		</div>
		
		<table class="table table-borderless table-condenseds" <?php /*?>ng-show='!agent_allow_to_use_acl'<?php */?>>
			<tr>
				<td width="150"></td>
				<td><label><input type="checkbox" ng-model='DATA.data_rsv.add_payment' ng-true-value='1' ng-false-value='0' /> &nbsp;&nbsp;ADD PAYMENT</label></td>
			</tr>
		
		</table>
        <?php /*?><table class="table table-borderless table-condenseds" ng-show='agent_allow_to_use_acl'>
			<tr>
				<td width="150"></td>
				<td>
                    <label><input type="radio" ng-model="agent_payment_use_acl" ng-value='0' ng-click="DATA.data_rsv.add_payment = '1'" /> &nbsp;&nbsp;ADD PAYMENT</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <label><input type="radio" ng-model="agent_payment_use_acl" ng-value='1' ng-click="DATA.data_rsv.add_payment = '0'" /> 
                    	Use Agent Credit Limit
                        (Current Limit : {{DATA.data_rsv.agent.out_standing_invoice.currency}} {{fn.formatNumber(DATA.data_rsv.agent.out_standing_invoice.current_limit,DATA.data_rsv.agent.out_standing_invoice.currency)}})
                    </label>
                </td>
			</tr>
		</table><?php */?>
        
		<table class="table table-borderless table-condenseds" ng-show="DATA.data_rsv.add_payment=='1'">
			<tr>
				<td width="150">Payment Type* </td>
				<td>
                    <select ng-required="DATA.data_rsv.add_payment=='1'" class="form-control input-md" ng-model='DATA.data_rsv.payment.payment_type' ng-change='changePaymentTypeInNewBookingForm()'>
                        <option value="" disabled="disabled">-- Select Payment Type --</option>
                        <option value="{{payment_method.code}}" ng-repeat='payment_method in $root.DATA_payment_method' ng-show="payment_method.code != 'ONLINE' && payment_method.code != 'OPENVOUCHER'">
                            {{payment_method.name}}
                        </option>
                        <option value="ACL" ng-show='agent_allow_to_use_acl' ng-disabled="DATA.data_rsv.TOTAL.grand_total > DATA.data_rsv.agent.out_standing_invoice.current_limit" ng-class="{'danger':(DATA.data_rsv.TOTAL.grand_total > DATA.data_rsv.agent.out_standing_invoice.current_limit)}">
                        	<p>Agent Credit Limit
                            (Credit Remaining : {{DATA.data_rsv.agent.out_standing_invoice.currency}} {{fn.formatNumber(DATA.data_rsv.agent.out_standing_invoice.current_limit,DATA.data_rsv.agent.out_standing_invoice.currency)}})
                            {{(DATA.data_rsv.TOTAL.grand_total > DATA.data_rsv.agent.out_standing_invoice.current_limit)?<?="'<-- Over Limit'"?>:""}}
                        	</p>
                        </option>
                        <option value="DEPOSIT" ng-show='agent_allow_to_use_deposit' ng-disabled="(deposit.current_deposit < DATA.data_rsv.TOTAL.grand_total)">
								Deposit Payment <span ng-show="deposit">(Deposit Remaining: 
								{{deposit.currency}} {{fn.formatNumber(deposit.current_deposit, deposit.currency)}})</span> 

								{{(DATA.data_rsv.TOTAL.grand_total > (deposit.current_deposit))?<?="'<-- Insufficient Deposit'"?>:""}}
						</option>

                    </select>
					<?php /* <!-- <div ng-show='error_deposit' class="alert alert-danger">Insufficient Deposit</div> -->
					<!-- <div ng-show="DATA.data_rsv.payment.payment_type == 'DEPOSIT'" class="text-left" style="padding: 10px 0;">
						<table class="table table-bordered table-payment" style="padding: 10px;">
							<tr ng-class="{'danger':(total_dep - usd_diposit<DATA.data_rsv.payment.payment_amount), 'info':(total_dep - usd_diposit>=DATA.data_rsv.payment.payment_amount)}">
								<td width="100"><strong>Total Deposit</strong></td>
								<td><strong> : {{currency}} {{fn.formatNumber(total_dep - usd_diposit, currency)}}</strong></td>
							</tr>
						</table>
					</div> --> */ ?>
				</td>
			</tr>
			<tr ng-show="DATA.data_rsv.payment.payment_type=='OPENVOUCHER'" class="header">
				<td>Open Voucher Code*</td>
				<td>
					<table width="100%">
						<tr ng-repeat='ov_code in DATA.data_rsv.payment.openvoucher_code' class="form-inline">
							<td width="30">{{($index+1)}}.</td>
							<td>
								<input ng-required="DATA.data_rsv.payment.payment_type=='OPENVOUCHER'" placeholder="Open Voucher Code {{($index+1)}}" type="text" class="form-control input-sm" ng-model='ov_code.code' style="width:200px" ng-blur="checkValidateOpenVoucherCode(ov_code)" />
								<span ng-show="ov_code.valid" style="color:green" class="glyphicon glyphicon-ok"></span>
								<span ng-show="ov_code.invalid" style="color:red" class="glyphicon glyphicon-remove"></span>
								<span ng-show="ov_code.loading"><small><em>Loading...</em></small></span>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<?php /*?><tr ng-show="DATA.data_rsv.payment.payment_type=='CC' || DATA.data_rsv.payment.payment_type=='ATM'" class="header"><?php */?>
            <tr ng-show="DATA.payment_is_cc" class="header">
				<td>
                	<span ng-show="DATA.payment_is_cc">Card Number</span>
                    <span ng-show="DATA.payment_is_atm">Account Number</span>
                </td>
				<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.data_rsv.payment.account_number' /></td>
			</tr>
			<?php /*?><tr ng-show="DATA.data_rsv.payment.payment_type=='CC' || DATA.data_rsv.payment.payment_type=='ATM'" class="header"><?php */?>
			<tr ng-show="DATA.payment_is_cc" class="header">
            	<td>Name On Card</td>
				<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.data_rsv.payment.name_on_card' /></td>
			</tr>
			<?php /*?><tr ng-show="DATA.data_rsv.payment.payment_type=='CC' || DATA.data_rsv.payment.payment_type=='ATM'" class="header"><?php */?>
            <tr ng-show="DATA.payment_is_cc" class="header">
				<td>Bank Name</td>
				<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.data_rsv.payment.bank_name' /></td>
			</tr>
			<?php /*?><tr ng-show="DATA.data_rsv.payment.payment_type=='CC' || DATA.data_rsv.payment.payment_type=='ATM'" class="header"><?php */?>
            <tr ng-show="DATA.payment_is_cc" class="header">
				<td>Approval Number</td>
				<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.data_rsv.payment.payment_reff_number' /></td>
			</tr>
			<tr ng-show="DATA.data_rsv.payment.payment_type!='ACL' && DATA.data_rsv.payment.payment_type!='DEPOSIT'">
				<td>Remarks</td>
				<td><input placeholder="Description" type="text" class="form-control input-md" ng-model='DATA.myPayment.description' /></td>
			</tr>
			<tr>
				<td>Payment Amount*</td>
				<td>
					<div class="input-group">
						<span class="input-group-addon" style="width:80px"><?=$vendor["default_currency"]?></span>
						<input placeholder="Payment Amount" min='0' step="any" ng-required="DATA.data_rsv.add_payment=='1'" type="number" min="0" class="form-control input-md" ng-model='DATA.data_rsv.payment.payment_amount' style="width:160px" ng-blur="convert_currency()"
							ng-disabled="DATA.data_rsv.payment.payment_type == 'ACL' || DATA.data_rsv.payment.payment_type == 'DEPOSIT' " ng-change='DATA.data_rsv.payment.balance = DATA.data_rsv.TOTAL.grand_total - DATA.data_rsv.payment.payment_amount'/>
					</div>
				</td>
			</tr>
            <tr ng-show="DATA.data_rsv.payment.payment_type!='OPENVOUCHER' && $root.DATA_available_currency.currency">
                <td>Paid In*</td>
                <td>
                    <div class="input-group" ng-show='$root.DATA_available_currency.currency'>
                        <select class="form-control input-md" ng-model='DATA.data_rsv.payment.payment_currency' style="width:80px" ng-change="convert_currency()" ng-disabled="!$root.DATA_available_currency.currency">
                            <option ng-repeat="crr in $root.DATA_available_currency.currency" value="{{crr}}">{{crr}}</option>
                        </select>
                        <input placeholder="Payment Amount" disabled="disabled" type="number" min="0" step="0.1" class="form-control input-md payment_amount" ng-model='DATA.data_rsv.payment.payment_amount_convertion' style="width:160px" />
                    </div>
                    <div ng-show='DATA.currency_converter.bookkeeping_rates' style="margin:10px 0">
                        {{DATA.currency_converter.bookkeeping_rates.from.currency}} {{DATA.currency_converter.bookkeeping_rates.from.amount}} = 
                        {{DATA.currency_converter.bookkeeping_rates.to.currency}} {{DATA.currency_converter.bookkeeping_rates.to.amount}}
                    </div>
                    <em ng-show='DATA.data_rsv.payment.payment_currency_loading'>Loading...</em>
                </td>
            </tr>
			<tr ng-show="DATA.data_rsv.payment.payment_type!='ACL' && DATA.data_rsv.payment.payment_type!='DEPOSIT'">
				<td>Balance</td>
				<td>
					<div class="input-group">
						<span class="input-group-addon" style="width:80px"><?=$vendor["default_currency"]?></span>
						<span class="form-control input-md" style="width:160px">{{fn.formatNumber(DATA.data_rsv.payment.balance, "<?=$vendor["default_currency"]?>")}}</span>
					</div>
				</td>
			</tr>
		</table>
		
	</div>
	
	<hr />
	<div class="text-center">
		<button class="btn btn-lg btn-primary">Submit Booking</button>
	</div>

</form>
</div>



	<?php /*?>
{{DATA.data_rsv}}
<hr />
{{DATA_RSV}}<?php */?>
	
	<?php $this->load->view("crs/reservation/v_act_reservation_new_add_product") ?>

<script>
    window.onbeforeunload = function(e) {
		//return 'Are you sure you want to leave?';
       //return 'Sure?';
    };
</script>
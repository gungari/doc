<!-- 	
<div class="sub-title"> Reservation Detail </div>
<br />
<div ng-init="editDataBookingTransportDetail();" class="reservation-detail">

	<div ng-show='!(DATA.current_booking)'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	
	<div ng-show='(DATA.current_booking)'>
		<div class="title">
			<h1>Edit - #{{DATA.current_booking.booking.booking_code}}</h1>
		</div>

		<div ui-view>
			<div class="sub-title"> Booking Information </div>
			<table class="table">
				<tr>
					<td width="130">Booking Code</td>
					<td><strong>{{DATA.current_booking.booking.booking_code}}</strong></td>
				</tr>
				<tr>
					<td>Booking Date</td>
					<td><strong>{{fn.formatDate(DATA.current_booking.booking.transaction_date, "dd MM yy")}}</strong></td>
				</tr>
				<tr>
					<td>Booking Status</td>
					<td class="text-capitalize">
						<strong><span ng-class="{'label label-danger':DATA.current_booking.booking.status_code == 'CANCEL', 'label label-warning':DATA.current_booking.booking.status_code == 'UNDEFINITE'}">
							{{DATA.current_booking.booking.status.toLowerCase()}}
						</span></strong>
					</td>
				</tr>
				<tr ng-show='DATA.current_booking.booking.cancelation'>
					<td>Cancelation Date</td>
					<td><strong>{{fn.formatDate(DATA.current_booking.booking.cancelation.date, "dd MM yy")}}</strong></td>
				</tr>
				<tr ng-show='DATA.current_booking.booking.cancelation'>
					<td>Cancelation Reason</td>
					<td><strong>{{DATA.current_booking.booking.cancelation.cancelation_reason}}</strong></td>
				</tr>
				<tr>
					<td>Booking Source</td>
					<td class="text-capitalize"><strong>{{DATA.current_booking.booking.source.toLowerCase()}}</strong></td>
				</tr>
				<tr ng-show="DATA.current_booking.booking.agent">
					<td>Agent</td>
					<td class="text-capitalize"><strong>{{DATA.current_booking.booking.agent.name}}</strong></td>
				</tr>
			</table>	
			<br />
			<div class="sub-title">
				<div class="pull-right">
					<a href="" data-toggle="modal" data-target="#modal-add-cust" ng-click="edit_customer()" style="text-transform: capitalize;"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
				</div>
				Customer Information
			</div>
			<table class="table">
				<tr>
					<td width="130">Full Name</td>
					<td><strong>{{DATA.current_booking.booking.customer.first_name}} {{DATA.current_booking.booking.customer.last_name}}</strong></td>
				</tr>
				<tr>
					<td>Email</td>
					<td><strong>{{DATA.current_booking.booking.customer.email}}</strong></td>
				</tr>
				<tr>
					<td>Country</td>
					<td><strong>{{DATA.current_booking.booking.customer.country_name}}</strong></td>
				</tr>
				<tr>
					<td>Telephone</td>
					<td><strong>{{DATA.current_booking.booking.customer.phone}}</strong></td>
				</tr>
			</table>
			<br />
			
			<div class="sub-title">
				Remarks / Special Request
			</div>
			<table class="table">
				<tr>
					<td width="130">Remarks</td>
					<td>
						<div ng-show='DATA.current_booking.booking.remarks_is_edit'>
	                    	<textarea placeholder="Description" class="form-control input-md autoheight" id="txt-remarks"
                            	ng-model='DATA.current_booking.booking.remarks' 
                                ng-blur="update_remarks(); DATA.current_booking.booking.remarks_is_edit = false;"
                                rows="3" required="required" style="text-align: justify;"></textarea>
                        </div>
                    	<div ng-hide='DATA.current_booking.booking.remarks_is_edit'>
	                    	<span class="pull-right">
                            	<a href="" ng-click="DATA.current_booking.booking.remarks_is_edit = true; fn.focus('#txt-remarks');"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
                            </span>
    	                    <strong>{{DATA.current_booking.booking.remarks}}</strong>
                        </div>
					</td>
				</tr>
			</table>
			<br />
			
			<div class="sub-title"> 
				Booking Details 
			</div>
			<table class="table table-bordered">
				<tr class="info">
					<td><strong>Description</strong></td>
					<td width="150" align="right"><strong>Sub Total</strong></td>
				</tr>
				<tbody ng-repeat='detail in DATA.current_booking.booking.detail' ng-init="loadDataRate(detail.rates.id, detail)">
					<tr>
						<td>
							<div class="pull-right text-right">
								Voucher#
								<div style="font-size:20px">
									<a href="<?=site_url("home/print_page/#/print/voucher_trans/")?>{{DATA.current_booking.booking.booking_code}}/{{detail.voucher_code}}" target="_blank">{{detail.voucher_code}}</a>
								</div>
							</div>
							<div>
								<strong>{{detail.departure.port.name}} ({{detail.departure.port.port_code}}) : {{detail.departure.time}}</strong>
								&nbsp;&nbsp;<i class="fa fa-chevron-right"></i>&nbsp;&nbsp;
								<strong>{{detail.arrival.port.name}} ({{detail.arrival.port.port_code}}) : {{detail.arrival.time}}</strong>
							</div>
							<div><small>({{detail.rates.name}})</small></div>
							<div><strong>{{fn.formatDate(detail.date, "dd MM yy")}}</strong></div>
							<div>
								<span ng-show='detail.qty_1 > 0'>{{detail.qty_1}} Adult</span>
								<span ng-show='detail.qty_2 > 0'>{{detail.qty_2}} Child</span>
								<span ng-show='detail.qty_3 > 0'>{{detail.qty_3}} Infant</span>
							</div>
							
							<div class="pull-right">
								<a href="" data-toggle="modal" data-target="#modal-add-trip" ng-click="edit_trip(detail)"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
								&nbsp;&nbsp;&nbsp;
                                <a href="" data-toggle="modal" data-target="#modal-split-voucher" ng-click="split_voucher(detail)"><i class="fa fa-files-o" aria-hidden="true"></i> Split Voucher</a>
							</div>
						</td>
						<td align="right">
							<strong>
								{{detail.rates.currency}}
								{{fn.formatNumber((detail.rates.rates_1 * detail.qty_1) + (detail.rates.rates_2 * detail.qty_2) + (detail.rates.rates_3 * detail.qty_3), detail.rates.currency)}}
							</strong>
						</td>
					</tr>
					<tr ng-show="!detail.pickup && DATA.service.is_pickup">
						<td>
                        	<div class="pull-right">
                                <a href="" data-toggle="modal" data-target="#modal-add-picdrp" ng-click="add_pickup_dropoff('PICKUP', detail)"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                            </div>
                            
							<a href="" onclick="$(this).parent('td').find('table').toggle()">Pickup Service (-)</a>
						</td>
					</tr>
					<tr ng-show="!detail.dropoff && DATA.service.is_dropoff">
						<td>
                        	<div class="pull-right">
                                <a href="" data-toggle="modal" data-target="#modal-add-picdrp" ng-click="add_pickup_dropoff('DROPOFF', detail)"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                            </div>
                            
							<a href="" onclick="$(this).parent('td').find('table').toggle()">Dropoff Service (-)</a>
						</td>
					</tr>
					<tr ng-show='detail.pickup'>
						<td>
							<div class="pull-right">
                                <a href="" data-toggle="modal" data-target="#modal-add-picdrp" ng-click="edit_pickup_dropoff('PICKUP', detail)"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                            </div>
							
							<a href="" onclick="$(this).parent('td').find('table').toggle()">Pickup Service ({{detail.pickup.area}} - {{detail.pickup.time}})</a>
							<table class="table table-borderless table-condensed hidden-field">
								<tr>
									<td width="130">Hotel Name</td>
									<td><strong>{{detail.pickup.hotel_name}}</strong></td>
								</tr>
								<tr>
									<td>Hotel Addres</td>
									<td><strong>{{detail.pickup.hotel_address}}</strong></td>
								</tr>
								<tr>
									<td>Hotel Phone Number</td>
									<td><strong>{{detail.pickup.hotel_phone_number}}</strong></td>
								</tr>
							</table>
						</td>
						<td align="right">
							<strong ng-show='detail.pickup.price_pickup > 0'>
								{{detail.rates.currency}}
								{{fn.formatNumber(detail.pickup.price_pickup,detail.rates.currency)}}
							</strong>
						</td>
					</tr>
					<tr ng-show='detail.dropoff'>
						<td>
							<div class="pull-right">
                                <a href="" data-toggle="modal" data-target="#modal-add-picdrp" ng-click="edit_pickup_dropoff('DROPOFF', detail)"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                            </div>

							<a href="" onclick="$(this).parent('td').find('table').toggle()">Dropoff Service ({{detail.dropoff.area}} - {{detail.dropoff.time}})</a>
							<table class="table table-borderless table-condensed hidden-field">
								<tr>
									<td width="130">Hotel Name</td>
									<td><strong>{{detail.dropoff.hotel_name}}</strong></td>
								</tr>
								<tr>
									<td>Hotel Addres</td>
									<td><strong>{{detail.dropoff.hotel_address}}</strong></td>
								</tr>
								<tr>
									<td>Hotel Phone Number</td>
									<td><strong>{{detail.dropoff.hotel_phone_number}}</strong></td>
								</tr>
							</table>
						</td>
						<td align="right">
							<strong ng-show='detail.dropoff.price_dropoff > 0'>
								{{detail.rates.currency}}
								{{fn.formatNumber(detail.dropoff.price_dropoff,detail.rates.currency)}}
							</strong>
						</td>
					</tr>
					<tr ng-show='detail.additional_service' ng-repeat='additional_service in detail.additional_service'>
						<td>{{additional_service.name}} ({{additional_service.qty}}x @ {{detail.rates.currency}} {{fn.formatNumber(additional_service.price,detail.rates.currency)}})</td>
						<td align="right">
							<strong>
							{{detail.rates.currency}} {{fn.formatNumber((additional_service.price * additional_service.qty),detail.rates.currency)}}
							</strong>
						</td>
					</tr>
				</tbody>
				<tr class="success">
					<td align="right"><strong>Total</strong></td>
					<td align="right">
						<strong>
							{{DATA.current_booking.booking.currency}}
							{{fn.formatNumber(DATA.current_booking.booking.total_before_discount,DATA.current_booking.booking.currency)}}
						</strong>
					</td>
				</tr>
				<tr class="success">
					<td align="right">
						<strong>
							Discount 
							<span ng-show='DATA.current_booking.booking.discount.amount > 0'>
								&nbsp;&nbsp;
								<span ng-show="DATA.current_booking.booking.discount.type == '%'">({{DATA.current_booking.booking.discount.value}}%)</span>
								<span ng-show="DATA.current_booking.booking.discount.type != '%'">({{fn.formatNumber(DATA.current_booking.booking.discount.value,DATA.current_booking.booking.currency)}})</span>
							</span>
						</strong>
					</td>
					<td align="right">
						<strong>
							{{DATA.current_booking.booking.currency}}
							{{fn.formatNumber(DATA.current_booking.booking.discount.amount,DATA.current_booking.booking.currency)}}
						</strong>
					</td>
				</tr>
				<tr class="success">
					<td align="right"><strong>Grand Total</strong></td>
					<td align="right">
						<strong>
							{{DATA.current_booking.booking.currency}}
							{{fn.formatNumber(DATA.current_booking.booking.grand_total,DATA.current_booking.booking.currency)}}
						</strong>
					</td>
				</tr>
				<tr class="info">
					<td align="right"><strong>Total Payment</strong></td>
					<td align="right">
						<strong>
							{{DATA.current_booking.booking.currency}}
							({{fn.formatNumber(DATA.current_booking.booking.total_payment,DATA.current_booking.booking.currency)}})
						</strong>
					</td>
				</tr>
				<tr style="font-weight:bold" ng-class="{'danger':(DATA.current_booking.booking.balance>0), 'info':(DATA.current_booking.booking.balance<=0)}">
					<td align="right"><strong>Balance</strong></td>
					<td align="right">
						<strong>
							{{DATA.current_booking.booking.currency}}
							{{fn.formatNumber(DATA.current_booking.booking.balance,DATA.current_booking.booking.currency)}}
						</strong>
					</td>
				</tr>
			</table>
			
			<br />
			
			
			<br /><hr />
			<a ui-sref="trans_reservation.detail({'booking_code':DATA.current_booking.booking.booking_code})"> <i class="fa fa-chevron-left"></i> Back to Reservation Detail</a>
			
			<?php $this->load->view("crs/reservation/v_trans_reservation_edit_pickdrop") ?>
			<?php $this->load->view("crs/reservation/v_trans_reservation_edit_cust") ?>
			<?php $this->load->view("crs/reservation/v_trans_reservation_edit_trip") ?>
			<?php $this->load->view("crs/reservation/v_trans_reservation_edit_split_voucher") ?>
			
		</div>
	</div>
</div>
<style>
	.reservation-detail .title{margin-bottom:20px}
	.reservation-detail .title h1{margin-bottom:10px !important;}
	.reservation-detail .title .code{margin-bottom:5px;}
</style> -->

	
<div class="sub-title"> Reservation Detail </div>
<br />
<div ng-init="editDataBookingTransportDetail();" class="reservation-detail">

	<div ng-show='!(DATA.current_booking)'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	
	<div ng-show='(DATA.current_booking)'>
		<div class="title">
			<h1>Edit - #{{DATA.current_booking.booking.booking_code}}</h1>
		</div>

		<div ui-view>
			<div class="sub-title"> Booking Information </div>
			<table class="table">
				<tr>
					<td width="130">Booking Code</td>
					<td><strong>{{DATA.current_booking.booking.booking_code}}</strong></td>
				</tr>
				<tr>
					<td>Booking Date</td>
					<td><strong>{{fn.formatDate(DATA.current_booking.booking.transaction_date, "dd MM yy")}}</strong></td>
				</tr>
				<tr>
					<td>Booking Status</td>
					<td class="text-capitalize">
						<strong><span ng-class="{'label label-danger':DATA.current_booking.booking.status_code == 'CANCEL', 'label label-warning':DATA.current_booking.booking.status_code == 'UNDEFINITE'}">
							{{DATA.current_booking.booking.status.toLowerCase()}}
						</span></strong>
					</td>
				</tr>
				<tr ng-show='DATA.current_booking.booking.cancelation'>
					<td>Cancelation Date</td>
					<td><strong>{{fn.formatDate(DATA.current_booking.booking.cancelation.date, "dd MM yy")}}</strong></td>
				</tr>
				<tr ng-show='DATA.current_booking.booking.cancelation'>
					<td>Cancelation Reason</td>
					<td><strong>{{DATA.current_booking.booking.cancelation.cancelation_reason}}</strong></td>
				</tr>
				<tr>
					<td>Booking Source</td>
					<td class="text-capitalize"><strong>{{DATA.current_booking.booking.source.toLowerCase()}}</strong></td>
				</tr>
				<tr ng-show="DATA.current_booking.booking.agent">
					<td>Agent</td>
					<td class="text-capitalize"><strong>{{DATA.current_booking.booking.agent.name}}</strong></td>
				</tr>
			</table>	
			<br />
			<div class="sub-title">
            	<div class="pull-right">
					<a href="" data-toggle="modal" data-target="#modal-add-cust" ng-click="edit_customer()" style="text-transform: capitalize;"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
				</div>
				Customer Information
			</div>
			<table class="table">
				<tr>
					<td width="130">Full Name</td>
					<td><strong>{{DATA.current_booking.booking.customer.first_name}} {{DATA.current_booking.booking.customer.last_name}}</strong></td>
				</tr>
				<tr>
					<td>Email</td>
					<td><strong>{{DATA.current_booking.booking.customer.email}}</strong></td>
				</tr>
				<tr>
					<td>Country</td>
					<td><strong>{{DATA.current_booking.booking.customer.country_name}}</strong></td>
				</tr>
				<tr>
					<td>Telephone</td>
					<td><strong>{{DATA.current_booking.booking.customer.phone}}</strong></td>
				</tr>
			</table>
			<br />
			
			<div class="sub-title">
				Remarks / Special Request
			</div>
			<table class="table">
				<tr>
					<td width="130">Remarks</td>
					<td>
                    	<div ng-show='DATA.current_booking.booking.remarks_is_edit'>
	                    	<textarea placeholder="Description" class="form-control input-md autoheight" id="txt-remarks"
                            	ng-model='DATA.current_booking.booking.remarks' 
                                ng-blur="update_remarks(); DATA.current_booking.booking.remarks_is_edit = false;"
                                rows="3" required="required" style="text-align: justify;"></textarea>
                        </div>
                    	<div ng-hide='DATA.current_booking.booking.remarks_is_edit'>
	                    	<span class="pull-right">
                            	<a href="" ng-click="DATA.current_booking.booking.remarks_is_edit = true;"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
                            </span>
    	                    <strong>{{DATA.current_booking.booking.remarks}}</strong>
                        </div>
					</td>
				</tr>
			</table>
			<br />
			
			<div class="sub-title"> 
				Booking Details 
			</div>
			<table class="table table-bordered">
				<tr class="info">
					<td><strong>Description</strong></td>
					<td width="150" align="right"><strong>Sub Total</strong></td>
				</tr>
				<tbody ng-repeat='detail in DATA.current_booking.booking.detail' ng-init="loadDataRate(detail.rates.id, detail)">
					<tr>
						<td>
							<div class="pull-right text-right">
								Voucher#
								<div style="font-size:20px">
									<a href="<?=site_url("home/print_page/#/print/voucher_trans/")?>{{DATA.current_booking.booking.booking_code}}/{{detail.voucher_code}}" target="_blank">{{detail.voucher_code}}</a>
								</div>
							</div>
							<div>
								<strong>{{detail.departure.port.name}} ({{detail.departure.port.port_code}}) : {{detail.departure.time}}</strong>
								&nbsp;&nbsp;<i class="fa fa-chevron-right"></i>&nbsp;&nbsp;
								<strong>{{detail.arrival.port.name}} ({{detail.arrival.port.port_code}}) : {{detail.arrival.time}}</strong>
							</div>
							<div><small>({{detail.rates.name}})</small></div>
							<div><strong>{{fn.formatDate(detail.date, "dd MM yy")}}</strong></div>
							<div>
								<span ng-show='detail.qty_1 > 0'>{{detail.qty_1}} Adult</span>
								<span ng-show='detail.qty_2 > 0'>{{detail.qty_2}} Child</span>
								<span ng-show='detail.qty_3 > 0'>{{detail.qty_3}} Infant</span>
							</div>
							
							<div class="pull-right">
                            	<a href="" data-toggle="modal" data-target="#modal-add-trip" ng-click="edit_trip(detail)"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                                &nbsp;&nbsp;&nbsp;
                                <a href="" data-toggle="modal" data-target="#modal-split-voucher" ng-click="split_voucher(detail)"><i class="fa fa-files-o" aria-hidden="true"></i> Split Voucher</a>
                            </div>
						</td>
						<td align="right">
							<strong>
								{{detail.rates.currency}}
								{{fn.formatNumber((detail.rates.rates_1 * detail.qty_1) + (detail.rates.rates_2 * detail.qty_2) + (detail.rates.rates_3 * detail.qty_3), detail.rates.currency)}}
							</strong>

						</td>
					</tr>
					
						<tr ng-show="!detail.pickup && DATA.service.is_pickup">
							<td>
	                        	<div class="pull-right">
	                                <a href="" data-toggle="modal" data-target="#modal-add-picdrp" ng-click="add_pickup_dropoff('PICKUP', detail)"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
	                            </div>
	                            
								<a href="" onclick="$(this).parent('td').find('table').toggle()">Pickup Service (-)</a>
							</td>
						</tr>
						<tr ng-show="!detail.dropoff && DATA.service.is_dropoff">
							<td>
	                        	<div class="pull-right">
	                                <a href="" data-toggle="modal" data-target="#modal-add-picdrp" ng-click="add_pickup_dropoff('DROPOFF', detail)"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
	                            </div>
	                            
								<a href="" onclick="$(this).parent('td').find('table').toggle()">Dropoff Service (-)</a>
							</td>
						</tr>
					
					<tr ng-show='detail.pickup'>
						<td>
                        	<div class="pull-right">
                                <a href="" data-toggle="modal" data-target="#modal-add-picdrp" ng-click="edit_pickup_dropoff('PICKUP', detail)"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                            </div>
                            
							<a href="" onclick="$(this).parent('td').find('table').toggle()">Pickup Service ({{detail.pickup.area}} - {{detail.pickup.time}})</a>
							<table class="table table-borderless table-condensed hidden-field">
								<tr>
									<td width="130">Hotel Name</td>
									<td><strong>{{detail.pickup.hotel_name}}</strong></td>
								</tr>
								<tr>
									<td>Hotel Address</td>
									<td><strong>{{detail.pickup.hotel_address}}</strong></td>
								</tr>
								<tr>
									<td>Hotel Phone Number</td>
									<td><strong>{{detail.pickup.hotel_phone_number}}</strong></td>
								</tr>
							</table>
						</td>
						<td align="right">
							<strong ng-show='detail.pickup.price > 0'>
								{{detail.rates.currency}}
								{{fn.formatNumber(detail.pickup.price,detail.rates.currency)}}
							</strong>
						</td>
					</tr>
					<tr ng-show='detail.dropoff'>
						<td>
                        	<div class="pull-right">
                                <a href="" data-toggle="modal" data-target="#modal-add-picdrp" ng-click="edit_pickup_dropoff('DROPOFF', detail)"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                            </div>
                            
							<a href="" onclick="$(this).parent('td').find('table').toggle()">Dropoff Service ({{detail.dropoff.area}} - {{detail.dropoff.time}})</a>
							<table class="table table-borderless table-condensed hidden-field">
								<tr>
									<td width="130">Hotel Name</td>
									<td><strong>{{detail.dropoff.hotel_name}}</strong></td>
								</tr>
								<tr>
									<td>Hotel Address</td>
									<td><strong>{{detail.dropoff.hotel_address}}</strong></td>
								</tr>
								<tr>
									<td>Hotel Phone Number</td>
									<td><strong>{{detail.dropoff.hotel_phone_number}}</strong></td>
								</tr>
							</table>
						</td>
						<td align="right">
							<strong ng-show='detail.dropoff.price > 0'>
								{{detail.rates.currency}}
								{{fn.formatNumber(detail.dropoff.price,detail.rates.currency)}}
							</strong>
						</td>
					</tr>
					<tr ng-show='detail.additional_service' ng-repeat='additional_service in detail.additional_service'>
						<td>{{additional_service.name}} ({{additional_service.qty}}x @ {{detail.rates.currency}} {{fn.formatNumber(additional_service.price,detail.rates.currency)}})</td>
						<td align="right">
							<strong>
							{{detail.rates.currency}} {{fn.formatNumber((additional_service.price * additional_service.qty),detail.rates.currency)}}
							</strong>
						</td>
					</tr>
				</tbody>
				<tr class="success">
					<td align="right"><strong>Total</strong></td>
					<td align="right">
						<strong>
							{{DATA.current_booking.booking.currency}}
							{{fn.formatNumber(DATA.current_booking.booking.total_before_discount,DATA.current_booking.booking.currency)}}
						</strong>
					</td>
				</tr>
				<tr class="success">
					<td align="right">
						<strong>
							Discount 
							<span ng-show='DATA.current_booking.booking.discount.amount > 0'>
								&nbsp;&nbsp;
								<span ng-show="DATA.current_booking.booking.discount.type == '%'">({{DATA.current_booking.booking.discount.value}}%)</span>
								<span ng-show="DATA.current_booking.booking.discount.type != '%'">({{fn.formatNumber(DATA.current_booking.booking.discount.value,DATA.current_booking.booking.currency)}})</span>
							</span>
						</strong>
					</td>
					<td align="right">
						<strong>
							{{DATA.current_booking.booking.currency}}
							{{fn.formatNumber(DATA.current_booking.booking.discount.amount,DATA.current_booking.booking.currency)}}
						</strong>
					</td>
				</tr>
				<tr class="success">
					<td align="right"><strong>Grand Total</strong></td>
					<td align="right">
						<strong>
							{{DATA.current_booking.booking.currency}}
							{{fn.formatNumber(DATA.current_booking.booking.grand_total,DATA.current_booking.booking.currency)}}
						</strong>
					</td>
				</tr>
				<tr class="info">
					<td align="right"><strong>Total Payment</strong></td>
					<td align="right">
						<strong>
							{{DATA.current_booking.booking.currency}}
							({{fn.formatNumber(DATA.current_booking.booking.total_payment,DATA.current_booking.booking.currency)}})
						</strong>
					</td>
				</tr>
				<tr style="font-weight:bold" ng-class="{'danger':(DATA.current_booking.booking.balance>0), 'info':(DATA.current_booking.booking.balance<=0)}">
					<td align="right"><strong>Balance</strong></td>
					<td align="right">
						<strong>
							{{DATA.current_booking.booking.currency}}
							{{fn.formatNumber(DATA.current_booking.booking.balance,DATA.current_booking.booking.currency)}}
						</strong>
					</td>
				</tr>
			</table>
			
			<br />
			
			
			<br /><hr />
			<a ui-sref="trans_reservation.detail({'booking_code':DATA.current_booking.booking.booking_code})"> <i class="fa fa-chevron-left"></i> Back to Reservation Detail</a>
			
			<?php $this->load->view("crs/reservation/v_trans_reservation_edit_pickdrop") ?>
			<?php $this->load->view("crs/reservation/v_trans_reservation_edit_cust") ?>
			<?php $this->load->view("crs/reservation/v_trans_reservation_edit_trip") ?>
            <?php $this->load->view("crs/reservation/v_trans_reservation_edit_split_voucher") ?>
			
		</div>
	</div>
</div>
<style>
	.reservation-detail .title{margin-bottom:20px}
	.reservation-detail .title h1{margin-bottom:10px !important;}
	.reservation-detail .title .code{margin-bottom:5px;}
</style>
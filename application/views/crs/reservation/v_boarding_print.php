
<div ng-init='loadDataBoarding()'>
	<div class="normal-paper page">
		<div ng-repeat='passenger in DATA.voucher.detail' style="page-break-after:always;">
			<div>
						<div class="header">
							<div class="title">Boarding Pass</div>
							<div style="font-size: 10px"><?=$vendor["business_name"]?></div>
						</div>
						
						<div>
							<div>
								<img src="<?=base_url()?>public/plugin/barcode.php?text={{passenger.passenger.pass_code}}&size=50" />
							</div>
							
							<div ng-show="DATA.current_booking.booking.status_code=='CANCEL'" style='color:red'>[CANCELLED]</div>
						</div>
						<hr />
						<div>
							<strong>CUSTOMER INFORMATION</strong><br />
							<table>
								<tr>
									<td width="70">Name</td>
									<td><strong>{{passenger.passenger.first_name}} {{passenger.passenger.last_name}} 
										<span ng-show="passenger.passenger.gender == 'm'">
											(Male)
										</span>
										<span ng-show="passenger.passenger.gender == 'f'">
											(Female)
										</span>
									</strong></td>
								</tr>
								<tr>
									<td>Passcode</td>
									<td><strong>{{passenger.passenger.pass_code}}</strong></td>
								</tr>
							</table>
						</div>
						<hr />
						<div>
							<strong>VOUCHER INFORMATION</strong><br />
							<table>
								<tr>
									<td width="70">Date</td>
									<td><strong>{{fn.formatDate(passenger.date, "dd MM yy")}}</strong></td>
								</tr>
								<tr ng-show="passenger.product_type == 'TRANS'"> 
									<td>Boat Name</td> 
									<td> 
										<strong>{{passenger.schedule.boat.name}}</strong> 
									</td> 
								</tr> 
								<tr ng-show="passenger.product_type == 'TRANS'">
									<td>Trip</td>
									<td></td>
								</tr>
								<tr ng-show="passenger.product_type == 'TRANS'">
									<td style="font-size:14px" colspan="2">
										<strong>{{passenger.departure.port.name}} ({{passenger.departure.port.port_code}}) - {{passenger.departure.time}}</strong>
										<i class="fa fa-chevron-right"></i> <br>
										<strong>{{passenger.arrival.port.name}} ({{passenger.arrival.port.port_code}}) - {{passenger.arrival.time}}</strong>
									</td>
								</tr>
					          
							</table>
						</div>
				</div>
				<hr />
				<div>
					<table>
						<tr>
							<td class="td-sub">
								<div>
									<div style="font-size: 12px;"><b>Boarding Pass</b></div>
									<div style="font-size: 10px"><?=$vendor["business_name"]?></div>
								</div>
								<div style="font-size: 12px;">
									<b>{{passenger.passenger.pass_code}}</b>
								</div>
								
							</td>
							<td class="td-sub">
								<div class="sub-code">
									<table>
										<tr>
											<td width="80">Name <br><strong>{{passenger.passenger.first_name}} {{passenger.passenger.last_name}}<span ng-show="passenger.passenger.gender == 'm'">(Male)</span>
												<span ng-show="passenger.passenger.gender == 'f'">(Female)</span></strong></td>
										</tr>
										
									</table>
								</div>
							</td>
							
							
						</tr>
						<tr>
							<td class="td-sub">
								<div class="sub-code">
									<table>
										<tr>
											
											<td>
												Boat Name <br>
												<span style="font-size: 10px;"><strong>{{passenger.schedule.boat.name}}</strong> </span>
											</td>
										</tr>
									</table>

								</div>
							</td>
							<td class="td-sub">
								<div class="sub-code">
									<table>
									<tr>
										<td>
											Date <br>
											<strong>{{fn.formatDate(passenger.date, "dd MM yy")}}</strong>
										</td>
									</tr>
									
									</table>
								</div>
							</td>
							<td class="td-sub">
								<div style=" -webkit-transform: rotate(90deg);
														    -moz-transform: rotate(90deg);
														    -o-transform: rotate(90deg);
														    -ms-transform: rotate(90deg);
														    transform: rotate(90deg);">
									<img src="<?=base_url()?>public/plugin/barcode.php?text={{passenger.passenger.pass_code}}&size=40" />
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="3" class="td-sub">
								<strong>{{passenger.departure.port.name}} ({{passenger.departure.port.port_code}}) - {{passenger.departure.time}}</strong>
								<i class="fa fa-chevron-right"></i><br>
								<strong>{{passenger.arrival.port.name}} ({{passenger.arrival.port.port_code}}) - {{passenger.arrival.time}}</strong>
							</td>
						</tr>
					</table>
				</div>
		</div>
	</div>
</div>

<style type="text/css">
	
	.td-sub {
		padding-bottom: 5px;
		padding-right: 5px;
	}
	.sub-code {
		font-size: 10px;
	}
	.flip {

		  -ms-transform: rotate(90deg); /* IE 9 */
		  -webkit-transform: rotate(90deg); /* Safari 3-8 */
		  transform: rotate(90deg);
	}
	.page {
		/*height: 550px;*/
		width:8cm !important; margin:0 auto !important
	}

	@media print {
        html, body {
            width: 80mm;
        	height: 200mm;
        	padding: 0 !important;      
        }
        .page {
        	width: 80mm;
            margin: 0 !important; padding: 0 !important;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
</style>
<?php
//pre($vendor)
?>
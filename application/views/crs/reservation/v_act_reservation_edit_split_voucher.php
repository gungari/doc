<div class="modal fade" id="modal-split-voucher" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Split Voucher</h4>
            </div>
		  	<div class="modal-body products">
                <div class="product">
                	<div class="pull-right text-right">
                        <small>Adult:</small> <strong>{{current_edit_trip.rates.currency}} {{fn.formatNumber(current_edit_trip.rates.rates_1, current_edit_trip.rates.currency)}}</strong><br />
                        <small>Child:</small> <strong>{{current_edit_trip.rates.currency}} {{fn.formatNumber(current_edit_trip.rates.rates_2, current_edit_trip.rates.currency)}}</strong><br />
                        <small>Infant:</small> <strong>{{current_edit_trip.rates.currency}} {{fn.formatNumber(current_edit_trip.rates.rates_3, current_edit_trip.rates.currency)}}</strong><br />
                    </div>
                    <div>
                        <strong>{{current_edit_trip.product.name}}</strong>
                    </div>
                    <div><small>({{current_edit_trip.rates.name}})</small></div>
                    <div><strong>{{fn.formatDate(current_edit_trip.date, "dd MM yy")}}</strong></div>
                    <div>
                        <span ng-show='current_edit_trip.qty_1 > 0'>{{current_edit_trip.qty_1}} Adult</span>
                        <span ng-show='current_edit_trip.qty_2 > 0'>{{current_edit_trip.qty_2}} Child</span>
                        <span ng-show='current_edit_trip.qty_3 > 0'>{{current_edit_trip.qty_3}} Infant</span>
                    </div>
                    
                    <hr />
                    <div>
                    	<h4>Select passenger</h4>
                        <ol>
                        	<li ng-repeat="passenger in current_edit_trip.passenger">
                            	<label style="cursor:pointer; font-weight:{{(passenger.selected?'bold':'normal')}}" xxng-click="split_voucher(current_edit_trip)">
	                            	<input type="checkbox" ng-model="passenger.selected" />
    	                        	<span>{{passenger.first_name}} {{passenger.last_name}}</span>
                                </label>
                            </li>
                        </ol>
                    </div>
                    <hr />
                    
                    <form ng-submit='check_availabilities_split_voucher($event)'>
                        <div ng-show='add_trip.error_desc.length>0' class="alert alert-danger"><ul><li ng-repeat='err in add_trip.error_desc'>{{err}}</li></ul></div>
                        <table class="table table-borderless table-condensed">
                            <tr>
                                <td width="80">Select Date</td>
                                <td>
                                    <input type="text" class="form-control input-sm datepicker" required="required" placeholder="Select Date" ng-model='add_trip.date' style="display:inline; width:150px" />
                                    <span ng-show='!add_trip.change_trip'>
                                        &nbsp;&nbsp;
                                        <button type="submit" tabindex="10" class="btn btn-success btn-sm">Check Availability</button>
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
    
                <div class="text-center" ng-show='check_availabilities_show_loading'>
                    <strong><em>Loading...</em></strong>
                </div>
                
                <div ng-show='check_availabilities_data.departure && !check_availabilities_data.departure.availabilities'>
                    <div class="alert alert-warning">
                        <strong>Sorry</strong>, Trip not available, please select another date...
                    </div>
                </div>
                
                <div class="product search-trip-result" ng-show='check_availabilities_data.departure.availabilities'>
                    <h4 style="margin-top:0">Departure</h4>
                    <div class="pull-right">
                        <strong>{{fn.formatDate(check_availabilities_data.departure.check.date, "dd M yy")}}</strong>
                    </div>
                    <div>
                        <strong>{{check_availabilities_data.departure.check.departure.port_code}} - {{check_availabilities_data.departure.check.departure.name}}</strong>
                        &nbsp; <i class="fa fa-chevron-right"></i> &nbsp;
                        <strong>{{check_availabilities_data.departure.check.arrival.port_code}} - {{check_availabilities_data.departure.check.arrival.name}}</strong>
                    </div>
                    <hr style="margin:5px 0" />
                    <div class="trip" ng-class="{'selected':availabilities.is_selected}" ng-show='!availabilities.is_hidden' ng-repeat="availabilities in check_availabilities_data.departure.availabilities | orderBy : 'departure.time'" ng-click="check_availabilities_select_this_trip(availabilities, check_availabilities_data.departure)">
                        <div class="media">
                          <div class="media-left">
                            <a href="#">
                              <img class="media-object" ng-src="{{availabilities.schedule.boat.main_image}}" width="80px" />
                            </a>
                          </div>
                          <div class="media-body">
                            <p class="pull-right text-right">
                                <small>Adult:</small> <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_1, availabilities.currency)}}</strong><br />
                                <small>Child:</small> <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_2, availabilities.currency)}}</strong><br />
                                <small>Infant:</small> <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_3, availabilities.currency)}}</strong><br />
                            </p>
                          
                            <i class="fa fa-clock-o"></i> <strong>{{availabilities.departure.time}} - {{availabilities.arrival.time}}</strong> ({{availabilities.schedule.boat.available_space}} space left)<br />
                            <small><i class="fa fa-ship"></i> 	<strong>{{availabilities.schedule.boat.name}} ({{availabilities.schedule.boat.capacity}} seats)</strong></small><br />
                            <small><i class="fa fa-dollar"></i> &nbsp; {{availabilities.name}}</small><br />
                            <small><i class="fa fa-car" aria-hidden="true"></i> 
                                <i class="fa fa-check" aria-hidden="true" ng-show="availabilities.pickup_service == 'yes'"></i> <i class="fa fa-remove" aria-hidden="true" ng-show="availabilities.pickup_service != 'yes'" style="color:red"></i> Pickup 
                                &nbsp;
                                <i class="fa fa-check" aria-hidden="true" ng-show="availabilities.dropoff_service == 'yes'"></i> <i class="fa fa-remove" aria-hidden="true" ng-show="availabilities.dropoff_service != 'yes'" style="color:red"></i> Dropoff
                            </small><br />
                          </div>
                        </div>
                    </div>
                </div>
                
                <div class="product search-trip-result" ng-show='check_availabilities_data.return.availabilities'>
                    <h4 style="margin-top:0">Return</h4>
                    <div class="pull-right">
                        <strong>{{fn.formatDate(check_availabilities_data.return.check.date, "dd M yy")}}</strong>
                    </div>
                    <div>
                        <strong>{{check_availabilities_data.return.check.departure.port_code}} - {{check_availabilities_data.return.check.departure.name}}</strong>
                        &nbsp; <i class="fa fa-chevron-right"></i> &nbsp;
                        <strong>{{check_availabilities_data.return.check.arrival.port_code}} - {{check_availabilities_data.return.check.arrival.name}}</strong>
                    </div>
                    <hr style="margin:5px 0" />
                    <div class="trip" ng-class="{'selected':availabilities.is_selected}" ng-show='!availabilities.is_hidden' ng-repeat="availabilities in check_availabilities_data.return.availabilities | orderBy : 'departure.time'" ng-click="check_availabilities_select_this_trip(availabilities, check_availabilities_data.return)">
                        <div class="media">
                          <div class="media-left">
                            <a href="#">
                              <img class="media-object" ng-src="{{availabilities.schedule.boat.main_image}}" width="80px" />
                            </a>
                          </div>
                          <div class="media-body">
                            <p class="pull-right text-right">
                                <small>Adult:</small> <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_1, availabilities.currency)}}</strong><br />
                                <small>Child:</small> <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_2, availabilities.currency)}}</strong><br />
                                <small>Infant:</small> <strong>{{availabilities.currency}} {{fn.formatNumber(availabilities.aplicable_rates.rates_3, availabilities.currency)}}</strong><br />
                            </p>
                          
                            <i class="fa fa-clock-o"></i> <strong>{{availabilities.departure.time}} - {{availabilities.arrival.time}}</strong> ({{availabilities.schedule.boat.available_space}} space left)<br />
                            <small><i class="fa fa-ship"></i> 	<strong>{{availabilities.schedule.boat.name}} ({{availabilities.schedule.boat.capacity}} seats)</strong></small><br />
                            <small><i class="fa fa-dollar"></i> &nbsp; {{availabilities.name}}</small><br />
                            <small><i class="fa fa-car" aria-hidden="true"></i> 
                                <i class="fa fa-check" aria-hidden="true" ng-show="availabilities.pickup_service == 'yes'"></i> <i class="fa fa-remove" aria-hidden="true" ng-show="availabilities.pickup_service != 'yes'" style="color:red"></i> Pickup 
                                &nbsp;
                                <i class="fa fa-check" aria-hidden="true" ng-show="availabilities.dropoff_service == 'yes'"></i> <i class="fa fa-remove" aria-hidden="true" ng-show="availabilities.dropoff_service != 'yes'" style="color:red"></i> Dropoff
                            </small><br />
                          </div>
                        </div>
                    </div>
                </div>
                
                <div class="product" ng-show='check_availabilities_data.departure.selected_trip'>
                    <strong>Rates (IDR)</strong><br />
                    Adult : <input type="number" min="0" step="0.5" class="text-right" style="width:100px" ng-model='check_availabilities_data.departure.selected_trip.aplicable_rates.rates_1' />
                    Child : <input type="number" min="0" step="0.5" class="text-right" style="width:100px" ng-model='check_availabilities_data.departure.selected_trip.aplicable_rates.rates_2' />
                    Infant : <input type="number" min="0" step="0.5" class="text-right"  style="width:100px" ng-model='check_availabilities_data.departure.selected_trip.aplicable_rates.rates_3' />
                </div>
                
                <div ng-show='update_voucher_trip_error_desc.error_desc.length>0' class="alert alert-danger"><ul><li ng-repeat='err in update_voucher_trip_error_desc.error_desc'>{{err}}</li></ul></div>
                
		  	</div>
            <div class="modal-footer" style="text-align:center">
                <button type="button" class="btn btn-primary btn-submit-edit-trip" ng-show='check_availabilities_show_OK_button' ng-click='update_split_voucher()'>Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
		</div>
	</div>
</div>
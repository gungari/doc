<style type="text/css">
    #table-info{
        width: 70%;
    }
</style>
<div class="modal fade" id="cancel-booking-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <form ng-submit='saveCancelation($event)'>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    Cancel Booking #{{DATA.current_booking.booking.booking_code}}
                </h4>
              </div>
              <div class="modal-body">
                <div ng-show='DATA.cancelation.error_desc.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.cancelation.error_desc'>{{err}}</li></ul></div>
                <table class="table table-borderless table-condensed">
                    <tr>
                        <td></td>
                        <td>
                            <label><h4><input type="checkbox" ng-model='DATA.cancelation.cancel_all_trip' ng-true-value='1' ng-false-value='0' ng-click='chkCancelAllBookingClick()' /> Cancel All Trips</h4></label>
                        </td>
                    </tr>
                    <tr>
                        <td>Trips</td>
                        <td>
                            <hr style="margin:5px 0" />
                            <div ng-repeat='detail in DATA.current_booking.booking.detail' ng-show="detail.booking_detail_status_code != 'CANCEL'">
                                <div ng-class="{'alert alert-info':DATA.cancelation.voucher[detail.voucher_code].is_cancel}" style="margin:0 !important">
                                    <label><h4><input type="checkbox" ng-model='DATA.cancelation.voucher[detail.voucher_code].is_cancel' ng-true-value='1' ng-false-value='0' ng-click='chkCancelAllBookingClick(true)' /> {{detail.voucher_code}}</h4></label>
                                    <div>
                                        <strong>{{detail.departure.port.name}} ({{detail.departure.port.port_code}}) : {{detail.departure.time}}</strong>
                                        &nbsp;&nbsp;<i class="fa fa-chevron-right"></i>&nbsp;&nbsp;
                                        <strong>{{detail.arrival.port.name}} ({{detail.arrival.port.port_code}}) : {{detail.arrival.time}}</strong>
                                    </div>
                                    <div><small>({{detail.rates.name}})</small></div>
                                    <div><strong>{{fn.formatDate(detail.date, "dd MM yy")}}</strong></div>
                                    <div>
                                        <span ng-show='detail.qty_1 > 0'>{{detail.qty_1}} Adult</span>
                                        <span ng-show='detail.qty_2 > 0'>{{detail.qty_2}} Child</span>
                                        <span ng-show='detail.qty_3 > 0'>{{detail.qty_3}} Infant</span>
                                    </div>
                                    <div>
                                    	<strong>{{DATA.current_booking.booking.currency}} {{fn.formatNumber(detail.subtotal, DATA.current_booking.booking.currency)}}</strong>
                                    </div>
                                   
                                    <div class="form-inline">
                                    	<hr style="margin:10px 0" />
                                    	<span style="color: red;">Cancelation Fee :</span> 
                                        <div class="input-group" style="margin-left: 15px;">
                                          <span class="input-group-addon input-sm" id="basic-addon1">{{DATA.current_booking.booking.currency}}</span>

                                          <input type="number" class="form-control input-sm" min="0" placeholder="Fee" ng-model="cancelation_fee[detail.voucher_code]['pay']" style="width:120px"/>

                                          <!-- <input type="number" class="form-control input-sm" min="0" placeholder="Fee" ng-model="DATA.cancelation.voucher[detail.voucher_code].fee" style="width:120px"/> -->
                                        </div>
                                        <small>
                                            <table id="table-info" class="table table-borderless table-striped" style="margin-top: 10px;">
                                                <tr>
                                                    <th>Cancellation prior to arrival</th>
                                                    <th>Cancellation Fee</th>
                                                </tr>
                                                <tr ng-show="cancelation_fee">
                                                    <td>{{cancelation_fee[detail.voucher_code]['discount']}} days</td>
                                                    <td>{{cancelation_fee[detail.voucher_code]['fee']}} %</td>
                                                </tr>
                                                <tr ng-show="!cancelation_fee">
                                                    <td>-</td>
                                                    <td>-</td>
                                                </tr>
                                            </table>   
                                        </small>
                                    </div>
                                </div>
                                <hr style="margin:5px 0" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="130">Cancellation Reason</td>
                        <td>
                            <textarea placeholder="Description" required="required" type="text" class="form-control input-md autoheight" ng-model='DATA.cancelation.cancelation_reason' rows="3"></textarea>
                        </td>
                    </tr>
                </table>
              </div>
              <div class="modal-footer" style="text-align:center">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              </div>
            </div>
        </div>
    </form>
</div>
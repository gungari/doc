
<div ng-init="checkInVoucherTrans()">
<div class="products">
	<div class="product form-inline text-center" style="font-size:18px">
		<form ng-submit='getVoucherParticipant()'>
			Passcode : <input  id="check_voucher_code" type="text" required="required" placeholder="Passcode" class="form-control input-lg" style="width:250px" ng-model='check_voucher_code' />
			<button type="submit" class="btn btn-lg btn-primary">OK</button>
		</form>
	</div>
</div>
<br />
<!-- 
<div ng-show='!show_getVoucherInformation' align="center">
	<img src="<?=base_url("public/images/loading_bar.gif")?>" />
</div> -->

<div ng-show='DATA.voucher.voucher' ng-repeat="voucher in DATA.voucher.voucher.detail">
	
	<div class="sub-title"><strong>VOUCHER INFORMATION</strong></div>
	<table width="100%" class="table">
		<tr>
			<td width="170">Booking Code</td>
			<td><strong>#{{DATA.voucher.voucher.booking_code}}</strong></td>
		</tr>
		<tr>
			<td>Voucher Code</td>
			<td><strong>{{voucher.voucher_code}}</strong></td>
		</tr>
		<tr>
			<td>Date</td>
			<td><strong>{{fn.formatDate(voucher.date, "dd MM yy")}}</strong></td>
		</tr>
		<tr ng-show="voucher.product_type == 'TRANS'">
			<td>Trip</td>
			<td style="font-size:16px">
                <strong>{{voucher.departure.port.name}} ({{voucher.departure.port.port_code}}) - {{voucher.departure.time}}</strong>
                &nbsp;&nbsp;<i class="fa fa-chevron-right"></i>&nbsp;&nbsp;
                <strong>{{voucher.arrival.port.name}} ({{voucher.arrival.port.port_code}}) - {{voucher.arrival.time}}</strong>
            </td>
		</tr>
        <tr ng-show="voucher.product_type == 'ACT'">
			<td>Product</td>
			<td style="font-size:16px">
                <strong>{{voucher.product.name}}</strong>
            </td>
		</tr>
		<tr ng-show="DATA.voucher.voucher.source_code == 'AGENT'">
			<td>Booking Source</td>
			<td><strong>{{DATA.voucher.voucher.agent.name}}</strong></td>
		</tr>
		<tr ng-show="!DATA.voucher.voucher.source_code == 'AGENT'">
			<td>Booking Source</td>
			<td><strong>{{DATA.voucher.voucher.source}}</strong></td>
		</tr>
		<tr>
			<td>Pass Code</td>
			<td><strong>{{voucher.passenger.pass_code}}</strong></td>
		</tr>
	</table>
	
	<div ng-show='voucher.pickup'>
		<br />
		<div class="sub-title"><strong>PICKUP INFORMATION</strong></div>
		<table width="100%" class="table">
			<tr>
				<td width="170">Area</td>
				<td><strong>{{voucher.pickup.area}} - {{voucher.pickup.time}}</strong></td>
			</tr>
			<tr>
				<td>Hotel Name</td>
				<td><strong>{{voucher.pickup.hotel_name}}</strong></td>
			</tr>
			<tr>
				<td>Hotel Address</td>
				<td><strong>{{voucher.pickup.hotel_address}}</strong></td>
			</tr>
			<tr>
				<td>Phone Number</td>
				<td><strong>{{voucher.pickup.hotel_phone_number}}</strong></td>
			</tr>
		</table>
	</div>
	<div ng-show='voucher.dropoff'>
		<br />
		<div class="sub-title"><strong>DROPOFF INFORMATION</strong></div>
		<table width="100%" class="table">
			<tr>
				<td width="170">Area</td>
				<td><strong>{{voucher.dropoff.area}} - {{voucher.dropoff.time}}</strong></td>
			</tr>
			<tr>
				<td>Hotel Name</td>
				<td><strong>{{voucher.dropoff.hotel_name}}</strong></td>
			</tr>
			<tr>
				<td>Hotel Address</td>
				<td><strong>{{voucher.dropoff.hotel_address}}</strong></td>
			</tr>
			<tr>
				<td>Phone Number</td>
				<td><strong>{{voucher.dropoff.hotel_phone_number}}</strong></td>
			</tr>
		</table>
	</div>
	<br>
	<div ng-show="!DATA.payment">
		<div class="sub-title"><strong>PAYMENT INFORMATION</strong></div>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	<div ng-show="DATA.payment">
		<div class="sub-title"><strong>PAYMENT INFORMATION</strong></div>
	<div ng-show="DATA.payment.payment.balance_real_payment_with_invoice != 0">
			
		<table width="100%" class="table">
			<tr class="success" style="font-weight:bold">
					<td width="170">Total Transaction</td>
					<td>{{DATA.payment.payment.currency}} {{fn.formatNumber(DATA.payment.payment.total_transaction, DATA.payment.payment.currency)}}</td>
				
				</tr>
				<tr class="info" style="font-weight:bold; color:green"
                	ng-hide="DATA.current_booking.booking.status_code == 'VOID'">
					<td>Paid</td>
				
					<td>({{DATA.payment.payment.currency}} {{fn.formatNumber(DATA.payment.payment.total_real_payment_with_invoice, DATA.payment.payment.currency)}})</td>
					
				</tr>
				<tr style="font-weight:bold" ng-class="{'danger':(DATA.payment.payment.balance_real_payment_with_invoice>0), 'info':(DATA.payment.payment.balance_real_payment_with_invoice<=0)}"
                	ng-hide="DATA.current_booking.booking.status_code == 'VOID'">
					<td>Outstanding Order</td>
				
					<td>{{DATA.payment.payment.currency}} {{fn.formatNumber(DATA.payment.payment.balance_real_payment_with_invoice, DATA.payment.payment.currency)}}</td>
					
				</tr>
				<tr>
					<td rowspan="2">
						<div class="add-product-button"> 
							<a href="" class="btn btn-success btn-xs" ui-sref="reservation.detail.payment({booking_code:DATA.voucher.voucher.booking_code})" target="_blank"> <span class="glyphicon glyphicon-plus"></span> Add Payment </a> 
						</div>
					</td>
				</tr>
		</table>
	</div>
	</div>
	<br>
	
	<iframe ng-src="{{trustAsUrl('<?=site_url("home/print_page/#/print/passenger/")?>',check_voucher_code)}}" id='print_participant' style="width:0;height:0;border:0; border:none;"></iframe>


</div>

<!-- modal Search reservation-->
<div class="modal fade" id="checkin-popup-info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="myModalLabel">
			Search Reservation
		</h4>
	  </div>
	  <div class="modal-body">
	  	<div>
	  		<div class="products">
	  			<div class="product form-inline" style="text-align: center;">
	  				<form ng-submit='loadDataBookingTransport_2()'>
	  					<input type="text" class="form-control input-lg" placeholder="Search" ng-model='search.q' /> 
	  					<button type="submit" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-search"></span></button>
	  					<?php /*?>Filter : <input type="text" ng-model="DATA.filter_booking" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
	  					<button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-search"></span></button><?php */?>
	  				</form>
	  			</div>
	  		</div>

	  		<div ng-show='show_loading_DATA_bookings'>
	  			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	  		</div>
	  		<div ng-show='DATA.bookings.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.bookings.error_msg'>{{err}}</li></ul></div>
	  		<div ng-show='!show_loading_DATA_bookings && DATA.bookings.bookings'>
	  			<div class="table-responsive">
	  				<table class="table table-condensed table-bordered table-hover">
	  					<tr class="header bold">
	  						<td align="center" width="110">Voucher Code#</td>
	  						<td align="center">Customer</td>
	  						<td align="center" width="50">Adult</td>
	  						<td align="center" width="50">Child</td>
	  						<td align="center" width="50">infant</td>
	  						<?php /*?><td align="center" colspan="2" width="200">Trip</td><?php */?>
	  						<td align="center" width="200">Booking Source</td>
	  						<td width="50"></td>
	  					</tr>
	  					<tbody ng-repeat="booking in DATA.bookings.bookings | filter:DATA.filter_booking"  >
	  						<tr ng-class="{'danger':(booking.cancel_status=='CAN'), 'warning':(booking.status_code=='UNDEFINITE')}" >
	  							<td style="text-align: center;">
	  								<a href="" ng-click="openPopUpWindowVoucher('<?=base_url('home/print_page')?>#/print/voucher/', booking.booking_code, booking.detail.voucher_code)">
	  									<strong>{{booking.detail.voucher_code}}</strong>
	  								</a><br/>
	  								{{fn.formatDate(booking.detail.date, "dd  M  yy")}}
	  							</td>
	  							<td>{{booking.customer.first_name + " " +booking.customer.last_name}}</td>
	  							<td align="center">{{booking.detail.qty_1}}</td>
	  							<td align="center">{{booking.detail.qty_2}}</td>
	  							<td align="center">{{booking.detail.qty_3}}</td>
	  							<?php /*?><td width="70" align="center">{{booking.detail.departure.port.port_code}} <br />({{booking.detail.departure.time}})</td>
								<td width="70" align="center">{{booking.detail.arrival.port.port_code}} <br />({{booking.detail.arrival.time}})</td><?php */?>
	  							<td class="text-capitalize">
                                	<div ng-show='!booking.agent' class="text-capitalize">{{booking.source.toLowerCase()}}</div>
									<div ng-show='booking.agent'>
                                    	{{booking.agent.name}}
                                        <div ng-show="booking.voucher_reff_number != ''">(Voucher# : {{booking.voucher_reff_number}})</div>
                                    </div>
	  							</td>
	  							<td>
	  								<button type="button" class="btn btn-primary btn-sm" data-ng-click="getClickVoucherInformation(booking.detail.voucher_code)">Checkin</button>
	  							</td>	  							
	  						</tr>
	  					</tbody>
	  				</table>
	  			</div>
	  			<nav aria-label="Page navigation" class="pull-right">
	  			  <ul class="pagination pagination-sm">
	  				<li ng-class="{'disabled':DATA.bookings.search.page <= 1}">
	  				  <a href="" ng-click='loadDataBookingTransport_2(DATA.bookings.search.page-1)' aria-label="Prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
	  				</li>
	  				<li	ng-show="DATA.bookings.search.page-3 > 1">
	  					<a href="" ng-click='loadDataBookingTransport_2(1)'>1</a>
	  				</li>
	  				<li class="disabled" ng-show="DATA.bookings.search.page-4 > 1">
	  					<a>...</a>
	  				</li>
	  				<li ng-repeat='pagination in DATA.bookings.search.pagination' ng-class="{'active':DATA.bookings.search.page == pagination}"
	  					ng-show="($index +1) <= DATA.bookings.search.page+3 && ($index+1) >= DATA.bookings.search.page-3">
	  					<a href="" ng-click='loadDataBookingTransport_2(($index+1))'>{{($index+1)}}</a>
	  				</li>
	  				<li class="disabled" ng-show="DATA.bookings.search.page+4 < DATA.bookings.search.number_of_pages">
	  					<a>...</a>
	  				</li>
	  				<li	ng-show="DATA.bookings.search.page+3 < DATA.bookings.search.number_of_pages">
	  					<a href="" ng-click='loadDataBookingTransport_2(DATA.bookings.search.number_of_pages)'>{{DATA.bookings.search.number_of_pages}}</a>
	  				</li>
	  				<li ng-class="{'disabled':DATA.bookings.search.page >= DATA.bookings.search.number_of_pages}">
	  				  <a href="" ng-click='loadDataBookingTransport_2(DATA.bookings.search.page+1)' aria-label="Next"><span class="glyphicon glyphicon-chevron-right"></span></a>
	  				</li>
	  			  </ul>
	  			</nav>
	  			<div class="clearfix"></div>
	  		</div>
	  	</div>
	  </div>
	  <div class="modal-footer" style="text-align:center">
		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	  </div>
	</div>
  </div>
</div>
<!--/ modal Search reservation-->

<!-- modal Open ticket checkin-->

<?php $this->load->view("crs/checkin/v_modal_presoldticket_checkin")?>
<!--/ modal Open ticket checkin-->

</div>

<script>$("#check_voucher_code").focus();</script>

<script>activate_sub_menu_agent_detail("participant");</script>
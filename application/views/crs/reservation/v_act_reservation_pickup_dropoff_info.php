
<h1>Pickup / Dropoff</h1>

<div ng-init='pickupDropoffTrans();'>
	<div class="products">
		<div class="product">
		<form ng-submit="pickupDropofTransSearch()">
			<table class="table table-borderless table-condensed">
				<tr>
					<td width="50"></td>
					<td>
						<label><input type="radio" ng-model='dept.type' value="pickup" /> Pickup</label>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<label><input type="radio" ng-model='dept.type' value="dropoff" /> Dropoff</label>
					</td>
				</tr>
				<tr>
					<td>Date</td>
					<td class="form-inline">
						<input type="text" ng-change='arrivalDepartureTime()' class="form-control input-md datepicker" style="width:200px;" placeholder="Select Date" ng-model='dept.date' required />
						<button type="submit" class="btn btn-md btn-primary">Search</button>
					</td>
				</tr>
				
			</table>
		</form>
		</div>
	</div>
	
	<div ng-show='show_arrivalDepartureTransSearch_loading'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	
	<div ng-show='!show_arrivalDepartureTransSearch_loading && pickup_dropoff'>
		<table class="table table-bordered table-condensed" id="data">
			<tr>
				<td colspan="14">
					<a href="<?=site_url("home/print_page/#/print/pickup_dropoff/")?>{{search.type}}/{{search.date}}" target="_blank" class="pull-right">
						<button type="button" class="btn btn-default"><span class="glyphicon glyphicon-print"></span> Print</button>
					</a>
					
					<h4 class="text-capitalize">{{search.type}} - {{fn.formatDate(search.date,"d MM yy")}}</h4>
				</td>

			</tr>
			<tr class="header bold">
				<td width="30">No</td>
				<td style="text-align: center;" width="100">Booking Number</td>
				<td style="text-align: center;" width="100">Voucher#</td>
				<td width="120">Guest Name</td>
				<td style="text-align: center;" colspan="3">Pax(S)</td>
				
				<td width="160">Pick Up Address</td>
				<td width="80">Area</td>
				<td width="120">Dept Date</td>
				<td width="60">Dept Time</td>
				<?php if($vendor['category'] == 'transport'){ ?>
					<td style="text-align: center;" colspan="2">Trip</td>
				<?php }else{ ?>
					<td style="text-align: center;">Product</td>
				<?php } ?>
			</tr>
			<tr style="text-align: center;" class="header bold">
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<td rowspan="1">Adl</td>
				<td rowspan="1">Chi</td>
				<td rowspan="1">Inf</td>
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<?php if($vendor['category'] == 'transport'){ ?>
					<td rowspan="1">Dept</td>
					<td rowspan="1">Arrv</td>
				<?php }else{ ?>
					<td></td>
				<?php } ?>

			</tr>
			<tbody  ng-repeat="pickup in  pickup_dropoff">
			<tr style="background:#FAFAFA">
				<?php if($vendor['category'] == 'transport'){ ?>
				<td colspan="11">
					<strong>{{pickup.departure_port.name}} ({{pickup.departure_time}}) </strong>
					-
					<strong>{{pickup.arrival_port.name}} ({{pickup.arrival_time}}) </strong>
				</td>
				<?php } ?>
					
			</tr>
			<tr ng-repeat="data in pickup.passenger_list">
				<td style="text-align: center;">{{($index+1)}}</td>
				<td style="text-align: center;">{{data.booking_code}}</td>
				<td style="text-align: center;">
					<a href="" ng-click="openPopUpWindowVoucher('<?=site_url("home/print_page/#/print/voucher_trans/")?>', data.booking_code, data.voucher_code)">
						{{data.voucher_code}}
					</a>
				</td>
				<td>{{data.customer.first_name}} {{data.customer.last_name}}</td>
				<td  style="text-align: center;">
					<span ng-show='data.qty_opt_1>0'>{{data.qty_opt_1}}</span>
					<span ng-show='data.qty_opt_1==0'>-</span>
				</td>
				<td  style="text-align: center;">
					<span ng-show='data.qty_opt_2>0'>{{data.qty_opt_2}}</span>
					<span ng-show='data.qty_opt_2==0'>-</span>
				</td>
				<td  style="text-align: center;">
					<span ng-show='data.qty_opt_3>0'>{{data.qty_opt_3}}</span>
					<span ng-show='data.qty_opt_3==0'>-</span>
				</td>
				<td>{{data.hotel_name}} <br> {{data.hotel_address}}</td>
				<td>{{data.area}}</td>
				<td  style="text-align: center;">{{fn.formatDate(data.date,"d MM yy")}}</td>
				<td>{{data.time}}</td>
				<?php if($vendor['category'] == 'transport'){ ?>
					<td>{{data.departure.port.name}}</td>
					<td>{{data.arrival.port.name}}</td>
				<?php }else{ ?>
					<td>{{pickup.product_name}}</td>
				<?php } ?>

					
			</tr>

			</tbody>
		</table>
		<!-- <table class="table table-bordered table-condensed">
			<tr style="text-align: center;" class="table-header header bold" style="font-weight:bold">
				<td width="30">No</td>
				
				<td width="100">Voucher#</td>
				<td width="120">GUEST NAME</td>
				<td colspan="3">PAX(S)</td>
				
				
				<td width="80">Hotel</td>
				<td width="120">Pickup TIme</td>
				
			</tr>
			<tr style="text-align: center;" class="table-header header bold">
				<td rowspan="1"></td>
				
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<td rowspan="1">ADL</td>
				<td rowspan="1">CHI</td>
				<td rowspan="1">INF</td>
				<td rowspan="1"></td>
				<td rowspan="1"></td>
			</tr>
			<tbody ng-repeat="pickup in pickup_dropoff">
			<tr style="background:#FAFAFA">
				<td colspan="13">
					<strong>{{passenger_list.product.product_code}} - {{passenger_list.product.name}}</strong>
				</td>
			</tr>
				<tr ng-repeat="data in pickup.passenger_list">	
					<td>{{($index+1)}}</td>
					
					<td>
						<a href="" ng-click="openPopUpWindowVoucher('<?=site_url("home/print_page/#/print/voucher_trans/")?>', pickup.booking_code, pickup.voucher_code)">
							{{pickup.voucher_code}}
						</a>
					</td>
					</td>
					<td>{{data.customer.first_name}} {{data.customer.last_name}}</td>
					<td>
						<span ng-show='data.qty_opt_1>0'>{{data.qty_opt_1}}</span>
						<span ng-show='data.qty_opt_1==0'>-</span>
					</td>
					<td>
						<span ng-show='data.qty_opt_2>0'>{{data.qty_opt_2}}</span>
						<span ng-show='data.qty_opt_2==0'>-</span>
					</td>
					<td>
						<span ng-show='data.qty_opt_3>0'>{{data.qty_opt_3}}</span>
						<span ng-show='data.qty_opt_3==0'>-</span>
					</td>
					<td>{{pickup.hotel_name}}</td>
					<td>{{pickup.time}}</td>
				</tr>
			</tbody>
		</table> -->
	</div>
	<hr />
		<a href="<?=site_url("export_to_excel/trans_pickup_dropoff")?>?s[type]={{search.type}}&s[date]={{search.date}}" target="_blank"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export To Excel</a>
		&nbsp;&nbsp;
		<a href="<?=site_url("home/print_page/#/print/pickup_dropoff/")?>{{search.type}}/{{search.date}}" target="_blank">
		<i class="fa fa-print" aria-hidden="true"></i> Print </a>
	<style>
		table tr.not_checkin{color:#666; background:#FAFAFA}
	</style>
	<script>GeneralJS.activateSubMenu(".nav-pills", "li", ".pickup-dropoff");</script>
	<?php /*?>{{dept}}
	<br />
	{{DATA_R}}<?php */?>
</div>
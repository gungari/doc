
<div ng-init='pickupDropofTransPrint();'>
	
	<div ng-show='pickup_dropoff'>
	
		<!-- <h4 class="text-capitalize">{{search.type}} - {{fn.formatDate(search.date,"d MM yy")}}</h4> -->
		<!-- <table class="table table-bordered table-condensed">
			<tr style="text-align: center;" class="table-header header bold" style="font-weight:bold">
				<td width="30">No</td>
				<td width="100">BOOKING NUMBER</td>
				<td width="100">Voucher#</td>
				<td width="120">GUEST NAME</td>
				<td colspan="3">PAX(S)</td>
				
				<td width="160">PICK UP ADDRESS</td>
				<td width="80">AREA</td>
				<td width="120">DEPT DATE</td>
				<td width="60">DEPT TIME</td>
				<td colspan="2">TRIP</td>
			</tr>
			<tr style="text-align: center;" class="table-header header bold">
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<td rowspan="1">ADL</td>
				<td rowspan="1">CHI</td>
				<td rowspan="1">INF</td>
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<td rowspan="1">DEPT</td>
				<td rowspan="1">ARRV</td>
			</tr>
			<tbody ng-repeat="pickup in pickup_dropoff">
			<tr style="background:#FAFAFA">
				<td colspan="13">
					<strong>{{passenger_list.product.product_code}} - {{passenger_list.product.name}}</strong>
				</td>
			</tr>
				<tr ng-repeat="data in pickup.passenger_list">	
					<td>{{($index+1)}}</td>
					<td>{{data.booking_code}}</td>
					<td>{{data.voucher_code}}
					</td>
					<td>{{data.customer.first_name}} {{data.customer.last_name}}</td>
					<td>
						<span ng-show='data.qty_opt_1>0'>{{data.qty_opt_1}}</span>
						<span ng-show='data.qty_opt_1==0'>-</span>
					</td>
					<td>
						<span ng-show='data.qty_opt_2>0'>{{data.qty_opt_2}}</span>
						<span ng-show='data.qty_opt_2==0'>-</span>
					</td>
					<td>
						<span ng-show='data.qty_opt_3>0'>{{data.qty_opt_3}}</span>
						<span ng-show='data.qty_opt_3==0'>-</span>
					</td>
					<td>{{data.hotel_name}} <br> {{data.hotel_address}}</td>
					<td>{{data.area}}</td>
					<td>{{fn.formatDate(data.date,"d MM yy")}}</td>
					<td>{{data.time}}</td>
					<td>{{data.departure.port.name}}</td>
					<td>{{data.arrival.port.name}}</td>
				</tr>
			</tbody>
		</table> -->
		<table class="table table-bordered table-condensed">
			<tr style="text-align: center;" class="table-header header bold" style="font-weight:bold">
				<td width="30">No</td>
				
				<td width="100">Voucher#</td>
				<td width="120">GUEST NAME</td>
				<td colspan="3">PAX(S)</td>
				
				
				<td width="80">Hotel</td>
				<td width="120">Pickup TIme</td>
				
			</tr>
			<tr style="text-align: center;" class="table-header header bold">
				<td rowspan="1"></td>
				
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<td rowspan="1">ADL</td>
				<td rowspan="1">CHI</td>
				<td rowspan="1">INF</td>
				<td rowspan="1"></td>

			</tr>
			<tbody ng-repeat="pickup in pickup_dropoff">
			<tr style="background:#FAFAFA">
				<td colspan="13">
					<strong>{{passenger_list.product.product_code}} - {{passenger_list.product.name}}</strong>
				</td>
			</tr>
				<tr ng-repeat="data in pickup.passenger_list">	
					<td>{{($index+1)}}</td>
					
					<td>{{data.voucher_code}}
					</td>
					<td>{{data.customer.first_name}} {{data.customer.last_name}}</td>
					<td>
						<span ng-show='data.qty_opt_1>0'>{{data.qty_opt_1}}</span>
						<span ng-show='data.qty_opt_1==0'>-</span>
					</td>
					<td>
						<span ng-show='data.qty_opt_2>0'>{{data.qty_opt_2}}</span>
						<span ng-show='data.qty_opt_2==0'>-</span>
					</td>
					<td>
						<span ng-show='data.qty_opt_3>0'>{{data.qty_opt_3}}</span>
						<span ng-show='data.qty_opt_3==0'>-</span>
					</td>
					<td>{{pickup.hotel_name}}</td>
					<td>{{pickup.time}}</td>
				</tr>
			</tbody>
		</table>
	</div>
	
	<style>
		table tr.not_checkin{color:#666; background:#FAFAFA}
	</style>
	<script>GeneralJS.activateSubMenu(".nav-pills", "li", ".pickup-dropoff");</script>

</div>
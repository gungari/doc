<table class="table table-borderlesss table-condenseds">
    <tr>
        <td width="150">
        	<div ng-show='DATA.data_rsv.payments.detail && DATA.data_rsv.payments.detail.length > 0'>
            	<div ng-show='DATA.data_rsv.payments.balance > 0'>
                	<?php /*?><div ng-show='deposit && deposit.current_deposit > 0' style="margin-bottom:10px">
                        <button style="width:100%" type="button" class="btn btn-danger" ng-click='newRsvAddPaymentsDeposit()'>Pay with Deposit <br />({{deposit.currency}} {{fn.formatNumber(deposit.current_deposit, deposit.currency)}})</button>
                    </div><?php */?>
	        		<button style="width:100%" type="button" class="btn btn-info" data-toggle="modal" data-target="#mdl-add-payment-rsv" ng-click="newRsvAddNewPayments('<?=$vendor["default_currency"]?>')"><i class="fa fa-plus"></i> Add Payment</button>
        		</div>
            </div>
        </td>
        <td>
        	<div ng-show="DATA.data_rsv.agent && DATA.data_rsv.agent.ACL">
        		<p style="color:green"><em>You can add multiple payment methods per order.</em></p>
            </div>
        	<div ng-show='DATA.data_rsv.payments.detail && DATA.data_rsv.payments.detail.length > 0'>
                <table class="table table-bordereds">
                	<tbody>
                    <tr ng-repeat='payment_detail in DATA.data_rsv.payments.detail'>
                    	<td>
                        	<div><strong>{{payment_detail.payment_type_name}}</strong></div>
                            <div ng-show='payment_detail.description'><em>Remarks : {{payment_detail.description}}</em></div>
                        </td>
                        <td width="150" align="right">
                        	<?=$vendor["default_currency"]?> {{fn.formatNumber(payment_detail.payment_amount, '<?=$vendor["default_currency"]?>')}}
                            <div ng-show='payment_detail.payment_currency && payment_detail.default_currency != payment_detail.payment_currency'>
                            	<em>Paid in : {{payment_detail.payment_currency}} {{fn.formatNumber(payment_detail.payment_amount_convertion, payment_detail.payment_currency)}}</em>
                            </div>
                        </td>
                        <td width="30" align="right">
                        	<a href="" style="color:red" ng-click='newRsvRemovePaymentsFromArray($index)'><span class="glyphicon glyphicon-trash"></span></a>
                        </td>
                    </tr>
                    </tbody>
                    <tbody style="font-weight:bold">
                    <tr>
                    	<td align="right">TOTAL PAYMENT</td>
                        <td align="right"><?=$vendor["default_currency"]?> {{fn.formatNumber(DATA.data_rsv.payments.total_payment, '<?=$vendor["default_currency"]?>')}}</td>
                        <td></td>
                    </tr>
                    <tr>
                    	<td align="right">BALANCE</td>
                        <td align="right"><?=$vendor["default_currency"]?> {{fn.formatNumber(DATA.data_rsv.payments.balance, '<?=$vendor["default_currency"]?>')}}</td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
                <br />
            </div>

            <div ng-hide='DATA.data_rsv.payments.detail && DATA.data_rsv.payments.detail.length > 0'>
            	<div ng-show='DATA.data_rsv.payments.balance > 0'>
                    <span ng-show="DATA.data_rsv.agent && DATA.data_rsv.agent.ACL">
                        <button type="button" class="btn btn-warning" ng-click='newRsvAddPaymentsACL()' ng-disabled="DATA.data_rsv.agent.ACL.current_limit < DATA.data_rsv.TOTAL.grand_total">
                        	<i class="fa fa-plus"></i> Pay with Agent Credit Limit ({{DATA.data_rsv.agent.ACL.currency}} {{fn.formatNumber(DATA.data_rsv.agent.ACL.current_limit, DATA.data_rsv.agent.ACL.currency)}})
                        </button>
                        &nbsp;
                    </span>
                    <?php /*?><span ng-show='deposit && deposit.current_deposit > 0'>
                        <button type="button" class="btn btn-danger" ng-click='newRsvAddPaymentsDeposit()'><i class="fa fa-plus"></i> Pay with Deposit ({{deposit.currency}} {{fn.formatNumber(deposit.current_deposit, deposit.currency)}})</button>
                    </span><?php */?>
                    <div style="margin-top:10px">
	                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#mdl-add-payment-rsv" ng-click="newRsvAddNewPayments('<?=$vendor["default_currency"]?>')"><i class="fa fa-plus"></i> Add Payment</button>
        			</div>
                </div>
            </div>
        </td>
    </tr>
</table>
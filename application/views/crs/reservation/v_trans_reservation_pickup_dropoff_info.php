
<h1>Pickup / Dropoff</h1>

<div ng-init='pickupDropoffTrans();'>
	<div class="products">
		<div class="product">
		<form ng-submit="pickupDropofTransSearch()">
			<table class="table table-borderless table-condensed">
				<tr>
					<td width="50"></td>
					<td>
						<label><input type="radio" ng-model='dept.type' value="pickup" /> Pickup</label>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<label><input type="radio" ng-model='dept.type' value="dropoff" /> Dropoff</label>
					</td>
				</tr>
				<tr>
					<td>Date</td>
					<td class="form-inline">
						<input type="text" ng-change='arrivalDepartureTime()' class="form-control input-md datepicker" style="width:200px;" placeholder="Select Date" ng-model='dept.date' required />
						<button type="submit" class="btn btn-md btn-primary">Search</button>
					</td>
				</tr>
				
			</table>
		</form>
		</div>
	</div>
	
	<div ng-show='show_arrivalDepartureTransSearch_loading'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	
	<div ng-show='!show_arrivalDepartureTransSearch_loading && pickup_dropoff' style="margin-bottom: 20px;">
		
	
		<table class="table table-bordered table-condensed" id="data">
			<tr>
				<td colspan="14">
					<a href="<?=site_url("home/print_page/#/print/pickup_dropoff/")?>{{search.type}}/{{search.date}}" target="_blank" class="pull-right">
						<button type="button" class="btn btn-default"><span class="glyphicon glyphicon-print"></span> Print</button>
					</a>
					
					<h4 class="text-capitalize">{{search.type}} - {{fn.formatDate(search.date,"d MM yy")}}</h4>
				</td>

			</tr>
			<tr class="header bold">
				<td width="30">No</td>
				<td style="text-align: center;" width="100">Booking Number</td>
				<td style="text-align: center;" width="100">Voucher#</td>
				<td width="120">Guest Name</td>
				<td style="text-align: center;" colspan="3">Pax(S)</td>
				
				<td width="160">Pick Up Address</td>
				<td width="80">Area</td>
				<td width="120">Dept Date</td>
				<td width="60">Dept Time</td>
				<td style="text-align: center;" colspan="2">Trip</td>
			</tr>
			<tr style="text-align: center;" class="header bold">
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<td rowspan="1">Adl</td>
				<td rowspan="1">Chi</td>
				<td rowspan="1">Inf</td>
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<td rowspan="1"></td>
				<td rowspan="1">Dept</td>
				<td rowspan="1">Arrv</td>
			</tr>
			<tbody  ng-repeat="pickup in  pickup_dropoff">
			<tr style="background:#FAFAFA">
				<td colspan="13">
					<strong>{{pickup.departure_port.name}} ({{pickup.departure_time}}) </strong>
					-
					<strong>{{pickup.arrival_port.name}} ({{pickup.arrival_time}}) </strong>
				</td>
			</tr>
			<tr ng-repeat="data in pickup.passenger_list">
				<td style="text-align: center;">{{($index+1)}}</td>
				<td style="text-align: center;">{{data.booking_code}}</td>
				<td style="text-align: center;">
					<a href="" ng-click="openPopUpWindowVoucher('<?=site_url("home/print_page/#/print/voucher_trans/")?>', data.booking_code, data.voucher_code)">
						{{data.voucher_code}}
					</a>
				</td>
				<td>{{data.customer.first_name}} {{data.customer.last_name}}</td>
				<td  style="text-align: center;">
					<span ng-show='data.qty_opt_1>0'>{{data.qty_opt_1}}</span>
					<span ng-show='data.qty_opt_1==0'>-</span>
				</td>
				<td  style="text-align: center;">
					<span ng-show='data.qty_opt_2>0'>{{data.qty_opt_2}}</span>
					<span ng-show='data.qty_opt_2==0'>-</span>
				</td>
				<td  style="text-align: center;">
					<span ng-show='data.qty_opt_3>0'>{{data.qty_opt_3}}</span>
					<span ng-show='data.qty_opt_3==0'>-</span>
				</td>
				<td>{{data.hotel_name}} <br> {{data.hotel_address}}</td>
				<td>{{data.area}}</td>
				<td  style="text-align: center;">{{fn.formatDate(data.date,"d MM yy")}}</td>
				<td>{{data.time}}</td>
				<td>{{data.departure.port.name}}</td>
				<td>{{data.arrival.port.name}}</td>
			</tr>

			</tbody>
		</table>
	</div>
	
		<a href="<?=site_url("export_to_excel/trans_pickup_dropoff")?>?s[type]={{search.type}}&s[date]={{search.date}}&s[schedule_code]={{search.schedule_code}}&s[boat_id]={{search.boat.id}}&s[departure_port_id]={{(search.departure_port=='ALL')?'ALL':search.departure_port.id}}&s[arrival_port_id]={{(search.destination_port=='ALL')?'ALL':search.destination_port.id}}&s[time]={{search.time}}" target="_blank"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export To Excel</a>
		&nbsp;&nbsp;
		<a href="<?=site_url("home/print_page/#/print/pickup_dropoff/")?>{{search.type}}/{{search.date}}" target="_blank">
		<i class="fa fa-print" aria-hidden="true"></i> Print </a>
	<style>
		table tr.not_checkin{color:#666; background:#FAFAFA}
	</style>
	<script>GeneralJS.activateSubMenu(".nav-pills", "li", ".pickup-dropoff");</script>
	<?php /*?>{{dept}}
	<br />
	{{DATA_R}}<?php */?>
</div>
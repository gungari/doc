<style type="text/css">
    #table-info{
        width: 70%;
    }
</style>
<div class="modal fade" id="cancel-booking-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <form ng-submit='saveCancelation($event)'>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    Cancel Booking #{{DATA.current_booking.booking.booking_code}}
                </h4>
              </div>
              <div class="modal-body">
                <div ng-show='DATA.cancelation.error_desc.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.cancelation.error_desc'>{{err}}</li></ul></div>


                <table class="table table-borderless table-condensed">
                	<?php //CANCEL FULL VOUCHER------------------?>
                    <tbody ng-show='!DATA.cancelation.is_cancel_partial' style="border:none">
                        <tr>
                            <td></td>
                            <td>
                                <label><h4><input type="checkbox" ng-model='DATA.cancelation.cancel_all_trip' ng-true-value='1' ng-false-value='0' ng-click='chkCancelAllBookingClick()' /> Cancel All Trips</h4></label>
                            </td>
                        </tr>
                        <tr>
                            <td>Trips</td>
                            <td>
                                <hr style="margin:5px 0" />
                                <div ng-repeat='detail in DATA.current_booking.booking.detail' ng-show="detail.booking_detail_status_code != 'CANCEL'">
                                    <div ng-class="{'alert alert-info':DATA.cancelation.voucher[detail.voucher_code].is_cancel}" style="margin:0 !important">
                                        <label><h4><input type="checkbox" ng-model='DATA.cancelation.voucher[detail.voucher_code].is_cancel' ng-true-value='1' ng-false-value='0' ng-click='chkCancelAllBookingClick(true)' /> {{detail.voucher_code}}</h4></label>
                                        
                                        <div ng-show="detail.product_type == 'ACT'">
                                            <strong>{{detail.product.name}}</strong>
                                        </div>
                                        <div ng-show="detail.product_type == 'TRANS'">
                                            <strong>{{detail.departure.port.name}} ({{detail.departure.port.port_code}}) : {{detail.departure.time}}</strong>
                                            &nbsp;&nbsp;<i class="fa fa-chevron-right"></i>&nbsp;&nbsp;
                                            <strong>{{detail.arrival.port.name}} ({{detail.arrival.port.port_code}}) : {{detail.arrival.time}}</strong>
                                        </div>
                                        <div><small>({{detail.rates.name}})</small></div>
                                        <div><strong>{{fn.formatDate(detail.date, "dd MM yy")}}</strong></div>
                                        
                                        <div ng-show='detail.is_packages'>
                                            <div>{{detail.packages.qty}} x {{detail.packages.name}} 
                                            (
                                            <span ng-show='detail.packages.include_pax.opt_1'>{{detail.packages.include_pax.opt_1}} Adult</span>
                                            <span ng-show='detail.packages.include_pax.opt_2'>{{detail.packages.include_pax.opt_2}} Child</span>
                                            <span ng-show='detail.packages.include_pax.opt_3'>{{detail.packages.include_pax.opt_3}} Infant</span>
                                            )
                                            @{{DATA.current_booking.booking.currency}} {{fn.formatNumber(detail.packages.rates,DATA.current_booking.booking.currency)}}</div>
                                            <div ng-show='detail.packages.additional.opt_1 || detail.packages.additional.opt_2 || detail.packages.additional.opt_3'>
                                                <strong>Additional:</strong><br />
                                                <span ng-show='detail.packages.additional.opt_1'>Adult : {{detail.packages.additional.opt_1}} @{{DATA.current_booking.booking.currency}} {{fn.formatNumber(detail.packages.extra_rates.opt_1,DATA.current_booking.booking.currency)}}</span><br />
                                                <span ng-show='detail.packages.additional.opt_2'>Child : {{detail.packages.additional.opt_2}} @{{DATA.current_booking.booking.currency}} {{fn.formatNumber(detail.packages.extra_rates.opt_2,DATA.current_booking.booking.currency)}}</span><br />
                                                <span ng-show='detail.packages.additional.opt_3'>Infant : {{detail.packages.additional.opt_3}} @{{DATA.current_booking.booking.currency}} {{fn.formatNumber(detail.packages.extra_rates.opt_3,DATA.current_booking.booking.currency)}}</span>
                                            </div>
                                        </div>
                                        
                                        <div ng-hide='detail.is_packages'>
                                            <?php /*?><span ng-show='detail.qty_1 > 0'>{{detail.qty_1}} Adult</span>
                                            <span ng-show='detail.qty_2 > 0'>{{detail.qty_2}} Child</span>
                                            <span ng-show='detail.qty_3 > 0'>{{detail.qty_3}} Infant</span><?php */?>
                                            
                                            <span ng-show='detail.qty_1 > 0'>{{detail.qty_1}} Adult @{{DATA.current_booking.booking.currency}} {{fn.formatNumber(detail.rates.rates_1,DATA.current_booking.booking.currency)}}</span>
                                            <span ng-show='detail.qty_2 > 0'> | {{detail.qty_2}} Child @{{DATA.current_booking.booking.currency}} {{fn.formatNumber(detail.rates.rates_2,DATA.current_booking.booking.currency)}}</span>
                                            <span ng-show='detail.qty_3 > 0'> | {{detail.qty_3}} Infant @{{DATA.current_booking.booking.currency}} {{fn.formatNumber(detail.rates.rates_3,DATA.current_booking.booking.currency)}}</span>
                                        </div>

                                        <div class="pull-right" xxng-hide='detail.is_packages'>
                                        	<a href="" ng-click='partialCancelSelectVoucher(detail)'>Partial Cancel</a>
                                        </div>
                                        
                                        <div>
                                            <strong>{{DATA.current_booking.booking.currency}} {{fn.formatNumber(detail.subtotal, DATA.current_booking.booking.currency)}}</strong>
                                        </div>
                                        
                                        <div class="form-inline" ng-show='DATA.current_booking.booking.total_payment == 0'>
                                            <hr style="margin:10px 0" />
                                            Cancelation Fee : 
                                            <div class="input-group" style="margin-left: 15px;" ng-init="cancelationFee(DATA.current_booking.booking.booking_code)">
                                              <span class="input-group-addon input-sm" id="basic-addon1">{{DATA.current_booking.booking.currency}}</span>
    
                                              <input type="number" class="form-control input-sm" min="0" placeholder="Fee" ng-model="cancelation_fee[detail.voucher_code]['pay']" style="width:120px" required/>
    
                                             <!-- <input type="number" class="form-control input-sm" min="0" placeholder="Fee" ng-model="DATA.cancelation.voucher[detail.voucher_code].fee" style="width:120px"/>  -->
                                              
                                            </div>
                                            <small ng-show="cancelation_fee[detail.voucher_code]['discount']">
                                                <table id="table-info" class="table table-borderless table-striped" style="margin-top: 10px;">
                                                    <tr>
                                                        <th>Cancellation prior to arrival</th>
                                                        <th>Cancellation Fee</th>
                                                    </tr>
                                                    <tr>
                                                        <td>{{cancelation_fee[detail.voucher_code]['discount']}} days</td>
                                                        <td>{{cancelation_fee[detail.voucher_code]['fee']}} %</td>
                                                    </tr>
                                                </table>   
                                            </small>
                                        </div>
                                    </div>
                                    <hr style="margin:5px 0" />
                                </div>
                            </td>
                        </tr>
                        <tr ng-show='DATA.current_booking.booking.discount.amount > 0'>
                            <td>Total</td>
                            <td><strong>{{DATA.current_booking.booking.currency}} {{fn.formatNumber(DATA.current_booking.booking.total_before_discount,DATA.current_booking.booking.currency)}}</strong></td>
                        </tr>
                        <tr ng-show='DATA.current_booking.booking.discount.amount > 0'>
                        	<td>
                            	Discount
                            	<span ng-show='DATA.current_booking.booking.discount.amount > 0'>
                                    <span ng-show="DATA.current_booking.booking.discount.type == '%'">({{DATA.current_booking.booking.discount.value}}%)</span>
                                    <span ng-show="DATA.current_booking.booking.discount.type != '%'">({{fn.formatNumber(DATA.current_booking.booking.discount.value,DATA.current_booking.booking.currency)}})</span>
                                </span>
                            </td>
                            <td>
                            	<strong>
                                    {{DATA.current_booking.booking.currency}}
                                    {{fn.formatNumber(DATA.current_booking.booking.discount.amount,DATA.current_booking.booking.currency)}}
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td>Grand Total</td>
                            <td><strong>{{DATA.current_booking.booking.currency}} {{fn.formatNumber(DATA.current_booking.booking.grand_total,DATA.current_booking.booking.currency)}}</strong></td>
                        </tr>
                        <tr style="color: #00d600;font-weight: bold;" ng-show='DATA.current_booking.booking.total_payment > 0'>
                            <td>Paid</td>
                            <td><strong>{{DATA.current_booking.booking.currency}} {{fn.formatNumber(DATA.current_booking.booking.total_payment,DATA.current_booking.booking.currency)}}</strong></td>
                        </tr>
                    </tbody>
                    
                    <?php //CANCEL PARTIAL ------------------?>
                    <tbody ng-show='DATA.cancelation.is_cancel_partial' style="border:none">
                    	<tr>
                        	<td colspan="2">
                            	<div class="alert alert-info" style="margin:0 !important">
                                    <h4>{{DATA.cancelation.partial_cancel.voucher.voucher_code}}</h4>
                                    
                                    <div ng-show="DATA.cancelation.partial_cancel.voucher.product_type == 'ACT'">
                                        <strong>{{DATA.cancelation.partial_cancel.voucher.product.name}}</strong>
                                    </div>
                                    <div ng-show="DATA.cancelation.partial_cancel.voucher.product_type == 'TRANS'">
                                        <strong>{{DATA.cancelation.partial_cancel.voucher.departure.port.name}} ({{DATA.cancelation.partial_cancel.voucher.departure.port.port_code}}) : {{DATA.cancelation.partial_cancel.voucher.departure.time}}</strong>
                                        &nbsp;&nbsp;<i class="fa fa-chevron-right"></i>&nbsp;&nbsp;
                                        <strong>{{DATA.cancelation.partial_cancel.voucher.arrival.port.name}} ({{DATA.cancelation.partial_cancel.voucher.arrival.port.port_code}}) : {{DATA.cancelation.partial_cancel.voucher.arrival.time}}</strong>
                                    </div>
                                    <div><small>({{DATA.cancelation.partial_cancel.voucher.rates.name}})</small></div>
                                    <div><strong>{{fn.formatDate(DATA.cancelation.partial_cancel.voucher.date, "dd MM yy")}}</strong></div>
                                    
                                    <div ng-show='DATA.cancelation.partial_cancel.voucher.is_packages'>
                                        <div>{{DATA.cancelation.partial_cancel.voucher.packages.qty}} x {{DATA.cancelation.partial_cancel.voucher.packages.name}} 
                                        (
                                        <span ng-show='DATA.cancelation.partial_cancel.voucher.packages.include_pax.opt_1'>{{DATA.cancelation.partial_cancel.voucher.packages.include_pax.opt_1}} Adult</span>
                                        <span ng-show='DATA.cancelation.partial_cancel.voucher.packages.include_pax.opt_2'>{{DATA.cancelation.partial_cancel.voucher.packages.include_pax.opt_2}} Child</span>
                                        <span ng-show='DATA.cancelation.partial_cancel.voucher.packages.include_pax.opt_3'>{{DATA.cancelation.partial_cancel.voucher.packages.include_pax.opt_3}} Infant</span>
                                        )
                                        @{{DATA.current_booking.booking.currency}} {{fn.formatNumber(DATA.cancelation.partial_cancel.voucher.packages.rates,DATA.current_booking.booking.currency)}}</div>
                                        <div ng-show='DATA.cancelation.partial_cancel.voucher.packages.additional.opt_1 || DATA.cancelation.partial_cancel.voucher.packages.additional.opt_2 || DATA.cancelation.partial_cancel.voucher.packages.additional.opt_3'>
                                            <strong>Additional:</strong><br />
                                            <span ng-show='DATA.cancelation.partial_cancel.voucher.packages.additional.opt_1'>Adult : {{DATA.cancelation.partial_cancel.voucher.packages.additional.opt_1}} @{{DATA.current_booking.booking.currency}} {{fn.formatNumber(DATA.cancelation.partial_cancel.voucher.packages.extra_rates.opt_1,DATA.current_booking.booking.currency)}}</span><br />
                                            <span ng-show='DATA.cancelation.partial_cancel.voucher.packages.additional.opt_2'>Child : {{DATA.cancelation.partial_cancel.voucher.packages.additional.opt_2}} @{{DATA.current_booking.booking.currency}} {{fn.formatNumber(DATA.cancelation.partial_cancel.voucher.packages.extra_rates.opt_2,DATA.current_booking.booking.currency)}}</span><br />
                                            <span ng-show='DATA.cancelation.partial_cancel.voucher.packages.additional.opt_3'>Infant : {{DATA.cancelation.partial_cancel.voucher.packages.additional.opt_3}} @{{DATA.current_booking.booking.currency}} {{fn.formatNumber(DATA.cancelation.partial_cancel.voucher.packages.extra_rates.opt_3,DATA.current_booking.booking.currency)}}</span>
                                        </div>
                                    </div>
                                    
                                    <div ng-hide='DATA.cancelation.partial_cancel.voucher.is_packages'>
                                        <span ng-show='DATA.cancelation.partial_cancel.voucher.qty_1 > 0'>{{DATA.cancelation.partial_cancel.voucher.qty_1}} Adult @{{DATA.current_booking.booking.currency}} {{fn.formatNumber(DATA.cancelation.partial_cancel.voucher.rates.rates_1,DATA.current_booking.booking.currency)}}</span>
                                        <span ng-show='DATA.cancelation.partial_cancel.voucher.qty_2 > 0'> | {{DATA.cancelation.partial_cancel.voucher.qty_2}} Child @{{DATA.current_booking.booking.currency}} {{fn.formatNumber(DATA.cancelation.partial_cancel.voucher.rates.rates_2,DATA.current_booking.booking.currency)}}</span>
                                        <span ng-show='DATA.cancelation.partial_cancel.voucher.qty_3 > 0'> | {{DATA.cancelation.partial_cancel.voucher.qty_3}} Infant @{{DATA.current_booking.booking.currency}} {{fn.formatNumber(DATA.cancelation.partial_cancel.voucher.rates.rates_3,DATA.current_booking.booking.currency)}}</span>
                                    </div>
                                    
                                    <?php /*?><div class="pull-right">
                                    	<a href="" ng-click='DATA.cancelation.is_cancel_partial=0;DATA.cancelation.partial_cancel=false'>Back</a>
                                    </div><?php */?>
                                    
                                    <div>
                                        <strong>{{DATA.current_booking.booking.currency}} {{fn.formatNumber(DATA.cancelation.partial_cancel.voucher.subtotal, DATA.current_booking.booking.currency)}}</strong>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                        	<td>Select Participant</td>
                            <td>
                            	<ol>
                                    <li ng-repeat="passenger in DATA.cancelation.partial_cancel.voucher.passenger">
                                        <label style="cursor:pointer; font-weight:{{(passenger.selected?'bold':'normal')}}" ng-click='calculateQtySelectedPassangerPartialCancel(DATA.cancelation.partial_cancel.voucher)'>
                                            <input type="checkbox" ng-model="passenger.selected" />
                                            <span>{{passenger.first_name}} {{passenger.last_name}} ({{passenger.type.desc}})</span>
                                        </label>
                                    </li>
                                </ol>
                                
                                <br />
                                <strong><em>{{DATA.cancelation.partial_cancel.qty}} Selected Participant will Canceled</em></strong>
                            </td>
                        </tr>
                        <tr ng-show='DATA.current_booking.booking.total_payment == 0'>
                        	<td>Cancelation Fee</td>
                            <td>
                            	<div class="input-group">
                                	<span class="input-group-addon input-sm" id="basic-addon1">{{DATA.current_booking.booking.currency}}</span>
                                	<input type="number" class="form-control input-sm" min="0" placeholder="Fee" 
                                    	ng-model="DATA.cancelation.partial_cancel.voucher.cancelation_fee" 
                                        style="width:130px" required
                                        ng-disabled="!DATA.cancelation.is_cancel_partial || DATA.current_booking.booking.total_payment != 0" />
                                        <div style="padding:7px 0 0 10px;"> &nbsp;&nbsp; / Participant</div>
                                </div>
                                
                                <div style="margin-top:10px">
	                                Total Cancelation Fee : <strong>{{DATA.current_booking.booking.currency}} {{fn.formatNumber((DATA.cancelation.partial_cancel.voucher.cancelation_fee * DATA.cancelation.partial_cancel.qty),DATA.current_booking.booking.currency)}}</strong>
                            	</div>
                            </td>
                        </tr>
                    </tbody>
                    
                    
                    <?php //REFUND ------------------?>
                    <tbody ng-show='DATA.current_booking.booking.total_payment!=0 && (DATA.current_booking.booking.total_payment - DATA.current_booking.booking.total_refund) > 0 && DATA.current_booking.booking.status_code != "COMPLIMEN"' style="border:none">
                    	<tr>
                            <td colspan="2"><hr style="margin:5px 0" /></td>
                        </tr>
                    	<tr>
                        	<td></td>
                            <td>
                            	<label>
	                            	<input type="checkbox" ng-model="DATA.cancelation.is_add_refund" ng-true-value='1' /> 
                                    <i class="fa fa-reply" aria-hidden="true"></i> Add Refund
                                </label>
                            </td>
                        </tr>
                    </tbody>
                    <tbody ng-show='DATA.current_booking.booking.total_payment!=0 && (DATA.current_booking.booking.total_payment - DATA.current_booking.booking.total_refund) > 0 && DATA.cancelation.is_add_refund && DATA.current_booking.booking.status_code != "COMPLIMEN"' style="border:none">
                        <tr>
                            <td colspan="2"><h4>Refund</h4></td>
                        </tr>
                        <?php /*?><tr>
                            <td>Grand Total</td>
                            <td><strong>{{DATA.current_booking.booking.currency}} {{fn.formatNumber(DATA.current_booking.booking.grand_total,DATA.current_booking.booking.currency)}}</strong></td>
                        </tr>
                        <tr style="color: #00d600;font-weight: bold;">
                            <td>Paid</td>
                            <td><strong>{{DATA.current_booking.booking.currency}} {{fn.formatNumber(DATA.current_booking.booking.total_payment,DATA.current_booking.booking.currency)}}</strong></td>
                        </tr><?php */?>
                        <tr ng-show='DATA.current_booking.booking.total_refund'>
                            <td>Refunded</td>
                            <td><strong>{{DATA.current_booking.booking.currency}} {{fn.formatNumber(DATA.current_booking.booking.total_refund,DATA.current_booking.booking.currency)}}</strong></td>
                        </tr>
                        <tr>
                            <td>Refund Amount*</td>
                            <td>
                                <div class="input-group">
                                    <span class="input-group-addon">{{DATA.current_booking.booking.currency}}</span>
                                    <input type="number" required="required" class="form-control input-md" style="width:200px" placeholder="Total Refund" ng-model='DATA.cancelation.refund_amount' ng-disabled="!(DATA.current_booking.booking.total_payment!=0 && DATA.cancelation.is_add_refund)" />
                                </div>
                                <small>Max. Refund : {{DATA.current_booking.booking.currency}} {{fn.formatNumber((DATA.current_booking.booking.total_payment - DATA.current_booking.booking.total_refund),DATA.current_booking.booking.currency)}}</small>
                            </td>
                        </tr>
                    </tbody>
                    
                    <?php //--REFUND ------------------?>
                    
                    <tbody style="border:none">
                    	<tr>
                            <td colspan="2"><hr style="margin:5px 0" /></td>
                        </tr>
                        <tr>
                            <td width="130">Cancellation Reason</td>
                            <td>
                                <textarea placeholder="Description" required="required" type="text" class="form-control input-md autoheight" ng-model='DATA.cancelation.cancelation_reason' rows="3"></textarea>
                            </td>
                        </tr>
                	</tbody>
                </table>
              </div>
              <div class="modal-footer" style="text-align:center">
                <button type="submit" class="btn btn-primary" ng-disabled="DATA.cancelation.is_cancel_partial && DATA.cancelation.partial_cancel.qty==0">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              </div>
            </div>
        </div>
    </form>
</div>
<form ng-submit='submit_booking($event)'>

	<div class="sub-title" ng-init='newReservationTransport()'>
		<span ng-show='!DATA.data_rsv.reservation_code'><i class="fa fa-plus-square" aria-hidden="true"></i> Add New</span> 
		<span ng-show='DATA.data_rsv.reservation_code'><i class="fa fa-pencil" aria-hidden="true"></i> Edit</span>
		Reservation
	</div>
	
	<br />
	
	<div ng-show='DATA.data_rsv.error_desc.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.data_rsv.error_desc'>{{err}}</li></ul></div>
	
	<table class="table table-borderless">
		<tr>
			<td width="150">Booking Source*</td>
			<td class="form-inline">
				<select class="form-control input-md" style="width:200px" ng-model='DATA.data_rsv.booking_source' ng-change='aplicable_rates_for_agent();check_booking_source_sub();'>
					<option value="{{booking_source.code}}" ng-repeat='booking_source in $root.DATA_booking_source' ng-show="booking_source.code!='ONLINE'">{{booking_source.name}}</option>
				</select>
                &nbsp;&nbsp;
                <select ng-disabled="!DATA.booking_source_sub" ng-show='DATA.booking_source_sub' class="form-control input-md" style="width:200px" ng-model='DATA.data_rsv.booking_source_sub'>
					<option value="{{booking_source.code}}" ng-repeat='booking_source in DATA.booking_source_sub'>{{booking_source.name}}</option>
				</select>
			</td>
		</tr>
		<tr ng-show='DATA.data_rsv.booking_source == "AGENT"'>
			<td>Agent Name*</td>
			<td>
				<select class="form-control input-md" ng-model='DATA.data_rsv.agent' ng-change='aplicable_rates_for_agent();' 
					ng-options="(agents.name +' - '+agents.agent_code) for agents in DATA.agents.agents">
					<option value="">-- Select Agent --</option>
					<?php /*?><option value="{{agent.agent_code}}" ng-repeat='agent in DATA.agents.agents'>{{agent.name}} - {{agent.agent_code}}</option><?php */?>
				</select>
			</td>
		</tr>
        <tr ng-show='DATA.data_rsv.booking_source == "AGENT"'>
			<td>Voucher# Reff.</td>
			<td>
            	<input type="text" class="form-control input-md" style="width:200px" ng-model='DATA.data_rsv.voucher_reff_number' placeholder="Voucher# Reff." />
			</td>
		</tr>
		<tr>
			<td>Booking Status*</td>
			<td>
				<select class="form-control input-md" style="width:200px; display:inline" ng-model='DATA.data_rsv.booking_status'>
					<option value="{{booking_status.code}}" ng-repeat='booking_status in $root.DATA_booking_status' ng-show="booking_status.code!='CANCEL' && booking_status.code!='VOID'">
                    	{{booking_status.name}}
                    </option>
				</select>
                <?php /*?><span ng-show="DATA.data_rsv.booking_status == 'UNDEFINITE'">&nbsp;&nbsp;&nbsp;<em>Valid for 24 hours only.</em></span><?php */?>
			</td>
		</tr>
        <tr ng-show="DATA.data_rsv.booking_status == 'UNDEFINITE'">
        	<td>Valid For*</td>
            <td>
            	<div class="input-group" style="width:200px">
	            	<input placeholder="Valid For" type="number" ng-disabled="DATA.data_rsv.booking_status!='UNDEFINITE'" ng-required="DATA.data_rsv.booking_status=='UNDEFINITE'" step="0.1" class="form-control input-md" ng-model='DATA.data_rsv.undefinite_valid_until_hour' />
                    <span class="input-group-addon">Hour</span>
                </div>
			</td>
        </tr>
	</table>
	
	<br />
	
	<div class="sub-title">Trips</div>
	<div class="products">
		<div class="product">
			<div ng-show="DATA.data_rsv.selected_trips && DATA.data_rsv.selected_trips.length > 0">
				<table class="table table-bordered table-condensed">
					<tr class="success" style="font-weight:bold">
						<td width="30">#</td>
						<td width="90">Date</td>
						<td colspan="2">Departure</td>
						<td colspan="2">Arrival</td>
						<td width="50">Adult</td>
						<td width="50">Child</td>
						<td width="50">Infant</td>
						<td align="right" width="100">Sub Total (<?=$vendor["default_currency"]?>)</td>
						<td width="25"></td>
						<td width="25"></td>
					</tr>
					<tbody ng-repeat="trips in DATA.data_rsv.selected_trips">
					<tr>
						<td>{{$index+1}}</td>
						<td align="center">
							<strong>{{fn.formatDate(trips.date, "dd M yy")}}</strong>
						</td>
						<td width="120">{{trips.trips.departure.port.name}}</td>
						<td align="center" width="60">{{trips.trips.departure.time}}</td>
						<td width="120">{{trips.trips.arrival.port.name}}</td>
						<td align="center" width="60">{{trips.trips.arrival.time}}</td>
						<td align="center">{{trips.adult}}</td>
						<td align="center">{{trips.child}}</td>
						<td align="center">{{trips.infant}}</td>
						<td align="right">{{fn.formatNumber(trips.sub_total, trips.trips.currency)}}</td>
						<td><a href="" style="color:blue" ng-click="trips.show_custom_price = (!trips.show_custom_price)"><span class="glyphicon glyphicon-pencil"></span></a></td>
						<td><a href="" style="color:red" ng-click='remove_trip($index)'><span class="glyphicon glyphicon-trash"></span></a></td>
					</tr>
					<tr ng-show='trips.show_custom_price' class="warning">
						<td>&nbsp;</td>
						<td colspan="8" class="form-inline">
							Adult : <input type="number" min="0" step="0.5" class="text-right" style="width:100px" ng-model='trips.trips.aplicable_rates.rates_1' ng-change="calculate_subtotal(trips)" />
							Child : <input type="number" min="0" step="0.5" class="text-right" style="width:100px" ng-model='trips.trips.aplicable_rates.rates_2' ng-change="calculate_subtotal(trips)" />
							Infant : <input type="number" min="0" step="0.5" class="text-right"  style="width:100px" ng-model='trips.trips.aplicable_rates.rates_3' ng-change="calculate_subtotal(trips)" />
							<button type="button" ng-click="trips.show_custom_price = (!trips.show_custom_price)">OK</button>
						</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr ng-show="trips.trips.pickup_area">
						<td>&nbsp;</td>
						<td colspan="8">
							<table class="table table-condensed table-borderless" style="background:none">
								<tr>
									<td width="120px"><strong>Pickup Service</strong></td>
									<td ng-init="area={'text':'Own Transport','nopickup':true, 'currency':trips.trips.currency}; trips.trips.pickup_area.unshift(area); trips.pickup.area = area">
										<select class="form-control input-sm" ng-model='trips.pickup.area' ng-change='calculate_total()' 
											ng-options="((area.text)?area.text:(area.area+' - '+area.time+' - '+area.currency+' '+fn.formatNumber(area.price, area.currency))) for area in trips.trips.pickup_area">
										</select>
									</td>
								</tr>
								<tr ng-show='!trips.pickup.area.nopickup'>
									<td>Hotel/Villa Name</td>
									<td><input ng-required="!trips.pickup.area.nopickup" ng-disabled='trips.pickup.area.nopickup' type="text" class="form-control input-sm" ng-model='trips.pickup.hotel_name' placeholder="Hotel/Villa Name" /></td>
								</tr>
								<tr ng-show='!trips.pickup.area.nopickup'>
									<td>Address</td>
									<td><input ng-required="!trips.pickup.area.nopickup" ng-disabled='trips.pickup.area.nopickup' type="text" class="form-control input-sm" ng-model='trips.pickup.hotel_address' placeholder="Hotel/Villa Address" /></td>
								</tr>
								<tr ng-show='!trips.pickup.area.nopickup'>
									<td>Phone Number</td>
									<td><input ng-required="!trips.pickup.area.nopickup" ng-disabled='trips.pickup.area.nopickup' type="text" class="form-control input-sm" ng-model='trips.pickup.hotel_phone' placeholder="Hotel/Villa Phone Number" /></td>
								</tr>
							</table>
						</td>
						<td align="right">
							<span ng-show='!trips.pickup.area.nopickup'>
								<span class="pickup_price" ng-show="trips.pickup.area.type == 'way'">
									{{fn.formatNumber(trips.pickup.area.price, trips.trips.currency)}}
								</span>
								<span class="pickup_price" ng-show="trips.pickup.area.type == 'pax'">
									{{fn.formatNumber((trips.pickup.area.price * (trips.adult + trips.child)), trips.trips.currency)}}
								</span>
								<span class="pickup_price hidden-field">
									<input type="number" min="0" onblur="$(this).closest('tr').find('.pickup_price').toggle()" step="0.5" class="text-right"  style="width:100px" ng-model='trips.pickup.area.price' ng-change="calculate_subtotal(trips)" />
									/ {{trips.pickup.area.type}}
								</span>
							</span>
						</td>
						<td align="center">
							<span ng-show='!trips.pickup.area.nopickup'>
								<a href="" style="color:blue" onclick="$(this).closest('tr').find('.pickup_price').toggle()"><span class="glyphicon glyphicon-pencil"></span></a>
							</span>
						</td>
						<td></td>
					</tr>
					<tr ng-show="trips.trips.dropoff_area">
						<td>&nbsp;</td>
						<td colspan="8">
							<table class="table table-condensed table-borderless" style="background:none">
								<tr>
									<td width="120px"><strong>Dropoff Service</strong></td>
									<td ng-init="area={'text':'Own Transport','nopickup':true, 'currency':trips.trips.currency}; trips.trips.dropoff_area.unshift(area); trips.dropoff.area = area">
										<select class="form-control input-sm" ng-model='trips.dropoff.area' ng-change='calculate_total()' 
											ng-options="((area.text)?area.text:(area.area+' - '+area.time+' - '+area.currency+' '+fn.formatNumber(area.price, area.currency))) for area in trips.trips.dropoff_area">
										</select>
									</td>
								</tr>
								<tr ng-show='!trips.dropoff.area.nopickup'>
									<td>Hotel/Villa Name</td>
									<td><input ng-required="!trips.dropoff.area.nopickup" ng-disabled='trips.dropoff.area.nopickup' type="text" class="form-control input-sm" ng-model='trips.dropoff.hotel_name' placeholder="Hotel/Villa Name" /></td>
								</tr>
								<tr ng-show='!trips.dropoff.area.nopickup'>
									<td>Address</td>
									<td><input ng-required="!trips.dropoff.area.nopickup" ng-disabled='trips.dropoff.area.nopickup' type="text" class="form-control input-sm" ng-model='trips.dropoff.hotel_address' placeholder="Hotel/Villa Address" /></td>
								</tr>
								<tr ng-show='!trips.dropoff.area.nopickup'>
									<td>Phone Number</td>
									<td><input ng-required="!trips.dropoff.area.nopickup" ng-disabled='trips.dropoff.area.nopickup' type="text" class="form-control input-sm" ng-model='trips.dropoff.hotel_phone' placeholder="Hotel/Villa Phone Number" /></td>
								</tr>
							</table>
						</td>
						<td align="right">
							<span ng-show='!trips.dropoff.area.nopickup'>
								<span class="pickup_price" ng-show="trips.dropoff.area.type == 'way'">
									{{fn.formatNumber(trips.dropoff.area.price, trips.trips.currency)}}
								</span>
								<span class="pickup_price" ng-show="trips.dropoff.area.type == 'pax'">
									{{fn.formatNumber((trips.dropoff.area.price * (trips.adult + trips.child)), trips.trips.currency)}}
								</span>
								<span class="pickup_price hidden-field">
									<input type="number" min="0" onblur="$(this).closest('tr').find('.pickup_price').toggle()" step="0.5" class="text-right"  style="width:100px" ng-model='trips.dropoff.area.price' ng-change="calculate_subtotal(trips)" />
									/ {{trips.pickup.area.type}}
								</span>
							</span>
							<?php /*?><span ng-show='trips.dropoff.area.price > 0'>
								{{fn.formatNumber(trips.dropoff.area.price, trips.trips.currency)}}
							</span><?php */?>
						</td>
						<td>
							<span ng-show='!trips.dropoff.area.nopickup'>
								<a href="" style="color:blue" onclick="$(this).closest('tr').find('.pickup_price').toggle()"><span class="glyphicon glyphicon-pencil"></span></a>
							</span>
						</td>
						<td></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan="11">
							<strong>Additional Service</strong>
							<table class="table table-condensed" style="background:none;">
								<tr ng-repeat='add_srv in trips.additional_service'>
									<td><input required="required" type="text" class="form-control input-sm" ng-model='add_srv.name' placeholder="Additional Service Name" title="Additional Service Name" /></td>
									<td width="60"><input type="number" ng-change='calculate_total()' class="form-control input-sm text-right" ng-model='add_srv.qty' min="1" step="1" title="Qty." /></td>
									<td width="180">
                                    	<div class="input-group">
                                        	<span class="input-group-addon" id="basic-addon1"><?=$vendor["default_currency"]?></span>
                                        	<input type="number" ng-change='calculate_total()' class="form-control input-sm text-right" ng-model='add_srv.price' min="0" step="0.5" title="Price" />
                                        </div>
                                    </td>
									<td width="100" align="right">
										{{fn.formatNumber((add_srv.qty*add_srv.price), trips.trips.currency)}}
									</td>
									<td></td>
									<td width="20" align="right"><a href="" style="color:red" ng-click='removeAdditionalService(trips, $index)'><span class="glyphicon glyphicon-trash"></span></a></td>
								</tr>
							</table>
							<a href="" ng-click='addAdditionalService(trips)'><span class="glyphicon glyphicon-plus"></span> Add</a>
						</td>
					</tr>
					</tbody>
					<tr>
						<td colspan="12"></td>
					</tr>
					<tr>
						<td colspan="9" align="right">Total (<?=$vendor["default_currency"]?>)</td>
						<td align="right">{{fn.formatNumber(DATA.data_rsv.TOTAL.total, "<?=$vendor["default_currency"]?>")}}</td>
						<td></td>
						<td></td>
					</tr>
                    <tr>
						<td colspan="9"class="form-inline">
							<strong>Discount :</strong>
							<div class="pull-right">
								Type:
								<select style="width:auto" class="form-control input-sm" ng-model='DATA.data_rsv.discount_type' ng-change='calculate_total()'>
									<option value="%">%</option>
									<option value="FIX"><?=$vendor["default_currency"]?></option>
								</select>
								<span>
									&nbsp;&nbsp;&nbsp;
									Amount:
									<div class="input-group">
										<span class="input-group-addon" ng-show="DATA.data_rsv.discount_type!='%'"><?=$vendor["default_currency"]?></span>
										<input class="form-control input-sm" type="number" min="0" max="{{(DATA.data_rsv.discount_type=='%')?100:DATA.data_rsv.TOTAL.total}}" step="0.1" style="width:{{(DATA.data_rsv.discount_type=='%')?80:150}}px; text-align:right" ng-model='DATA.data_rsv.discount_amount' ng-change='calculate_total()' />
										<span class="input-group-addon" ng-show="DATA.data_rsv.discount_type=='%'">%</span>
									</div>
								</span>
							</div>
						</td>
						<td align="right">
							{{fn.formatNumber(DATA.data_rsv.TOTAL.discount, "<?=$vendor["default_currency"]?>")}}
						</td>
						<td></td>
						<td></td>
					</tr>
                    <tr>
						<td colspan="9"class="form-inline">
							<strong>Tax & Service :</strong>
							<div class="pull-right">
								Type:
								<select style="width:auto" class="form-control input-sm" ng-model='DATA.data_rsv.tax_service_type' ng-change='calculate_total()'>
									<option value="%">%</option>
									<option value="FIX"><?=$vendor["default_currency"]?></option>
								</select>
								<span>
									&nbsp;&nbsp;&nbsp;
									Amount:
									<div class="input-group">
										<span class="input-group-addon" ng-show="DATA.data_rsv.tax_service_type!='%'"><?=$vendor["default_currency"]?></span>
										<input class="form-control input-sm" type="number" min="0" max="{{(DATA.data_rsv.tax_service_type=='%')?100:DATA.data_rsv.TOTAL.total}}" step="0.1" style="width:{{(DATA.data_rsv.tax_service_type=='%')?80:150}}px; text-align:right" ng-model='DATA.data_rsv.tax_service_amount' ng-change='calculate_total()' />
										<span class="input-group-addon" ng-show="DATA.data_rsv.tax_service_type=='%'">%</span>
									</div>
								</span>
							</div>
						</td>
						<td align="right">
							{{fn.formatNumber(DATA.data_rsv.TOTAL.tax_service, "<?=$vendor["default_currency"]?>")}}
						</td>
						<td></td>
						<td></td>
					</tr>
					<tr class="info" style="font-weight:bold">
						<td colspan="9" align="right">GRAND TOTAL (<?=$vendor["default_currency"]?>)</td>
						<td align="right">{{fn.formatNumber(DATA.data_rsv.TOTAL.grand_total, "<?=$vendor["default_currency"]?>")}}</td>
						<td></td>
						<td></td>
					</tr>
				</table>
				<hr />
			</div>
			<div class="text-center">
				<a href="" data-toggle="modal" data-target="#modal-add-trip" ng-click="add_new_trip()">
					<button type="button" class="btn btn-info"><i class="fa fa-plus"></i> Add Trip</button>
				</a>
			</div>
		</div>
	</div>
	
	<br />
	
	<div class="sub-title">Guest Information</div>
	<table class="table">
		<tr>
			<td width="150">Name*</td>
			<td>
			
				<input type="text" placeholder="First Name" class="form-control input-md" ng-model='DATA.data_rsv.customer.first_name' style="display:inline; width:48.5%" />
				&nbsp;&nbsp;&nbsp;
				<input type="text" placeholder="Last Name" class="form-control input-md" ng-model='DATA.data_rsv.customer.last_name' style="display:inline; width:48.5%" />
			</td>
		</tr>
		<tr>
			<td>Email</td>
			<td><input type="text" placeholder="Email" class="form-control input-md" ng-model='DATA.data_rsv.customer.email' /></td>
		</tr>
		<tr>
			<td>Phone / Mobile*</td>
			<td><input type="text" placeholder="Phone / Mobile" class="form-control input-md" ng-model='DATA.data_rsv.customer.phone' /></td>
		</tr>
		<tr>
			<td>Country*</td>
			<td>
				<select class="form-control input-md" ng-model='DATA.data_rsv.customer.country_code'>
					<option value="">-- Select Country --</option>
					<option value="{{country.code}}" ng-repeat='country in $root.DATA_country_list'>{{country.name}}</option>
				</select>
			</td>
		</tr>
	</table>
	
	<br />
	
	<div class="sub-title">Remarks / Special Request</div>
	<table class="table">
		<tr>
			<td width="150"></td>
			<td><textarea placeholder="Description" class="form-control input-md autoheight" ng-model='DATA.data_rsv.remarks' rows="3"></textarea></td>
		</tr>
	</table>
	
	<div ng-show="DATA.data_rsv.booking_status != 'COMPLIMEN'">
		<br />
		<div class="sub-title">
			Payment
		</div>
		
		<table class="table table-borderless table-condenseds" <?php /*?>ng-show='!agent_allow_to_use_acl'<?php */?>>
			<tr>
				<td width="150"></td>
				<td><label><input type="checkbox" ng-model='DATA.data_rsv.add_payment' ng-true-value='1' ng-false-value='0' /> &nbsp;&nbsp;ADD PAYMENT</label></td>
			</tr>
		</table>
        <?php /*?><table class="table table-borderless table-condenseds" ng-show='agent_allow_to_use_acl'>
			<tr>
				<td width="150"></td>
				<td>
                    <label><input type="radio" ng-model="agent_payment_use_acl" ng-value='0' ng-click="DATA.data_rsv.add_payment = '1'" /> &nbsp;&nbsp;ADD PAYMENT</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <label><input type="radio" ng-model="agent_payment_use_acl" ng-value='1' ng-click="DATA.data_rsv.add_payment = '0'" /> 
                    	Use Agent Credit Limit
                        (Current Limit : {{DATA.data_rsv.agent.out_standing_invoice.currency}} {{fn.formatNumber(DATA.data_rsv.agent.out_standing_invoice.current_limit,DATA.data_rsv.agent.out_standing_invoice.currency)}})
                    </label>
                </td>
			</tr>
		</table><?php */?>
        
		<table class="table table-borderless table-condenseds" ng-show="DATA.data_rsv.add_payment=='1'">
			<tr>
				<td width="150">Payment Type*</td>
				<td>
                    <select ng-required="DATA.data_rsv.add_payment=='1'" class="form-control input-md" ng-model='DATA.data_rsv.payment.payment_type' ng-change='changePaymentTypeInNewBookingForm()'>
                        <option value="" disabled="disabled">-- Select Payment Type --</option>
                        <option value="{{payment_method.code}}" ng-repeat='payment_method in $root.DATA_payment_method' ng-show="payment_method.code != 'ONLINE'">
                            {{payment_method.name}}
                        </option>
                        <option value="ACL" ng-show='agent_allow_to_use_acl' ng-disabled="DATA.data_rsv.TOTAL.grand_total > DATA.data_rsv.agent.out_standing_invoice.current_limit">
                        	Agent Credit Limit
                            (Maksimum : {{DATA.data_rsv.agent.out_standing_invoice.currency}} {{fn.formatNumber(DATA.data_rsv.agent.out_standing_invoice.current_limit,DATA.data_rsv.agent.out_standing_invoice.currency)}})
                            {{(DATA.data_rsv.TOTAL.grand_total > DATA.data_rsv.agent.out_standing_invoice.current_limit)?<?="'<-- Cannot use ACL (Over Limit)'"?>:""}}
                        </option>
                    </select>
					<?php /*?><select ng-required="DATA.data_rsv.add_payment=='1'" class="form-control input-md" ng-model='DATA.data_rsv.payment.payment_type' ng-change='changePaymentTypeInNewBookingForm()'>
						<option value="" disabled="disabled">-- Select Payment Type --</option>
						<option value="ONLINE">Online Payment</option>
						<option value="CC">Credit Card/Debit Card</option>
						<option value="ATM">ATM Transfer</option>
						<option value="CASH">Cash</option>
						<option value="OPENVOUCHER">Open Voucher</option>
					</select><?php */?>
				</td>
			</tr>
			<tr ng-show="DATA.data_rsv.payment.payment_type=='OPENVOUCHER'" class="header">
				<td>Open Voucher Code*</td>
				<td>
					<table width="100%">
						<tr ng-repeat='ov_code in DATA.data_rsv.payment.openvoucher_code' class="form-inline">
							<td width="30">{{($index+1)}}.</td>
							<td>
								<input ng-required="DATA.data_rsv.payment.payment_type=='OPENVOUCHER'" placeholder="Open Voucher Code {{($index+1)}}" type="text" class="form-control input-sm" ng-model='ov_code.code' style="width:200px" ng-blur="checkValidateOpenVoucherCode(ov_code)" />
								<span ng-show="ov_code.valid" style="color:green" class="glyphicon glyphicon-ok"></span>
								<span ng-show="ov_code.invalid" style="color:red" class="glyphicon glyphicon-remove"></span>
								<span ng-show="ov_code.loading"><small><em>Loading...</em></small></span>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<?php /*?><tr ng-show="DATA.data_rsv.payment.payment_type=='CC' || DATA.data_rsv.payment.payment_type=='ATM'" class="header"><?php */?>
            <tr ng-show="DATA.payment_is_cc || DATA.payment_is_atm" class="header">
				<td>
                	<span ng-show="DATA.payment_is_cc">Card Number</span>
                    <span ng-show="DATA.payment_is_atm">Account Number</span>
                </td>
				<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.data_rsv.payment.account_number' /></td>
			</tr>
			<?php /*?><tr ng-show="DATA.data_rsv.payment.payment_type=='CC' || DATA.data_rsv.payment.payment_type=='ATM'" class="header"><?php */?>
			<tr ng-show="DATA.payment_is_cc || DATA.payment_is_atm" class="header">
            	<td>Name On Card</td>
				<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.data_rsv.payment.name_on_card' /></td>
			</tr>
			<?php /*?><tr ng-show="DATA.data_rsv.payment.payment_type=='CC' || DATA.data_rsv.payment.payment_type=='ATM'" class="header"><?php */?>
            <tr ng-show="DATA.payment_is_cc || DATA.payment_is_atm" class="header">
				<td>Bank Name</td>
				<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.data_rsv.payment.bank_name' /></td>
			</tr>
			<?php /*?><tr ng-show="DATA.data_rsv.payment.payment_type=='CC' || DATA.data_rsv.payment.payment_type=='ATM'" class="header"><?php */?>
            <tr ng-show="DATA.payment_is_cc || DATA.payment_is_atm" class="header">
				<td>Approval Number</td>
				<td><input placeholder="" type="text" class="form-control input-md" ng-model='DATA.data_rsv.payment.payment_reff_number' /></td>
			</tr>
			<tr ng-show="DATA.data_rsv.payment.payment_type!='ACL'">
				<td>Description</td>
				<td><input placeholder="Description" type="text" class="form-control input-md" ng-model='DATA.myPayment.description' /></td>
			</tr>
			<tr>
				<td>Payment Amount*</td>
				<td>
					<div class="input-group">
						<span class="input-group-addon"><?=$vendor["default_currency"]?></span>
						<input placeholder="Payment Amount" min='0' ng-required="DATA.data_rsv.add_payment=='1'" type="number" min="0" class="form-control input-md" ng-model='DATA.data_rsv.payment.payment_amount' style="width:160px" 
							ng-disabled="DATA.data_rsv.payment.payment_type == 'ACL'" ng-change='DATA.data_rsv.payment.balance = DATA.data_rsv.TOTAL.grand_total - DATA.data_rsv.payment.payment_amount'/>
					</div>
				</td>
			</tr>
			<tr ng-show="DATA.data_rsv.payment.payment_type!='ACL'">
				<td>Balance</td>
				<td>
					<div class="input-group">
						<span class="input-group-addon"><?=$vendor["default_currency"]?></span>
						<span class="form-control input-md" style="width:160px">{{fn.formatNumber(DATA.data_rsv.payment.balance, "<?=$vendor["default_currency"]?>")}}</span>
						<?php /*?><input placeholder="Balance" disabled="disabled" type="number" min="0" ng-model='DATA.data_rsv.payment.balance' class="form-control input-md" style="width:160px" /><?php */?>
					</div>
				</td>
			</tr>
		</table>
		
	</div>
	
	<hr />
	<div class="text-center">
		<button class="btn btn-lg btn-primary">Submit Booking</button>
	</div>

</form>
	<?php /*?>
{{DATA.data_rsv}}
<hr />
{{DATA_RSV}}<?php */?>
	
	<?php $this->load->view("crs/reservation/v_trans_reservation_new_add_trip") ?>

<script>
    window.onbeforeunload = function(e) {
		//return 'Are you sure you want to leave?';
       //return 'Sure?';
    };
</script>
<?php if (is_array($vendor["category"])){ ?>
    <div class="pull-right">
        <div class="btn-group">
            <button type="button" class="btn btn-success btn-md dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="glyphicon glyphicon-plus"></span> New Reservation </a> <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a ui-sref="act_reservation.new">Activities</a></li>
                <li><a ui-sref="trans_reservation.new">Transport</a></li>
            </ul>
        </div>
	</div>
<?php }elseif ($this->fn->isVendorCRS_TRANS($vendor)){ ?>
	<div class="pull-right"> <a ui-sref="trans_reservation.new" class="btn btn-success btn-md"> <span class="glyphicon glyphicon-plus"></span> New Reservation </a> </div>
<?php }elseif ($this->fn->isVendorCRS_ACT($vendor)){ ?>
	<div class="pull-right"> <a ui-sref="act_reservation.new" class="btn btn-success btn-md"> <span class="glyphicon glyphicon-plus"></span> New Reservation </a> </div>
<?php } ?>

<h1>Reservation</h1>

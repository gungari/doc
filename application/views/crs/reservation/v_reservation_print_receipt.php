<style type="text/css">
	.dropdown-menu {
		display: inline-block;

	    position: absolute !important;

	    min-width: 125px !important;

	    left: 10%;
	}

</style>
<div ng-init='printReceiptTrans()'>
	<div class="no-print text-center" style="text-align:center !important">
		<select class="form-control input-md change-paper-size" ng-model='print_type' style="width:auto; display:inline">
			<option value="">Normal Paper</option>
			<option value="small"> Small Paper</option>
		</select>
		<br /><br />
	</div>
	<div ng-show='!(DATA.current_booking)'>
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	<div class="small-paper" ng-show="print_type == 'small'">
		<div class="header">
			<div class="title"><?=$vendor["business_name"]?></div>
			<div><?=$vendor["address"]?>, <?=$vendor["regency"]?>, <?=$vendor["province"]?>, <?=$vendor["country"]?></div>
			<div>[P] : <?=$vendor["telephone"]?></div>
			<div>[E] : <?=((@$vendor["contact"]["email"]=='')?$vendor["email"]:$vendor["contact"]["email"])?></div>
			<div><?=$vendor["website"]?></div>
		</div>
		
        <div style="font-weight:bold; text-align:center" class="header">
            <strong>
                <?php /* <span ng-show="DATA.current_booking.booking.status_code == 'TENTATIVE' || DATA.current_booking.booking.status_code == 'UNDEFINITE'">PROFORMA </span>INVOICE */ ?>
				ORDER
            </strong>
        </div>
		
		<strong>BOOKING INFORMATION</strong>
		<table width="100%">
			<tr>
				<td width="80">Code</td>
				<td><strong>#{{DATA.current_booking.booking.booking_code}}</strong></td>
			</tr>
			<tr>
				<td>Date</td>
				<td><strong>{{fn.formatDate(DATA.current_booking.booking.transaction_date, "dd MM yy")}}</strong></td>
			</tr>
			<tr>
				<td>Status</td>
				<td class="text-capitalize"><strong>{{DATA.current_booking.booking.status.toLowerCase()}}</strong></td>
			</tr>
            <tr ng-show="DATA.current_booking.booking.status_code == 'UNDEFINITE'">
                <td>Cut Off Date</td>
                <td><strong>{{fn.formatDate(DATA.current_booking.booking.undefinite_cut_off_date, "dd MM yy")}}</strong></td>
            </tr>
			<tr>
				<td>Source</td>
				<td class="text-capitalize"><strong>{{DATA.current_booking.booking.source.toLowerCase()}}</strong></td>
			</tr>
			<tr ng-show="DATA.current_booking.booking.agent">
				<td>Agent</td>
				<td class="text-capitalize"><strong>{{DATA.current_booking.booking.agent.name}}</strong></td>
			</tr>
		</table>
		<hr />
		
		<strong>CUSTOMER INFORMATION</strong>
		<table width="100%">
			<tr>
				<td width="80">Full Name</td>
				<td><strong>{{DATA.current_booking.booking.customer.first_name}} {{DATA.current_booking.booking.customer.last_name}}</strong></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><strong>{{DATA.current_booking.booking.customer.email}}</strong></td>
			</tr>
            <tr>
				<td>Telephone</td>
				<td><strong>{{DATA.current_booking.booking.customer.phone}}</strong></td>
			</tr>
			<tr>
				<td>Country</td>
				<td><strong>{{DATA.current_booking.booking.customer.country_name}}</strong></td>
			</tr>
		</table>
		
		<hr />
		
		<table class="table table-bordered table-condensed" width="100%" cellspacing="1" cellpadding="8">
			<tr class="info table-header">
				<td colspan="2"><strong>Description</strong></td>
			</tr>
			<tbody ng-repeat='detail in DATA.current_booking.booking.detail' ng-hide="detail.booking_detail_status_code == 'CANCEL' && detail.subtotal == 0">
				<tr>
					<td colspan="2">
						<div>
							Voucher#
							<div style="font-size:16px">{{detail.voucher_code}}</div>
						</div>
						<div>
							<strong>{{detail.departure.port.name}} ({{detail.departure.port.port_code}}) : {{detail.departure.time}}</strong>
							&nbsp;&nbsp;<i class="fa fa-chevron-right"></i>&nbsp;&nbsp;
							<strong>{{detail.arrival.port.name}} ({{detail.arrival.port.port_code}}) : {{detail.arrival.time}}</strong>
						</div>
						<div><small>({{detail.rates.name}})</small></div>
						<div><strong>{{fn.formatDate(detail.date, "dd MM yy")}}</strong></div>
						<div>
							<span ng-show='detail.qty_1 > 0'>{{detail.qty_1}} Adult</span>
							<span ng-show='detail.qty_2 > 0'>{{detail.qty_2}} Child</span>
							<span ng-show='detail.qty_3 > 0'>{{detail.qty_3}} Infant</span>
						</div>
						<br />
						<div class="text-right">
							<strong ng-show='detail.subtotal == detail.subtotal_before_cancel'>
								{{detail.rates.currency}}
								{{fn.formatNumber((detail.rates.rates_1 * detail.qty_1) + (detail.rates.rates_2 * detail.qty_2) + (detail.rates.rates_3 * detail.qty_3), detail.rates.currency)}}
							</strong>
							<strong ng-show='detail.subtotal != detail.subtotal_before_cancel'>
								{{detail.rates.currency}}
								{{fn.formatNumber(detail.subtotal, detail.rates.currency)}}
							</strong>
						</div>
					</td>
				</tr>
				<tr ng-show='detail.pickup'>
					<td colspan="2">
						Pickup Service ({{detail.pickup.area}} - {{detail.pickup.time}})
						<div class="text-right">
							<strong ng-show='detail.pickup.price_pickup > 0 && detail.subtotal == detail.subtotal_before_cancel'>
								{{detail.rates.currency}}
								{{fn.formatNumber(detail.pickup.price,detail.rates.currency)}}
							</strong>
						</div>
					</td>
				</tr>
				<tr ng-show='detail.dropoff'>
					<td colspan="2">
						Dropoff Service ({{detail.dropoff.area}} - {{detail.dropoff.time}})
						<div class="text-right">
							<strong ng-show='detail.dropoff.price_dropoff > 0 && detail.subtotal == detail.subtotal_before_cancel'>
								{{detail.rates.currency}}
								{{fn.formatNumber(detail.dropoff.price,detail.rates.currency)}}
							</strong>
						</div>
					</td>
				</tr>
				<tr ng-show='detail.additional_service' ng-repeat='additional_service in detail.additional_service'>
					<td>{{additional_service.name}} ({{additional_service.qty}}x @ {{detail.rates.currency}} {{fn.formatNumber(additional_service.price,detail.rates.currency)}})</td>
					<td align="right">
						<strong>
						{{detail.rates.currency}} {{fn.formatNumber((additional_service.price * additional_service.qty),detail.rates.currency)}}
						</strong>
					</td>
				</tr>
			</tbody>
			<tr class="success table-header">
				<td align="right"><strong>Total</strong></td>
				<td align="right">
					<strong>
						{{DATA.current_booking.booking.currency}}
						{{fn.formatNumber(DATA.current_booking.booking.total_before_discount,DATA.current_booking.booking.currency)}}
					</strong>
				</td>
			</tr>
			<tr class="success table-header">
				<td align="right">
					<strong>
						Discount 
						<span ng-show='DATA.current_booking.booking.discount.amount > 0'>
							&nbsp;&nbsp;
							<span ng-show="DATA.current_booking.booking.discount.type == '%'">({{DATA.current_booking.booking.discount.value}}%)</span>
							<span ng-show="DATA.current_booking.booking.discount.type != '%'">({{fn.formatNumber(DATA.current_booking.booking.discount.value,DATA.current_booking.booking.currency)}})</span>
						</span>
					</strong>
				</td>
				<td align="right">
					<strong>
						{{DATA.current_booking.booking.currency}}
						{{fn.formatNumber(DATA.current_booking.booking.discount.amount,DATA.current_booking.booking.currency)}}
					</strong>
				</td>
			</tr>
			<tr class="info table-header">
				<td align="right"><strong>Grand Total</strong></td>
				<td align="right">
					<strong>
						{{DATA.current_booking.booking.currency}}
						{{fn.formatNumber(DATA.current_booking.booking.grand_total,DATA.current_booking.booking.currency)}}
					</strong>
				</td>
			</tr>
			<tr ng-show="DATA.current_booking.booking.status_code != 'UNDEFINITE'" class="info table-header">
				<td align="right"><strong>Total Payment</strong></td>
				<td align="right">
					<strong>
						{{DATA.current_booking.booking.currency}}
						({{fn.formatNumber(DATA.current_booking.booking.total_payment,DATA.current_booking.booking.currency)}})
					</strong>
				</td>
			</tr>
			<tr ng-show="DATA.current_booking.booking.status_code != 'UNDEFINITE'" class="info table-header">
				<td align="right"><strong>Outstanding Order</strong></td>
				<td align="right">
					<strong>
						{{DATA.current_booking.booking.currency}}
						{{fn.formatNumber(DATA.current_booking.booking.balance,DATA.current_booking.booking.currency)}}
					</strong>
				</td>
			</tr>
            <tr class="info table-header" ng-show="DATA.current_booking.booking.total_refund && DATA.current_booking.booking.status_code != 'VOID'">
                <td align="right"><strong>Refund</strong></td>
                <td align="right">
                    <strong class="ng-binding">
                        {{DATA.current_booking.booking.currency}}
						{{fn.formatNumber(DATA.current_booking.booking.total_refund,DATA.current_booking.booking.currency)}}
                    </strong>
                </td>
            </tr>
		</table>
		
	</div>
	<br />
	<div class="normal-paper" ng-show="print_type != 'small' && DATA.current_booking">
		<div class="header">
			<div class="pull-right text-right">
				<br>
				<div style="font-size:18px">
                	 <?php /* <span ng-show="DATA.current_booking.booking.status_code == 'TENTATIVE' || DATA.current_booking.booking.status_code == 'UNDEFINITE'">PROFORMA </span>INVOICE */ ?>
					 ORDER
                </div>

				<div class="title">
					#{{DATA.current_booking.booking.booking_code}}
				</div>
			</div>
			
			<div class="title"><?=$vendor["business_name"]?></div>
			<div><?=$vendor["address"]?>, <?=$vendor["regency"]?>, <?=$vendor["province"]?>, <?=$vendor["country"]?></div>
			<div>[P] : <?=$vendor["telephone"]?>, [E] : <?=((@$vendor["contact"]["email"]=='')?$vendor["email"]:$vendor["contact"]["email"])?></div>
			<div><?=$vendor["website"]?></div>
		</div>

		<br />
		<table width="100%">
			<tr>
				<td width="50%" valign="top">
					<strong>BOOKING INFORMATION</strong>
					<table width="100%">
						<tr>
							<td width="80">Order</td>
							<td><strong>#{{DATA.current_booking.booking.booking_code}}</strong></td>
						</tr>
						<tr>
							<td>Date</td>
							<td><strong>{{fn.formatDate(DATA.current_booking.booking.transaction_date, "dd MM yy")}}</strong></td>
						</tr>
						<tr>
							<td>Status</td>
							<td class="text-capitalize"><strong>{{DATA.current_booking.booking.status.toLowerCase()}}</strong></td>
						</tr>
                        <tr ng-show="DATA.current_booking.booking.status_code == 'UNDEFINITE'">
							<td>Cut Off Date</td>
							<td><strong>{{fn.formatDate(DATA.current_booking.booking.undefinite_cut_off_date, "dd MM yy")}}</strong></td>
						</tr>
						<tr>
							<td>Source</td>
							<td class="text-capitalize"><strong>{{DATA.current_booking.booking.source.toLowerCase()}}</strong></td>
						</tr>
						<tr ng-show="DATA.current_booking.booking.agent">
							<td>Agent</td>
							<td class="text-capitalize"><strong>{{DATA.current_booking.booking.agent.name}}</strong></td>
						</tr>
					</table>
				</td>
				<td width="50%" valign="top">
					<strong>CUSTOMER INFORMATION</strong>
					<table width="100%">
						<tr>
							<td width="100">Full Name</td>
							<td><strong>{{DATA.current_booking.booking.customer.first_name}} {{DATA.current_booking.booking.customer.last_name}}</strong></td>
						</tr>
						<tr>
							<td>Email</td>
							<td><strong>{{DATA.current_booking.booking.customer.email}}</strong></td>
						</tr>
                        <tr>
							<td>Telephone</td>
							<td><strong>{{DATA.current_booking.booking.customer.phone}}</strong></td>
						</tr>
						<tr>
							<td>Country</td>
							<td><strong>{{DATA.current_booking.booking.customer.country_name}}</strong></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<br />
			<table width="100%">
				<tr>
					<td><strong>Remarks / Special Request</strong></td>
				</tr>
				<tr>
					<td ng-show="DATA.current_booking.booking.remarks">
						<div style="width: 100%;overflow: hidden;text-align: justify;">
							{{DATA.current_booking.booking.remarks}}
						</div>
					</td>					
				
					<td ng-show="!DATA.current_booking.booking.remarks"><strong> - </strong></td>
				</tr>
			</table>
		<br />
		<table class="table table-bordered table-condensed" width="100%" cellspacing="1" cellpadding="8">
			<tr class="info table-header">
				<td><strong>Description</strong></td>
				<td width="150" align="right"><strong>Sub Total</strong></td>
			</tr>
			<tbody ng-repeat='detail in DATA.current_booking.booking.detail' ng-hide="detail.booking_detail_status_code == 'CANCEL' && detail.subtotal == 0">
				<tr>
					<td>
						<div class="pull-right text-right">
							Voucher#
							<div style="font-size:20px">{{detail.voucher_code}}</div>
						</div>
						<div>
	                    	<div ng-show="detail.product_type == 'ACT'">
	                        	<strong>{{detail.product.name}}</strong>
	                        </div>
	                        <div ng-show="detail.product_type == 'TRANS'">
	                            <strong>{{detail.departure.port.name}} ({{detail.departure.port.port_code}}) : {{detail.departure.time}}</strong>
	                            &nbsp;&nbsp;<i class="fa fa-chevron-right"></i>&nbsp;&nbsp;
	                            <strong>{{detail.arrival.port.name}} ({{detail.arrival.port.port_code}}) : {{detail.arrival.time}}</strong>
	                        </div>
						</div>
						<div><small>({{detail.rates.name}})</small></div>
						<div><strong>{{fn.formatDate(detail.date, "dd MM yy")}}</strong></div>
						<div>
							<span ng-show='detail.qty_1 > 0'>{{detail.qty_1}} Adult</span>
							<span ng-show='detail.qty_2 > 0'>{{detail.qty_2}} Child</span>
							<span ng-show='detail.qty_3 > 0'>{{detail.qty_3}} Infant</span>
						</div>
					</td>
					<td align="right">
						<strong ng-show='detail.subtotal == detail.subtotal_before_cancel'>
							{{detail.rates.currency}}
							{{fn.formatNumber((detail.rates.rates_1 * detail.qty_1) + (detail.rates.rates_2 * detail.qty_2) + (detail.rates.rates_3 * detail.qty_3), detail.rates.currency)}}
						</strong>
						<strong ng-show='detail.subtotal != detail.subtotal_before_cancel'>
							{{detail.rates.currency}}
							{{fn.formatNumber(detail.subtotal, detail.rates.currency)}}
						</strong>
					</td>
				</tr>
				<tr ng-show='detail.pickup'>
					<td>
						Pickup Service ({{detail.pickup.area}} - {{detail.pickup.time}})
					</td>
					<td align="right">
						<strong ng-show='detail.pickup.price_pickup > 0 && detail.subtotal == detail.subtotal_before_cancel'>
							{{detail.rates.currency}}
							{{fn.formatNumber(detail.pickup.price_pickup,detail.rates.currency)}}
						</strong>
					</td>
				</tr>
				<tr ng-show='detail.dropoff'>
					<td>
						Dropoff Service ({{detail.dropoff.area}} - {{detail.dropoff.time}})
					</td>
					<td align="right">
						<strong ng-show='detail.dropoff.price_dropoff > 0 && detail.subtotal == detail.subtotal_before_cancel'>
							{{detail.rates.currency}}
							{{fn.formatNumber(detail.dropoff.price_dropoff,detail.rates.currency)}}
						</strong>
					</td>
				</tr>
				<tr ng-show='detail.additional_service' ng-repeat='additional_service in detail.additional_service'>
					<td>{{additional_service.name}} ({{additional_service.qty}}x @ {{detail.rates.currency}} {{fn.formatNumber(additional_service.price,detail.rates.currency)}})</td>
					<td align="right">
						<strong>
						{{detail.rates.currency}} {{fn.formatNumber((additional_service.price * additional_service.qty),detail.rates.currency)}}
						</strong>
					</td>
				</tr>
			</tbody>
			<tr class="success table-header">
				<td align="right"><strong>Total</strong></td>
				<td align="right">
					<strong>
						{{DATA.current_booking.booking.currency}}
						{{fn.formatNumber(DATA.current_booking.booking.total_before_discount,DATA.current_booking.booking.currency)}}
					</strong>
				</td>
			</tr>
			<tr class="success table-header">
				<td align="right">
					<strong>
						Discount 
						<span ng-show='DATA.current_booking.booking.discount.amount > 0'>
							&nbsp;&nbsp;
							<span ng-show="DATA.current_booking.booking.discount.type == '%'">({{DATA.current_booking.booking.discount.value}}%)</span>
							<span ng-show="DATA.current_booking.booking.discount.type != '%'">({{fn.formatNumber(DATA.current_booking.booking.discount.value,DATA.current_booking.booking.currency)}})</span>
						</span>
					</strong>
				</td>
				<td align="right">
					<strong>
						{{DATA.current_booking.booking.currency}}
						{{fn.formatNumber(DATA.current_booking.booking.discount.amount,DATA.current_booking.booking.currency)}}
					</strong>
				</td>
			</tr>
			<tr class="info table-header">
				<td align="right"><strong>Grand Total</strong></td>
				<td align="right">
					<strong>
						{{DATA.current_booking.booking.currency}}
						{{fn.formatNumber(DATA.current_booking.booking.grand_total,DATA.current_booking.booking.currency)}}
					</strong>
				</td>
			</tr>
			<tr ng-show="DATA.current_booking.booking.status_code != 'UNDEFINITE'" class="info table-header">
				<td align="right"><strong>Total Payment</strong></td>
				<td align="right">
					<strong>
						{{DATA.current_booking.booking.currency}}
						({{fn.formatNumber(DATA.current_booking.booking.total_payment,DATA.current_booking.booking.currency)}})
					</strong>
				</td>
			</tr>
			<tr ng-show="DATA.current_booking.booking.status_code != 'UNDEFINITE'" class="info table-header">
				<td align="right"><strong>Outstanding Order</strong></td>
				<td align="right">
					<strong>
						{{DATA.current_booking.booking.currency}}
						{{fn.formatNumber(DATA.current_booking.booking.balance,DATA.current_booking.booking.currency)}}
					</strong>
				</td>
			</tr>
            <tr class="info table-header" ng-show="DATA.current_booking.booking.total_refund && DATA.current_booking.booking.status_code != 'VOID'">
                <td align="right"><strong>Refund</strong></td>
                <td align="right">
                    <strong class="ng-binding">
                        {{DATA.current_booking.booking.currency}}
						{{fn.formatNumber(DATA.current_booking.booking.total_refund,DATA.current_booking.booking.currency)}}
                    </strong>
                </td>
            </tr>
		</table>
	</div>

	<br />
	<div class="no-print">
		<div class="row">
			<div class="dropdown col-md-6 text-left" style="margin-top: 10px;">
				<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
		    		<span class="glyphicon glyphicon-envelope"></span>&nbsp;Send Email
				    <span class="caret"></span>
				</button>
					<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					  <li style="cursor: pointer;" ng-show="DATA.current_booking.booking.agent"><a ng-click="send_email_profoma(DATA.current_booking.booking.booking_code, '1')">To Agent</a></li>
					  <li ng-show="DATA.current_booking.booking.agent && DATA.current_booking.booking.agent.email" style="cursor: pointer;"><a ng-click="send_email_profoma(DATA.current_booking.booking.booking_code, '0')">To Customer</a></li>
					  <li ng-show="!DATA.current_booking.booking.agent && !DATA.current_booking.booking.agent.email" style="cursor: pointer;"><a data-toggle="modal" data-target="#payment-detail" class="delete-icon" ng-show="payment.id != ''">To Customer</a></li>
					</ul>
			</div>
		</div>
	</div>
	<div class="modal fade" id="payment-detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	  <form ng-submit="update_email_customer($event, 'invoice')">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">
				<span>Customer Email</span>
			</h4>
		  </div>
		  <div class="modal-body">
		  <div ng-show='error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in error_msg'>{{err}}</li></ul></div>
			<table class="table table-borderless table-condensed" style="margin-bottom: 0 !important;">
				<tr>
					<td>Email</td>
					<td><input placeholder="Email" type="email" class="form-control input-md" ng-model='DATA.current_booking.booking.customer.email' /></td>
				</tr>
			</table>
		  </div>
		  <div class="modal-footer" style="text-align:center">
			<button type="submit"  class="btn btn-primary">
			<small><span class="glyphicon glyphicon-envelope"></span>
			 &nbsp;Send <span ng-show="DATA.current_booking.booking.status_code == 'TENTATIVE' || DATA.current_booking.booking.status_code == 'UNDEFINITE'">Proforma </span>Invoice</small>
			</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
		</form>
	  </div>
	</div>
</div>
<script type="text/javascript">
	function print_billing(){
		window.print();
	}
</script>
<script>
	$(".change-paper-size").change(function(){
		var _this = $(this);
		
		if (_this.val() == 'small'){
			$(".print_area").addClass("small_paper");
		}else{
			$(".print_area").removeClass("small_paper");
		}
	});
</script>

<style>
	.print_area{background:none !important; padding:0 !important;}
	.small_paper{width:8cm !important; margin:0 auto !important}
	.normal-paper{background:#FFF; padding:10px}
	.small-paper{width:8cm; margin:auto; padding:10px; background:#FFF}
	.small-paper .header .title{font-size:18px !important}
</style>


<div ng-init='arrivalDepartureTransPrint()'>
	<div ng-show='arrival_departure'>
		<h4 class="text-capitalize">{{search.type}}</h4>
		<table class="xtable xtable-condensed xtable-borderless">
			<tr>
				<td width="50">Date</td>
				<td width="200"><strong>{{fn.formatDate(search.date,"d MM yy")}}</strong></td>
				<td width="70">Boat</td>
				<td width="200"><strong>{{search.boat.name}} ({{search.boat.capacity}} seats)</strong></td>
				<td width="50"></td>
				<td></td>
			</tr>
			<tr>
				<td>From</td>
				<td><strong>{{(search.departure_port=='ALL')?'All':search.departure_port.name}}</strong></td>
				<td>To</td>
				<td><strong>{{(search.destination_port=='ALL')?'All':search.destination_port.name}}</strong></td>
				<td><?php /*?>Time<?php */?></td>
				<td><?php /*?><strong>{{search.time}}</strong><?php */?></td>
			</tr>
			<tr>
				<td>Total</td>
				<td><strong>{{summary.total_all}} Pax</strong></td>
				<td>Check In</td>
				<td><strong>{{summary.total_checked_in}} Pax</strong></td>
				<td></td>
				<td></td>
			</tr>
		</table>
		<br />
		
		<table class="table table-bordered table-condensed" style="font-size:11px">
			<tr class="success" style="font-weight:bold">
				<td width="30">No</td>
				<td width="90">Voucher#</td>
				<td>Name</td>
				<!-- <td width="50" align="center" title="Nationality">Nat.</td>
				<td width="110">ID Number</td>
				<td width="100">Phone</td> -->
				<?php /*?><td width="150">Booking Source</td><?php */?>
				<?php /*?><td colspan="2" align="center">Trip</td><?php */?>
				<td width="150">Booking Source</td>
						<td colspan="2" align="center">
							Trip
							<?php /*?><span ng-show="search.type =='departure'">Destination</span>
							<span ng-show="search.type =='arrival'">Departure</span><?php */?>
						</td>
				<td width="300">Pickup / Dropoff</td>
				<td width="65" align="center">Check In</td>
			</tr>
			<tbody ng-repeat='passenger_list in  arrival_departure'>
				<tr style="background:#FAFAFA">
					<td colspan="8">
						<strong>{{passenger_list.departure_port.name}} ({{passenger_list.departure_time}}) </strong>
						-
						<strong>{{passenger_list.arrival_port.name}} ({{passenger_list.arrival_time}}) </strong>
					</td>
				</tr>
				<tr ng-repeat="arr_dept in  passenger_list.passenger_list" ng-class="{'not_checkin':arr_dept.checkin_status != '1'}">
					<td>{{($index+1)}}</td>
					<td>{{arr_dept.voucher_code}}</td>
					<td>
						<div>{{arr_dept.first_name}} {{arr_dept.last_name}}</div>
						<div title="{{arr_dept.country_name}}">{{arr_dept.country_code}}</div>
						<div ng-show="arr_dept.passport_number">{{arr_dept.passport_number}}</div>
						<div ng-show="arr_dept.phone">{{arr_dept.phone}}</div>
					</td>
					<!-- <td title="{{arr_dept.country_name}}" align="center">{{arr_dept.country_code}}</td>
					<td>{{arr_dept.passport_number}}</td>
					<td>{{arr_dept.phone}}</td> -->
					<?php /*?><td>
						<div ng-show='!arr_dept.agent' class="text-capitalize">{{arr_dept.source.toLowerCase()}}</div>
						<div ng-show='arr_dept.agent'>{{arr_dept.agent.name}}</div>
					</td><?php */?>
					<?php /*?><td width="90" align="center">{{arr_dept.departure.port_code}} ({{arr_dept.departure.time}})</td>
					<td width="90" align="center">{{arr_dept.arrival.port_code}} ({{arr_dept.arrival.time}})</td><?php */?>
					<td>
								<div ng-show='!arr_dept.agent' class="text-capitalize">{{arr_dept.source.toLowerCase()}}</div>
								<div ng-show='arr_dept.agent'>{{arr_dept.agent.name}}</div>
							</td>
							<td width="90" align="center">{{arr_dept.departure.port_code}} ({{arr_dept.departure.time}})</td>
							<td width="90" align="center">{{arr_dept.arrival.port_code}} ({{arr_dept.arrival.time}})</td>
					<td>
						<div ng-show='arr_dept.pickup'>{{arr_dept.pickup.hotel_name}}</div>
						<div ng-show='arr_dept.dropoff'>{{arr_dept.dropoff.hotel_name}}</div>
					</td>
					<td align="center"><span class="glyphicon glyphicon-ok" ng-show="arr_dept.checkin_status == '1'"></span> </td>
					<?php /*?><td>
						<span ng-show="search.type =='departure'">{{DATA.ports_by_code[arr_dept.arrival.port_code].name}}</span>
						<span ng-show="search.type =='arrival'">{{DATA.ports_by_code[arr_dept.departure.port_code].name}}</span>
					</td><?php */?>
				</tr>
			</tbody>
			<?php /*?><tr class="header bold">
				<td width="40">No</td>
				<td width="120" align="center">Voucher#</td>
				<td>Name</td>
				<td width="150">Phone</td>
				<td width="150">Country</td>
				<td width="70" align="center">Check In</td>
			</tr>
			<tr ng-repeat="arr_dept in  arrival_departure" ng-class="{'not_checkin':arr_dept.checkin_status != '1'}">
				<td>{{($index+1)}}</td>
				<td align="center">{{arr_dept.voucher_code}}</td>
				<td>{{arr_dept.first_name}} {{arr_dept.last_name}}</td>
				<td>{{arr_dept.phone}}</td>
				<td>{{arr_dept.country_name}}</td>
				<td align="center">
					<span ng-show="arr_dept.checkin_status == '1'" class="glyphicon glyphicon-ok"></span>
				</td>
			</tr><?php */?>
		</table>
		
	</div>
	
	<style>
		table tr.not_checkin td{color:#666 !important; background:#EEE !important}
	</style>
	<?php /*?>{{dept}}
	<br />
	{{DATA_R}}<?php */?>
</div>
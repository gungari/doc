<div ng-init='printReceiptTransPayment()'>
	<div class="no-print text-center" style="text-align:center !important">
		<select class="form-control input-md change-paper-size" ng-model='print_type' style="width:auto; display:inline">
			<option value="">Normal Paper</option>
			<option value="small"> Small Paper</option>
		</select>
		<br /><br />
	</div>
	
	<div class="small-paper" ng-show="print_type == 'small'">
		<div class="header">
			<div class="title"><?=$vendor["business_name"]?></div>
			<div><?=$vendor["address"]?>, <?=$vendor["regency"]?>, <?=$vendor["province"]?>, <?=$vendor["country"]?></div>
			<div>[P] : <?=$vendor["telephone"]?></div>
			<div>[E] : <?=((@$vendor["contact"]["email"]=='')?$vendor["email"]:$vendor["contact"]["email"])?></div>
			<div><?=$vendor["website"]?></div>
		</div>
		
        <div style="text-align:center" class="header">
            
               
                <span ng-show="DATA.current_booking.booking.status_code != 'UNDEFINITE'">RECEIPT <strong>#{{DATA.current_booking.booking.detail.payment_code}}</strong></span>
            
        </div>
		
		<strong>BOOKING INFORMATION</strong>
		<table width="100%">
			<tr>
				<td width="80">Code</td>
				<td><strong>#{{DATA.current_booking.booking.booking_code}}</strong></td>
			</tr>
			<tr>
				<td>Date</td>
				<td><strong>{{fn.formatDate(DATA.current_booking.booking.transaction_date, "dd MM yy")}}</strong></td>
			</tr>
			<tr>
				<td>Status</td>
				<td class="text-capitalize"><strong>{{DATA.current_booking.booking.status.toLowerCase()}}</strong></td>
			</tr>
            <tr ng-show="DATA.current_booking.booking.status_code == 'UNDEFINITE'">
                <td>Cut Off Date</td>
                <td><strong>{{fn.formatDate(DATA.current_booking.booking.undefinite_cut_off_date, "dd MM yy")}}</strong></td>
            </tr>
			<tr>
				<td>Source</td>
				<td class="text-capitalize"><strong>{{DATA.current_booking.booking.source.toLowerCase()}}</strong></td>
			</tr>
			<tr ng-show="DATA.current_booking.booking.agent">
				<td>Agent</td>
				<td class="text-capitalize"><strong>{{DATA.current_booking.booking.agent.name}}</strong></td>
			</tr>
		</table>
		<hr />
		
		<strong>CUSTOMER INFORMATION</strong>
		<table width="100%">
			<tr>
				<td width="80">Full Name</td>
				<td><strong>{{DATA.current_booking.booking.customer.first_name}} {{DATA.current_booking.booking.customer.last_name}}</strong></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><strong>{{DATA.current_booking.booking.customer.email}}</strong></td>
			</tr>
            <tr>
				<td>Telephone</td>
				<td><strong>{{DATA.current_booking.booking.customer.phone}}</strong></td>
			</tr>
			<tr>
				<td>Country</td>
				<td><strong>{{DATA.current_booking.booking.customer.country_name}}</strong></td>
			</tr>
		</table>
		
		<hr />
		
		<table class="table table-bordered table-condensed" width="100%" cellspacing="1" cellpadding="8">
			<tr class="info table-header">
				<td><strong>Date</strong></td>
				<td><strong>Payment Type</strong></td>
				<td align="right"><strong>Amount</strong></td>

			</tr>
		
			<tr>
				<td>{{fn.formatDate(DATA.current_booking.booking.detail.date, "dd MM yy")}}</td>
				<td >{{DATA.current_booking.booking.detail.payment_type}}</td>
				<td align="right">{{DATA.current_booking.booking.detail.currency}} {{fn.formatNumber(DATA.current_booking.booking.detail.amount,DATA.current_booking.booking.detail.currency)}}</td>

			</tr>
		
		</table>
		<br />
		<dir style="margin: 0 !important;padding: 0 !important;" class="footer">
			<div class="text-center">
				<strong>Thank You</strong><br/>
				<strong><?=@$vendor["business_name"]?></strong>
			</div>
		</dir>
	</div>
	<br />
	<div class="normal-paper" ng-show="print_type != 'small'">
		<div class="header">
			<div class="pull-right text-right">
				<br>
				<div style="font-size:18px">
                	<!-- <span ng-show="DATA.current_booking.booking.status_code == 'UNDEFINITE'">Invoice</span> -->
                    <span ng-show="DATA.current_booking.booking.status_code != 'UNDEFINITE'">Receipt</span>
                </div>
				<div class="title">
					#{{DATA.current_booking.booking.detail.payment_code}}
				</div>
			</div>
			
			<div class="title"><?=$vendor["business_name"]?></div>
			<div><?=$vendor["address"]?>, <?=$vendor["regency"]?>, <?=$vendor["province"]?>, <?=$vendor["country"]?></div>
			<div>[P] : <?=$vendor["telephone"]?>, [E] : <?=((@$vendor["contact"]["email"]=='')?$vendor["email"]:$vendor["contact"]["email"])?></div>
			<div><?=$vendor["website"]?></div>
		</div>
		
		<br />
		<table width="100%">
			<tr>
				<td width="50%" valign="top">
					<strong>BOOKING INFORMATION</strong>
					<table width="100%">
						<tr>
							<td width="80">Code</td>
							<td><strong>#{{DATA.current_booking.booking.booking_code}}</strong></td>
						</tr>
						<tr>
							<td>Date</td>
							<td><strong>{{fn.formatDate(DATA.current_booking.booking.transaction_date, "dd MM yy")}}</strong></td>
						</tr>
						<tr>
							<td>Status</td>
							<td class="text-capitalize"><strong>{{DATA.current_booking.booking.status.toLowerCase()}}</strong></td>
						</tr>
                        <tr ng-show="DATA.current_booking.booking.status_code == 'UNDEFINITE'">
							<td>Cut Off Date</td>
							<td><strong>{{fn.formatDate(DATA.current_booking.booking.undefinite_cut_off_date, "dd MM yy")}}</strong></td>
						</tr>
						<tr>
							<td>Source</td>
							<td class="text-capitalize"><strong>{{DATA.current_booking.booking.source.toLowerCase()}}</strong></td>
						</tr>
						<tr ng-show="DATA.current_booking.booking.agent">
							<td>Agent</td>
							<td class="text-capitalize"><strong>{{DATA.current_booking.booking.agent.name}}</strong></td>
						</tr>
					</table>
				</td>
				<td width="50%" valign="top">
					<strong>CUSTOMER INFORMATION</strong>
					<table width="100%">
						<tr>
							<td width="100">Full Name</td>
							<td><strong>{{DATA.current_booking.booking.customer.first_name}} {{DATA.current_booking.booking.customer.last_name}}</strong></td>
						</tr>
						<tr>
							<td>Email</td>
							<td><strong>{{DATA.current_booking.booking.customer.email}}</strong></td>
						</tr>
                        <tr>
							<td>Telephone</td>
							<td><strong>{{DATA.current_booking.booking.customer.phone}}</strong></td>
						</tr>
						<tr>
							<td>Country</td>
							<td><strong>{{DATA.current_booking.booking.customer.country_name}}</strong></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<br />
			<table width="100%">
				<tr>
					<td><strong>Remarks / Special Request</strong></td>
				</tr>
				<tr>
					<td ng-show="DATA.current_booking.booking.remarks">
						<div style="width: 100%;overflow: hidden;text-align: justify;">
							{{DATA.current_booking.booking.remarks}}
						</div>
					</td>					
				
					<td ng-show="!DATA.current_booking.booking.remarks"><strong> - </strong></td>
				</tr>
			</table>
		<br />
		<table class="table table-bordered table-condensed" width="100%" cellspacing="1" cellpadding="8">
			<tr class="info table-header">
				<td><strong>Date</strong></td>
				<td><strong>Payment Type</strong></td>
				<td width="150" align="right"><strong>Amount</strong></td>
			</tr>
			<tbody>
				
				<tr>
					<td>{{fn.formatDate(DATA.current_booking.booking.detail.date, "dd MM yy")}}</td>
					<td >{{DATA.current_booking.booking.detail.payment_type}}</td>
					<td align="right">{{DATA.current_booking.booking.detail.currency}} {{fn.formatNumber(DATA.current_booking.booking.detail.amount,DATA.current_booking.booking.detail.currency)}}</td>
				</tr>
			</tbody>
			
		</table>
		<br />
		<dir style="margin: 0 !important;padding: 0 !important;" class="footer">
			<div class="text-center">
				<strong>Thank You</strong><br/>
				<strong><?=@$vendor["business_name"]?></strong>
			</div>
		</dir>
	</div>

	<div class="no-print row">
		<div class="dropdown col-md-6 text-left" style="margin-top: 10px;">
				<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
		    		<span class="glyphicon glyphicon-envelope"></span>&nbsp;Send Email
				    <span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
				  <li style="cursor: pointer;" ng-show="DATA.current_booking.booking.agent"><a ng-click="send_email_receipt(DATA.current_booking.booking.booking_code, '1', DATA.current_booking.booking.detail.payment_code)">To Agent</a></li>
				  <!-- <li ng-show="!DATA.current_booking.booking.agent" style="cursor: pointer;"><a ng-click="send_email_receipt(DATA.current_booking.booking.booking_code, '0', DATA.current_booking.booking.detail.payment_code)">To Customer</a></li> -->
				  <li ng-show="!DATA.current_booking.booking.agent" style="cursor: pointer;"><a data-toggle="modal" data-target="#payment-detail" class="delete-icon" ng-show="payment.id != ''">To Customer</a></li>
				</ul>
		</div>
		<!-- <div class="col-md-6 text-right" style="margin-top: 10px;">
			<button onclick="print_billing()" type="button" class="btn btn-primary">
				<span class="glyphicon glyphicon-print"></span>
				&nbsp;Print Receipt
			</button>
		</div> -->
	</div>
	<div class="modal fade" id="payment-detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	  <form ng-submit="update_email_customer($event, 'receipt')">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">
				<span>Customer Email</span> 
			</h4>
		  </div>
		  <div class="modal-body">
		  <div ng-show='error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in error_msg'>{{err}}</li></ul></div>
			<table class="table table-borderless table-condensed" style="margin-bottom: 0 !important;">
				<tr>
					<td>Email</td>
					<td><input placeholder="Email" type="email" class="form-control input-md" ng-model='DATA.current_booking.booking.customer.email' /></td>
				</tr>
			</table>
		  </div>
		  <div class="modal-footer" style="text-align:center">
			<button type="submit"  class="btn btn-primary">
			<small><span class="glyphicon glyphicon-envelope"></span>
			 &nbsp;Send Receipt</small>
			</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
		</form>
	  </div>
	</div>
</div>
<script type="text/javascript">
	function print_billing(){
		window.print();
	}
</script>
<script>
	$(".change-paper-size").change(function(){
		var _this = $(this);
		
		if (_this.val() == 'small'){
			$(".print_area").addClass("small_paper");
		}else{
			$(".print_area").removeClass("small_paper");
		}
	});
</script>

<style>


	.print_area{background:none !important; padding:0 !important;}
	.small_paper{width:8cm !important; margin:0 auto !important}
	.normal-paper{background:#FFF; padding:10px}
	.small-paper{width:8cm; margin:auto; padding:10px; background:#FFF}
	.small-paper .header .title{font-size:18px !important}

</style>
<?php
//pre($vendor)
?>

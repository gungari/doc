<div class="text-center" ng-show='load_data_packages_show_loading'>
    <strong><em>Loading...</em></strong>
</div>

<div ng-show='!load_data_packages_show_loading' class="search-packages-result">
<form ng-submit="packages_add_to_data_rsv($event)" class="frm-add-new-packages">
    <div ng-show='add_packages.error_desc.length>0' class="alert alert-danger"><ul><li ng-repeat='err in add_packages.error_desc'>{{err}}</li></ul></div>
    
    <div ng-show='add_packages.status == "SUCCESS"'>
    	<?php //JIKA BELUM DIPILIH ?>
        <div class="packages">
        	<div class="products" ng-show='!add_packages.selected_packages'>
                <div class="product ">
                    Filter : <input type="text" ng-model="filter_packages" class="form-control input-sm ng-pristine ng-untouched ng-valid ng-empty" placeholder="Search" style="width:200px; display:inline">
                </div>
        	</div>
            <div ng-repeat="package in add_packages.packages | filter : filter_packages" class="packages-item" 
            	ng-click="select_this_package(package);"
                ng-hide="add_packages.selected_packages && add_packages.selected_packages.id != package.id"
                ng-class="{'selected':add_packages.selected_packages.id == package.id}">

                <h4>{{package.package_code}} - {{package.name}}</h4>
                <p ng-show='package.description'>{{package.description}}</p>
                
                <div ng-hide="add_packages.selected_packages.id == package.id">
                    <p>
                        <strong>Routes Option</strong>
                        <ol style="margin:0; padding-left:20px">
                            <li ng-repeat="routes in package.routes_option">{{routes.str_route}}</li>
                        </ol>
                    </p>
                    <p>
                        <span>
                            Adult : <strong>{{package.currency}} {{fn.formatNumber(package.rates.rates_1, package.currency)}}</strong>
                        </span>
                        <span ng-show='package.rates.rates_2 > 0'>
                            Child : <strong>{{package.currency}} {{fn.formatNumber(package.rates.rates_2, package.currency)}}</strong>
                        </span>
                        <span ng-show='package.rates.rates_3 > 0'>
                            Infant : <strong>{{package.currency}} {{fn.formatNumber(package.rates.rates_3, package.currency)}}</strong>
                        </span>
                    </p>
            	</div>
            </div>
        </div>
    	
        <?php //JIKA Sudah DIPILIH ?>
        <div ng-show='add_packages.selected_packages'>
        	<hr />
            <div ng-show="add_packages.selected_packages.selected_routes">
            	<table class="table table-borderless table-condensed">
                    <tr>
                        <td width="130">
                            Adult<br />
                            <small> {{add_packages.selected_packages.currency}} {{fn.formatNumber(add_packages.selected_packages.rates.rates_1, add_packages.selected_packages.currency)}} </small>
                        </td>
                        <td width="130">
                        	Child<br />
                            <small> {{add_packages.selected_packages.currency}} {{fn.formatNumber(add_packages.selected_packages.rates.rates_2, add_packages.selected_packages.currency)}} </small>
                        </td>
                        <td width="130">
                        	Ifant<br />
                            <small> {{add_packages.selected_packages.currency}} {{fn.formatNumber(add_packages.selected_packages.rates.rates_3, add_packages.selected_packages.currency)}} </small>
                        </td>
                        <td align="right"><br /><strong>TOTAL</strong></td>
                    </tr>
                    <tr>
                    	<td>
                        	<input required="required" ng-model='add_packages.qty_1' type="number" min="1" class="form-control input-sm" placeholder="Adult" />
                        </td>
                        <td>
                        	<input required="required" ng-model='add_packages.qty_2' type="number" min="0" class="form-control input-sm" placeholder="Child" />
                        </td>
                        <td>
                        	<input required="required" ng-model='add_packages.qty_3' type="number" min="0" class="form-control input-sm" placeholder="Infant" />
                        </td>
                        <td align="right">
                        	<strong>{{add_packages.selected_packages.currency}} 
                            {{fn.formatNumber(	(add_packages.qty_1*add_packages.selected_packages.rates.rates_1) + 
                            					(add_packages.qty_2*add_packages.selected_packages.rates.rates_2) + 
                                                (add_packages.qty_3*add_packages.selected_packages.rates.rates_3), add_packages.selected_packages.currency)}}</strong>
                        </td>
                    </tr>
                </table>
                <table class="table table-bordered table-condensed">
                	<tr class="active">
                    	<td width="30" align="center">#</td>
                    	<td><strong>Departure</strong></td>
                        <td width="30"></td>
                        <td><strong>Arrival</strong></td>
                        <td width="120"><strong>Date</strong></td>
                        <td width="100"><strong>Time</strong></td>
                    </tr>
                    <tr ng-repeat="trip in add_packages.selected_packages.selected_routes.trip">
                    	<td align="center">{{($index+1)}}</td>
                        <td>
                        	<strong>{{trip.departure_port.port_code}}</strong><br />
                            <small>{{trip.departure_port.name}}</small>
                        </td>
                        <td align="center"><span class="glyphicon glyphicon-chevron-right"></span></td>
                        <td>
                        	<strong>{{trip.arrival_port.port_code}}</strong><br />
                            <small>{{trip.arrival_port.name}}</small>
                        </td>
                        <td><input required="required" type="text" class="form-control input-sm datepicker_pckg" ng-model="trip.selected_date" placeholder="Select Date" /></td>
                        <td>
                        	<select required="required" class="form-control input-sm" ng-model="trip.selected_time">
                            	<option value="" disabled="disabled">Time</option>
                                <option ng-repeat="_time in trip.available_time_list" value="{{_time.value}}">{{_time.text}}</option>
                            </select>
                        </td>
                    </tr>
                </table>
                <br />
            </div>
            <p>
                <strong>Routes Option</strong>
                <ol style="margin:0; padding-left:15px">
                    <li ng-repeat="routes in add_packages.selected_packages.routes_option">
                        <a href="" ng-click="packages_select_this_route(routes);"><h5><strong>{{routes.str_route}}</strong></h5></a>
                    </li>
                </ol>
            </p>
            <div ng-show='add_packages.selected_packages.show_detail'>
                <p>
                    <strong>Additional Item</strong>
                    <ul style="margin:0; padding-left:15px" ng-show='add_packages.selected_packages.inclusion'>
                        <li ng-repeat='str in add_packages.selected_packages.additional_item'>{{str}}</li>
                    </ul>
                </p>
                
                <p>
                    <strong>Inclusions</strong>
                    <ul style="margin:0; padding-left:15px" ng-show='add_packages.selected_packages.inclusion'>
                        <li ng-repeat='str in add_packages.selected_packages.inclusion'>{{str}}</li>
                    </ul>
                </p>
                
                <p>
                    <strong>Exclusions</strong>
                    <ul style="margin:0; padding-left:15px" ng-show='add_packages.selected_packages.exclusion'>
                        <li ng-repeat='str in add_packages.selected_packages.exclusion'>{{str}}</li>
                    </ul>
                </p>
                <p>
                    <strong>Additional info</strong>
                    <ul style="margin:0; padding-left:15px" ng-show='add_packages.selected_packages.additional_info'>
                        <li ng-repeat='str in add_packages.selected_packages.additional_info'>{{str}}</li>
                    </ul>
                </p>
        	</div>
            <hr />
            <div ng-show='add_packages.selected_packages.show_detail'>
            	<a href="" ng-click='add_packages.selected_packages.show_detail = false'>Hide Detail <span class="glyphicon glyphicon-chevron-up"></span></a>
            </div>
            <div ng-hide='add_packages.selected_packages.show_detail'>
            	<a href="" ng-click='add_packages.selected_packages.show_detail = true'>View Detail <span class="glyphicon glyphicon-chevron-down"></span></a>
            </div>
        </div>
    </div>
    <button type="submit" style="display:none">OK</button>
</form>
</div>

<style>
.search-packages-result .packages .packages-item{padding:5px 10px; border:solid 1px #99cbe4; margin-bottom:10px;}
.search-packages-result .packages .packages-item:hover{background:#d9edf7; cursor:pointer}
.search-packages-result .packages .packages-item.selected{background:#d9edf7}
</style>
<script>
	$(document).on("click",".datepicker_pckg",function(){
		$(".datepicker_pckg").datepicker({dateFormat :"yy-mm-dd"});
	});
</script>
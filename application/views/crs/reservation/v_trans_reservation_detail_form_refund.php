<div class="modal fade" id="refund-booking-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <form ng-submit='saveRefund($event)'>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    Refund Booking #{{DATA.current_booking.booking.booking_code}}
                </h4>
              </div>
              <div class="modal-body">
                <div ng-show='DATA.myRefund.error_desc.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myRefund.error_desc'>{{err}}</li></ul></div>
                <table class="table table-borderless table-condensed">
                    <tr>
                        <td>Grand Total</td>
                        <td><strong>{{DATA.current_booking.booking.currency}} {{fn.formatNumber(DATA.current_booking.booking.grand_total,DATA.current_booking.booking.currency)}}</strong></td>
                    </tr>
                    <tr>
                        <td>Total Payment</td>
                        <td><strong>{{DATA.current_booking.booking.currency}} {{fn.formatNumber(DATA.current_booking.booking.total_payment,DATA.current_booking.booking.currency)}}</strong></td>
                    </tr>
                    <tr ng-show='DATA.current_booking.booking.total_refund'>
                        <td>Refunded</td>
                        <td><strong>{{DATA.current_booking.booking.currency}} {{fn.formatNumber(DATA.current_booking.booking.total_refund,DATA.current_booking.booking.currency)}}</strong></td>
                    </tr>
                    <tr>
                        <td>Refund Amount*</td>
                        <td>
                            <div class="input-group">
                                <span class="input-group-addon">{{DATA.current_booking.booking.currency}}</span>
                                <input type="number" required="required" class="form-control input-md" style="width:200px" placeholder="Total Refund" ng-model='DATA.myRefund.refund_amount' />
                            </div>
                            <small>Max. Refund : {{DATA.current_booking.booking.currency}} {{fn.formatNumber((DATA.current_booking.booking.total_payment - DATA.current_booking.booking.total_refund),DATA.current_booking.booking.currency)}}</small>
                        </td>
                    </tr>
                    <tr>
                        <td width="130">Refund Reason*</td>
                        <td>
                            <textarea placeholder="Description" required="required" type="text" class="form-control input-md autoheight" ng-model='DATA.myRefund.description' rows="3"></textarea>
                        </td>
                    </tr>
                </table>
                
              </div>
              <div class="modal-footer" style="text-align:center">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              </div>
            </div>
        </div>
    </form>
</div>
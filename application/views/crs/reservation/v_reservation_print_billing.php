
<div class="no-print text-center" style="text-align:center !important">
	<select class="form-control input-md change-paper-size" ng-model='print_type' style="width:auto; display:inline">
		<option value="">Normal Paper</option>
		<option value="small"> Small Paper</option>
	</select>
	<br /><br />
</div>
<div ng-init="loadDataBookingPaymentPrint()" class="reservation-detail">
	<div class="small-paper" ng-show="print_type == 'small'">
		<div class="header">	
			<div class="title"><?=$vendor["business_name"]?></div>
			<div><?=$vendor["address"]?>, <?=$vendor["regency"]?>, <?=$vendor["province"]?>, <?=$vendor["country"]?></div>
			<div>[P] : <?=$vendor["telephone"]?>, [E] : <?=((@$vendor["contact"]["email"]=='')?$vendor["email"]:$vendor["contact"]["email"])?></div>
			<div><?=$vendor["website"]?></div>
		</div>

		 <div style="font-weight:bold; text-align:center" class="header">
            <strong>
                <span>BILLING STATEMENT #{{DATA.current_booking.booking.booking_code}}</span>
            </strong>
        </div>

		<table width="100%">
			<tr>
				<td width="100%">
					<strong>BOOKING INFORMATION</strong>
					<table width="100%">
						<tr>
							<td width="120">Code</td>
							<td><strong>#{{DATA.current_booking.booking.booking_code}}</strong></td>
						</tr>
						<tr>
							<td>Date</td>
							<td><strong>{{fn.formatDate(DATA.current_booking.booking.transaction_date, "dd MM yy")}}</strong></td>
						</tr>
						<tr>
							<td>Status</td>
							<td class="text-capitalize"><strong>{{DATA.current_booking.booking.status.toLowerCase()}}</strong></td>
						</tr>
                        <tr ng-show="DATA.current_booking.booking.status_code == 'UNDEFINITE'">
							<td>Cut Off Date</td>
							<td><strong>{{fn.formatDate(DATA.current_booking.booking.undefinite_cut_off_date, "dd MM yy")}}</strong></td>
						</tr>
						<tr>
							<td>Source</td>
							<td class="text-capitalize"><strong>{{DATA.current_booking.booking.source.toLowerCase()}}</strong></td>
						</tr>
						<tr ng-show="DATA.current_booking.booking.agent">
							<td>Agent</td>
							<td class="text-capitalize"><strong>{{DATA.current_booking.booking.agent.name}}</strong></td>
						</tr>
					</table>
				</td>
				
			</tr>
		</table>
		<hr />

		<table width="100%">
			<tr>
				<td >
					<strong>CUSTOMER INFORMATION</strong>
					<table width="100%">
						<tr>
							<td width="120">Full Name</td>
							<td><strong>{{DATA.current_booking.booking.customer.first_name}} {{DATA.current_booking.booking.customer.last_name}}</strong></td>
						</tr>
						<tr>
							<td>Email</td>
							<td><strong>{{DATA.current_booking.booking.customer.email}}</strong></td>
						</tr>
                        <tr>
							<td>Telephone</td>
							<td><strong>{{DATA.current_booking.booking.customer.phone}}</strong></td>
						</tr>
						<tr>
							<td>Country</td>
							<td><strong>{{DATA.current_booking.booking.customer.country_name}}</strong></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<hr />
		<table width="100%">
			<tr>
				<td>
					<strong>Remarks / Special Request</strong><br>
					<span ng-show="!DATA.current_booking.booking.remarks">-</span>
					<div ng-show="DATA.current_booking.booking.remarks" class="">
						{{DATA.current_booking.booking.remarks}}
					</div>
				</td>
			</tr>	
		</table>
		<hr />
		<!-- <div class="sub-title"> Billing Statement</div>  -->
			<table ng-repeat='payment in DATA.payment.payment.detail'>
				<tr>
					<td><strong>{{payment.description}}</strong></td>
					<td></td>
				</tr>
				<tr class="info table-header">
					<td width="120">Date</td>
					<td>{{fn.formatDate(payment.date, "dd M yy")}}</td>
				</tr>
				<tr>
					<td width="120">Code</td>
					<td>
						<span ng-show='payment.amount<0'>
							{{payment.payment_code}}
						</span>
						<span ng-show='payment.amount>=0'>{{payment.payment_code}}</span>
					</td>
				</tr>
				<tr>
					<td width="120">Payment Type</td>
					<td>{{payment.payment_type}}</td>
				</tr>
				<tr>
					<td width="120">Amount</td>
					<td>
						<strong ng-show='payment.amount>=0'>{{payment.currency}} {{fn.formatNumber(payment.amount, payment.currency)}}</strong>
						<strong ng-show='payment.amount<0'>{{payment.currency}} {{fn.formatNumber(payment.amount*-1, payment.currency)}}</strong>
					</td>
				</tr>
				<tr>
					<td rowspan="2"><br /></td>
				</tr>
			</table>
			<hr />
			<table>
				<tr class="success table-header" style="font-weight:bold">
					<td width="120">Total Transaction</td>
					<td>{{DATA.payment.payment.currency}} {{fn.formatNumber(DATA.payment.payment.total_transaction, DATA.payment.payment.currency)}}</td>	
				</tr>
				<tr class="success table-header" style="font-weight:bold">
					<td width="120">Total Payment</td>
					<td>{{DATA.payment.payment.currency}} {{fn.formatNumber(DATA.payment.payment.total_payment, DATA.payment.payment.currency)}}</td>
				</tr>
				<tr class="success table-header" style="font-weight:bold">
					<td width="120">Outstanding Invoice</td>
					<td>{{DATA.payment.payment.currency}} {{fn.formatNumber(DATA.payment.payment.balance, DATA.payment.payment.currency)}}</td>	
				</tr>
			</table>
	</div>

	<div class="normal-paper" ng-show="print_type != 'small'">
		<div class="header">
			<div class="pull-right text-right">
				<br>
				<div style="font-size:18px">
                	BILLING STATEMENT
                	
                </div>
				<div class="title">
					#{{DATA.current_booking.booking.booking_code}}
				</div>
			</div>
			
			<div class="title"><?=$vendor["business_name"]?></div>
			<div><?=$vendor["address"]?>, <?=$vendor["regency"]?>, <?=$vendor["province"]?>, <?=$vendor["country"]?></div>
			<div>[P] : <?=$vendor["telephone"]?>, [E] : <?=((@$vendor["contact"]["email"]=='')?$vendor["email"]:$vendor["contact"]["email"])?></div>
			<div><?=$vendor["website"]?></div>
		</div>
		
		<br />
		<table width="100%">
			<tr>
				<td width="50%" valign="top">
					<strong>BOOKING INFORMATION</strong>
					<table width="100%">
						<tr>
							<td width="80">Code</td>
							<td><strong>#{{DATA.current_booking.booking.booking_code}}</strong></td>
						</tr>
						<tr>
							<td>Date</td>
							<td><strong>{{fn.formatDate(DATA.current_booking.booking.transaction_date, "dd MM yy")}}</strong></td>
						</tr>
						<tr>
							<td>Status</td>
							<td class="text-capitalize"><strong>{{DATA.current_booking.booking.status.toLowerCase()}}</strong></td>
						</tr>
                        <tr ng-show="DATA.current_booking.booking.status_code == 'UNDEFINITE'">
							<td>Cut Off Date</td>
							<td><strong>{{fn.formatDate(DATA.current_booking.booking.undefinite_cut_off_date, "dd MM yy")}}</strong></td>
						</tr>
						<tr>
							<td>Source</td>
							<td class="text-capitalize"><strong>{{DATA.current_booking.booking.source.toLowerCase()}}</strong></td>
						</tr>
						<tr ng-show="DATA.current_booking.booking.agent">
							<td>Agent</td>
							<td class="text-capitalize"><strong>{{DATA.current_booking.booking.agent.name}}</strong></td>
						</tr>
					</table>
				</td>
				<td width="50%" valign="top">
					<strong>CUSTOMER INFORMATION</strong>
					<table width="100%">
						<tr>
							<td width="100">Full Name</td>
							<td><strong>{{DATA.current_booking.booking.customer.first_name}} {{DATA.current_booking.booking.customer.last_name}}</strong></td>
						</tr>
						<tr>
							<td>Email</td>
							<td><strong>{{DATA.current_booking.booking.customer.email}}</strong></td>
						</tr>
                        <tr>
							<td>Telephone</td>
							<td><strong>{{DATA.current_booking.booking.customer.phone}}</strong></td>
						</tr>
						<tr>
							<td>Country</td>
							<td><strong>{{DATA.current_booking.booking.customer.country_name}}</strong></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<br />
			<table width="100%">
				<tr>
					<td><strong>Remarks / Special Request</strong></td>
				</tr>
				<tr>
					<td ng-show="DATA.current_booking.booking.remarks">
						<div style="width: 100%;overflow: hidden;text-align: justify;">
							{{DATA.current_booking.booking.remarks}}
						</div>
					</td>					
				
					<td ng-show="!DATA.current_booking.booking.remarks"><strong> - </strong></td>
				</tr>
			</table>
		<br />
		<!-- <div class="sub-title"> Billing Statement</div>  -->
		<div class="table-responsive">
			<table class="table table-bordered table-payment">
				<tr class="info table-header" style="font-weight:bold">
					<td align="center" width="90">Date</td>
					<td align="center" width="120">Code</td>
					<td>Description</td>
					<td width="150">Payment Type</td>
					<td align="right" colspan="2">Amount</td>
				</tr>
				<tr ng-repeat='payment in DATA.payment.payment.detail'>
					<td align="center">{{fn.formatDate(payment.date, "dd M yy")}}</td>
					<td align="center">
						<span ng-show='payment.amount<0'>
							{{payment.payment_code}}
						</span>
						<span ng-show='payment.amount>=0'>{{payment.payment_code}}</span>
					</td>
					<td>{{payment.description}}</td>
					<td>{{payment.payment_type}}</td>
					<td align="center" width="50">{{payment.currency}}</td>
					<td align="right" width="110">
						<strong ng-show='payment.amount>=0'>{{fn.formatNumber(payment.amount, payment.currency)}}</strong>
						<strong ng-show='payment.amount<0'>{{fn.formatNumber(payment.amount*-1, payment.currency)}}</strong>
					</td>
				</tr>
				<tr>
					<td colspan="7"></td>
				</tr>
				<tr class="success table-header" style="font-weight:bold">
					<td colspan="4" align="right">Total Transaction</td>
					<td>{{DATA.payment.payment.currency}}</td>
					<td align="right">{{fn.formatNumber(DATA.payment.payment.total_transaction, DATA.payment.payment.currency)}}</td>
					
				</tr>
				<tr class="success table-header" style="font-weight:bold">
					<td colspan="4" align="right">Total Payment</td>
					<td>{{DATA.payment.payment.currency}}</td>
					<td align="right">{{fn.formatNumber(DATA.payment.payment.total_payment, DATA.payment.payment.currency)}}</td>
					
				</tr>
				<tr class="success table-header" style="font-weight:bold">
					<td colspan="4" align="right">Outstanding Invoice</td>
					<td>{{DATA.payment.payment.currency}}</td>
					<td align="right">{{fn.formatNumber(DATA.payment.payment.balance, DATA.payment.payment.currency)}}</td>
					
				</tr>
				
			</table>

		</div>
	</div>
</div>
<br />
	<div class="no-print row">
		<div class="dropdown col-md-6 text-left" style="margin-top: 10px;">
				<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
		    		<span class="glyphicon glyphicon-envelope"></span>&nbsp;Send Email
				    <span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
				  <li style="cursor: pointer;" ng-show="DATA.current_booking.booking.agent"><a ng-click="send_email_billing(DATA.current_booking.booking.booking_code, '1')">To Agent</a></li>
				  <!-- <li ng-show="!DATA.current_booking.booking.agent" style="cursor: pointer;"><a ng-click="send_email_billing(DATA.current_booking.booking.booking_code, '0')">To Customer</a></li> -->
				  <li ng-show="!DATA.current_booking.booking.agent" style="cursor: pointer;"><a data-toggle="modal" data-target="#payment-detail" class="delete-icon" ng-show="payment.id != ''">To Customer</a></li>
				</ul>
		</div>
		<!-- <div class="col-md-6 text-right" style="margin-top: 10px;">
			<button onclick="print_billing()" type="button" class="btn btn-primary">
				<span class="glyphicon glyphicon-print"></span>
				&nbsp;Print Billing Statement
			</button>
		</div> -->
	</div>
	<div class="modal fade" id="payment-detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	  <form ng-submit="update_email_customer($event, 'billing')">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">
				<span>Customer Email</span> 
			</h4>
		  </div>
		  <div class="modal-body">
		  <div ng-show='error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in error_msg'>{{err}}</li></ul></div>
			<table class="table table-borderless table-condensed" style="margin-bottom: 0 !important;">
				<tr>
					<td>Email</td>
					<td><input placeholder="Email" type="email" class="form-control input-md" ng-model='DATA.current_booking.booking.customer.email' /></td>
				</tr>
			</table>
		  </div>
		  <div class="modal-footer" style="text-align:center">
			<button type="submit"  class="btn btn-primary">
			<small><span class="glyphicon glyphicon-envelope"></span>
			 &nbsp;Send Billing Statement</small>
			</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  </div>
		</div>
		</form>
	  </div>
	</div>
<script type="text/javascript">
	function print_billing(){
		window.print();
	}
</script>
<script>
	$(".change-paper-size").change(function(){
		var _this = $(this);
		
		if (_this.val() == 'small'){
			$(".print_area").addClass("small_paper");
		}else{
			$(".print_area").removeClass("small_paper");
		}
	});

	
</script>

<style>


	.print_area{background:none !important; padding:0 !important;}
	.small_paper{width:8cm !important; margin:0 auto !important}
	.normal-paper{background:#FFF; padding:10px}
	.small-paper{width:8cm; margin:auto; padding:10px; background:#FFF}
	.small-paper .header .title{font-size:18px !important}
	

</style>
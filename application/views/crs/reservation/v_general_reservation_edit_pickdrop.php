<div class="modal" id="modal-add-picdrp" tabindex="-1" role="dialog" aria-labelledby="ModalEditPicdrpLabel">
	<form ng-submit='submit_pickup_dropoff($event)'>
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="ModalEditPicdrpLabel">EDIT {{DATA.myPickupDropoff.type}} SERVICE</h4>

				<div ng-show='DATA.myPickupDropoff.error_desc.length > 0' class="alert alert-danger"><ul><li ng-repeat='error in DATA.myPickupDropoff.error_desc'>{{error}}</li></ul></div>
			</div> 
            <div class="modal-body products">
                <div>
                    <span style="margin-right: 20px;margin-bottom: 15px;">
                    	<label ng-show="DATA.myPickupDropoff.type == 'PICKUP'">Pickup Service</label>
                        <label ng-show="DATA.myPickupDropoff.type == 'DROPOFF'">Dropoff Service</label>
                    </span>
                    <label>
                    	<input style="margin-left:3px;margin-bottom:15px" type="radio" class="chk-prior-cancellation" ng-model="DATA.myPickupDropoff.transport_service" value="yes" />
                    	Yes 
					</label>
					&nbsp;&nbsp;
                    <label>
                    	<input style="margin-left:3px;margin-bottom:15px" type="radio" class="chk-prior-cancellation" ng-model="DATA.myPickupDropoff.transport_service" value="no" />
                    	No
                    </label>
                </div>
                <div ng-show="DATA.myPickupDropoff.transport_service == 'yes'">
                    <table class="table table-borderless table-condensed">
                        <tr >
                            <td width="100">Area</td>
                            <td>
                                <select ng-options="item.area +' - '+item.time + '  (' + item.currency + ' ' + fn.formatNumber(item.price, item.currency) + ' / ' + item.type +') '  for item in DATA.myPickupDropoff.rates_area" 
                                    ng-model="DATA.myPickupDropoff.selected_area"
                                    class="form-control input-md" ng-change='editPrickUpDropoffCalculatePrice()'>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Hotel*</td>
                            <td><input type="text" placeholder="Hotel name" class="form-control input-md" ng-model='DATA.myPickupDropoff.hotel_name' /></td>
                        </tr>
                        <tr>
                            <td>Address*</td>
                            <td><input type="text" placeholder="Address" class="form-control input-md" ng-model='DATA.myPickupDropoff.hotel_address' /></td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td><input type="text" placeholder="Phone/Mobile" class="form-control input-md" ng-model='DATA.myPickupDropoff.hotel_phone_number' /></td>
                        </tr>
                        <tr>
                            <td>Price</td>
                            <td>
                                <div class="input-group" style="width:200px">
                                    <span class="input-group-addon" id="basic-addon1">{{DATA.current_booking.booking.currency}}</span>
                                    <input type="number" class="form-control input-md" ng-model='DATA.myPickupDropoff.price' />
                                </div>
                            </td>
                        </tr>
                    </table>
                   
                </div>  
                <div class="modal-footer" style="text-align:center">
                    <button type="submit" class="btn btn-primary btn-submit-edit-picdrp" >Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
			</div>	
		</div>
	</div>
    </form>
</div>
<div ng-init='printReceipt()'>
	<div class="normal-paper">
		<div class="page" ng-repeat='passenger in DATA.voucher.passenger' style="page-break-after:always;">
			<div>
						<div class="header">
							<div class="title">Boarding Pass</div>
							<div style="font-size: 10px"><?=$vendor["business_name"]?></div>
						</div>
						
						<div>
							<div>
								<img src="<?=base_url()?>public/plugin/barcode.php?text={{passenger.pass_code}}&size=50" />
							</div>
							
							<div ng-show="DATA.current_booking.booking.status_code=='CANCEL'" style='color:red'>[CANCELLED]</div>
						</div>
						<hr />
						<div>
							<strong>CUSTOMER INFORMATION</strong><br />
							<table>
								<tr>
									<td width="70">Name</td>
									<td><strong>{{passenger.first_name}} {{passenger.last_name}} 
										<span ng-show="passenger.gender == 'm'">
											(Male)
										</span>
										<span ng-show="passenger.gender == 'f'">
											(Female)
										</span>
									</strong></td>
								</tr>
								<tr>
									<td>Passcode</td>
									<td><strong>{{passenger.pass_code}}</strong></td>
								</tr>
							</table>
						</div>
						<hr />
						<div>
							<strong>VOUCHER INFORMATION</strong><br />
							<table>
								<tr>
									<td width="70">Date</td>
									<td><strong>{{fn.formatDate(DATA.voucher.date, "dd MM yy")}}</strong></td>
								</tr>
								<tr ng-show="DATA.voucher.product_type == 'TRANS'"> 
									<td>Boat Name</td> 
									<td> 
										<strong>{{DATA.voucher.schedule.boat.name}}</strong> 
									</td> 
								</tr> 
								<tr ng-show="DATA.voucher.product_type == 'TRANS'">
									<td>Trip</td>
									<td></td>
								</tr>
								<tr ng-show="DATA.voucher.product_type == 'TRANS'">
									<td style="font-size:14px" colspan="2">
										<strong>{{DATA.voucher.departure.port.name}} ({{DATA.voucher.departure.port.port_code}}) - {{DATA.voucher.departure.time}}</strong>
										<i class="fa fa-chevron-right"></i> <br>
										<strong>{{DATA.voucher.arrival.port.name}} ({{DATA.voucher.arrival.port.port_code}}) - {{DATA.voucher.arrival.time}}</strong>
									</td>
								</tr>
					          
							</table>
						</div>
				</div>
				<hr />
				<div>
					<table>
						<tr>
							<td class="td-sub">
								<div>
									<div style="font-size: 12px;"><b>Boarding Pass</b></div>
									<div style="font-size: 10px"><?=$vendor["business_name"]?></div>
								</div>
								<div style="font-size: 12px;">
									<b>{{passenger.pass_code}}</b>
								</div>
								
							</td>
							<td class="td-sub">
								<div class="sub-code">
									<table>
										<tr>
											<td width="80">Name <br><strong>{{passenger.first_name}} {{passenger.last_name}}<span ng-show="passenger.gender == 'm'">(Male)</span>
												<span ng-show="passenger.gender == 'f'">(Female)</span></strong></td>
										</tr>
										
									</table>
								</div>
							</td>
						</tr>
						<tr>
							<td class="td-sub">
								<div class="sub-code">
									<table>
										<tr>
											
											<td>
												Boat Name <br>
												<span style="font-size: 10px;"><strong>{{DATA.voucher.schedule.boat.name}}</strong> </span>
											</td>
										</tr>
									</table>

								</div>
							</td>
							<td class="td-sub">
								<div class="sub-code">
									<table>
									<tr>
										<td>
											Date <br>
											<strong>{{fn.formatDate(DATA.voucher.date, "dd MM yy")}}</strong>
										</td>
									</tr>
									
									</table>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="3" class="td-sub">
								<strong>{{DATA.voucher.departure.port.name}} ({{DATA.voucher.departure.port.port_code}}) - {{DATA.voucher.departure.time}}</strong>
								<i class="fa fa-chevron-right"></i><br>
								<strong>{{DATA.voucher.arrival.port.name}} ({{DATA.voucher.arrival.port.port_code}}) - {{DATA.voucher.arrival.time}}</strong>
							</td>
						</tr>
					</table>
				</div>
		</div>
	</div>
</div>
<style type="text/css">
	
	.td-sub {
		padding: 5px !important;
	}
	.sub-code {
		font-size: 10px;
	}
	.flip {

		  -ms-transform: rotate(90deg); /* IE 9 */
		  -webkit-transform: rotate(90deg); /* Safari 3-8 */
		  transform: rotate(90deg);
	}
	.page {
		height: 550px;
		width:8cm !important; margin:0 auto !important
	}

	@media print {
        html, body {
            width: 80mm;
        	height: 200mm;
        	padding: 0 !important;      
        }
        .page {
        	width: 80mm;
        	height: 550px;
            margin: 0 !important; padding: 0 !important;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
</style>
<?php
//pre($vendor)
?>
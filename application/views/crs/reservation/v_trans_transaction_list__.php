<h1>Transaction</h1>

<div ui-view>
	<div ng-init="loadDataTransactionTransport()">
		<div class="products">
			<div class="product">
				Filter : <input type="text" ng-model="filter_booking" class="form-control input-sm" placeholder="Search" style="width:200px; display:inline" />
			</div>
		</div>
		
		<div ng-show='!DATA.bookings.bookings'>
			<img src="<?=base_url("public/images/loading_bar.gif")?>" />
		</div>
		
		<div ng-show='DATA.bookings.bookings'>
			<table class="table table-condensed table-bordered">
				<tr class="header bold">
					<td align="center" width="130">Order#</td>
					<td>Customer Name</td>
					<td align="center" width="100">Source</td>
					<?php /*?><td align="center" width="100">Status</td><?php */?>
					<td align="right" width="100">Total Amount</td>
					<td align="right" width="100">Total Payment</td>
					<td align="right" width="100">Total Balance</td>
				</tr>
				<tr ng-repeat='booking in DATA.bookings.bookings | filter:filter_booking' ng-class="{'danger':(booking.status_code=='CANCEL'), 'warning':(booking.status_code=='UNDEFINITE')}">
					<td align="center">
						<a ui-sref="trans_reservation.detail({'booking_code':booking.booking_code})" ng-show="!booking.booking_type"><strong>{{booking.booking_code}}</strong></a>
						<a ui-sref="open_voucher.booking_detail({'booking_code':booking.booking_code})" ng-show="booking.booking_type=='OPENVOUCHER'"><strong>{{booking.booking_code}}</strong></a>
						<br />
						{{fn.formatDate(booking.transaction_date, "dd M yy")}}
					</td>
					<td>
						<strong>{{booking.customer.first_name}} {{booking.customer.last_name}}</strong>
						<br />
						{{booking.customer.email}}
					</td>
					<td align="center" class="text-capitalize">{{booking.source.toLowerCase()}}</td>
					<?php /*?><td align="center" class="text-capitalize">{{booking.status.toLowerCase()}}</td><?php */?>
					<td align="right">{{booking.currency}} {{fn.formatNumber(booking.grand_total, booking.currency)}}</td>
					<td align="right">
						<span ng-show='booking.total_payment > 0'>
							{{booking.currency}} {{fn.formatNumber(booking.total_payment, booking.currency)}}
						</span>
						<span ng-show='!booking.total_payment > 0'>-</span>
					</td>
					<td align="right">
						<span ng-show='booking.balance > 0'>
							{{booking.currency}} {{fn.formatNumber(booking.balance, booking.currency)}}
						</span>
						<span ng-show='!booking.balance > 0'>-</span>
					</td>
				</tr>
			</table>
		</div>
		<br />
		<?php /*?><div class="add-product-button"> <a ui-sref="trans_reservation.new" class="btn btn-success btn-lg btn-block"> <span class="glyphicon glyphicon-plus"></span> New Reservation </a> </div><?php */?>
	</div>
</div>

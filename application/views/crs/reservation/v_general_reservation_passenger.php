<div class="sub-title"> Participant Informationsssss</div>
<br />
<div ng-init="loadDataBookingPassenger();" class="reservation-detail">
    <div class="table-responsive">
        <div ng-repeat="detail in DATA.current_booking.booking.detail | orderBy : '-booking_detail_status_code'">
            <table class="table table-bordered table-condensed" style="margin-bottom:10px" >
              <!-- <?php if($vendor['setting']['checkin_use_pass_code'] == '1'){ ?>   -->
                    <tr height="70">
                        <td colspan="3">
                             Auto Generate Passcode
                            <a href="" ng-click="autoGeneratePassocde(0)" >
                            <div 
                                style="color:#090;margin-top: 10px;"><img src="<?=base_url("public/images/on-button.png")?>"></span> </div>
                            </a> 
                            <a href="" ng-click="autoGeneratePassocde(1)" >
                                <div 
                                style="color:#F30;margin-top: 10px;"><img src="<?=base_url("public/images/off-button.png")?>"></span></div>
                            </a> 
                        </td>
                        
                        <td colspan="7">
                            <div style="margin-left: 20px;font-size: 18px;" class="pull-right" >
                                 <a href="" title="Add Code" data-toggle="modal" ng-click="addBarcode(detail)" data-target="#add_code"><span class="fa fa-qrcode" ></span>&nbsp;&nbsp;Add Passcode</a>
                            </div>
                            <div >
                                 <span class="pull-right"  style="margin-left: 20px;font-size: 18px;" ><a href="" title="Generate Passcode" ng-click="generateBarcode()"><span class="fa fa-qrcode" ></span>&nbsp;&nbsp;Generate Passcode</a></span>
                                 <br>
                                 <br>
                                 <span class="pull-right" >
                                    <a href="<?=site_url("home/print_page/#/print/passcode/")?>{{DATA.current_booking.booking.booking_code}}/{{detail.voucher_code}}" target="_blank">
                                    <span class="fa fa-print" ></span>&nbsp;Print Passcode</a>
                                </span>
                            </div>
                        </td>
                    </tr>
              
              <!--  <?php } ?>   --> 
                <tr ng-class="{'danger':(detail.booking_detail_status_code == 'CANCEL' || DATA.current_booking.booking.status_code == 'CANCEL')}" class="info">
                    <td colspan="9">
                        <div class="pull-right" ng-show="($index > 0)">
                            <a href="" ng-click="copyPassenger(($index-1), $index)">
                                <span class="glyphicon glyphicon-duplicate"></span> Same as above
                            </a>
                        </div>
                    
                        Voucher# : 
                        <strong>{{detail.voucher_code}}</strong> - 
                        <strong>{{fn.newDate(detail.date) | date : 'dd MMM yyyy'}}</strong> -
                        
                        <span ng-show="detail.product_type == 'ACT'">
                            <strong>{{detail.product.name}}</strong>
                        </span>
                        <span ng-show="detail.product_type == 'TRANS'">
                            <strong>{{detail.departure.port.name}} ({{detail.departure.port.port_code}}) : {{detail.departure.time}}</strong>
                            &nbsp;&nbsp;<i class="fa fa-chevron-right"></i>&nbsp;&nbsp;
                            <strong>{{detail.arrival.port.name}} ({{detail.arrival.port.port_code}}) : {{detail.arrival.time}}</strong>
                        </span>
                        
                        <span ng-show="detail.booking_detail_status_code == 'CANCEL'"> - <span class="label label-danger">CANCEL</span></span>
                    </td>
                </tr>
                <tr class="header bold">
                    <td width="30">#</td>
                    <td colspan="2">Full Name</td>
                    <td width="150">Email</td>
                    <td width="130">Phone</td>
                    <td width="130">ID Number</td>
                    <td width="130">Nationality</td>
                    <td>
                        Gender
                    </td>
                    <td width="40" align="center">
                        <a href="" title="Edit Passenger" ng-click="bulkEditPassenger(detail)">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                    </td>
                    
                </tr>
                <tr ng-repeat='passenger in detail.passenger' ng-class="{'warning':passenger.is_data_updated != '1'}">
                    <td>{{($index+1)}}</td>
                    <td width="45">{{passenger.type.desc}}</td>
                    <td width="150">
                        <a href="" title="Edit Passenger" data-toggle="modal" ng-click="addEditPassenger(passenger, detail)" data-target="#add-edit-passenger">
                            {{passenger.first_name}} {{passenger.last_name}}
                        </a>
                        <br>
                        <span style="font-size: 8px;"><b>{{passenger.pass_code}}</b></span>
                        <br>
                        <span style="cursor: pointer;" class="pull-right"><a ng-click="printBoardingPass(passenger.passenger_code)"><span class="fa fa-print" ></span></a></span>
                       
                        <iframe ng-src="{{trustAsUrl('<?=site_url("home/print_page/#/print/boarding/")?>',passenger.passenger_code, DATA.current_booking.booking.booking_code)}}" id='{{passenger.passenger_code}}' name="{{passenger.passenger_code}}" style="width:0;height:0;border:0; border:none;"></iframe>
                    </td>
                    <td>{{passenger.email}}</td>
                    <td>{{passenger.phone}}</td>
                    <td>{{passenger.passport_number}}</td>
                    <td>{{passenger.country_name}}</td>
                    <td align="center">{{passenger.gender}}</td>
                    <td align="center"><a href="" title="Edit Passenger" data-toggle="modal" ng-click="addEditPassenger(passenger, detail)" data-target="#add-edit-passenger"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    
                </tr>
            </table>
            
            <form ng-submit='saveBulkEditPassenger(detail, $event)' ng-show='detail.passenger_edit'>
                <table class="table table-bordered table-condensed" style="margin-bottom:10px" ng-show='detail.passenger_edit'>
                    <tr ng-class="{'danger':(detail.booking_detail_status_code == 'CANCEL' || DATA.current_booking.booking.status_code == 'CANCEL')}" class="info">
                        <td colspan="8">
                            Voucher# : 
                            <strong>{{detail.voucher_code}}</strong> - 
                            <strong>{{fn.newDate(detail.date) | date : 'dd MMM yyyy'}}</strong> -
                            
                            <span ng-show="detail.product_type == 'ACT'">
                                <strong>{{detail.product.name}}</strong>
                            </span>
                            <span ng-show="detail.product_type == 'TRANS'">
                                <strong>{{detail.departure.port.name}} ({{detail.departure.port.port_code}}) : {{detail.departure.time}}</strong>
                                &nbsp;&nbsp;<i class="fa fa-chevron-right"></i>&nbsp;&nbsp;
                                <strong>{{detail.arrival.port.name}} ({{detail.arrival.port.port_code}}) : {{detail.arrival.time}}</strong>
                            </span>
                        </td>
                    </tr>
                    <tr class="header bold">
                        <td width="30">#</td>
                        <td colspan="2">Full Name</td>
                        <td width="130">Email</td>
                        <td width="130">Phone</td>
                        <td width="130">ID Number</td>
                        <td width="130">Nationality</td>
                        <td >Gender</td>
                    </tr>
                    <tr ng-repeat='mypassenger in detail.passenger_edit'>
                        <td>{{($index+1)}}</td>
                        <td width="45">{{detail.passenger_edit[$index].type.desc}}</td>
                        <td>
                            <input type="text" required="required" placeholder="First Name" class="form-control input-sm" ng-model='detail.passenger_edit[$index].first_name' style="display:inline; width:47%" />
                            &nbsp;
                            <input type="text" required="required" placeholder="Last Name" class="form-control input-sm" ng-model='detail.passenger_edit[$index].last_name' style="display:inline; width:47%" />
                        </td>
                        <td>
                            <input type="text" placeholder="Email" class="form-control input-sm" ng-model='detail.passenger_edit[$index].email' />
                        </td>
                        <td>
                            <input type="text" placeholder="Phone / Mobile" class="form-control input-sm" ng-model='detail.passenger_edit[$index].phone' />
                        </td>
                        <td>
                            <input type="text" placeholder="ID Number" class="form-control input-sm" ng-model='detail.passenger_edit[$index].passport_number' />
                        </td>
                        <td>
                            <select class="form-control input-sm" ng-model='detail.passenger_edit[$index].country_code'>
                                <option value="">-- Select Country --</option>
                                <option value="{{country.code}}" ng-repeat='country in DATA.country_list.country_list'>{{country.name}}</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7" align="center">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-default" ng-click='cancelBulkEditPassenger(detail)'>Cancel</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
     <div class="modal fade" id="add_code" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">
                Add Passcode
            </h4>
          </div>
          <div class="modal-body">
            <div ng-show='DATA.myPassenger.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myPassenger.error_msg'>{{err}}</li></ul></div>
            <div class="products">
                <div class="product form-inline text-center" style="font-size:18px">
                    <form ng-submit='getVoucherParticipant()'>
                        Passcode : <input id="check_voucher_code" type="text" required="required" placeholder="Passcode" class="form-control input-lg" style="width:250px" ng-model='passcode'  />
                        <button type="submit" class="btn btn-lg btn-success">OK</button>
                    </form>
                </div>
            </div>
            <br />
            <div ng-show="showLoading">
                 <img style="width: 100px; height: auto;" src="<?=base_url("public/images/loading_bar.gif")?>" />
            </div>
            <div style="color: green;margin-bottom: 20px;font-size: 20px;">
                <strong>Please scan your passcode as boarding pass</strong>
            </div>
            <div>
                <table class="table table-bordered table-condensed" style="margin-bottom:10px" >
                     <tr>
                        <td colspan="7" align="right" width="100">
                            Auto Checkin
                            <a href="" ng-click="publishUnpublishAccount(0)" ng-show="auto_login">
                        
                                <div 
                                style="color:#090;"><img src="<?=base_url("public/images/on-button.png")?>"></span> </div>
                            </a> 
                            <a href="" ng-click="publishUnpublishAccount(1)" ng-show="!auto_login">
                                <div 
                                style="color:#F30"><img src="<?=base_url("public/images/off-button.png")?>"></span></div>
                            </a> 

                        </td>
                    </tr>
                    <tr class="info">
                        <td colspan="7">
                            Voucher# : 
                            <strong>{{barcode_edit.voucher_code}}</strong> - 
                            <strong>{{fn.newDate(barcode_edit.date) | date : 'dd MMM yyyy'}}</strong> -
                            <?php if ($vendor['category'] != 'activities') { ?>
                               <strong>{{barcode_edit.departure.port.name}} ({{barcode_edit.departure.port.port_code}}) : {{barcode_edit.departure.time}}</strong>
                                &nbsp;&nbsp;<i class="fa fa-chevron-right"></i>&nbsp;&nbsp;
                                <strong>{{barcode_edit.arrival.port.name}} ({{barcode_edit.arrival.port.port_code}}) : {{barcode_edit.arrival.time}}</strong>
                            <?php }else{ ?>
                                <strong>{{barcode_edit.product.name}}</strong>
                            <?php } ?>
                            
                        </td>
                    </tr>
                    <tr class="header bold">
                        <td ng-class="{'success':barcode_edit.adult == barcode_edit.qty_1,'selected':barcode_edit.adult < barcode_edit.qty_1}" style="font-size:20px;" align="center">Adult <span ng-show="barcode_edit.adult == barcode_edit.qty_1"><i id="icon" class="fa fa-check" style="color: green;" aria-hidden="true"></i></span></td>
                        <td ng-show="barcode_edit.qty_2 != 0" ng-class="{'success':barcode_edit.child == barcode_edit.qty_2}" style="font-size:20px;" align="center">Child <span ng-show="barcode_edit.child == barcode_edit.qty_2"><i id="icon" class="fa fa-check" style="color: green;" aria-hidden="true"></i></span></td>
                        <td ng-show="barcode_edit.qty_3 != 0" ng-class="{'success':barcode_edit.infant == barcode_edit.qty_3}" style="font-size:20px;" align="center">Infant <span ng-show="barcode_edit.infant == barcode_edit.qty_3"><i id="icon" class="fa fa-check" style="color: green;" aria-hidden="true"></i></span></td>
                        
                    </tr>
                    <tr class="header bold">
                        <td ng-class="{'success':barcode_edit.adult == barcode_edit.qty_1}" style="font-size:50px;" align="center">{{barcode_edit.adult}}/{{barcode_edit.qty_1}}</td>
                        <td ng-show="barcode_edit.qty_2 != 0" ng-class="{'success':barcode_edit.child == barcode_edit.qty_2}" style="font-size:50px;" align="center">{{barcode_edit.child}}/{{barcode_edit.qty_2}}</td>
                        <td ng-show="barcode_edit.qty_3 != 0" ng-class="{'success':barcode_edit.infant == barcode_edit.qty_3}" style="font-size:50px;" align="center">{{barcode_edit.infant}}/{{barcode_edit.qty_3}}</td>
                    </tr>
                </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="add-edit-passenger" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <form ng-submit='saveDataPassenger($event)'>
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">
                Edit Passenger
            </h4>
          </div>
          <div class="modal-body">
            <div ng-show='DATA.myPassenger.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.myPassenger.error_msg'>{{err}}</li></ul></div>
            <table class="table table-borderless table-condensed">
                <tr>
                    <td width="120">Name*</td>
                    <td>
                    
                        <input type="text" required="required" placeholder="First Name" class="form-control input-md" ng-model='DATA.myPassenger.first_name' style="display:inline; width:48%" />
                        &nbsp;&nbsp;&nbsp;
                        <input type="text" required="required" placeholder="Last Name" class="form-control input-md" ng-model='DATA.myPassenger.last_name' style="display:inline; width:48%" />
                    </td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><input type="text" placeholder="Email" class="form-control input-md" ng-model='DATA.myPassenger.email' /></td>
                </tr>
                <tr>
                    <td>Phone / Mobile</td>
                    <td><input type="text" placeholder="Phone / Mobile" class="form-control input-md" ng-model='DATA.myPassenger.phone' /></td>
                </tr>
                <tr>
                    <td>ID Number</td>
                    <td><input type="text" placeholder="ID Number" class="form-control input-md" ng-model='DATA.myPassenger.passport_number' /></td>
                </tr>
                <tr>
                    <td>Country</td>
                    <td>
                        <select class="form-control input-md" ng-model='DATA.myPassenger.country_code'>
                            <option value="">-- Select Country --</option>
                            <option value="{{country.code}}" ng-repeat='country in DATA.country_list.country_list'>{{country.name}}</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Gender</td>
                    <td>
                         <input ng-model='DATA.myPassenger.gender' type="radio" name="gender" value="m"> Male<br>
                         <input ng-model='DATA.myPassenger.gender' type="radio" name="gender" value="f"> Female<br>
                    </td>
                </tr>
            </table>
          </div>
          <div class="modal-footer" style="text-align:center">
            <button type="submit" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
      </form>
    </div>
</div>
<script type="text/javascript">
    function printGas(code){
        console.log(code);
    }
</script>
<script>activate_sub_menu_agent_detail("passenger");</script>
<div class="modal" id="modal-add-picdrp" tabindex="-1" role="dialog" aria-labelledby="ModalEditPicdrpLabel">
	<form ng-submit='submit_pickup_dropoff($event)'>
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="ModalEditPicdrpLabel">EDIT {{DATA.myPickupDropoff.type}} SERVICE</h4>

				<div ng-show='DATA.myPickupDropoff.error_desc.length > 0' class="alert alert-danger"><ul><li ng-repeat='error in DATA.myPickupDropoff.error_desc track by $index'>{{error}}</li></ul></div>
			</div> 
            <div class="modal-body products">
                <div ng-show="DATA.myPickupDropoff.type == 'PICKUP'">
                    <span style="margin-right: 20px;margin-bottom: 15px;"><label>Pickup Service</label></span>
                        <input style="margin-left:3px;margin-bottom:15px" type="radio" class="chk-prior-cancellation" ng-model="check_edit" ng-change="change_check('yes','PICKUP')" value="yes"/>
                        Yes 
                        &nbsp;&nbsp;
                        <input style="margin-left:3px;margin-bottom:15px" type="radio" class="chk-prior-cancellation" ng-model="check_edit" value="no" ng-change="change_check('no','PICKUP')" />
                        No
                </div>
                <div ng-show="DATA.myPickupDropoff.type == 'DROPOFF'">
                    <span style="margin-right: 20px;margin-bottom: 15px;"><label>Dropoff Service</label></span>
                   
                        <input style="margin-left:3px;margin-bottom:15px" type="radio" class="chk-prior-cancellation" ng-model="check_edit_dropoff" ng-change="change_check('yes','DROPOFF')" value="yes"/>
                        Yes 
                        &nbsp;&nbsp;
                        <input style="margin-left:3px;margin-bottom:15px" type="radio" class="chk-prior-cancellation" ng-model="check_edit_dropoff" value="no" ng-change="change_check('no','DROPOFF')" />
                        No
                    
                </div>
                <div ng-show="check_edit_dropoff == 'yes' && DATA.myPickupDropoff.type == 'DROPOFF'">
                    <table class="table table-borderless table-condensed">
                        <tr >
                            <td width="100">Area</td>
                            <td>
                                <select ng-required="check_edit_dropoff == 'yes' && DATA.myPickupDropoff.type == 'DROPOFF'" ng-options="item.area +' - '+item.time + '  (' + item.currency + ' ' + fn.formatNumber(item.price, item.currency) + ' / ' + item.type +') ' for item in DATA.myPickupDropoff.rates_area" 
                                    ng-model="DATA.myPickupDropoff.selected_area"
                                    class="form-control input-md" ng-change='editPrickUpDropoffCalculatePrice()'>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Hotel*</td>
                            <td><input ng-required="check_edit_dropoff == 'yes' && DATA.myPickupDropoff.type == 'DROPOFF'" type="text" placeholder="Hotel name" class="form-control input-md" ng-model='DATA.myPickupDropoff.hotel_name' /></td>
                        </tr>
                        <tr>
                            <td>Address*</td>
                            <td><input ng-required="check_edit_dropoff == 'yes' && DATA.myPickupDropoff.type == 'DROPOFF'" type="text" placeholder="Address" class="form-control input-md" ng-model='DATA.myPickupDropoff.hotel_address' /></td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td><input ng-required="check_edit_dropoff == 'yes' && DATA.myPickupDropoff.type == 'DROPOFF'" type="text" placeholder="Phone/Mobile" class="form-control input-md" ng-model='DATA.myPickupDropoff.hotel_phone_number' /></td>
                        </tr>
                        <tr>
                            <td>Price</td>
                            <td>
                                <div class="input-group" style="width:200px">
                                    <span class="input-group-addon" id="basic-addon1">{{DATA.current_booking.booking.currency}}</span>
                                    <input ng-required="check_edit_dropoff == 'yes' && DATA.myPickupDropoff.type == 'DROPOFF'" required="required" type="number" placeholder="Price" class="form-control input-md" ng-model='DATA.myPickupDropoff.price_dropoff' />
                                </div>
                            </td>
                        </tr>
                    </table>
                   
                </div>  
                <div ng-show="check_edit == 'yes' && DATA.myPickupDropoff.type == 'PICKUP'">
                    <table class="table table-borderless table-condensed">
                        <tr>
                            <td width="100">Area</td>
                            <td>
                                <select ng-required="check_edit == 'yes' && DATA.myPickupDropoff.type == 'PICKUP'" ng-options="item.area +' - '+item.time + '  (' + item.currency + ' ' + fn.formatNumber(item.price, item.currency) + ' / ' + item.type +') ' for item in DATA.myPickupDropoff.rates_area" 
                                    ng-model="DATA.myPickupDropoff.selected_area"
                                    class="form-control input-md" ng-change='editPrickUpDropoffCalculatePrice()'>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Hotel*</td>
                            <td><input ng-required="check_edit == 'yes' && DATA.myPickupDropoff.type == 'PICKUP'" type="text" placeholder="Hotel name" class="form-control input-md" ng-model='DATA.myPickupDropoff.hotel_name' /></td>
                        </tr>
                        <tr>
                            <td>Address*</td>
                            <td><input ng-required="check_edit == 'yes' && DATA.myPickupDropoff.type == 'PICKUP'" type="text" placeholder="Address" class="form-control input-md" ng-model='DATA.myPickupDropoff.hotel_address' /></td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td><input ng-required="check_edit == 'yes' && DATA.myPickupDropoff.type == 'PICKUP'" type="text" placeholder="Phone/Mobile" class="form-control input-md" ng-model='DATA.myPickupDropoff.hotel_phone_number' /></td>
                        </tr>
                        <tr>
                            <td>Price</td>
                            <td>
                                <div class="input-group" style="width:200px">
                                    <span class="input-group-addon" id="basic-addon1">{{DATA.current_booking.booking.currency}}</span>
                                    <input ng-required="check_edit == 'yes' && DATA.myPickupDropoff.type == 'PICKUP'" type="number" required="required" placeholder="Price" class="form-control input-md" ng-model='DATA.myPickupDropoff.price_pickup' />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer" style="text-align:center">
                    <button type="submit" class="btn btn-primary btn-submit-edit-picdrp" >Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
			</div>	
		</div>
	</div>
    </form>
</div>
<div ng-init='checkInVoucherTrans(); <?php /*?>check_voucher_code="TDQ170678535"; getVoucherInformation()<?php */?> '>
	<h1>Check In</h1>
	
	<div class="products">
		<div class="product form-inline text-center" style="font-size:18px">
			<form ng-submit='getVoucherInformation()'>
				Voucher Code : <input id="check_voucher_code" type="text" required="required" placeholder="Voucher Code" class="form-control input-lg" style="width:250px" ng-model='check_voucher_code' />
				<button type="submit" class="btn btn-lg btn-primary">OK</button>
				<button type="button" class="btn btn-lg btn-default" onclick='$("#check_voucher_code").val("").focus();' ng-click="DATA.voucher = {}">Clear</button>
			</form>
		</div>
	</div>
	<br />
	
	<div ng-show='!DATA.voucher.voucher.voucher && show_getVoucherInformation' align="center">
		<img src="<?=base_url("public/images/loading_bar.gif")?>" />
	</div>
	
	<div ng-show='DATA.voucher.voucher.voucher'>
		<div class="sub-title"><strong>CUSTOMER INFORMATION</strong></div>
	
		<table width="100%" class="table">
			<tr>
				<td width="100">Full Name</td>
				<td><strong>{{DATA.voucher.voucher.booking.customer.first_name}} {{DATA.voucher.voucher.booking.customer.last_name}}</strong></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><strong>{{DATA.voucher.voucher.booking.customer.email}}</strong></td>
			</tr>
			<tr>
				<td>Country</td>
				<td><strong>{{DATA.voucher.voucher.booking.customer.country_name}}</strong></td>
			</tr>
			<tr>
				<td>Telephone</td>
				<td><strong>{{DATA.voucher.voucher.booking.customer.phone}}</strong></td>
			</tr>
		</table>
		
		<br />
		<div class="sub-title"><strong>VOUCHER INFORMATION</strong></div>
		<table width="100%" class="table">
			<tr>
				<td width="100">Booking Code</td>
				<td><strong>#{{DATA.voucher.voucher.booking.booking_code}}</strong></td>
			</tr>
			<tr>
				<td>Voucher Code</td>
				<td><strong>{{DATA.voucher.voucher.voucher.voucher_code}}</strong></td>
			</tr>
			<tr>
				<td>Date</td>
				<td><strong>{{fn.formatDate(DATA.voucher.voucher.voucher.date, "dd MM yy")}}</strong></td>
			</tr>
			<tr>
				<td>Trip</td>
				<td style="font-size:16px">
					<strong>{{DATA.voucher.voucher.voucher.departure.port.name}} ({{DATA.voucher.voucher.voucher.departure.port.port_code}}) - {{DATA.voucher.voucher.voucher.departure.time}}</strong>
					&nbsp;&nbsp;<i class="fa fa-chevron-right"></i>&nbsp;&nbsp;
					<strong>{{DATA.voucher.voucher.voucher.arrival.port.name}} ({{DATA.voucher.voucher.voucher.arrival.port.port_code}}) - {{DATA.voucher.voucher.voucher.arrival.time}}</strong>
				</td>
			</tr>
			<tr>
				<td>Participants</td>
				<td>
					<strong>
						<span ng-show='DATA.voucher.voucher.voucher.qty_1 > 0'>{{DATA.voucher.voucher.voucher.qty_1}} Adult</span>
						<span ng-show='DATA.voucher.voucher.voucher.qty_2 > 0'>{{DATA.voucher.voucher.voucher.qty_2}} Child</span>
						<span ng-show='DATA.voucher.voucher.voucher.qty_3 > 0'>{{DATA.voucher.voucher.voucher.qty_3}} Infant</span>
					</strong>
				</td>
			</tr>
		</table>
		
		<div ng-show='DATA.voucher.voucher.voucher.pickup'>
			<br />
			<div class="sub-title"><strong>PICKUP INFORMATION</strong></div>
			<table width="100%" class="table">
				<tr>
					<td width="100">Area</td>
					<td><strong>{{DATA.voucher.voucher.voucher.pickup.area}} - {{DATA.voucher.voucher.voucher.pickup.time}}</strong></td>
				</tr>
				<tr>
					<td>Hotel Name</td>
					<td><strong>{{DATA.voucher.voucher.voucher.pickup.hotel_name}}</strong></td>
				</tr>
				<tr>
					<td>Hotel Address</td>
					<td><strong>{{DATA.voucher.voucher.voucher.pickup.hotel_address}}</strong></td>
				</tr>
				<tr>
					<td>Phone Number</td>
					<td><strong>{{DATA.voucher.voucher.voucher.pickup.hotel_phone_number}}</strong></td>
				</tr>
			</table>
		</div>
		<div ng-show='DATA.voucher.voucher.voucher.dropoff'>
			<br />
			<div class="sub-title"><strong>DROPOFF INFORMATION</strong></div>
			<table width="100%" class="table">
				<tr>
					<td width="100">Area</td>
					<td><strong>{{DATA.voucher.voucher.voucher.dropoff.area}} - {{DATA.voucher.voucher.voucher.dropoff.time}}</strong></td>
				</tr>
				<tr>
					<td>Hotel Name</td>
					<td><strong>{{DATA.voucher.voucher.voucher.dropoff.hotel_name}}</strong></td>
				</tr>
				<tr>
					<td>Hotel Address</td>
					<td><strong>{{DATA.voucher.voucher.voucher.dropoff.hotel_address}}</strong></td>
				</tr>
				<tr>
					<td>Phone Number</td>
					<td><strong>{{DATA.voucher.voucher.voucher.dropoff.hotel_phone_number}}</strong></td>
				</tr>
			</table>
		</div>
	
		<br />
		<div class="sub-title"><strong>PARTICIPANT</strong></div>
		<br />
		<div class="table-responsive">
			<table width="100%" class="table table-bordered table-hover table-condensed" cellspacing="1">
				<tr class="header bold">
					<td width="40">#</td>
					<td>Full Name</td>
					<?php /*?><td width="150">Email</td><?php */?>
					<td width="130">Phone</td>
					<td width="130">ID Number</td>
					<td width="130">Nationality</td>
					<td width="80"></td>
				</tr>
				<tr ng-repeat='passenger in DATA.voucher.voucher.voucher.passenger' ng-class="{'success':(passenger.checkin_status==1 && DATA.voucher.allow_to_checkin)}"
					ng-click="selectToCheckIn(passenger)" style="cursor:pointer">
					<td width="30">{{($index+1)}}</td>
					<td>{{passenger.first_name}} {{passenger.last_name}}</td>
					<?php /*?><td width="150">{{passenger.email}}</td><?php */?>
					<td width="150">{{passenger.phone}}</td>
					<td width="150">{{passenger.passport_number}}</td>
					<td width="150">{{passenger.country_name}}</td>
					<?php /*?><td width="100" style="padding:0;">
						<input type="text" class="form-control input-sm" ng-click='passenger.checkin_status=1' placeholder="Code" ng-model='passenger.pass_code' />
					</td><?php */?>
					<td width="50" align="center">
						<div ng-show='passenger.already_checkin'><small style="color:green"><em>Checked In</em></small></div>
						<div ng-show='DATA.voucher.allow_to_checkin && !passenger.already_checkin'>
							<input type="checkbox" ng-disabled="is_checkedin_proccessed" ng-true-value="1" ng-false-value="0" ng-checked="(passenger.checkin_status==1)" ng-model="passenger.checkin_status" />
						</div>
					</td>
				</tr>
			</table>
		</div>
		<br /><br />
		<div ng-show='DATA.voucher.allow_to_checkin'>
			<div class="text-right" ng-show='!is_checkedin_proccessed'>
				<a href="" ng-click="checkUncheckParticipant(1, DATA.voucher.voucher.voucher.passenger)"><span class="glyphicon glyphicon-ok"></span> Check All</a> 
				&nbsp;&nbsp; | &nbsp;&nbsp; 
				<a href="" ng-click="checkUncheckParticipant(0, DATA.voucher.voucher.voucher.passenger)"><span class="glyphicon glyphicon-remove"></span> Uncheck All</a>
				<br /><br />
				<hr />
				<button type="button" class="btn btn-primary btn-lg" ng-click='checkInParticipantNow()'>Check In Now</button>
			</div>
			<div class="text-right" ng-show='is_checkedin_proccessed'>
				<strong><em>Processing...</em></strong>
			</div>
		</div>
		
		<div ng-show="!DATA.voucher.allow_to_checkin && DATA.voucher.voucher.booking.balance">
			<div class="alert alert-danger">
				<div class="pull-right text-right">
					<strong>Balance</strong>
					<div style="font-size:18px">
						{{DATA.voucher.voucher.booking.currency}}
						{{fn.formatNumber(DATA.voucher.voucher.booking.balance, DATA.voucher.voucher.booking.currency)}}
					</div>
				</div>
				<div>
					<strong>Sorry...</strong><br />
					Can not check in at this time.
				</div>
			</div>
		</div>
	</div>
	<script>$("#check_voucher_code").focus();</script>
</div>

<?php
//pre($vendor)
?>
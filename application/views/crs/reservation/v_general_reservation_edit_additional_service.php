<div class="modal" id="modal-add-additional-service" tabindex="-1" role="dialog" aria-labelledby="ModalEditPicdrpLabel">
	<form ng-submit='submit_edit_additional_service($event)'>
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="ModalEditPicdrpLabel">EDIT ADDITIONAL SERVICE</h4>
			</div> 
            <div class="modal-body products">
				<div ng-show='DATA.myAdditionalService.error_desc.length > 0' class="alert alert-danger"><ul><li ng-repeat='error in DATA.myAdditionalService.error_desc'>{{error}}</li></ul></div>
            	<table class="table" style="background:none;">
                	<tr class="info" style="font-weight:bold">
                    	<td>#</td>
                        <td>Additional Service</td>
                        <td>Qty.</td>
                        <td>Price</td>
                        <td>Sub Total</td>
                        <td></td>
                    </tr>
                    <tr ng-repeat='add_srv in DATA.myAdditionalService.items'>
                    	<td width="50">{{($index+1)}}</td>
                        <td><input required="required" type="text" class="form-control input-sm" ng-model='add_srv.name' placeholder="Additional Service Name" title="Additional Service Name" /></td>
                        <td width="90"><input type="number" class="form-control input-sm text-right" ng-model='add_srv.qty' min="1" step="1" title="Qty." ng-change="edit_additional_service_calculateTotal()" /></td>
                        <td width="180">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><?=$vendor["default_currency"]?></span>
                                <input required="required" type="number" class="form-control input-sm text-right" ng-model='add_srv.price' min="0" step="0.5" title="Price" ng-change="edit_additional_service_calculateTotal()" />
                            </div>
                        </td>
                        <td width="180" align="right">
                        	<div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><?=$vendor["default_currency"]?></span>
                                <input required="required" type="text" disabled="disabled" class="form-control input-sm text-right" value="{{fn.formatNumber((add_srv.qty*add_srv.price), '<?=$vendor["default_currency"]?>')}}" />
                            </div>
                        </td>
                        <td width="50" align="right"><a href="" style="color:red" ng-click='edit_additional_service_removeAdditionalService($index)'><span class="glyphicon glyphicon-trash"></span></a></td>
                    </tr>
                    <tr>
                    	<td></td>
                        <td colspan="5">
                        	<a href="" ng-click='edit_additional_service_addAdditionalService ()'><span class="glyphicon glyphicon-plus"></span> Add Item</a>
                        </td>
                    </tr>
                    <tr class="success" style="font-weight:bold">
                    	<td colspan="4" align="right">TOTAL</td>
                        <td class="text-right">
                        	<?=$vendor["default_currency"]?>
                        	{{fn.formatNumber(DATA.myAdditionalService.TOTAL, '<?=$vendor["default_currency"]?>')}}
                        </td>
                        <td></td>
                    </tr>
                </table>
                <br /><br />
                            
                <div class="modal-footer" style="text-align:center">
                    <button type="submit" class="btn btn-primary btn-submit-edit-picdrp" >Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
			</div>	
		</div>
	</div>
    </form>
</div>
<?php
	$email_user = isset($_SESSION["ss_login_CRS"]["credential"]["user"])?$_SESSION["ss_login_CRS"]["credential"]["user"]["email"]:$_SESSION["ss_login_CRS"]["vendor"]["email"];
	$code_user  = isset($_SESSION["ss_login_CRS"]["credential"]["user"])?$_SESSION["ss_login_CRS"]["credential"]["user"]["id"]:"master";
?>
<style>
	.red {
		background: #f2dede;
    	border-color: #eed3d7;
	}
	.white {
		background: #ffffff;
    	border-color: #ffffff;
	}
</style>
<div ng-init="loadDataRoomType()" ng-show="DATA.room_type">
	<div ui-view>
		<div class="pull-right">
			<a ui-sref="roomtype.add">
				<button type="button" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-plus"></span> New Room Type</button>
			</a>
		</div>

		<h1>Room Type</h1>
		<div class="products">
			<div class="product row" ng-repeat="roomtype in DATA.room_type" ng-class="bg_color">
				<div class="col-xs-5 col">
					<div>
						<img src="<?=base_url("public/images/no_image.jpg")?>" width="300px" height="200px" ng-show="!DATA.roomtype.name" />
					</div>
				</div>
				<div class="col-xs-7 col">
					<div class="desc">
						<div class="title">
							<span class="glyphicon glyphicon-link" title="Link with room Link Room"></span> 
							<a ui-sref="roomtype.edit({'roomtype_code':roomtype.roomtype_code})" class="title">{{roomtype.name}}</a>
						</div>
						<div class="stats">
							<span class="glyphicon glyphicon-heart"></span> 12 Likes
							&nbsp;&nbsp;&nbsp;
							<span class="glyphicon glyphicon-eye-open"></span> 100 Views
							&nbsp;&nbsp;&nbsp;
							<span class="glyphicon glyphicon-usd"></span> 20 Sold
						</div>
						<div class="code">
							Code : {{roomtype.roomtype_code}}
						</div>
						<div class="perroom">
							Per Room Rates
						</div>
						<br />
						<div class="button-show">
							<div class="pull-right" style="padding-top:10px" ng-model="bg_color">
								<img value="white" style="cursor: pointer;" ng-click="publishUnpublishRoomtype(roomtype.roomtype_code, 1)" ng-show="roomtype.publish_status == 1" src="<?=base_url("public/images/off-button.png")?>" height="20" />
								<img value="red" style="cursor: pointer;" ng-click="publishUnpublishRoomtype(roomtype.roomtype_code, 0)" ng-show="roomtype.publish_status == 0" src="<?=base_url("public/images/on-button.png")?>" height="20" />
							</div>
							<a ui-sref="roomtype.edit({'roomtype_code':roomtype.roomtype_code})" title="Edit Product">
								<button type="button" class="btn btn-info btn-sm">&nbsp;&nbsp;<span class="glyphicon glyphicon-pencil"></span> Edit&nbsp;&nbsp;&nbsp;</button>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<br/>
		<div class="clearfix"></div>
		<br/>
		<div class="add-product-button">
			<a ui-sref="roomtype.add">
				<button type="button" class="btn btn-success btn-lg" style="width:100%"><span class="glyphicon glyphicon-plus"></span> New <?=ACC_ROOM_CAPTION($vendor)?> Type</button>
			</a>
		</div>
	</div>
</div>
<script>activate_sub_menu_agent_detail("roomtype");</script>

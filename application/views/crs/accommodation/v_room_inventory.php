<?php
	$email_user = isset($_SESSION["ss_login_CRS"]["credential"]["user"])?$_SESSION["ss_login_CRS"]["credential"]["user"]["email"]:$_SESSION["ss_login_CRS"]["vendor"]["email"];
	$code_user  = isset($_SESSION["ss_login_CRS"]["credential"]["user"])?$_SESSION["ss_login_CRS"]["credential"]["user"]["id"]:"master";
?>
<style>
	td.CTA{
		background: -webkit-gradient(linear, left top, right bottom, color-stop(50%,#FFC36A), color-stop(50%,#FFF));
	}
	td.CTD{
		background: -webkit-gradient(linear, left top, right bottom, color-stop(50%,#FFF), color-stop(50%,#D479F9));
	}
	td.CTA.CTD{
		background: -webkit-gradient(linear, left top, right bottom, color-stop(50%,#FFC36A), color-stop(50%,#D479F9)) !important;
	}
</style>
<h1>RATE & Inventory</h1>
<br/>
<div>
    <div ui-view>
        <div class="pull-right" style="margin-right:10px">
            <div class="rows">
                <form ng-submit="loadAllotment()">
                    <div class="col-md-7">
                        <div class="input-group" style="width:200px" ng-init="loadAllotment();">
                            <input type="text" class="form-control input-sm datepicker" ng-model="DATA.dateNow" style="text-align:center" />
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-success btn-sm">GO</button>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-sm"><span class='glyphicon glyphicon-menu-left'></span> Pref </button>
                            <button type="button" class="btn btn-default btn-sm"><span class='glyphicon glyphicon-menu-right'></span> Next </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class='clearfix'>
            <h4>Room Inventories</h4>
        </div>
        <table ng-init="loadDataRoomType(); loadAllotment();"  class="table table-bordered table-view-rate" width="100%" style="font-size:12px">
            <tr>
                <td>{{DATA.MonthNow}}</td>
                <td ng-repeat="item in DATA.inventory[0].inventori" style="text-align: center;">{{item.date | date:'d / EEE'}}</td>
            </tr>
            <tr ng-repeat="inven in DATA.inventory" class="successs" style="font-size:11px; background:#EEE">
                <td><a ui-sref="roomtype.edit({'roomtype_code':inven.room.roomtype_code})" target="_blank">{{inven.room.name}}</a></td>
                <td ng-class="{'CTA':(item.is_cta == 1), 'CTD':(item.is_ctd == 1), 'CTA.CTD':(item.is_cta == 1 && item.is_ctd == 1)}" ng-repeat="item in inven.inventori" style="text-align: center;">{{item.allotment}}</td>
            </tr>
        </table>
        <br />
        <div style="text-align:center">
            <div class="btn-group" ng-init="loadDataRoomType()" ng-show="DATA.room_type">
                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="glyphicon glyphicon-calendar"></span> Inventories Calendar <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li ng-repeat="roomtype in DATA.room_type">
                        <a href="#" class="fancybox ajax-data" style="font-size: 15px;">{{roomtype.name}}</a>
                    </li>
                </ul>
            </div>
            <a href="" data-toggle="modal" data-target="#modal-edit-allotment" ng-click=''>
                <button type="button" class="btn btn-sm btn-success btn-fancybox">
                    <span class="glyphicon glyphicon-pencil"></span> Setup Inventory 
                </button>
            </a>
            <a href="" data-toggle="modal" data-target="#modal-edit-date" ng-click=''>
                <button type="button" class="btn btn-sm btn-danger btn-fancybox">
                    <span class="glyphicon glyphicon-cog"></span> Date Setting
                </button>
            </a>    
            <br/>
            <br/>
        </div>
        <div style="text-align:center;">
            <table>
                <tr>
                    <td class="closed-date" width="30">&nbsp;</td>
                    <td width="100">&nbsp;Closed Date</td>
                    
                    <td class="CTA" width="40">&nbsp;</td>
                    <td width="100">&nbsp;Closed To Arrival</td>
                    
                    <td class="CTD" width="40">&nbsp;</td>
                    <td width="130">&nbsp;Closed To Departure</td>
                    
                    <td width="20" align="right"><span class="glyphicon glyphicon-link"></span></td>
                    <td width="100">&nbsp;Linked Room</td>
                    
                    <td></td>
                </tr>
            </table>
        </div>
        <br/>
        <br/>
        <h4>Base Rates (IDR)</h4>
        <table ng-init="loadDataRoomType(); loadRate();" class="table table-bordered table-view-rate" width="100%" style="font-size:12px">
            <tr>
                <td>{{DATA.MonthNow}}</td>
                <td ng-repeat="item in DATA.inventory[0].inventori" style="text-align: center;">{{item.date | date:'d / EEE'}}</td>
            </tr>
            <tr ng-repeat="inven in DATA.rate" class="successs" style="font-size:11px; background:#EEE">
                <td><a ui-sref="roomtype.edit({'roomtype_code':inven.room.roomtype_code})" target="_blank">{{inven.room.name}}</a></td>
                <td ng-repeat="item in inven.rate" style="text-align: center;">{{item.rate}}</td>
            </tr>
        </table>
        <hr/>
    </div>
    <?php $this->load->view("crs/accommodation/v_room_inventory_edit_date") ?>
    <?php $this->load->view("crs/accommodation/v_room_inventory_edit_allotment") ?>
</div>
<script>activate_sub_menu_agent_detail("roomtype");</script>
<?php
	$email_user = isset($_SESSION["ss_login_CRS"]["credential"]["user"])?$_SESSION["ss_login_CRS"]["credential"]["user"]["email"]:$_SESSION["ss_login_CRS"]["vendor"]["email"];
	$code_user  = isset($_SESSION["ss_login_CRS"]["credential"]["user"])?$_SESSION["ss_login_CRS"]["credential"]["user"]["id"]:"master";
?>
<div ng-init="loadDataRoomTypeDetail();" ng-show="DATA.current_roomtype">
    <div ui-view>
        <h1>Edit Room Type</h1>
        <div class="products">
            <div>
            <form ng-submit="editDataRoomtype($event, DATA.current_roomtype.roomtype_code)">
                <table class="table table-borderlesss table-condensed">
                    <tr>
                        <td width="150">Room Type Name</td>
                        <td>
                            <div class="col-md-10">
                                <input type="text" class="form-control" placeholder="Room Type Name" required="required" ng-model='DATA.current_roomtype.name' />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="150">Room Size</td>
                        <td>
                            <div class="col-md-3">
                                <input type="text" class="form-control" placeholder="Room Size" required="required" ng-model='DATA.current_roomtype.room_size' />
                            </div>    
                        </td>
                    </tr>
                    <tr>
                        <td width="150">Bed Type</td>
                        <td>
                            <div class="col-md-6" ng-init="loadBedtype()" ng-show="DATA.bed_type">
                                <!-- <input type="text" class="form-control" placeholder="Bed Type" required="required" ng-value='DATA.current_roomtype.bed_type.name' ng-model='DATA.current_roomtype.id_bed_type' /> -->
                                <select class="form-control" required="required" ng-model='DATA.current_roomtype.extra_bed'>
                                    <option value="{{bed_type.id_bed_type}}" ng-repeat="bed_type in DATA.bed_type">{{bed_type.bed_type_name}}</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="150">Max. Person</td>
                        <td>
                            <div class="col-md-3">
                                <input type="number" class="form-control" placeholder="Max. Person" required="required" ng-value='DATA.current_roomtype.max_person' ng-model='DATA.current_roomtype.max_person' />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="150">Extra Bed</td>
                        <td>
                            <div class="col-md-3">
                                <select class="form-control" required="required" ng-model='DATA.current_roomtype.extra_bed'>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="150">Max. Extra Bed</td>
                        <td>
                            <div class="col-md-2">
                                <select class="form-control" required="required" placeholder="Max. Extra Bed" ng-model='DATA.current_roomtype.max_extra_bed'>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                            </div>
                            <div class="col-xs-2">
                                <p>Currency</p>
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control" placeholder="Currency" required="required" ng-value="DATA.current_roomtype.extra_bed.currency" ng-model='DATA.current_roomtype.currency' />
                            </div>
                            <div class="col-xs-2">
                                <p>Price Extra Bed</p>
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control" placeholder="Price Extra Bed" required="required" ng-value="DATA.current_roomtype.extra_bed.price" ng-model='DATA.current_roomtype.price_extra_bed' />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="150">Room Description</td>
                        <td>
                            <div class="col-md-12">
                                <textarea class="form-control autoheight" placeholder="Remarks" ng-value="DATA.current_roomtype.description" ng-model='DATA.current_roomtype.description' rows="3"></textarea>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="150">Room Facilities</td>
                        <td>
                            <div class="col-md-12">
                                <input type="text" class="form-control" placeholder="Room Facilities" ng-model='DATA.current_roomtype.room_facilities' />
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="table table-borderlesss">
                    <tr>
                        <td width="150"></td>
                        <td>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </td>
                    </tr>
                </table>
                </form>
            </div>
        </div>
        <div style="text-align:right">
            <a href=""><span class="glyphicon glyphicon-duplicate"></span> Duplicate Room Type</a>
            &nbsp;&nbsp;&nbsp;
            <a href="" style="color:red" style="cursor: pointer;" ng-click="deleteRoomtype(DATA.current_roomtype.roomtype_code)"><span class="glyphicon glyphicon-trash" style="color:red"></span> Delete Room Type</a>
        </div>
    </div>
</div>
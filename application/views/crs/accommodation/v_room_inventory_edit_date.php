<div ng-init="loadDataRoomType();" class="modal" id="modal-edit-date" role="dialog">
	<form method="post" ng-submit="updateClose($event)">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Date Setting</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-condensed table-borderless">
                        <tr>
                            <td>Room</td>
                            <td>
                                <div ng-repeat="(index,roomtype) in DATA.room_type">
                                    <label>
                                    <input type="checkbox" ng-model="is_checked" ng-click="getRoomTypeCode(roomtype.roomtype_code, is_checked)" />
                                        {{roomtype.name}}
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td width="90">Start Date</td>
                            <td><input type="text" class="form-control input-sm datepicker" placeholder="Start Date" ng-model="DATA.select.start_date" style="width:150px"/></td>
                        </tr>
                        <tr>
                            <td width="90">End Date</td>
                            <td><input type="text" class="form-control input-sm datepicker" placeholder="End Date" ng-model="DATA.select.end_date" style="width:150px"/></td>
                        </tr>
                        <tr>
                            <td width="90">Status</td>
                            <td>
                            <select name="status" style="width:150px" class="form-control input-sm" ng-model="DATA.select.close">
                                <option value="" disabled selected>-- Status --</option>
                                <option value="1">Close Date</option>
                                <option value="0">Open Date</option>
                                <option value="cta">Closed To Arrival</option>
                                <option value="ctd">Closed To Departure</option>
                            </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer" style="text-align:center">
                    <button type="submit" class="btn btn-primary btn-submit-edit-picdrp">Update</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </form>
</div>
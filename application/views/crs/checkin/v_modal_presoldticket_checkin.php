<div class="modal fade" id="frm-openticket-checkin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">
            Pre Sold Ticket Checkin : <strong>{{DATA.openticket.openticket_code}}</strong>
        </h4>
      </div>
      <div class="modal-body">
		<div ng-show='DATA.openticket.error_msg.length>0' class="alert alert-danger"><ul><li ng-repeat='err in DATA.openticket.error_msg'>{{err}}</li></ul></div>
        
        <form ng-submit='check_availability_openticket($event)'>
        	<div ng-show='!DATA.data_rsv_OT.selected_trip'>
            	<h4>TRIPS</h4><hr style="margin:5px 0" />
                <table class="table table-condensed table-borderless">
                    <tr>
                        <td width="100">Departure Date</td>
                        <td><input placeholder="Departure Date" required="required" type="text" class="form-control input-md datepicker" ng-model='add_trip.departure_date' style="width:150px" /></td>
                    </tr>
                    <tr>
                        <td>From</td>
                        <td>
                            <select class="form-control input-md departure-port" ng-model='add_trip.departure_port_id' required="required">
                                <option value="">-- Departure Port --</option>
                                <option value="{{port.id}}" ng-repeat='port in $root.DATA_available_port'>{{port.port_code}} - {{port.name}}</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>To</td>
                        <td>
                            <select class="form-control input-md departure-port" ng-model='add_trip.arrival_port_id' required="required">
                                <option value="">-- Destination Port --</option>
                                <option value="{{port.id}}" ng-repeat='port in $root.DATA_available_port'>{{port.port_code}} - {{port.name}}</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <button type="submit" class="btn btn-success">Check Availability</button>
                        </td>
                    </tr>
                </table>
                <hr />
            </div>
            <div class="text-center" ng-show='check_availabilities_show_loading'>
                <strong><em>Loading...</em></strong>
            </div>
            
            <div ng-show='check_availabilities_data.departure && !check_availabilities_data.departure.availabilities'>
				<div class="alert alert-warning">
					<strong>Sorry</strong>, Trip not available, please select another date...
				</div>
			</div>
            
            <div class="product search-trip-result" ng-show='check_availabilities_data.departure.availabilities'>
				<h4>TRIPS</h4><hr style="margin:5px 0" />
				<div class="pull-right">
					<strong>{{fn.formatDate(check_availabilities_data.departure.check.date, "dd M yy")}}</strong>
				</div>
				<div>
					<strong>{{check_availabilities_data.departure.check.departure.port_code}} - {{check_availabilities_data.departure.check.departure.name}}</strong>
					&nbsp; <i class="fa fa-chevron-right"></i> &nbsp;
					<strong>{{check_availabilities_data.departure.check.arrival.port_code}} - {{check_availabilities_data.departure.check.arrival.name}}</strong>
				</div>
				<hr style="margin:5px 0" />
				<div class="trip" ng-class="{'selected':availabilities.is_selected}" ng-show='!availabilities.is_hidden' 
                	ng-repeat="availabilities in check_availabilities_data.departure.availabilities | orderBy : 'departure.time'" 
                    ng-click="check_availabilities_select_this_trip(availabilities, check_availabilities_data.departure)">
					<div class="media">
					  <div class="media-left">
						<a href="#">
						  <img class="media-object" ng-src="{{availabilities.schedule.boat.main_image}}" width="80px" />
						</a>
					  </div>
					  <div class="media-body">
					  	<i class="fa fa-clock-o"></i> <strong>{{availabilities.departure.time}} - {{availabilities.arrival.time}}</strong> ({{availabilities.schedule.boat.available_space}} space left)<br />
						<small><i class="fa fa-ship"></i> 	<strong>{{availabilities.schedule.boat.name}} ({{availabilities.schedule.boat.capacity}} seats)</strong></small><br />
					  </div>
					</div>
				</div>
			</div>
            
		</form>
        
        <form ng-submit='submit_booking_open_ticket($event)' id="frm-booking-openticket">
            <table class="table table-condensed table-borderless" ng-show='DATA.data_rsv_OT.selected_trip'>
                <tr>
                    <td colspan="2"><br><h4>GUEST INFORMATION</h4><hr style="margin:5px 0" /></td>
                </tr>
                <tr>
                    <td width="100">Name*</td>
                    <td>
                    
                        <input type="text" placeholder="First Name" class="form-control input-md" ng-model='DATA.data_rsv_OT.customer.first_name' style="display:inline; width:47.5%" required />
                        &nbsp;&nbsp;&nbsp;
                        <input type="text" placeholder="Last Name" class="form-control input-md" ng-model='DATA.data_rsv_OT.customer.last_name' style="display:inline; width:47.5%" required />
                    </td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><input type="text" placeholder="Email" class="form-control input-md" ng-model='DATA.data_rsv_OT.customer.email' /></td>
                </tr>
                <tr>
                    <td>Phone / Mobile</td>
                    <td><input type="text" placeholder="Phone / Mobile" class="form-control input-md" ng-model='DATA.data_rsv_OT.customer.phone' /></td>
                </tr>
                <tr>
                    <td>Country*</td>
                    <td>
                        <select class="form-control input-md" ng-model='DATA.data_rsv_OT.customer.country_code' required>
                            <option value="">-- Select Country --</option>
                            <option value="{{country.code}}" ng-repeat='country in $root.DATA_country_list'>{{country.name}}</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><br><h4>REMARKS / SPECIAL REQUEST</h4><hr style="margin:5px 0" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td><textarea placeholder="Remarks / Special Request..." class="form-control input-md autoheight" ng-model='DATA.data_rsv_OT.remarks' rows="2"></textarea></td>
                </tr>
                <tr>
                    <td colspan="2"><br><h4>CHECK IN</h4><hr style="margin:5px 0" /></td>
                </tr>
                <tr>
                	<td></td>
                    <td>
                    	<label> 
                        	<input type="checkbox" ng-model='DATA.data_rsv_OT.checkin_now' ng-true-value='1' ng-false-value='0' ng-checked="DATA.data_rsv_OT.checkin_now == '1'" /> Check In Now
                        </label>
                    </td>
                </tr>
            </table>
            <button type="submit" style="display:none"></button>
        </form>
        
      </div>
      <div class="modal-footer" style="text-align:center">
        <button type="button" class="btn btn-primary" ng-show='DATA.data_rsv_OT.selected_trip' onClick="$('#frm-booking-openticket button:submit').click();">Submit Now</button>
		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<style>
.search-trip-result .trip{padding:5px}
.search-trip-result .trip:hover{background:#d9edf7; cursor:pointer}
.xxsearch-trip-result .trip.selected{background:#a3cce0}
</style>
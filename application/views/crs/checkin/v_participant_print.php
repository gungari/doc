
<div ng-init='printReceipt()'>

	
    <?php //SMALL PAPER ----------------------------------------------------------------------------------------------------------------------------------------- ?>
	<div class="small-paper" ng-repeat="voucher in DATA.voucher.voucher.detail">
		<div class="header">
			<div class="title"><?=$vendor["business_name"]?></div>
			<div><?=$vendor["address"]?>, <?=$vendor["regency"]?>, <?=$vendor["province"]?>, <?=$vendor["country"]?></div>
			<div>[P] : <?=$vendor["telephone"]?></div>
			<div>[E] : <?=$vendor["email"]?></div>
			<div><?=$vendor["website"]?></div>
		</div>
		
       	<div class="sub-title"><strong>VOUCHER INFORMATION</strong></div>
            <table width="100%" class="table">
            <tr>
                  <td width="100">Booking Code</td>
                  <td><strong>#{{DATA.voucher.voucher.booking_code}}</strong></td>
            </tr>
            <tr>
                  <td>Voucher Code</td>
                  <td><strong>{{voucher.voucher_code}}</strong></td>
            </tr>
            <tr>
                  <td height="10">Date</td>
                  <td height="10"><strong>{{fn.formatDate(voucher.date, "dd MM yy")}}</strong></td>
            </tr>
            <tr ng-show="voucher.product_type == 'TRANS'">
                  <td height="10">Trip</td>
                  <td height="10" style="font-size:16px">
                        <strong>{{voucher.departure.port.name}} ({{voucher.departure.port.port_code}}) - {{voucher.departure.time}}</strong>&nbsp;&nbsp;<i class="fa fa-chevron-right"></i>&nbsp;&nbsp;
                        <strong>{{voucher.arrival.port.name}} ({{voucher.arrival.port.port_code}}) - {{voucher.arrival.time}}</strong>
                  </td>
            </tr>
        <tr ng-show="voucher.product_type == 'ACT'">
                  <td>Product</td>
                  <td style="font-size:16px">
                <strong>{{voucher.product.name}}</strong>
            </td>
            </tr>
            <tr ng-show="DATA.voucher.voucher.source_code == 'AGENT'">
                  <td>Booking Source</td>
                  <td><strong>{{DATA.voucher.voucher.agent.name}}</strong></td>
            </tr>
            <tr ng-show="!DATA.voucher.voucher.source_code == 'AGENT'">
                  <td>Booking Source</td>
                  <td><strong>{{DATA.voucher.voucher.source}}</strong></td>
            </tr>
            <tr>
                  <td>Pass Code</td>
                  <td><strong>{{voucher.passenger.pass_code}}</strong></td>
            </tr>
      </table>
      <div class="sub-title"><strong>PICKUP INFORMATION</strong></div>
      <div ng-show='voucher.pickup'>
            
            
            <table width="100%" class="table">
                  <tr>
                        <td width="100">Area</td>
                        <td><strong>{{voucher.pickup.area}} - {{voucher.pickup.time}}</strong></td>
                  </tr>
                  <tr>
                        <td>Hotel Name</td>
                        <td><strong>{{voucher.pickup.hotel_name}}</strong></td>
                  </tr>
                  <tr>
                        <td>Hotel Address</td>
                        <td><strong>{{voucher.pickup.hotel_address}}</strong></td>
                  </tr>
                  <tr>
                        <td>Phone Number</td>
                        <td><strong>{{voucher.pickup.hotel_phone_number}}</strong></td>
                  </tr>
            </table>
      </div>
       <div class="sub-title"><strong>DROPOFF INFORMATION</strong></div>
      <div ng-show='voucher.dropoff'>
            
           
            <table width="100%" class="table">
                  <tr>
                        <td width="100">Area</td>
                        <td><strong>{{voucher.dropoff.area}} - {{voucher.dropoff.time}}</strong></td>
                  </tr>
                  <tr>
                        <td>Hotel Name</td>
                        <td><strong>{{voucher.dropoff.hotel_name}}</strong></td>
                  </tr>
                  <tr>
                        <td>Hotel Address</td>
                        <td><strong>{{voucher.dropoff.hotel_address}}</strong></td>
                  </tr>
                  <tr>
                        <td>Phone Number</td>
                        <td><strong>{{voucher.dropoff.hotel_phone_number}}</strong></td>
                  </tr>
            </table>
      </div>
      <div style="margin-top: 50px;" class="pull-center">
      _ _ _ _ _
      </div>
	</div>
	
</div>
<!-- <script type="text/javascript">
	$(document).ready(function () {
	    window.print();
	});
</script> -->
<script>
	$(".print_area").addClass("small_paper");
		
</script>

<style>


	.print_area{background:none !important; padding:0 !important;height: 500px;}
	.small_paper{width:8cm !important; margin:0 auto !important}
	.normal-paper{background:#FFF; padding:10px}
	.small-paper{width:8cm; margin:auto; padding:10px; background:#FFF}
	.small-paper .header .title{font-size:18px !important}
	.modal-dialog{width: 400px !important}
	.sub-title{padding: 0 !important;margin: 0 !important;margin-top: 10px !important;}
      table{margin: 0 !important}
      table > tbody > tr > td{padding: 0 !important}

</style>
<?php
//pre($vendor)
?>

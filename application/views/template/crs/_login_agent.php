<!DOCTYPE html>
<html>
  <head>
  	<title>CRS - <?=$this->my_config->company_name?></title>
    <?php $this->load->view("template/crs/_include_js_css"); ?>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  </head>
  <body class='login' style="background-color:#eeeeee; background-image:url(<?=base_url("public/admin_clean/login-left-corner.png")?>); background-repeat: no-repeat;">

	
    <div style="text-align:center; margin-top:100px">
    <?php if (@$code != '') { ?>
        <?php if ($image_logo != '') { ?>
            <img src="<?="https://bes.hybridbooking.com/webimages/vendor/thumb/".$image_logo?>" width="auto" height="80px" />
        <?php }else{ ?>
            <h3><?php echo $company; ?></h3>
       <?php } ?>
    <?php }else{ ?>
	    <img src="<?=site_url("public/images/hybrid-png.png")?>" width="auto" height="80px" />
    <?php } ?>
    </div>
    
	<div id="MainContainer" align="center" style="background:#FFF; padding:20px 10px; box-shadow:0 0 0 #CCCCCC; border-radius: 5px;"> 
   <?php /*?> <a href="<?=site_url()?>"><img src="<?=base_url($this->my_config->company_logo)?>" alt="<?=$this->my_config->company_name?>" border="0" /></a><br /><br />
	<div id="DivSeparator" style="width:100%; border-color:#999"></div><?php */?>
    
    <?php if (isset($recover_password)){ ?>
	<style>#login{display:none}</style>
    <div id="recover_password">
        <h3 class="title-page">Recover Your Password</h3>
        <hr />
        <form action="<?=site_url("login_agent/update_recover_password")?>" method="post" id="frm_change_password">
            <input type="hidden" name="code_verification" value="<?=$validation_forgot_pass?>" />
            <input type="hidden" name="agent_code_verify" value="<?=@$agent_code_verify?>">
            <input type="hidden" name="vendor_code_verify" value="<?=@$vendor_code_verify?>">
            <div class="show_error hidden-field" style="text-align:left"></div>
            <table border="0" align="center" cellpadding="3" cellspacing="0" class="table table-borderless">
           <!--  <?=pre($agent);?> -->
                <tr>
                    <td width="150">Full Name</td>
                    <td><strong><?=@$name?></strong></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><strong><?=@$agent['email']?></strong></td>
                </tr>
                <tr>
                    <td>New Password</td>
                    <td><input type="password" name="new_password" class="form-control input-sm" /> </td>
                </tr>
                <tr>
                    <td>Retype New Password</td>
                    <td><input type="password" name="new_password_2" class="form-control input-sm" /> </td>
                </tr>
                <tr>
                    <td></td>
                    <td><button type="submit" class="btn btn-success">Update Password</button></td>
                </tr>
            </table>
        </form>
        <hr />
        <a href="<?=site_url("agent/$code")?>">Login</a>
    </div>
    <script>
        $("#frm_change_password").submit(function(){
            var mydata = $(this).serialize();
            var form = $(this);
            $.ajax({
                type: "POST",
                url: form.attr("action")+"/ajax",
                data: mydata,
                beforeSend : function(){
                    form.find(".show_error").slideUp().html("");
                    //form.find("button[type=submit]").attr("disabled","disabled");
                },
                success: function(response, textStatus, xhr) {

                    var respon = JSON.parse(response);
                    if ($.trim(respon.status) == "SUCCESS"){
                        form.find(".show_error").hide().html("<div class='alert alert-success'><strong>Success</strong>. Your password is successfully updated.</div>").slideDown("fast");
                        //form.find("button[type=submit]").removeAttr("disabled");
                        form.find("input[type=password]").val("");
                    }else{
                        var error_arr = respon.error_desc;
                        error_msg = error_arr.join('</li><li>');
                        form.find(".show_error").hide().html("<div class='alert alert-danger'><strong>Sorry...</strong><br /><br /><ul><li>"+error_msg+"</li></ul>.</div>").slideDown("fast");
                        form.find("button[type=submit]").removeAttr("disabled");
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    //form.find("button[type=submit]").removeAttr("disabled");
                }
            });
            return false;
        });
    </script>
    <?php } ?>
    
    <div id="login">
       
        <form id="frm_login" <?=(isset($is_forgot_pwd))?'class="hidden-field"':''?> name="form1" method="post" action="<?=site_url("login_agent/checklogin".(isset($_GET["redirect"])?"?redirect=$_GET[redirect]":""))?>">
            <h3 style="margin-top:0">Sign in to your Agent Pagee </h3>
            Please enter your email and password.
			
			<?php if (isset($err_login) && is_array($err_login) && count($err_login) > 0){ ?>
			<br><br>
			<div class="alert alert-danger text-left">
				<strong>Sorry..</strong><br /><br />
				<ul style="padding-left:20px">
					<li><?=implode("</li><li>", $err_login)?></li>
				</ul>
			</div>
			<?php } ?>
			
            <hr style="margin:10px 0" />
             <div class="show_error_login hidden-field" style="text-align:left"><?=implode("</li><li>", $err_login)?></div>
          	<table border="0" align="center" cellpadding="5" cellspacing="0" width="90%" class="table table-borderless">
              <tr>
                <td>EMAIL</td>
                <td><input type="text" name="email" value="<?=(isset($_POST["email"]))?$_POST["email"]:""?>" id="f_email" class='form-control' placeholder='Type your email' />
                </td>
              </tr>
              <tr>
                <td>PASSWORD</td>
                <td><input type="password" name="password" id="f_password" class='form-control' placeholder='Type your password' />
                </td>
              </tr>
              <?php if (@$code == '') { ?>
                 <tr>
                    <td>CODE</td>
                    <td><input type="text" name="code" value="<?=(isset($_POST["code"]))?$_POST["code"]:""?>" id="f_code" maxlength="3" style="width:100px" class='form-control' placeholder='Code' />
                    </td>
                  </tr>
              <?php }else{ ?>

                 <tr style="display: none;">
                    <td>CODE</td>
                    <td><input type="hidden" name="code" value="<?=(isset($code))?$code:""?>" id="f_code" maxlength="3" style="width:100px" class='form-control' placeholder='Code' />
                    </td>
                  </tr>
			  <?php } ?>
              <tr>
                <td>&nbsp;</td>
                <td>
                    <button type="submit" class='btn btn-info'><span class="glyphicon glyphicon-log-in"></span> Login</button>
                    <?php /*?><span style="color:#FFF">{elapsed_time} |  {memory_usage}</span><?php */?>
                    
                    <div class="pull-right" style="margin-top:15px"><a href="#" onClick="$('#frm_login').slideUp('fast'); $('#frm-forgot-password').slideDown('fast'); return false;">Forgot Password</a></div>
                </td>
              </tr>
              <tr>
                <td colspan="2" valign="top"></td>
              </tr>
            </table>
        </form>
        
        <form action="<?=site_url("login_agent/forgot_password/".@$code)?>" method="post" <?=(isset($is_forgot_pwd))?'':'class="hidden-field"'?> id="frm-forgot-password">
            <h3>Forgot your password?</h3>

            <p>Enter your email and we will send you a link to get your password back.</p>
            <div class="show_error hidden-field" style="text-align:left"></div>
            <?php
            if(isset($scs_forgot) && @$scs_forgot=="SUCCESS"){
            ?>
            <br/>
            <div class="alert alert-info" style="margin:0">
                <strong>Successfully</strong><br />
                Guide to regain your password has been sent to email. Please check your email.
            </div>
            <br/>
            <?php } ?>
            <?php if (isset($err_forgot) && is_array($err_forgot) && count($err_forgot) > 0 && isset($_POST['email'])){ ?>
            <br>
            <div class="alert alert-danger text-left">
                <strong>Sorry..</strong><br /><br />
                <ul style="padding-left:20px">
                    <li><?=implode("</li><li>", $err_forgot)?></li>
                </ul>
            </div>
            <br/>
            <?php } ?>
            <div style="margin:auto" class="table-field">
                <div class="row">
                    
                    <div class="col-xs-3">Code</div>
                    <div class="col-xs-8">
                       
                        <input required name="code_vendor" value="<?=(isset($code))?$code:""?>" id="f_code" maxlength="3" style="width:100px" class='form-control' placeholder='Code' />
                        <br />
                    </div>
                    <div class="col-xs-1"></div>
                </div>
                <div class="row tr-field">
                    <div class="col-xs-3">Email</div>
                    <div class="col-xs-8 td-field right">
                        <input class="form-control input-sm" type="email" name="email" placeholder="Type your email..." />
                        <br />
                        <button type="submit" class="btn btn-info">Submit</button>
                        <button type="button" class="btn btn-danger" onClick="$('#frm_login').slideDown('fast'); $('#frm-forgot-password').slideUp('fast');">Back</button>
                    </div>
                    <div class="col-xs-1"></div>
                </div>
                </div>
            </div>
        </form>
    </div>
   <!--  <script>
        $("#frm_login").submit(function(){
            var mydata = $(this).serialize();
            var form = $(this);
            $.ajax({
                type: "POST",
                url: form.attr("action")+"/ajax",
                data: mydata,
                beforeSend : function(){
                    form.find(".show_error_login").slideUp().html("");
                    //form.find("button[type=submit]").attr("disabled","disabled");
                },
                success: function(response, textStatus, xhr) {

                    var respon = JSON.parse(response);
                    if ($.trim(respon.status) == "SUCCESS"){
                        //form.find(".show_error_login").hide().html("<div class='alert alert-success'><strong>Success</strong>. Your password is successfully updated.</div>").slideDown("fast");
                        //form.find("button[type=submit]").removeAttr("disabled");
                        //form.find("input[type=password]").val("");
                        form.load();
                    }else{
                        var error_arr = respon.error_desc;
                        error_msg = error_arr.join('</li><li>');
                        form.find(".show_error_login").hide().html("<div class='alert alert-danger'><strong>Sorry...</strong><br /><br /><ul><li>"+error_msg+"</li></ul>.</div>").slideDown("fast");
                        form.find("button[type=submit]").removeAttr("disabled");
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    //form.find("button[type=submit]").removeAttr("disabled");
                }
            });
            return false;
        });
    </script> -->
    </div>
	<script> $(document).ready(function(e){$("#f_email").focus();}); </script>

  </body>
</html>

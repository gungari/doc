<?php if ($this->fn->isExtranetAutoLogin()){ ?>
  	<div style="position:fixed; top:0; width:100%; text-align:center; z-index:10000">
    	<div style="background:#FC0; width:200px; margin:auto; border-radius:0 0 5px 5px"><strong>AUTO LOGIN</strong></div>
    </div>
    <?php } ?>
	<div class="max-width main-container">
    	<?php /* <header>
			<div class="activate-icon-mobile-menu pull-left">
				<a href="" onClick="$.togleMenu();">
					<i class="fa fa-bars" aria-hidden="true"></i>
				</a>
			</div>
        	<div class="logo">
            	<div class="img">
            	<?php /* if ($vendor['logo'] != '') { ?>
            		<a href="<?=site_url()?>agent"><img src="<?=$vendor['logo']?>" width="auto" height="50px"></a>
            		
            	<?php }else{ ?>
	               <a href="<?=site_url("extranet/general_information")?>"><?=$vendor["business_name"]?></a>
	            <?php } */ /*?>
                    <div class="pull-right extranet-type">
                    	AGENT - <?=$vendor["business_name"]?>
                    </div>
                </div>
            </div>
            <div class="vendor-right">
            	<div class="vendor-name">
                	<a href="<?=site_url("extranet/general_information")?>"><?=$agent['name']?></a>
                    <?php if (@$vendor["f_status_online"] == "0"){ ?> &nbsp;&nbsp;&nbsp;<strong style="color:#F90">[SANDBOX]</strong> <?php } ?>
                </div>
                <div class="menus">
                	<div class="menu"><a href="<?=site_url("agent/logout")?>">Logout</a></div>

                    <?php /*?><div class="menu"><a class="fancyboxclose ajax" href="<?=site_url("extranet/home/support")?>">Support</a></div><?php */ /*?>
                </div>
            </div>
        </header> */ ?>
        
        <div class="container">
        	
            <!-- <div class="inside"> -->
          
				<?php /* <div class="menu-left hide-div-sm" id="menu-left">
					<ul class='menus'>
						<?php if ($_menu_link){ ?>
							<?php foreach ($_menu_link as $menu){ ?>
								<li class="left-menu <?=$menu["menu_class"]?>">
									<?php if($menu['link']!="reports"){ ?>
									<a <?=$menu["link_type"]?>="<?=$menu["link"]?>" onclick="$.togleMenu()"><?=$menu["icon"]?> <?=$menu["name"]?></a>
									<?php }else{ ?>
									<a <?=$menu["link_type"]?>="<?=$menu["link"]?>" onclick="$.togleMenu()" ui-sref-opts="{reload: true}"><?=$menu["icon"]?> <?=$menu["name"]?></a>
									<?php } ?>
									<?php if ($agent['payment_method']['payment_code'] == 'DEPOSIT') { ?>
										<a ui-sref='deposit' onclick="$.togleMenu()"><span class="fa fa-money"></span></span></span> Deposit</a>
									<?php } ?>
								</li>
							<?php } ?>
						<?php } ?>
					</ul>
				</div> */ ?>

				<!-- NAVBAR SESUAI BES -->
				<div>
					<nav class="navbar navbar-default">
					  <div class="container-fluid">
					    <!-- Collect the nav links, forms, and other content for toggling -->
					    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					      <ul class="nav nav-pills navbar-left" style="padding-top: 5px;">
					        <?php if ($_menu_link){ ?>
							<?php foreach ($_menu_link as $menu){ ?>
								<li class="nav-item act <?=$menu["menu_class"]?>">
									<?php if($menu['link']!="reports"){ ?>
									<a <?=$menu["link_type"]?>="<?=$menu["link"]?>" onclick="$.togleMenu()"><?=$menu["icon"]?> <?=$menu["name"]?></a>
									<?php }else{ ?>
									<a <?=$menu["link_type"]?>="<?=$menu["link"]?>" onclick="$.togleMenu()" ui-sref-opts="{reload: true}"><?=$menu["icon"]?> <?=$menu["name"]?></a>
									<?php } ?>
									<?php if ($agent['payment_method']['payment_code'] == 'DEPOSIT') { ?>
										<a ui-sref='deposit' onclick="$.togleMenu()"><span class="fa fa-money"></span></span></span> Deposit</a>
									<?php } ?>
								</li>
							<?php } ?>
						<?php } ?>
					      </ul>
					      <ul class="nav nav-pills navbar-right" style="margin-top: 14px;font-size: 15px;">
					        <li>
					        	<p>
					        		<strong> <a href="<?=site_url("agent/inside#/profile")?>"><?=$agent['name']?></a></strong>   &nbsp;&nbsp;&nbsp;, &nbsp;<a href="<?=site_url("agent/logout")?>">Logout</a>
					        	</p>
					        </li>
					      </ul>
					    </div><!-- /.navbar-collapse -->
					  </div><!-- /.container-fluid -->
					</nav>
				</div>

				<!-- <div class="content-right show-div-sm" id="content-right"> -->
				<div class="content" id="content" style="min-height: 500px;background: #FFF;float: left;width: 100%;box-shadow: 0 0 0 #999999;border-top: solid 3px #F5F5F5;">
					<div ui-view style="padding: 30px 30px;"></div>
				</div>
			

        </div>
        <footer style="clear:both">
			<?php if(isset($_SESSION["ss_login_CRS_agent"]["credential"]["user"])){ ?>
				<div align="center" style="padding-top:50px">
					Login As : <strong><?=$_SESSION["ss_login_CRS_agent"]["credential"]["user"]["name"];?></strong> - <?=$_SESSION["ss_login_CRS_agent"]["credential"]["user"]["access_role"]["name"];?>
				</div>
			<?php }else{ ?>
				&nbsp;&nbsp;
			<?php } ?>
			
			<div class="text-center">
				{elapsed_time} |  {memory_usage} 
			</div>
		
        </footer>
        
    </div>
	
	
	<?php //================================================== ?>
	
	<?php $this->load->view("template/crs/_include_js_css_agent"); ?>
	<?php $this->load->view("template/crs/_include_ng_app_agent"); ?>
	
	<?php //================================================== ?>
   <script type="text/javascript">
   		$(".nav-item").on('click', function(){
	    $(this).siblings().removeClass('active')
	    $(this).addClass('active');
	})
   </script>
   <script>
   	$(document).ready(function(e) {
		$.togleMenu = function(){
			if ($(".content .inside .menu-left.hide-div-sm").length > 0){
				$(".content .inside .menu-left.hide-div-sm").removeClass("hide-div-sm").addClass("show-div-sm");
			}else if ($(".content .inside .menu-left.show-div-sm").length > 0){
				$(".content .inside .menu-left.show-div-sm").removeClass("show-div-sm").addClass("hide-div-sm");
			}
			
			if ($(".content .inside .content-right.hide-div-sm").length > 0){
				$(".content .inside .content-right.hide-div-sm").removeClass("hide-div-sm").addClass("show-div-sm");
			}else if ($(".content .inside .content-right.show-div-sm").length > 0){
				$(".content .inside .content-right.show-div-sm").removeClass("show-div-sm").addClass("hide-div-sm");
			}
			//$(".content .inside .menu-left").toggleClass("show-div-sm", "hide-div-sm");
			//$(".content .inside .content-right").toggleClass("hide-div-sm", "show-div-sm");
			return false;
		}
	});
   </script>
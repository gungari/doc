
<?php $ss_vendor = $this->fn->get_ss_CRS("vendor"); ?>

<script type="text/javascript" src="<?=base_url("app/general.js?".time())?>"></script> 
<script type="text/javascript" src="<?=base_url("app/angular.min.js")?>"></script> 
<script type="text/javascript" src="<?=base_url("app/plugin/angular-ui-router.min.js")?>"></script>
<script type="text/javascript" src="<?=base_url("app/plugin/ocLazyLoad/dist/ocLazyLoad.min.js")?>"></script> 
<script type="text/javascript" src="<?=base_url("app/plugin/angular-webstorage.js")?>"></script>
<script type="text/javascript" src="<?=base_url("app/plugin/angular-cookies.js")?>"></script>
<script type="text/javascript" src="<?=base_url("app/plugin/angular-base64-upload.js")?>"></script>
<script type="text/javascript" src="<?=base_url("app/plugin/angular-img-cropper.js")?>"></script>
<script type="text/javascript" src="<?=base_url("app/app/app.js")?>"></script> 

<?php if (isset($print_template) && $print_template){ ?>
<script type="text/javascript" src="<?=base_url("app/app/app.route.print.js?v=1?".time())?>"></script>
<?php }elseif (isset($email_template) && $email_template){ ?>
<script type="text/javascript" src="<?=base_url("app/app/app.route.email.js?v=1?".time())?>"></script>
<?php }else{ ?>
	
	<script type="text/javascript" src="<?=base_url("app/app/app.route.js?v=1".time())?>"></script>
	
	<?php if ($this->fn->isVendorCRS_ACT($ss_vendor)){ ?>
		<script type="text/javascript" src="<?=base_url("app/app/app.route.act.js?v=1".time())?>"></script>
    <?php } ?>
    
	<?php if ($this->fn->isVendorCRS_TRANS($ss_vendor)){ ?>
		<script type="text/javascript" src="<?=base_url("app/app/app.route.trans.js?v=1".time())?>"></script>
    <?php } ?>
<?php } ?>

<script type="text/javascript" src="<?=base_url("app/services/httpSrv.js?".time())?>"></script>
<script type="text/javascript" src="<?=base_url("app/services/fn.js?".time())?>"></script>


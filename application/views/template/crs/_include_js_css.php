<style type="text/css">
	.pagination > li > a, .pagination > li > span {
	   	background-color: white !important;
	   	border-color: rgba(0,0,0,0.19) !important; 
	    color: #7f8c8d !important;
	}
	a:focus, a:hover {
    	color: #1d504c !important;
	}
	a {
	    color: #7f8c8d;
	}
	.content .inside {
	    border-top: solid 8px #7f8c8d !important;
	}
	.panel-primary > .panel-heading {
	    color: #fff !important;
	    background-color: #7f8c8d !important;
	    border-color: #7f8c8d !important;
	}
	.panel-primary {
	    border-color: #7f8c8d !important;
	}
	.nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover {
	    color: #fff !important;
	    background-color: #7f8c8d !important;
	}
	.content .home .main-panel .panel:hover {
	    background: #1d504c !important;
	    text-decoration: none;
	}
	.content .home .main-panel .panel {
	    background: #7f8c8d !important;
	  
	}

</style>
<link href="<?=base_url()?>public/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet" xxmedia="screen">
<link href="<?=base_url()?>public/css/extranet.css" rel="stylesheet" xxxmedia="screen">
<link href="<?=base_url()?>public/css/prism.css" rel="stylesheet" xxxmedia="screen">
<link href="<?=base_url()?>public/admin_clean/style.css?v=1" rel="stylesheet" xxxmedia="screen">

<script src="<?=base_url()?>public/js/jquery/jquery-1.10.2.min.js"></script>
<script src="<?=base_url()?>public/plugin/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>public/js/myjs.js"></script>

<script type="text/javascript" src="<?=base_url()?>public/plugin/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>public/plugin/jquery-ui-1.10.3.custom/css/redmond/jquery-ui-1.10.3.custom.css">

<script type="text/javascript" src="<?=base_url()?>public/plugin/fancybox/source/jquery.fancybox.js?v=2.1.0"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>public/plugin/fancybox/source/jquery.fancybox.css?v=2.1.0" media="screen" />

<link href="<?=base_url("public/plugin/font-awesome/css/font-awesome.min.css")?>" rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" href="<?=base_url("public/plugin/toastr/toastr.min.css")?>"/>
<script type="text/javascript" src="<?=base_url("public/plugin/toastr/toastr.min.js")?>"></script>
<script type="text/javascript" src="<?=base_url("public/plugin/accounting.min.js")?>"></script>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/xlsx.js">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/jszip.js">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.10.8/xlsx.full.min.js">
</script>

<div class="inside"> 
	<div class="menu-left hide-div-sm" id="menu-left">
		<ul class='menus'>
			<?php if ($_menu_link){ ?>
				<?php foreach ($_menu_link as $menu){ ?>
					<li class="left-menu <?=$menu["menu_class"]?>">
						<?php if($menu['link']!="reports"){ ?>
						<a <?=$menu["link_type"]?>="<?=$menu["link"]?>" onclick="$.togleMenu()"><?=$menu["icon"]?> <?=$menu["name"]?></a>
						<?php }else{ ?>
						<a <?=$menu["link_type"]?>="<?=$menu["link"]?>" onclick="$.togleMenu()" ui-sref-opts="{reload: true}"><?=$menu["icon"]?> <?=$menu["name"]?></a>
						<?php } ?>
						
					</li>
				<?php } ?>
			<?php } ?>
		</ul>
	</div>
	<div class="content-right show-div-sm" id="content-right">
		<div ui-view></div>
	</div>
</div>

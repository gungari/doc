<!DOCTYPE html>
<html>
  <head>
  	<title><?=$vendor["business_name"]?> - CRS - <?=$this->my_config->company_name?></title>
	<!-- <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> -->
	<link rel="shortcut icon" href="<?=base_url("public/images/ico.png")?>" />
	<!-- <script>
		var BASE_URL = "<?=base_url()?>"; 
		var SITE_URL = "<?=site_url()?>";
		var VIEW_URL = "<?=base_url("application/views/")?>";
		var MC = "<?=$credential["merchant_code"]?>";
		var MK = "<?=$credential["merchant_key"]?>";

	</script> -->

  </head>
  <body ng-app='app'>
  	<?php if ($this->fn->isExtranetAutoLogin()){ ?>
  	<div style="position:fixed; top:0; width:100%; text-align:center; z-index:10000">
    	<div style="background:#FC0; width:200px; margin:auto; border-radius:0 0 5px 5px"><strong>AUTO LOGIN</strong></div>
    </div>
    <?php } ?>
	<div class="max-width main-container" style="padding: 0;">
    	<!-- <header>
			<div class="activate-icon-mobile-menu pull-left">
				<a href="" onClick="$.togleMenu();">
					<i class="fa fa-bars" aria-hidden="true"></i>
				</a>
			</div>
        	<div class="logo">
            	<div class="img">
	                <a href="<?=site_url()?>"><img src="<?=base_url("public/images/hybrid-png.png")?>" width="100%" /></a>
                    <div class="pull-right extranet-type">
                    	CRS - <?=strtoupper($vendor["category"])?>
                    </div>
                </div>
            </div>
            <div class="vendor-right">
            	<div class="vendor-name">
                	<a href="<?=site_url("extranet/general_information")?>"><?=$vendor["business_name"]?></a>
                    <?php if (@$vendor["f_status_online"] == "0"){ ?> &nbsp;&nbsp;&nbsp;<strong style="color:#F90">[SANDBOX]</strong> <?php } ?>
                </div>
                <div class="menus">
                	<div class="menu"><a href="<?=site_url("logout")?>">Logout</a></div>

                    <?php /*?><div class="menu"><a class="fancyboxclose ajax" href="<?=site_url("extranet/home/support")?>">Support</a></div><?php */?>
                </div>
            </div>
        </header -->
<div class="home">
	<div class="row main-panel">
	<?php if ($_menu_link){ ?>
				<?php foreach ($_menu_link as $menu){ ?>
		<div class="col-sm-4 col">
			<a  href="<?=site_url('home/inside')?>#<?=str_replace('.', '/', $menu["link"])?>"><div class="panel"><?=strtoupper($menu["name"]);?></div></a>
		</div>
		<?php } ?>
	<?php } ?>
					

			<?php /*?><li class="left-menu profile">
				<a ui-sref="profile"><span class="fa fa-user"></span> Profile</a>
			</li>
			<li class="left-menu trips_schedule">
				<a ui-sref="transport.trips_schedule"><span class="f */
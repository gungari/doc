<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><?=$vendor["business_name"]?> - CRS - <?=$this->my_config->company_name?></title>
		<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
		
		<script>
			var BASE_URL = "<?=base_url()?>"; 
			var SITE_URL = "<?=site_url()?>";
			var VIEW_URL = "<?=base_url("application/views/")?>";
			var MC = "<?=$credential["merchant_code"]?>";
			var MK = "<?=$credential["merchant_key"]?>";
		</script>
	</head>
	
	<body ng-app='app'>
		<div class="no-print text-center" id="btn-print-top">
			<br /><br />
			<button onclick="window.print()" ng-disabled="!show_print_button" type="button" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-print"></span> Print Now</button>
		</div>
			
		<div class="print_area">
			<div ui-view></div>
		</div>
	</body>
	
	<?php //================================================== ?>
	
	<?php $this->load->view("template/crs/_include_js_css"); ?>
	<?php $this->load->view("template/crs/_include_ng_app"); ?>
	
	<?php //================================================== ?>
	
	<style>
		.print_area{width:750px; margin:20px auto; font-size:12px; font-family:Arial, Helvetica, sans-serif; background:#FFF; padding:10px; min-height:500px}
		.print_area .header{padding-bottom:5px; margin-bottom:10px; border-bottom:solid 1px #DDD}
		.print_area .header .title{font-size:26px}
		.sub-title{background:#F2F2F2 !important; padding:5px; border:solid 1px #EEE; font-weight:bold; margin-bottom:5px}
		hr{border:none; border-top:solid 1px #DDD; margin:10px 0; padding:0}
		
		table.table{background:#CCC}
		table.table td{background:#FFF; vertical-align:top}
		table.table tr.table-header td{background:#F2F2F2 !important}
		
		table.print-table td{padding:3px}
		
		.print-toolbox{margin:20px 0 50px 0; background:#F3F3F3; padding:15px; border:solid 1px #DDD}
		
		@media print {
			.no-print{display:none;}
			body{background:none !important}
		}
	</style>
	
	<script>
		$(document).ready(function(e) {
			//window.print() ;
		});
	</script>
</html>
<!DOCTYPE html>
<html>
  <head>
  	<title><?=$vendor["business_name"]?> - CRS - <?=$this->my_config->company_name?></title>
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="shortcut icon" href="<?=base_url("public/images/ico.png")?>" />
	<script>
		var BASE_URL = "<?=base_url()?>"; 
		var SITE_URL = "<?=site_url()?>";
		var VIEW_URL = "<?=base_url("application/views/")?>";
		var MC = "<?=$credential["merchant_code"]?>";
		var MK = "<?=$credential["merchant_key"]?>";

	</script>

  </head>
  <body ng-app='app'>
  	<?php if ($this->fn->isExtranetAutoLogin()){ ?>
  	<div style="position:fixed; top:0; width:100%; text-align:center; z-index:10000">
    	<div style="background:#FC0; width:200px; margin:auto; border-radius:0 0 5px 5px"><strong>AUTO LOGIN</strong></div>
    </div>
    <?php } ?>
	<div class="max-width main-container">
    	<header>
			<div class="activate-icon-mobile-menu pull-left">
				<a href="" onClick="$.togleMenu();">
					<i class="fa fa-bars" aria-hidden="true"></i>
				</a>
			</div>
        	<div class="logo">
            	<div class="img">
	                <a href="<?=site_url()?>"><img src="<?=base_url("public/images/hybrid-png.png")?>" width="100%" /></a>
                    <div class="pull-right extranet-type">
                    	CRS - <?=@strtoupper($vendor["category"])?>
                    </div>
                </div>
            </div>
            <div class="vendor-right">
            	<div class="vendor-name">
                	<a href="<?=site_url("extranet/general_information")?>"><?=$vendor["business_name"]?></a>
                    <?php if (@$vendor["f_status_online"] == "0"){ ?> &nbsp;&nbsp;&nbsp;<strong style="color:#F90">[SANDBOX]</strong> <?php } ?>
                </div>
                <div class="menus">
                	<div class="menu">
                    	<?php if(isset($_SESSION["ss_login_CRS"]["credential"]["user"])){ ?>
                                Login As : <strong><?=$_SESSION["ss_login_CRS"]["credential"]["user"]["name"];?></strong> - <?=$_SESSION["ss_login_CRS"]["credential"]["user"]["access_role"]["name"];?>
                        <?php }else{ ?>
                                Login As : <strong><?=$_SESSION["ss_login_CRS"]["vendor"]["business_name"];?></strong>
                        <?php } ?>
                    	&nbsp;|&nbsp;
                    	<a href="<?=site_url("logout")?>">Logout</a>
                    </div>

                    <?php /*?><div class="menu"><a class="fancyboxclose ajax" href="<?=site_url("extranet/home/support")?>">Support</a></div><?php */?>
                </div>
            </div>
        </header>
        
        <div class="content">
        	
            <?=@$_content?>
			
		
        </div>
        <footer style="clear:both">
        	<div align="center" style="padding-top:50px">
			<?php if(isset($_SESSION["ss_login_CRS"]["credential"]["user"])){ ?>
					Login As : <strong><?=$_SESSION["ss_login_CRS"]["credential"]["user"]["name"];?></strong> - <?=$_SESSION["ss_login_CRS"]["credential"]["user"]["access_role"]["name"];?>
			<?php }else{ ?>
					Login As : <strong><?=$_SESSION["ss_login_CRS"]["vendor"]["business_name"];?></strong>
			<?php } ?>
			</div>
            
			<div class="text-center">
				{elapsed_time} |  {memory_usage} 
			</div>
		
        </footer>
        
    </div>
	
	
	<?php //================================================== ?>
	
	<?php $this->load->view("template/crs/_include_js_css"); ?>
	<?php $this->load->view("template/crs/_include_ng_app"); ?>
	
	<?php //================================================== ?>
   
   <script>
   	$(document).ready(function(e) {
		$.togleMenu = function(){
			if ($(".content .inside .menu-left.hide-div-sm").length > 0){
				$(".content .inside .menu-left.hide-div-sm").removeClass("hide-div-sm").addClass("show-div-sm");
			}else if ($(".content .inside .menu-left.show-div-sm").length > 0){
				$(".content .inside .menu-left.show-div-sm").removeClass("show-div-sm").addClass("hide-div-sm");
			}
			
			if ($(".content .inside .content-right.hide-div-sm").length > 0){
				$(".content .inside .content-right.hide-div-sm").removeClass("hide-div-sm").addClass("show-div-sm");
			}else if ($(".content .inside .content-right.show-div-sm").length > 0){
				$(".content .inside .content-right.show-div-sm").removeClass("show-div-sm").addClass("hide-div-sm");
			}
			//$(".content .inside .menu-left").toggleClass("show-div-sm", "hide-div-sm");
			//$(".content .inside .content-right").toggleClass("hide-div-sm", "show-div-sm");
			return false;
		}
	});
   </script>
   
    <?php //pre($_SESSION); ?>
  </body>
</html>


<style type="text/css">
	.pagination > li > a, .pagination > li > span {
	   
	    color: #951a4e !important;
	}
	a:focus, a:hover {
    	color: #6f1038 !important;
	}
	a {
	    color: #951a4e;
	}
	.content .inside {
	    border-top: solid 8px #951a4e !important;
	}
	.panel-primary > .panel-heading {
	    color: #fff !important;
	    background-color: #951a4e !important;
	    border-color: #951a4e !important;
	}
	.panel-primary {
	    border-color: #951a4e !important;
	}
	.nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover {
	    color: #fff !important;
	    background-color: #951a4e !important;
	}
	.content .home .main-panel .panel:hover {
	    background: #6f1038 !important;
	    text-decoration: none;
	}
	.content .home .main-panel .panel {
	    background: #951a4e !important;
	  
	}
</style>

<link href="<?=base_url()?>public/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet" xxmedia="screen">
<link href="<?=base_url()?>public/css/extranet.css" rel="stylesheet" xxxmedia="screen">
<link href="<?=base_url()?>public/admin_clean/style.css?v=1" rel="stylesheet" xxxmedia="screen">

<script src="<?=base_url()?>public/js/jquery/jquery-1.10.2.min.js"></script>
<script src="<?=base_url()?>public/plugin/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>public/js/myjs.js"></script>

<script type="text/javascript" src="<?=base_url()?>public/plugin/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>public/plugin/jquery-ui-1.10.3.custom/css/redmond/jquery-ui-1.10.3.custom.css">

<script type="text/javascript" src="<?=base_url()?>public/plugin/fancybox/source/jquery.fancybox.js?v=2.1.0"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>public/plugin/fancybox/source/jquery.fancybox.css?v=2.1.0" media="screen" />

<link href="<?=base_url("public/plugin/font-awesome/css/font-awesome.min.css")?>" rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" href="<?=base_url("public/css/booking_process.css")?>"/>
<link rel="stylesheet" type="text/css" href="<?=base_url("public/plugin/toastr/toastr.min.css")?>"/>
<script type="text/javascript" src="<?=base_url("public/plugin/toastr/toastr.min.js")?>"></script>
<script type="text/javascript" src="<?=base_url("public/plugin/accounting.min.js")?>"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<link href="<?=base_url("public/css/print.min.css")?>" rel="stylesheet" type="text/css" /> 
<div class="inside"> 
	<div class="menu-left hide-div-sm" id="menu-left">
		<ul class='menus'>
			<?php if ($_menu_link){ ?>
				<?php foreach ($_menu_link as $menu){ ?>
					<li class="left-menu <?=$menu["menu_class"]?>">
						<?php if($menu['link']!="reports"){ ?>
						<a <?=$menu["link_type"]?>="<?=$menu["link"]?>" onclick="$.togleMenu()"><?=$menu["icon"]?> <?=$menu["name"]?></a>
						<?php }else{ ?>
						<a <?=$menu["link_type"]?>="<?=$menu["link"]?>" onclick="$.togleMenu()" ui-sref-opts="{reload: true}"><?=$menu["icon"]?> <?=$menu["name"]?></a>
						<?php } ?>
					</li>
				<?php } ?>
			<?php } ?>
            
            <li class="left-menu logout visible-xs-block">
            	<a href="<?=site_url("logout")?>"><span class="fa fa-sign-out"></span> Logout</a>
			</li>

			<?php /*?><li class="left-menu profile">
				<a ui-sref="profile"><span class="fa fa-user"></span> Profile</a>
			</li>
			<li class="left-menu trips_schedule">
				<a ui-sref="transport.trips_schedule"><span class="fa fa-th-large"></span>Trips &amp; Schedules</a>
			</li>
			<li class="left-menu agent">
				<a ui-sref="agent"><span class="fa fa-users" aria-hidden="true"></span>Business Source</a>
			</li>
			
			<li class="left-menu reservation">
				<a ui-sref="trans_reservation"><span class="fa fa-book"></span> Reservation</a>
			</li>
			
			<li class="left-menu open_voucher">
				<a ui-sref="open_voucher"><span class="fa fa-ticket"></span> Open Voucher</a>
			</li>
			
			<li class="left-menu transaction">
				<a ui-sref="trans_transaction"><span class="fa fa-book"></span> Transaction</a>
			</li>
			
			<li class="left-menu checkin"> 
				<a ui-sref="checkin"><span class="fa fa fa-sign-in"></span> Check In</a> 
			</li>
			
			<li class="left-menu arrival_departure"> 
				<a ui-sref="trans_arrival_departure"><span class="fa fa fa-paper-plane"></span> Arrival/Departure</a> 
			</li>
			
			<li class="left-menu reports"> <a href="<?=site_url("home/inside/")?>#"><span class="fa fa-pie-chart"></span> Reports</a> </li>
			<li class="left-menu setting"> <a href="<?=site_url("home/inside/")?>#"><span class="fa fa-cog"></span> Settings </a> </li><?php */?>
			
			<?php /*?><li class="left-menu trips_calendar">
				<a ui-sref="transport.trips_calendar"><span class="fa fa-calendar"></span>Trips Calendar</a> 
			</li><?php */?>
			
			
			<?php /*?><li class="left-menu discount_rules"> <a href="<?=site_url("home/inside/")?>#discount_rules"><span class="fa fa-qrcode"></span> Discount Rules</a> </li>
			<li class="left-menu transaction"> <a href="<?=site_url("home/inside/")?>#"><span class="fa fa-dollar"></span> Transactions &amp; Withdrawal</a> </li>
			<li class="left-menu custom-booking"> <a href="<?=site_url("home/inside/")?>#"><span class="fa fa-th-large"></span> Quote Generator</a> </li>
			<li class="left-menu customer-review"> <a href="<?=site_url("home/inside/")?>#"><span class="fa fa-comment"></span> Customer Feedback</a> </li>
			<li class="left-menu e-marketing"> <a href="<?=site_url("home/inside/")?>#"><span class="fa fa-envelope-o"></span> E-Marketing</a> </li><?php */?>
		</ul>
	</div>
	<div class="content-right show-div-sm" id="content-right">
		<div ui-view></div>
	</div>
</div>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><?=$vendor["business_name"]?> - CRS - <?=$this->my_config->company_name?></title>
		<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
		
		<script>
			var BASE_URL = "<?=base_url()?>"; 
			var SITE_URL = "<?=site_url()?>";
			var VIEW_URL = "<?=base_url("application/views/")?>";
			var MC = "<?=$credential["merchant_code"]?>";
			var MK = "<?=$credential["merchant_key"]?>";
		</script>
		<style type="text/css">
		            body, html
		            {
		                margin: 0; padding: 0; height: 100%; overflow: hidden;
		            }

		            div
		            {
		                position:absolute; left: 0; right: 0; bottom: 0; top: 0px; 
		            }
		        </style>
	</head>
	
	<body ng-app='app'>
		<div class="email_area">
			<div ui-view id="content"></div>
		</div>
	</body>
	
	<?php //================================================== ?>
	
	<?php $this->load->view("template/crs/_include_js_css"); ?>
	<?php $this->load->view("template/crs/_include_ng_app"); ?>
	
	<?php //================================================== ?>

</html>
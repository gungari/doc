	<div class="max-width main-container">
    	<header>
			<div class="activate-icon-mobile-menu pull-left">
				<a href="" onClick="$.togleMenu();">
					<i class="fa fa-bars" aria-hidden="true"></i>
				</a>
			</div>
        	<div class="logo">
            	<div class="img">
            	<?php /* if ($vendor['logo'] != '') { ?>
            		<a href="<?=site_url()?>agent"><img src="<?=$vendor['logo']?>" width="auto" height="50px"></a>
            		
            	<?php }else{ ?>
	               <a href="<?=site_url("extranet/general_information")?>"><?=$vendor["business_name"]?></a>
	            <?php } */?>
                    <div class="pull-right extranet-type">
                    	AGENT - <?=$vendor["business_name"]?>
                    </div>
                </div>
            </div>
            <div class="vendor-right">
            	<div class="vendor-name">
                	<a href="<?=site_url("extranet/general_information")?>"><?=$agent['name']?></a>
                    <?php if (@$vendor["f_status_online"] == "0"){ ?> &nbsp;&nbsp;&nbsp;<strong style="color:#F90">[SANDBOX]</strong> <?php } ?>
                </div>
                <div class="menus">
                	<div class="menu"><a href="<?=site_url("agent/logout")?>">Logout</a></div>

                    <?php /*?><div class="menu"><a class="fancyboxclose ajax" href="<?=site_url("extranet/home/support")?>">Support</a></div><?php */?>
                </div>
            </div>
        </header>
        
        <div class="content">
        	
            <div class="home">
				<div class="row main-panel">
				<?php if ($_menu_link){ ?>
							<?php foreach ($_menu_link as $menu){ ?>
					<div class="col-sm-4 col">
						<a  href="<?=site_url('agent/inside')?>#<?=$menu["link"]?>"><div class="panel"><?=strtoupper($menu["name"]);?></div></a>
					</div>
					<?php } ?>
				<?php } ?>
				<div class="col-sm-4 col">
					<a  href="<?=site_url('agent/inside')?>#reports"><div class="panel">REPORT</div></a>
				</div>
				</div>
			</div>

        </div>
        <footer style="clear:both">
			<?php if(isset($_SESSION["ss_login_CRS_agent"]["credential"]["user"])){ ?>
				<div align="center" style="padding-top:50px">
					Login As : <strong><?=$_SESSION["ss_login_CRS_agent"]["credential"]["user"]["name"];?></strong> - <?=$_SESSION["ss_login_CRS_agent"]["credential"]["user"]["access_role"]["name"];?>
				</div>
			<?php }else{ ?>
				&nbsp;&nbsp;
			<?php } ?>
			
			<div class="text-center">
				{elapsed_time} |  {memory_usage} 
			</div>
		
        </footer>
        
    </div>
	
	
	<?php //================================================== ?>
	
	<?php //$this->load->view("template/crs/_include_js_css_agent"); ?>
	<?php //$this->load->view("template/crs/_include_ng_app_agent"); ?>
	
	<?php //================================================== ?>
   
   <script>
   	$(document).ready(function(e) {
		$.togleMenu = function(){
			if ($(".content .inside .menu-left.hide-div-sm").length > 0){
				$(".content .inside .menu-left.hide-div-sm").removeClass("hide-div-sm").addClass("show-div-sm");
			}else if ($(".content .inside .menu-left.show-div-sm").length > 0){
				$(".content .inside .menu-left.show-div-sm").removeClass("show-div-sm").addClass("hide-div-sm");
			}
			
			if ($(".content .inside .content-right.hide-div-sm").length > 0){
				$(".content .inside .content-right.hide-div-sm").removeClass("hide-div-sm").addClass("show-div-sm");
			}else if ($(".content .inside .content-right.show-div-sm").length > 0){
				$(".content .inside .content-right.show-div-sm").removeClass("show-div-sm").addClass("hide-div-sm");
			}
			//$(".content .inside .menu-left").toggleClass("show-div-sm", "hide-div-sm");
			//$(".content .inside .content-right").toggleClass("hide-div-sm", "show-div-sm");
			return false;
		}
	});
   </script>

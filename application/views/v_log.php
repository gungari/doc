<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>API LOG</title>

</head>

<body>
	<h2 align="center">API LOG</h2>
	<table class="table table-bordered table-hover table-condensed">
		<tr class="success" style="font-weight:bold">
			<td>ID</td>
			<td>Vendor Code</td>
			<td>URL</td>
			<td>Format</td>
			<td>Time</td>
			<td>IP Address</td>
			<td>Status</td>
			<td>Date Time</td>
			<td></td>
		</tr>
		<?php foreach ($logs as $log){ ?>
		<tr class="<?=$log["f_status"]=="ERROR"?"danger":""?>">
			<td>
				<a href="#" onclick="$('#param_<?=$log["f_id"]?>').toggle();$('#output_<?=$log["f_id"]?>').toggle();return false;">
				<?=$log["f_id"]?>
				</a>
			</td>
			<td align="center"><?=$log["f_code_vendor"]?></td>
			<td>/<?=$log["f_url"]?></td>
			<td align="center"><?=$log["f_output_format"]?></td>
			<td><?=$log["f_rtime"]?></td>
			<td><?=$log["f_ip_addess"]?></td>
			<td align="center"><?=$log["f_status"]?></td>
			<td><?=$log["f_timestamp"]?></td>
			<td align="center">
				<a href="#" onclick="$('#param_<?=$log["f_id"]?>').toggle();$('#output_<?=$log["f_id"]?>').toggle();return false;">Detail</a>
			</td>
		</tr>
		<tr class="<?=$log["f_status"]=="ERROR"?"danger":""?>" style="display:none" id="output_<?=$log["f_id"]?>">
			<td></td>
			<td colspan="8">
				<strong>OUTPUT :</strong><br />
				<?=pre(json_decode($log["f_output"]))?>
			</td>
		</tr>
		<tr class="<?=$log["f_status"]=="ERROR"?"danger":""?>" style="display:none" id="param_<?=$log["f_id"]?>">
			<td></td>
			<td colspan="8">
				<strong>PARAMETER : </strong><br />
				<?=pre(json_decode($log["f_params"]))?>
			</td>
		</tr>
		<?php } ?>
	</table>
	
	<link rel="stylesheet" href="<?="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"?>" />
	<script src="<?="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"?>"></script>
	<script src="<?="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"?>"></script>

	
	<?php //pre($logs); ?>
</body>
</html>
<?php
require APPPATH . '/libraries/CRS_Controller.php';

class Login_agent extends CRS_Controller {
	public function __construct(){
		parent::__construct();
	}
	public $code_vendor = '';
	public function index($code = ''){
		
		if ($this->fn->get_ss_CRS_agent()){
			redirect ("agent/home");
		}else{
			 $data = array();
			if($code!=""){
				$this->code_vendor = $code;
				/* GET API KEY*/
				$this->load->library("hybrid_api");
				if(isset($_GET['demo'])){
					$key = $this->hybrid_api->getData("login_agent/getapikey/".$code."?demo=1");
				}else{
					$key = $this->hybrid_api->getData("login_agent/getapikey/".$code);
				}
				

				if ($key['status'] == 'SUCCESS') {
					$data = $key['vendor'];
					$this->code_vendor = $key['vendor']['code'];
				}
			}
			$code_vendor = $this->code_vendor;
			
			//default
			// $this->load->view("template/crs/_login_agent.php", $data);

			//layout sesuai template vendor
			$this->template->agent_layout($code_vendor, "template/crs/v_login_agent", $data);
		}
	}
	
	public function checklogin($is_ajax=""){
		
		if ($this->input->post("email")){
			
			$this->load->library("hybrid_api");
			$this->code_vendor = $this->input->post("code");
			$hybrid_api = $this->hybrid_api->getData("login_agent/check_login_agent", $_POST);
			
			if ($hybrid_api["status"] == "SUCCESS"){
				
				
				$ss = array("vendor"	=>$hybrid_api["vendor"], 
							"agent" 	=>$hybrid_api["agent"]);


				
				$this->fn->set_ss_CRS_agent($ss);
				//echo json_encode($hybrid_api);
				// redirect("agent");
				redirect("agent/inside#/rsv");
				
			}else{
				$data["err_login"] = $hybrid_api["error_desc"];
				redirect("agent/".$this->code_vendor);
				//echo json_encode($hybrid_api);
				//pre_exit($data['err_login']);
				
				//$this->load->view("template/crs/_login_agent.php", $data);
			}
		}else{
			redirect("agent/".$this->code_vendor);
		}
	}
	
	public function autologin(){
		
		$this->load->library("hybrid_api");
		
		$data_send["md5_id"] 	= $_GET['s']['id'];
		// $data_send["md5_email"] = $_GET['s']['email'];
		$data_send["code"] 	= $_GET['s']['code'];
		
		$hybrid_api = $this->hybrid_api->getData("login_agent/autologin", $data_send);
		//pre_exit($hybrid_api);
		if ($hybrid_api["status"] == "SUCCESS"){
			
			
				$ss = array("vendor"	=>$hybrid_api["vendor"], 
							"agent" 	=>$hybrid_api["agent"]);
				
				$this->fn->set_ss_CRS_agent($ss);
			
			// redirect("agent/home");
			redirect("agent/inside#/rsv");
			
		}else{
			$data["err_login"] = $hybrid_api["error_desc"];
			$this->load->view("template/crs/_login_agent.php", $data);
		}

	}

	public function logout(){
		
		$this->code_vendor = $code = $_SESSION["ss_login_CRS_agent"]['vendor']['code'];
		
		unset($_SESSION["ss_login_CRS_agent"]);
		redirect("agent/".$this->code_vendor);
		
	}

	
	public function home(){
		// $this->template->home_agent('template/crs/_ng_home_agent');
		$this->template->home_agent('template/crs/v_home_agent');

	}

	public function inside(){
		// $this->template->home_agent('template/crs/_ng_inside_agent');
		$this->template->inside_agent('template/crs/v_inside_agent');

	}

	public function forgot_password($code = ""){
		$this->load->library("hybrid_api");
		
		$hybrid_api = $this->hybrid_api->getData("login_agent/check_email_forgot_pwd", $_POST);
		
		if($code!=""){
			$this->code_vendor = $code;
			/* GET API KEY*/
			$this->load->library("hybrid_api");
			$key = $this->hybrid_api->getData("login_agent/getapikey/".$code);

			if ($key['status'] == 'SUCCESS') {
				$data = $key['vendor'];
				
				$this->code_vendor = $key['vendor']['code'];
			}
		}else{
			$code = $_POST['code_vendor'];

			$this->load->library("hybrid_api");
			$key = $this->hybrid_api->getData("login_agent/getapikey/".$code);

			if ($key['status'] == 'SUCCESS') {
				$data = $key['vendor'];

				$this->code_vendor = $key['vendor']['code'];
			}
		}

		if($hybrid_api['status']=="ERROR"){
			$data['err_forgot'] = $hybrid_api['error_desc'];
		}else{
			$data['scs_forgot'] = "SUCCESS";
		}
		$data['is_forgot_pwd'] = true;
		$this->load->view("template/crs/_login_agent.php",$data);
	}

	public function recover_password($md5_code_vendor="", $code_verification="",$md5_code_agent=""){
		$this->load->library("hybrid_api");
		$post['code_verification'] = $code_verification;
		$post['vendor_code_verify'] = md5($md5_code_vendor);
		$post['agent_code_verify'] = $md5_code_agent;
		$hybrid_api = $this->hybrid_api->getData("login_agent/check_ferivication_code", $post);

		$this->load->library("hybrid_api");
		$key = $this->hybrid_api->getData("login_agent/getapikey/".$md5_code_vendor);
		$agent = $this->hybrid_api->getData("login_agent/getAgentmd5/".$md5_code_agent);

		if ($key['status'] == 'SUCCESS') {
			$data = $key['vendor'];
			$data['agent'] = $agent;
			$this->code_vendor = $key['vendor']['code'];
		}

		if($hybrid_api['status']=="SUCCESS"){
			$data+=$hybrid_api;
			$data['vendor_code_verify'] = $md5_code_vendor;
			$data['agent_code_verify'] = $md5_code_agent;
			$data['recover_password'] = true;
			$data['agent_email'] = $hybrid_api['email'];
			$this->load->view("template/crs/_login_agent.php",$data);
		}else{
			redirect ("agent/$md5_code_vendor");
		}
	}

	public function update_recover_password($is_ajax=""){
		$this->load->library("hybrid_api");
		$hybrid_api = $this->hybrid_api->getData("login_agent/recover_password", $_POST);
		
		echo json_encode($hybrid_api);
	}

}

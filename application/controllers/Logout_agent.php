<?php
require APPPATH . '/libraries/CRS_Controller.php';

class Logout_agent extends CRS_Controller {
	public function __construct(){
		parent::__construct();
	}
	
	public function index($code){
		unset($_SESSION["ss_login_CRS_agent"]);
		redirect("agent/".$code);
	}	
}

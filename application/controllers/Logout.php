<?php
require APPPATH . '/libraries/CRS_Controller.php';

class Logout extends CRS_Controller {
	public function __construct(){
		parent::__construct();
	}
	
	public function index(){
		unset($_SESSION["ss_login_CRS"]);
		redirect("login");
	}	
}

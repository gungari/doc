<?php
require APPPATH . '/libraries/CRS_Controller.php';

class Home extends CRS_Controller {
	public function __construct(){
		parent::__construct();
	}
	
	
	public function index(){
		
		//redirect ("home/inside");
		//$this->template->home('welcome_message');
		$this->template->home('template/crs/_ng_inside_');


	}
	
	public function inside(){
		$this->template->home('template/crs/_ng_inside');

	}

	public function send_email_profoma($invoice_code){
		$this->load->library("hybrid_api");

		$hybrid_api = $this->hybrid_api->getData("invoice/send_email/".$invoice_code);

		pre_exit($hybrid_api);
	}

	
	public function print_page(){
		
		$this->template->print_page();

	}

	public function resend_email(){
		
		$this->template->resend_email();

	}
	public function forgot_password(){
		$this->load->library("hybrid_api");
		$hybrid_api = $this->hybrid_api->getData("login/check_email_forgot_pwd", $_POST);
		if($hybrid_api['status']=="ERROR"){
			$data['err_forgot'] = $hybrid_api['error_desc'];
		}else{
			$data['scs_forgot'] = "SUCCESS";
		}
		
		$data['is_forgot_pwd'] = true;
		$this->load->view("template/crs/_login.php",$data);
	}
	
	public function recover_password($code_verification="",$md5_code_vendor=""){
		$this->load->library("hybrid_api");
		$post['code_verification'] = $code_verification;
		$post['vendor_code_verify'] = $md5_code_vendor;
		$hybrid_api = $this->hybrid_api->getData("login/check_ferivication_code", $post);
		if($hybrid_api['status']=="SUCCESS"){
			$data=$hybrid_api;
			$data['vendor_code_verify'] = $md5_code_vendor;
			$data['recover_password'] = true;
			$this->load->view("template/crs/_login.php",$data);
		}else{
			redirect ("home");
		}
	}
	public function update_recover_password($is_ajax=""){
		$this->load->library("hybrid_api");
		$hybrid_api = $this->hybrid_api->getData("login/recover_password", $_POST);
		
		echo json_encode($hybrid_api);
	}
	
	public function test_jurnal(){

		$curl = curl_init();
		
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://sandbox-api.jurnal.id/core/api/v1/terms/23",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_POSTFIELDS => "{}",
		  CURLOPT_HTTPHEADER => array(
			"apikey: fb7901698319ec0981f5b93876caaf09",
			"content-type: application/json"
		  ),
		));
		
		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);
		
		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  $str_json = $response;
		  pre(json_decode($str_json,true));
		}
	}
}

<?php
require APPPATH . '/libraries/CRS_Controller.php';

class Home extends CRS_Controller {
	public function __construct(){
		parent::__construct();
	}
	
	
	public function index(){
		
		redirect ("home/inside");
		//$this->template->home('welcome_message');
		//$this->template->home('template/crs/_ng_inside');

	}
	
	public function inside(){
		$this->template->home('template/crs/_ng_inside');

	}
	
	public function print_page(){
		
		$this->template->print_page();

	}

	public function resend_email(){
		
		$this->template->resend_email();

	}
	public function forgot_password(){
		$this->load->library("hybrid_api");
		$hybrid_api = $this->hybrid_api->getData("login/check_email_forgot_pwd", $_POST);
		if($hybrid_api['status']=="ERROR"){
			$data['err_forgot'] = $hybrid_api['error_desc'];
		}else{
			$data['scs_forgot'] = "SUCCESS";
		}
		
		$data['is_forgot_pwd'] = true;
		$this->load->view("template/crs/_login.php",$data);
	}
	public function recover_password($code_verification="",$md5_code_vendor=""){
		$this->load->library("hybrid_api");
		$post['code_verification'] = $code_verification;
		$post['vendor_code_verify'] = $md5_code_vendor;
		$hybrid_api = $this->hybrid_api->getData("login/check_ferivication_code", $post);
		if($hybrid_api['status']=="SUCCESS"){
			$data=$hybrid_api;
			$data['vendor_code_verify'] = $md5_code_vendor;
			$data['recover_password'] = true;
			$this->load->view("template/crs/_login.php",$data);
		}else{
			redirect ("home");
		}
	}
	public function update_recover_password($is_ajax=""){
		$this->load->library("hybrid_api");
		$hybrid_api = $this->hybrid_api->getData("login/recover_password", $_POST);
		
		echo json_encode($hybrid_api);
	}
}

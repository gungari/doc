<?php
require APPPATH . '/libraries/CRS_Controller.php';

class Export_to_excel extends CRS_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->library("hybrid_api");
	}

	public function agent_trans_pickup_dropoff(){
		
		$vendor = $this->template->checkLogin();
				
		if ($vendor){
			$this->hybrid_api->merchant_code	= $vendor["credential"]["merchant_code"];
			$this->hybrid_api->api_key 			= $vendor["credential"]["merchant_key"];
			
			$send_data = $_GET["s"];
			
			$respon_api = $this->hybrid_api->getData("agent_controller/pickup_dropoff_information", $send_data);
			
			//pre($respon_api);
			//return;
			
			if ($respon_api["status"] == "SUCCESS"){

				$respon_api["vendor"] = $vendor["vendor"];
				
				$this->load->view("crs/export_to_excel/v_trans_pickup_dropoff", $respon_api);
				
			}else{
				$respon_api["vendor"] = $vendor["vendor"];
				
				$this->load->view("crs/export_to_excel/v_trans_pickup_dropoff", $respon_api);
			}
		}
	}

	public function agent_act_arrival_departure(){
		
		$vendor = $this->template->checkLogin();
				
		if ($vendor){
			$this->hybrid_api->merchant_code	= $vendor["credential"]["merchant_code"];
			$this->hybrid_api->api_key 			= $vendor["credential"]["merchant_key"];
			
			$send_data = $_GET["s"];
			
			$respon_api = $this->hybrid_api->getData("agent_controller/passenger_list", $send_data);
			
			//pre($respon_api);
			//return;
			
			if ($respon_api["status"] == "SUCCESS"){

				$respon_api["vendor"] = $vendor["vendor"];
				
				$this->load->view("crs/export_to_excel/v_act_arrival_departure", $respon_api);
				
			}
		}
	}

	public function report_expected(){
		$vendor = $this->template->checkLogin();
				
		if ($vendor){
			$this->hybrid_api->merchant_code	= $vendor["credential"]["merchant_code"];
			$this->hybrid_api->api_key 			= $vendor["credential"]["merchant_key"];
			
			$send_data = $_GET["s"];
			$send_data['finish_date'] = $_GET['s']['end_date'];
			
			$respon_api = $this->hybrid_api->getData("report/expected", $send_data);
			
			
			if ($respon_api["status"] == "SUCCESS"){

				$respon_api["vendor"] = $vendor["vendor"];
				
				$this->load->view("crs/export_to_excel/v_report_expected", $respon_api);
				
			}
		}
	}

	public function report_act_expected(){
		$vendor = $this->template->checkLogin();
				
		if ($vendor){
			$this->hybrid_api->merchant_code	= $vendor["credential"]["merchant_code"];
			$this->hybrid_api->api_key 			= $vendor["credential"]["merchant_key"];
			
			$send_data = $_GET["s"];
			$send_data['finish_date'] = $_GET['s']['end_date'];
			
			$respon_api = $this->hybrid_api->getData("report/act_expected", $send_data);
			//pre_exit($respon_api);
			
			if ($respon_api["status"] == "SUCCESS"){

				$respon_api["vendor"] = $vendor["vendor"];
				
				$this->load->view("crs/export_to_excel/v_report_expected", $respon_api);
				
			}
		}
	}

	public function report_act_forecast(){
		$vendor = $this->template->checkLogin();
				
		if ($vendor){
			$this->hybrid_api->merchant_code	= $vendor["credential"]["merchant_code"];
			$this->hybrid_api->api_key 			= $vendor["credential"]["merchant_key"];
			
			$send_data = array();
			
			if (isset($_GET["s"])){
				$send_data["year"] 		= $_GET["s"]["year"];
				$send_data["month"] 	= $_GET["s"]["month"];
				$send_data["product_code"] 	= $_GET["s"]["product_code"];
			}
			
			$respon_api = $this->hybrid_api->getData("report/forcast_act", $send_data);
			
				
			if ($respon_api["status"] == "SUCCESS"){

				$respon_api["vendor"] = $vendor["vendor"];
				//pre($respon_api);
				
				$this->load->view("crs/export_to_excel/v_report_act_forecast", $respon_api);
				
			}
		}
	}

	public function trans_pickup_dropoff(){
		
		$vendor = $this->template->checkLogin();
				
		if ($vendor){
			$this->hybrid_api->merchant_code	= $vendor["credential"]["merchant_code"];
			$this->hybrid_api->api_key 			= $vendor["credential"]["merchant_key"];
			
			$send_data = $_GET["s"];
			
			$respon_api = $this->hybrid_api->getData("transport/booking/pickup_dropoff_information", $send_data);
			
			//pre($respon_api);
			//return;
			
			if ($respon_api["status"] == "SUCCESS"){

				$respon_api["vendor"] = $vendor["vendor"];
				
				$this->load->view("crs/export_to_excel/v_trans_pickup_dropoff", $respon_api);
				
			}else{
				$respon_api["vendor"] = $vendor["vendor"];
				
				$this->load->view("crs/export_to_excel/v_trans_pickup_dropoff", $respon_api);
			}
		}
	}

	public function act_arrival_departure(){
		
		$vendor = $this->template->checkLogin();
				
		if ($vendor){
			$this->hybrid_api->merchant_code	= $vendor["credential"]["merchant_code"];
			$this->hybrid_api->api_key 			= $vendor["credential"]["merchant_key"];
			
			$send_data = $_GET["s"];
			
			$respon_api = $this->hybrid_api->getData("activities/passenger_list", $send_data);
			
			//pre($respon_api);
			//return;
			
			if ($respon_api["status"] == "SUCCESS"){

				$respon_api["vendor"] = $vendor["vendor"];
				
				$this->load->view("crs/export_to_excel/v_act_arrival_departure", $respon_api);
				
			}
		}
	}
	
	public function trans_bookings(){
		$vendor = $this->template->checkLogin();
				
		if ($vendor){
			$this->hybrid_api->merchant_code	= $vendor["credential"]["merchant_code"];
			$this->hybrid_api->api_key 			= $vendor["credential"]["merchant_key"];
			
			if (isset($_GET["s"])){
				$send_data["search"] = $_GET["s"];
			}else{
				$send_data["search"] = array("limit" => "all");
			}
			
			$respon_api = $this->hybrid_api->getData("transport/booking/booking_and_voucher_list", $send_data);
			
			//pre($respon_api);
			
			if ($respon_api["status"] == "SUCCESS"){
				
				$arr_data_bookings = array();
				foreach ($respon_api["bookings"] as $booking){
					foreach ($booking["detail"] as $index => $detail){
						$data_booking = $booking;
						$data_booking["detail"] = $detail;
						
						$arr_data_bookings[] = $data_booking;
					}
				}
				
				//if (isset($_GET["test"])) pre($arr_data_bookings);
				
				$respon_api["data_bookings"] = $arr_data_bookings;
				$respon_api["vendor"] = $vendor["vendor"];
				
				$this->load->view("crs/export_to_excel/v_trans_bookings", array("data"=>$respon_api));
				
			}
		}
	}

	public function report_forecast(){
		$vendor = $this->template->checkLogin();
				
		if ($vendor){
			$this->hybrid_api->merchant_code	= $vendor["credential"]["merchant_code"];
			$this->hybrid_api->api_key 			= $vendor["credential"]["merchant_key"];
			
			$send_data = array();
			
			if (isset($_GET["s"])){
				$send_data["year"] 		= $_GET["s"]["year"];
				$send_data["month"] 	= $_GET["s"]["month"];
				$send_data["boat_id"] 	= $_GET["s"]["boat_id"];
				
			}
			
			$respon_api = $this->hybrid_api->getData("report/forcast", $send_data);
			
			
			
			if ($respon_api["status"] == "SUCCESS"){

				$respon_api["vendor"] = $vendor["vendor"];
				//pre($respon_api);
				
				$this->load->view("crs/export_to_excel/v_report_forecast", $respon_api);
				
			}
		}
	}
	
	public function report_revenue(){
		$vendor = $this->template->checkLogin();
				
		if ($vendor){
			$this->hybrid_api->merchant_code	= $vendor["credential"]["merchant_code"];
			$this->hybrid_api->api_key 			= $vendor["credential"]["merchant_key"];
			
			$send_data = array();
			
			if (isset($_GET["s"])){
				$send_data["search"]["year"] 			= $_GET["s"]["year"];
				$send_data["search"]["month"] 			= $_GET["s"]["month"];
				$send_data["search"]["date"] 			= $_GET["s"]["date"];
				// $send_data["search"]["start_date"] 		= $_GET["s"]["start_date"];
				$send_data["search"]["view"] 			= $_GET["s"]["view"];
				$send_data["search"]["booking_source"] 	= @$_GET["s"]["booking_source"];
				$send_data["search"]["group_by_trip"] 	= "1";
			}
			
			$respon_api = $this->hybrid_api->getData("transport/report/revenue", $send_data);
			
			//pre($respon_api);
			
			if ($respon_api["status"] == "SUCCESS"){

				$respon_api["vendor"] = $vendor["vendor"];
				//pre($respon_api);
				
				$this->load->view("crs/export_to_excel/v_report_revenue", $respon_api);
				
			}
		}
	}
	
	public function bookings_open_voucher(){
		$vendor = $this->template->checkLogin();
				
		if ($vendor){
			$this->hybrid_api->merchant_code	= $vendor["credential"]["merchant_code"];
			$this->hybrid_api->api_key 			= $vendor["credential"]["merchant_key"];
			
			$send_data = array();
			
			$respon_api = $this->hybrid_api->getData("transport/open_voucher/booking", $send_data);
			
			//pre($respon_api);
			
			if ($respon_api["status"] == "SUCCESS"){

				$respon_api["vendor"] = $vendor["vendor"];
				
				$this->load->view("crs/export_to_excel/v_bookings_open_voucher", $respon_api);
				
			}
		}
	}
	
	public function trans_arrival_departure(){
		
		$vendor = $this->template->checkLogin();
				
		if ($vendor){
			$this->hybrid_api->merchant_code	= $vendor["credential"]["merchant_code"];
			$this->hybrid_api->api_key 			= $vendor["credential"]["merchant_key"];
			
			$send_data = $_GET["s"];
			
			$respon_api = $this->hybrid_api->getData("transport/passenger_list", $send_data);
			
			//pre($respon_api);
			//return;
			
			if ($respon_api["status"] == "SUCCESS"){

				$respon_api["vendor"] = $vendor["vendor"];
				
				$this->load->view("crs/export_to_excel/v_trans_arrival_departure", $respon_api);
				
			}
		}
	}

	public function sales(){
		
		$vendor = $this->template->checkLogin();
				
		if ($vendor){
			$this->hybrid_api->merchant_code	= $vendor["credential"]["merchant_code"];
			$this->hybrid_api->api_key 			= $vendor["credential"]["merchant_key"];
			
			$send_data = $_GET["s"];
			
			$respon_api = $this->hybrid_api->getData("report/sales", $send_data);
			
			
			//return;
			
			if ($respon_api["status"] == "SUCCESS"){

				$respon_api["vendor"] = $vendor["vendor"];
				
				$this->load->view("crs/export_to_excel/v_report_sales", $respon_api);
				
			}
		}
	}

	public function cancell(){
		
		$vendor = $this->template->checkLogin();
				
		if ($vendor){
			$this->hybrid_api->merchant_code	= $vendor["credential"]["merchant_code"];
			$this->hybrid_api->api_key 			= $vendor["credential"]["merchant_key"];
			
			$send_data = $_GET["s"];
			
			$respon_api = $this->hybrid_api->getData("transport/report/cancell", $send_data);
			
			
			//return;
			
			if ($respon_api["status"] == "SUCCESS"){

				$respon_api["vendor"] = $vendor["vendor"];
				
				$this->load->view("crs/export_to_excel/v_report_cancell", $respon_api);
				
			}
		}
	}

	public function check(){
		
		$vendor = $this->template->checkLogin();
				
		if ($vendor){
			$this->hybrid_api->merchant_code	= $vendor["credential"]["merchant_code"];
			$this->hybrid_api->api_key 			= $vendor["credential"]["merchant_key"];
			
			$send_data = $_GET["s"];
			
			$respon_api = $this->hybrid_api->getData("report/checkin", $send_data);
			//pre_exit($respon_api);
			
			if ($respon_api["status"] == "SUCCESS"){

				$respon_api["vendor"] = $vendor["vendor"];
				
				$this->load->view("crs/export_to_excel/v_report_check", $respon_api);
				
			}
		}
	}

	public function agent(){
		$vendor = $this->template->checkLogin();
				
		if ($vendor){
			$this->hybrid_api->merchant_code	= $vendor["credential"]["merchant_code"];
			$this->hybrid_api->api_key 			= $vendor["credential"]["merchant_key"];
			
			$send_data = $_GET["s"];
			
			$respon_api = $this->hybrid_api->getData("transport/report/agent", $send_data);
			//pre_exit($respon_api);
			
			if ($respon_api["status"] == "SUCCESS"){

				$respon_api["vendor"] = $vendor["vendor"];
				
				$this->load->view("crs/export_to_excel/v_report_agent", $respon_api);
				
			}
		}
	}
	
}

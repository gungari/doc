<?php
require APPPATH . '/libraries/CRS_Controller.php';

class Login extends CRS_Controller {
	public function __construct(){
		parent::__construct();
	}
	
	public function index(){
		if ($this->fn->get_ss_CRS()){
			redirect ("home");
		}else{
			$this->load->view("template/crs/_login.php");
		}
	}
	
	public function checklogin(){
		
		if ($this->input->post("email")){
			
			$this->load->library("hybrid_api");
			
			$hybrid_api = $this->hybrid_api->getData("login/check_login", $_POST);
			
			if ($hybrid_api["status"] == "SUCCESS"){
				
				$ss = array("credential"=>$hybrid_api["credential"],
							"vendor"	=>$hybrid_api["vendor"]);
				
				$this->fn->set_ss_CRS($ss);
				
				redirect("home");
				
			}else{
				$data["err_login"] = $hybrid_api["error_desc"];
				$this->load->view("template/crs/_login.php", $data);
			}
		}else{
			redirect("login");
		}
	}
	
	public function autologin($md5_id_vendor, $md5_email, $tk = ""){
		
		$this->load->library("hybrid_api");
		
		$data_send["encid"] 	= $md5_id_vendor;
		$data_send["encemail"] = $md5_email;
		$data_send["token"] 	= $tk;
		
		$hybrid_api = $this->hybrid_api->getData("login/autologin", $data_send);
		
		if ($hybrid_api["status"] == "SUCCESS"){
			
			$ss = array("credential"=>$hybrid_api["credential"],
						"vendor"	=>$hybrid_api["vendor"],
						"autologin"	=> TRUE);
			
			$this->fn->set_ss_CRS($ss);
			
			redirect("home");
			
		}else{
			$data["err_login"] = $hybrid_api["error_desc"];
			$this->load->view("template/crs/_login.php", $data);
		}

	}
}

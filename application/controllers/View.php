<?php
require APPPATH . '/libraries/CRS_Controller.php';

class View extends CRS_Controller {
	public function __construct(){
		parent::__construct();
	}
	
	public function load(){
		$view = isset($_GET["v"])?$_GET["v"]:"";
		
		$full_path = "$view.php";
		
		$data_vendor = $this->fn->get_ss_CRS();
		if ($data_vendor){
			if (file_exists(APPPATH."views/".$full_path )){
				$this->load->view($full_path, $data_vendor);
			}else{
				$this->load->view("_404", array("file_name"=>$full_path));
			}
		}else{
			?>
			<script>alert("Your session has expired, please log in again.");window.location='';</script>
			<?php
		}
	}

	public function load_agent(){
		$view = isset($_GET["v"])?$_GET["v"]:"";
		
		$full_path = "$view.php";
		
		$data_vendor = $this->fn->get_ss_CRS_agent();
		
		if (file_exists(APPPATH."views/".$full_path )){
			$this->load->view($full_path, $data_vendor);
		}else{
			$this->load->view("_404", array("file_name"=>$full_path));
		}
	}
	
	public function qrcode(){
		/*
		str		-> 	string, 
		level	->	'L','M','Q','H' (default H)
		size	->	1 - 10 (default 7)
		*/
		
		$str	= (isset($_GET["str"])?$_GET["str"]:"NO TEXT");
		$level 	= (isset($_GET["level"])?$_GET["level"]:"");
		$size 	= (isset($_GET["size"])?$_GET["size"]:"");
		
		$this->load->library("phpqrcode");
		$this->load->helper('file');
		
		$image_path = $this->phpqrcode->generate($str, $level, $size);
		
		$this->output->set_content_type(get_mime_by_extension($image_path));
		$this->output->set_output(file_get_contents($image_path));
		
	}
}

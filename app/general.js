// JavaScript Document
var GeneralJS = {}

GeneralJS.activateLeftMenu = function (class_menu){
	$("#menu-left .left-menu").removeClass("active");
	$("#menu-left .left-menu."+class_menu).addClass("active");
}
GeneralJS.activateSubMenu = function (parent_class_menu, class_remove, class_add){
	$(parent_class_menu+" "+class_remove).removeClass("active");
	$(parent_class_menu+" "+class_add).addClass("active");
}
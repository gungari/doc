// JavaScript Document
app.service('fn',function($http){
	var _this = this;
	
	
	this.alert = function(msg){
		alert(msg);
	}
	
	this.formatNumber = function(number, currency){
		if (currency == "IDR"){
			return accounting.formatNumber(number,0,".");
		}else{
			return accounting.formatNumber(number,2,".", ",");
		}
	}
	this.formatDate = function(date, format){
		//if (typeof date === 'string'){ date = date+" 00:00:00"; }
		//var date = new Date(date);
		var date = this.newDate(date);
		return $.datepicker.formatDate(format, date);
	}
	
	this.newDate = function(date){
		if (typeof date === 'string'){ 
			var arr_date = date.split(" ");
			if (arr_date.length == 1){
				date = date+" 00:00:00"; 
			}
			date = date.replace(/-/g, '/');
		}
		return new Date(date);
	}
	
	this.isFn = function (fn_handler){
		if (typeof fn_handler == 'function') {
			return true;
		}else{
			return false;
		}
	}
	
	this.pre = function(data, caption){
		if (caption) console.log(caption);
		console.log(data);
	}

});

$(function () {$('[data-toggle="tooltip"]').tooltip()})
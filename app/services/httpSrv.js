// JavaScript Document
app.service('httpSrv',function($http){
	var _this = this;
	
	this.get = function(param){
		return $http.get(API_URL + param.url + "?merchant_code="+MC+"&merchant_key="+MK)
			.then(
				function(response){	//JIKA SUKSES
					if (response.data.SESSION_EXPIRED){
						_this.redirectSessionExpired(response);
					}else{
						if (typeof param.success == 'function') {
							param.success(response);
						}
					}
				},
				function(response){ //JIKA ERROR
					if (typeof param.error == 'function') {
						param.error(response);
					}
				}
			);
	};
	
	this.post = function(param){
		return $http({
				url		: API_URL + param.url + "?merchant_code="+MC+"&merchant_key="+MK,
				data 	: param.data,
				method 	: 'POST',
				headers	: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
			}).then(
				function(response){	//JIKA SUKSES
					if (response.data.SESSION_EXPIRED){
						_this.redirectSessionExpired(response);
					}else{
						if (typeof param.success == 'function') {
							param.success(response);
						}
					}
				},
				function(response){ //JIKA ERROR
					if (typeof param.error == 'function') {
						param.error(response);
					}
				}
			);
	}
	
	this.redirectSessionExpired = function(response){
		alert("Sorry, your session has expired. Please login...");
		if (response.data.REDIRECT_URL){
			window.location = response.data.REDIRECT_URL;
		}else{
			window.location = "#/login";
		}
		$.hideLoading();
	}
	
});
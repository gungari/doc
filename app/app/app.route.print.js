// JavaScript Document
app.config(function($stateProvider, $urlRouterProvider) {
	
	function load_view(view_path, param){
		
		var full_path = SITE_URL + "view/load/?v="+view_path;
		if (param){
			full_path = full_path+"&"+param;
		}
		return full_path;
	}
	
    $urlRouterProvider.otherwise('/home');
    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: load_view("crs/home/v_index"),
        })
		
		.state('print', {
			url: '/print',
			template: '<div ui-view></div>',
		})
			.state('print.receipt_trans', {
				url: '/receipt_trans/:booking_code',
				templateUrl: load_view("crs/reservation/v_reservation_print_receipt"),
				resolve : load([ BASE_URL + 'app/controllers/crs/transport/trans_reservation_controller.js']),
				controller : "trans_reservation_controller",
			})
			.state('print.receipt', {
				url: '/receipt/:booking_code',
				templateUrl: load_view("crs/reservation/v_general_reservation_print_receipt"),
				resolve : load([ BASE_URL + 'app/controllers/crs/reservation_controller.js']),
				controller : "reservation_controller",
			})
			.state('print.receipt_trans_payment', {
				url: '/receipt_trans_payment/:payment_code',
				templateUrl: load_view("crs/reservation/v_reservation_print_receipt_payment"),
				resolve : load([ BASE_URL + 'app/controllers/crs/transport/trans_reservation_controller.js']),
				controller : "trans_reservation_controller",
			})
			.state('print.receipt_billing_statement', {
				url: '/receipt_billing_statement/:booking_code',
				templateUrl: load_view("crs/reservation/v_reservation_print_billing"),
				resolve : load([ BASE_URL + 'app/controllers/crs/transport/trans_reservation_controller.js']),
				controller : "trans_reservation_controller",
			})
			.state('print.voucher_trans', {
				url: '/voucher_trans/:booking_code/:voucher_code',
				templateUrl: load_view("crs/reservation/v_reservation_print_voucher"),
				resolve : load([ BASE_URL + 'app/controllers/crs/transport/trans_reservation_controller.js']),
				controller : "trans_reservation_controller",
			})
			.state('print.voucher', {
				url: '/voucher/:booking_code/:voucher_code',
				templateUrl: load_view("crs/reservation/v_general_reservation_print_voucher"),
				resolve : load([ BASE_URL + 'app/controllers/crs/reservation_controller.js']),
				controller : "reservation_controller",
			})
			.state('print.trans_arrival_departure', {
				url: '/trans_arrival_departure/:search_type/:search_date/:search_schedule_code/:search_departure_port_id/:search_destination_port_id/:search_time',
				templateUrl: load_view("crs/reservation/v_trans_reservation_departure_print"),
				resolve : load([ BASE_URL + 'app/controllers/crs/transport/trans_reservation_controller.js']),
				controller : "trans_reservation_controller",
			})
			.state('print.act_arrival_departure', {
				url: '/act_arrival_departure/:search_date/:product_code',
				templateUrl: load_view("crs/reservation/v_act_reservation_departure_print"),
				resolve : load([ BASE_URL + 'app/controllers/crs/activities/act_reservation_controller.js']),
				controller : "act_reservation_controller",
			})
			.state('print.trans_pickup_dropoff', {
				url: '/pickup_dropoff/:search_type/:search_date',
				templateUrl: load_view("crs/reservation/v_trans_reservation_pickup_dropoff_info_print"),
				resolve : load([ BASE_URL + 'app/controllers/crs/transport/trans_reservation_controller.js']),
				controller : "trans_reservation_controller",
			})
				
			.state('print.receipt_trans_open_voucher', {
				url: '/receipt_trans_open_voucher/:booking_code',
				templateUrl: load_view("crs/open_voucher/v_reservation_print_receipt"),
				resolve : load([ BASE_URL + 'app/controllers/crs/open_voucher_controller.js']),
				controller : "open_voucher_controller",
			})
			.state('print.receipt_trans_open_voucher_code', {
				url: '/receipt_trans_open_voucher_code/:booking_code',
				templateUrl: load_view("crs/open_voucher/v_booking_detail_voucher_code_print"),
				resolve : load([ BASE_URL + 'app/controllers/crs/open_voucher_controller.js']),
				controller : "open_voucher_controller",
			})

			//INVOICE
			.state('print.invoice_agent', {
				url: '/invoice_agent/:invoice_code',
				templateUrl: load_view("crs/invoice/v_invoice_print_detail"),
				resolve : load([ BASE_URL + 'app/controllers/crs/invoice/invoice_controller.js']),
				controller : "invoice_controller",
			})
			
			//AR
			.state('print.ar_invoice', {
				url: '/ar_invoice/:invoice_code',
				templateUrl: load_view("crs/ar/v_invoice_print_detail"),
				resolve : load([ BASE_URL + 'app/controllers/crs/ar_controller.js']),
				controller : "ar_controller",
			})
			//--
			
			.state('print.invoice_open_voucher', {
				url: '/invoice_open_voucher/:invoice_code',
				templateUrl: load_view("crs/invoice/v_invoice_print_detail_open"),
				resolve : load([ BASE_URL + 'app/controllers/crs/invoice/invoice_controller.js']),
				controller : "invoice_controller",
			})
			.state('print.report_forecast', {
				url: '/report_forecast/:month/:year/:boat_id',
				templateUrl: load_view("crs/report/v_report_forecast_print"),
				resolve : load([ BASE_URL + 'app/controllers/crs/report_controller.js']),
				controller : "report_controller",
			})
			.state('print.report_act_forecast', {
				url: '/report_act_forecast/:month/:year/:product_code',
				templateUrl: load_view("crs/report/v_report_act_forecast_print"),
				resolve : load([ BASE_URL + 'app/controllers/crs/report_controller.js']),
				controller : "report_controller",
			})
			.state('print.report_revenue', {
				url: '/report_revenue/:date/:booking_source/:month/:year/:view/:start_date',
				templateUrl: load_view("crs/report/v_report_revenue_print"),
				resolve : load([ BASE_URL + 'app/controllers/crs/report_revenue_controller.js']),
				controller : "report_revenue_controller",
			})
			.state('print.report_expected', {
				url: '/report_expected/:start_date/:booking_source/:month/:year/:view/:finish_date',
				templateUrl: load_view("crs/report/v_report_expected_print"),
				resolve : load([ BASE_URL + 'app/controllers/crs/report_expected_controller.js']),
				controller : "report_expected_controller",
			})
			.state('print.report_expected_date', {
				url: '/report_expected_date/:date/:paid',
				templateUrl: load_view("crs/report/v_report_expected_date_print"),
				resolve : load([ BASE_URL + 'app/controllers/crs/report_expected_controller.js']),
				controller : "report_expected_controller",
			})
			.state('print.report_act_expected', {
				url: '/report_act_expected/:start_date/:booking_source/:month/:year/:view/:finish_date',
				templateUrl: load_view("crs/report/v_report_act_expected_print"),
				resolve : load([ BASE_URL + 'app/controllers/crs/report_expected_controller.js']),
				controller : "report_expected_controller",
			})
			.state('print.report_check', {
				url: '/report_check/:date/:month/:year/:view/:product',
				templateUrl: load_view("crs/report/v_report_checkin_print"),
				resolve : load([ BASE_URL + 'app/controllers/crs/report_controller.js']),
				controller : "report_controller",
			})
			.state('print.report_sales', {
				url: '/report_sales/:date/:month/:year/:view/:product',
				templateUrl: load_view("crs/report/v_report_sales_print"),
				resolve : load([ BASE_URL + 'app/controllers/crs/report_controller.js']),
				controller : "report_controller",
			})
			.state('print.transaction', {
				url: '/transaction/:start_date/:finish_date/:booking_source',
				templateUrl: load_view("crs/transaction/v_transaction_print"),
				resolve : load([ BASE_URL + 'app/controllers/crs/transport/trans_transaction_controller.js']),
				controller : "trans_transaction_controller",
			})
			.state('print.passenger', {
				url: '/passenger/:passenger_code',
				templateUrl: load_view("crs/checkin/v_participant_print"),
				resolve : load([ BASE_URL + 'app/controllers/crs/transport/checkin_controller.js']),
				controller : "checkin_controller",
			})
			.state('print.deposit', {
				url: '/deposit/:agent_code',
				templateUrl: load_view("crs/transaction/v_transaction_deposit_print"),
				resolve : load([ BASE_URL + 'app/controllers/crs/transport/trans_transaction_controller.js']),
				controller : "trans_transaction_controller",
			})
			.state('print.passcode', {
				url: '/passcode/:booking_code/:voucher_code',
				templateUrl: load_view("crs/reservation/v_passcode_print"),
				resolve : load([ BASE_URL + 'app/controllers/crs/reservation_controller.js']),
				controller : "reservation_controller",
			})
			.state('print.boarding', {
				url: '/boarding/:passenger_code/:booking_code',
				templateUrl: load_view("crs/reservation/v_boarding_print"),
				resolve : load([ BASE_URL + 'app/controllers/crs/reservation_controller.js']),
				controller : "reservation_controller",
			})
		
		
		function load(srcs, callback) {
			return {
				deps : [ '$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
					var deferred = $q.defer();
					var promise = false;
					srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
					if (!promise) {
						promise = deferred.promise;
					}
					angular.forEach(srcs, function(src) {
						promise = promise.then(function() {
							return $ocLazyLoad.load(src+"?"+Math.random());
						});
					});
					deferred.resolve();
					return callback ? promise.then(function() {
						return callback();
					}) : promise;
				} ]
			}
		}
});

//app.controller('CRUDController',function($scope, $http, $ocLazyLoad){
//	$scope.aa = 'asd';
//	$ocLazyLoad.load([ 'ng_app/controllers/CRUDController.js' ]);
//});
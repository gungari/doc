	// JavaScript Document
app.config(function($stateProvider, $urlRouterProvider) {
	
	function load_view(view_path, param){
		
		var full_path = SITE_URL + "view/load/?v="+view_path;
		if (param){
			full_path = full_path+"&"+param;
		}
		return full_path;
	}
	
    $urlRouterProvider.otherwise('/home');
    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: load_view("crs/home/v_index"),
        })
		
		.state('profile', {
            url: '/profile',
           	templateUrl: load_view("crs/home/v_index"),
        })
        .state('agent', {
			url: '/agent',
			templateUrl: load_view("crs/home/v_index"),
		})
		.state('reservation', {
            url: '/reservation',
			templateUrl: load_view("crs/reservation/v_general_reservation_list_2"),
			resolve : load([ BASE_URL + 'app/controllers/crs/reservation_controller.js']),
			controller : "reservation_controller",
        })
        .state('trans_reservation', {
            url: '/trans_reservation',
			templateUrl: load_view("crs/reservation/v_trans_reservation_list_2"),
			resolve : load([ BASE_URL + 'app/controllers/crs/transport/trans_reservation_controller.js']),
			controller : "trans_reservation_controller",
        })
        .state('open_voucher', {
            url: '/open_voucher',
			templateUrl: load_view("crs/open_voucher/v_index"),
			resolve : load([ BASE_URL + 'app/controllers/crs/open_voucher_controller.js']),
			controller : "open_voucher_controller",
        })
        .state('trans_transaction', {
            url: '/trans_transaction',
			templateUrl: load_view("crs/transaction/v_trans_transaction_list"),
			resolve : load([ BASE_URL + 'app/controllers/crs/transport/trans_transaction_controller.js']),
			controller : "trans_transaction_controller",
        })
        .state('refund', {
            url: '/refund',
			templateUrl: load_view("crs/refund/v_refund"),
			resolve : load([ BASE_URL + 'app/controllers/crs/refund_controller.js']),
			controller : "refund_controller",
        })
        .state('checkin', {
            url: '/checkin',
			templateUrl: load_view("crs/reservation/v_trans_checkin"),
			resolve : load([ BASE_URL + 'app/controllers/crs/transport/checkin_controller.js']),
			controller : "checkin_controller",
        })
    	.state('participant', {
            url: '/participant',
			templateUrl: load_view("crs/reservation/v_trans_participant_checkin"),
			resolve : load([ BASE_URL + 'app/controllers/crs/transport/checkin_controller.js']),
			controller : "checkin_controller",
        })
		
		.state('trans_arrival_departure', {
            url: '/trans_arrival_departure',
			templateUrl: load_view("crs/reservation/v_trans_reservation_departure"),
			resolve : load([ BASE_URL + 'app/controllers/crs/transport/trans_reservation_controller.js']),
			controller : "trans_reservation_controller",
        })
        .state('reports', {
            url: '/reports',
			templateUrl: load_view("crs/report/v_report"),
			resolve : load([ BASE_URL + 'app/controllers/crs/report_home_controller.js']),
			controller : "report_home_controller",
        })
        .state('setting', {
            url: '/setting',
			templateUrl: load_view("crs/setting/v_setting"),
			resolve : load([ BASE_URL + 'app/controllers/crs/setting_controller.js']),
			controller : "setting_controller",
        })
        .state('invoice', {
				url: '/invoice',
				templateUrl: load_view("crs/invoice/v_invoice"),
				resolve : load([ BASE_URL + 'app/controllers/crs/invoice/invoice_controller.js']),
				controller : "invoice_controller",
			})
		
        .state('about', {
            url: '/about',
			views: {

				// the main template will be placed here (relatively named)
				'': { templateUrl: VIEW_URL + "home/v_about.php" },
	
				// the child views will be defined here (absolutely named)
				'columnOne@about': { template: 'Look I am a column ONE!' },
	
				// for column two, we'll define a separate controller 
				'columnTwo@about': { template: 'Look I am a column TWO!' },
			}
        })
        .state('transport', {
            url: '/transport',
            templateUrl: load_view("crs/transport/v_index"),
        })
        .state('transport.trips_schedule', {
				url: '/trips_schedule',
				templateUrl: load_view("crs/transport/trips_schedule/v_schedule"),
				
			})
		.state('CRUD', {
            url: '/CRUD',
            templateUrl: VIEW_URL + "home/v_crud_view.php",
			resolve : load([ BASE_URL + 'ng_app/controllers/CRUDController.js?'+Math.random() ]),
			controller : "CRUDController",
        });
        
		function load(srcs, callback) {
			return {
				deps : [ '$ocLazyLoad', '$q', function($ocLazyLoad, $q) {

					var deferred = $q.defer();
					var promise = false;
					srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
					if (!promise) {
						promise = deferred.promise;
					}
					angular.forEach(srcs, function(src) {
						promise = promise.then(function() {
							//return $ocLazyLoad.load(src+"?"+Math.random());
							return $ocLazyLoad.load(src+"?v=11");
							//return $ocLazyLoad.load(src);
						});
					});
					deferred.resolve();
					return callback ? promise.then(function() {
						return callback();
					}) : promise;
				} ]
			}
		}
});

//app.controller('CRUDController',function($scope, $http, $ocLazyLoad){
//	$scope.aa = 'asd';
//	$ocLazyLoad.load([ 'ng_app/controllers/CRUDController.js' ]);
//});
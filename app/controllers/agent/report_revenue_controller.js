angular.module('app').controller('report_revenue_controller', function($scope, $http, httpSrv, $stateParams, $interval, fn, $filter){
	GeneralJS.activateLeftMenu("reports");	
	
	$scope.DATA = {};
	$scope.fn = fn;
	$scope.search = [];
	
	//google.charts.load('current', {'packages':['corechart']});

	$scope.graph = function($object,id_name) {

		var data = new google.visualization.DataTable();
		angular.forEach($object.cols, function(arr){
			data.addColumn(arr.type, arr.label);
		});

		angular.forEach($object.rows, function(arr){
			if (id_name == 'revenueLine') {
				data.addRows([
					[arr.c[0].v, Number(arr.c[1].v)],
				]);;
			}else{
				data.addRows([
					[arr.c[0].v, Number(arr.c[1].v)],
				]);;
			}
		});
		
		var maxNumber = 10;
		for(var i=0; i<$object.rows.length; i++){
			
			for(var j=1; j<$object.rows[i]['c'].length; j++){
				if($object.rows[i]['c'][j]['v'] > maxNumber){
					maxNumber = $object.rows[i]['c'][j]['v'];
				}
			}
		}
		var options = {
			vAxis: {format: 'decimal', minValue: 0, maxValue: maxNumber, gridlines: {count: -1}, textStyle: {fontSize: 10}},
			hAxis: {textStyle: {fontSize: 8}},
	        'title': 'Booking',
	        legend: { position: 'bottom' },
	        fontSize: 10,
	        responsive: true,
	        chartArea: {width: '85%', heigth: '100%'},
		};

		var chart = new google.visualization.LineChart(document.getElementById(id_name));

		chart.draw(data, options);
	}

	$scope.openPopUpWindowVoucher = function (base_url, booking_code, voucher_code){
		var myWindow = window.open(base_url+booking_code+'/'+voucher_code, 'VoucherWindow', 'width=800,height=600');
	}

	
	
	$scope.total_data = function(data){

		$scope.total = 0;
		$scope.currency = '';
		var total = 0;
	    angular.forEach(data, function(row){
	    	total += parseInt(row.sources.total); 
	    	if ($scope.currency == '') {
	    		$scope.currency = row.sources.currency;
	    	}
	   	});
	   
	    $scope.total = total;
	    
	}


	$scope.loadReportCancell = function()
	{
		google.charts.load('current', {'packages':['corechart']});
		// $scope.show_loading_DATA = false;
		$scope.search.year = $filter('date')(new Date(), 'yyyy');
		$scope.search.month = $filter('date')(new Date(), 'M');
		$scope.search.date = $filter('date')(new Date(), 'd');
		$scope.search.view = "month";
		$scope.show_graph = false;
		$scope.date_data = false;
		// $scope.show_error = false;
		$scope.loadReportCancellData();
	}

	$scope.loadCancellDate = function(date)
	{
		$scope.date_data = true;
		$scope.search = [];
		$scope.show_loading_DATA = true;
		date = $filter('date')(date, 'yyyy-M-d');
		var date2 = date.split("-", 3);
				
		$scope.search.date = date2[2];
		$scope.search.year = date2[0];
		$scope.search.month = date2[1];
		// $scope.search.code = AGENT_CODE;
		$scope.search.view = "date";

		$scope.date_data_agent = false;
		date = $filter('date')(date, 'yyyy-mm-dd');
		httpSrv.post({
			url 	: "report_agent/cancell_date",
			data	: $.param({"date":date,"code": $scope.search.code}),
			success : function(response){
				$scope.show_graph = false;
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.report = response.data;
					$scope.getTotalDate(response.data);
					$scope.date_data = true;
					// $scope.total_guests(response.data);
					//$scope.total_books(response.data);
					
				}else{
					$scope.show_loading_DATA = false;
					$scope.date_data = false;
					$scope.show_error = true;
				}
				
			},
			error : function (response){}
			})
	}

	$scope.loadReportCancellData = function()
	{
		
		if (!$scope.search.month) $scope.search.month = $scope.month.toString();
		if (!$scope.search.year) $scope.search.year = $scope.year.toString();
		if (!$scope.search.date) $scope.search.date = $scope.date.toString();
		$scope.show_loading_DATA = true;
		$scope.show_graph = false;
		// $scope.search.code = AGENT_CODE;
		var _search = {	
						"month" 	: $scope.search.month,
						"year"		: $scope.search.year,
						"date" 		: $scope.search.date,
						"view" 		: $scope.search.view,
						"code"		: $scope.search.code
					  };
		if ($scope.search.view != 'date') {
			$scope.show_graph = true;
			$scope.date_data = false;
			$scope.date_data_agent = false;
		}
		else{
			$scope.show_graph = false;
			$scope.date_data = true;
			$scope.date_data_agent = false;
		}
		httpSrv.post({
			url 	: "report_agent/cancell",
			data	: $.param(_search),
			success : function(response){
				
				$scope.show_loading_DATA = false;
				if (response.data.status == "SUCCESS") {
					
					$scope.report = response.data;
					$scope.getTotal(response.data.sales);
					//$scope.getTotalAgent(response.data.agent);
					
					$scope.show_error = false;
					
					
					if ($scope.search.view == 'date') {
						
						$scope.getTotalDate(response.data);
						$scope.date_data_agent = false;
						$scope.date_data = true;
						$scope.show_graph = false;
					}else{
						
						$scope.getTotalSales(response.data.sales);
						$scope.total_books(response.data.sales);
						$scope.total_guests(response.data.sales);
						$scope.graph(response.data.graph, 'cancelLine');
						$scope.show_graph = true;
					}
					
				}else{
					$scope.report = [];
					$scope.show_loading_DATA = false;
					$scope.show_graph = false;
					$scope.show_error = true;
					
				}
				
			},
			error : function (response){}
			})
	}

	$scope.getTotal = function(data){
	    var total = 0;
	    angular.forEach(data.sources, function(row){
	    	total += parseInt(row.total); 
	    
	    });
	   
	    $scope.total = total;
	   
	}

	$scope.getTotalDate = function(data){
		
	    var total = 0;
		var total_trip = 0;
		var currency = '';

	    angular.forEach(data.sales, function(row){
	    	total += parseInt(row.total);
	    	total_trip += parseInt(row.book);

	    	currency = row.currency;
	    });
	   
	    $scope.total_date = total;
	    $scope.total_trip = total_trip;

	    $scope.currency = currency;
	   
	}
	
	$scope.getTotalSales = function(data){
	    var total = 0;
	    var total_guest = 0;
	    var total_book = 0;
	    var currency = '';

	    angular.forEach(data, function(row){

	    	total += parseInt(row.sources.total); 
	    	total_guest += parseInt(row.sources.total_guest);
			total_book += parseInt(row.sources.total_book);

			currency = row.sources.currency;
	    
	    });
	   
	    $scope.total_sales = total;
	    $scope.total_guest = total_guest;
	    $scope.total_book = total_book;
	    $scope.currency = currency;
	  
	}
	$scope.total_books = function(data)
	{
		var total_agent = 0;
		var total_offline = 0;
		var total_online = 0;
		angular.forEach(data, function(row){
			//total += parseInt(row.sources);
			total_agent += parseInt(row.sources['AGENT'].book);
			total_offline += parseInt(row.sources['OFFLINE'].book);
			total_online += parseInt(row.sources['ONLINE'].book);
		});

		$scope.total_book_agent = total_agent;
		$scope.total_book_offline = total_offline;
		$scope.total_book_online = total_online;
	}

	$scope.total_guests = function(data)
	{
		var total_agent = 0;
		var total_offline = 0;
		var total_online = 0;
		angular.forEach(data, function(row){
			//total += parseInt(row.sources['AGENT'].total_guest);
			total_agent += parseInt(row.sources['AGENT'].guest);
			total_offline += parseInt(row.sources['OFFLINE'].guest);
			total_online += parseInt(row.sources['ONLINE'].guest);
		});
		
		$scope.total_guest_agent = total_agent;
		$scope.total_guest_offline = total_offline;
		$scope.total_guest_online = total_online;
	}
	
});
function activate_sub_menu_agent_detail(class_menu){
	$(".agent-detail ul.nav.sub-nav li").removeClass("active");
	$(".agent-detail ul.nav.sub-nav li."+class_menu).addClass("active");
}
// JavaScript Document
angular.module('app').controller('report_controller',function($scope, $http, httpSrv, $stateParams, $interval, fn, $filter){
	
	GeneralJS.activateLeftMenu("reports");	
	
	$scope.DATA = {};
	$scope.fn = fn;
	$scope.search = [];
	
	
	$scope.loadReport = function()
	{

		$scope.show_loading_DATA = true;
		$scope.search.year = $filter('date')(new Date(), 'yyyy');
		$scope.search.month = $filter('date')(new Date(), 'M');
		$scope.search.date = $filter('date')(new Date(), 'd');
		$scope.search.view = "month";
		$scope.show_graph = false;
		$scope.date_data = true;
		$scope.show_error = false;
		$scope.loadReportSales();
	}

	$scope.loadReportSalesData = function(date)
	{
		
		$scope.search = [];
		$scope.show_loading_DATA = true;
		date = $filter('date')(date, 'yyyy-M-d');
		var date2 = date.split("-", 3);
				
		$scope.search.date = date2[2];
		$scope.search.year = date2[0];
		$scope.search.month = date2[1];
		// $scope.search.code = AGENT_CODE;
		$scope.search.view = "date";

		$scope.date_data = false;
		$scope.date_data_agent = false;
		httpSrv.post({
			url 	: "report_agent/sales_date",
			data	: $.param({"date":date, "code": $scope.search.code}),
			success : function(response){
				$scope.show_graph = false;
				
				$scope.getTotalDate_(response.data);
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.report = response.data;
					
					$scope.date_data = true;
					
					
				}else{
					$scope.show_loading_DATA = false;
					$scope.date_data = false;
					$scope.show_error = true;
				}
				
			},
			error : function (response){}
			})
	}



	$scope.loadReportSales = function()
	{
		
		if (!$scope.search.month) $scope.search.month = $scope.month.toString();
		if (!$scope.search.year) $scope.search.year = $scope.year.toString();
		if (!$scope.search.date) $scope.search.date = $scope.date.toString();
		$scope.show_loading_DATA = true;
		$scope.show_graph = false;
		$scope.search.code = AGENT_CODE;
		var _search = {	
						"month" 	: $scope.search.month,
						"year"		: $scope.search.year,
						"date" 		: $scope.search.date,
						"view" 		: $scope.search.view,
						// "code" 		: $scope.search.code,
					  };
		if ($scope.search.view != 'date') {
			$scope.show_graph = true;
			$scope.date_data = false;
			$scope.date_data_agent = false;
		}
		else{
			$scope.show_graph = false;
			$scope.date_data = true;
			$scope.date_data_agent = false;
		}
		httpSrv.post({
			url 	: "report_agent/sales",
			data	: $.param(_search),
			success : function(response){
				
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.report = response.data;
					
					$scope.show_error = false;
					
					
					if ($scope.search.view == 'date') {
						
						$scope.getTotalDate(response.data);
						$scope.date_data_agent = false;
					}else{
						$scope.getTotal(response.data.sales);
						$scope.getTotalAgent(response.data.agent);
						//$scope.chartObject.data = response.graph;
						$scope.graph(response.data.graph, "chartLine");
						$scope.getTotalSales(response.data.sales);
						$scope.total_books(response.data.sales);
						$scope.total_guests(response.data.sales);
						$scope.sum(response.data.sales);
						$scope.show_graph = true;
					}
					
				}else{
					$scope.report = [];
					$scope.show_loading_DATA = false;
					$scope.show_graph = false;
					$scope.show_error = true;
					
				}
				
			},
			error : function (response){}
			})


	}
	$scope.getTotalDate_ = function(data){

		
	    var total = 0;
	    var total_trip = 0;
	    var currency = '';

	    angular.forEach(data.sales, function(row){
	    	total += parseInt(row.total);
	    	total_trip += parseInt(row.book);

	    	currency = row.currency;
	    });
	   
	    $scope.total_date = total;
	    $scope.total_trip = total_trip;

	    $scope.currency = currency;
	 
	   
	}
	$scope.getTotalDate = function(data){

		
	    var total = 0;
	    var total_trip = 0;
	    var currency = '';

	    angular.forEach(data.sales, function(row){
	    	total += parseInt(row.total);
	    	total_trip += parseInt(row.book);

	    	currency = row.currency;
	    });
	   
	    $scope.total_date = total;
	    $scope.total_trip = total_trip;

	    $scope.currency = currency;
	 
	   
	}
	$scope.getTotal = function(data){
		    var total = 0;
		    angular.forEach(data.sources, function(row){
		    total += parseInt(row.total); 
	});

	$scope.getTotalAgent = function(data){

	    var total = 0;
	    angular.forEach(data, function(row){
	    total += parseInt(row.total);

	    });

	    $scope.totalagent = total;
	}
	$scope.total_books = function(data)
	{
		var total_agent = 0;
		var total_offline = 0;
		var total_online = 0;
		angular.forEach(data, function(row){
			//total += parseInt(row.sources);
			total_agent += parseInt(row.sources['AGENT'].book);
			total_offline += parseInt(row.sources['OFFLINE'].book);
			total_online += parseInt(row.sources['ONLINE'].book);
		});

		$scope.total_book_agent = total_agent;
		$scope.total_book_offline = total_offline;
		$scope.total_book_online = total_online;
	}

	$scope.sum = function(data)
	{
		var total_agent = 0;
		var total_offline = 0;
		var total_online = 0;
		angular.forEach(data, function(row){
			//total += parseInt(row.sources);
			total_agent += parseInt(row.sources['AGENT'].total);
			total_offline += parseInt(row.sources['OFFLINE'].total);
			total_online += parseInt(row.sources['ONLINE'].total);
		});

		$scope.sum_agent = total_agent;
		$scope.sum_offline = total_offline;
		$scope.sum_online = total_online;
	}

	$scope.total_guests = function(data)
	{
		var total_agent = 0;
		var total_offline = 0;
		var total_online = 0;
		angular.forEach(data, function(row){
			//total += parseInt(row.sources['AGENT'].total_guest);
			total_agent += parseInt(row.sources['AGENT'].guest);
			total_offline += parseInt(row.sources['OFFLINE'].guest);
			total_online += parseInt(row.sources['ONLINE'].guest);
		});
		
		$scope.total_guest_agent = total_agent;
		$scope.total_guest_offline = total_offline;
		$scope.total_guest_online = total_online;
	}

	$scope.getTotalSales = function(data){
	    var total = 0;
	    var total_guest = 0;
	    var total_book = 0;
	    var currency = '';


	    angular.forEach(data, function(row){

	    	total += parseInt(row.sources.total); 
	    	total_guest += parseInt(row.sources.total_guest);
			total_book += parseInt(row.sources.total_book);

			currency = row.sources.currency;
	    
	    });
	   
	    $scope.total_sales = total;
	    $scope.total_guest = total_guest;
	    $scope.total_book = total_book;
	    $scope.currency = currency;
	  
	   
	}

	$scope.graph = function($object,id_name ) {

		google.charts.load('current', {'packages':['corechart']});
		var data = new google.visualization.DataTable();
		angular.forEach($object.cols, function(arr){
				
			data.addColumn(arr.type, arr.label);
		});

		angular.forEach($object.rows, function(arr){
			if (id_name == 'revenueLine') {
				data.addRows([
					[arr.c[0].v, Number(arr.c[1].v)],
				]);;
			}else{
				data.addRows([
					[arr.c[0].v, Number(arr.c[1].v)],
				]);;
			}
			
		});
	
		var maxNumber = 10;
		for(var i=0; i<$object.rows.length; i++){
			
			for(var j=1; j<$object.rows[i]['c'].length; j++){
				if($object.rows[i]['c'][j]['v'] > maxNumber){
					maxNumber = $object.rows[i]['c'][j]['v'];
				}
			}
		}
		var options = {
			vAxis: {format: 'decimal', minValue: 0, maxValue: maxNumber, gridlines: {count: -1}, textStyle: {fontSize: 10}},
			hAxis: {textStyle: {fontSize: 8}},
	        'title': 'Booking',
	        legend: { position: 'bottom' },
	        fontSize: 10,
	        responsive: true,
	        chartArea: {width: '85%', heigth: '100%'},
		};
		if (id_name) {
			var chart = new google.visualization.LineChart(document.getElementById(id_name));
		}else{
			var chart = new google.visualization.LineChart(document.getElementById('chartLine'));
		}
		
		chart.draw(data, options);
	}


	$scope.loadAgentDate = function(date){


		$scope.search = [];
		//$scope.show_loading_DATA = true;
		date = $filter('date')(date, 'yyyy-M-d');
		var date2 = date.split("-", 3);
				
		$scope.search.date = date2[2];
		$scope.search.year = date2[0];
		$scope.search.month = date2[1];

		$scope.search.view = "month";

		
		// $scope.date_data_agent = false;

		httpSrv.post({
			url 	: "report/sales_agent_date",
			data	: $.param({"date":date}),
			success : function(response){

			
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.agent = response.data;
					// $scope.show_graph = false;
					// $scope.date_data_agent = true;
					$scope.total_agentt(response.data);
					
				}else{
					$scope.show_loading_DATA = false;
					$scope.show_error = true;
				}
				
			},
			error : function (response){}
			})


	}

	$scope.loadAgentMonth = function(month){

		$scope.search = [];
		//$scope.show_loading_DATA = true;
		month = $filter('date')(month + '-01', 'yyyy-M-d');
		var date3 = month.split("-", 2);	

		$scope.search.year = date3[0];
		$scope.search.month = date3[1];
		$scope.search.date =  $filter('date')(new Date(), 'MM');
		$scope.search.view = "year";

		
		// $scope.date_data_agent = false;

		httpSrv.post({
			url 	: "report/sales_agent_month",
			data	: $.param({"month":month}),
			success : function(response){

				
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.agent = response.data;
					// $scope.show_graph = false;
					// $scope.date_data_agent = true;
					$scope.total_agentt(response.data);
					
				}else{
					$scope.show_loading_DATA = false;
					$scope.show_error = true;
				}
				
			},
			error : function (response){}
			})
	}

	$scope.total_agentt = function(data){

		$scope.total_agent = 0;
		var total = 0;
	    angular.forEach(data.sales, function(row){
	    	total += parseInt(row.total); 
	   	});
	   
	    $scope.total_agent = total;
	
	}
	$scope.loadReportSalesMonth = function(month)
	{
		$scope.search = [];
		$scope.show_loading_DATA = true;
		month = $filter('date')(month + '-01', 'yyyy-M-d');
		var date3 = month.split("-", 2);	

		$scope.search.year = date3[0];
		$scope.search.month = date3[1];
		$scope.search.date =  $filter('date')(new Date(), 'MM');
		$scope.search.view = "month";

		
		$scope.date_data = false;
		$scope.date_data_agent = false;
		httpSrv.post({
			url 	: "report_agent/sales_month",
			data	: $.param({"month":month}),
			success : function(response){
				
				$scope.show_graph = false;
				var date = month.split("-", 2);
				
				$scope.search.year = date[0];
				$scope.search.month = date[1];
			
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.report = response.data;
					$scope.show_graph = true;
					
					$scope.getTotal(response.data.sales);
					$scope.getTotalAgent(response.data.agent);
					$scope.getTotalSales(response.data.sales);
					$scope.graph(response.data.graph, "chartLine");
					//$scope.getTotalDate(response.data);
					
				}else{
					$scope.show_loading_DATA = false;
					$scope.show_error = true;
				}
				
			},
			error : function (response){}
			})
	}

	
	$scope.printReport =  function()
	{
		 var month = $stateParams.month;
		 var year = $stateParams.year;

		 $scope.month = $filter('date')(year+"-"+month+"-01", 'MMMM');
		 $scope.year = $filter('date')(year+"-"+month+"-01", 'yyyy');

		 var _search = {	
						"month" 	: month,
						"year"		: year
					  };

		httpSrv.post({
			url 	: "report/forcast",
			data	: $.param(_search),
			success : function(response){
				if (response.data.status == "SUCCESS") {
					$scope.$parent.$parent.show_print_button = true;
					$scope.report = response.data;
				}else{
					
					$scope.report = [];
				}
				
				
			},
			error : function (response){}
			})
	}
	$scope.total = 0;
	$scope.total_sales = 0;
	$scope.total_guest = 0;
	$scope.total_book = 0;
	$scope.total_date = 0;
	$scope.total_trip = 0;

	
	   
	$scope.total = total;
	   
	}
	
	$scope.getTotalMonth = function(data){

		var total = 0;
	    var total_guest = 0;
	    var total_book = 0;
	     var currency = '';

		angular.forEach(data.sources, function(row){
			total += parseInt(row.sources.total); 
	    	total_guest += parseInt(row.sources.total_guest);
			total_book += parseInt(row.sources.total_book);

			currency = row.sources.currency;
	    
		})
		 $scope.total_sales = total;
	    $scope.total_guest = total_guest;
	    $scope.total_book = total_book;
	    $scope.currency = currency;
	}
	
	
	

	
	$scope.loading = function(revert)
	{
		if(revert===true) {
			$('.report-content').show();
			$(".loading-animation").hide();
		} else {
			$('.report-content').hide();
			$(".loading-animation").show();
		}
	}

	


});
function activate_sub_menu_agent_detail(class_menu){
	$(".agent-detail ul.nav.sub-nav li").removeClass("active");
	$(".agent-detail ul.nav.sub-nav li."+class_menu).addClass("active");
}

// untuk dapat menjalankan 2 ng-map
// angular.bootstrap(document.getElementById("Map"), ['ngMap']);
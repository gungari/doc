// JavaScript Document
angular.module('app').controller('profile_controller',function($scope, $http, httpSrv, $stateParams, $interval){
	
	GeneralJS.activateLeftMenu("profile");
	$scope.fn = fn;
	$scope.DATA = {};
	
	$scope.loadAgent =  function(){
	

		httpSrv.get({
			url : "agent",
			success : function(response){
				
				if (response.data.status == "SUCCESS") {
					$(".pwd").val("");
				}
				$scope.DATA.agent =  response.data.agents;
				
			},
			error : function(err){

			}
		})
	}
	
});

function activate_sub_menu_agent_detail(class_menu){
	$("ul.nav.sub-nav.profile li").removeClass("active");
	$("ul.nav.sub-nav.profile li."+class_menu).addClass("active");
}
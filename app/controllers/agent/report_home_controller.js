angular.module('app').controller('report_home_controller',function($scope, $http, httpSrv, $stateParams, $interval, fn, $filter){
	
	GeneralJS.activateLeftMenu("reports");	
	
	$scope.DATA = {};
	$scope.fn = fn;
	$scope.home = [];

	$scope.graph = function($object,id_name) {
		
		var data = new google.visualization.DataTable();
		angular.forEach($object.cols, function(arr){
				
			data.addColumn(arr.type, arr.label);
		});

		angular.forEach($object.rows, function(arr){
			if (id_name == 'revenueLine_home') {
				data.addRows([
					[arr.c[0].v, Number(arr.c[1].v)],
				]);;
			}else{
				data.addRows([
					[arr.c[0].v, Number(arr.c[1].v)],
				]);;
			}
			
			
		});

		var maxNumber = 10;
		for(var i=0; i<$object.rows.length; i++){
			
			for(var j=1; j<$object.rows[i]['c'].length; j++){
				if($object.rows[i]['c'][j]['v'] > maxNumber){
					maxNumber = $object.rows[i]['c'][j]['v'];
				}
			}
		}
		
		var title = 'Booking';
		if (id_name == 'revenueLine_home') {
			title = 'Total';
		}else if(id_name == 'cancelLine_home'){
			title = 'Cancel';
		}

		var options = {
			vAxis: {format: 'decimal', minValue: 0, maxValue: maxNumber, gridlines: {count: -1}, textStyle: {fontSize: 10}},
			hAxis: {textStyle: {fontSize: 8}},
	        'title': title,
	        legend: { position: 'bottom' },
	        fontSize: 10,
	        responsive: true,
	        chartArea: {width: '85%', heigth: '100%'},
		};

		var chart = new google.visualization.LineChart(document.getElementById(id_name));
		
		
		chart.draw(data, options);
	}

	$scope.loadReportHome = function()
	{
		
		$scope.show_loading_DATA = true;
		//$scope.home.year = $filter('date')(new Date(), 'yyyy');
		//$scope.home.month = $filter('date')(new Date(), 'MM');
		$scope.home.year = '2017';
		$scope.home.month = '08';
		$scope.home.date = '30';
		$scope.home.view = "month";
		$scope.show_graph = false;
		$scope.date_data = true;
		$scope.show_error = false;
		$scope.loadSalesHome();
		$scope.loadReportCancellData();
	}

	$scope.getTotalCancell = function(data){
		
	    var total = 0;
	    var ammount = 0;
	   
	    angular.forEach(data, function(row){

	    	total += parseInt(row.sources.total); 
	    	ammount += parseInt(row.sources.total_book);

			currency = row.sources.currency;
	    
	    });
	   
	    $scope.total_cancell = ammount;
	    $scope.amount = total;
	    $scope.currency_cancell = currency;
	 
	}

	$scope.loadReportCancellData = function()
	{
		google.charts.load('current', {'packages':['corechart']});
		if (!$scope.home.month) $scope.home.month = $scope.month.toString();
		if (!$scope.home.year) $scope.home.year = $scope.year.toString();
		if (!$scope.home.date) $scope.home.date = $scope.date.toString();


		var _search = {	
						"month" 	: $scope.home.month,
						"year"		: $scope.home.year,
						"date" 		: $scope.home.date,
						"view" 		: $scope.home.view,
						// "agent_code": AGENT_CODE,
					  };
		
		httpSrv.post({
			//url 	: "transport/report/sales_cancell_home",
			url 	: "report_agent/sales_cancell_home",
			data	: $.param(_search),
			success : function(response){
				$scope.show_loading_DATA = false;

				if (response.data.status == "SUCCESS") {

					$scope.graph(response.data.graph, "cancelLine_home");
					
					$scope.getTotalCancell(response.data.sales);
					
				}else{
					//$scope.graph(response.data.graph, "cancelLine");
					
				}
				
			},
			error : function (err){
				console.log('hello: ', err);
			}
			})
	}

	$scope.loadSalesHome = function()
	{
		google.charts.load('current', {'packages':['corechart']});
		if (!$scope.home.month) $scope.home.month = $scope.month.toString();
		if (!$scope.home.year) $scope.home.year = $scope.year.toString();
		if (!$scope.home.date) $scope.home.date = $scope.date.toString();
		$scope.show_loading_DATA = true;
		var _search = {	
						"month" 	: $scope.home.month,
						"year"		: $scope.home.year,
						"date" 		: $scope.home.date,
						"view" 		: $scope.home.view,
						// "agent_code": AGENT_CODE,	
					  };
		if ($scope.home.view != 'date') {
			$scope.show_graph = true;
			$scope.date_data = false;
			$scope.date_data_agent = false;
		}
		else{
			$scope.show_graph = false;
			$scope.date_data = true;
			$scope.date_data_agent = false;
		}
		httpSrv.post({
			url 	: "report_agent/Sales_home",
			data	: $.param(_search),
			success : function(response){
				
				$scope.graph(response.data.graph, 'chartLine_home');
				$scope.show_loading_DATA = false;
				if (response.data.status == "SUCCESS") {
					
					
					$scope.getTotal_sales(response.data.sales);
												
				}else{
					$scope.report = [];
					$scope.show_loading_DATA = false;
					$scope.show_graph = false;
					$scope.show_error = true;
					
				}
				
			},
			error : function (response){}
			})


	}

	$scope.getTotal_sales = function(data){

	    var total = 0;
	    angular.forEach(data, function(row){
	    	total += parseInt(row.sources.total); 
	    	$scope.currency_home = row.sources.currency;
	    });
	   
	    $scope.total_home = total;
	   
	}

	

	
	

});
// JavaScript Document

angular.module('app').controller('trans_reservation_controller',function($scope, $http, httpSrv, $stateParams, $interval, fn, $location){
	
	GeneralJS.activateLeftMenu("reservation");	
	
	$scope.DATA = {};
	$scope.fn = fn;

	$scope.arrivalDepartureTransSearch = function(data, after_load_handler_function){
		$scope.show_arrivalDepartureTransSearch_loading = true;
		
		if (!data){
			var data = {"type" 				: $scope.dept.type,
						"date" 				: $scope.dept.date,
						"departure_port_id" : $scope.dept.departure.id,
						"arrival_port_id" 	: $scope.dept.destination.id,
						"data.boat_id"		: $scope.dept.boat.id};
			
			if ($scope.dept.time){
				if ($scope.dept.time.schedule_code){ data.schedule_code = $scope.dept.time.schedule_code; }
				if ($scope.dept.time.time){ data.time = $scope.dept.time.time; }
			}else{
				data.boat_id = $scope.dept.boat.id;
				data.time = "ALL";
			}
	
			//data = {"type":"departure","date":"2017-09-11","departure_port_id":"10","arrival_port_id":"3"};
			//$scope.DATA_R = data;
		}
		
		httpSrv.post({
				url 	: "agent_controller/passenger_list",
				data	: $.param(data),
				success : function(response){
					$scope.show_arrivalDepartureTransSearch_loading = false;
					$scope.search = response.data.search;
					if (response.data.status == "SUCCESS"){
						$scope.arrival_departure = response.data.passenger_list;
						$scope.summary = response.data.summary;
						
						if (fn.isFn(after_load_handler_function)) {
							after_load_handler_function(response.data);
						}
						
					}else{
						$scope.arrival_departure = [];
					}
				}
			});
		
		
	}

	$scope.pickupDropofTransSearch = function(data, after_load_handler_function){
		$scope.show_arrivalDepartureTransSearch_loading = true;
		
		if (!data){
			var data = {"type" 				: $scope.dept.type,
						"date" 				: $scope.dept.date};
	
			//data = {"type":"departure","date":"2017-06-30","schedule_code":"TDQ0507","departure_port_id":"10","arrival_port_id":"3","time":"09:30"};
			//$scope.DATA_R = data;
		}
		httpSrv.post({
				url 	: "agent_controller/pickup_dropoff_information",
				data	: $.param(data),
				success : function(response){
					$scope.show_arrivalDepartureTransSearch_loading = false;
					$scope.search = response.data.search;
					
					if (response.data.status == "SUCCESS"){
						$scope.pickup_dropoff = response.data.pickup_dropoff;
						
						if (fn.isFn(after_load_handler_function)) {
							after_load_handler_function(response.data);
						}
						
					}else{
						$scope.pickup_dropoff = [];
					}
				}
			});
		
		
	}

	$scope.submit_edit_additional_service = function(event){
		
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myAdditionalService.error_desc = [];
		
		var data_submit = {	"additional_service": $scope.DATA.myAdditionalService.items,
							"voucher_code" 		: $scope.DATA.myAdditionalService.voucher.voucher_code};
		
		httpSrv.post({
			url 	: "booking/update_additional_service",
			data	: $.param(data_submit),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.editDataBookingTransportDetail();
					$("#modal-add-additional-service").modal("hide");
					toastr.success("Additional service updated...");
				}else{
					$scope.DATA.myAdditionalService.error_desc = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
			}
		});
	}

	$scope.edit_additional_service = function(booking_detail){
		
		$scope.DATA.myAdditionalService = {};
		$scope.DATA.myAdditionalService.voucher = booking_detail;
		$scope.DATA.myAdditionalService.items = [];
		$scope.DATA.myAdditionalService.TOTAL = 0;
		if (booking_detail.additional_service){
			$scope.DATA.myAdditionalService.items = angular.copy(booking_detail.additional_service);
		}else{
			$scope.edit_additional_service_addAdditionalService();
		}
		$scope.edit_additional_service_calculateTotal();
	}

	$scope.edit_additional_service_addAdditionalService = function (){
		var additional = {"name":"", "qty":1, "price":0};
		if (!$scope.DATA.myAdditionalService.items){
			$scope.DATA.myAdditionalService.items = [];
		}
		$scope.DATA.myAdditionalService.items.push(additional);
		$scope.edit_additional_service_calculateTotal();
	}

	$scope.edit_additional_service_removeAdditionalService = function (index){
		$scope.DATA.myAdditionalService.items.splice(index,1);
		$scope.edit_additional_service_calculateTotal();
	}

	$scope.edit_additional_service_calculateTotal = function(){
		var TOTAL = 0;
		for (x in $scope.DATA.myAdditionalService.items){
			var items = $scope.DATA.myAdditionalService.items[x];
			var sub_total = items.qty * items.price;
			TOTAL += sub_total;
		}
		$scope.DATA.myAdditionalService.TOTAL = TOTAL;
	}

	$scope.calculateQtySelectedPassangerPartialCancel = function(voucher){
		$scope.DATA.cancelation.partial_cancel.qty = 0;
		for (x in voucher.passenger){
			if (voucher.passenger[x].selected){
				$scope.DATA.cancelation.partial_cancel.qty += 1;
			}
		}
	}

	//Cancel Booking
	$scope.cancelBooking = function(booking){
		$scope.DATA.cancelation = {	'cancel_all_trip' 	: 0,
									'voucher'			: {},
									'is_cancel_partial' : 0};
		
		if (!$scope.cancelation_fee){
			$scope.cancelation_fee = {};
		}
		
		for (i in $scope.DATA.current_booking.booking.detail){
			var voucher = {};
			voucher.code = $scope.DATA.current_booking.booking.detail[i].voucher_code;
			voucher.is_cancel = 0;
			voucher.fee = $scope.DATA.current_booking.booking.detail[i].subtotal;
			$scope.DATA.cancelation.voucher[voucher.code] = voucher;
			
			if (!$scope.cancelation_fee[voucher.code]) $scope.cancelation_fee[voucher.code] = {};
			$scope.cancelation_fee[voucher.code].pay = $scope.DATA.current_booking.booking.detail[i].subtotal;
		}

		$scope.DATA.cancelation.cancel_all_trip = 1;
		$scope.chkCancelAllBookingClick();

		if ($scope.DATA.current_booking.booking.total_payment == 0){
			$scope.cancelationFee(booking.booking_code);

			if ($scope.cancelation_fee[voucher.code]) {
				$scope.DATA.cancelation.voucher[voucher.code].fee = $scope.cancelation_fee[voucher.code]['pay'];
			}
		}
	}

	$scope.partialCancelSelectVoucher = function(voucher){
		$scope.DATA.cancelation.is_cancel_partial = 1;
		$scope.DATA.cancelation.partial_cancel = {};
		$scope.DATA.cancelation.partial_cancel.qty = 0;
		$scope.DATA.cancelation.partial_cancel.voucher = angular.copy(voucher);
	}

	$scope.change_check = function(value, type){
		if (type == 'DROPOFF') {
			$scope.check_edit_dropoff = value;
		}else{
			$scope.check_edit = value;
		}	
	}

	$scope.submit_pickup_dropoff = function(event){
		
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myPickupDropoff.error_desc = [];
		if (!$scope.DATA.myPickupDropoff.selected_area) {
			$scope.DATA.myPickupDropoff.selected_area = [];
		}
		
		var data_submit = {	"is_pickup" 		: $scope.check_edit,
							"is_dropoff" 		: $scope.check_edit_dropoff,
							"type" 				: $scope.DATA.myPickupDropoff.type,
							"area"				: $scope.DATA.myPickupDropoff.selected_area.area,
							"time"				: $scope.DATA.myPickupDropoff.selected_area.time,
							"voucher_code" 		: $scope.DATA.myPickupDropoff.voucher_code,
							"hotel_name" 		: $scope.DATA.myPickupDropoff.hotel_name,
							"hotel_address" 	: $scope.DATA.myPickupDropoff.hotel_address,
							"hotel_phone_number": $scope.DATA.myPickupDropoff.hotel_phone_number,
							"price_pickup"		: $scope.DATA.myPickupDropoff.price_pickup,
							"price_dropoff"		: $scope.DATA.myPickupDropoff.price_dropoff};
		
		httpSrv.post({
			url 	: "transport/booking/update_pickup_dropoff",
			data	: $.param(data_submit),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.editDataBookingTransportDetail();
					$("#modal-add-picdrp").modal("hide");
					toastr.success(data_submit.type+" updated...");
				}else{
					$scope.DATA.myPickupDropoff.error_desc = response.data.error_desc;
					console.log(response.data.error_desc);
				}
				$(event.target).find("button:submit").removeAttr("disabled");
			}
		});
	}
	
	// update current customer
	$scope.update_customer = function (event){
		
		$(event.target).find("button:submit").attr("disabled","disabled");
		
		var data_submit = $scope.DATA.myCustomer;
		data_submit.booking_code = $scope.DATA.current_booking.booking.booking_code;
		
		$scope.DATA.myCustomer.error_desc = [];
		
		httpSrv.post({
			url 	: "transport/booking/update_customer",
			data	: $.param(data_submit),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataBookingTransportDetail();
					$("#modal-add-cust").modal("hide");
					toastr.success("Updated...");
				}else{
					$scope.DATA.myCustomer.error_desc = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
			}
		});
	}

	// obtain pop-up data current customer
	$scope.edit_customer = function(){
		
		$scope.DATA.myCustomer = angular.copy($scope.DATA.current_booking.booking.customer);
		$scope.DATA.myCustomer.error_desc = [];

		// get country list
		//Load Data Country List
		if (!$scope.$root.DATA_country_list){
			httpSrv.post({
				url 	: "general/country_list",
				success : function(response){ $scope.$root.DATA_country_list = response.data.country_list;},
			});
		}
		$("#modal-add-cust .btn-submit-edit-cust").removeAttr("disabled");
	}

	$scope.DATA.service = [];
	$scope.loadDataRate = function(rate_id){

		httpSrv.post({
			url 	: "transport/schedule/rates_detail_by_id/"+rate_id,
			success : function(response){ 
				
				if (response.data.status == "SUCCESS"){
					
					$scope.DATA.service.push({id:rate_id,"is_pickup":response.data.pickup_service,"is_dropoff":response.data.dropoff_service});
					//console.log($scope.DATA.service);

				}
			},
		});

	}

	$scope.add_pickup_dropoff = function(type, booking_detail){
		
		
		$scope.DATA.myPickupDropoff = {};
		if (type == "PICKUP") {
			$scope.check_edit = "no";
		}else if(type == "DROPOFF"){
			$scope.check_edit_dropoff = "no";
		}
		
		$scope.check_edit =  booking_detail.is_pickup.toLowerCase();
		$scope.check_edit_dropoff =  booking_detail.is_dropoff.toLowerCase();

		$scope.DATA.myPickupDropoff.voucher_code = booking_detail.voucher_code;
		$scope.DATA.myPickupDropoff.type = type;
		
		httpSrv.post({
			url 	: "transport/schedule/rates_detail_by_id/"+booking_detail.rates.id,
			success : function(response){ 
				
				if (response.data.status == "SUCCESS"){
					$scope.DATA.myPickupDropoff.is_pickup = response.data.pickup_service;
					$scope.DATA.myPickupDropoff.is_dropoff = response.data.dropoff_service;

					if (type == "PICKUP"){
						$scope.DATA.myPickupDropoff.rates_area = response.data.pickup_area;
					}else if (type == "DROPOFF"){
						$scope.DATA.myPickupDropoff.rates_area = response.data.dropoff_area;
					}
					for (_x in $scope.DATA.myPickupDropoff.rates_area){
						if ($scope.DATA.myPickupDropoff.rates_area[_x].area == $scope.DATA.myPickupDropoff.area){
							$scope.DATA.myPickupDropoff.selected_area = $scope.DATA.myPickupDropoff.rates_area[_x];
							break
						}
					}
				}
			},
		});	
	}

	$scope.edit_pickup_dropoff = function(type, booking_detail){
		
		

		$scope.DATA.myPickupDropoff = {};

		if (booking_detail.pickup || booking_detail.dropoff) {
			$scope.check_edit =  booking_detail.is_pickup.toLowerCase();
			$scope.check_edit_dropoff =  booking_detail.is_dropoff.toLowerCase();

			if (type == "PICKUP"){
				$scope.DATA.myPickupDropoff = angular.copy(booking_detail.pickup);
				
			}else if (type == "DROPOFF"){
				$scope.DATA.myPickupDropoff = angular.copy(booking_detail.dropoff);
				
			}
		}


		$scope.DATA.myPickupDropoff.voucher_code = booking_detail.voucher_code;
		$scope.DATA.myPickupDropoff.type = type;

		httpSrv.post({
			url 	: "transport/schedule/rates_detail_by_id/"+booking_detail.rates.id,
			success : function(response){ 
				
				if (response.data.status == "SUCCESS"){
					$scope.DATA.myPickupDropoff.is_pickup = response.data.pickup_service;
					$scope.DATA.myPickupDropoff.is_dropoff = response.data.dropoff_service;
					
					
					if (type == "PICKUP"){
						$scope.DATA.myPickupDropoff.rates_area = response.data.pickup_area;
					}else if (type == "DROPOFF"){
						$scope.DATA.myPickupDropoff.rates_area = response.data.dropoff_area;
					}
					for (_x in $scope.DATA.myPickupDropoff.rates_area){
						if ($scope.DATA.myPickupDropoff.rates_area[_x].area == $scope.DATA.myPickupDropoff.area){
							$scope.DATA.myPickupDropoff.selected_area = $scope.DATA.myPickupDropoff.rates_area[_x];
							break
						}
					}
				}
			},
		});

	}

	
	$scope.editDataBookingTransportDetail = function(){
	
		$scope.loadDataBookingTransportDetail();
		
		//Load Data Available Port
		httpSrv.post({
			url 	: "transport/port/available_port",
			success : function(response){ 
				$scope.DATA.available_port = response.data;
			},
		});
		
	}

	$scope.loadDataBookingTransport = function(){
		$scope.show_loading_DATA_bookings = true;
		httpSrv.post({
			url 	: "transport/booking",
			//data	: $.param({"publish_status":"all"}),
			success : function(response){
				$scope.show_loading_DATA_bookings = false;
				$scope.DATA.bookings = response.data;
			},
			error : function (response){}
		});
	}
	$scope.loadDataBookingTransport_2 = function(page, show_loading){
		
		if (!$scope.DATA.bookings || show_loading){
			$scope.show_loading_DATA_bookings = true;
		}
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		
		if (!$scope.search){ 
			var now = new Date(),
				start_date = new Date(),
				end_date = new Date();
			
			start_date.setDate(start_date.getDate() - 30);
			
			$scope.search = { "start_date" : $scope.fn.formatDate(start_date, "yy-mm-dd"),
							  "end_date" : $scope.fn.formatDate(end_date, "yy-mm-dd"),}; 
		}
		
		if (!page) page = 1;
		
		var _search = $scope.search;
			_search.page = page;
		
		httpSrv.post({
			url 	: "transport/booking/booking_and_voucher_list",
			data	: $.param({"search":_search}),
			success : function(response){
				$scope.show_loading_DATA_bookings = false;
				$scope.DATA.bookings = response.data;
				
				if ($scope.DATA.bookings.search){
					$scope.DATA.bookings.search.pagination = [];
					for (i=1;i<=$scope.DATA.bookings.search.number_of_pages; i++){
						$scope.DATA.bookings.search.pagination.push(i);
					}
				}
			},
			error : function (response){}
		});
		
		//Load Data Booking Source
		if (!$scope.$root.DATA_booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.$root.DATA_booking_source = response.data.booking_source; },
			});
		}
		
		//Load Data Booking Status
		if (!$scope.$root.DATA_booking_status){
			httpSrv.post({
				url 	: "setting/booking_status",
				success : function(response){ $scope.$root.DATA_booking_status = response.data.booking_status; },
			});
		}
	}
	
	$scope.loadDataTransactionTransportXXXX = function(page){
		GeneralJS.activateLeftMenu("transaction");
		$scope.show_loading_DATA_bookings = true;
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		
		if (!$scope.search){ 
			var now = new Date(),
				start_date = new Date(),
				end_date = new Date();
			
			start_date.setDate(start_date.getDate() - 30);
			
			$scope.search = { "start_date" : $scope.fn.formatDate(start_date, "yy-mm-dd"),
							  "end_date" : $scope.fn.formatDate(end_date, "yy-mm-dd"),}; 
		}
		
		if (!page) page = 1;
		
		var _search = $scope.search;
			_search.page = page;
		
		httpSrv.post({
			url 	: "transport/transaction",
			data	: $.param({"search":_search}),
			success : function(response){
				$scope.show_loading_DATA_bookings = false;
				$scope.DATA.transaction = response.data;
				
				if ($scope.DATA.transaction.search){
					$scope.DATA.transaction.search.pagination = [];
					for (i=1;i<=$scope.DATA.transaction.search.number_of_pages; i++){
						$scope.DATA.transaction.search.pagination.push(i);
					}
				}
			},
			error : function (response){}
		});
		
		//Load Data Booking Source
		if (!$scope.DATA.booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.DATA.booking_source= response.data; },
			});
		}
		
		//Load Data Booking Status
		if (!$scope.DATA.booking_status){
			httpSrv.post({
				url 	: "setting/booking_status",
				success : function(response){ $scope.DATA.booking_status = response.data; },
			});
		}
	}
	
	$scope.loadDataBookingTransportDetail = function(booking_code, after_load_handler_function){
		
		if (!booking_code){ booking_code = $stateParams.booking_code; }
		var agent_code = AGENT_CODE;
		
		httpSrv.get({
			url 	: "booking/detail/"+booking_code+"/"+agent_code,
			success : function(response){
				var booking = response.data;
				//console.log(booking);
				if (booking.status == "SUCCESS"){
					$scope.DATA.current_booking = booking;

					if (fn.isFn(after_load_handler_function)) {
						after_load_handler_function(booking);
					}
				}else{
					alert("Sorry, data not found..");
					window.location = "#/trans_reservation";
				}
			},
			error : function (response){}
		});
	}
	$scope.loadDataBookingTransportPassenger = function(){
		
		booking_code = $stateParams.booking_code;
		
		var load_data_passenger = function (booking_detail){
			//console.log(booking_detail);
		}
		
		if ($scope.DATA.current_booking){
			
			$scope.DATA.current_booking = $scope.DATA.current_booking;
			
			load_data_passenger($scope.DATA.current_booking);
			
		}else{
			$scope.loadDataBookingTransportDetail(booking_code, function(current_booking){
				$scope.DATA.current_booking = current_booking;
				load_data_passenger($scope.DATA.current_booking);
			});
		}
	}
	
	
	$scope.editDataBookingTransportDetail = function(){
		$scope.loadDataBookingTransportDetail();
		
		//Load Data Available Port
		httpSrv.post({
			url 	: "transport/port/available_port",
			success : function(response){ 
				$scope.DATA.available_port = response.data;
			},
		});
		
	}
	$scope.edit_trip = function(trip){
		
		$scope.current_edit_trip = trip;
		
		$scope.check_availabilities_data = {};
		$scope.add_trip = {};
		$scope.add_trip.departure_date = trip.date;
		$scope.add_trip.departure_port_id = trip.departure.port.id;
		$scope.add_trip.arrival_port_id = trip.arrival.port.id;
		
		$scope.add_trip.participant = {};
		$scope.add_trip.participant.qty_1 = trip.qty_1;
		$scope.add_trip.participant.qty_2 = trip.qty_2;
		$scope.add_trip.participant.qty_3 = trip.qty_3;
		//$scope.add_trip.participant.qty_4 = trip.qty_4;
		//$scope.add_trip.participant.qty_5 = trip.qty_5;
		
		$scope.add_trip.min_participant = {};
		$scope.add_trip.min_participant = angular.copy($scope.add_trip.participant);
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		$("#modal-add-trip .btn-submit-edit-trip").removeAttr("disabled");
		$scope.check_availabilities_show_OK_button = false;
		
		//Load Data Available Port
		if (!$scope.$root.DATA_available_port){
			httpSrv.post({
				url 	: "transport/port/available_port",
				success : function(response){ $scope.$root.DATA_available_port = response.data.ports; },
			});
		}
		
	}
	$scope.check_availabilities_edit_trip = function(event){
		//$(event.target).find("button:submit").attr("disabled","disabled");
		
		var data_edit_trip = angular.copy($scope.add_trip);
		
		//fn.pre(data, "check_availabilities");
		
		var data = {};
		data.departure_date = data_edit_trip.departure_date;
		data.trip_model == "ONEWAY";
		
		if (data_edit_trip.change_trip){
			data.departure_port_id	= data_edit_trip.departure_port_id;
			data.arrival_port_id	= data_edit_trip.arrival_port_id;
		}else{
			data.departure_port_id	= $scope.current_edit_trip.departure.port.id;
			data.arrival_port_id	= $scope.current_edit_trip.arrival.port.id;
		}
		
		if (data_edit_trip.change_trip){
			data.adult	= data_edit_trip.participant.qty_1;
			data.child	= data_edit_trip.participant.qty_2;
			data.infant	= data_edit_trip.participant.qty_3;
		}else{
			data.adult	= $scope.current_edit_trip.qty_1;
			data.child	= $scope.current_edit_trip.qty_2;
			data.infant	= $scope.current_edit_trip.qty_3;	
		}
		$scope.add_trip.error_desc = [];
		$scope.check_availabilities_data = {};
		$scope.check_availabilities_show_loading = true;
		
		httpSrv.post({
			url 	: "agent_controller/check_availabilities",
			data	: $.param(data),
			success : function(response){
				$(event.target).find("button:submit").removeAttr("disabled");
				$scope.check_availabilities_show_loading = false;
				$scope.check_availabilities_show_searching_form = false;

				if (response.data.status == "SUCCESS"){
					$scope.check_availabilities_data = response.data;
					
				}else{
					$scope.add_trip.error_desc = response.data.error_desc;
					//$('html, body').animate({scrollTop:200}, 400);
				}
				
				//fn.pre($scope.check_availabilities_data, "check_availabilities_data");
				//$.hideLoading();
			}
		});
		
	}
	$scope.update_voucher_trip = function (){
		
		$scope.update_voucher_trip_error_desc = {};
		$scope.update_voucher_trip_error_desc.error_desc = [];
		$("#modal-add-trip .btn-submit-edit-trip").attr("disabled","disabled");
		
		var data_submit = {	"voucher_code"		: $scope.current_edit_trip.voucher_code,
							"date" 				: $scope.check_availabilities_data.departure.check.date,
							"rates_code"		: $scope.check_availabilities_data.departure.selected_trip.rates_code,
							"schedule_code"		: $scope.check_availabilities_data.departure.selected_trip.schedule.schedule_code,
							"applicable_rates"	: $scope.check_availabilities_data.departure.selected_trip.aplicable_rates,
							};
		
		httpSrv.post({
			url 	: "transport/booking/update_voucher",
			data	: $.param(data_submit),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.editDataBookingTransportDetail();
					$("#modal-add-trip").modal("hide");
					toastr.success("Updated...");
				}else{
					$scope.update_voucher_trip_error_desc.error_desc = response.data.error_desc;
					$("#modal-add-trip .btn-submit-edit-trip").removeAttr("disabled");
				}
				//$.hideLoading();
			}
		});
		
		//$scope.DATA_r = data_submit;
		//console.log(data_submit);
	}
	
	
	$scope.newReservationTransport = function(){
		$scope.DATA.data_rsv = {"booking_source" : "OFFLINE",
								"booking_status" : "DEFINITE",
								"discount_type"	 : "%",
								"discount_amount": 0,
								"selected_trips" : [],
								"TOTAL"			 : {"total":0,"discount":0}};
		
		//$scope.DATA.data_rsv = {"discount_amount":10,"discount_type":"%","booking_source":"OFFLINE","booking_status":"DEFINITE","selected_trips":[{"date":"2017-05-30","adult":1,"child":0,"infant":0,"trips":{"id":"710","rates_code":"TDQ5341","name":"Sanur Lembongan Publish Rates","description":"","booking_handling":"INSTANT","departure":{"port":{"id":"10","name":"Sanur","port_code":"SNR"},"time":"09:30","trip_number":"1"},"arrival":{"port":{"id":"3","name":"Lembongan","port_code":"LEM"},"time":"10:00","trip_number":"1"},"start_date":"2017-05-01","end_date":"2017-12-31","available_on":"all_day","min_order":1,"currency":"IDR","rates":{"one_way_rates":{"rates_1":350000,"rates_2":300000,"rates_3":0},"return_rates":{"rates_1":300000,"rates_2":275000,"rates_3":0}},"cut_of_booking":0,"pickup_service":"yes","dropoff_service":"yes","publish_status":"1","lang":"EN","pickup_area":[{"id":"25258","area":"Sanur","time":"08:00","price":100000,"currency":"IDR","type":"way"},{"id":"25259","area":"Kuta","time":"08:00","price":0,"currency":"IDR","type":"way"}],"dropoff_area":[{"id":"25260","area":"All Lembongan","time":"09:00","price":0,"currency":"IDR","type":"way"},{"id":"25261","area":"All Lembongan VIP","time":"09:00","price":200000,"currency":"IDR","type":"way"}],"aplicable_rates":{"rates_1":300000,"rates_2":275000,"rates_3":0},"schedule":{"id":"9","schedule_code":"TDQ0507","name":"Sanur, Lembongan, Lombok, Inter Island","description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. \n\nIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.","publish_status":"1","lang":"EN","boat":{"id":"62","boat_code":"TDQ9954","name":"Bima X 01.50","capacity":"50","main_image":"https://bes.hybridbooking.com/image/100/500/0/public/images/no_image.jpg","available_space":32}},"is_hidden":false,"is_selected":true},"sub_total":300000,"pickup":{"area":{"id":"25258","area":"Sanur","time":"08:00","price":100000,"currency":"IDR","type":"way"},"hotel_name":"Sanur Beach Hotel","hotel_address":"Jl. By Pass Sanur","hotel_phone":"0361 126387"},"dropoff":{"area":{"id":"25261","area":"All Lembongan VIP","time":"09:00","price":200000,"currency":"IDR","type":"way"},"hotel_name":"Sanur Beach Hotel","hotel_address":"Jl. By Pass Sanur","hotel_phone":"0361 126387"}},{"date":"2017-06-02","adult":1,"child":0,"infant":0,"trips":{"id":"704","rates_code":"TDQ4650","name":"Lembongan Sanur Publish Rates 1","description":"","booking_handling":"INSTANT","departure":{"port":{"id":"3","name":"Lembongan","port_code":"LEM"},"time":"08:00","trip_number":"1"},"arrival":{"port":{"id":"10","name":"Sanur","port_code":"SNR"},"time":"08:30","trip_number":"1"},"start_date":"2017-05-01","end_date":"2017-12-31","available_on":"all_day","min_order":1,"currency":"IDR","rates":{"one_way_rates":{"rates_1":350000,"rates_2":300000,"rates_3":0},"return_rates":{"rates_1":300000,"rates_2":275000,"rates_3":0}},"cut_of_booking":0,"pickup_service":"yes","dropoff_service":"yes","publish_status":"1","lang":"EN","pickup_area":false,"dropoff_area":false,"aplicable_rates":{"rates_1":300000,"rates_2":275000,"rates_3":0},"schedule":{"id":"10","schedule_code":"TDQ1015","name":"Sanur Lembongan","description":"Desc to Test New Schedule","publish_status":"1","lang":"EN","boat":{"id":"63","boat_code":"TDQ6783","name":"Bima X Small","capacity":"20","main_image":"https://bes.hybridbooking.com/image/100/500/0/public/images/no_image.jpg","available_space":2}},"is_hidden":false,"is_selected":true},"sub_total":300000,"pickup":{"area":{"text":"Own Transport","nopickup":true,"currency":"IDR"}},"dropoff":{"area":{"text":"Own Transport","nopickup":true,"currency":"IDR"}}}],"TOTAL":{"total":900000,"discount":90000,"grand_total":810000},"customer":{"first_name":"Suta","last_name":"Darmayasa","email":"suta@gmail.com","phone":"0852863786712","country_code":"ID"},"remarks":"Minta disediakan tour guide.."};
		$scope.calculate_total();

		//fn.pre($scope.DATA.data_rsv);

		//Load Data Agent
		httpSrv.post({
			url 	: "agent",
			success : function(response){ $scope.DATA.agents = response.data; }
		});
		
		//Load Data Country List
		httpSrv.post({
			url 	: "general/country_list",
			success : function(response){ $scope.DATA.country_list = response.data; },
		});
		
		//Load Data Available Port
		httpSrv.post({
			url 	: "transport/port/available_port",
			success : function(response){ $scope.DATA.available_port = response.data; },
		});
		
		//Load Data Booking Source
		httpSrv.post({
			url 	: "setting/booking_source",
			success : function(response){ $scope.DATA.booking_source= response.data; },
		});
		
		//Load Data Booking Status
		httpSrv.post({
			url 	: "setting/booking_status",
			success : function(response){ $scope.DATA.booking_status = response.data; },
		});

		httpSrv.post({
			url 	: "setting/payment_method",
			success : function(response){ $scope.DATA.payment_method = response.data; }
		})
	}
	$scope.add_new_trip = function(){
		$scope.add_trip = {};
		$scope.check_availabilities_data = {};
		
		if ($scope.check_availabilities_data.departure) $scope.check_availabilities_data.departure.selected_trip = {};
		if ($scope.check_availabilities_data.return) $scope.check_availabilities_data.return.selected_trip = {};
		$scope.check_availabilities_show_OK_button = false;
		
		$scope.add_trip = {"trip_model":"ONEWAY","adult":1,"child":0,"infant":0};
		
		
		//$scope.add_trip = {"trip_model":"ONEWAY","adult":35,"child":0,"infant":0,"departure_port_id":"1","destination_port_id":"7","departure_date":"2017-08-10","return_date":"2017-08-11","arrival_port_id":"7"};
		//$scope.check_availabilities(event);
		
		$("#modal-add-trip .departure-port").focus();
		
		//$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd", minDate: fn.formatDate(new Date(),"yy-mm-dd")});
	}
	
	$scope.check_availabilities = function(event){
		//$.showLoading();
		
		var data = angular.copy($scope.add_trip);
		
		//fn.pre(data, "check_availabilities");
		
		if (data.trip_model == "ONEWAY"){
			delete data.return_date;
		}
		
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.add_trip.error_desc = [];
		$scope.check_availabilities_data = {};
		$scope.check_availabilities_show_loading = true;
		
		httpSrv.post({
			url 	: "agent_controller/check_availabilities",
			data	: $.param(data),
			success : function(response){
				$(event.target).find("button:submit").removeAttr("disabled");
				$scope.check_availabilities_show_loading = false;
				$scope.check_availabilities_show_searching_form = false;

				if (response.data.status == "SUCCESS"){
					$scope.check_availabilities_data = response.data;

				}else{
					$scope.add_trip.error_desc = response.data.error_desc;
					//$('html, body').animate({scrollTop:200}, 400);
				}
				
				//fn.pre($scope.check_availabilities_data, "check_availabilities_data");
				//$.hideLoading();
			}
		});
		
	}

	
	$scope.check_availabilities_select_this_trip = function (trip, trips_parent){
		//fn.pre(trip);
		for (index in trips_parent.availabilities){
			if (trips_parent.selected_trip){
				delete trips_parent.availabilities[index].is_hidden;
			}else{
				trips_parent.availabilities[index].is_hidden = true;
			}
			delete trips_parent.availabilities[index].is_selected;
		}
		if (trips_parent.selected_trip){
			delete trips_parent.selected_trip;
		}else{
			trip.is_selected = true;
			trip.is_hidden = false;
			
			trips_parent.selected_trip = trip;
		}
		
		$scope.check_availabilities_show_OK_button = false;
		if ($scope.add_trip.trip_model == "RETURN"){
			if ($scope.check_availabilities_data.departure.selected_trip && $scope.check_availabilities_data.return.selected_trip){
				$scope.check_availabilities_show_OK_button = true;
			}
		}else{
			if ($scope.check_availabilities_data.departure.selected_trip){
				$scope.check_availabilities_show_OK_button = true;
			}
		}
	}
	
	$scope.printReceiptTrans = function(){
		booking_code = $stateParams.booking_code;
		$scope.loadDataBookingTransportDetail(booking_code, function(){
			$scope.$parent.$parent.show_print_button = true;
		});
	}
	$scope.printVoucherTrans = function(){
		booking_code = $stateParams.booking_code;
		voucher_code = $stateParams.voucher_code;
		
		httpSrv.post({
			url 	: "setting/terms_and_conditions",
			success : function(response){ $scope.terms_and_conditions = response.data.terms_and_conditions; },
		});
		
		$scope.loadDataBookingTransportDetail(booking_code, function(booking_detail){
			$scope.$parent.$parent.show_print_button = true;
			
			$scope.DATA.voucher = {};
			
			for (i in booking_detail.booking.detail){
				if (voucher_code == booking_detail.booking.detail[i].voucher_code){
					$scope.DATA.voucher = booking_detail.booking.detail[i];
					break;
				}
			}

		});
	}
	
	$scope.aplicable_rates_for_agent = function(){
		var selected_trips = $scope.DATA.data_rsv.selected_trips;
			
		if ($scope.DATA.data_rsv.booking_source == "AGENT" && $scope.DATA.data_rsv.agent){
			
			var agent = $scope.DATA.data_rsv.agent;
			
			for (i in selected_trips){
				var _rates = selected_trips[i].trips.aplicable_rates_original;
				selected_trips[i].trips.aplicable_rates.rates_1 = _rates.rates_1 - (_rates.rates_1 * agent.category.percentage/100);
				selected_trips[i].trips.aplicable_rates.rates_2 = _rates.rates_2 - (_rates.rates_2 * agent.category.percentage/100);
				selected_trips[i].trips.aplicable_rates.rates_3 = _rates.rates_3 - (_rates.rates_3 * agent.category.percentage/100);
				
				$scope.calculate_subtotal(selected_trips[i]);
			}
			
		}else{
			
			for (i in selected_trips){
				var _rates = selected_trips[i].trips.aplicable_rates_original;
				selected_trips[i].trips.aplicable_rates = angular.copy(_rates);
				
				$scope.calculate_subtotal(selected_trips[i]);
			}
			
		}
		$scope.calculate_total();
	}
	$scope._check_availabilities_add_to_data_rsv = function(trip){
		var selected_trips = {};
		
		selected_trips.date		= trip.check.date;
		
		selected_trips.adult 	= trip.check.adult;
		selected_trips.child 	= trip.check.child;
		selected_trips.infant 	= trip.check.infant;
		
		selected_trips.trips = trip.selected_trip;
		selected_trips.trips.aplicable_rates_original = angular.copy(trip.selected_trip.aplicable_rates);
		
		/*selected_trips.sub_total =  (selected_trips.adult*selected_trips.trips.aplicable_rates.rates_1) +
									(selected_trips.child*selected_trips.trips.aplicable_rates.rates_2) +
									(selected_trips.infant*selected_trips.trips.aplicable_rates.rates_3);*/
		
		$scope.DATA.data_rsv.selected_trips.push(selected_trips);
		
	}
	$scope.check_availabilities_add_to_data_rsv = function(){
		
		if ($scope.check_availabilities_data.departure && $scope.check_availabilities_data.departure.selected_trip){
			if ($scope.check_availabilities_data.departure.selected_trip.smart_pricing && $scope.check_availabilities_data.departure.selected_trip.smart_pricing.applicable_rates){
				
				var smart_pricing_applicable_rates = angular.copy($scope.check_availabilities_data.departure.selected_trip.smart_pricing.applicable_rates);
				
				for (_i in smart_pricing_applicable_rates){
					if (smart_pricing_applicable_rates[_i].allotment > 0){
						var departure_trip = angular.copy($scope.check_availabilities_data.departure);
						departure_trip.selected_trip.aplicable_rates = angular.copy(smart_pricing_applicable_rates[_i].rates);
						delete departure_trip.selected_trip.smart_pricing;
						departure_trip.selected_trip.smart_pricing = smart_pricing_applicable_rates[_i];
						
						departure_trip.check.adult 	= smart_pricing_applicable_rates[_i].allotment;
						departure_trip.check.child 	= 0;
						departure_trip.check.infant = 0;
						
						$scope._check_availabilities_add_to_data_rsv(departure_trip);
					}
				}
				
			}else{
				$scope._check_availabilities_add_to_data_rsv($scope.check_availabilities_data.departure);
			}
		}
		if ($scope.check_availabilities_data.return && $scope.check_availabilities_data.return.selected_trip){
			if ($scope.check_availabilities_data.return.selected_trip.smart_pricing && $scope.check_availabilities_data.return.selected_trip.smart_pricing.applicable_rates){
				
				var smart_pricing_applicable_rates = angular.copy($scope.check_availabilities_data.return.selected_trip.smart_pricing.applicable_rates);
				
				for (_x in smart_pricing_applicable_rates){
					
					if (smart_pricing_applicable_rates[_x].allotment > 0){
						var return_trip = angular.copy($scope.check_availabilities_data.return);
						return_trip.selected_trip.aplicable_rates = angular.copy(smart_pricing_applicable_rates[_x].rates);
						delete return_trip.selected_trip.smart_pricing;
						return_trip.selected_trip.smart_pricing = smart_pricing_applicable_rates[_x];
						
						return_trip.check.adult 	= smart_pricing_applicable_rates[_x].allotment;
						return_trip.check.child 	= 0;
						return_trip.check.infant 	= 0;
						
						$scope._check_availabilities_add_to_data_rsv(return_trip);
					}
				}
				
			}else{
				$scope._check_availabilities_add_to_data_rsv($scope.check_availabilities_data.return);
			}
			//$scope._check_availabilities_add_to_data_rsv($scope.check_availabilities_data.return);
		}
		
		$scope.aplicable_rates_for_agent();

		$("#modal-add-trip").modal("hide");
		//console.log($scope.check_availabilities_data);
		//console.log($scope.DATA.data_rsv);
	}
	$scope.calculate_total = function(){
		var total = 0;
		for (x in $scope.DATA.data_rsv.selected_trips){
			
			var total_person = $scope.DATA.data_rsv.selected_trips[x].adult + $scope.DATA.data_rsv.selected_trips[x].child;
			
			total += $scope.DATA.data_rsv.selected_trips[x].sub_total;
			
			//Pickup
			if ($scope.DATA.data_rsv.selected_trips[x].pickup && $scope.DATA.data_rsv.selected_trips[x].pickup.area.price){
				if ($scope.DATA.data_rsv.selected_trips[x].pickup.area.type == 'way'){
					pickup_sub_total = $scope.DATA.data_rsv.selected_trips[x].pickup.area.price;
				}else{
					pickup_sub_total = $scope.DATA.data_rsv.selected_trips[x].pickup.area.price * total_person;
				}
				total += pickup_sub_total;
			}
			//Dropoff
			if ($scope.DATA.data_rsv.selected_trips[x].dropoff && $scope.DATA.data_rsv.selected_trips[x].dropoff.area.price){
				if ($scope.DATA.data_rsv.selected_trips[x].pickup.area.type == 'way'){
					dropoff_sub_total = $scope.DATA.data_rsv.selected_trips[x].dropoff.area.price;
				}else{
					dropoff_sub_total = $scope.DATA.data_rsv.selected_trips[x].dropoff.area.price * total_person;
				}
				total += dropoff_sub_total;
			}
			//Additional Service
			if ($scope.DATA.data_rsv.selected_trips[x].additional_service){
				for (y in $scope.DATA.data_rsv.selected_trips[x].additional_service){
					total += ($scope.DATA.data_rsv.selected_trips[x].additional_service[y].qty * $scope.DATA.data_rsv.selected_trips[x].additional_service[y].price);
				}
			}
		}
		//$scope.DATA.data_rsv.TOTAL = {"total":0};
		$scope.DATA.data_rsv.TOTAL.total = total;
		
		$scope.calculate_discount();
		
		$scope.DATA.data_rsv.TOTAL.grand_total = total - $scope.DATA.data_rsv.TOTAL.discount;
		
		if (!$scope.DATA.data_rsv.payment){
			$scope.DATA.data_rsv.payment = {};
		}
		
		$scope.DATA.data_rsv.payment.payment_amount = $scope.DATA.data_rsv.TOTAL.grand_total;
		$scope.DATA.data_rsv.payment.balance = $scope.DATA.data_rsv.TOTAL.grand_total - $scope.DATA.data_rsv.payment.payment_amount;
	}
	$scope.calculate_subtotal = function(trips){
		trips.sub_total =  	(trips.adult*trips.trips.aplicable_rates.rates_1) +
							(trips.child*trips.trips.aplicable_rates.rates_2) +
							(trips.infant*trips.trips.aplicable_rates.rates_3);
		
		$scope.calculate_total();
	}
	$scope.calculate_discount = function(){
		var discount = 0;
		if ($scope.DATA.data_rsv.discount_type=='%'){
			if ($scope.DATA.data_rsv.discount_amount > 100) $scope.DATA.data_rsv.discount_amount = 100;
			discount = $scope.DATA.data_rsv.TOTAL.total * $scope.DATA.data_rsv.discount_amount/100;
		}else{
			discount = angular.copy($scope.DATA.data_rsv.discount_amount);
		}
		$scope.DATA.data_rsv.TOTAL.discount = discount;
	}
	$scope.remove_trip = function (index){
		if (confirm("Are you sure to delete this trip?")){
			$scope.DATA.data_rsv.selected_trips.splice(index,1);
			$scope.calculate_total();
		}
	}
	$scope.addAdditionalService = function (trip){
		var additional = {"name":"", "qty":1, "price":0};
		if (!trip.additional_service){
			trip.additional_service = [];
		}
		trip.additional_service.push(additional);
	}
	$scope.removeAdditionalService = function (trips, index){
		trips.additional_service.splice(index,1);
	}

	$scope.loadBookingStatus = function()
	{
		//Load Data Booking Status
		if (!$scope.DATA_booking_status){
			httpSrv.post({
				url 	: "setting/booking_status",
				success : function(response){ $scope.DATA_booking_status = response.data.booking_status; },
			});
		}
	}
	$scope.loadBookingSource = function()
	{
		//Load Data Booking Source
		if (!$scope.$root.DATA_booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.$root.DATA_booking_source = response.data.booking_source; },
			});
		}
	}
	
	$scope.submit_booking = function(event){
		//';window.location = "#/trans_reservation/";
		
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.data_rsv.error_desc = [];
		
		var data_rsv = {};
		var ss_data_rsv = angular.copy($scope.DATA.data_rsv);
		
		//data_rsv.booking_source = ss_data_rsv.booking_source;
		data_rsv.booking_source = "AGENT"
		data_rsv.agent_code = AGENT_CODE;

		data_rsv.booking_status = "DEFINITE";
		data_rsv.discount 		= {"type":ss_data_rsv.discount_type, "amount":ss_data_rsv.discount_amount};
		data_rsv.remarks 		= ss_data_rsv.remarks;
		data_rsv.customer 		= ss_data_rsv.customer;
		data_rsv.trips 			= [];
		
		for (i in ss_data_rsv.selected_trips){
			var trip = {};
			
			trip.date 	= ss_data_rsv.selected_trips[i].date;
			trip.adult 	= ss_data_rsv.selected_trips[i].adult;
			trip.child 	= ss_data_rsv.selected_trips[i].child;
			trip.infant	= ss_data_rsv.selected_trips[i].infant;
			
			trip.schedule_code	= ss_data_rsv.selected_trips[i].trips.schedule.schedule_code;
			trip.rates_code		= ss_data_rsv.selected_trips[i].trips.rates_code;
			trip.applicable_rates = ss_data_rsv.selected_trips[i].trips.aplicable_rates;

			if (ss_data_rsv.selected_trips[i].trips.smart_pricing){
				//console.log(ss_data_rsv.selected_trips[i].trips.smart_pricing);
				
				if (ss_data_rsv.selected_trips[i].trips.smart_pricing && ss_data_rsv.selected_trips[i].trips.smart_pricing.type == "SMARTPRICING"){
					trip.smart_pricing_level = ss_data_rsv.selected_trips[i].trips.smart_pricing.level;
				}
				
				//if (ss_data_rsv.selected_trips[i].trips.smart_pricing.level || ss_data_rsv.selected_trips[i].trips.smart_pricing.level === 0){
					//alert(ss_data_rsv.selected_trips[i].trips.smart_pricing.level);
				//	trip.smart_pricing_level = ss_data_rsv.selected_trips[i].trips.smart_pricing.level;
				//}
			}
			
			if (ss_data_rsv.selected_trips[i].trips.is_return && ss_data_rsv.selected_trips[i].trips.is_return == "yes"){
				trip.is_return	= "yes";
			}
			
			//Check Pickup Service
			if (ss_data_rsv.selected_trips[i].pickup 
				&& ss_data_rsv.selected_trips[i].pickup.area 
				&& !ss_data_rsv.selected_trips[i].pickup.area.nopickup){
				
				trip.pickup = ss_data_rsv.selected_trips[i].pickup;
				trip.pickup.area_id = trip.pickup.area.id;
				trip.pickup.price 	= trip.pickup.area.price;
				delete trip.pickup.area;
				
			}
			//Check Dropoff Service
			if (ss_data_rsv.selected_trips[i].dropoff 
				&& ss_data_rsv.selected_trips[i].dropoff.area 
				&& !ss_data_rsv.selected_trips[i].dropoff.area.nopickup){
				
				trip.dropoff = ss_data_rsv.selected_trips[i].dropoff;
				trip.dropoff.area_id = trip.dropoff.area.id;
				trip.dropoff.price 	 = trip.dropoff.area.price;
				delete trip.dropoff.area;
				
			}
			
			//Check Additional Service
			if (ss_data_rsv.selected_trips[i].additional_service){
				
				trip.additional_service = ss_data_rsv.selected_trips[i].additional_service;
				
			}
			
			data_rsv.trips.push(trip);
			
		}//End For
		
		if ($scope.DATA.data_rsv.add_payment == '1' && $scope.DATA.data_rsv.booking_status != 'COMPLIMEN'){
			var payment = $scope.DATA.data_rsv.payment;
			data_rsv.payment = {"amount" 		: payment.payment_amount,
								"payment_type"	: payment.payment_type,
								"account_number": payment.account_number,
								"name_on_card"	: payment.name_on_card,
								"bank_name"		: payment.bank_name,
								"payment_reff_number" : payment.payment_reff_number,
								"description"	: payment.description};
			if (payment.payment_type == "OPENVOUCHER"){
				data_rsv.payment.openvoucher_code = payment.openvoucher_code
			}
		}
		
		//$scope.DATA_RSV = data_rsv;
		//console.log(data_rsv);
		//return;
		
		httpSrv.post({
			url 	: "agent_controller/create_reservation",
			data	: $.param(data_rsv),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					// window.location = "#/trans_reservation/detail/"+response.data.booking_code+"/passenger";
					
					$location.url('/detail_passenger/'+response.data.booking_code);
					toastr.success("Saved...");
				}else{
					$(event.target).find("button:submit").attr("disabled","disabled");
					$scope.DATA.data_rsv.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
					$('html, body').animate({scrollTop:200}, 400);
				}
				//$.hideLoading();
			}
		});
		
		$scope.DATA_RSV = data_rsv;
		
		//fn.pre(ss_data_rsv,"ss_data_rsv");
		//fn.pre(data_rsv,"data_rsv");
	}


	$scope.cancelationFee = function(booking_code){

		httpSrv.get({
			url 	: "transport/booking/cancelation/"+booking_code,
			success : function(response){
				
				var booking = response.data;
				
				if (booking.status == "SUCCESS"){
					$scope.cancelation_fee = booking.booking.cancelation;
					
				}else{
					alert("Sorry, data not found..");
					window.location = "#/trans_reservation/detail/"+booking_code;
				}
			},
			error : function (response){}
		});
	}

	$scope.chkCancelAllBookingClick = function(check_cancel){
		if (!check_cancel){
			var is_cancel_all = $scope.DATA.cancelation.cancel_all_trip;
	
			for (code in $scope.DATA.cancelation.voucher){
				$scope.DATA.cancelation.voucher[code].is_cancel = is_cancel_all;
			}
		}else{
			var is_cancel_all = 1;
			for (code in $scope.DATA.cancelation.voucher){
				if(!$scope.DATA.cancelation.voucher[code].is_cancel){
					is_cancel_all = 0;
					break;
				}
			}
			$scope.DATA.cancelation.cancel_all_trip = is_cancel_all;
		}
	}
	$scope.saveCancelation = function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.cancelation.error_desc = [];
		
		var data = {};
		
		//Cancel Voucher Full
		if (!$scope.DATA.cancelation.is_cancel_partial){
			data = {};
			data = {"booking_code" 			: $scope.DATA.current_booking.booking.booking_code,
					"cancelation_reason"	: $scope.DATA.cancelation.cancelation_reason}
			
			if ($scope.DATA.cancelation.cancel_all_trip){
				data.cancel_all_trip = 1;
			}
			data.voucher_code = [];
			for (code in $scope.DATA.cancelation.voucher){
				if ($scope.DATA.cancelation.voucher[code].is_cancel){
					if ($scope.cancelation_fee) {
						$scope.DATA.cancelation.voucher[code].fee = $scope.cancelation_fee[code]['pay'];
					}
					data.voucher_code.push({"code":code, "fee":$scope.DATA.cancelation.voucher[code].fee});
				}
			}
		}
		//Partial Cancel Voucher
		else if ($scope.DATA.cancelation.is_cancel_partial){
			data = {};
			data = {"booking_code" 			: $scope.DATA.current_booking.booking.booking_code,
					"cancelation_reason"	: $scope.DATA.cancelation.cancelation_reason}
			data.cancel_all_trip = 0;
			
			var _voucher = {"code"	: $scope.DATA.cancelation.partial_cancel.voucher.voucher_code, 
							"fee"	: ($scope.DATA.cancelation.partial_cancel.voucher.cancelation_fee * $scope.DATA.cancelation.partial_cancel.qty)};
			
			_voucher.passengers = [];
			
			for (_x in $scope.DATA.cancelation.partial_cancel.voucher.passenger){
				if ($scope.DATA.cancelation.partial_cancel.voucher.passenger[_x].selected){
					_voucher.passengers.push({"passenger_code"	: $scope.DATA.cancelation.partial_cancel.voucher.passenger[_x].passenger_code,
											  "type"			: $scope.DATA.cancelation.partial_cancel.voucher.passenger[_x].type.code});
				}
			}
			
			data.voucher_code = [];
			data.voucher_code.push(_voucher);
		}
		
		//REFUND
		if ($scope.DATA.cancelation.is_add_refund == '1'){
			data.refund_amount 	= $scope.DATA.cancelation.refund_amount;
			data.currency		= $scope.DATA.current_booking.booking.currency;
		}
		//--
		
		//console.log($scope.DATA.cancelation);
		//$scope.DATASS = data;
		//console.log(data);
		//return
				
		httpSrv.post({
			url 	: "booking/cancel",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					toastr.success("Saved...");
					$scope.loadDataBookingTransportDetail();
					$("#cancel-booking-form").modal("hide");
				}else{
					$(event.target).find("button:submit").attr("disabled","disabled");
					$scope.DATA.cancelation.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
				}
				//$.hideLoading();
			}
		});
	}
	
	//Refund
	$scope.refundBooking = function(booking){
		$scope.DATA.myRefund = {	'refund_amount' : 0,
									'description'	: ''};
	}
	$scope.saveRefund= function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myRefund.error_desc = [];
		
		var data = {"booking_code" 			: $scope.DATA.current_booking.booking.booking_code,
					"currency" 				: $scope.DATA.current_booking.booking.currency,
					"refund_amount"			: $scope.DATA.myRefund.refund_amount,
					"description"			: $scope.DATA.myRefund.description}
		
		httpSrv.post({
			url 	: "refund/create",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					toastr.success("Saved...");
					$scope.loadDataBookingTransportDetail();
					$("#refund-booking-form").modal("hide");
				}else{
					$(event.target).find("button:submit").attr("disabled","disabled");
					$scope.DATA.myRefund.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
				}
				//$.hideLoading();
			}
		});
	}
	//--
	
	
	$scope.loadDataBookingPayment = function(){
		booking_code = $stateParams.booking_code;
		
		if ($scope.DATA.current_booking){
			$scope.DATA.current_booking = $scope.DATA.current_booking;
		}else{
			$scope.loadDataBookingTransportDetail(booking_code, function(booking_detail){
				$scope.DATA.current_booking = booking_detail;
			});
		}

		$scope.booking_code = booking_code;
		
		httpSrv.get({
			url 	: "payment/reservation/"+booking_code,
			success : function(response){
				$scope.DATA.payment = response.data;
				
				//console.log($scope.DATA.payment);
			},
			error : function (response){}
		});
		
		httpSrv.get({
			url 	: "refund/reservation/"+booking_code,
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.DATA.refund = response.data;
				}
			},
			error : function (response){}
		});
	}
	$scope.changePaymentType = function(){
		if ($scope.DATA.myPayment.payment_type == 'OPENVOUCHER'){
			$scope.DATA.myPayment.openvoucher_code = [];
			var total_person = 0;
			
			for (i in $scope.DATA.current_booking.booking.detail){
				var detail = $scope.DATA.current_booking.booking.detail[i];

				total_person += detail.qty_1;
				total_person += detail.qty_2;
				total_person += detail.qty_3;
			}
			
			for (j=0; j<total_person; j++){
				$scope.DATA.myPayment.openvoucher_code.push({"code":""});
			}
			
			//console.log($scope.DATA.myPayment.openvoucher_code);
		}	
	}
	$scope.changePaymentTypeInNewBookingForm = function(){
		
		if ($scope.DATA.data_rsv.payment.payment_type == 'OPENVOUCHER'){
			$scope.DATA.data_rsv.payment.openvoucher_code = [];
			var total_person = 0;
			
			for (i in $scope.DATA.data_rsv.selected_trips){
				var detail = $scope.DATA.data_rsv.selected_trips[i];

				total_person += detail.adult;
				total_person += detail.child;
				total_person += detail.infant;
			}
			
			for (j=0; j<total_person; j++){
				$scope.DATA.data_rsv.payment.openvoucher_code.push({"code":""});
			}
			
			//console.log($scope.DATA.data_rsv.payment.openvoucher_code);
		}	
	}
	$scope.checkValidateOpenVoucherCode = function (ov_code){
		delete ov_code.loading; delete ov_code.valid; delete ov_code.invalid;
		
		if (ov_code.code != ''){
			ov_code.loading = true;
			
			httpSrv.post({
				url 	: "transport/open_voucher/check_openticket_code",
				data	: $.param({"openticket_code":ov_code.code}),
				success : function(response){
					var result = response.data;
					delete ov_code.loading;
					
					if (result.status == "ERROR") ov_code.invalid = true;
					else if (result.used) ov_code.invalid = true;
					else if (result.notyet_used) ov_code.valid = true;
				}
			});
		}
	}
	$scope.addEditPayment = function (current_payment){
		//Load Data Payment Method
		if (!$scope.$root.DATA_payment_method){
			httpSrv.post({
				url 	: "setting/payment_method",
				success : function(response){ $scope.$root.DATA_payment_method = response.data.payment_method; },
			});
		}
		
		if (current_payment){
			$scope.DATA.myPayment = angular.copy(current_payment);
		}else{
			$scope.DATA.myPayment = {"booking_code" : $scope.DATA.current_booking.booking.booking_code,
									 "date" : fn.formatDate(new Date(),"yy-mm-dd"),
									 "description":"Payment #"+$scope.DATA.current_booking.booking.booking_code,
									 "currency" : $scope.DATA.payment.payment.currency,
									 "amount" : $scope.DATA.payment.payment.balance};
		}

		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
	}
	$scope.saveDataPayment = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myPayment.error_msg = [];
		
		httpSrv.post({
			url 	: "payment/create/",
			data	: $.param($scope.DATA.myPayment),
			success : function(response){
				
				if (response.data.status == "SUCCESS"){
					$scope.loadDataBookingPayment();
					$("#add-edit-payment").modal("hide");
					toastr.success("Saved...");
				}else{
					$scope.DATA.myPayment.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	$scope.submitDeletePayment = function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.myPaymentDetail.error_msg = [];
		
		var data = {"payment_code" 	: $scope.myPaymentDetail.payment_code,
					"delete_remarks": $scope.myPaymentDetail.delete_remarks,
					"booking_code"  : $scope.DATA.current_booking.booking.booking_code};
		
		httpSrv.post({
			url 	: "payment/delete/",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataBookingPayment();
					$("#payment-detail").modal("hide");
					toastr.success("Deleted...");
				}else{
					$scope.myPaymentDetail.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
			}
		});
	}
	$scope.paymentDetail = function (current_payment, delete_payment){
		$scope.myPaymentDetail = angular.copy(current_payment);
		$scope.myPaymentDetail.amount = Math.abs($scope.myPaymentDetail.amount);
		if (delete_payment == true){
			$scope.myPaymentDetail.delete_payment = true;
			$scope.myPaymentDetail.sure_to_delete = '0';
		}else{
			delete $scope.myPaymentDetail.delete_payment;
		}
	}
	
	$scope.addEditPassenger = function (current_passenger, voucher){
		
		$scope.DATA.myPassenger = angular.copy(current_passenger);	
		
		if (current_passenger.is_data_updated != "1"){
			if (current_passenger.passenger_number == 0){
				var cus = angular.copy($scope.DATA.current_booking.booking.customer);
				$scope.DATA.myPassenger.first_name	= cus.first_name;
				$scope.DATA.myPassenger.last_name 	= cus.last_name;
				$scope.DATA.myPassenger.country_code= cus.country_code;
				$scope.DATA.myPassenger.country_name= cus.country_name;
				$scope.DATA.myPassenger.email 		= cus.email;
				$scope.DATA.myPassenger.phone 		= cus.phone;
			}else{
				$scope.DATA.myPassenger.first_name	= '';
				$scope.DATA.myPassenger.last_name 	= '';
			}
			
		}
		
		$scope.DATA.myPassenger.voucher = voucher;
		
		if (!$scope.DATA.country_list){
			httpSrv.get({
				url 	: "general/country_list",
				success : function(response){
					$scope.DATA.country_list = response.data;
				},
				error : function (response){}
			});
		}
	}
	$scope.saveDataPassenger = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myPassenger.error_msg = [];
		
		var data = angular.copy($scope.DATA.myPassenger);
		delete data.voucher;
		
		data.booking_code = $scope.DATA.current_booking.booking.booking_code;
		data.voucher_code = $scope.DATA.myPassenger.voucher.voucher_code;
		
		httpSrv.post({
			url 	: "transport/booking/passanger_create/",
			data	: $.param(data),
			success : function(response){
				
				if (response.data.status == "SUCCESS"){
					$scope.loadDataBookingTransportDetail();
					$("#add-edit-passenger").modal("hide");
					toastr.success("Saved...");
				}else{
					$scope.DATA.myPassenger.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	$scope.copyPassenger = function (index_from, index_to){
		if (confirm('Click OK to continue...')){
			var booking_detail 	= $scope.DATA.current_booking.booking.detail;
			var passenger_from 	= angular.copy(booking_detail[index_from].passenger);
			var passenger_to 	= angular.copy(booking_detail[index_to].passenger);
			
			for (i in passenger_to){
				var pss_code = passenger_to[i].passenger_code;
				passenger_to[i] = angular.copy(passenger_from[i]);
				passenger_to[i].passenger_code = pss_code;
			}
			
			var data = {};
			data.booking_code 	= $scope.DATA.current_booking.booking.booking_code;
			data.voucher_code 	= booking_detail[index_to].voucher_code;
			data.passanger		= passenger_to;
			
			httpSrv.post({
				url 	: "transport/booking/passanger_create_bulk/",
				data	: $.param(data),
				success : function(response){
					
					if (response.data.status == "SUCCESS"){
						$scope.loadDataBookingTransportDetail();
						$("#add-edit-passenger").modal("hide");
						toastr.success("Saved...");
					}else{
						alert(response.data.error_desc);
						//$scope.DATA.myPassenger.error_msg = response.data.error_desc;
					}
					$(event.target).find("button:submit").removeAttr("disabled");
					//$.hideLoading();
				}
			});

		}
	}
	

	$scope.arrivalDepartureTrans = function(){
		GeneralJS.activateLeftMenu("arrival_departure");	
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		
		$scope.dept = {	"type" : "departure",
						"date" : fn.formatDate(new Date(), "yy-mm-dd")};
		
		httpSrv.post({
			url 	: "transport/boats",
			success : function(response){
				$scope.DATA.boats = response.data;
				if ($scope.DATA.boats && $scope.DATA.boats.boats[0]){
					$scope.dept.boat = $scope.DATA.boats.boats[0];
				}
			}
		});
		httpSrv.post({
			url 	: "transport/port/available_port",
			success : function(response){
				$scope.DATA.ports = response.data;
				
				$scope.DATA.ports.ports.push({"id":"ALL","name":"All Port","port_code":"ALL"});
				$scope.selectArrivalDepartureType();
				
				//Generate Data Ports By Code
				var ports_by_code = {};
				for (i in $scope.DATA.ports.ports){
					var port = $scope.DATA.ports.ports[i];
					ports_by_code[port.port_code] = port;
				}
				$scope.DATA.ports_by_code = ports_by_code;
				//console.log($scope.DATA.ports_by_code);
			}
		});
		
	}
	$scope.arrivalDepartureTime = function(){
		
		if ($scope.dept && $scope.dept.date && $scope.dept.boat && $scope.dept.departure && $scope.dept.destination){
			$scope.DATA.time = [];
			
			var data = {"type" 				: $scope.dept.type,
						"date" 				: $scope.dept.date,
						"boat_id" 			: $scope.dept.boat.id,
						"departure_port_id" : $scope.dept.departure.id,
						"arrival_port_id" 	: $scope.dept.destination.id};
			
			httpSrv.post({
				url 	: "transport/schedule/arrival_departure_time",
				data	: $.param(data),
				success : function(response){
					if (response.data.status == "SUCCESS"){
						$scope.DATA.time = response.data.arrival_departure_time;
						//if ($scope.DATA.time[0]){
						//	$scope.dept.time = $scope.DATA.time[0];
						//}
					}
				}
			});
		}
	}

	$scope.arrivalDepartureTransPrint = function(){
		
		var data = {"type" 				: $stateParams.search_type,
					"date" 				: $stateParams.search_date,
					"departure_port_id" : $stateParams.search_departure_port_id,
					"arrival_port_id" 	: $stateParams.search_destination_port_id,
					"boat_id"		: $stateParams.search_schedule_code};
		
		$scope.arrivalDepartureTransSearch(data, function(arrival_departure){$scope.$parent.$parent.show_print_button = true;});
	}

	$scope.selectArrivalDepartureType = function(){
		
		$scope.DATA.ports_arrival	= angular.copy($scope.DATA.ports);
		$scope.DATA.ports_departure	= angular.copy($scope.DATA.ports);
		
		if ($scope.dept.type == 'arrival'){
			$scope.DATA.ports_departure.ports.splice($scope.DATA.ports_departure.ports.length-1, 1);
		}else if ($scope.dept.type == 'departure'){
			$scope.DATA.ports_arrival.ports.splice($scope.DATA.ports_arrival.ports.length-1, 1);
		}
	}
	
	$scope.openPopUpWindowVoucher = function (base_url, booking_code, voucher_code){
		var myWindow = window.open(base_url+booking_code+'/'+voucher_code, 'VoucherWindow', 'width=800,height=600');
	}
	
	$scope.pickupDropoffTrans = function(){
		GeneralJS.activateLeftMenu("arrival_departure");	
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		
		$scope.dept = {	"type" : "pickup",
						"date" : fn.formatDate(new Date(), "yy-mm-dd")};
		$scope.pickupDropofTransSearch();
	}

	$scope.pickupDropofTransPrint = function(){
		var data = {"type":$stateParams.search_type,
					"date":$stateParams.search_date};
		
		$scope.pickupDropofTransSearch(data, function(pickup_dropoff){$scope.$parent.$parent.show_print_button = true;});
	}
	
	$scope.departureTransGetScheduleDetail = function(){
		if (!$scope.DATA.schedules_detail){
			$scope.DATA.schedules_detail = {};
		}
		
		if (!$scope.DATA.schedules_detail[$scope.dept.schedule.schedule_code]){
			httpSrv.get({
				url 	: "transport/schedule/detail/"+$scope.dept.schedule.schedule_code,
				success : function(response_detail){
					$scope.DATA.schedules_detail[$scope.dept.schedule.schedule_code] = response_detail.data;
				},
				error : function (response){}
			});
		}
	}

	// obtain pop-up data current customer
	$scope.edit_cust = function(cust){
		$scope.DATA.update_cust = {};
		$scope.DATA.update_cust.error_desc = [];

		// scope data customer
		$scope.DATA.current_edit_cust = cust;

		// get country list
		httpSrv.get({
			url 	: "general/country_list",
			success : function(response){
				$scope.DATA.country = response.data;
			},
			error : function (response){}
		});
		$("#modal-add-cust .btn-submit-edit-cust").removeAttr("disabled");
	}

	// update current customer
	$scope.update_cust = function (){
		$scope.DATA.update_cust = {};
		$scope.DATA.update_cust.error_desc = [];

		$("#modal-add-cust .btn-submit-edit-cust").attr("disabled","disabled");
		
		var data_submit = {	"id_member" : $scope.DATA.current_edit_cust.id,
							"first_name": $scope.DATA.current_edit_cust.first_name,
							"last_name" : $scope.DATA.current_edit_cust.last_name,
							"email"		: $scope.DATA.current_edit_cust.email,
							"mobile"	: $scope.DATA.current_edit_cust.phone,
							"country"	: $scope.DATA.current_edit_cust.country_code
						  };

		httpSrv.post({
			url 	: "customer/update",
			data	: $.param(data_submit),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.editDataBookingTransportDetail();
					$("#modal-add-cust").modal("hide");
					toastr.success("Updated!");
				}else{
					$scope.DATA.update_cust.error_desc = response.data.error_desc;
					$("#modal-add-cust .btn-submit-edit-cust").removeAttr("disabled");
				}
			}
		});
	}

	// cancel update customer
	$scope.cancel_cust = function(){
		$scope.editDataBookingTransportDetail();
	}

	// update remarks
	$scope.update_remarks = function(event){
		var booking_code = $scope.DATA.current_booking.booking.booking_code;
		var remarks		 = $scope.DATA.current_booking.booking.remarks;
		httpSrv.post({
			url 	: "transport/booking/update_remarks",
			data	: $.param({"booking_code": booking_code, "remarks": remarks}),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					toastr.success("Remarks updated!");
				}
			}
		});
	}

	// obtain pop-up data current pickup/dropoff service
	$scope.edit_picdrp = function(type, id_booking_detail){
		$scope.DATA.data_picdrp = {};
		$scope.DATA.data_picdrp.error_desc = [];

		$("#modal-add-cust .btn-submit-edit-cust").removeAttr("disabled");
		$scope.DATA.current_picdrp = {};
		httpSrv.post({
			url     : "transport/booking/pickup_dropoff_bybookingdetail",
			data    : $.param({"type": type, "id_booking_detail": id_booking_detail}),
			success : function(response){
				if (response.data.status == "SUCCESS") {
					$scope.DATA.current_picdrp = response.data;
				}
			}
		})
	}

	// update pickup/dropoff
	$scope.update_picdrp = function(id_booking_detail){
		$scope.DATA.data_picdrp = {};
		$scope.DATA.data_picdrp.error_desc = [];
		$("#modal-add-cust .btn-submit-edit-cust").attr("disabled","disabled");
		var data_submit = {	"id_booking_detail" 	: $scope.DATA.current_picdrp.picdrp.id_booking_detail,
							"type"					: $scope.DATA.current_picdrp.picdrp.type,
							"hotel_name" 			: $scope.DATA.current_picdrp.picdrp.hotel_name,
							"hotel_address"			: $scope.DATA.current_picdrp.picdrp.hotel_address,
							"hotel_phone_number"	: $scope.DATA.current_picdrp.picdrp.hotel_phone_number
						  };

		httpSrv.post({
			url 	: "transport/booking/update_picdrp",
			data	: $.param(data_submit),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.editDataBookingTransportDetail();
					$("#modal-add-picdrp").modal("hide");
					toastr.success("Updated!");
				}else{
					$scope.DATA.data_picdrp.error_desc = response.data.error_desc;
					$("#modal-add-picdrp .btn-submit-edit-picdrp").removeAttr("disabled");
				}
			}
		});
	}
});

function activate_sub_menu_agent_detail(class_menu){
	$(".reservation-detail ul.nav.sub-nav li").removeClass("active");
	$(".reservation-detail ul.nav.sub-nav li."+class_menu).addClass("active");
}

// JavaScript Document 
angular.module('app').controller('reservation_controller',function($location, $state, $scope, $http, httpSrv, $stateParams, $interval, fn,$filter, $window, webStorage, $cookies, cart){
	
	GeneralJS.activateLeftMenu("vendor");
	
	$scope.DATA = [];
	$scope.fn = fn;
	$scope._cart = cart;
	$scope.id_vendor = '';
	$scope.add_product = {};
	$scope.CART = [];

	$scope.RSV = [];
	$scope.KEY = AGENT_CODE;
	$scope.gallery = {};
	$scope.product_detail  = [];
	$scope.product_rate  = [];
	$scope.count = 0;
	$scope.DATA.cart = [];


	$scope.get_total = function(cart)
	{
		var total = 0;
		angular.forEach(cart, function(data){

			total += (data.adult * data.applicable_rates.rates_1) + (data.child * data.applicable_rates.rates_2) + (data.infant * data.applicable_rates.rates_3);
 			
		});

		$scope.CART.grand_total = total;
	}

	$scope.putCart = function(value){
		
		var key = $scope.getCookies();
	
		httpSrv.post({
			url 	: "session/getSessionBooking",
			data	: $.param({"key":key}),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					var cart = response.data.cart.session_value;
					$scope.saveCart(cart, value, 1);
					
				}else{

					cart = [];
					$scope.saveCart(cart, value, 0);
					
				}
				
			}
		});
	}

	$scope.saveCart = function(cart, value, issnul){
		
		if (cart) {
			$scope.CART = angular.copy(cart);
		}else{
			$scope.CART = [];
		}
			
		if (issnul==1) {
			var key = $scope.getCookies();
			if (value.length > 1) {
				
				angular.forEach(value, function(data, index){
					
					if (value.length == (index+1)) {
						data.type = 'RETURN';
					}
					$scope.CART.push(data);
					
				});

			}else{
				value.type = 'ONEWAY';
				$scope.CART.push(value);
			}
			
		
		}else{
			var key = '';
			
			if (value.length > 1) {
				
				angular.forEach(value, function(data, index){
					
					if (value.length == (index+1)) {
						data.type = 'RETURN';
					}
					$scope.CART.push(data);
					
				});

			}else{
				value.type = 'ONEWAY';
				$scope.CART.push(value);
			}
		}

		httpSrv.post({
			url 	: "session/createSessionBooking",
			data	: $.param({"session_value":$scope.CART, "key":key}),
			success : function(response){

				$scope.key = response.data.key;	

				if (response.data.status == "SUCCESS"){
					
					$scope.setCookies($scope.key);

					var result = { key:$scope.key };
					$state.go('cart', result);
		
				}

			}
		});
	}

	$scope.getCart = function(key = ''){
			
		if (key=='') {
			var key = $scope.getCookies();
		}
	
		
		httpSrv.post({
			url 	: "session/getSessionBooking",
			data	: $.param({"key":key}),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					var value = response.data.cart.session_value;
					$scope.generateCart(value);

				}else{
					
				}
				//$.hideLoading();
			}
		});

		
	}

	$scope.generateCart = function(cart){

		if (cart) {
			$scope.total_product = [];
			$scope.total_all = 0;

			$scope._cart.cart(cart);
			
			angular.forEach(cart, function(data, index){
				$scope.DATA.cart[index] = data; 
				$scope.getProductDetail(data.schedule_code, data.rates_code, index);
				$scope.count_data_cart($scope.DATA.cart[index], index);
			});

			$scope.get_total(cart);

		}
	}

	$scope.setCookies = function(key){
		var expireDate = new Date();
		expireDate.setDate(expireDate.getDate() + 7);
		
		$cookies.put($scope.KEY, key, {'expires': expireDate});
	}
	
	$scope.getCookies = function(){
		var key = $cookies.get($scope.KEY);
		
		if (key) {
			return key;
		}
	}

	$scope.check_submit = function(){
		if (!webStorage.get($scope.KEY)) {
			toastr.warning("Select your trip first");
			$location.url('/rsv');
		}
	}

	$scope.submit_booking = function(event){
		//';window.location = "#/trans_reservation/";
		var key = $scope.getCookies();
		
		httpSrv.post({
			url 	: "session/getSessionBooking",
			data	: $.param({"key":key}),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					var value = response.data.cart.session_value;
					$scope.submit_booking_rsv(event,value);

				}else{
					
				}
			
			}
		});

	}


	$scope.submit_booking_rsv = function(event, data){

	
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.data_rsv.error_desc = [];

		var data_rsv = {};
		
		var customer = angular.copy($scope.DATA.data_rsv.customer);
		
		$scope.DATA.data_rsv = angular.copy( $scope._cart.getRsv() );
		$scope.DATA.data_rsv.selected_trips = angular.copy( data );
		$scope.DATA.data_rsv.customer = angular.copy(customer);
	
		var ss_data_rsv = $scope.DATA.data_rsv;
		
		data_rsv.booking_source = "AGENT"
		data_rsv.agent_code = AGENT_CODE;

		data_rsv.booking_status = "DEFINITE";
		data_rsv.discount 		= {"type":ss_data_rsv.discount_type, "amount":ss_data_rsv.discount_amount};
		data_rsv.remarks 		= ss_data_rsv.remarks;
		data_rsv.customer 		= ss_data_rsv.customer;
		data_rsv.trips 			= [];
		
		for (i in ss_data_rsv.selected_trips){
			var trip = {};
			
			trip.date 	= ss_data_rsv.selected_trips[i].date;
			trip.adult 	= ss_data_rsv.selected_trips[i].adult;
			trip.child 	= ss_data_rsv.selected_trips[i].child;
			trip.infant	= ss_data_rsv.selected_trips[i].infant;
			
			trip.schedule_code	= ss_data_rsv.selected_trips[i].schedule_code;
			trip.rates_code		= ss_data_rsv.selected_trips[i].rates_code;
			trip.applicable_rates = ss_data_rsv.selected_trips[i].applicable_rates;

			if (ss_data_rsv.selected_trips[i].smart_pricing){

				if (ss_data_rsv.selected_trips[i].smart_pricing && ss_data_rsv.selected_trips[i].smart_pricing.type == "SMARTPRICING"){
					trip.smart_pricing_level = ss_data_rsv.selected_trips[i].smart_pricing.level;
				}
				
			}
			
			if (ss_data_rsv.selected_trips[i].is_return && ss_data_rsv.selected_trips[i].is_return == "yes"){
				trip.is_return	= "yes";
			}
			
			//Check Pickup Service
			if (ss_data_rsv.selected_trips[i].pickup 
				&& ss_data_rsv.selected_trips[i].pickup.area 
				&& !ss_data_rsv.selected_trips[i].pickup.area.nopickup){
				
				trip.pickup = ss_data_rsv.selected_trips[i].pickup;
				trip.pickup.area_id = trip.pickup.area.id;
				trip.pickup.price 	= trip.pickup.area.price;
				delete trip.pickup.area;
				
			}
			//Check Dropoff Service
			if (ss_data_rsv.selected_trips[i].dropoff 
				&& ss_data_rsv.selected_trips[i].dropoff.area 
				&& !ss_data_rsv.selected_trips[i].dropoff.area.nopickup){
				
				trip.dropoff = ss_data_rsv.selected_trips[i].dropoff;
				trip.dropoff.area_id = trip.dropoff.area.id;
				trip.dropoff.price 	 = trip.dropoff.area.price;
				delete trip.dropoff.area;
				
			}
			
			//Check Additional Service
			if (ss_data_rsv.selected_trips[i].additional_service){
				
				trip.additional_service = ss_data_rsv.selected_trips[i].additional_service;
				
			}
			
			data_rsv.trips.push(trip);
			
		}//End For
		
		if ($scope.DATA.data_rsv.add_payment == '1' && $scope.DATA.data_rsv.booking_status != 'COMPLIMEN'){
			var payment = $scope.DATA.data_rsv.payment;
			data_rsv.payment = {"amount" 		: payment.payment_amount,
								"payment_type"	: payment.payment_type,
								"account_number": payment.account_number,
								"name_on_card"	: payment.name_on_card,
								"bank_name"		: payment.bank_name,
								"payment_reff_number" : payment.payment_reff_number,
								"description"	: payment.description};
			if (payment.payment_type == "OPENVOUCHER"){
				data_rsv.payment.openvoucher_code = payment.openvoucher_code
			}
		}
	
		httpSrv.post({
			url 	: "agent_controller/create_reservation",
			data	: $.param(data_rsv),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					// window.location = "#/trans_reservation/detail/"+response.data.booking_code+"/passenger";
					$scope.clearCart();
					$location.url('/detail_passenger/'+response.data.booking_code);
					toastr.success("Redirect to Booking Detail...");
					toastr.success("Booking Success...");
					
				}else{
					$(event.target).find("button:submit").attr("disabled","disabled");
					$scope.DATA.data_rsv.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
					$('html, body').animate({scrollTop:200}, 400);
				}
				//$.hideLoading();
			}
		});
		
		
	}

	$scope.clearCart = function(){
		var key = $scope.getCookies();

		httpSrv.post({
			url 	: "session/delete",
			data	: $.param({"key":key}),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					toastr.success('delete success');

				}else{
					
				}
			
			}
		});
	}

	$scope.changeClick = function(click)
	{
		if (click == true) {
			$("input#return").removeAttr("disabled");
			$scope.add_trip.trip_model = "RETURN";
		}else{
			$("input#return").attr("disabled", true);
			$scope.add_trip.trip_model = "ONEWAY";
		}

		

	}

	$scope.deleteCart = function(index){
		
		var key = $scope.getCookies();
		
		httpSrv.post({
			url 	: "session/getSessionBooking",
			data	: $.param({"key":key}),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					var value = response.data.cart.session_value;
					$scope.delCart(index, value, key);
					
				}else{
					
				}

			}
		});
			   
	}

	$scope.delCart = function(index, cart, key){

		var detail = $scope.product_detail;
		var rate = $scope.product_rate;

		if (confirm("Are you sure to delete this trip?")){
			
			data = angular.copy(cart);

			if ((index+1) != cart.length) {
				if (cart[index+1].type == 'RETURN') {
					
					data.splice(index, 2);
					detail.splice(index, 2);
					rate.splice(index, 2);
					
				}else{

					data.splice(index, 1);
					detail.splice(index, 1);
					rate.splice(index, 1);

				}			
			}else{
				data.splice(index, 1);
				detail.splice(index, 1);
				rate.splice(index, 1);
			}

			
			
			$scope.product_detail = detail;
		    $scope.product_rate = rate;

		    $scope.get_total(data);

		  	httpSrv.post({
				url 	: "session/createSessionBooking",
				data	: $.param({"session_value":data, "key":key}),
				success : function(response){
					if (response.data.status == "SUCCESS"){
						
						$scope.key = response.data.key;
						$scope.setCookies($scope.key);

					}else{
						
					}

				}
			});

		}
		
	    
	}

	$scope.check_availabilities = function(event){
		
		var data = angular.copy($scope.add_trip);

		if (data.trip_model == "ONEWAY"){
			delete data.return_date;
		}
		
		data.booking_source = $scope.DATA.data_rsv.booking_source;
		if ($scope.DATA.data_rsv.booking_source == "AGENT" && $scope.DATA.data_rsv.agent){
			data.agent_code = $scope.DATA.data_rsv.agent.agent_code;
		}
		
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.add_trip.error_desc = [];
		$scope.check_availabilities_data = {};
		$scope.check_availabilities_show_loading = true;
		
		httpSrv.post({
			url 	: "agent_controller/check_availabilities",
			data	: $.param(data),
			success : function(response){
				$(event.target).find("button:submit").removeAttr("disabled");
				$scope.check_availabilities_show_loading = false;
				$scope.check_availabilities_show_searching_form = false;
				
				if (response.data.status == "SUCCESS"){
					$scope.check_availabilities_data = response.data;
					
				}else{
					$scope.add_trip.error_desc = response.data.error_desc;
					//$('html, body').animate({scrollTop:200}, 400);
				}

			}
		});
		
	}

	

	$scope.count_data_cart = function(data, index){

		$scope.total_product[index] = (data.applicable_rates.rates_1 * data.adult);
		
		if (data.child) {
			$scope.total_product[index] +=(data.applicable_rates.rates_2 * data.child); 
		}else if(data.infant) {
			$scope.total_product[index] +=(data.applicable_rates.rates_3 * data.infant);
		}

		if (!$scope.currency_total) {
			$scope.currency_total = data.currency;
		}

	}

	$scope.getProductDetail = function(schedule_code, rate_code, index)
	{
		

		httpSrv.post({
			url		: 	"transport/schedule/detail",
			data 	: 	$.param({"schedule_code": schedule_code}),
			success : function(response){
				
				$scope.product_detail[index] =  response.data;
				
			},
			error 	: function(err){}
		});

		httpSrv.post({
			url		: 	"transport/schedule/getRateDetail",
			data 	: 	$.param({"rates_code": rate_code}),
			success : function(response){
				
				$scope.product_rate[index] =  response.data;
				
			},
			error 	: function(err){}
		});

		
	}

	$scope.loadCart = function(){



		if (!$scope.DATA.country_list){
			httpSrv.get({
				url 	: "general/country_list",
				success : function(response){
					$scope.DATA.country_list = response.data;

				},
				error : function (response){}
			});
		}

		httpSrv.get({
			url 	: "setting/terms_and_conditions",
			success : function(response){
				$scope.terms_and_conditions = response.data.terms_and_conditions;
				
			},
			error : function (response){}
		});
		
		$(".step").removeClass('current');
		$(".step.2").addClass('current');

		$scope.getCart();
		
	}

	$scope.add_to_chart = function(check = 0)
	{
		
		$scope.data = [];
		$scope.add = {};

		$scope.DATA.data_rsv.error_desc = [];
		
		var data_rsv = {};
		var ss_data_rsv = angular.copy($scope.DATA.data_rsv);
		
		data_rsv.booking_source = ss_data_rsv.booking_source;
		

		if (data_rsv.booking_source == "AGENT"){
			data_rsv.agent_code = AGENT_CODE;
			
			if (ss_data_rsv.voucher_reff_number && ss_data_rsv.voucher_reff_number != ''){
				data_rsv.voucher_reff_number = ss_data_rsv.voucher_reff_number;
				
			}
		}
		
		data_rsv.booking_status = ss_data_rsv.booking_status;
		

		if (ss_data_rsv.booking_status == 'UNDEFINITE'){
			data_rsv.undefinite_valid_until_hour = ss_data_rsv.undefinite_valid_until_hour;
			
		}
		
		data_rsv.discount 		= {"type":ss_data_rsv.discount_type, "amount":ss_data_rsv.discount_amount};
		data_rsv.tax_service	= {"type":ss_data_rsv.tax_service_type, "amount":ss_data_rsv.tax_service_amount};
		data_rsv.remarks 		= ss_data_rsv.remarks;
		
		data_rsv.products 		= [];

		var index = 0;
		for (i in ss_data_rsv.selected_trips){
			var trip = {};
			
			trip.date 	= ss_data_rsv.selected_trips[i].date;
			trip.adult 	= ss_data_rsv.selected_trips[i].adult;
			trip.child 	= ss_data_rsv.selected_trips[i].child;
			trip.infant	= ss_data_rsv.selected_trips[i].infant;
			
			trip.schedule_code	= ss_data_rsv.selected_trips[i].trips.schedule.schedule_code;
			trip.rates_code		= ss_data_rsv.selected_trips[i].trips.rates_code;
			trip.applicable_rates = ss_data_rsv.selected_trips[i].trips.aplicable_rates;

			if (ss_data_rsv.selected_trips[i].trips.smart_pricing){

				
				if (ss_data_rsv.selected_trips[i].trips.smart_pricing && ss_data_rsv.selected_trips[i].trips.smart_pricing.type == "SMARTPRICING"){
					trip.smart_pricing_level = ss_data_rsv.selected_trips[i].trips.smart_pricing.level;
				}
			}
			
			if (ss_data_rsv.selected_trips[i].trips.is_return && ss_data_rsv.selected_trips[i].trips.is_return == "yes"){
				trip.is_return	= "yes";
			}
			
			//Check Pickup Service
			if (ss_data_rsv.selected_trips[i].pickup 
				&& ss_data_rsv.selected_trips[i].pickup.area 
				&& !ss_data_rsv.selected_trips[i].pickup.area.nopickup){
				
				trip.pickup = ss_data_rsv.selected_trips[i].pickup;
				trip.pickup.area_id = trip.pickup.area.id;
				trip.pickup.price 	= trip.pickup.area.price;
				delete trip.pickup.area;
				
			}
			//Check Dropoff Service
			if (ss_data_rsv.selected_trips[i].dropoff 
				&& ss_data_rsv.selected_trips[i].dropoff.area 
				&& !ss_data_rsv.selected_trips[i].dropoff.area.nopickup){
				
				trip.dropoff = ss_data_rsv.selected_trips[i].dropoff;
				trip.dropoff.area_id = trip.dropoff.area.id;
				trip.dropoff.price 	 = trip.dropoff.area.price;
				delete trip.dropoff.area;
				
			}
			
			//Check Additional Service
			if (ss_data_rsv.selected_trips[i].additional_service){
				
				trip.additional_service = ss_data_rsv.selected_trips[i].additional_service;
				
			}

			 data_rsv.products[index] = trip;

			 index++;
 	  
		}//End For

		$scope.button_check = false;
		
		if ($scope.add_trip.trip_model == "RETURN") {
			
			$scope.button_check = true;
			if (data_rsv.products.length>1) {
				toastr.success("Product Added to Cart...");
				

				// angular.forEach(data_rsv.products, function(data){
					
					data_rsv.type = $scope.add_trip.trip_model;
					 $scope.putCart(data_rsv.products);
				// });
				
			}
		}else{
			toastr.success("Product Added to Cart...");
			data_rsv.products.type = $scope.add_trip.trip_model;
			 $scope.putCart(data_rsv.products[0]);	
		}
		//$scope.loadCart();
		data_rsv.vendor_code = $stateParams.id_vendor;
		
		$scope.show_payment = false;
	}

	$scope.count_cart = function(data)
	{
		if (!$scope.count) {
			$scope.count = 0;
			$scope.count = data;
			$scope.count = $scope.count.length;
		}else{
			$scope.count = data;
			$scope.count = $scope.count.length;
		}
		
	}

	$scope.check_availabilities_select_this_trip = function (trip, trips_parent){

		//fn.pre(trip);
		for (index in trips_parent.availabilities){
			if (trips_parent.selected_trip){
				delete trips_parent.availabilities[index].is_hidden;
			}else{
				trips_parent.availabilities[index].is_hidden = true;
			}
			delete trips_parent.availabilities[index].is_selected;
		}
		if (trips_parent.selected_trip){
			delete trips_parent.selected_trip;
		}else{
			trip.is_selected = true;
			trip.is_hidden = false;
			
			trips_parent.selected_trip = trip;
		}
		
		$scope.check_availabilities_show_OK_button = false;
		if ($scope.add_trip.trip_model == "RETURN"){
			if ($scope.check_availabilities_data.departure.selected_trip && $scope.check_availabilities_data.return.selected_trip){
				$scope.check_availabilities_show_OK_button = true;
			}
		}else{
			if ($scope.check_availabilities_data.departure.selected_trip){
				$scope.check_availabilities_show_OK_button = true;
			}
		}
		
		$scope.check_availabilities_add_to_data_rsv();
		$scope.add_to_chart();
		
		$scope.RSV = angular.copy($scope.DATA.data_rsv);

		$scope._cart.saveRsv($scope.RSV);
	}

	$scope._check_availabilities_add_to_data_rsv = function(trip){

		var selected_trips = {};
		
		selected_trips.date		= trip.check.date;
		
		selected_trips.adult 	= trip.check.adult;
		selected_trips.child 	= trip.check.child;
		selected_trips.infant 	= trip.check.infant;
		
		selected_trips.trips = trip.selected_trip;
		selected_trips.trips.aplicable_rates_original = angular.copy(trip.selected_trip.aplicable_rates);
		
		$scope.DATA.data_rsv.selected_trips.push(selected_trips);
		
		
	}
	$scope.check_availabilities_add_to_data_rsv = function(){
		$scope.DATA.data_rsv.selected_trips = [];
		if ($scope.check_availabilities_data.departure && $scope.check_availabilities_data.departure.selected_trip){
			if ($scope.check_availabilities_data.departure.selected_trip.smart_pricing && $scope.check_availabilities_data.departure.selected_trip.smart_pricing.applicable_rates){
				
				var smart_pricing_applicable_rates = angular.copy($scope.check_availabilities_data.departure.selected_trip.smart_pricing.applicable_rates);
				
				for (_i in smart_pricing_applicable_rates){
					if (smart_pricing_applicable_rates[_i].allotment > 0){
						var departure_trip = angular.copy($scope.check_availabilities_data.departure);
						departure_trip.selected_trip.aplicable_rates = angular.copy(smart_pricing_applicable_rates[_i].rates);
						delete departure_trip.selected_trip.smart_pricing;
						departure_trip.selected_trip.smart_pricing = smart_pricing_applicable_rates[_i];
						
						departure_trip.check.adult 	= smart_pricing_applicable_rates[_i].allotment;
						departure_trip.check.child 	= 0;
						departure_trip.check.infant = 0;
						
						$scope._check_availabilities_add_to_data_rsv(departure_trip);
					}
				}
				
			}else{

				$scope._check_availabilities_add_to_data_rsv($scope.check_availabilities_data.departure);
			}
		}
		if ($scope.check_availabilities_data.return && $scope.check_availabilities_data.return.selected_trip){
			if ($scope.check_availabilities_data.return.selected_trip.smart_pricing && $scope.check_availabilities_data.return.selected_trip.smart_pricing.applicable_rates){
				
				var smart_pricing_applicable_rates = angular.copy($scope.check_availabilities_data.return.selected_trip.smart_pricing.applicable_rates);
				
				for (_x in smart_pricing_applicable_rates){
					
					if (smart_pricing_applicable_rates[_x].allotment > 0){
						var return_trip = angular.copy($scope.check_availabilities_data.return);
						return_trip.selected_trip.aplicable_rates = angular.copy(smart_pricing_applicable_rates[_x].rates);
						delete return_trip.selected_trip.smart_pricing;
						return_trip.selected_trip.smart_pricing = smart_pricing_applicable_rates[_x];
						
						return_trip.check.adult 	= smart_pricing_applicable_rates[_x].allotment;
						return_trip.check.child 	= 0;
						return_trip.check.infant 	= 0;
						
						$scope._check_availabilities_add_to_data_rsv(return_trip);
					}
				}
				
			}else{

				$scope._check_availabilities_add_to_data_rsv($scope.check_availabilities_data.return);
			}
			//$scope._check_availabilities_add_to_data_rsv($scope.check_availabilities_data.return);
		}

		$scope.aplicable_rates_for_agent();
		
		$scope.cmb_booking_status_change();
	
	}

	$scope.cmb_booking_status_change = function(){
		
		if ($scope.DATA.data_rsv.selected_trips){
			if ($scope.DATA.data_rsv.booking_status == 'COMPLIMEN'){
				for (x in $scope.DATA.data_rsv.selected_trips){
					if ($scope.DATA.data_rsv.selected_trips[x].trips.aplicable_rates){
						
						$scope.DATA.data_rsv.selected_trips[x].trips.rates_original = angular.copy($scope.DATA.data_rsv.selected_trips[x].trips.aplicable_rates);
						
						if ($scope.DATA.data_rsv.selected_trips[x].trips.aplicable_rates.rates_1){
							$scope.DATA.data_rsv.selected_trips[x].trips.aplicable_rates.rates_1 = 0;
						}
						if ($scope.DATA.data_rsv.selected_trips[x].trips.aplicable_rates.rates_2){
							$scope.DATA.data_rsv.selected_trips[x].trips.aplicable_rates.rates_2 = 0;
						}
						if ($scope.DATA.data_rsv.selected_trips[x].trips.aplicable_rates.rates_3){
							$scope.DATA.data_rsv.selected_trips[x].trips.aplicable_rates.rates_3 = 0;
						}
						if ($scope.DATA.data_rsv.selected_trips[x].trips.aplicable_rates.rates_4){
							$scope.DATA.data_rsv.selected_trips[x].trips.aplicable_rates.rates_4 = 0;
						}
						if ($scope.DATA.data_rsv.selected_trips[x].trips.aplicable_rates.rates_5){
							$scope.DATA.data_rsv.selected_trips[x].trips.aplicable_rates.rates_5 = 0;
						}
						$scope.calculate_subtotal($scope.DATA.data_rsv.selected_trips[x]);
					}
					//console.log($scope.DATA.data_rsv.selected_trips[x].trips.aplicable_rates.rates_1);
				}
				//console.log($scope.DATA.data_rsv.selected_trips);
			}else{
				for (x in $scope.DATA.data_rsv.selected_trips){
					if ($scope.DATA.data_rsv.selected_trips[x].trips.rates_original){
						$scope.DATA.data_rsv.selected_trips[x].trips.aplicable_rates = angular.copy($scope.DATA.data_rsv.selected_trips[x].trips.rates_original);
						$scope.calculate_subtotal($scope.DATA.data_rsv.selected_trips[x]);
					}
				}
				//console.log($scope.DATA.data_rsv.selected_trips);
			}
		}
	}

	$scope.calculate_subtotal = function(trips){
		trips.sub_total =  	(trips.adult*trips.trips.aplicable_rates.rates_1) +
							(trips.child*trips.trips.aplicable_rates.rates_2) +
							(trips.infant*trips.trips.aplicable_rates.rates_3);
		
		$scope.calculate_total();
	}

	$scope.aplicable_rates_for_agent = function(){

		$scope.agent_allow_to_use_acl = false;
		$scope.agent_allow_to_use_deposit = false;
		$scope.DATA.data_rsv.add_payment = '0';
		$scope.agent_payment_use_acl = '0';
		$scope.DATA.data_rsv.payment.payment_type = '';


		var cart = $scope.DATA.data_rsv.selected_trips;

	   	webStorage.set("CART",cart);


	   var selected_trips = cart;

		if ($scope.DATA.data_rsv.booking_source == "AGENT" && $scope.DATA.data_rsv.agent){
			
			var agent = $scope.DATA.data_rsv.agent;
			
			//Check Agent Credit Limit
			if (agent.payment_method.payment_code == "ACL"){
				$scope.agent_allow_to_use_acl = true;
				
				$scope.DATA.data_rsv.add_payment = '0';
				$scope.agent_payment_use_acl = '1';
				
				if (!agent.out_standing_invoice){
					agent.out_standing_invoice = {};
					httpSrv.post({
						url 	: "agent/out_standing_invoice/",
						data	: $.param({"agent_code":agent.agent_code}),
						success : function(response){
							if (response.data.status == "SUCCESS"){
								agent.out_standing_invoice = response.data.out_standing_invoice;
							}
						},
						error : function (response){}
					});
				}
			}
			//--
			$scope.deposit = [];

			if (agent.payment_method.payment_code == "DEPOSIT"){
				$scope.agent_allow_to_use_deposit = true;
				$scope.agent_allow_to_use_acl = false;

				$scope.DATA.data_rsv.add_payment = '0';
				httpSrv.post({
					url 	: "agent/deposit/",
					data	: $.param({"publish_status":"ALL", "agent_code":agent.agent_code}),
					success : function(response){
						
						$scope.deposit = response.data.deposit;
					},
					error : function (response){}
				});
			}
			
			for (i in selected_trips){
				var _rates = selected_trips[i].trips.aplicable_rates_original;
			
				
				$scope.calculate_subtotal(selected_trips[i]);
			}
			
		}else{
			
			for (i in selected_trips){
				var _rates = selected_trips[i].trips.aplicable_rates_original;
				selected_trips[i].trips.aplicable_rates = angular.copy(_rates);
				
				$scope.calculate_subtotal(selected_trips[i]);
			}
			
		}
		$scope.calculate_total();

	}

	$scope.calculate_total = function(){
		var total = 0;
		for (x in $scope.DATA.data_rsv.selected_trips){
			
			var total_person = $scope.DATA.data_rsv.selected_trips[x].adult + $scope.DATA.data_rsv.selected_trips[x].child;
			
			total += $scope.DATA.data_rsv.selected_trips[x].sub_total;
			
			//Pickup
			if ($scope.DATA.data_rsv.selected_trips[x].pickup && $scope.DATA.data_rsv.selected_trips[x].pickup.area.price){
				if ($scope.DATA.data_rsv.selected_trips[x].pickup.area.type == 'way'){
					pickup_sub_total = $scope.DATA.data_rsv.selected_trips[x].pickup.area.price;
				}else{
					pickup_sub_total = $scope.DATA.data_rsv.selected_trips[x].pickup.area.price * total_person;
				}
				total += pickup_sub_total;
			}
			//Dropoff
			if ($scope.DATA.data_rsv.selected_trips[x].dropoff && $scope.DATA.data_rsv.selected_trips[x].dropoff.area.price){
				if ($scope.DATA.data_rsv.selected_trips[x].pickup.area.type == 'way'){
					dropoff_sub_total = $scope.DATA.data_rsv.selected_trips[x].dropoff.area.price;
				}else{
					dropoff_sub_total = $scope.DATA.data_rsv.selected_trips[x].dropoff.area.price * total_person;
				}
				total += dropoff_sub_total;
			}
			//Additional Service
			if ($scope.DATA.data_rsv.selected_trips[x].additional_service){
				for (y in $scope.DATA.data_rsv.selected_trips[x].additional_service){
					total += ($scope.DATA.data_rsv.selected_trips[x].additional_service[y].qty * $scope.DATA.data_rsv.selected_trips[x].additional_service[y].price);
				}
			}
		}
		//$scope.DATA.data_rsv.TOTAL = {"total":0};
		$scope.DATA.data_rsv.TOTAL.total = total;
		
		$scope.calculate_discount();
		
		$scope.DATA.data_rsv.TOTAL.grand_total = total - $scope.DATA.data_rsv.TOTAL.discount;
		
		if (!$scope.DATA.data_rsv.payment){
			$scope.DATA.data_rsv.payment = {};
		}
		
		$scope.DATA.data_rsv.payment.payment_amount = $scope.DATA.data_rsv.TOTAL.grand_total;
		$scope.DATA.data_rsv.payment.balance = $scope.DATA.data_rsv.TOTAL.grand_total - $scope.DATA.data_rsv.payment.payment_amount;
	}

	$scope.initData = function(){

		$scope.DATA.data_rsv = {"booking_source" : "AGENT",
								"agent_code" 	 : AGENT_CODE,
								"booking_status" : "DEFINITE",
								"discount_type"	 : "%",
								"discount_amount": 0,
								"tax_service_type"	 : "%",
								"tax_service_amount" : 0,
								"commission_type"	 : "%",				//COM
								"commission_amount"  : 0,				//COM
								"undefinite_valid_until_hour" : 24,
								"undefinite_valid_until_type" : "DATE",
								"selected_trips" : [],
								"TOTAL"			 : {"total":0,"discount":0}};

		$scope.calculate_total();

		//Load Data Available Port
		httpSrv.post({
			url 	: "transport/port/available_port",
			success : function(response){ $scope.DATA.available_port = response.data; },
		});

		$scope.add_trip = {};
		$scope.check_availabilities_data = {};
		
		if ($scope.check_availabilities_data.departure) $scope.check_availabilities_data.departure.selected_trip = {};
		if ($scope.check_availabilities_data.return) $scope.check_availabilities_data.return.selected_trip = {};
		$scope.check_availabilities_show_OK_button = false;
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd", minDate: fn.formatDate(new Date(),"yy-mm-dd")});

		$scope.add_trip = {"trip_model":"ONEWAY","adult":1,"child":0,"infant":0};
	}

	

	$scope.loadProcess = function(){
		$('#smartwizard').smartWizard({
			  selected: 0, 
                    theme: 'arrows',
                    transitionEffect:'slide',
		});
	}

	$scope.calculate_discount = function(){
		var discount = 0;
		if ($scope.DATA.data_rsv.discount_type=='%'){
			if ($scope.DATA.data_rsv.discount_amount > 100) $scope.DATA.data_rsv.discount_amount = 100;
			discount = $scope.DATA.data_rsv.TOTAL.total * $scope.DATA.data_rsv.discount_amount/100;
		}else{
			discount = angular.copy($scope.DATA.data_rsv.discount_amount);
		}
		$scope.DATA.data_rsv.TOTAL.discount = discount;
	}
	
});
function activate_sub_menu_agent_detail(class_menu){
	$(".agent-detail ul.nav.sub-nav li").removeClass("active");
	$(".agent-detail ul.nav.sub-nav li."+class_menu).addClass("active");
}

// JavaScript Document 
angular.module('app').controller('agent_controller',function($scope, $http, httpSrv, $stateParams, $interval, fn,$filter, $window){
	
	$scope.DATA = {};
	$scope.fn = fn;

	$scope.min = [];
	$scope.min[0] = 0;

	$scope.num = function(data, index){
		
		if (!data) {
			$scope.tumb = 0;
		}else{
			$scope.tumb = parseInt(data) + 1;
		}
		
		$scope.min[index+1] = $scope.tumb;
		
	}

	$scope.loadDataTermsAndConditions = function(){

		$scope.is_cancellation = false;
		$scope.item = 0;

		httpSrv.get({
			url 	: "setting/terms_and_conditions",
			success : function(response){
				$scope.terms_and_conditions = response.data.terms_and_conditions;
				$scope.advanced_setting = response.data.advanced_setting;
				$scope.rules = JSON.parse(response.data.advanced_setting.f_cancelation_rule_crs);
				$scope.convert_int($scope.rules);
				
				if ($scope.advanced_setting.f_is_cancellation_crs && $scope.advanced_setting.f_is_cancellation_crs == 1) {
					$scope.is_cancellation = true;
				}
				if ($scope.is_cancellation == true) {
					$scope.check = 1;
				}else{
					$scope.check = 0;
				}

				$scope.DATA = response.data;
			},
			error : function (response){}
		});

		activate_sub_menu_agent_detail("terms_and_condition"); 
	}

	$scope.convert_int = function(data){
		
		var array = [];

		angular.forEach(data, function(row){
			array.push({f_min_activation_days: parseInt(row.f_min_activation_days), f_percentage: parseInt(row.f_percentage)});
		});

		$scope.rules = array;
	}

	$scope.paymentDetail = function (current_payment, delete_payment){
		$scope.myPaymentDetail = angular.copy(current_payment);
		//$scope.myPaymentDetail.amount = Math.abs($scope.myPaymentDetail.amount);
		if (delete_payment == true){
			$scope.myPaymentDetail.delete_payment = true;
			$scope.myPaymentDetail.sure_to_delete = '0';
		}else{
			delete $scope.myPaymentDetail.delete_payment;
		}
	}

	$scope.loadDataDeposit = function(){
		
		GeneralJS.activateLeftMenu("deposit");
		var agent_code = $stateParams.agent_code; 
		$scope.show_loading = true;
		httpSrv.post({
			url 	: "agent_controller/deposit",
			data	: $.param({"publish_status":"ALL"}),
			success : function(response){
				$scope.DATA = response.data;
				$scope.show_loading = false;
				$scope.loadDataTransactionTransportDeposit(agent_code);
				//$scope.total_deposit(response.data.deposit);
			},
			error : function (response){}
		});

		
	}

	$scope.loadDataTransactionTransportDeposit = function(agent_code){
			
		$scope.search = {   "start_date" : "",
							"end_date" : "",
							 "agent_code": agent_code,
							 "payment_method": "DEPOSIT"}; 		
		
		var _search = $scope.search;
	
		httpSrv.post({
			url 	: "agent_controller/agent_transaction",
			data	: $.param({"search":_search}),
			success : function(response){
				
				$scope.DATA.transaction = response.data.transactions;

				$scope.used_diposit($scope.DATA.transaction);
			
			},
			error : function (response){}
		});

	}

	$scope.used_diposit = function(data){
		var total = 0;
		$scope.usd_diposit = 0;
		angular.forEach(data, function(row){
			total += parseInt(row.amount);
		});

		$scope.usd_diposit = total;

		
	}
	


	$scope.total_deposit = function(data){
		
		GeneralJS.activateLeftMenu("deposit");	
		
		var total = 0;
		var currency = '';
		$scope.total_dep = 0; 

		angular.forEach(data, function(row){
			total += parseInt(row.amount);
			currency = row.currency;
		});

		$scope.total_dep = total; 
		$scope.currency = currency; 
	}

	$scope.total_transaction = function(data){
		$scope.total_paid = 0;
		$scope.total_refund = 0;
		$scope.currency = '';
		var crr = '';
		var paid = 0;
		var refund = 0;
		angular.forEach(data.transactions, function(row){
			if (row.trx_type == 'RESERVATION') {
				paid+=row.amount;
			}else if(row.trx_type == 'REFUND'){
				refund+=row.amount;
			}else if(row.trx_type == 'INVOICE'){
				paid+=row.amount;
			}

			if ($scope.currency == '') {
				crr = row.currency;
			}
			
		});

		$scope.total_paid = paid;
		$scope.total_refund = refund;
		$scope.currency = crr;

	}
	
	$scope.loadDataTransactionTransport = function(page){

		$scope.show_loading_DATA_bookings = true;
		var agent_code = AGENT_CODE;
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		
		if (!$scope.search){ 
			var now = new Date(),
				start_date = new Date(),
				end_date = new Date();
			
			start_date.setDate(start_date.getDate() - 30);
			
			// $scope.search = { "start_date" : $scope.fn.formatDate(start_date, "yy-mm-dd"),
			// 				  "end_date" : $scope.fn.formatDate(end_date, "yy-mm-dd"),}; 
			$scope.search = { "start_date" : "",
							  "end_date" : ""};
		}
		
		if (!page) page = 1;
		
		var _search = $scope.search;
			_search.page = page;
			// _search.agent_code = agent_code;
		
		httpSrv.post({
			url 	: "agent_controller/agent_transaction",
			data	: $.param({"search":_search}),
			success : function(response){
				$scope.show_loading_DATA_bookings = false;
				$scope.DATA.transaction = response.data;
				$scope.total_transaction(response.data);
				if ($scope.DATA.transaction.search){
					$scope.DATA.transaction.search.pagination = [];
					for (i=1;i<=$scope.DATA.transaction.search.number_of_pages; i++){
						$scope.DATA.transaction.search.pagination.push(i);
					}
				}
			},
			error : function (response){}
		});
		
		//Load Data Booking Source
		if (!$scope.$root.DATA_booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.$root.DATA_booking_source = response.data.booking_source; },
			});
		}
		
		//Load Data Booking Status
		if (!$scope.$root.DATA_booking_status){
			httpSrv.post({
				url 	: "setting/booking_status",
				success : function(response){ $scope.$root.DATA_booking_status = response.data.booking_status; },
			});
		}
		
		//Load Data Payment Method
		if (!$scope.$root.DATA_payment_method){
			httpSrv.post({
				url 	: "setting/payment_method",
				success : function(response){ $scope.$root.DATA_payment_method = response.data.payment_method; },
			});
		}
		
		//Out Standing Invoice
		// httpSrv.post({
		// 	url 	: "agent_controller/out_standing_invoice/",
			
		// 	success : function(response){
		// 		if (response.data.status == "SUCCESS"){
		// 			$scope.DATA.out_standing_invoice = response.data.out_standing_invoice;
		// 		}
		// 	},
		// 	error : function (response){}
		// });

		GeneralJS.activateLeftMenu("transaction");	
	}

	$scope.loadDataAgentDetail = function(agent_code, after_load_handler_function){
		
		GeneralJS.activateLeftMenu("profile");
		$scope.phone_code();
		

		if (!agent_code){ agent_code = AGENT_CODE; }
		
		httpSrv.get({
			url 	: "agent_controller/detail",
			success : function(response){
				var agent = response.data;
				
				if (agent.status == "SUCCESS"){
					$scope.DATA.current_agent = agent;
					$scope.DATA.agent_code = agent_code;
					
					if (typeof after_load_handler_function == 'function') {
						after_load_handler_function(agent);
					}
				}else{
					alert("Sorry, agent not found..");
					window.location = "#/agent";
				}
			},
			error : function (response){}
		});

		//Out Standing Invoice
		httpSrv.post({
			url 	: "agent_controller/out_standing_invoice/",
			
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.DATA.out_standing_invoice = response.data.out_standing_invoice;
				}
			},
			error : function (response){}
		});

		//Load Data Country List
		httpSrv.post({
			url 	: "general/country_list",
			success : function(response){ $scope.DATA.country_list = response.data.country_list; },
		});

		httpSrv.post({
			url 	: "agent/deposit",
			data	: $.param({"publish_status":"ALL", "agent_code": agent_code}),
			success : function(response){
				$scope.DATA.deposit = response.data.deposit;
			},
			error : function (response){}
		});

		$scope.dataSearch = {};
		$scope.dataSearch.search = {};
		$scope.dataSearch.search = {
			"agent_code": agent_code,
		};

		// UnInvoicing
		httpSrv.post({
			url 	: "invoice/unInvoicing/",
			data	: $.param($scope.dataSearch),
			success : function(response){

				if (response.data.status == "SUCCESS"){
					$scope.DATA.uninvoicing = response.data.uninvoicing;
				}
			},
			error : function (response){}
		});

		
	}

	$scope.loadDataAgentContracRatesAssign = function(){
		
		GeneralJS.activateLeftMenu("contract_rates");
		
		httpSrv.post({
			url 	: "agent_controller/contract_rates_assign/",
			success : function(response){

				$scope.DATA.contract_rates_assign = response.data;
			},
			error : function (response){}
		});
	}

	$scope.assignAgentContractRates = function(contract_rates){
		if (contract_rates){
			
			$scope.DATA.myContractRates = angular.copy(contract_rates);
			$scope.DATA.myContractRates.is_edit = true;
			
			var agent_code = AGENT_CODE;
			var param = {"id" : $scope.DATA.myContractRates.id};
			delete $scope.DATA.agent_contract_rates;
			
			httpSrv.post({
				url 	: "agent_controller/assign_contract_rates_detail/",
				data	: $.param(param),
				success : function(response){
					if (response.data.contract_rates){
						$scope.DATA.myContractRates.contract_rates = response.data.contract_rates;
					}
				},
				error : function (response){}
			});
			
		}else{
			
			delete $scope.DATA.myContractRates;
			
			if (!$scope.DATA.agent_contract_rates){
				httpSrv.post({
					url 	: "agent/category",
					data	: $.param({"publish_status":"1"}),
					success : function(response){
						$scope.DATA.agent_contract_rates = response.data.categories;
					},
					error : function (response){}
				});
			}
		}
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
	}

	$scope.loadDataInvoice = function(page){

		GeneralJS.activateLeftMenu("invoice");
		$scope.show_loading_DATA_bookings = true;
		var agent_code = AGENT_CODE;
		if (!page) page = 1;
		var myDate = new Date();
		var nextDay = new Date(myDate);
		$scope.DATA.invoice = false;
		nextDay.setDate(myDate.getDate());
		httpSrv.post({
			url 	: "agent_controller/invoice",
			data	: $.param({"page":page}),
			success : function(response){
				$scope.DATA.invoice = response.data;
				if ($scope.DATA.invoice.search){
					$scope.DATA.invoice.search.pagination = [];
					for (i=1;i<=$scope.DATA.invoice.search.number_of_pages; i++){
						$scope.DATA.invoice.search.pagination.push(i);
					}
				}
				$scope.DATA.invoice.now = $filter('date')(nextDay, "yyyy-MM-dd");
			},
			error : function (response){}
		});

		// Out Standing Invoice
		httpSrv.post({
			url 	: "agent_controller/out_standing_invoice/",
			data	: $.param({"agent_code":agent_code}),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.DATA.out_standing_invoice = response.data.out_standing_invoice;
				}
			},
			error : function (response){}
		});

		$scope.dataSearch = {};
		$scope.dataSearch.search = {};
		$scope.dataSearch.search = {
			"agent_code": agent_code,
		};

		// UnInvoicing
		httpSrv.post({
			url 	: "agent_controller/unInvoicing/",
			data	: $.param($scope.dataSearch),
			success : function(response){

				if (response.data.status == "SUCCESS"){
					$scope.DATA.uninvoicing = response.data.uninvoicing;
				}
			},
			error : function (response){}
		});
	}

	$scope.loadDataBookingTransport = function(page){

		GeneralJS.activateLeftMenu("reservation");
		$scope.show_loading_DATA_bookings = true;
		var agent_code = AGENT_CODE;
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		
		if (!$scope.search){ 
			var now = new Date(),
				start_date = new Date(),
				end_date = new Date();
			
			start_date.setDate(start_date.getDate() - 30);
			
			// $scope.search = { "start_date" : $scope.fn.formatDate(start_date, "yy-mm-dd"),
			// 				  "end_date" : $scope.fn.formatDate(end_date, "yy-mm-dd"),}; 

			$scope.search = { "start_date" : "",
							  "end_date" : ""}; 
		}
		
		if (!page) page = 1;
		
		var _search = $scope.search;
			_search.page = page;
			// _search.agent_code = agent_code;
			
		httpSrv.post({
			url 	: "agent_controller/booking_and_voucher_list",
			data	: $.param({"search":_search}),
			success : function(response){
				$scope.show_loading_DATA_bookings = false;
				$scope.DATA.bookings = response.data;
				
				if ($scope.DATA.bookings.search){
					$scope.DATA.bookings.search.pagination = [];
					for (i=1;i<=$scope.DATA.bookings.search.number_of_pages; i++){
						$scope.DATA.bookings.search.pagination.push(i);
					}
				}
			},
			error : function (response){}
		});
		
		//Load Data Booking Source
		if (!$scope.$root.DATA_booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.$root.DATA_booking_source = response.data.booking_source; },
			});
		}
		
		//Load Data Booking Status
		if (!$scope.$root.DATA_booking_status){
			httpSrv.post({
				url 	: "setting/booking_status",
				success : function(response){ $scope.$root.DATA_booking_status = response.data.booking_status; },
			});
		}
	}

	// $scope.agentAddEdit = function (){
	// 	var agent_code = $stateParams.agent_code;
		
		
	// 	httpSrv.get({
	// 		url 	: "general/country_list",
	// 		success : function(response){
	// 			$scope.DATA.country_list = response.data;
	// 		},
	// 		error : function (response){}
	// 	});
	// 	httpSrv.get({
	// 		url 	: "agent_controller/category",
	// 		success : function(response){
	// 			$scope.DATA.categories = response.data;
	// 		},
	// 		error : function (response){}
	// 	});
		
	// 	if (agent_code){
	// 		$scope.loadDataAgentDetail(agent_code, function(agent_detail){
	// 			agent_detail.country_code = agent_detail.country.code;
	// 			$scope.DATA.current_agent = agent_detail;
				
	// 			if (agent_detail.category){
	// 				$scope.DATA.current_agent.category_code = agent_detail.category.category_code;
	// 			}else{
	// 				$scope.DATA.current_agent.category_code = "";
	// 			}
				
	// 			$scope.DATA.current_agent.payment_method_code = agent_detail.payment_method.payment_code;
				
	// 			$scope.DATA.current_agent.ready = true;
	// 		});
	// 	}else{
	// 		$scope.DATA.current_agent = {};
	// 		$scope.DATA.current_agent.country_code = "ID";
	// 		$scope.DATA.current_agent.category_code = "";
	// 		$scope.DATA.current_agent.ready = true;
	// 	}
	// }

	$scope.save_profile = function(event, type){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$(event.target).find('input').trigger('input');
		$scope.DATA.error_msg = [];
		
		//var data = angular.copy($scope.DATA.current_agent);

		var data = {
			address			:  $scope.DATA.current_agent.address,
			contact_person 	: $scope.DATA.current_agent.contact_person,
			country 		: $scope.DATA.current_agent.country,
			email 			:$scope.DATA.current_agent.email,
			id 				:$scope.DATA.current_agent.id,
			name 			:$scope.DATA.current_agent.name,
			phone 			:$scope.DATA.current_agent.phone,
			publish_status 	:$scope.DATA.current_agent.publish_status,
			remarks			:$scope.DATA.current_agent.remarks,
			status  		:$scope.DATA.current_agent.status,
			website 		:$scope.DATA.current_agent.website,
		};

		
		data.payment_method = data.payment_method_code;
		
		httpSrv.post({
			url 	: "agent_controller/update_profile/"+type,
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$(event.target).closest(".inline-edit").find(".edit-text").toggle();
					$(event.target).closest(".inline-edit").find(".main-text").toggle();
					toastr.success("Saved...");
					$scope.loadDataAgentDetail();
				}else{
					//console.log(response.data.error_desc);
					$scope.DATA.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			},
			error 	: function(err)
			{
				console.log(err);
			}
		});
	}

	$scope.phone_code = function(){
		
		httpSrv.get({
			url : "phonecode.json",
			success : function(response){
				
				$scope.code_phone = response.data;
			},
			error 	: function(err){

			}
		})
	}

	$scope.change_password = function(event){

		$scope.DATA.error_pwd = [];

		$(event.target).find("button:submit").attr("disabled","disabled");
		$(event.target).find('input').trigger('input');;
		//$scope.DATA.vendor.error_pwd = [];
		var agent_code = AGENT_CODE;
		httpSrv.post({
			url 	: "agent_controller/change_password/"+agent_code,
			data	: $.param($scope.DATA.dataPwd),
			success : function(response){
				
				if (response.data.status == "SUCCESS"){
					$scope.DATA.dataPwd = "";
					$(event.target).closest(".inline-edit").find(".edit-text").toggle();
					$(event.target).closest(".inline-edit").find(".main-text").toggle();
					toastr.success("Password Changed...");

				}else{
					toastr.warning("Password Changed Failed...");
					//console.log(response);
					$scope.DATA.error_pwd = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}

	$scope.loadDataInvoiceDetail = function(is_reload=false,after_load_handler_function){

		GeneralJS.activateLeftMenu("invoice");
		if(is_reload){$scope.DATA.current_invoice = false;}
		invoice_code = $stateParams.invoice_code; 
		var agent_code = AGENT_CODE;
		
		httpSrv.post({
			url 	: "agent_controller/detail_agent_invoice/" + invoice_code,
			success : function(response){
				var invoice = response.data;
				invoice.type = "RESERVATION";
				if(invoice.invoice){
					var regex = /<br\s*[\/]?>/gi;
					var remarks = invoice.invoice.remarks;
					invoice.invoice.remarks = remarks.replace(regex, "\n")
				}
				if (fn.isFn(after_load_handler_function)) {
					if (invoice.status == "SUCCESS"){
						$scope.DATA.current_invoice = invoice;
						after_load_handler_function(invoice);
					}
				}else{
					$scope.DATA.current_invoice = invoice;
					if (invoice.status != "SUCCESS"){
						alert("Sorry, invoice not found...");
						window.location = "#/invoices";
					}
				}
			},
			error : function (response){}
		});


	}

	$scope.loadDataInvoicePayment = function(){
		
		GeneralJS.activateLeftMenu("invoice");
		invoice_code = $stateParams.invoice_code;
		
		/*if ($scope.$parent.DATA.current_invoice){
			$scope.DATA.current_invoice = $scope.$parent.DATA.current_invoice;
		}else{
			$scope.loadDataInvoiceDetail(invoice_code, function(invoice_detail){
				$scope.DATA.current_invoice = invoice_detail;
			});
		}*/
		
		httpSrv.get({
			url 	: "agent_controller/reservation/" + invoice_code,
			success : function(response){
				$scope.DATA.payment = response.data;
				if($scope.DATA.payment.payment.invoice_type=="OPENVOUCHER"){
					$scope.loadDataInvoiceDetailOT();
				}else{
					$scope.loadDataInvoiceDetail();
				}
			},
			error : function (response){}
		});
	}

	$scope.loadDataInvoiceDetailOT = function(is_reload=false,after_load_handler_function){
		if(is_reload){$scope.DATA.current_invoice = false;}
		invoice_code = $stateParams.invoice_code; 
		
		httpSrv.post({
			url 	: "invoice/detail_open_voucher/" + invoice_code,
			success : function(response){
				var invoice = response.data;
				invoice.type = "OPENVOUCHER";
				if(invoice.invoice){
					var regex = /<br\s*[\/]?>/gi;
					var remarks = invoice.invoice.remarks;
					invoice.invoice.remarks = remarks.replace(regex, "\n")
				}
				if (fn.isFn(after_load_handler_function)) {
					if (invoice.status == "SUCCESS"){
						$scope.DATA.current_invoice = invoice;
						after_load_handler_function(invoice);
					}
				}else{
					$scope.DATA.current_invoice = invoice;
					if (invoice.status != "SUCCESS"){
						alert("Sorry, invoice not found..");
						window.location = "#/invoices";
					}
				}
			},
			error : function (response){}
		});
	}

	
});
function activate_sub_menu_agent_detail(class_menu){
	$(".agent-detail ul.nav.sub-nav li").removeClass("active");
	$(".agent-detail ul.nav.sub-nav li."+class_menu).addClass("active");
}

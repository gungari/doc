// JavaScript Document 
angular.module('app').controller('product_controller', function($scope, $http, httpSrv, $state, $stateParams, $interval, fn,$filter, $window, $location, webStorage, $cookies, cart){
	

	GeneralJS.activateLeftMenu("vendor");
	
	$scope.DATA = {};
	$scope.fn = fn;
	$scope.DATA.data_rsv = {};
	$scope._cart = cart;
	$scope.id_vendor = '';
	$scope.product_code = '';
	$scope.DATA.select_rate = '';
	$scope.check_availabilities_data = {};
	$scope.add_product = {};
	$scope.DATA.gallery = {};
	$scope.CART = [];
	$scope.KEY = 'CART';
	$scope.gallery = {};
	$scope.product_detail = [];
	$scope.count = 0;
	$scope.DATA.cart = [];

	$scope.loadTerm = function(){
		$scope.is_cancellation = false;
		$scope.item = 0;

		httpSrv.get({
			url 	: "setting/terms_and_conditions",
			success : function(response){
				$scope.terms_and_conditions = response.data.terms_and_conditions;
				
			},
			error : function (response){}
		});
	}

	$scope.count_data_cart = function(data, index){

		if(data.is_packages){
			$scope.total_product[index] = (data.packages.qty * data.packages.rates);
		
			if (data.packages.additional){
				if (data.packages.additional.opt_1) $scope.total_product[index] += (data.packages.additional.opt_1 * data.packages.extra_rates.opt_1);
				if (data.packages.additional.opt_2) $scope.total_product[index] += (data.packages.additional.opt_2 * data.packages.extra_rates.opt_2);
				if (data.packages.additional.opt_3) $scope.total_product[index] += (data.packages.additional.opt_3 * data.packages.extra_rates.opt_3);
			}
		}else{
			$scope.total_product[index] = (data.applicable_rates.rates_1 * data.qty_1);
			
			if (data.qty_2) {
				$scope.total_product[index] +=(data.applicable_rates.rates_2 * data.qty_2); 
			}else if(data.qty_3) {
				$scope.total_product[index] +=(data.applicable_rates.rates_3 * data.qty_3);
			}

		}
		
		if (!$scope.currency_total) {
			$scope.currency_total = data.currency;
		}

	}

	$scope.get_total = function(cart)
	{

		var total = 0;
		var call = 0;

		angular.forEach(cart, function(data){

			if(!data.is_packages){
				call = (data.applicable_rates.rates_1 * data.qty_1);
				
				if (data.qty_2) {
					call +=(data.applicable_rates.rates_2 * data.qty_2); 
				}else if(data.qty_3) {
					call +=(data.applicable_rates.rates_3 * data.qty_3);
				}

				total += call;	
			}else{
				call = (data.packages.qty * data.packages.rates);
		
				if (data.packages.additional){
					if (data.packages.additional.opt_1) call += (data.packages.additional.opt_1 * data.packages.extra_rates.opt_1);
					if (data.packages.additional.opt_2) call += (data.packages.additional.opt_2 * data.packages.extra_rates.opt_2);
					if (data.packages.additional.opt_3) call += (data.packages.additional.opt_3 * data.packages.extra_rates.opt_3);
				}
				total += call;	
			}
 			
		});
	
		$scope.CART.grand_total = total;

	}

	$scope.putCart = function(value){
		
		var key = $scope.getCookies();
	
		httpSrv.post({
			url 	: "session/getSessionBooking",
			data	: $.param({"key":key}),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					var cart = response.data.cart.session_value;
					$scope.saveCart(cart, value, 1);
					
				}else{

					cart = [];
					$scope.saveCart(cart, value, 0);
					
				}
				
			}
		});
	}

	$scope.saveCart = function(cart, value, issnul){
		
		if (cart) {
			$scope.CART = angular.copy(cart);
		}else{
			$scope.CART = [];
		}
			
		if (issnul==1) {
			var key = $scope.getCookies();
			if (value.length > 1) {
				
				angular.forEach(value, function(data, index){
					
					if (value.length == (index+1)) {
						data.type = 'RETURN';
					}
					$scope.CART.push(data);
					
				});

			}else{
				value.type = 'ONEWAY';
				$scope.CART.push(value);
			}
			
		
		}else{
			var key = '';
			
			if (value.length > 1) {
				
				angular.forEach(value, function(data, index){
					
					if (value.length == (index+1)) {
						data.type = 'RETURN';
					}
					$scope.CART.push(data);
					
				});

			}else{
				value.type = 'ONEWAY';
				$scope.CART.push(value);
			}
		}

		httpSrv.post({
			url 	: "session/createSessionBooking",
			data	: $.param({"session_value":$scope.CART, "key":key}),
			success : function(response){

				$scope.key = response.data.key;	

				if (response.data.status == "SUCCESS"){
					
					$scope.setCookies($scope.key);

					var result = { key:$scope.key };
					$state.go('act_cart', result);
		
				}

			}
		});
	}

	$scope.getCart = function(key = ''){
			
		if (key=='') {
			var key = $scope.getCookies();
		}
	
		
		httpSrv.post({
			url 	: "session/getSessionBooking",
			data	: $.param({"key":key}),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					var value = response.data.cart.session_value;
					console.log(value);
					$scope.generateCart(value);

				}else{
					
				}
				//$.hideLoading();
			}
		});

		
	}

	$scope.generateCart = function(cart){

		if (cart) {
			$scope.total_product = [];
			$scope.total_all = 0;

			$scope._cart.cart(cart);

			angular.forEach(cart, function(data, index){
				$scope.DATA.cart[index] = data; 
				$scope.getProductDetail(data.product_code, data.rates_code, index);
				$scope.count_data_cart($scope.DATA.cart[index], index);
			});

			$scope.get_total(cart);

		}
	}

	$scope.setCookies = function(key){
		var expireDate = new Date();
		expireDate.setDate(expireDate.getDate() + 7);
		
		$cookies.put($scope.KEY, key, {'expires': expireDate});
	}
	
	$scope.getCookies = function(){
		var key = $cookies.get($scope.KEY);
		
		if (key) {
			return key;
		}
	}

	$scope.deleteCart = function(index){
		
		var key = $scope.getCookies();
		
		httpSrv.post({
			url 	: "session/getSessionBooking",
			data	: $.param({"key":key}),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					var value = response.data.cart.session_value;
					$scope.delCart(index, value, key);
					
				}else{
					
				}

			}
		});
			   
	}

	$scope.delCart = function(index, cart, key){

		if (confirm("Are you sure to delete this trip?")){
			var data = cart;
		
			var detail = angular.copy($scope.product_detail);
			
			data.splice(index, 1);
			detail.splice(index, 1);
				
			$scope.product_detail = angular.copy(detail);

		    $scope.get_total(data);

		  	httpSrv.post({
				url 	: "session/createSessionBooking",
				data	: $.param({"session_value":data, "key":key}),
				success : function(response){
					if (response.data.status == "SUCCESS"){
						$scope.key = response.data.key;
						$scope.setCookies($scope.key);
						
					}else{
						
					}

				}
			});

			$scope.getCart();	
		}
	}
	
	$scope.count_total = function(data, index){

		$scope.total_product[index] = (data.applicable_rates.rates_1 * data.qty_1);
		
		if (data.qty_2) {
			$scope.total_product[index] +=(data.applicable_rates.rates_2 * data.qty_2); 
		}else if(data.qty_3) {
			$scope.total_product[index] +=(data.applicable_rates.rates_3 * data.qty_3);
		}

		if (!$scope.currency_total) {
			$scope.currency_total = data.currency;
		}

	}

	 
	$scope.loadCart = function(){

		if (!$scope.DATA.country_list){
			httpSrv.get({
				url 	: "general/country_list",
				success : function(response){
					$scope.DATA.country_list = response.data;

				},
				error : function (response){}
			});
		}

		httpSrv.get({
			url 	: "setting/terms_and_conditions",
			success : function(response){
				$scope.terms_and_conditions = response.data.terms_and_conditions;
				
			},
			error : function (response){}
		});
		
		$(".step").removeClass('current');
		$(".step.2").addClass('current');

		$scope.getCart();	
	}

	$scope.getProductDetail = function(product_code, vendor_code, index)
	{

		httpSrv.post({
			url		: 	"activities/product/detail",
			data 	: 	$.param({"product_code": product_code}),
			success : function(response){
				
				$scope.product_detail[index] =  response.data;

			},
			error 	: function(err){}
		});

		
	}

	$scope.add_to_chart = function(check = 0)
	{
		
		$scope.data = [];
		//$scope.DATA.data_rsv.customer.push($scope.add);
		$scope.add = {};

		$scope.DATA.data_rsv.error_desc = [];
		
		var data_rsv = {};
		var ss_data_rsv = angular.copy($scope.DATA.data_rsv);
		
		data_rsv.booking_source = ss_data_rsv.booking_source;
		

		if (data_rsv.booking_source == "AGENT"){
			data_rsv.agent_code = AGENT_CODE;
			
			if (ss_data_rsv.voucher_reff_number && ss_data_rsv.voucher_reff_number != ''){
				data_rsv.voucher_reff_number = ss_data_rsv.voucher_reff_number;
				
			}
		}
		
		data_rsv.booking_status = ss_data_rsv.booking_status;
		

		if (ss_data_rsv.booking_status == 'UNDEFINITE'){
			data_rsv.undefinite_valid_until_hour = ss_data_rsv.undefinite_valid_until_hour;
			
		}
		
		data_rsv.discount 		= {"type":ss_data_rsv.discount_type, "amount":ss_data_rsv.discount_amount};
		data_rsv.tax_service	= {"type":ss_data_rsv.tax_service_type, "amount":ss_data_rsv.tax_service_amount};
		data_rsv.remarks 		= ss_data_rsv.remarks;
		// data_rsv.customer 		= $scope.add.customer;
		data_rsv.products 		= [];
	
		var index = 0;
		for (i in ss_data_rsv.selected_products){
			var product = {};
			
			product.date 	= ss_data_rsv.selected_products[i].date;

			if (ss_data_rsv.selected_products[i].is_packages){
				//Packages Rates
				product.is_packages = 1;
				product.packages = {};
				product.packages.qty = ss_data_rsv.selected_products[i].qty;
				product.packages.id	= ss_data_rsv.selected_products[i].packages_option.id;
				product.packages.additional	= {};
				product.packages.additional.opt_1 = ss_data_rsv.selected_products[i].packages_option.additional_qty_opt_1 || 0;
				product.packages.additional.opt_2 = ss_data_rsv.selected_products[i].packages_option.additional_qty_opt_2 || 0;
				product.packages.additional.opt_3 = ss_data_rsv.selected_products[i].packages_option.additional_qty_opt_3 || 0;
				product.packages.rates = ss_data_rsv.selected_products[i].packages_option.rates;
				product.packages.extra_rates = ss_data_rsv.selected_products[i].packages_option.extra_rates;
				
			}else{
				product.qty_1 	= ss_data_rsv.selected_products[i].qty_1;
				if (ss_data_rsv.selected_products[i].qty_2) product.qty_2 	= ss_data_rsv.selected_products[i].qty_2;
				if (ss_data_rsv.selected_products[i].qty_3) product.qty_3	= ss_data_rsv.selected_products[i].qty_3;
				if (ss_data_rsv.selected_products[i].qty_4) product.qty_4	= ss_data_rsv.selected_products[i].qty_4;
				if (ss_data_rsv.selected_products[i].qty_5) product.qty_5	= ss_data_rsv.selected_products[i].qty_5;

				product.applicable_rates = ss_data_rsv.selected_products[i].rates.rates;
			}
		
			product.product_code	 = ss_data_rsv.selected_products[i].product.product_code;
			product.rates_code		 = ss_data_rsv.selected_products[i].rates.rates_code;
			
			product.currency = ss_data_rsv.selected_products[i].rates.currency;
			//Check Pickup Service
			if (ss_data_rsv.selected_products[i].pickup){
				
				product.pickup = ss_data_rsv.selected_products[i].pickup;
				product.pickup.area_id = product.pickup.area.id;
				product.pickup.price 	= product.pickup.area.price;
				delete product.pickup.area;
				
			}
			//Check Dropoff Service
			if (ss_data_rsv.selected_products[i].dropoff){
				
				product.dropoff = ss_data_rsv.selected_products[i].dropoff;
				product.dropoff.area_id  = product.dropoff.area.id;
				product.dropoff.price 	 = product.dropoff.area.price;
				delete product.dropoff.area;
				
			}
			
			//Check Additional Service
			if (ss_data_rsv.selected_products[i].additional_service){
				
				product.additional_service = ss_data_rsv.selected_products[i].additional_service;
				
			}
			
			 data_rsv.products[index] = product;

			 index++;
		}//End For

		$scope.putCart(data_rsv.products[0]);
			
	}

	
	$scope.loadFinish = function(){

		$(".step").removeClass('current');
		$(".step.4").addClass('current');

		$scope.vendor_code = $stateParams.id_vendor;

		httpSrv.post({
			url 	: "vendor/getVendorByCode",
			data	: $.param({"id_vendor":$scope.vendor_code}),
			success : function(response){
				$scope.DATA.vendor_detail = response.data;
			},
			error : function (response){}
		});
	}

	$scope.submit_booking = function(event){
		//';window.location = "#/trans_reservation/";
		var key = $scope.getCookies();
		
		httpSrv.post({
			url 	: "session/getSessionBooking",
			data	: $.param({"key":key}),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					var value = response.data.cart.session_value;
					$scope.submit_booking_rsv(event,value);

				}else{
					
				}
			
			}
		});

	}

	$scope.clearCart = function(){
		var key = $scope.getCookies();

		httpSrv.post({
			url 	: "session/delete",
			data	: $.param({"key":key}),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					toastr.success('delete success');

				}else{
					
				}
			
			}
		});
	}

	$scope.submit_booking_rsv = function(event, data){

		
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.data_rsv.error_desc = [];
		
		var data_rsv = {};
		
		var customer = angular.copy($scope.DATA.data_rsv.customer);


		$scope.DATA.data_rsv = angular.copy( $scope._cart.getRsv() );
	
		$scope.DATA.data_rsv.selected_products = angular.copy( data );
		$scope.DATA.data_rsv.customer = angular.copy(customer);

		var ss_data_rsv = $scope.DATA.data_rsv;

		data_rsv.booking_source = "AGENT"
		data_rsv.agent_code = AGENT_CODE;
		
		if (data_rsv.booking_source == "AGENT"){
			data_rsv.agent_code =	data_rsv.agent_code;
			if (ss_data_rsv.voucher_reff_number && ss_data_rsv.voucher_reff_number != ''){
				data_rsv.voucher_reff_number = ss_data_rsv.voucher_reff_number;
			}
		}
		
		data_rsv.booking_status = ss_data_rsv.booking_status;
		if (ss_data_rsv.booking_status == 'UNDEFINITE'){
			if (ss_data_rsv.undefinite_valid_until_type == 'HOUR'){
				data_rsv.undefinite_valid_until_hour = ss_data_rsv.undefinite_valid_until_hour;
			}else if (ss_data_rsv.undefinite_valid_until_type == 'DATE'){
				data_rsv.undefinite_valid_until_date = ss_data_rsv.undefinite_valid_until_date;
			}
		}
		
		data_rsv.discount 		= {"type":ss_data_rsv.discount_type, "amount":ss_data_rsv.discount_amount};
		data_rsv.tax_service	= {"type":ss_data_rsv.tax_service_type, "amount":ss_data_rsv.tax_service_amount};
		data_rsv.remarks 		= ss_data_rsv.remarks;
		data_rsv.customer 		= ss_data_rsv.customer;
		data_rsv.products 		= [];
		
		if (ss_data_rsv.add_commission_for_agent == '1' && ss_data_rsv.booking_source == "AGENT"){
			data_rsv.commission	= {	"type"		: ss_data_rsv.commission_type, 
									"amount"	: ss_data_rsv.commission_amount,
									"remarks"	: ss_data_rsv.commission_remarks};
		}
		
		for (i in ss_data_rsv.selected_products){
			var product = {};
			
			product.date 	= ss_data_rsv.selected_products[i].date;

			if (ss_data_rsv.selected_products[i].is_packages) {
				
				product.is_packages = 1;
				product.packages = {};
				product.packages.qty = ss_data_rsv.selected_products[i].packages.qty;
				product.packages.id	= ss_data_rsv.selected_products[i].packages.id;
				product.packages.additional	= {};
				product.packages.additional.opt_1 = ss_data_rsv.selected_products[i].packages.additional.opt_1 || 0;
				product.packages.additional.opt_2 = ss_data_rsv.selected_products[i].packages.additional.opt_2 || 0;
				product.packages.additional.opt_3 = ss_data_rsv.selected_products[i].packages.additional.opt_3 || 0;
				product.packages.rates = ss_data_rsv.selected_products[i].packages.rates;
				product.packages.extra_rates = ss_data_rsv.selected_products[i].packages.extra_rates;

			}else{
				product.qty_1 	= ss_data_rsv.selected_products[i].qty_1;
				if (ss_data_rsv.selected_products[i].qty_2) product.qty_2 	= ss_data_rsv.selected_products[i].qty_2;
				if (ss_data_rsv.selected_products[i].qty_3) product.qty_3	= ss_data_rsv.selected_products[i].qty_3;
				if (ss_data_rsv.selected_products[i].qty_4) product.qty_4	= ss_data_rsv.selected_products[i].qty_4;
				if (ss_data_rsv.selected_products[i].qty_5) product.qty_5	= ss_data_rsv.selected_products[i].qty_5;
				product.applicable_rates = ss_data_rsv.selected_products[i].rates;
			}
			
			
			
			product.product_code	 = ss_data_rsv.selected_products[i].product_code;
			product.rates_code		 = ss_data_rsv.selected_products[i].rates_code;
			

			//if (ss_data_rsv.selected_trips[i].trips.smart_pricing){
				//console.log(ss_data_rsv.selected_trips[i].trips.smart_pricing);
				
			//	if (ss_data_rsv.selected_trips[i].trips.smart_pricing && ss_data_rsv.selected_trips[i].trips.smart_pricing.type == "SMARTPRICING"){
			//		trip.smart_pricing_level = ss_data_rsv.selected_trips[i].trips.smart_pricing.level;
			//	}
				
				//if (ss_data_rsv.selected_trips[i].trips.smart_pricing.level || ss_data_rsv.selected_trips[i].trips.smart_pricing.level === 0){
					//alert(ss_data_rsv.selected_trips[i].trips.smart_pricing.level);
				//	trip.smart_pricing_level = ss_data_rsv.selected_trips[i].trips.smart_pricing.level;
				//}
			//}
			
			//Check Pickup Service
			if (ss_data_rsv.selected_products[i].pickup 
				&& ss_data_rsv.selected_products[i].pickup.area 
				&& !ss_data_rsv.selected_products[i].pickup.area.nopickup){
				
				product.pickup = ss_data_rsv.selected_products[i].pickup;
				product.pickup.area_id = product.pickup.area.id;
				product.pickup.price 	= product.pickup.area.price;
				delete product.pickup.area;
				
			}
			//Check Dropoff Service
			if (ss_data_rsv.selected_products[i].dropoff 
				&& ss_data_rsv.selected_products[i].dropoff.area 
				&& !ss_data_rsv.selected_products[i].dropoff.area.nopickup){
				
				product.dropoff = ss_data_rsv.selected_products[i].dropoff;
				product.dropoff.area_id  = product.dropoff.area.id;
				product.dropoff.price 	 = product.dropoff.area.price;
				delete product.dropoff.area;
				
			}
			
			//Check Additional Service
			if (ss_data_rsv.selected_products[i].additional_service){
				
				product.additional_service = ss_data_rsv.selected_products[i].additional_service;
				
			}
			
			data_rsv.products.push(product);
			
		}//End For
		
		if ($scope.DATA.data_rsv.add_payment == '1' && $scope.DATA.data_rsv.booking_status != 'COMPLIMEN'){
			var payment = $scope.DATA.data_rsv.payment;
			data_rsv.payment = {"amount" 		: payment.payment_amount,
								"payment_type"	: payment.payment_type,
								"account_number": payment.account_number,
								"name_on_card"	: payment.name_on_card,
								"bank_name"		: payment.bank_name,
								"payment_reff_number" : payment.payment_reff_number,
								"description"	: payment.description};
			
			if ($scope.$root.DATA_available_currency && $scope.$root.DATA_available_currency.currency && $scope.DATA.data_rsv.payment.payment_currency){
				if ($scope.DATA.data_rsv.payment.payment_currency != $scope.DATA.data_rsv.payment.default_currency){
					data_rsv.payment.payment_currency = $scope.DATA.data_rsv.payment.payment_currency;
				}
			}
			
			if (payment.payment_type == "OPENVOUCHER"){
				data_rsv.payment.openvoucher_code = payment.openvoucher_code
			}
		}
		
		if ($scope.DATA.data_rsv.payments && $scope.DATA.data_rsv.payments.detail && $scope.DATA.data_rsv.payments.detail.length > 0){
			data_rsv.payments = [];
			for (x in $scope.DATA.data_rsv.payments.detail){
				var payment = $scope.DATA.data_rsv.payments.detail[x];
				
				var payment_item = {"amount" 		: payment.payment_amount,
									"payment_type"	: payment.payment_type,
									"account_number": payment.account_number || "",
									"name_on_card"	: payment.name_on_card || "",
									"bank_name"		: payment.bank_name || "",
									"payment_reff_number" : payment.payment_reff_number || "",
									"description"	: payment.description || ""};
				
				if (payment.default_currency && payment.payment_currency && (payment.default_currency != payment.payment_currency)){
					payment_item.payment_currency = payment.payment_currency;
				}
				//console.log(payment);
				//console.log(payment_item);
				data_rsv.payments.push(payment_item);
			}
			//console.log($scope.DATA.data_rsv.payments);
		}
		
		//$scope.DATA_RSV = data_rsv;
		
		//return;
		
		httpSrv.post({
			url 	: "agent_controller_act/create_booking",
			data	: $.param(data_rsv),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					window.location = "#/detail_passenger/"+response.data.booking_code;
					toastr.success("Saved...");
					$scope.clearCart();
				}else{
					$(event.target).find("button:submit").attr("disabled","disabled");
					$scope.DATA.data_rsv.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
					$('html, body').animate({scrollTop:200}, 400);
				}
				//$.hideLoading();
			}
		});
		
		$scope.DATA_RSV = data_rsv;
		
		//fn.pre(ss_data_rsv,"ss_data_rsv");
		//fn.pre(data_rsv,"data_rsv");
	}



	// $scope.submit_booking_rsv = function(event, data){

		
	// 	var data_rsv = {};
		
	// 	data_rsv.voucher_reff_number = webStorage.get("voucher_reff_number");
	// 	data_rsv.booking_source = "AGENT";
	// 	data_rsv.agent_code = webStorage.get("agent_code");
	// 	data_rsv.booking_status = webStorage.get("booking_status");
	// 	data_rsv.undefinite_valid_until_hour = webStorage.get("undefinite_valid_until_hour");
	// 	data_rsv.discount 		= webStorage.get("discount");
	// 	data_rsv.tax_service	= webStorage.get("tax_service");
	// 	data_rsv.remarks 		= webStorage.get("remarks");
	// 	data_rsv.customer 		= $scope.add.customer;
	// 	data_rsv.products 		= [];
		

	// 	if ($scope.CART) {
	// 		data_rsv.products = $scope.CART;
	// 	}
		

	// 	data_rsv.vendor_code = $stateParams.id_vendor;

		
	// 	httpSrv.post({
	// 		url		: 	"activities/booking/create",
	// 		data 	: 	$.param(data_rsv),
	// 		success : function(response){
	// 			if (response.status = "SUCCESS") {
					
	// 				$state.go('vendor.finish', {"id_vendor": $stateParams.id_vendor});	
	// 			}

	// 		},
	// 		error 	: function(err){}
	// 	});
		
	// }

	$scope.countTotal = function(){
		
		
		if (!$scope.grand_total) {
			
			$scope.grand_total = $scope.DATA.data_rsv.TOTAL.grand_total;
		}else{
			$scope.calculate_total();
			
		}	
	}

	$scope.loadDetailRates = function(id){

		$scope.selected_rate = $scope.check_availabilities_data.selected_rates;
		$scope.selected_product = $scope.DATA.data_rsv.selected_products;
		var rate_code = id;

		var id_vendor = $stateParams.id_vendor;

		// $scope.show_data = false;

		// $scope.DATA.rates_detail = {};	

		$scope.add_product.arr_qty = {"qty_1":[], "qty_2":[], "qty_3":[], "qty_4":[], "qty_5":[]};

		for (x in $scope.check_availabilities_data.availabilities){		
			if ($scope.add_product.arr_qty.qty_1.length == 0){
				var allotment = $scope.check_availabilities_data.availabilities[x].calendar.allotment;
				for (_i = 0; _i<=allotment; _i++){
					if (_i>0) 
					$scope.add_product.arr_qty.qty_1.push({"qty":_i});
					$scope.add_product.arr_qty.qty_2.push({"qty":_i});
					$scope.add_product.arr_qty.qty_3.push({"qty":_i});
					$scope.add_product.arr_qty.qty_4.push({"qty":_i});
					$scope.add_product.arr_qty.qty_5.push({"qty":_i});
						//console.log(i);
					//console.log(i);
				}
			}
		}
		
		$scope.show_data = true;

		httpSrv.post({
			url 	: "agent_controller/getRatesDetailByCode",
			data	: $.param({"vendor_code":id_vendor, "id_rates":rate_code}),
			success : function(response){
			
				$scope.DATA.rates_detail = response.data.detail;
				$scope.countTotal();
			},
			error : function (response){}
		})

	}



	$scope.add_new_product = function(product){
		
		$scope.DATA.data_rsv = {"booking_source" : "AGENT",
								"agent_code"	 : AGENT_CODE,
								"booking_status" : "DEFINITE",
								"discount_type"	 : "%",
								"discount_amount": 0,
								"tax_service_type"	 : "%",
								"tax_service_amount" : 0,
								"undefinite_valid_until_hour" : 24,
								"selected_products"	: [],
								"customer"			: [],
								"TOTAL"			 	: {"total":0,"discount":0}};

		$scope.product_code = product.product_code;
		
		$scope.vendor_id = $stateParams.id_vendor;
		
		if ($scope.add_product.product){
			delete $scope.add_product.product;
			$scope.check_availabilities_show_OK_button = false;
			$scope.check_availabilities_data = {};
		}
		
		$scope.add_product.product = angular.copy(product);
		
		$scope.check_availabilities = function(event){
			//$.showLoading();
			
			var data = {}
			data.product_code= $scope.product_code;
			data.date = $scope.date_act;
			data.booking_source = $scope.DATA.data_rsv.booking_source;
			data.agent_code = $scope.DATA.data_rsv.agent_code;

			$(event.target).find("button:submit").attr("disabled","disabled");
			// $scope.add_product.error_desc = [];
			
			$scope.check_availabilities_show_loading = true;
			
			$scope.add_product.product.arr_qty = {"qty_1":[], "qty_2":[], "qty_3":[], "qty_4":[], "qty_5":[]};
			
			httpSrv.post({
				url 	: "activities/rates/check_availabilities",
				data	: $.param(data),
				success : function(response){
					$(event.target).find("button:submit").removeAttr("disabled");
					$scope.check_availabilities_show_loading = false;
					
					$scope.check_availabilities_data = response.data;

					if ($scope.check_availabilities_data.status == 'SUCCESS'){

						for (x in $scope.check_availabilities_data.availabilities){
							
							if ($scope.add_product.product.arr_qty.qty_1.length == 0){
								var allotment = $scope.check_availabilities_data.availabilities[x].calendar.allotment;
								for (_i = 0; _i<=allotment; _i++){
									if (_i>0) 
										$scope.add_product.product.arr_qty.qty_1.push({"qty":_i});
									$scope.add_product.product.arr_qty.qty_2.push({"qty":_i});
									$scope.add_product.product.arr_qty.qty_3.push({"qty":_i});
									$scope.add_product.product.arr_qty.qty_4.push({"qty":_i});
									$scope.add_product.product.arr_qty.qty_5.push({"qty":_i});
									//console.log(i);
								}
							}
						}
					}
				}
			});


			$scope.RSV = angular.copy($scope.DATA.data_rsv);

			$scope._cart.saveRsv($scope.RSV);
		}

		
	}


	$scope.check_availabilities_select_this_product = function (rates, rates_parent){
		//fn.pre(trip);
		if (!rates_parent.selected_rates){
			for (index in rates_parent.availabilities){
				if (rates_parent.selected_rates){
					delete rates_parent.availabilities[index].is_hidden;
				}else{
					rates_parent.availabilities[index].is_hidden = true;
				}
				delete rates_parent.availabilities[index].is_selected;
			}
			if (rates_parent.selected_rates){
				delete rates_parent.selected_rates;
			}else{
				rates.is_selected = true;
				rates.is_hidden = false;
				
				rates_parent.selected_rates = rates;
			}
			
			$scope.check_availabilities_show_OK_button = true;
		}
	}

	$scope.check_availabilities_add_to_data_rsv = function(id_vendor, product_code){
		
		//console.log($scope.check_availabilities_data);

		$scope.show_detail = false;
		
		if ($scope.check_availabilities_data && $scope.check_availabilities_data.selected_rates){
		
			///$scope._check_availabilities_add_to_data_rsv($scope.check_availabilities_data);
			
			var selected_product = {};
		
			selected_product.product	= angular.copy($scope.add_product.product);
			delete selected_product.product.short_description; delete selected_product.product.full_description; 
			delete selected_product.product.highlight;delete selected_product.product.category; 
			
			selected_product.date		= $scope.check_availabilities_data.check.date;
			

			if ($scope.check_availabilities_data.selected_rates.is_packages){
				
				//PACKAGES
				
				var total_qty_1 = total_qty_2 = total_qty_3 = 0;
				for (_x in $scope.check_availabilities_data.selected_rates.packages_option){
					
					var packages_option = $scope.check_availabilities_data.selected_rates.packages_option[_x];
					
					if (packages_option.qty){
						
						var selected_packages = angular.copy(selected_product);
						selected_packages.is_packages = 1;
						selected_packages.packages_option = packages_option;
						
						selected_packages.qty = packages_option.qty;
						
						selected_packages.additional_pax = {};
						selected_packages.additional_pax.qty_1 = packages_option.additional_qty_opt_1 || 0;
						selected_packages.additional_pax.qty_2 = packages_option.additional_qty_opt_2 || 0;
						selected_packages.additional_pax.qty_3 = packages_option.additional_qty_opt_3 || 0;
						
						selected_packages.rates = $scope.check_availabilities_data.selected_rates;
						
						//TOTAL QTY 1, QTY 2, QTY 3
						selected_packages.total_qty_1 = (packages_option.qty * packages_option.include_pax.opt_1) + selected_packages.additional_pax.qty_1;
						selected_packages.total_qty_2 = (packages_option.qty * packages_option.include_pax.opt_2) + selected_packages.additional_pax.qty_2;
						selected_packages.total_qty_3 = (packages_option.qty * packages_option.include_pax.opt_3) + selected_packages.additional_pax.qty_3;
						
						//Additional Charge
						if (selected_packages.rates.additional_charge){
							for (_x in selected_packages.rates.additional_charge){
								var name	= selected_packages.rates.additional_charge[_x].name;
								var price 	= selected_packages.rates.additional_charge[_x].price;
								var payment_based_on = selected_packages.rates.additional_charge[_x].payment_based_on;
								var qty 	= total_pax;
								if (payment_based_on == 'PERBOOKING'){
									qty 	= 1;
								}
								
								$scope.addAdditionalService(selected_packages,name, qty, price);
							}
						}
						//console.log(packages_option);
						//console.log(selected_packages);

						$scope.DATA.data_rsv.selected_products.push(selected_packages);

						$scope.calculate_subtotal(selected_packages);
						$scope.cmb_booking_status_change();
					}
				}
				//console.log(selected_packages.total_qty_1,selected_packages.total_qty_2,selected_packages.total_qty_3);
				//console.log($scope.check_availabilities_data.selected_rates);
				//console.log(selected_product);
				
				if (selected_packages.total_qty_1 + selected_packages.total_qty_2 + selected_packages.total_qty_3 > 0){
					$("#modal-add-product").modal("hide");
					$scope.show_detail = true;

					$scope.loadDetailProduct(id_vendor, product_code, $scope.DATA.data_rsv);
				}else{
					alert("Please choose at least one package!");
				}
				
			}else{

				selected_product.qty_1 	= parseInt($scope.check_availabilities_data.selected_rates.selected_qty_1)||0;
				selected_product.qty_2 	= parseInt($scope.check_availabilities_data.selected_rates.selected_qty_2)||0;
				selected_product.qty_3 	= parseInt($scope.check_availabilities_data.selected_rates.selected_qty_3)||0;
				selected_product.qty_4 	= parseInt($scope.check_availabilities_data.selected_rates.selected_qty_4)||0;
				selected_product.qty_5 	= parseInt($scope.check_availabilities_data.selected_rates.selected_qty_5)||0;
				
				var total_pax = selected_product.qty_1 + selected_product.qty_2 + selected_product.qty_3 + selected_product.qty_4 + selected_product.qty_5;
				
				if (total_pax > 0){
					selected_product.rates = $scope.check_availabilities_data.selected_rates;
					//selected_trips.trips.aplicable_rates_original = angular.copy(trip.selected_trip.aplicable_rates);
					
					/*selected_trips.sub_total =  (selected_trips.adult*selected_trips.trips.aplicable_rates.rates_1) +
												(selected_trips.child*selected_trips.trips.aplicable_rates.rates_2) +
												(selected_trips.infant*selected_trips.trips.aplicable_rates.rates_3);*/
					
					$scope.DATA.data_rsv.selected_products.push(selected_product);
					$scope.calculate_subtotal(selected_product);
					
					$("#modal-add-product").modal("hide");
					$scope.show_detail = true;
					$scope.loadDetailProduct(id_vendor, product_code, $scope.DATA.data_rsv);
				}else{
					alert("Please select total participant!");
				}
			}

		}
	}

	$scope.countStar = function(star){
		$scope.star = [];
		for (var i = 5; i >= 0; i--) {
			$scope.star.push(i);
		}
		
	}

	$scope.calculate_total = function(){
		var total = 0;
		for (x in $scope.DATA.data_rsv.selected_products){
			
			var total_person = $scope.DATA.data_rsv.selected_products[x].adult + $scope.DATA.data_rsv.selected_products[x].child;
			
			total += $scope.DATA.data_rsv.selected_products[x].sub_total;
			
			//Pickup
			if ($scope.DATA.data_rsv.selected_products[x].pickup && $scope.DATA.data_rsv.selected_products[x].pickup.area.price){
				if ($scope.DATA.data_rsv.selected_products[x].pickup.area.type == 'way'){
					pickup_sub_total = $scope.DATA.data_rsv.selected_products[x].pickup.area.price;
				}else{
					pickup_sub_total = $scope.DATA.data_rsv.selected_products[x].pickup.area.price * total_person;
				}
				total += pickup_sub_total;
			}
			//Dropoff
			if ($scope.DATA.data_rsv.selected_products[x].dropoff && $scope.DATA.data_rsv.selected_products[x].dropoff.area.price){
				if ($scope.DATA.data_rsv.selected_products[x].pickup.area.type == 'way'){
					dropoff_sub_total = $scope.DATA.data_rsv.selected_products[x].dropoff.area.price;
				}else{
					dropoff_sub_total = $scope.DATA.data_rsv.selected_products[x].dropoff.area.price * total_person;
				}
				total += dropoff_sub_total;
			}
			//Additional Service
			if ($scope.DATA.data_rsv.selected_products[x].additional_service){
				for (y in $scope.DATA.data_rsv.selected_products[x].additional_service){
					total += ($scope.DATA.data_rsv.selected_products[x].additional_service[y].qty * $scope.DATA.data_rsv.selected_products[x].additional_service[y].price);
				}
			}
		}
		//$scope.DATA.data_rsv.TOTAL = {"total":0};
		$scope.DATA.data_rsv.TOTAL.total = total;
		
		$scope.calculate_discount();
		$scope.tax_service_discount();
		
		$scope.DATA.data_rsv.TOTAL.grand_total = (total - $scope.DATA.data_rsv.TOTAL.discount) + $scope.DATA.data_rsv.TOTAL.tax_service;
		
		if (!$scope.DATA.data_rsv.payment){
			$scope.DATA.data_rsv.payment = {};
		}
		
		$scope.DATA.data_rsv.payment.payment_amount = $scope.DATA.data_rsv.TOTAL.grand_total;
		$scope.DATA.data_rsv.payment.balance = $scope.DATA.data_rsv.TOTAL.grand_total - $scope.DATA.data_rsv.payment.payment_amount;
	}

	$scope.cmb_booking_status_change = function(){
		if ($scope.DATA.data_rsv.selected_products){
			if ($scope.DATA.data_rsv.booking_status == 'COMPLIMEN'){
				for (x in $scope.DATA.data_rsv.selected_products){
					if ($scope.DATA.data_rsv.selected_products[x].rates.rates){
						
						$scope.DATA.data_rsv.selected_products[x].rates.rates_original = angular.copy($scope.DATA.data_rsv.selected_products[x].rates.rates);
						
						if ($scope.DATA.data_rsv.selected_products[x].rates.rates.rates_1){
							$scope.DATA.data_rsv.selected_products[x].rates.rates.rates_1 = 0;
						}
						if ($scope.DATA.data_rsv.selected_products[x].rates.rates.rates_2){
							$scope.DATA.data_rsv.selected_products[x].rates.rates.rates_2 = 0;
						}
						if ($scope.DATA.data_rsv.selected_products[x].rates.rates.rates_3){
							$scope.DATA.data_rsv.selected_products[x].rates.rates.rates_3 = 0;
						}
						if ($scope.DATA.data_rsv.selected_products[x].rates.rates.rates_4){
							$scope.DATA.data_rsv.selected_products[x].rates.rates.rates_4 = 0;
						}
						if ($scope.DATA.data_rsv.selected_products[x].rates.rates.rates_5){
							$scope.DATA.data_rsv.selected_products[x].rates.rates.rates_5 = 0;
						}
						$scope.calculate_subtotal($scope.DATA.data_rsv.selected_products[x]);
					}
					//console.log($scope.DATA.data_rsv.selected_products[x].rates.rates.rates_1);
				}
				//console.log($scope.DATA.data_rsv.selected_products);
			}else{
				for (x in $scope.DATA.data_rsv.selected_products){
					if ($scope.DATA.data_rsv.selected_products[x].rates.rates_original){
						$scope.DATA.data_rsv.selected_products[x].rates.rates = angular.copy($scope.DATA.data_rsv.selected_products[x].rates.rates_original);
						$scope.calculate_subtotal($scope.DATA.data_rsv.selected_products[x]);
					}
				}
				//console.log($scope.DATA.data_rsv.selected_products);
			}
		}
	}

	$scope.calculate_subtotal = function(selected_product){
		
		if (selected_product.is_packages){
			
			selected_product.sub_total = (selected_product.qty * selected_product.packages_option.rates);
			
			if (selected_product.additional_pax){
				if (selected_product.additional_pax.qty_1) selected_product.sub_total += (selected_product.additional_pax.qty_1 * selected_product.packages_option.extra_rates.opt_1);
				if (selected_product.additional_pax.qty_2) selected_product.sub_total += (selected_product.additional_pax.qty_2 * selected_product.packages_option.extra_rates.opt_2);
				if (selected_product.additional_pax.qty_3) selected_product.sub_total += (selected_product.additional_pax.qty_3 * selected_product.packages_option.extra_rates.opt_3);
			}
			
		}else{
			selected_product.sub_total =  (selected_product.qty_1*selected_product.rates.rates.rates_1);
			
			if (selected_product.qty_2) selected_product.sub_total += (selected_product.qty_2*selected_product.rates.rates.rates_2);
			if (selected_product.qty_3) selected_product.sub_total += (selected_product.qty_3*selected_product.rates.rates.rates_3);
			if (selected_product.qty_4) selected_product.sub_total += (selected_product.qty_4*selected_product.rates.rates.rates_4);
			if (selected_product.qty_5) selected_product.sub_total += (selected_product.qty_5*selected_product.rates.rates.rates_5);
		}
		
		$scope.calculate_total();
	}
	$scope.calculate_discount = function(){
		var discount = 0;
		if ($scope.DATA.data_rsv.discount_type=='%'){
			if ($scope.DATA.data_rsv.discount_amount > 100) $scope.DATA.data_rsv.discount_amount = 100;
			discount = $scope.DATA.data_rsv.TOTAL.total * $scope.DATA.data_rsv.discount_amount/100;
		}else{
			discount = angular.copy($scope.DATA.data_rsv.discount_amount);
		}
		$scope.DATA.data_rsv.TOTAL.discount = discount;
	}
	$scope.tax_service_discount = function(){
		var tax_service = 0;
		if ($scope.DATA.data_rsv.tax_service_type=='%'){
			if ($scope.DATA.data_rsv.tax_service_amount > 100) $scope.DATA.data_rsv.tax_service_amount = 100;
			tax_service = $scope.DATA.data_rsv.TOTAL.total * $scope.DATA.data_rsv.tax_service_amount/100;
		}else{
			tax_service = angular.copy($scope.DATA.data_rsv.tax_service_amount);
		}
		$scope.DATA.data_rsv.TOTAL.tax_service = tax_service;
	}

	$scope.loadProductActivities = function(){

		$(".step.1").addClass('current');
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd", minDate: fn.formatDate(new Date(),"yy-mm-dd")});
	
		$scope.check_availabilities_data = {};

		httpSrv.post({
			url 	: "activities/product",
			data	: $.param({"lang":"EN"}),
			success : function(response){
				$scope.DATA.product = response.data.products;
			},
			error : function (response){}
		});

	}

	$scope.loadDetailProduct = function(id_vendor, product_code, data){

		$(".step").removeClass('current');
		$(".step.1").addClass('current');

		$scope.DATA.data_rsv = data;
		httpSrv.post({
			url 	: "activities/product/detail",
			data	: $.param({"vendor_code":id_vendor, "product_code":product_code}),
			success : function(response){
				$scope.DATA.product_detail = response.data;
				$scope.DATA.gallery = response.data.gallery;
				$scope.DATA.detail_vendor = response.data.vendor;
			},
			error : function (response){}
		});

		httpSrv.post({
			url		: "activities/rates/product",
			data	: $.param({"vendor_code":id_vendor, "product_code":product_code}),
			success : function(response){
				$scope.DATA.rates = response.data.rates;
				$scope.DATA.select_rate = $scope.DATA.rates[0].rates_code;
				$scope.loadDetailRates($scope.DATA.select_rate);
			},
			error : function(response){}
		});
		$scope.y = {};
		
	}

	$scope.carouselNext = function(id) {
	  $('#myCarousel_'+id).carousel('next');
	}
	$scope.carouselPrev = function() {
	  $('#myCarousel'+id).carousel('prev');
	}

	$scope.loadProductDetail = function(){

		

		$scope.id_vendor = $stateParams.id_vendor;
		var product_code = $stateParams.product_code;

		
		httpSrv.post({
			url 	: "activities/product/detail",
			data	: $.param({"vendor_code":$scope.id_vendor, "product_code":product_code}),
			success : function(response){
				$scope.DATA.product_detail = response.data;
				$scope.DATA.gallery = response.data.gallery;
				$scope.DATA.detail_vendor = response.data.vendor;
			},
			error : function (response){}
		});

		httpSrv.post({
			url		: "activities/rates/product",
			data	: $.param({"vendor_code":$scope.id_vendor, "product_code":product_code}),
			success : function(response){
				$scope.DATA.rates = response.data.rates;
				$scope.DATA.select_rate = $scope.DATA.rates[0].rates_code;
				
			},
			error : function(response){}
		});
	}
	
})
// .service('Gallery', ['$http', 'httpSrv', function($http, httpSrv) {    
// 	this.get = function(vendor_code, product_code) {
// 		var data = {};
// 		httpSrv.post({
// 			url		: 	"activities/product/getGallery",
// 			data 	: 	$.param({"product_code": product_code, "vendor_code":vendor_code}),
// 			success : function(response){
// 				//data = response.data;
// 				return response.data;
// 			},
// 			error 	: function(err){}
// 		});

		
// 	}
// }])
.filter('range', function() {
  return function(input, total) {
    total = parseInt(total);

    for (var i=0; i<total; i++) {
      input.push(i);
    }

    return input;
  };
});


function activate_sub_menu_agent_detail(class_menu){
	$(".agent-detail ul.nav.sub-nav li").removeClass("active");
	$(".agent-detail ul.nav.sub-nav li."+class_menu).addClass("active");
}



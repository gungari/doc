// JavaScript Document

angular.module('app').controller('act_reservation_controller',function($scope, $http, httpSrv, $stateParams, $interval, fn){
	
	GeneralJS.activateLeftMenu("reservation");	
	
	$scope.DATA = {};
	$scope.fn = fn;

	// Gung Ari Update
	$scope.INFO = [];
	$scope.INFO.discount = [];
	$scope.INFO.fee = [];
	$scope.disc = [];
	$scope.DATA.add_fee = [];
	
	$scope.newReservationActivities = function(){
		$scope.DATA.data_rsv = {"booking_source" : "AGENT",
								"booking_status" : "DEFINITE",
								"discount_type"	 : "%",
								"discount_amount": 0,
								"tax_service_type"	 : "%",
								"tax_service_amount" : 0,
								"undefinite_valid_until_hour" : 24,
								"selected_products"	: [],
								"TOTAL"			 	: {"total":0,"discount":0}};
		
		
		//$scope.DATA.data_rsv = {"booking_source":"OFFLINE","booking_status":"DEFINITE","discount_type":"%","discount_amount":0,"tax_service_type":"%","tax_service_amount":0,"undefinite_valid_until_hour":24,"selected_products":[{"product":{"id":"3","product_code":"IHD1165","lang":"EN","name":"Indoholiday Rafting","location":{"location":"Ubud","regency":"Gianyar","province":"Bali","country":"Indonesia","address":"Desa Payangan, Kecamatan Ubud, Kabupaten Gianyar","latitude":"-8.45569044614082","longitude":"115.23559557812496"},"main_image":"http://localhost:81/IH/extranet/image/100/500/375/webimages/product/gallery/1440814717.JPG","created_date":"2016-08-16 23:15:29","updated_date":"2018-06-27 02:08:23","publish_status":"1"},"date":"2018-06-29","qty_1":1,"qty_2":2,"qty_3":3,"rates":{"product_id":"3","id":"2781","rates_code":"IHD8790","name":"Test Rates API asd","description":"","booking_handling":"INSTANT","start_date":"2018-06-26","end_date":"2018-12-26","available_on":["mon","wed","fri","sun"],"duration":30,"duration_unit":"MENIT","duration_unit_desc":"minutes(s)","min_order":2,"currency":"IDR","rates":{"rates_1":420000,"rates_2":320000,"rates_3":220000},"is_custom_sell_rate":1,"rates_caption":{"rates_1":"Adult Driver","rates_2":"Adult Pss","rates_3":"Child"},"rates_unit":{"rates_1":"Paxx","rates_2":"Paxc","rates_3":"Paxv"},"cut_of_booking":2,"pickup_service":"yes","dropoff_service":"yes","publish_status":1,"lang":"EN","rates_applied_for":"CRS","calendar":{"date":"2018-06-29","allotment":"20","rates":{"rates_1":420000,"rates_2":320000,"rates_3":220000},"is_available":"1"},"inventory_model":{"id":"837","name":"Inventory Model - Indoholiday Rafting 0002"},"is_hidden":false,"is_selected":true,"selected_qty_1":1,"selected_qty_2":2,"selected_qty_3":3},"sub_total":1720000,"pickup":{"area":{"text":"Own Transport","nopickup":true}},"dropoff":{"area":{"text":"Own Transport","nopickup":true,"currency":"IDR"}}}],"TOTAL":{"total":1720000,"discount":0,"tax_service":0,"grand_total":1720000},"payment":{"payment_amount":1720000,"balance":0},"customer":{"first_name":"Suta","last_name":"Darmayasa","email":"suta32@gmail.com","phone":"08512635675132","country_code":"ID"},"remarks":"Test Booking","add_payment":0};
		
		$scope.calculate_total();

		//console.log($scope.DATA.data_rsv);

		//Load Data Agent
		httpSrv.post({
			url 	: "agent",
			data	: $.param({"search":{"limit":"all"}}),
			success : function(response){ $scope.DATA.agents = response.data; }
		});
		
		//Load Data Country List
		httpSrv.post({
			url 	: "general/country_list",
			success : function(response){ $scope.DATA.country_list = response.data; },
		});
		
		/*//Load Data Available Port
		httpSrv.post({
			url 	: "transport/port/available_port",
			success : function(response){ $scope.DATA.available_port = response.data; },
		});*/
		
		//Load Data Booking Source
		if (!$scope.$root.DATA_booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.$root.DATA_booking_source = response.data.booking_source; },
			});
		}
		
		//Load Data Booking Status
		if (!$scope.$root.DATA_booking_status){
			httpSrv.post({
				url 	: "setting/booking_status",
				success : function(response){ $scope.$root.DATA_booking_status = response.data.booking_status; },
			});
		}
		
		//Load Data Payment Method
		if (!$scope.$root.DATA_payment_method){
			httpSrv.post({
				url 	: "setting/payment_method",
				success : function(response){ $scope.$root.DATA_payment_method = response.data.payment_method; },
			});
		}
		
	}
	$scope.add_new_product = function(){
		
		$scope.add_product = {};
		$scope.check_availabilities_data = {};
		
		//Load Data Products
		if (!$scope.DATA.products){
			httpSrv.post({
				url 	: "activities/product",
				//data	: $.param({"publish_status":"all"}),
				success : function(response){
					$scope.DATA.products = response.data;
				},
				error : function (response){}
			});
		}
		//--
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});

		$scope.check_availabilities_show_OK_button = false;
	}
	$scope.add_new_product_select_this_product = function(selected_product){
		if ($scope.add_product.product){
			delete $scope.add_product.product;
			$scope.check_availabilities_show_OK_button = false;
			$scope.check_availabilities_data = {};
		}else{
			$scope.add_product.product = angular.copy(selected_product);
		}
	}
	
	$scope.check_availabilities = function(event){
		//$.showLoading();
		
		var data = {}
		data.product_code= $scope.add_product.product.product_code;
		data.date = $scope.add_product.date;
		
		if ($scope.DATA.data_rsv.booking_source == "AGENT" && $scope.DATA.data_rsv.agent){
			data.agent_code = $scope.DATA.data_rsv.agent.agent_code;
		}
		
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.add_product.error_desc = [];
		$scope.check_availabilities_data = {};
		$scope.check_availabilities_show_loading = true;
		
		$scope.add_product.product.arr_qty = {"qty_1":[], "qty_2":[], "qty_3":[], "qty_4":[], "qty_5":[]};
		
		httpSrv.post({
			url 	: "activities/rates/check_availabilities",
			data	: $.param(data),
			success : function(response){
				$(event.target).find("button:submit").removeAttr("disabled");
				$scope.check_availabilities_show_loading = false;
				
				$scope.check_availabilities_data = response.data;
				
				//$scope.check_availabilities_data.selected_rates = {};
				//$scope.check_availabilities_data.selected_rates.selected_qty_1 = '1';
				//$scope.check_availabilities_data.selected_rates.selected_qty_2 = '0';
				//$scope.check_availabilities_data.selected_rates.selected_qty_3 = '0';
				//$scope.check_availabilities_data.selected_rates.selected_qty_4 = '0';
				//$scope.check_availabilities_data.selected_rates.selected_qty_5 = '0';
				
				if ($scope.check_availabilities_data.status == 'SUCCESS'){
					for (x in $scope.check_availabilities_data.availabilities){
						
						if ($scope.add_product.product.arr_qty.qty_1.length == 0){
							var allotment = $scope.check_availabilities_data.availabilities[x].calendar.allotment;
							for (_i = 0; _i<=allotment; _i++){
								if (_i>0) $scope.add_product.product.arr_qty.qty_1.push({"qty":_i});
								$scope.add_product.product.arr_qty.qty_2.push({"qty":_i});
								$scope.add_product.product.arr_qty.qty_3.push({"qty":_i});
								$scope.add_product.product.arr_qty.qty_4.push({"qty":_i});
								$scope.add_product.product.arr_qty.qty_5.push({"qty":_i});
								//console.log(i);
							}
						}
					}
				}
			}
		});
	}
	$scope.check_availabilities_select_this_product = function (rates, rates_parent){
		//fn.pre(trip);
		if (!rates_parent.selected_rates){
			for (index in rates_parent.availabilities){
				if (rates_parent.selected_rates){
					delete rates_parent.availabilities[index].is_hidden;
				}else{
					rates_parent.availabilities[index].is_hidden = true;
				}
				delete rates_parent.availabilities[index].is_selected;
			}
			if (rates_parent.selected_rates){
				delete rates_parent.selected_rates;
			}else{
				rates.is_selected = true;
				rates.is_hidden = false;
				
				rates_parent.selected_rates = rates;
			}
			
			$scope.check_availabilities_show_OK_button = true;
		}
	}
	$scope.check_availabilities_add_to_data_rsv = function(){
		
		//console.log($scope.check_availabilities_data);
		
		if ($scope.check_availabilities_data && $scope.check_availabilities_data.selected_rates){
		
			///$scope._check_availabilities_add_to_data_rsv($scope.check_availabilities_data);
			
			var selected_product = {};
		
			selected_product.product	= angular.copy($scope.add_product.product);
			delete selected_product.product.short_description; delete selected_product.product.full_description; 
			delete selected_product.product.highlight;delete selected_product.product.category; 
			
			selected_product.date		= $scope.check_availabilities_data.check.date;
			
			if ($scope.check_availabilities_data.selected_rates.is_packages){
				
				//PACKAGES
				
				var total_qty_1 = total_qty_2 = total_qty_3 = 0;
				for (_x in $scope.check_availabilities_data.selected_rates.packages_option){
					
					var packages_option = $scope.check_availabilities_data.selected_rates.packages_option[_x];
					
					if (packages_option.qty){
						
						var selected_packages = angular.copy(selected_product);
						selected_packages.is_packages = 1;
						selected_packages.packages_option = packages_option;
						
						selected_packages.qty = packages_option.qty;
						
						selected_packages.additional_pax = {};
						selected_packages.additional_pax.qty_1 = packages_option.additional_qty_opt_1 || 0;
						selected_packages.additional_pax.qty_2 = packages_option.additional_qty_opt_2 || 0;
						selected_packages.additional_pax.qty_3 = packages_option.additional_qty_opt_3 || 0;
						
						selected_packages.rates = $scope.check_availabilities_data.selected_rates;
						
						//TOTAL QTY 1, QTY 2, QTY 3
						selected_packages.total_qty_1 = (packages_option.qty * packages_option.include_pax.opt_1) + selected_packages.additional_pax.qty_1;
						selected_packages.total_qty_2 = (packages_option.qty * packages_option.include_pax.opt_2) + selected_packages.additional_pax.qty_2;
						selected_packages.total_qty_3 = (packages_option.qty * packages_option.include_pax.opt_3) + selected_packages.additional_pax.qty_3;
						
						//Additional Charge
						if (selected_packages.rates.additional_charge){
							for (_x in selected_packages.rates.additional_charge){
								var name	= selected_packages.rates.additional_charge[_x].name;
								var price 	= selected_packages.rates.additional_charge[_x].price;
								var payment_based_on = selected_packages.rates.additional_charge[_x].payment_based_on;
								var qty 	= total_pax;
								if (payment_based_on == 'PERBOOKING'){
									qty 	= 1;
								}
								
								$scope.addAdditionalService(selected_packages,name, qty, price);
							}
						}
						//console.log(packages_option);
						//console.log(selected_packages);

						$scope.DATA.data_rsv.selected_products.push(selected_packages);
						$scope.calculate_subtotal(selected_packages);
						$scope.cmb_booking_status_change();
					}
				}
				//console.log(selected_packages.total_qty_1,selected_packages.total_qty_2,selected_packages.total_qty_3);
				//console.log($scope.check_availabilities_data.selected_rates);
				//console.log(selected_product);
				
				if (selected_packages.total_qty_1 + selected_packages.total_qty_2 + selected_packages.total_qty_3 > 0){
					$("#modal-add-product").modal("hide");
				}else{
					alert("Please choose at least one package!");
				}
				
			}else{

				selected_product.qty_1 	= parseInt($scope.check_availabilities_data.selected_rates.selected_qty_1)||0;
				selected_product.qty_2 	= parseInt($scope.check_availabilities_data.selected_rates.selected_qty_2)||0;
				selected_product.qty_3 	= parseInt($scope.check_availabilities_data.selected_rates.selected_qty_3)||0;
				selected_product.qty_4 	= parseInt($scope.check_availabilities_data.selected_rates.selected_qty_4)||0;
				selected_product.qty_5 	= parseInt($scope.check_availabilities_data.selected_rates.selected_qty_5)||0;
				
				var total_pax = selected_product.qty_1 + selected_product.qty_2 + selected_product.qty_3 + selected_product.qty_4 + selected_product.qty_5;
				
				if (total_pax > 0){
					selected_product.rates = $scope.check_availabilities_data.selected_rates;
					//selected_trips.trips.aplicable_rates_original = angular.copy(trip.selected_trip.aplicable_rates);
					
					/*selected_trips.sub_total =  (selected_trips.adult*selected_trips.trips.aplicable_rates.rates_1) +
												(selected_trips.child*selected_trips.trips.aplicable_rates.rates_2) +
												(selected_trips.infant*selected_trips.trips.aplicable_rates.rates_3);*/
					
					$scope.DATA.data_rsv.selected_products.push(selected_product);
					$scope.calculate_subtotal(selected_product);
					$scope.cmb_booking_status_change();

					$("#modal-add-product").modal("hide");
				}else{
					alert("Please select total participant!");
				}
			}

		}

		//$scope.aplicable_rates_for_agent();

		//console.log($scope.check_availabilities_data);
		//console.log($scope.DATA.data_rsv);
	}

	$scope.cmb_booking_status_change = function(){
		if ($scope.DATA.data_rsv.selected_products){
			if ($scope.DATA.data_rsv.booking_status == 'COMPLIMEN'){
				for (x in $scope.DATA.data_rsv.selected_products){
					if ($scope.DATA.data_rsv.selected_products[x].rates.rates){
						
						$scope.DATA.data_rsv.selected_products[x].rates.rates_original = angular.copy($scope.DATA.data_rsv.selected_products[x].rates.rates);
						
						if ($scope.DATA.data_rsv.selected_products[x].rates.rates.rates_1){
							$scope.DATA.data_rsv.selected_products[x].rates.rates.rates_1 = 0;
						}
						if ($scope.DATA.data_rsv.selected_products[x].rates.rates.rates_2){
							$scope.DATA.data_rsv.selected_products[x].rates.rates.rates_2 = 0;
						}
						if ($scope.DATA.data_rsv.selected_products[x].rates.rates.rates_3){
							$scope.DATA.data_rsv.selected_products[x].rates.rates.rates_3 = 0;
						}
						if ($scope.DATA.data_rsv.selected_products[x].rates.rates.rates_4){
							$scope.DATA.data_rsv.selected_products[x].rates.rates.rates_4 = 0;
						}
						if ($scope.DATA.data_rsv.selected_products[x].rates.rates.rates_5){
							$scope.DATA.data_rsv.selected_products[x].rates.rates.rates_5 = 0;
						}
						$scope.calculate_subtotal($scope.DATA.data_rsv.selected_products[x]);
					}
					//console.log($scope.DATA.data_rsv.selected_products[x].rates.rates.rates_1);
				}
				//console.log($scope.DATA.data_rsv.selected_products);
			}else{
				for (x in $scope.DATA.data_rsv.selected_products){
					if ($scope.DATA.data_rsv.selected_products[x].rates.rates_original){
						$scope.DATA.data_rsv.selected_products[x].rates.rates = angular.copy($scope.DATA.data_rsv.selected_products[x].rates.rates_original);
						$scope.calculate_subtotal($scope.DATA.data_rsv.selected_products[x]);
					}
				}
				//console.log($scope.DATA.data_rsv.selected_products);
			}
		}
	}
	
	$scope.calculate_total = function(){
		var total = 0;
		for (x in $scope.DATA.data_rsv.selected_products){
			
			if ($scope.DATA.data_rsv.selected_products[x].is_packages){
				var total_person =  $scope.DATA.data_rsv.selected_products[x].total_qty_1 +
									$scope.DATA.data_rsv.selected_products[x].total_qty_2 +
									$scope.DATA.data_rsv.selected_products[x].total_qty_3;
			}else{
				var total_person =  $scope.DATA.data_rsv.selected_products[x].qty_1 +
									$scope.DATA.data_rsv.selected_products[x].qty_2 +
									$scope.DATA.data_rsv.selected_products[x].qty_3 +
									$scope.DATA.data_rsv.selected_products[x].qty_4 + 
									$scope.DATA.data_rsv.selected_products[x].qty_5;
			}
			
			total += $scope.DATA.data_rsv.selected_products[x].sub_total;
			
			//Pickup
			if ($scope.DATA.data_rsv.selected_products[x].pickup && $scope.DATA.data_rsv.selected_products[x].pickup.area.price){
				if ($scope.DATA.data_rsv.selected_products[x].pickup.area.type == 'way'){
					pickup_sub_total = $scope.DATA.data_rsv.selected_products[x].pickup.area.price;
				}else{
					pickup_sub_total = $scope.DATA.data_rsv.selected_products[x].pickup.area.price * total_person;
				}
				total += pickup_sub_total;
			}
			//Dropoff
			if ($scope.DATA.data_rsv.selected_products[x].dropoff && $scope.DATA.data_rsv.selected_products[x].dropoff.area.price){
				if ($scope.DATA.data_rsv.selected_products[x].pickup.area.type == 'way'){
					dropoff_sub_total = $scope.DATA.data_rsv.selected_products[x].dropoff.area.price;
				}else{
					dropoff_sub_total = $scope.DATA.data_rsv.selected_products[x].dropoff.area.price * total_person;
				}
				total += dropoff_sub_total;
			}
			//Additional Service
			if ($scope.DATA.data_rsv.selected_products[x].additional_service){
				for (y in $scope.DATA.data_rsv.selected_products[x].additional_service){
					total += ($scope.DATA.data_rsv.selected_products[x].additional_service[y].qty * $scope.DATA.data_rsv.selected_products[x].additional_service[y].price);
				}
			}
		}
		//$scope.DATA.data_rsv.TOTAL = {"total":0};
		$scope.DATA.data_rsv.TOTAL.total = total;
		
		$scope.calculate_discount();
		$scope.tax_service_discount();
		
		$scope.DATA.data_rsv.TOTAL.grand_total = (total - $scope.DATA.data_rsv.TOTAL.discount) + $scope.DATA.data_rsv.TOTAL.tax_service;
		
		if (!$scope.DATA.data_rsv.payment){
			$scope.DATA.data_rsv.payment = {};
		}
		
		$scope.DATA.data_rsv.payment.payment_amount = $scope.DATA.data_rsv.TOTAL.grand_total;
		$scope.DATA.data_rsv.payment.balance = $scope.DATA.data_rsv.TOTAL.grand_total - $scope.DATA.data_rsv.payment.payment_amount;
	}
	$scope.calculate_subtotal = function(selected_product){
		
		if (selected_product.is_packages){
			
			selected_product.sub_total = (selected_product.qty * selected_product.packages_option.rates);
			
			if (selected_product.additional_pax){
				if (selected_product.additional_pax.qty_1) selected_product.sub_total += (selected_product.additional_pax.qty_1 * selected_product.packages_option.extra_rates.opt_1);
				if (selected_product.additional_pax.qty_2) selected_product.sub_total += (selected_product.additional_pax.qty_2 * selected_product.packages_option.extra_rates.opt_2);
				if (selected_product.additional_pax.qty_3) selected_product.sub_total += (selected_product.additional_pax.qty_3 * selected_product.packages_option.extra_rates.opt_3);
			}
			
		}else{
			selected_product.sub_total =  (selected_product.qty_1*selected_product.rates.rates.rates_1);
			
			if (selected_product.qty_2) selected_product.sub_total += (selected_product.qty_2*selected_product.rates.rates.rates_2);
			if (selected_product.qty_3) selected_product.sub_total += (selected_product.qty_3*selected_product.rates.rates.rates_3);
			if (selected_product.qty_4) selected_product.sub_total += (selected_product.qty_4*selected_product.rates.rates.rates_4);
			if (selected_product.qty_5) selected_product.sub_total += (selected_product.qty_5*selected_product.rates.rates.rates_5);
		}
		
		$scope.calculate_total();
	}
	$scope.calculate_discount = function(){
		var discount = 0;
		if ($scope.DATA.data_rsv.discount_type=='%'){
			if ($scope.DATA.data_rsv.discount_amount > 100) $scope.DATA.data_rsv.discount_amount = 100;
			discount = $scope.DATA.data_rsv.TOTAL.total * $scope.DATA.data_rsv.discount_amount/100;
		}else{
			discount = angular.copy($scope.DATA.data_rsv.discount_amount);
		}
		$scope.DATA.data_rsv.TOTAL.discount = discount;
	}
	$scope.tax_service_discount = function(){
		var tax_service = 0;
		if ($scope.DATA.data_rsv.tax_service_type=='%'){
			if ($scope.DATA.data_rsv.tax_service_amount > 100) $scope.DATA.data_rsv.tax_service_amount = 100;
			tax_service = $scope.DATA.data_rsv.TOTAL.total * $scope.DATA.data_rsv.tax_service_amount/100;
		}else{
			tax_service = angular.copy($scope.DATA.data_rsv.tax_service_amount);
		}
		$scope.DATA.data_rsv.TOTAL.tax_service = tax_service;
	}
	$scope.remove_product = function (index){
		if (confirm("Are you sure to delete this product?")){
			$scope.DATA.data_rsv.selected_products.splice(index,1);
			$scope.calculate_total();
		}
	}
	$scope.addAdditionalService = function (trip){
		var additional = {"name":"", "qty":1, "price":0};
		if (!trip.additional_service){
			trip.additional_service = [];
		}
		trip.additional_service.push(additional);
	}
	$scope.removeAdditionalService = function (trips, index){
		trips.additional_service.splice(index,1);
	}
	
	$scope.submit_booking = function(event){
		
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.data_rsv.error_desc = [];
		
		var data_rsv = {};
		var ss_data_rsv = angular.copy($scope.DATA.data_rsv);
		
		// data_rsv.booking_source = ss_data_rsv.booking_source;
		data_rsv.booking_source = 'AGENT';
		// if (data_rsv.booking_source == "AGENT"){

		// Agent 	
		//data_rsv.agent_code = ss_data_rsv.agent.agent_code;
		if (ss_data_rsv.voucher_reff_number && ss_data_rsv.voucher_reff_number != ''){
			data_rsv.voucher_reff_number = ss_data_rsv.voucher_reff_number;
		}

		// }
		// else if (data_rsv.booking_source == "OFFLINE"){
		// 	if (ss_data_rsv.booking_source_sub){
		// 		data_rsv.booking_source_sub = ss_data_rsv.booking_source_sub;
		// 	}
		// }
		
		data_rsv.booking_status = ss_data_rsv.booking_status;
		if (ss_data_rsv.booking_status == 'UNDEFINITE'){
			data_rsv.undefinite_valid_until_hour = ss_data_rsv.undefinite_valid_until_hour;
		}
		
		data_rsv.discount 		= {"type":ss_data_rsv.discount_type, "amount":ss_data_rsv.discount_amount};
		data_rsv.tax_service	= {"type":ss_data_rsv.tax_service_type, "amount":ss_data_rsv.tax_service_amount};
		data_rsv.remarks 		= ss_data_rsv.remarks;
		data_rsv.customer 		= ss_data_rsv.customer;
		data_rsv.products 		= [];
		
		for (i in ss_data_rsv.selected_products){
			var product = {};
			
			product.date 	= ss_data_rsv.selected_products[i].date;
			
			product.qty_1 	= ss_data_rsv.selected_products[i].qty_1;
			if (ss_data_rsv.selected_products[i].qty_2) product.qty_2 	= ss_data_rsv.selected_products[i].qty_2;
			if (ss_data_rsv.selected_products[i].qty_3) product.qty_3	= ss_data_rsv.selected_products[i].qty_3;
			if (ss_data_rsv.selected_products[i].qty_4) product.qty_4	= ss_data_rsv.selected_products[i].qty_4;
			if (ss_data_rsv.selected_products[i].qty_5) product.qty_5	= ss_data_rsv.selected_products[i].qty_5;
			
			product.product_code	 = ss_data_rsv.selected_products[i].product.product_code;
			product.rates_code		 = ss_data_rsv.selected_products[i].rates.rates_code;
			product.applicable_rates = ss_data_rsv.selected_products[i].rates.rates;

			//if (ss_data_rsv.selected_trips[i].trips.smart_pricing){
				//console.log(ss_data_rsv.selected_trips[i].trips.smart_pricing);
				
			//	if (ss_data_rsv.selected_trips[i].trips.smart_pricing && ss_data_rsv.selected_trips[i].trips.smart_pricing.type == "SMARTPRICING"){
			//		trip.smart_pricing_level = ss_data_rsv.selected_trips[i].trips.smart_pricing.level;
			//	}
				
				//if (ss_data_rsv.selected_trips[i].trips.smart_pricing.level || ss_data_rsv.selected_trips[i].trips.smart_pricing.level === 0){
					//alert(ss_data_rsv.selected_trips[i].trips.smart_pricing.level);
				//	trip.smart_pricing_level = ss_data_rsv.selected_trips[i].trips.smart_pricing.level;
				//}
			//}
			
			//Check Pickup Service
			if (ss_data_rsv.selected_products[i].pickup 
				&& ss_data_rsv.selected_products[i].pickup.area 
				&& !ss_data_rsv.selected_products[i].pickup.area.nopickup){
				
				product.pickup = ss_data_rsv.selected_products[i].pickup;
				product.pickup.area_id = product.pickup.area.id;
				product.pickup.price 	= product.pickup.area.price;
				delete product.pickup.area;
				
			}
			//Check Dropoff Service
			if (ss_data_rsv.selected_products[i].dropoff 
				&& ss_data_rsv.selected_products[i].dropoff.area 
				&& !ss_data_rsv.selected_products[i].dropoff.area.nopickup){
				
				product.dropoff = ss_data_rsv.selected_products[i].dropoff;
				product.dropoff.area_id  = product.dropoff.area.id;
				product.dropoff.price 	 = product.dropoff.area.price;
				delete product.dropoff.area;
				
			}
			
			//Check Additional Service
			if (ss_data_rsv.selected_products[i].additional_service){
				
				product.additional_service = ss_data_rsv.selected_products[i].additional_service;
				
			}
			
			data_rsv.products.push(product);
			
		}//End For
		
		if ($scope.DATA.data_rsv.add_payment == '1' && $scope.DATA.data_rsv.booking_status != 'COMPLIMEN'){
			var payment = $scope.DATA.data_rsv.payment;
			data_rsv.payment = {"amount" 		: payment.payment_amount,
								"payment_type"	: payment.payment_type,
								"account_number": payment.account_number,
								"name_on_card"	: payment.name_on_card,
								"bank_name"		: payment.bank_name,
								"payment_reff_number" : payment.payment_reff_number,
								"description"	: payment.description};
			if (payment.payment_type == "OPENVOUCHER"){
				data_rsv.payment.openvoucher_code = payment.openvoucher_code
			}
		}
		
		//$scope.DATA_RSV = data_rsv;
	
		//return;
		
		httpSrv.post({
			url 	: "agent_controller_act/create_booking",
			data	: $.param(data_rsv),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					window.location = "#/detail_passenger/"+response.data.booking_code;
					toastr.success("Saved...");
				}else{
					$(event.target).find("button:submit").attr("disabled","disabled");
					$scope.DATA.data_rsv.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
					$('html, body').animate({scrollTop:200}, 400);
				}
				
			}
		});
		
		// $scope.DATA_RSV = data_rsv;
		
		//fn.pre(ss_data_rsv,"ss_data_rsv");
		//fn.pre(data_rsv,"data_rsv");
	}
	
	$scope.loadDataBookingTransportDetail = function(booking_code, after_load_handler_function){
		
		if (!booking_code){ booking_code = $stateParams.booking_code; }
		
		httpSrv.get({
			url 	: "activities/booking/detail/"+booking_code,
			success : function(response){
				var booking = response.data;
				//console.log(booking);
				if (booking.status == "SUCCESS"){
					$scope.DATA.current_booking = booking;
					
					if (fn.isFn(after_load_handler_function)) {
						after_load_handler_function(booking);
					}
				}else{
					alert("Sorry, data not found..");
					window.location = "#/trans_reservation";
				}
			},
			error : function (response){}
		});
	}
	$scope.loadDataBookingPaymentPrint = function(){
		booking_code = $stateParams.booking_code;
		
		
		$scope.loadDataBookingTransportDetail(booking_code, function(booking_detail){
			$scope.DATA.current_booking = booking_detail;
		});
		
		$scope.booking_code = booking_code;

		httpSrv.get({
			url 	: "payment/reservation/"+booking_code,
			success : function(response){
				$scope.DATA.payment = response.data;
				
				//console.log($scope.DATA.payment);
			},
			error : function (response){}
		});

	}
	$scope.loadDataBookingTransportPassenger = function(){
		
		booking_code = $stateParams.booking_code;
		
		var load_data_passenger = function (booking_detail){
			//console.log(booking_detail);
		}
		
		if ($scope.$parent.DATA.current_booking){
			
			$scope.DATA.current_booking = $scope.$parent.DATA.current_booking;
			
			load_data_passenger($scope.DATA.current_booking);
			
		}else{
			$scope.loadDataBookingTransportDetail(booking_code, function(current_booking){
				$scope.DATA.current_booking = current_booking;
				load_data_passenger($scope.DATA.current_booking);
			});
		}
	}
	$scope.loadDataBookingTransport_2 = function(page, show_loading){
		
		if (!$scope.DATA.bookings || show_loading){
			$scope.show_loading_DATA_bookings = true;
		}
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		
		if (!$scope.search){ 
			var now = new Date(),
				start_date = new Date(),
				end_date = new Date();
			
			start_date.setDate(start_date.getDate() - 30);
			
			$scope.search = { "start_date" : $scope.fn.formatDate(start_date, "yy-mm-dd"),
							  "end_date" : $scope.fn.formatDate(end_date, "yy-mm-dd"),}; 
		}
		
		if (!page) page = 1;
		
		var _search = $scope.search;
			_search.page = page;
		
		httpSrv.post({
			url 	: "agent_controller_act/booking_and_voucher_list",
			data	: $.param({"search":_search}),
			success : function(response){
				$scope.show_loading_DATA_bookings = false;
				$scope.DATA.bookings = response.data;
				
				if ($scope.DATA.bookings.search){
					$scope.DATA.bookings.search.pagination = [];
					for (i=1;i<=$scope.DATA.bookings.search.number_of_pages; i++){
						$scope.DATA.bookings.search.pagination.push(i);
					}
				}
			},
			error : function (response){}
		});
		
		//Load Data Booking Source
		if (!$scope.$root.DATA_booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.$root.DATA_booking_source = response.data.booking_source; },
			});
		}
		
		//Load Data Booking Status
		if (!$scope.$root.DATA_booking_status){
			httpSrv.post({
				url 	: "setting/booking_status",
				success : function(response){ $scope.$root.DATA_booking_status = response.data.booking_status; },
			});
		}
	}
	
	$scope.arrivalDepartureAct = function(){
		GeneralJS.activateLeftMenu("arrival_departure");	
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		
		$scope.dept = { "date" : fn.formatDate(new Date(), "yy-mm-dd"),
						"status": "",
						"product": ""};

		httpSrv.post({
			url : "activities/product",
			success : function(response){
				$scope.DATA.products = response.data.products;
			},	
			error : function(err){}
		});

		httpSrv.post({
			url : "setting/booking_status",
			success : function(response){
				$scope.DATA.booking_status = response.data.booking_status;
			},	
			error : function(err){}
		});
	}
	$scope.arrivalDepartureActSearch = function(data, after_load_handler_function){
		$scope.show_arrivalDepartureTransSearch_loading = true;
		
		if (!data){
			var data = {"date" : $scope.dept.date,
						"status" : $scope.dept.status,
						"product": $scope.dept.product};
		}
		
		httpSrv.post({
				url 	: "agent_controller_act/passenger_list",
				data	: $.param(data),
				success : function(response){
					$scope.show_arrivalDepartureTransSearch_loading = false;
					$scope.search = response.data.search;
					
					$scope.summary = {"total_all":0,"total_checked_in":0};
					
					if (response.data.status == "SUCCESS"){
						$scope.arrival_departure = response.data.passenger_list;
						$scope.summary = response.data.summary;
						
						if (fn.isFn(after_load_handler_function)) {
							after_load_handler_function(response.data);
						}
						
					}else{
						$scope.arrival_departure = [];
					}
				}
			});
		
	}
	$scope.openPopUpWindowVoucher = function (base_url, booking_code, voucher_code){
		var myWindow = window.open(base_url+booking_code+'/'+voucher_code, 'VoucherWindow', 'width=800,height=600');
	}
	
	//Edit Booking
	$scope.editDataBookingActivitiesDetail = function(){
		var booking_code = $stateParams.booking_code;
		$scope.loadDataBookingTransportDetail(booking_code, function(booking){
		});
	}
	$scope.update_remarks = function(event){
		var booking_code = $scope.DATA.current_booking.booking.booking_code;
		var remarks		 = $scope.DATA.current_booking.booking.remarks;
		httpSrv.post({
			url 	: "booking/update_remarks",
			data	: $.param({"booking_code": booking_code, "remarks": remarks}),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					toastr.success("Remarks updated!");
				}
			}
		});
	}
	$scope.edit_customer = function(){
		
		$scope.DATA.myCustomer = angular.copy($scope.DATA.current_booking.booking.customer);
		$scope.DATA.myCustomer.error_desc = [];

		// get country list
		//Load Data Country List
		if (!$scope.$root.DATA_country_list){
			httpSrv.post({
				url 	: "general/country_list",
				success : function(response){ $scope.$root.DATA_country_list = response.data.country_list;},
			});
		}
		$("#modal-add-cust .btn-submit-edit-cust").removeAttr("disabled");
	}
	$scope.update_customer = function (event){
		
		$(event.target).find("button:submit").attr("disabled","disabled");
		
		var data_submit = $scope.DATA.myCustomer;
		data_submit.booking_code = $scope.DATA.current_booking.booking.booking_code;
		
		$scope.DATA.myCustomer.error_desc = [];
		
		httpSrv.post({
			url 	: "booking/update_customer",
			data	: $.param(data_submit),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataBookingTransportDetail();
					$("#modal-add-cust").modal("hide");
					toastr.success("Updated...");
				}else{
					$scope.DATA.myCustomer.error_desc = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
			}
		});
	}
	
	$scope.edit_pickup_dropoff = function(type, booking_detail){
		
		$scope.DATA.myPickupDropoff = {};
		$scope.DATA.myPickupDropoff.transport_service = "yes";
		if (type == "PICKUP" && booking_detail.pickup){
			$scope.DATA.myPickupDropoff = angular.copy(booking_detail.pickup);
			$scope.DATA.myPickupDropoff.transport_service = "yes";
		}else if (type == "DROPOFF" && booking_detail.dropoff){
			$scope.DATA.myPickupDropoff = angular.copy(booking_detail.dropoff);
			$scope.DATA.myPickupDropoff.transport_service = "yes";
		}

		$scope.DATA.myPickupDropoff.voucher_code = booking_detail.voucher_code;
		$scope.DATA.myPickupDropoff.type = type;
		
		httpSrv.post({
			url 	: "activities/rates/rates_detail_by_id/"+booking_detail.rates.id,
			success : function(response){ 
			
				if (response.data.status == "SUCCESS"){
					if (type == "PICKUP"){
						if (response.data.pickup_area){
							$scope.DATA.myPickupDropoff.rates_area = response.data.pickup_area;
						}else{
							$scope.DATA.myPickupDropoff.available = false;
						}
					}else if (type == "DROPOFF"){
						if (response.data.dropoff_area){
							$scope.DATA.myPickupDropoff.rates_area = response.data.dropoff_area;
						}else{
							$scope.DATA.myPickupDropoff.available = false;
						}
					}

					for (_x in $scope.DATA.myPickupDropoff.rates_area){
						if ($scope.DATA.myPickupDropoff.rates_area[_x].area == $scope.DATA.myPickupDropoff.area){
							$scope.DATA.myPickupDropoff.selected_area = $scope.DATA.myPickupDropoff.rates_area[_x];
							break
						}
					}
				}
			},
		});
	}
	$scope.submit_pickup_dropoff = function(event){
		
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myPickupDropoff.error_desc = [];
		
		//console.log($scope.DATA.myPickupDropoff);
		//return;
		
		if ($scope.DATA.myPickupDropoff.transport_service == "no"){
			var data_submit = {	"type" 				: $scope.DATA.myPickupDropoff.type,
								"voucher_code" 		: $scope.DATA.myPickupDropoff.voucher_code};
			httpSrv.post({
				url 	: "booking/remove_pickup_dropoff",
				data	: $.param(data_submit),
				success : function(response){
					if (response.data.status == "SUCCESS"){
						$scope.editDataBookingActivitiesDetail();
						$("#modal-add-picdrp").modal("hide");
						toastr.success(data_submit.type+" removed...");
					}else{
						$scope.DATA.myPickupDropoff.error_desc = response.data.error_desc;
					}
					$(event.target).find("button:submit").removeAttr("disabled");
				}
			});
		}else{
			var data_submit = {	"type" 				: $scope.DATA.myPickupDropoff.type,
								"area"				: $scope.DATA.myPickupDropoff.selected_area.area,
								"time"				: $scope.DATA.myPickupDropoff.selected_area.time,
								"voucher_code" 		: $scope.DATA.myPickupDropoff.voucher_code,
								"hotel_name" 		: $scope.DATA.myPickupDropoff.hotel_name,
								"hotel_address" 	: $scope.DATA.myPickupDropoff.hotel_address,
								"hotel_phone_number": $scope.DATA.myPickupDropoff.hotel_phone_number,
								"price"				: $scope.DATA.myPickupDropoff.price};
			httpSrv.post({
				url 	: "booking/update_pickup_dropoff",
				data	: $.param(data_submit),
				success : function(response){
					if (response.data.status == "SUCCESS"){
						$scope.editDataBookingActivitiesDetail();
						$("#modal-add-picdrp").modal("hide");
						toastr.success(data_submit.type+" updated...");
					}else{
						$scope.DATA.myPickupDropoff.error_desc = response.data.error_desc;
					}
					$(event.target).find("button:submit").removeAttr("disabled");
				}
			});
		}
	}
	//--
	
	
	
	
	

	$scope.addEditPayment = function (current_payment){
		
		//Load Data Payment Method
		if (!$scope.$root.DATA_payment_method){
			httpSrv.post({
				url 	: "setting/payment_method",
				success : function(response){ $scope.$root.DATA_payment_method = response.data.payment_method; },
			});
		}
		
		if (current_payment){
			$scope.DATA.myPayment = angular.copy(current_payment);
		}else{
			$scope.DATA.myPayment = {"booking_code" : $scope.DATA.current_booking.booking.booking_code,
									 "date" : fn.formatDate(new Date(),"yy-mm-dd"),
									 "description":"Payment #"+$scope.DATA.current_booking.booking.booking_code,
									 "currency" : $scope.DATA.payment.payment.currency,
									 "amount" : $scope.DATA.payment.payment.balance};
			//$scope.DATA.myPayment.payment_type = 'OPENVOUCHER';
			//$scope.changePaymentType();
		}
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});

		if ($scope.DATA.current_booking.booking.source_code == 'AGENT') {

			var agent = $scope.DATA.current_booking.booking.agent;
			var agent_code = agent.agent_code;

			//Check Agent Deposit
			if (agent.payment_type_code == "DEPOSIT"){
				$scope.agent_allow_to_use_acl = false;
				$scope.agent_allow_to_use_deposit = true;

				httpSrv.post({
					url 	: "agent/deposit",
					data	: $.param({"publish_status":"ALL", "agent_code":agent_code}),
					success : function(response){
						$scope.deposit = response.data.deposit;
					},
					error : function (response){}
				});
			}

			//Check Agent Credit Limit
			if (agent.payment_type_code == "ACL"){
				$scope.agent_allow_to_use_acl = true;
				$scope.agent_allow_to_use_deposit = false;

				
				if (!agent.out_standing_invoice){
					agent.out_standing_invoice = {};
					httpSrv.post({
						url 	: "agent/out_standing_invoice/",
						data	: $.param({"agent_code":agent_code}),
						success : function(response){
							if (response.data.status == "SUCCESS"){
								agent.out_standing_invoice = response.data.out_standing_invoice;
							}
						},
						error : function (response){}
					});
				}
			}
			
		}
	}

	$scope.loadDataTransactionTransport = function(agent_code){
			
		$scope.search = {   "start_date" : "",
							"end_date" : "",
							"agent_code": agent_code,
							"payment_method": "DEPOSIT"}; 		
		
		var _search = $scope.search;
		
		
		httpSrv.post({
			url 	: "transport/transaction",
			data	: $.param({"search":_search}),
			success : function(response){
				
				$scope.DATA.transaction = response.data.transactions;

				// $scope.used_diposit($scope.DATA.transaction);
			
			},
			error : function (response){}
		});

	}

	// $scope.saveDataPayment = function(event){
	// 	//$.showLoading();
	// 	$(event.target).find("button:submit").attr("disabled","disabled");
	// 	$scope.DATA.myPayment.error_msg = [];
		
	// 	httpSrv.post({
	// 		url 	: "payment/create/",
	// 		data	: $.param($scope.DATA.myPayment),
	// 		success : function(response){
	// 			console.log(response.data);
	// 			if (response.data.status == "SUCCESS"){
	// 				$scope.loadDataBookingPayment();
	// 				$("#add-edit-payment").modal("hide");
	// 				toastr.success("Saved...");
	// 			}else{
	// 				$scope.DATA.myPayment.error_msg = response.data.error_desc;
	// 			}
	// 			$(event.target).find("button:submit").removeAttr("disabled");
	// 			//$.hideLoading();
	// 		}
	// 	});
	// }

	$scope.saveDataPayment = function(event){
		//$.showLoading();
		//$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myPayment.error_msg = [];
		
		$scope.error_deposit = false;
		$scope.save_payment = true;

		if ($scope.DATA.current_booking.booking.source_code == 'AGENT') {

			if ($scope.DATA.myPayment.payment_type == 'DEPOSIT') {
				var total = $scope.deposit.current_deposit;
				if (total < $scope.DATA.myPayment.amount) {
					$scope.error_deposit = true;
					$scope.save_payment = false;
				}else{
					$scope.error_deposit = false;
				}
			}
		}
		
		if ($scope.save_payment) {	
			httpSrv.post({
				url 	: "payment/create/",
				data	: $.param($scope.DATA.myPayment),
				success : function(response){
					
					if (response.data.status == "SUCCESS"){
						$scope.loadDataBookingPayment();
						$("#add-edit-payment").modal("hide");
						toastr.success("Saved...");
					}else{
						$scope.DATA.myPayment.error_msg = response.data.error_desc;
					}
				}
			});
		}else{
			toastr.error("Payment Failed");
		}		
	}

	// Gung ari Update

	$scope.send_email_profoma = function(id, is_agent){

		
		httpSrv.post({
			url 	: "invoice/send_email_profoma",
			data 	: $.param({"booking_code": id, "is_agent": is_agent}),
			success : function(response){
				if (response.data.status == "SUCCESS") {
					toastr.success("Email Send...");
				}else{
					toastr.warning("Email Send Failed...");
				}
			},
			error : function (response){}
		});

	}

	$scope.send_email_billing = function(id, is_agent){

		httpSrv.post({
			url 	: "invoice/send_email_billing",
			data 	: $.param({"booking_code": id, "is_agent": is_agent}),
			success : function(response){
				if (response.data.status == "SUCCESS") {
					toastr.success("Email Send...");
				}else{
					toastr.warning("Email Send Failed...");
				}

					
			},
			error : function (response){}
		});

	}

	$scope.loadDataTermsAndConditions = function(){

		$scope.is_cancellation = false;
		$scope.item = 0;

		httpSrv.get({
			url 	: "setting/terms_and_conditions",
			success : function(response){
				
				//$scope.rules = JSON.parse(response.data.advanced_setting.cancelation_rule_crs);
				$scope.convert_int($scope.rules);
			},
			error : function (response){}
		});
	}

	$scope.convert_int = function(data){
		
		var array = [];

		angular.forEach(data, function(row){
			array.push({f_min_activation_days: parseInt(row.f_min_activation_days), f_percentage: parseInt(row.f_percentage)});
		});

		$scope.rules = array;

	}



	$scope.send_email_receipt = function(id, is_agent, payment_code){


		
		httpSrv.post({
			url 	: "invoice/send_email_receipt",
			data 	: $.param({"booking_code": id, "is_agent": is_agent, 'payment_code': payment_code}),
			success : function(response){
				if (response.data.status == "SUCCESS") {
					toastr.success("Email Send...");
				}else{
					toastr.warning("Email Send Failed...");
				}
			},
			error : function (response){}
		});

	}
	
	
	$scope.loadDataBookingTransport = function(){
		$scope.show_loading_DATA_bookings = true;
		httpSrv.post({
			url 	: "transport/booking",
			//data	: $.param({"publish_status":"all"}),
			success : function(response){
				$scope.show_loading_DATA_bookings = false;
				$scope.DATA.bookings = response.data;
			},
			error : function (response){}
		});
	}
	
	
	$scope.loadDataTransactionTransportXXXX = function(page){
		GeneralJS.activateLeftMenu("transaction");
		$scope.show_loading_DATA_bookings = true;
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		
		if (!$scope.search){ 
			var now = new Date(),
				start_date = new Date(),
				end_date = new Date();
			
			start_date.setDate(start_date.getDate() - 30);
			
			$scope.search = { "start_date" : $scope.fn.formatDate(start_date, "yy-mm-dd"),
							  "end_date" : $scope.fn.formatDate(end_date, "yy-mm-dd"),}; 
		}
		
		if (!page) page = 1;
		
		var _search = $scope.search;
			_search.page = page;
		
		httpSrv.post({
			url 	: "transport/transaction",
			data	: $.param({"search":_search}),
			success : function(response){
				$scope.show_loading_DATA_bookings = false;
				$scope.DATA.transaction = response.data;
				
				if ($scope.DATA.transaction.search){
					$scope.DATA.transaction.search.pagination = [];
					for (i=1;i<=$scope.DATA.transaction.search.number_of_pages; i++){
						$scope.DATA.transaction.search.pagination.push(i);
					}
				}
			},
			error : function (response){}
		});
		
		//Load Data Booking Source
		if (!$scope.DATA.booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.DATA.booking_source= response.data; },
			});
		}
		
		//Load Data Booking Status
		if (!$scope.DATA.booking_status){
			httpSrv.post({
				url 	: "setting/booking_status",
				success : function(response){ $scope.DATA.booking_status = response.data; },
			});
		}
	}
	
	$scope.edit_trip = function(trip){
		
		
		$scope.current_edit_trip = trip;
		
		$scope.check_availabilities_data = {};
		$scope.add_trip = {};
		$scope.add_trip.date = fn.formatDate(trip.date, "yy-mm-dd");
		$scope.add_trip.product_code = trip.product.product_code;
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		$("#modal-add-trip .btn-submit-edit-trip").removeAttr("disabled");
		$scope.check_availabilities_show_OK_button = false;
		
		//Product
		httpSrv.post({
			url 	: "activities/product/detail/"+trip.product.product_code,
			success : function(response){
				$scope.add_trip.products = response.data;
			},
			error : function (response){}
		});
		
	}

	$scope.check_availabilities_edit_trip = function(event){
		//$(event.target).find("button:submit").attr("disabled","disabled");
		

		
		var data_edit_trip = angular.copy($scope.add_trip);

		var data = {};
		
		data.date = data_edit_trip.date;
		data.product_code = $scope.current_edit_trip.product.product_code;
		data.qty_1 = $scope.current_edit_trip.qty_1;
		data.qty_2 = $scope.current_edit_trip.qty_2;
		data.qty_3 = $scope.current_edit_trip.qty_3;
		data.qty_4 = $scope.current_edit_trip.qty_4;
		data.qty_5 = $scope.current_edit_trip.qty_5;
		data.booking_source = "AGENT"

		$scope.add_trip.error_desc = [];
		$scope.check_availabilities_data = {};
		$scope.check_availabilities_show_loading = true;
		
		httpSrv.post({
			// url 	: "activities/rates/check_availabilities",
			url 	: "agent_controller_act/check_availabilities",
			data	: $.param(data),
			success : function(response){
				$(event.target).find("button:submit").removeAttr("disabled");
				$scope.check_availabilities_show_loading = false;
				$scope.check_availabilities_show_searching_form = false;
				
				if (response.data.status == "SUCCESS"){
					$scope.check_availabilities_data = response.data;
				}else{
					$scope.add_trip.error_desc = response.data.error_desc;
					//$('html, body').animate({scrollTop:200}, 400);
				}
				
				//fn.pre($scope.check_availabilities_data, "check_availabilities_data");
				//$.hideLoading();
			}
		});
		
	}

	$scope.update_voucher_trip = function (){
		
		$scope.update_voucher_trip_error_desc = {};
		$scope.update_voucher_trip_error_desc.error_desc = [];
		$("#modal-add-trip .btn-submit-edit-trip").attr("disabled","disabled");
		
		var data_submit = {	"voucher_code"		: $scope.current_edit_trip.voucher_code,
							"date" 				: $scope.check_availabilities_data.check.date,
							"rates_code"		: $scope.check_availabilities_data.selected_rates.rates_code,
							"product_code"		: $scope.current_edit_trip.product.product_code,
							"applicable_rates"	: $scope.check_availabilities_data.selected_rates.rates,
							};
		
		if ($scope.add_trip.is_change_participant){
			data_submit.participant = $scope.add_trip.participant;
		}
		console.log(data_submit);
		
		httpSrv.post({
			url 	: "activities/booking/update_voucher",
			data	: $.param(data_submit),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.editDataBookingActivitiesDetail();
					$("#modal-add-trip").modal("hide");
					toastr.success("Updated...");
				}else{
					$scope.update_voucher_trip_error_desc.error_desc = response.data.error_desc;
					$("#modal-add-trip .btn-submit-edit-trip").removeAttr("disabled");
				}
				//$.hideLoading();
			}
		});
		
		//$scope.DATA_r = data_submit;
		//console.log(data_submit);
	}
	
	//Split Voucher
	$scope.split_voucher = function(trip){

		$scope.current_edit_trip = trip;
		
		$scope.check_availabilities_data = {};
		$scope.add_trip = {};
		$scope.add_trip.departure_date = trip.date;
		$scope.add_trip.departure_port_id = trip.departure.port.id;
		$scope.add_trip.arrival_port_id = trip.arrival.port.id;
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		$("#modal-split-voucher .btn-submit-edit-trip").removeAttr("disabled");
		$scope.check_availabilities_show_OK_button = false;
		
		//Load Data Available Port
		if (!$scope.$root.DATA_available_port){
			httpSrv.post({
				url 	: "transport/port/available_port",
				success : function(response){ $scope.$root.DATA_available_port = response.data.ports; },
			});
		}
	}
	$scope.check_availabilities_split_voucher = function(event){
		//$(event.target).find("button:submit").attr("disabled","disabled");
		
		var _qty = 0;
		for (_x in $scope.current_edit_trip.passenger){
			if ($scope.current_edit_trip.passenger[_x].selected){_qty += 1;}
		}
		
		if (_qty == 0){

			alert("Please select at least one passenger.");
			
		}else if(_qty == ($scope.current_edit_trip.qty_1 + $scope.current_edit_trip.qty_2 + $scope.current_edit_trip.qty_3)){
			
			alert("If you select all passenger, please use Edit Trips.");
			
		}else{
			
			var data_edit_trip = angular.copy($scope.add_trip);
	
			var data = {};
			data.departure_date = data_edit_trip.departure_date;
			data.trip_model == "ONEWAY";
			
			if (data_edit_trip.change_trip){
				data.departure_port_id	= data_edit_trip.departure_port_id;
				data.arrival_port_id	= data_edit_trip.arrival_port_id;
			}else{
				data.departure_port_id	= $scope.current_edit_trip.departure.port.id;
				data.arrival_port_id	= $scope.current_edit_trip.arrival.port.id;
			}
			
			data.adult	= _qty; //$scope.current_edit_trip.qty_1;
			data.child	= $scope.current_edit_trip.qty_2;
			data.infant = $scope.current_edit_trip.qty_3;
			
			$scope.add_trip.error_desc = [];
			$scope.check_availabilities_data = {};
			$scope.check_availabilities_show_loading = true;
	
			httpSrv.post({
				url 	: "transport/schedule/check_availabilities",
				data	: $.param(data),
				success : function(response){
					$(event.target).find("button:submit").removeAttr("disabled");
					$scope.check_availabilities_show_loading = false;
					$scope.check_availabilities_show_searching_form = false;
					
					if (response.data.status == "SUCCESS"){
						$scope.check_availabilities_data = response.data;
					}else{
						$scope.add_trip.error_desc = response.data.error_desc;
						//$('html, body').animate({scrollTop:200}, 400);
					}
					
					//fn.pre($scope.check_availabilities_data, "check_availabilities_data");
					//$.hideLoading();
				}
			});
		}
	}
	$scope.update_split_voucher = function (){
		
		$scope.update_voucher_trip_error_desc = {};
		$scope.update_voucher_trip_error_desc.error_desc = [];
		$("#modal-split-voucher .btn-submit-edit-trip").attr("disabled","disabled");
		
		var data_submit = {	"voucher_code"		: $scope.current_edit_trip.voucher_code,
							"date" 				: $scope.check_availabilities_data.departure.check.date,
							"rates_code"		: $scope.check_availabilities_data.departure.selected_trip.rates_code,
							"schedule_code"		: $scope.check_availabilities_data.departure.selected_trip.schedule.schedule_code,
							"applicable_rates"	: $scope.check_availabilities_data.departure.selected_trip.aplicable_rates,
							};
		
		data_submit.passengers = [];
		for (_x in $scope.current_edit_trip.passenger){
			if ($scope.current_edit_trip.passenger[_x].selected){
				data_submit.passengers.push({"passenger_code":$scope.current_edit_trip.passenger[_x].passenger_code});
			}
		}

		httpSrv.post({
			url 	: "transport/booking/update_split_voucher",
			data	: $.param(data_submit),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.editDataBookingTransportDetail();
					$("#modal-split-voucher").modal("hide");
					toastr.success("Updated...");
				}else{
					$scope.update_voucher_trip_error_desc.error_desc = response.data.error_desc;
					$("#modal-split-voucher .btn-submit-edit-trip").removeAttr("disabled");
				}
				//$.hideLoading();
			}
		});
		
		//$scope.DATA_r = data_submit;
		//console.log(data_submit);
	}
	//--
	
	$scope.printReceiptTrans = function(){
		booking_code = $stateParams.booking_code;
		$scope.loadDataBookingTransportDetail(booking_code, function(){
			$scope.$parent.$parent.show_print_button = true;
		});
	}
	$scope.printVoucherTrans = function(){
		booking_code = $stateParams.booking_code;
		voucher_code = $stateParams.voucher_code;
		
		httpSrv.post({
			url 	: "setting/terms_and_conditions",
			success : function(response){ 
				$scope.terms_and_conditions = response.data.terms_and_conditions; 
				$scope.voucher_notes = response.data.notes; 
			},
		});
		
		$scope.loadDataBookingTransportDetail(booking_code, function(booking_detail){
			$scope.$parent.$parent.show_print_button = true;
			
			$scope.DATA.voucher = {};
			
			for (i in booking_detail.booking.detail){
				if (voucher_code == booking_detail.booking.detail[i].voucher_code){
					$scope.DATA.voucher = booking_detail.booking.detail[i];
					break;
				}
			}
			
			console.log($scope.DATA.voucher);
			
		});
	}
	
	$scope.aplicable_rates_for_agent = function(){
		$scope.agent_allow_to_use_acl = false;
		$scope.agent_allow_to_use_deposit = false;
		$scope.DATA.data_rsv.add_payment = '0';
		$scope.agent_payment_use_acl = '0';
		$scope.DATA.data_rsv.payment.payment_type = '';
		
		var selected_trips = $scope.DATA.data_rsv.selected_trips;
			
		if ($scope.DATA.data_rsv.booking_source == "AGENT" && $scope.DATA.data_rsv.agent){
			
			var agent = $scope.DATA.data_rsv.agent;
			
			//Check Agent Credit Limit
			if (agent.payment_method.payment_code == "ACL"){
				$scope.agent_allow_to_use_acl = true;
				
				$scope.DATA.data_rsv.add_payment = '0';
				$scope.agent_payment_use_acl = '1';
				
				if (!agent.out_standing_invoice){
					agent.out_standing_invoice = {};
					httpSrv.post({
						url 	: "agent/out_standing_invoice/",
						data	: $.param({"agent_code":agent.agent_code}),
						success : function(response){
							if (response.data.status == "SUCCESS"){
								agent.out_standing_invoice = response.data.out_standing_invoice;
							}
						},
						error : function (response){}
					});
				}
			}
			//--
			$scope.deposit = [];

			if (agent.payment_method.payment_code == "DEPOSIT"){
				$scope.agent_allow_to_use_deposit = true;
				$scope.agent_allow_to_use_acl = false;

				$scope.DATA.data_rsv.add_payment = '0';
				httpSrv.post({
					url 	: "agent/deposit/",
					data	: $.param({"publish_status":"ALL", "agent_code":agent.agent_code}),
					success : function(response){
						
						$scope.deposit = response.data.deposit;
					},
					error : function (response){}
				});
			}
			
			for (i in selected_trips){
				var _rates = selected_trips[i].trips.aplicable_rates_original;
				//selected_trips[i].trips.aplicable_rates.rates_1 = _rates.rates_1 - (_rates.rates_1 * agent.category.percentage/100);
				//selected_trips[i].trips.aplicable_rates.rates_2 = _rates.rates_2 - (_rates.rates_2 * agent.category.percentage/100);
				//selected_trips[i].trips.aplicable_rates.rates_3 = _rates.rates_3 - (_rates.rates_3 * agent.category.percentage/100);
				
				$scope.calculate_subtotal(selected_trips[i]);
			}
			
		}else{
			
			for (i in selected_trips){
				var _rates = selected_trips[i].trips.aplicable_rates_original;
				selected_trips[i].trips.aplicable_rates = angular.copy(_rates);
				
				$scope.calculate_subtotal(selected_trips[i]);
			}
			
		}
		$scope.calculate_total();
	}
	
	
	
	// $scope.submit_booking = function(event){
		
	// 	$(event.target).find("button:submit").attr("disabled","disabled");
	// 	$scope.DATA.data_rsv.error_desc = [];
		
	// 	var data_rsv = {};
	// 	var ss_data_rsv = angular.copy($scope.DATA.data_rsv);
		
	// 	data_rsv.booking_source = ss_data_rsv.booking_source;
	// 	if (data_rsv.booking_source == "AGENT"){
	// 		data_rsv.agent_code = ss_data_rsv.agent.agent_code;
	// 		if (ss_data_rsv.voucher_reff_number && ss_data_rsv.voucher_reff_number != ''){
	// 			data_rsv.voucher_reff_number = ss_data_rsv.voucher_reff_number;
	// 		}
	// 	}else if (data_rsv.booking_source == "OFFLINE"){
	// 		if (ss_data_rsv.booking_source_sub){
	// 			data_rsv.booking_source_sub = ss_data_rsv.booking_source_sub;
	// 		}
	// 	}
		
	// 	data_rsv.booking_status = ss_data_rsv.booking_status;
	// 	if (ss_data_rsv.booking_status == 'UNDEFINITE'){
	// 		data_rsv.undefinite_valid_until_hour = ss_data_rsv.undefinite_valid_until_hour;
	// 	}
		
	// 	data_rsv.discount 		= {"type":ss_data_rsv.discount_type, "amount":ss_data_rsv.discount_amount};
	// 	data_rsv.tax_service	= {"type":ss_data_rsv.tax_service_type, "amount":ss_data_rsv.tax_service_amount};
	// 	data_rsv.remarks 		= ss_data_rsv.remarks;
	// 	data_rsv.customer 		= ss_data_rsv.customer;
	// 	data_rsv.trips 			= [];
		
	// 	for (i in ss_data_rsv.selected_trips){
	// 		var trip = {};
			
	// 		trip.date 	= ss_data_rsv.selected_trips[i].date;
	// 		trip.adult 	= ss_data_rsv.selected_trips[i].adult;
	// 		trip.child 	= ss_data_rsv.selected_trips[i].child;
	// 		trip.infant	= ss_data_rsv.selected_trips[i].infant;
			
	// 		trip.schedule_code	= ss_data_rsv.selected_trips[i].trips.schedule.schedule_code;
	// 		trip.rates_code		= ss_data_rsv.selected_trips[i].trips.rates_code;
	// 		trip.applicable_rates = ss_data_rsv.selected_trips[i].trips.aplicable_rates;

	// 		if (ss_data_rsv.selected_trips[i].trips.smart_pricing){
	// 			//console.log(ss_data_rsv.selected_trips[i].trips.smart_pricing);
				
	// 			if (ss_data_rsv.selected_trips[i].trips.smart_pricing && ss_data_rsv.selected_trips[i].trips.smart_pricing.type == "SMARTPRICING"){
	// 				trip.smart_pricing_level = ss_data_rsv.selected_trips[i].trips.smart_pricing.level;
	// 			}
				
	// 			//if (ss_data_rsv.selected_trips[i].trips.smart_pricing.level || ss_data_rsv.selected_trips[i].trips.smart_pricing.level === 0){
	// 				//alert(ss_data_rsv.selected_trips[i].trips.smart_pricing.level);
	// 			//	trip.smart_pricing_level = ss_data_rsv.selected_trips[i].trips.smart_pricing.level;
	// 			//}
	// 		}
			
	// 		if (ss_data_rsv.selected_trips[i].trips.is_return && ss_data_rsv.selected_trips[i].trips.is_return == "yes"){
	// 			trip.is_return	= "yes";
	// 		}
			
	// 		//Check Pickup Service
	// 		if (ss_data_rsv.selected_trips[i].pickup 
	// 			&& ss_data_rsv.selected_trips[i].pickup.area 
	// 			&& !ss_data_rsv.selected_trips[i].pickup.area.nopickup){
				
	// 			trip.pickup = ss_data_rsv.selected_trips[i].pickup;
	// 			trip.pickup.area_id = trip.pickup.area.id;
	// 			trip.pickup.price 	= trip.pickup.area.price;
	// 			delete trip.pickup.area;
				
	// 		}
	// 		//Check Dropoff Service
	// 		if (ss_data_rsv.selected_trips[i].dropoff 
	// 			&& ss_data_rsv.selected_trips[i].dropoff.area 
	// 			&& !ss_data_rsv.selected_trips[i].dropoff.area.nopickup){
				
	// 			trip.dropoff = ss_data_rsv.selected_trips[i].dropoff;
	// 			trip.dropoff.area_id = trip.dropoff.area.id;
	// 			trip.dropoff.price 	 = trip.dropoff.area.price;
	// 			delete trip.dropoff.area;
				
	// 		}
			
	// 		//Check Additional Service
	// 		if (ss_data_rsv.selected_trips[i].additional_service){
				
	// 			trip.additional_service = ss_data_rsv.selected_trips[i].additional_service;
				
	// 		}
			
	// 		data_rsv.trips.push(trip);
			
	// 	}//End For
		
	// 	if ($scope.DATA.data_rsv.add_payment == '1' && $scope.DATA.data_rsv.booking_status != 'COMPLIMEN'){
	// 		var payment = $scope.DATA.data_rsv.payment;
	// 		data_rsv.payment = {"amount" 		: payment.payment_amount,
	// 							"payment_type"	: payment.payment_type,
	// 							"account_number": payment.account_number,
	// 							"name_on_card"	: payment.name_on_card,
	// 							"bank_name"		: payment.bank_name,
	// 							"payment_reff_number" : payment.payment_reff_number,
	// 							"description"	: payment.description};
	// 		if (payment.payment_type == "OPENVOUCHER"){
	// 			data_rsv.payment.openvoucher_code = payment.openvoucher_code
	// 		}
	// 	}
		
	// 	//$scope.DATA_RSV = data_rsv;
	// 	//console.log(data_rsv);
	// 	//return;
		
	// 	httpSrv.post({
	// 		url 	: "transport/booking/create",
	// 		data	: $.param(data_rsv),
	// 		success : function(response){
	// 			if (response.data.status == "SUCCESS"){
	// 				window.location = "#/trans_reservation/detail/"+response.data.booking_code+"/passenger";
	// 				toastr.success("Saved...");
	// 			}else{
	// 				$(event.target).find("button:submit").attr("disabled","disabled");
	// 				$scope.DATA.data_rsv.error_desc = response.data.error_desc;
	// 				$(event.target).find("button:submit").removeAttr("disabled");
	// 				$('html, body').animate({scrollTop:200}, 400);
	// 			}
	// 			//$.hideLoading();
	// 		}
	// 	});
		
	// 	$scope.DATA_RSV = data_rsv;
		
	// 	//fn.pre(ss_data_rsv,"ss_data_rsv");
	// 	//fn.pre(data_rsv,"data_rsv");
	// }
	
	
	//Cancel Booking
	$scope.cancelBooking = function(booking){
		$scope.DATA.cancelation = {	'cancel_all_trip' : 0,
									'voucher':{}};
		for (i in $scope.DATA.current_booking.booking.detail){
			var voucher = {};
			voucher.code = $scope.DATA.current_booking.booking.detail[i].voucher_code;
			voucher.is_cancel = 0;
			voucher.fee = $scope.DATA.current_booking.booking.detail[i].subtotal;
			$scope.DATA.cancelation.voucher[voucher.code] = voucher;
		}
		$scope.DATA.cancelation.cancel_all_trip = 1;
		$scope.chkCancelAllBookingClick();
		$scope.cancelationFee(booking.booking_code);

		if ($scope.cancelation_fee[voucher.code]) {
			$scope.DATA.cancelation.voucher[voucher.code].fee = $scope.cancelation_fee[voucher.code]['pay'];
		}
	}

	$scope.cancelationFee = function(booking_code){

		$scope.cancelation_fee = {};

		httpSrv.get({
			url 	: "transport/booking/cancelation/"+booking_code,
			success : function(response){
				
				var booking = response.data;
				
				if (booking.status == "SUCCESS"){
					$scope.cancelation_fee = booking.booking.cancelation;
					
				}else{
					alert("Sorry, data not found..");
					window.location = "#/trans_reservation/detail/"+booking_code;
				}
			},
			error : function (response){}
		});



	}

	$scope.chkCancelAllBookingClick = function(check_cancel){
		if (!check_cancel){
			var is_cancel_all = $scope.DATA.cancelation.cancel_all_trip;
	
			for (code in $scope.DATA.cancelation.voucher){
				$scope.DATA.cancelation.voucher[code].is_cancel = is_cancel_all;
			}
		}else{
			var is_cancel_all = 1;
			for (code in $scope.DATA.cancelation.voucher){
				if(!$scope.DATA.cancelation.voucher[code].is_cancel){
					is_cancel_all = 0;
					break;
				}
			}
			$scope.DATA.cancelation.cancel_all_trip = is_cancel_all;
		}
	}
	$scope.saveCancelation = function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.cancelation.error_desc = [];
		
		var data = {"booking_code" 			: $scope.DATA.current_booking.booking.booking_code,
					"cancelation_reason"	: $scope.DATA.cancelation.cancelation_reason}
		
		if ($scope.DATA.cancelation.cancel_all_trip){
			data.cancel_all_trip = 1;
		}
		data.voucher_code = [];
		for (code in $scope.DATA.cancelation.voucher){
			if ($scope.DATA.cancelation.voucher[code].is_cancel){
				if ($scope.cancelation_fee) {
					$scope.DATA.cancelation.voucher[code].fee = $scope.cancelation_fee[code]['pay'];
				}
				data.voucher_code.push({"code":code, "fee":$scope.DATA.cancelation.voucher[code].fee});
			}
		}
		
		
		//console.log($scope.DATA.cancelation);
		//console.log(data);
		//return
				
		httpSrv.post({
			url 	: "transport/booking/cancel",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					toastr.success("Saved...");
					$scope.loadDataBookingTransportDetail();
					$("#cancel-booking-form").modal("hide");
				}else{
					$(event.target).find("button:submit").attr("disabled","disabled");
					$scope.DATA.cancelation.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
				}
				//$.hideLoading();
			}
		});
	}
	//--
	
	//Void Booking
	$scope.voidBooking = function(booking){
		$scope.DATA.void = {'void_all_trip' : 0,
							'voucher':{}};
		for (i in $scope.DATA.current_booking.booking.detail){
			var voucher = {};
			voucher.code = $scope.DATA.current_booking.booking.detail[i].voucher_code;
			voucher.is_void = 0;
			$scope.DATA.void.voucher[voucher.code] = voucher;
		}
		$scope.DATA.void.void_all_trip = 1;
		$scope.chkVoidAllBookingClick();
	}
	$scope.chkVoidAllBookingClick = function(check_void){
		if (!check_void){
			var is_void_all = $scope.DATA.void.void_all_trip;
	
			for (code in $scope.DATA.void.voucher){
				$scope.DATA.void.voucher[code].is_void = is_void_all;
			}
		}else{
			var is_void_all = 1;
			for (code in $scope.DATA.void.voucher){
				if(!$scope.DATA.void.voucher[code].is_void){
					is_void_all = 0;
					break;
				}
			}
			$scope.DATA.void.void_all_trip = is_void_all;
		}
	}
	$scope.saveVoidBooking = function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.void.error_desc = [];
		
		var data = {"booking_code" 			: $scope.DATA.current_booking.booking.booking_code,
					"void_reason"	: $scope.DATA.void.void_reason}
		
		if ($scope.DATA.void.void_all_trip){
			data.void_all_trip = 1;
		}else{
			data.voucher_code = [];
			for (code in $scope.DATA.void.voucher){
				if ($scope.DATA.void.voucher[code].is_void){
					data.voucher_code.push(code);
				}
			}
		}
				
		httpSrv.post({
			url 	: "transport/booking/void",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					toastr.success("Saved...");
					$scope.loadDataBookingTransportDetail();
					$("#void-booking-form").modal("hide");
				}else{
					$(event.target).find("button:submit").attr("disabled","disabled");
					$scope.DATA.void.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
				}
				//$.hideLoading();
			}
		});
	}
	//--
	
	//Refund
	$scope.refundBooking = function(booking){
		$scope.DATA.myRefund = {	'refund_amount' : 0,
									'description'	: ''};
	}
	$scope.saveRefund= function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myRefund.error_desc = [];
		
		var data = {"booking_code" 			: $scope.DATA.current_booking.booking.booking_code,
					"currency" 				: $scope.DATA.current_booking.booking.currency,
					"refund_amount"			: $scope.DATA.myRefund.refund_amount,
					"description"			: $scope.DATA.myRefund.description}
		
		httpSrv.post({
			url 	: "refund/create",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					toastr.success("Saved...");
					$scope.loadDataBookingTransportDetail();
					$("#refund-booking-form").modal("hide");
				}else{
					$(event.target).find("button:submit").attr("disabled","disabled");
					$scope.DATA.myRefund.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
				}
				//$.hideLoading();
			}
		});
	}
	//--
	
	
	$scope.loadDataBookingPayment = function(){
		booking_code = $stateParams.booking_code;
		
		if ($scope.$parent.DATA.current_booking){
			$scope.DATA.current_booking = $scope.$parent.DATA.current_booking;
		}else{
			$scope.loadDataBookingTransportDetail(booking_code, function(booking_detail){
				$scope.DATA.current_booking = booking_detail;
			});
		}

		$scope.booking_code = booking_code;
		
		httpSrv.get({
			url 	: "payment/reservation/"+booking_code,
			success : function(response){
				$scope.DATA.payment = response.data;
				
				//console.log($scope.DATA.payment);
			},
			error : function (response){}
		});
		
		httpSrv.get({
			url 	: "refund/reservation/"+booking_code,
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.DATA.refund = response.data;
				}
			},
			error : function (response){}
		});
	}

	$scope.printReceiptTransPayment = function(){
		
		payment_code = $stateParams.payment_code;

		$scope.loadDataBookingPaymentDetail(payment_code, function(){
			$scope.$parent.$parent.show_print_button = true;
		});
	}

	$scope.loadDataBookingPaymentDetail = function(payment_code, after_load_handler_function){

		if (!payment_code){ payment_code = $stateParams.payment_code; }
		
		httpSrv.get({
			url 	: "transport/booking/detail_payment/"+payment_code,
			success : function(response){
				
				var booking = response.data;
				//console.log(booking);
				if (booking.status == "SUCCESS"){
					$scope.DATA.current_booking = booking;
					
					if (fn.isFn(after_load_handler_function)) {
						after_load_handler_function(booking);
					}
				}else{
					alert("Sorry, data not found..");
					window.location = "#/trans_reservation";
				}
			},
			error : function (response){}
		});
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	$scope.changePaymentType = function(){
		
		$scope.DATA.payment_is_cc = false;
		$scope.DATA.payment_is_atm = false;
		
		if ($scope.DATA.myPayment.payment_type == 'OPENVOUCHER'){
			$scope.DATA.myPayment.openvoucher_code = [];
			var total_person = 0;
			
			for (i in $scope.DATA.current_booking.booking.detail){
				var detail = $scope.DATA.current_booking.booking.detail[i];

				total_person += detail.qty_1;
				total_person += detail.qty_2;
				total_person += detail.qty_3;
			}
			
			for (j=0; j<total_person; j++){
				$scope.DATA.myPayment.openvoucher_code.push({"code":""});
			}
			
			//console.log($scope.DATA.myPayment.openvoucher_code);
		}else{
			//console.log($scope.$root.DATA_payment_method);
			
			for (index in $scope.$root.DATA_payment_method){
				var payment_method = $scope.$root.DATA_payment_method[index];
				if (payment_method.code == $scope.DATA.myPayment.payment_type){
					if (payment_method.type == 'CC'){
						$scope.DATA.payment_is_cc = true;
						$scope.DATA.payment_is_atm = false;
					}else if (payment_method.type == 'TRANSFER'){
						$scope.DATA.payment_is_cc = false;
						$scope.DATA.payment_is_atm = true;
					}else{
						$scope.DATA.payment_is_cc = false;
						$scope.DATA.payment_is_atm = false;
					}
					break;
				}
			}
			
			//$scope.$root.DATA_payment_method.forEach(function(payment_method, index){
			//	if (payment_method.code == $scope.DATA.data_rsv.payment.payment_type){
					//console.log(item);
					//break;
			//	}
			//});
		}
	}
	$scope.changePaymentTypeInNewBookingForm = function(){
		//console.log($scope.DATA);
		
		$scope.DATA.payment_is_cc = false;
		$scope.DATA.payment_is_atm = false;
		//console.log($scope.DATA.data_rsv.payment.payment_type);
		
		if ($scope.DATA.data_rsv.payment.payment_type == 'OPENVOUCHER'){
			$scope.DATA.data_rsv.payment.openvoucher_code = [];
			var total_person = 0;
			
			for (i in $scope.DATA.data_rsv.selected_trips){
				var detail = $scope.DATA.data_rsv.selected_trips[i];

				total_person += detail.adult;
				total_person += detail.child;
				total_person += detail.infant;
				
			}
			
			for (j=0; j<total_person; j++){
				$scope.DATA.data_rsv.payment.openvoucher_code.push({"code":""});
			}
			
			//console.log($scope.DATA.data_rsv.payment.openvoucher_code);
		}else{
			//console.log($scope.$root.DATA_payment_method);
			
			for (index in $scope.$root.DATA_payment_method){
				var payment_method = $scope.$root.DATA_payment_method[index];
				if (payment_method.code == $scope.DATA.data_rsv.payment.payment_type){
					if (payment_method.type == 'CC'){
						$scope.DATA.payment_is_cc = true;
						$scope.DATA.payment_is_atm = false;
					}else if (payment_method.type == 'TRANSFER'){
						$scope.DATA.payment_is_cc = false;
						$scope.DATA.payment_is_atm = true;
					}else{
						$scope.DATA.payment_is_cc = false;
						$scope.DATA.payment_is_atm = false;
					}
					break;
				}
			}
			
			//$scope.$root.DATA_payment_method.forEach(function(payment_method, index){
			//	if (payment_method.code == $scope.DATA.data_rsv.payment.payment_type){
					//console.log(item);
					//break;
			//	}
			//});
		}
	}
	$scope.checkValidateOpenVoucherCode = function (ov_code){
		delete ov_code.loading; delete ov_code.valid; delete ov_code.invalid;
		
		if (ov_code.code != ''){
			ov_code.loading = true;
			
			httpSrv.post({
				url 	: "transport/open_voucher/check_openticket_code",
				data	: $.param({"openticket_code":ov_code.code}),
				success : function(response){
					var result = response.data;
					delete ov_code.loading;
					
					if (result.status == "ERROR") ov_code.invalid = true;
					else if (result.used) ov_code.invalid = true;
					else if (result.notyet_used) ov_code.valid = true;
				}
			});
		}
	}
	
	
	$scope.submitDeletePayment = function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.myPaymentDetail.error_msg = [];
		
		var data = {"payment_code" 	: $scope.myPaymentDetail.payment_code,
					"delete_remarks": $scope.myPaymentDetail.delete_remarks,
					"booking_code"  : $scope.DATA.current_booking.booking.booking_code};
		
		httpSrv.post({
			url 	: "payment/delete/",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataBookingPayment();
					$("#payment-detail").modal("hide");
					toastr.success("Deleted...");
				}else{
					$scope.myPaymentDetail.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
			}
		});
	}
	$scope.paymentDetail = function (current_payment, delete_payment){
		$scope.myPaymentDetail = angular.copy(current_payment);
		$scope.myPaymentDetail.amount = Math.abs($scope.myPaymentDetail.amount);
		if (delete_payment == true){
			$scope.myPaymentDetail.delete_payment = true;
			$scope.myPaymentDetail.sure_to_delete = '0';
		}else{
			delete $scope.myPaymentDetail.delete_payment;
		}
	}
	
	$scope.addEditPassenger = function (current_passenger, voucher){
		
		$scope.DATA.myPassenger = angular.copy(current_passenger);	
		
		if (current_passenger.is_data_updated != "1"){
			if (current_passenger.passenger_number == 0){
				var cus = angular.copy($scope.DATA.current_booking.booking.customer);
				$scope.DATA.myPassenger.first_name	= cus.first_name;
				$scope.DATA.myPassenger.last_name 	= cus.last_name;
				$scope.DATA.myPassenger.country_code= cus.country_code;
				$scope.DATA.myPassenger.country_name= cus.country_name;
				$scope.DATA.myPassenger.email 		= cus.email;
				$scope.DATA.myPassenger.phone 		= cus.phone;
			}else{
				$scope.DATA.myPassenger.first_name	= '';
				$scope.DATA.myPassenger.last_name 	= '';
			}
			
		}
		
		$scope.DATA.myPassenger.voucher = voucher;
		
		if (!$scope.DATA.country_list){
			httpSrv.get({
				url 	: "general/country_list",
				success : function(response){
					$scope.DATA.country_list = response.data;
				},
				error : function (response){}
			});
		}
	}
	$scope.saveDataPassenger = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myPassenger.error_msg = [];
		
		var data = angular.copy($scope.DATA.myPassenger);
		delete data.voucher;
		
		data.booking_code = $scope.DATA.current_booking.booking.booking_code;
		data.voucher_code = $scope.DATA.myPassenger.voucher.voucher_code;
		
		httpSrv.post({
			url 	: "agent_controller_act/passanger_create/",
			data	: $.param(data),
			success : function(response){
				
				if (response.data.status == "SUCCESS"){
					$scope.loadDataBookingTransportDetail();
					$("#add-edit-passenger").modal("hide");
					toastr.success("Saved...");
				}else{
					$scope.DATA.myPassenger.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	$scope.copyPassenger = function (index_from, index_to){
		if (confirm('Click OK to continue...')){
			var booking_detail 	= $scope.DATA.current_booking.booking.detail;
			var passenger_from 	= angular.copy(booking_detail[index_from].passenger);
			var passenger_to 	= angular.copy(booking_detail[index_to].passenger);
			
			for (i in passenger_to){
				var pss_code = passenger_to[i].passenger_code;
				passenger_to[i] = angular.copy(passenger_from[i]);
				passenger_to[i].passenger_code = pss_code;
			}
			
			var data = {};
			data.booking_code 	= $scope.DATA.current_booking.booking.booking_code;
			data.voucher_code 	= booking_detail[index_to].voucher_code;
			data.passanger		= passenger_to;
			
			httpSrv.post({
				url 	: "transport/booking/passanger_create_bulk/",
				data	: $.param(data),
				success : function(response){
					
					if (response.data.status == "SUCCESS"){
						$scope.loadDataBookingTransportDetail();
						$("#add-edit-passenger").modal("hide");
						toastr.success("Saved...");
					}else{
						alert(response.data.error_desc);
						//$scope.DATA.myPassenger.error_msg = response.data.error_desc;
					}
					$(event.target).find("button:submit").removeAttr("disabled");
					//$.hideLoading();
				}
			});
			
			//$scope.DATA_R = data;
			
			//console.log(data);
			
			//$scope.DATA.current_booking.booking.detail[index_to].passenger = passenger_from;
			
			//console.log(passenger_from);
			//console.log(passenger_to);
		}
	}
	
	$scope.bulkEditPassenger = function (voucher){
		voucher.passenger_edit = angular.copy(voucher.passenger);
		if (!$scope.DATA.country_list){
			httpSrv.get({
				url 	: "general/country_list",
				success : function(response){
					$scope.DATA.country_list = response.data;
				},
				error : function (response){}
			});
		}
	}
	$scope.cancelBulkEditPassenger = function (voucher){
		delete voucher.passenger_edit;
	}
	$scope.saveBulkEditPassenger = function (voucher, event){
		
		$(event.target).find("button:submit").attr("disabled","disabled");
		var booking_detail 	= $scope.DATA.current_booking.booking.detail;
		
		var data = {};
		data.booking_code 	= $scope.DATA.current_booking.booking.booking_code;
		data.voucher_code 	= voucher.voucher_code;
		data.passanger		= voucher.passenger_edit;
		
		httpSrv.post({
			url 	: "transport/booking/passanger_create_bulk/",
			data	: $.param(data),
			success : function(response){
				
				if (response.data.status == "SUCCESS"){
					$scope.loadDataBookingTransportDetail();
					delete voucher.passenger_edit;
					toastr.success("Saved...");
				}else{
					alert(response.data.error_desc);
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});

	}
	
	$scope.checkInVoucherTrans = function(){
		GeneralJS.activateLeftMenu("checkin");	
	}
	$scope.getVoucherInformation = function(){
		var voucher_code = $scope.check_voucher_code;
		$scope.DATA.voucher = {};
		$scope.show_getVoucherInformation = true;
		
		httpSrv.post({
			url 	: "transport/booking/voucher/",
			data	: $.param({"voucher_code":voucher_code}),
			success : function(response){
				var voucher = response.data;

				if (voucher.status == "SUCCESS"){
					$scope.DATA.voucher = voucher;
					if ($scope.DATA.voucher.voucher.booking.balance > 0){
						$scope.DATA.voucher.allow_to_checkin = false;
						if($scope.DATA.voucher.voucher.booking.agent){
							if ($scope.DATA.voucher.voucher.booking.agent.payment_type_code="ACL"){
								$scope.DATA.voucher.allow_to_checkin = true;
							}
						}
					}else if ($scope.DATA.voucher.voucher.booking.status_code == 'CANCEL' || $scope.DATA.voucher.voucher.voucher.booking_detail_status_code == 'CANCEL'){
						$scope.DATA.voucher.allow_to_checkin = false;
						$scope.DATA.voucher.is_canceled = true;
					}else{
						$scope.DATA.voucher.allow_to_checkin = true;
						
						for (i in $scope.DATA.voucher.voucher.voucher.passenger){
							if ($scope.DATA.voucher.voucher.voucher.passenger[i].checkin_status == '1'){
								$scope.DATA.voucher.voucher.voucher.passenger[i].already_checkin = '1';
							}
						}
						
					}
					//console.log($scope.DATA.voucher.voucher.booking.balance);
					$scope.is_checkedin_proccessed = false;
				}else{
					alert("Sorry, voucher not found..");
					$("#check_voucher_code").focus();
					$("#check_voucher_code").val("");
				}
				$scope.show_getVoucherInformation = false;
			},
			error : function (response){}
		});
	}
	$scope.getClickVoucherInformation = function(voucher_code){
		$scope.check_voucher_code = voucher_code;
		$("#checkin-popup-info").modal("hide");
		$scope.getVoucherInformation();
	}
	$scope.selectToCheckIn = function(passenger){
		if (!passenger.already_checkin){
			if (passenger.checkin_status=='1'){
				passenger.checkin_status = '0';
			}else{
				passenger.checkin_status = '1';
			}
		}
	}
	$scope.checkUncheckParticipant = function(status, participant){
		for (i in participant){
			if (!participant[i].already_checkin){
				if (status == 1){
					participant[i].checkin_status = 1;
				}else{
					participant[i].checkin_status = 0;
				}
			}
		}
	}
	$scope.checkInParticipantNow = function(){
		var participants =  $scope.DATA.voucher.voucher.voucher.passenger;
		var data = {"voucher_code" : $scope.DATA.voucher.voucher.voucher.voucher_code,
					"participant"  : []};
		
		for (i in participants){
			data.participant.push({	"participant_code"	:participants[i].passenger_code,
									"pass_code"			:participants[i].pass_code,
									"checkin_status"	:participants[i].checkin_status});
		}
		$scope.is_checkedin_proccessed = true;
		httpSrv.post({
			url 	: "transport/booking/checkin_participant",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					alert ("Chekin Success...");
					$('html, body').animate({scrollTop:0}, 400);
					$scope.DATA.voucher = {};
					$scope.check_voucher_code = "";
					$("#check_voucher_code").focus();
				}
			},
			error : function (response){}
		});
		
	}
	
	$scope.arrivalDepartureTime = function(){
		
		if ($scope.dept && $scope.dept.date && $scope.dept.boat && $scope.dept.departure && $scope.dept.destination){
			$scope.DATA.time = [];
			
			var data = {"type" 				: $scope.dept.type,
						"date" 				: $scope.dept.date,
						"boat_id" 			: $scope.dept.boat.id,
						"departure_port_id" : $scope.dept.departure.id,
						"arrival_port_id" 	: $scope.dept.destination.id};
			
			httpSrv.post({
				url 	: "transport/schedule/arrival_departure_time",
				data	: $.param(data),
				success : function(response){
					if (response.data.status == "SUCCESS"){
						$scope.DATA.time = response.data.arrival_departure_time;
						//if ($scope.DATA.time[0]){
						//	$scope.dept.time = $scope.DATA.time[0];
						//}
					}
				}
			});
		}
	}
	
	$scope.arrivalDepartureTransPrint = function(){
		
		var data = {"type" 				: $stateParams.search_type,
					"date" 				: $stateParams.search_date,
					"departure_port_id" : $stateParams.search_departure_port_id,
					"arrival_port_id" 	: $stateParams.search_destination_port_id,
					"boat_id"		: $stateParams.search_schedule_code};
		
		/*httpSrv.post({
			url 	: "transport/port/available_port",
			success : function(response){
				$scope.DATA.ports = response.data;
				
				//Generate Data Ports By Code
				var ports_by_code = {};
				for (i in $scope.DATA.ports.ports){
					var port = $scope.DATA.ports.ports[i];
					ports_by_code[port.port_code] = port;
				}
				$scope.DATA.ports_by_code = ports_by_code;
			}
		});*/
		
		$scope.arrivalDepartureTransSearch(data, function(arrival_departure){$scope.$parent.$parent.show_print_button = true;});
	}
	$scope.selectArrivalDepartureType = function(){
		
		$scope.DATA.ports_arrival	= angular.copy($scope.DATA.ports);
		$scope.DATA.ports_departure	= angular.copy($scope.DATA.ports);
		
		if ($scope.dept.type == 'arrival'){
			$scope.DATA.ports_departure.ports.splice($scope.DATA.ports_departure.ports.length-1, 1);
		}else if ($scope.dept.type == 'departure'){
			$scope.DATA.ports_arrival.ports.splice($scope.DATA.ports_arrival.ports.length-1, 1);
		}
	}

	
	$scope.pickupDropoffTrans = function(){
		GeneralJS.activateLeftMenu("arrival_departure");	
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		
		$scope.dept = {	"type" : "pickup",
						"date" : fn.formatDate(new Date(), "yy-mm-dd")};
		$scope.pickupDropofTransSearch();
	}
	$scope.pickupDropofTransSearch = function(data, after_load_handler_function){
		$scope.show_arrivalDepartureTransSearch_loading = true;
		
		if (!data){
			var data = {"type" 				: $scope.dept.type,
						"date" 				: $scope.dept.date};
	
			//data = {"type":"departure","date":"2017-06-30","schedule_code":"TDQ0507","departure_port_id":"10","arrival_port_id":"3","time":"09:30"};
			//$scope.DATA_R = data;
		}
		httpSrv.post({
				url 	: "agent_controller_act/pickup_dropoff_information",
				data	: $.param(data),
				success : function(response){
					$scope.show_arrivalDepartureTransSearch_loading = false;
					$scope.search = response.data.search;
					
					if (response.data.status == "SUCCESS"){
						$scope.pickup_dropoff = response.data.pickup_dropoff;
						
						if (fn.isFn(after_load_handler_function)) {
							after_load_handler_function(response.data);
						}
						
					}else{
						$scope.pickup_dropoff = [];
					}
				}
			});
		
		
	}
	$scope.pickupDropofTransPrint = function(){
		var data = {"type":$stateParams.search_type,
					"date":$stateParams.search_date};
		
		$scope.pickupDropofTransSearch(data, function(pickup_dropoff){$scope.$parent.$parent.show_print_button = true;});
	}
	
	$scope.departureTransGetScheduleDetail = function(){
		if (!$scope.DATA.schedules_detail){
			$scope.DATA.schedules_detail = {};
		}
		
		if (!$scope.DATA.schedules_detail[$scope.dept.schedule.schedule_code]){
			httpSrv.get({
				url 	: "transport/schedule/detail/"+$scope.dept.schedule.schedule_code,
				success : function(response_detail){
					$scope.DATA.schedules_detail[$scope.dept.schedule.schedule_code] = response_detail.data;
				},
				error : function (response){}
			});
		}
	}

	
	
	// update remarks //03012018

	// update current customer
	$scope.update_email_customer = function (event, email){
		
		$(event.target).find("button:submit").attr("disabled","disabled");
		
		var data_submit = $scope.DATA.current_booking.booking.customer.email;
		var booking_code = $scope.DATA.current_booking.booking.booking_code;
		
		$scope.error_msg = [];
		
		httpSrv.post({
			url 	: "transport/booking/update_email_customer",
			data	: $.param({'email':data_submit, 'booking_code': booking_code}),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					// $scope.loadDataBookingTransportDetail();
					$("#payment-detail").modal("hide");
					toastr.success("Updated...");
					if (email == 'invoice') {
						$scope.send_email_profoma($scope.DATA.current_booking.booking.booking_code, '0');
					}else if (email == 'billing') {
						$scope.send_email_billing($scope.DATA.current_booking.booking.booking_code, '0');	
					}else if(email == 'receipt'){
						$scope.send_email_receipt($scope.DATA.current_booking.booking.booking_code, '0', $scope.DATA.current_booking.booking.detail.payment_code)
					}
					
				}else{
					$scope.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
			}
		});
	}
});

function activate_sub_menu_agent_detail(class_menu){
	$(".reservation-detail ul.nav.sub-nav li").removeClass("active");
	$(".reservation-detail ul.nav.sub-nav li."+class_menu).addClass("active");
}

angular.module('app').controller('report_revenue_controller', function($scope, $http, httpSrv, $stateParams, $interval, fn, $filter){
	GeneralJS.activateLeftMenu("reports");	
	
	$scope.DATA = {};
	$scope.fn = fn;
	$scope.search = [];

	$scope.printReportRevenue =  function()
	{
		$scope.show_loading_DATA = true;
		 var month = $stateParams.month;
		 var year = $stateParams.year;
		 var date = $stateParams.date;
		 var view = $stateParams.view;
		 var booking_source = $stateParams.booking_source;
		 var start_date = $stateParams.start_date;

		 if (view == 'date') {
			$scope.date_data = true;
			$scope.month_data = false;
			
		}else{
			$scope.date_data = false;
			$scope.month_data = true;
			
		}
		
		var _search = {	
						"start_date" 		: (start_date?start_date:""),
						"group_by_trip"		: "1",
						"booking_source"	: (booking_source?booking_source:""),
						"month" 			: month,
						"year"				: year,
						"date" 				: date,
						"view" 				: view
		};

		httpSrv.post({
			url 	: "transport/report/revenue",
			data	: $.param({"search":_search}),
			success : function(response){
				$scope.show_loading_DATA = false;
				if (response.data.status == "SUCCESS") {
					$scope.$parent.$parent.show_print_button = true;
					$scope.month = month;
					$scope.date = date;
					$scope.year = year;
					$scope.start_date = date + '-' + month + '-' + year;

					$scope.DATA = response.data;
					$scope.filter_date = start_date;
					$scope.total_data(response.data.sales);
					$scope.show_loading_DATA = false;
					if ($scope.month_data) {
						$scope.data_summary = response.data.summary;
					}
					
				}else{
					$scope.show_error = true;
					$scope.date_data = false;
				}
				
				
			},
			error : function (response){}
			})
	}
	
	$scope.loadProduct = function(){

		httpSrv.post({
			url : "activities/product",
			success : function(response){
				$scope.DATA.products = response.data.products;
			},	
			error : function(err){}
		});

	}
	
	$scope.graph = function($object,id_name) {
		
		var data = new google.visualization.DataTable();
		angular.forEach($object.cols, function(arr){
				
			data.addColumn(arr.type, arr.label);
		});

		angular.forEach($object.rows, function(arr){
			if (id_name == 'revenueLine') {
				data.addRows([
					[arr.c[0].v, Number(arr.c[1].v)],
				]);;
			}else{
				data.addRows([
					[arr.c[0].v, Number(arr.c[1].v), Number(arr.c[2].v), Number(arr.c[3].v)],
				]);;
			}
		});
		
		var maxNumber = 10;
		for(var i=0; i<$object.rows.length; i++){
			
			for(var j=1; j<$object.rows[i]['c'].length; j++){
				if($object.rows[i]['c'][j]['v'] > maxNumber){
					maxNumber = $object.rows[i]['c'][j]['v'];
				}
			}
		}
		var options = {
			vAxis: {format: 'decimal', minValue: 0, maxValue: maxNumber, gridlines: {count: -1}, textStyle: {fontSize: 10}},
			hAxis: {textStyle: {fontSize: 8}},
	        'title': 'Booking',
	        legend: { position: 'bottom' },
	        fontSize: 10,
	       	responsive: true,
	       	chartArea: {width: '80%', heigth: '100%'},
		};
		
		var chart = new google.visualization.LineChart(document.getElementById(id_name));
	
		
		chart.draw(data, options);
	}

	$scope.loadAgentDate = function(date){


		$scope.search = [];
		$scope.agent = [];
		//$scope.show_loading_DATA = true;
		date = $filter('date')(date, 'yyyy-M-d');
		var date2 = date.split("-", 3);
				
		$scope.search.date = date2[2];
		$scope.search.year = date2[0];
		$scope.search.month = date2[1];

		$scope.search.view = "month";

		
		// $scope.date_data_agent = false;

		httpSrv.post({
			url 	: "transport/report/Cancell_agent_date",
			data	: $.param({"date":date}),
			success : function(response){

			
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.agent = response.data;
					// $scope.show_graph = false;
					// $scope.date_data_agent = true;
					$scope.total_agentt(response.data);
					
				}else{
					$scope.show_loading_DATA = false;
					$scope.show_error = true;
				}
				
			},
			error : function (response){}
			})


	}

	$scope.total_agentt = function(data){

		$scope.total_agent = 0;
		var total = 0;
	    angular.forEach(data.sales, function(row){
	    	total += parseInt(row.total); 
	   	});
	   
	    $scope.total_agent = total;
	
	}

	$scope.openPopUpWindowVoucher = function (base_url, booking_code, voucher_code){
		var myWindow = window.open(base_url+booking_code+'/'+voucher_code, 'VoucherWindow', 'width=800,height=600');
	}

	$scope.loadReportRevenue = function()
	{
		google.charts.load('current', {'packages':['corechart']});
		$scope.show_loading_DATA = true;
		$scope.search.year = $filter('date')(new Date(), 'yyyy');
		$scope.search.month = $filter('date')(new Date(), 'M');
		$scope.search.date = $filter('date')(new Date(), 'd');
		$scope.search.product = '';
		$scope.today = $filter('date')(new Date(), 'yyyy-M-d');

		$scope.search.view = "month";
		$scope.show_graph = true;
		$scope.date_data = false;
		$scope.month_data = true;
		$scope.show_error = false;

		$scope.loadReportRevenueTrans();
	}

	$scope.loadReportRevenueTrans = function(page){

		$scope.show_loading_DATA = true;
		$scope.show_error = false;
		if (!$scope.search) $scope.search = {}
		
		if (!$scope.$root.DATA_booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.$root.DATA_booking_source = response.data.booking_source;},
			});
		}
		
		if (!page) page = 1;
		var _search = {	"page" 				: page,
						"start_date" 		: ($scope.search.date?$scope.search.date:""),
						"group_by_trip"		: "1",
						"booking_source"	: ($scope.search.booking_source?$scope.search.booking_source:""),
						"month" 			: $scope.search.month,
						"year"				: $scope.search.year,
						"date" 				: $scope.search.date,
						"view" 				: $scope.search.view,
						"product" 			: $scope.search.product,
		};

		if ($scope.search.booking_source) _search.booking_source = $scope.search.booking_source;
		if ($scope.search.view == 'date') {
			$scope.date_data = true;
			$scope.month_data = false;
			$scope.show_graph = false;
		}else{
			$scope.date_data = false;
			$scope.month_data = true;
			$scope.show_graph = true;
		}
		httpSrv.post({
			url 	: "transport/report/revenue",
			data	: $.param({"search":_search}),
			success : function(response){
				$scope.DATA = response.data;
				
				$scope.show_loading_DATA = false;
				if (response.data.status == 'SUCCESS') {
					
					if (response.data.search){
						
						
						
						$scope.filter_date = $scope.DATA.search.start_date;
						$scope.date_data = true;
						$scope.month_data = false;
						$scope.show_graph = false;
						
					}else{
						$scope.graph(response.data.graph, 'revenueLine');
						$scope.data_summary = response.data.summary;
						$scope.total_data(response.data.sales);
						$scope.DATA.sales = response.data.sales;
					

					}
				}else{
					
					$scope.show_error = true;
					$scope.month_data = false;
					$scope.date_data = false;
					$scope.show_graph = false;
				}
				
			},
			error : function (response){}
		});
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
	}

	$scope.loadReportSalesData = function(date){
		date = $filter('date')(date, 'yyyy-M-d');
		var date = date.split("-", 3);

		$scope.search.year = date[0];
		$scope.search.month = date[1];
		$scope.search.date = date[2];
		$scope.search.view = "date";

		$scope.show_graph = false;
		
		$scope.month_data = false;
		$scope.show_loading_DATA = true;
		$scope.show_error = false;
		

		var _search = {	
						"start_date" 		: ($scope.search.date?$scope.search.date:""),
						"group_by_trip"		: "1",
						"booking_source"	: ($scope.search.booking_source?$scope.search.booking_source:""),
						"month" 			: $scope.search.month,
						"year"				: $scope.search.year,
						"date" 				: $scope.search.date,
						"view" 				: $scope.search.view
		};

		httpSrv.post({
			url 	: "transport/report/revenue",
			data	: $.param({"search":_search}),
			success : function(response){

				$scope.show_loading_DATA = false;
				if (response.data.status == "SUCCESS") {

					$scope.date_data = true;
					$scope.DATA = response.data;
					$scope.filter_date = $scope.DATA.search.start_date;
					
				}else{
					$scope.show_error = true;
					$scope.date_data = false;
				}
				
			},
			error : function (response){}
		});

	}

	

	$scope.total_data = function(data){

		$scope.total = 0;
		$scope.currency = '';
		var total = 0;
	    angular.forEach(data, function(row){
	    	total += parseInt(row.sources.total); 
	    	if ($scope.currency == '' && $scope.currency == null) {
	    		$scope.currency = row.sources.currency;
	    	}
	   	});
	   
	    $scope.total = total;
	    
	}


	$scope.loadReportCancell = function()
	{
		google.charts.load('current', {'packages':['corechart']});
		// $scope.show_loading_DATA = false;
		$scope.search.year = $filter('date')(new Date(), 'yyyy');
		$scope.search.month = $filter('date')(new Date(), 'M');
		$scope.search.date = $filter('date')(new Date(), 'd');
		$scope.search.view = "month";
		$scope.search.product = "";
		$scope.show_graph = false;
		$scope.date_data = false;
		// $scope.show_error = false;
		$scope.loadReportCancellData();
	}

	$scope.loadCancellDate = function(date)
	{

		$scope.date_data = false;
		$scope.search = [];
		$scope.report = [];
		$scope.show_loading_DATA = true;
		date = $filter('date')(date, 'yyyy-M-d');
		var date2 = date.split("-", 3);
				
		$scope.search.date = date2[2];
		$scope.search.year = date2[0];
		$scope.search.month = date2[1];

		$scope.search.view = "date";

		$scope.date_data_agent = false;
	
		httpSrv.post({
			url 	: "transport/report/cancell_date",
			data	: $.param({"date":date}),
			success : function(response){

				$scope.show_graph = false;
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.report = response.data.sales;
					$scope.getTotalDate(response.data);
					$scope.date_data = true;
					// $scope.total_guests(response.data);
					//$scope.total_books(response.data);
					
				}else{
					$scope.show_loading_DATA = false;
					$scope.date_data = false;
					$scope.show_error = true;
				}
				
			},
			error : function (response){}
			})
	}

	$scope.loadReportCancellData = function()
	{
		
		if (!$scope.search.month) $scope.search.month = $scope.month.toString();
		if (!$scope.search.year) $scope.search.year = $scope.year.toString();
		if (!$scope.search.date) $scope.search.date = $scope.date.toString();
		$scope.show_loading_DATA = true;
		$scope.show_graph = false;

		var _search = {	
						"month" 	: $scope.search.month,
						"year"		: $scope.search.year,
						"date" 		: $scope.search.date,
						"view" 		: $scope.search.view,
						"product"	: $scope.search.product,
					  };
		if ($scope.search.view != 'date') {
			$scope.show_graph = true;
			$scope.date_data = false;
			$scope.date_data_agent = false;
		}
		else{
			$scope.show_graph = false;
			$scope.date_data = true;
			$scope.date_data_agent = false;
		}
		httpSrv.post({
			url 	: "transport/report/cancell",
			data	: $.param(_search),
			success : function(response){
				
				$scope.show_loading_DATA = false;
				if (response.data.status == "SUCCESS") {
					
					$scope.report = response.data.sales;
					$scope.getTotal(response.data.sales);
					$scope.getTotalAgent(response.data.agent);
					
					$scope.show_error = false;
					
					
					if ($scope.search.view == 'date') {
						
						$scope.getTotalDate(response.data);
						$scope.date_data_agent = false;
						$scope.date_data = true;
						$scope.show_graph = false;
					}else{
						
						$scope.getTotalSales(response.data.sales);
						$scope.total_books(response.data.sales);
						$scope.total_guests(response.data.sales);
						$scope.graph(response.data.graph, 'cancelLine');
						$scope.show_graph = true;
					}
					
				}else{
					$scope.report = [];
					$scope.show_loading_DATA = false;
					$scope.show_graph = false;
					$scope.show_error = true;
					
				}
				
			},
			error : function (response){}
			})
	}

	$scope.loadReportAgent = function()
	{
		start_date = new Date(),
		end_date = new Date();
		start_date.setDate(start_date.getDate() - 30);

		google.charts.load('current', {'packages':['corechart']});
		$scope.show_loading_DATA = true;
		$scope.search.year = $filter('date')(new Date(), 'yyyy');
		$scope.search.month = $filter('date')(new Date(), 'M');
		$scope.search.start_date = $filter('date')(start_date, 'yyyy-MM-d');
		$scope.search.end_date = $filter('date')(end_date, 'yyyy-MM-d');
		$scope.search.view = "date";
		$scope.show_graph = false;
		$scope.date_data = true;
		$scope.show_error = false;
		
		$scope.loadReportAgentData();
	}

	$scope.graph_agent_null = function(){

	    var data = new google.visualization.DataTable();
		
		data.addColumn('string', 'Booking');
		data.addColumn('number', 'Booking');
		data.addColumn('number', 'Guest');
		data.addRows([
			['Agent', 0, 0],
		]);
		
		
		var options = {
			hAxis: {format: 'decimal', minValue: 0, maxValue: 10, gridlines: {count: -1}, textStyle: {fontSize: 10}},
			'title': 'Most Active Agencies',
	        legend: { position: 'bottom' },
	        fontSize: 10,
	        responsive: true,
	        chartArea: {width: '45%', heigth: '100%'},
		};
		
		var chart = new google.visualization.BarChart(document.getElementById('agentLine'));
		
		chart.draw(data, options);
	}

	$scope.loadAgentMonth = function(month){
		
		$scope.search = [];
		$scope.agent = [];
		//$scope.show_loading_DATA = true;
		
		var date3 = month.split("-", 2);	
		month = date3[1];
		year = date3[0];
		$scope.search.year = date3[0];
		$scope.search.month = date3[1];
		$scope.search.date =  $filter('date')(new Date(), 'MM');
		$scope.search.view = "year";

		
		// $scope.date_data_agent = false;

		httpSrv.post({
			url 	: "transport/report/Cancell_agent_month",
			data	: $.param({"month":month,"year":year}),
			success : function(response){

				
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.agent = response.data;
					// $scope.show_graph = false;
					// $scope.date_data_agent = true;
					$scope.total_agentt(response.data);
					
				}else{
					$scope.show_loading_DATA = false;
					$scope.show_error = true;
				}
				
			},
			error : function (response){}
			})
	}

	$scope.graph_agent = function($object){

	    var data = new google.visualization.DataTable();
		angular.forEach($object.cols, function(arr){
				
			data.addColumn(arr.type, arr.label);
		});

		angular.forEach($object.rows, function(arr){
				data.addRows([
					[arr.c[0].v, Number(arr.c[1].v), Number(arr.c[2].v),  Number(arr.c[3].v)],
				]);;
		});
		
		// var maxNumber = 10;

		// for(var i=0; i<10; i++){
			
		// 	if ($object.rows[i]['c'].length > 2) {
		// 		for(var j=1; j<$object.rows[i]['c'].length; j++){
		// 			if($object.rows[i]['c'][j]['v'] > maxNumber){
		// 				maxNumber = $object.rows[i]['c'][j]['v'];
		// 			}
		// 		}
		// 	}
			
		// }
		var options = {
			'title': 'Most Active Agencies',
	        legend: { position: 'bottom' },
	        fontSize: 10,
	        responsive: true,
	        colors: ['#2980b9', '#27ae60', '#e74c3c'],
	        chartArea: {width: '45%', heigth: '100%'},
		};
		
		var chart = new google.visualization.BarChart(document.getElementById('agentLine'));
		
		chart.draw(data, options);
	}

	$scope.loadReportAgentData = function()
	{
		
		$scope.show_loading_DATA = true;
		$scope.agent = [];
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		if (!$scope.search.month) $scope.search.month = $scope.month.toString();
		if (!$scope.search.year) $scope.search.year = $scope.year.toString();
		if (!$scope.search.start_date) $scope.search.start_date = $scope.start_date.toString();


		var _search = {	
						"month" 	: $scope.search.month,
						"year"		: $scope.search.year,
						"start_date" 		: $scope.search.start_date,
						"finish_date" 		: $scope.search.end_date,
						"view" 		: $scope.search.view
					  };
		
		httpSrv.post({
			url 	: "transport/report/agent",
			data	: $.param(_search),
			success : function(response){
				$scope.show_graph = true;
				$scope.show_loading_DATA = false;
				if (response.data.status == "SUCCESS") {
					
					$scope.show_error = false;
					$scope.agent = response.data.sales;
					$scope.total_cancel_agents(response.data.sales);
					$scope.graph_agent(response.data.graph);
					$scope.getTotalAgent2(response.data.sales);
					
				}else{

					$scope.graph_agent_null();
					$scope.show_error = true;
				}
				
			},
			error : function (error){
				$scope.show_error = true;
				$scope.show_loading_DATA = false;
			}
		})
	}

	$scope.total_cancel_agents = function(data){
		var total = 0;
		var currency = '';
		$scope.total_cancel_agent = 0;
		angular.forEach(data, function(row){
			total += parseInt(row.cancel);
			if (currency=='') {
				currency = row.currency;
			}
		});
		$scope.currency_agent = currency;
		$scope.total_cancel_agent = total;

	}

	$scope.getTotal = function(data){
	    var total = 0;
	    angular.forEach(data.sources, function(row){
	    	total += parseInt(row.total); 
	    
	    });
	   
	    $scope.total = total;
	   
	}
	$scope.getTotalDate = function(data){
		
	    var total = 0;
		var total_trip = 0;
		var currency = '';

	    angular.forEach(data.sales, function(row){
	    	total += parseInt(row.total);
	    	total_trip += parseInt(row.book);

	    	currency = row.currency;
	    });
	   
	    $scope.total_date = total;
	    $scope.total_trip = total_trip;

	    $scope.currency = currency;
	   
	}
	$scope.getTotalAgent = function(data){

	    var total = 0;
	    angular.forEach(data, function(row){
	    total += parseInt(row.total);

	    });

	    $scope.totalagent = total;
	}
	$scope.getTotalAgent2 = function(data){

	    var total = 0;
	    var book = 0;
	    var guest = 0;
	    var currency = '';
	    angular.forEach(data, function(row){
	    	total += parseInt(row.total);
	    	book += parseInt(row.book);
	    	guest += parseInt(row.guest);
	    	currency = row.currency;

	    });
	    $scope.currency = currency;
	    $scope.total_book = book;
	    $scope.totalagent = total;
	    $scope.total_guest = guest;
	}
	$scope.getTotalSales = function(data){
	    var total = 0;
	    var total_guest = 0;
	    var total_book = 0;
	    var currency = '';

	    angular.forEach(data, function(row){

	    	total += parseInt(row.sources.total); 
	    	total_guest += parseInt(row.sources.total_guest);
			total_book += parseInt(row.sources.total_book);

			currency = row.sources.currency;
	    
	    });
	   
	    $scope.total_sales = total;
	    $scope.total_guest = total_guest;
	    $scope.total_book = total_book;
	    $scope.currency = currency;
	  
	}
	$scope.total_books = function(data)
	{
		var total_agent = 0;
		var total_offline = 0;
		var total_online = 0;
		angular.forEach(data, function(row){
			//total += parseInt(row.sources);
			total_agent += parseInt(row.sources['AGENT'].book);
			total_offline += parseInt(row.sources['OFFLINE'].book);
			total_online += parseInt(row.sources['ONLINE'].book);
		});

		$scope.total_book_agent = total_agent;
		$scope.total_book_offline = total_offline;
		$scope.total_book_online = total_online;
	}

	$scope.total_guests = function(data)
	{
		var total_agent = 0;
		var total_offline = 0;
		var total_online = 0;
		angular.forEach(data, function(row){
			//total += parseInt(row.sources['AGENT'].total_guest);
			total_agent += parseInt(row.sources['AGENT'].guest);
			total_offline += parseInt(row.sources['OFFLINE'].guest);
			total_online += parseInt(row.sources['ONLINE'].guest);
		});
		
		$scope.total_guest_agent = total_agent;
		$scope.total_guest_offline = total_offline;
		$scope.total_guest_online = total_online;
	}
	$scope.graph2 = function($object){
		
		$scope.chartObject = {};
		var maxNumber = 10;
		for(var i=0; i<$object.rows.length; i++){
			
			for(var j=1; j<$object.rows[i]['c'].length; j++){
				if($object.rows[i]['c'][j]['v'] > maxNumber){
					maxNumber = $object.rows[i]['c'][j]['v'];
				}
			}
		}
		
	    $scope.chartObject.data = $object;
	   
	    $scope.chartObject.type = "LineChart";
	    $scope.chartObject.options = {
	    	vAxis: {format: 'decimal', minValue: 0, maxValue: maxNumber, gridlines: {count: -1}, textStyle: {fontSize: 10}},
			hAxis: {textStyle: {fontSize: 8}},
	        'title': 'Booking',
	        legend: { position: 'bottom' },
	        fontSize: 10,
	        responsive: true,
	        chartArea: {width: '85%', heigth: '100%'},
	       
	    }

	}
	
});
function activate_sub_menu_agent_detail(class_menu){
	$(".agent-detail ul.nav.sub-nav li").removeClass("active");
	$(".agent-detail ul.nav.sub-nav li."+class_menu).addClass("active");
}
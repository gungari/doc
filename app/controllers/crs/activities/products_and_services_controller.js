// JavaScript Document
angular.module('app').controller('products_and_services',function($scope, $http, httpSrv, fn, $stateParams, $interval){

	GeneralJS.activateLeftMenu("products_and_services");
	GeneralJS.activateSubMenu(".nav-pills", "li", ".schedules")
	
	$scope.DATA = {};
	$scope.fn = fn;
	
	//Load Data Product
	$scope.loadDataProducts = function(){
		httpSrv.post({
			url 	: "activities/product",
			data	: $.param({"publish_status":"all"}),
			success : function(response){
				$scope.DATA.products = response.data;
				
				if ($scope.DATA.products.status == "SUCCESS"){
					/*
					$scope.DATA.products.products.forEach(function(_item, index) {
						var products = angular.copy(_item);
						httpSrv.get({
							url 	: "activities/product/detail/"+schedule.schedule_code,
							success : function(response_detail){
								_item.schedule_detail = response_detail.data;
							},
							error : function (response){}
						});
					});
					*/
				}
			},
			error : function (response){}
		});
	}
	$scope.publishUnpublishProduct = function (product, status){

		var path = (status=='1')?"publish":"unpublish";
		product.publish_status = status;
		
		httpSrv.get({
			url 	: "activities/product/"+path+"/"+product.product_code,
			success : function(response){
				if (response.data.status != "SUCCESS"){
					product.publish_status = !status;
				}
			},
			error : function (response){}
		});
	}
	$scope.loadDataProductDetail = function(product_code, after_load_handler_function){
		
		if (!product_code){ product_code = $stateParams.product_code; }
		
		httpSrv.get({
			url 	: "activities/product/detail/"+product_code,
			success : function(response){
				var product = response.data;
				
				if (product.status == "SUCCESS"){
					$scope.DATA.current_product = product;
					
					if (typeof after_load_handler_function == 'function') {
						after_load_handler_function(product);
					}
				}else{
					alert("Sorry, product not found..");
					window.location = "#/activities/product_add";
				}
			},
			error : function (response){}
		});
	}
	//---
	
	//Inventory
	$scope.loadInventoryProduct = function(){
		
		if (!$scope.filter_date_search){
			var now = new Date();
			$scope.filter_search = {};
			$scope.filter_date_search = {};
			$scope.filter_date_search.date = now;
			$scope.filter_date_search.str = fn.formatDate(now, "yy-mm-dd");
		}else{
			$scope.filter_date_search.date = new Date($scope.filter_date_search.str);
		}

		var loadInventory = function(){
				var current_product = $scope.DATA.current_product;
				
				var start_date	= $scope.filter_date_search; 
				var end_date	= {};
				end_date.date 	= start_date.date; end_date.date.setDate(start_date.date.getDate() + 9);
				end_date.str	= fn.formatDate(end_date.date, "yy-mm-dd");
				
				var data = {"product_code":current_product.product_code,
							"start_date":start_date.str,
							"end_date":end_date.str};
				
				//Load Product Inventory
				httpSrv.post({
					url 	: "activities/inventory/product/",
					data	: $.param(data),
					success : function(response){
						var inventories = response.data;
						$scope.DATA.inventories = inventories;
					},
					error : function (response){}
				});
				
				//Load Inventory Model
				httpSrv.post({
					url 	: "activities/inventory/",
					data	: $.param({"product_code":current_product.product_code}),
					success : function(response){
						var inventories = response.data;
						$scope.DATA.inventory_models = inventories;
					},
					error : function (response){}
				});
			}
		
		if (!$scope.$parent.DATA.current_product){
			product_code = $stateParams.product_code;
			$scope.loadDataProductDetail(product_code,loadInventory);
		}else{
			$scope.DATA.current_product = $scope.$parent.DATA.current_product;
			loadInventory();
		}
		
	}
	$scope.prev_next_inventory = function(act){
		var start_date = $scope.DATA.inventories.start_date;
		var end_date = $scope.DATA.inventories.end_date;

		if (act == 'prev'){
			var date = new Date(start_date);
			date.setDate(date.getDate() - 10);
		}else if (act == 'next'){
			var date = new Date(end_date);
			date.setDate(date.getDate() + 1);
		}
		$scope.filter_date_search.str = fn.formatDate(date, "yy-mm-dd");
		$scope.loadInventoryProduct();
	}
	$scope.change_inventory_model = function(){
		$scope.DATA.inventory_model_update = {	"inventory_id"	:"",
												"inventory_id_2":"",
												"inventory_id_3":""};
		$scope.DATA.inventory_model_update.qty = 0;
		if ($scope.DATA.inventories.inventory_model){
			$scope.DATA.inventory_model_update.inventory_id = $scope.DATA.inventories.inventory_model.id;
			$scope.DATA.inventory_model_update.qty = 1;
		}
		if ($scope.DATA.inventories.inventory_model_2){
			$scope.DATA.inventory_model_update.inventory_id_2 = $scope.DATA.inventories.inventory_model_2.id;
			$scope.DATA.inventory_model_update.qty = 2;
		}
		if ($scope.DATA.inventories.inventory_model_3){
			$scope.DATA.inventory_model_update.inventory_id_3 = $scope.DATA.inventories.inventory_model_3.id;
			$scope.DATA.inventory_model_update.qty = 3;
		}
	}
	$scope.product_change_inventory_model = function(event){

		$(event.target).find("button:submit").attr("disabled","disabled");
		var current_product = $scope.DATA.current_product;
		
		var data = {"product_code":current_product.product_code,
					"inventory_id":$scope.DATA.inventory_model_update.inventory_id};
					
		if ($scope.DATA.inventory_model_update.inventory_id_2 && $scope.DATA.inventory_model_update.inventory_id_2!="" && $scope.DATA.inventory_model_update.qty >= 2){
			data.inventory_id_2 = $scope.DATA.inventory_model_update.inventory_id_2;
		}
		if ($scope.DATA.inventory_model_update.inventory_id_3 && $scope.DATA.inventory_model_update.inventory_id_3!="" && $scope.DATA.inventory_model_update.qty >= 3){
			data.inventory_id_3 = $scope.DATA.inventory_model_update.inventory_id_3;
		}
		
		httpSrv.post({
			url 	: "activities/inventory/product_change_inventory_model",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadInventoryProduct();
					toastr.success("Saved...");
					$('.allotment-header').show(); $('.change-allotment-header').hide();
				}else{
					toastr.error("Update failed...");
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	$scope.addInventoryOpenCloseDate = function(){
		$(".openCloseDateDatepicker").datepicker({dateFormat :"yy-mm-dd", "minDate":0});
		$scope.DATA.inv_open_close_date = {};
	}
	$scope.saveInventoryOpenCloseDate = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		
		$scope.DATA.inv_open_close_date.error_msg = [];
		
		var current_product = $scope.DATA.current_product;
		
		var data_send = angular.copy($scope.DATA.inv_open_close_date);
		data_send.product_code = current_product.product_code;
		
		httpSrv.post({
			url 	: "activities/inventory/update_closed_date/",
			data	: $.param(data_send),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadInventoryProduct();
					$("#inventory-open-close-date").modal("hide");
					toastr.success("Saved...");
				}else{
					$scope.DATA.inv_open_close_date.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	$scope.addInventoryUpdateAllotment = function(){
		$(".updateAllotmentDatepicker").datepicker({dateFormat :"yy-mm-dd", "minDate":0});
		$scope.DATA.inv_update_allotment = {"inventory":0};
	}
	$scope.saveInventoryUpdateAllotment = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		
		$scope.DATA.inv_update_allotment.error_msg = [];
		
		var current_product = $scope.DATA.current_product;
		
		var data_send = angular.copy($scope.DATA.inv_update_allotment);
		data_send.product_code = current_product.product_code;
		
		httpSrv.post({
			url 	: "activities/inventory/update_inventory/",
			data	: $.param(data_send),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadInventoryProduct();
					$("#inventory-update-allotment").modal("hide");
					toastr.success("Saved...");
				}else{
					$scope.DATA.inv_update_allotment.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	$scope.addInventoryModel = function(){
		$(".inventoryModelDatepicker").datepicker({dateFormat :"yy-mm-dd", "minDate":0});
		var current_product = $scope.DATA.current_product;
		$scope.DATA.inv_inventory_model = {"inventory":0,"inventory_name":"Inventory Model - "+current_product.name};
	}
	$scope.saveInventoryModel = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		
		$scope.DATA.inv_inventory_model.error_msg = [];
		
		var current_product = $scope.DATA.current_product;
		
		var data_send = angular.copy($scope.DATA.inv_inventory_model);
		data_send.product_code = current_product.product_code;

		httpSrv.post({
			url 	: "activities/inventory/create_inventory/",
			data	: $.param(data_send),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadInventoryProduct();
					$("#inventory-new-inventory-model").modal("hide");
					$('.allotment-header').show(); $('.change-allotment-header').hide();
					toastr.success("Saved...");
				}else{
					$scope.DATA.inv_inventory_model.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	$scope.addInventoryDblClickCellTable = function(inventory){
		if (!$scope.DATA.inventories.inventory_model_2){
			$scope.addInventoryUpdateAllotment();
			$scope.DATA.inv_update_allotment = {"inventory":inventory.allotment,
												"start_date":inventory.date,
												"end_date":inventory.date};
			$("#inventory-update-allotment").modal("show");
		}
	}
	//---
	
	//RATES
	$scope.loadMasterRates = function(){
		
		var loadMasterRates = function(){
				var current_product = $scope.DATA.current_product;
				//Load Data Rates
				httpSrv.post({
					url 	: "activities/rates/product/"+current_product.product_code,
					data	: $.param({"publish_status":"all", "group_by_trip" : "1"}),
					success : function(response){
						var rates = response.data;
						$scope.DATA.rates = rates;
					},
					error : function (response){}
				});
			}
		
		if (!$scope.$parent.DATA.current_product){
			product_code = $stateParams.product_code;
			$scope.loadDataProductDetail(product_code,loadMasterRates);
		}else{
			$scope.DATA.current_product = $scope.$parent.DATA.current_product;
			loadMasterRates();
		}
		
	}
	$scope.publishUnpublishProductRates = function (rates, status){
		
		var path = (status=='1')?"publish":"unpublish";
		rates.publish_status = status;
		
		httpSrv.get({
			url 	: "activities/rates/"+path+"_rates/"+rates.rates_code,
			success : function(response){
				if (response.data.status != "SUCCESS"){
					schedule.publish_status = !status;
				}
			},
			error : function (response){}
		});
	}
	$scope.loadMasterRatesDetail = function (rates_code, affter_success_handler){
		httpSrv.post({
			url 	: "activities/rates/rates_detail/"+rates_code,
			success : function(response){
				
				var rates_detail = response.data;
				
				if (fn.isFn(affter_success_handler)){
					affter_success_handler(rates_detail);
				}
				
			},
			error : function (response){}
		});
	}
	$scope.loadMasterRatesDetailForRatesList = function(rates){
		if (!rates.already_get_detail){
			var rates_code = rates.rates_code;
			$scope.loadMasterRatesDetail(rates_code, function(rates_detail){
				if (rates_detail.status == "SUCCESS"){
					//Check Key One by One and insert new data
					for (var key in rates_detail){
						if (!rates[key]){
							rates[key] = rates_detail[key];
						}
					}
					rates.already_get_detail = true;
				}
			});
		}		
	}
	$scope.duplicateProductRates = function (rates){
		
		rates_detail = angular.copy(rates);
		
		if (!confirm("Click OK to continue duplicate this rates...")){
			return;
		}
		
		var duplicateRates = function(rates_detail){
			rates_detail.product_code = $scope.DATA.current_product.product_code;
			rates_detail.name = rates_detail.name+' - (Copy)';
			
			delete rates_detail.already_get_detail;
			
			if (!rates_detail.is_custom_sell_rate){
				delete rates_detail.rates_caption;
				delete rates_detail.rates_unit;
			}
			console.log(rates_detail);
			
			httpSrv.post({
				url 	: "activities/rates/create",
				data	: $.param(rates_detail),
				success : function(response){
					if (response.data.status == "SUCCESS"){
						$scope.loadMasterRates();
						toastr.success("Duplicated...");
					}
				}
			});
		}
		
		if (rates_detail.already_get_detail){
			duplicateRates(rates_detail);
		}else{
			$scope.loadMasterRatesDetail(rates_detail.rates_code, 
				function(rates_detail){
					duplicateRates(rates_detail);
				}
			);
		}
		//console.log(rates_detail);
	}
	$scope.deleteProductRates = function (rates){
		if (confirm("Are you sure to delete this rates?")){
			httpSrv.get({
				url 	: "activities/rates/delete_rates/"+rates.rates_code,
				success : function(response){
					if (response.data.status == "SUCCESS"){
						$scope.loadMasterRates();
						toastr.success("Deleted...");
					}
				},
				error : function (response){}
			});
		}
	}

	$scope.ratesAddEdit = function(){
		var product_code = $stateParams.product_code;
		var rates_code = $stateParams.rates_code;
		
		$scope.DATA.current_rates = {};
		
		var generateDataEditRates = function(){
			var generate_data_important_info = function (data){
				var arr_data = [];
				if (data){
					for (index in data){
						arr_data.push({"str":data[index]});
					}
				}
				return arr_data;
			}
			//$scope.DATA.current_rates.additional_item = generate_data_important_info($scope.DATA.current_rates.additional_item);
			$scope.DATA.current_rates.inclusion = generate_data_important_info($scope.DATA.current_rates.inclusion);
			$scope.DATA.current_rates.exclusion = generate_data_important_info($scope.DATA.current_rates.exclusion);
			$scope.DATA.current_rates.additional_info = generate_data_important_info($scope.DATA.current_rates.additional_info);
			
			if ($scope.DATA.current_rates.available_on == "all_day"){
				var days = ["sun", "mon", "tue", "wed", "thu", "fri", "sat"];
				$scope.DATA.current_rates.available_on = days;
			}
		}
		
		if (rates_code){
			//Edit Rates
			$scope.loadMasterRatesDetail(rates_code,function(rates_detail){
				$scope.DATA.current_rates = rates_detail;
				
				if (!$scope.DATA.current_rates.additional_charge){
					$scope.DATA.current_rates.additional_charge = [];
				}
				
				//console.log($scope.DATA.current_rates);
				
				if (!rates_detail.pickup_area) rates_detail.pickup_area = [];
				if (!rates_detail.dropoff_area) rates_detail.dropoff_area = [];
				if (!rates_detail.packages_option) rates_detail.packages_option = [];
				
				$scope.DATA.current_rates.custom_rates = [];
				if (rates_detail.is_custom_sell_rate){
					for (index in rates_detail.rates){
						var add = {	"rates" 		: rates_detail.rates[index],
									"rates_caption"	: rates_detail.rates_caption[index],
									"rates_unit"	: rates_detail.rates_unit[index]};
						$scope.DATA.current_rates.custom_rates.push(add);
					}
				}
				generateDataEditRates();
			});
		}else{
			//Create New Rates
			$scope.DATA.current_rates = {	"booking_handling":"INSTANT",
											"available_on":"all_day",
											"duration":1,
											"duration_unit":"JAM",
											"min_order":1,
											"cut_of_booking":0,
											"pickup_service":"no", "pickup_area":[],
											"dropoff_service":"no","dropoff_area":[],
											"additional_charge":[],"inclusion":[],"exclusion":[],"additional_info":[],
											"rates":{"rates_1":0,"rates_2":0,"rates_3":0,"rates_4":0,"rates_5":0},
											"is_custom_rates_name":0,
											"custom_rates":[],
											"packages_option":[],
										};
			$scope.DATA.enable_copy_rates = true;
			generateDataEditRates();
		}
				
		$scope.loadDataProductDetail(product_code, 
			function(product_detail){
				$scope.DATA.product_detail = product_detail;
			}
		);
		
		//$scope.DATA.current_rates = {"name":"Test Promo Rates","booking_handling":"INSTANT","start_date":"2017-05-01","end_date":"2017-12-31","min_order":1,"rates":{"one_way_rates":{"rates_1":650000,"rates_2":550000,"rates_3":500000}}};
		//$scope.DATA.current_rates = {"booking_handling":"INSTANT","available_on":["sun","mon","wed","fri"],"min_order":1,"is_rates_return_active":false,"cut_of_booking":0,"pickup_service":"yes","pickup_area":[{"area":"Sanur","time":"10:00 AM","price":0,"type":"way"},{"area":"Kuta","time":"10:00","price":150000,"type":"way"}],"dropoff_service":"yes","dropoff_area":[{"area":"Sanur Drop","time":"15:00 PM","price":0,"type":"way"},{"area":"Kuta Drop","time":"15:00 PM","price":150000,"type":"way"}],"inclusion":[{"str":"i"},{"str":"ii"},{"str":"iii"}],"exclusion":[{"str":"e"},{"str":"ee"},{"str":"eee"}],"additional_info":[{"str":"a"},{"str":"aa"},{"str":"aaa"}],"name":"Test Input Promo Rates","departure_trip_number":"4","arrival_trip_number":"1","start_date":"2017-05-01","end_date":"2017-12-31","rates":{"one_way_rates":{"rates_1":500000,"rates_2":450000,"rates_3":0},"is_rates_return_active":true,"return_rates":{"rates_1":900000,"rates_2":850000,"rates_3":0}}};
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
	}
	$scope.saveDataProductRates = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.current_rates.error_desc = [];
		
		var current_rates =angular.copy($scope.DATA.current_rates);
		var data_rates = current_rates;
		data_rates.product_code = $scope.DATA.current_product.product_code;
		
		delete data_rates.rates_caption;
		delete data_rates.rates_unit;
		delete data_rates.duration_unit_desc;
		delete data_rates.rates_for.all;
		if (!data_rates.rates_for.offline) delete data_rates.rates_for.offline;
		if (!data_rates.rates_for.walkin) delete data_rates.rates_for.walkin;
		if (!data_rates.rates_for.agent) delete data_rates.rates_for.agent;
		
		//Rates
		if (data_rates.is_custom_sell_rate){
			delete data_rates.rates;
			data_rates.rates = {}; data_rates.rates_caption = {}; data_rates.rates_unit = {};
			
			if (data_rates.custom_rates[0]){
				data_rates.rates.rates_1 		= data_rates.custom_rates[0].rates;
				data_rates.rates_caption.rates_1= data_rates.custom_rates[0].rates_caption;
				data_rates.rates_unit.rates_1 	= data_rates.custom_rates[0].rates_unit;
			}
			if (data_rates.custom_rates[1]){
				data_rates.rates.rates_2 		= data_rates.custom_rates[1].rates;
				data_rates.rates_caption.rates_2= data_rates.custom_rates[1].rates_caption;
				data_rates.rates_unit.rates_2 	= data_rates.custom_rates[1].rates_unit;
			}
			if (data_rates.custom_rates[2]){
				data_rates.rates.rates_3 		= data_rates.custom_rates[2].rates;
				data_rates.rates_caption.rates_3= data_rates.custom_rates[2].rates_caption;
				data_rates.rates_unit.rates_3 	= data_rates.custom_rates[2].rates_unit;
			}
			if (data_rates.custom_rates[3]){
				data_rates.rates.rates_4 		= data_rates.custom_rates[3].rates;
				data_rates.rates_caption.rates_4= data_rates.custom_rates[3].rates_caption;
				data_rates.rates_unit.rates_4 	= data_rates.custom_rates[3].rates_unit;
			}
			if (data_rates.custom_rates[4]){
				data_rates.rates.rates_5 		= data_rates.custom_rates[4].rates;
				data_rates.rates_caption.rates_5= data_rates.custom_rates[4].rates_caption;
				data_rates.rates_unit.rates_5 	= data_rates.custom_rates[4].rates_unit;
			}
		}else{
			delete data_rates.custom_rates;
		}
		//
		
		var get_arr = function(data_str){
			var data = [];
			for (key in data_str){ 
				data.push(data_str[key].str); 
			}
			return data;
		}
		//data_rates.additional_item = get_arr(data_rates.additional_item);
		data_rates.inclusion = get_arr(data_rates.inclusion);
		data_rates.exclusion = get_arr(data_rates.exclusion);
		data_rates.additional_info = get_arr(data_rates.additional_info);
		
		//Rates For Category Contract Rates
		if ($scope.DATA.current_contract_rates){
			delete data_rates.rates_for.all;
			delete data_rates.rates_for.offline;
			delete data_rates.rates_for.walkin;
			delete data_rates.rates_for.agent;
			
			data_rates.rates_for.agent = 1;
			data_rates.contract_rates_code = $scope.DATA.current_contract_rates.contract_rates_code;
		}
		//--
		
		//console.log(data_rates);
		//return;
		//$scope.data_rates = data_rates;
		httpSrv.post({
			url 	: "activities/rates/"+(($scope.DATA.current_rates.rates_code)?"update":"create"),
			data	: $.param(data_rates),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					if ($scope.DATA.current_contract_rates){
						window.location = "#/agent/real_category/detail/"+$scope.DATA.current_contract_rates.real_category_code+"/contract_rates/detail/"+$scope.DATA.current_contract_rates.contract_rates_code;
					}else{
						window.location = "#/activities/product_detail/"+$scope.DATA.current_product.product_code+"/rates";
					}
					toastr.success("Saved...");
				}else{
					$scope.DATA.current_rates.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
					$('html, body').animate({scrollTop:200}, 400);
				}
				//$.hideLoading();
			}
		});
	}
	$scope.ratesAddImportantInfo = function(var_to_add){
		var str = {"str":""};
		var_to_add.push(str);
	}
	$scope.ratesAddPickUpOrDropOffArea = function(var_to_add){
		var add = {	"area" : "",
					"time" : "",
					"price": 0,
					"type" : "way"};
		var_to_add.push(add);
	}
	$scope.ratesAddCustomRatesName = function(var_to_add){
		var add = {	"rates" : 0,
					"rates_caption" : "",
					"rates_unit": "Pax"};
		if (var_to_add.length < 5){
			var_to_add.push(add);
		}
	}
	$scope.ratesCheckAvailableDayInWeek = function (day){
		var index = $scope.DATA.current_rates.available_on.indexOf(day);
		if (index >= 0){
			$scope.DATA.current_rates.available_on.splice(index,1);
		}else{
			$scope.DATA.current_rates.available_on.push(day);
		}
	}
	$scope.ratesAddAdditionalCharge = function(var_to_add){
		var _item = {"name":"",
					 "description":"",
					 "price":0,
					 "unit":"Pax",
					 "payment_based_on":"PERPERSON"};
		var_to_add.push(_item);
	}
	
	$scope.ratesAddPackagesName = function(var_to_add){
		var add = {	"caption" : "",
					"rates" : 0,
					"rates_unit": "Packages",
					"remarks" : "",
					"include_pax" : {"opt_1":2,"opt_2":0,"opt_3":0},
					"extra_rates" : {"opt_1":0,"opt_2":0,"opt_3":0}};
		var_to_add.push(add);
	}
	
	$scope.ratesEditInline = function (current_rates){
		current_rates.rates_edit = {};
		current_rates.rates_edit = angular.copy(current_rates.rates);
	}
	$scope.ratesEditInlineSave = function(event, current_rates){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		current_rates.error_desc = [];
		
		var data_rates = angular.copy(current_rates);
			data_rates.rates = angular.copy(data_rates.rates_edit);
			data_rates.product_code = $scope.DATA.current_product.product_code;
		delete data_rates.rates_edit;
		if (!data_rates.is_custom_sell_rate || data_rates.is_custom_sell_rate != 1){
			delete data_rates.rates_caption;
		}

		//console.log(data_rates);
		//return;
		httpSrv.post({
			url 	: "activities/rates/update",
			data	: $.param(data_rates),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					//window.location = "#/activities/product_detail/"+$scope.DATA.current_product.product_code+"/rates";
					current_rates.rates = angular.copy(data_rates.rates);
					delete current_rates.rates_edit;
					toastr.success("Saved...");
				}else{
					current_rates.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
				}
				//$.hideLoading();
			}
		});
	}
	
	//Copy Rates From other product
	$scope.copyRatesGetProductRates = function(){
		httpSrv.post({
			url 	: "activities/rates/product/"+$scope.DATA.copy_rates_from.product.product_code,
			data	: $.param({"publish_status":"all", "group_by_trip" : "1"}),
			success : function(response){
				var rates = response.data;
				$scope.DATA.copy_rates_from.product.rates = rates;
			},
			error : function (response){}
		});
	}
	$scope.copySelectedRates = function(rates){
		$scope.loadMasterRatesDetail(rates.rates_code, function(detail_rates){
			var product_detail_rates = angular.copy(detail_rates);
			delete product_detail_rates.rates_code;
			$scope.DATA.current_rates = product_detail_rates;
			
			var generateDataEditRates = function(){
				var generate_data_important_info = function (data){
					var arr_data = [];
					if (data){
						for (index in data){
							arr_data.push({"str":data[index]});
						}
					}
					return arr_data;
				}
				//$scope.DATA.current_rates.additional_item = generate_data_important_info($scope.DATA.current_rates.additional_item);
				$scope.DATA.current_rates.inclusion = generate_data_important_info($scope.DATA.current_rates.inclusion);
				$scope.DATA.current_rates.exclusion = generate_data_important_info($scope.DATA.current_rates.exclusion);
				$scope.DATA.current_rates.additional_info = generate_data_important_info($scope.DATA.current_rates.additional_info);
				
				if ($scope.DATA.current_rates.available_on == "all_day"){
					var days = ["sun", "mon", "tue", "wed", "thu", "fri", "sat"];
					$scope.DATA.current_rates.available_on = days;
				}
			}
			generateDataEditRates();
			delete $scope.DATA.copy_rates_from.product;
			$("#form-product-list").modal("toggle");
		});
	}
	//--
	
	//Rates For Category Contract Rates
	$scope.getContractRatesDetail = function(){
		var contract_rates_code = $stateParams.contract_rates_code;
		if (contract_rates_code){
			httpSrv.post({
				url 	: "agent_category/contract_rates_detail/"+contract_rates_code,
				success : function(response){
					if (response.data.status == 'SUCCESS'){
						delete response.data.rates_act;
						delete response.data.rates_trans;
						$scope.DATA.current_contract_rates = response.data;
					}
				},
				error : function (response){}
			});
		}
	}
	//--

	$scope.saveReorderRates = function(){
		var form_data = $("#sortable-rates .rates_codes").serializeArray();
		var rates_codes = [];

		for(_i in form_data){
			if (form_data[_i].name = "rates_codes[]"){
				rates_codes.push(form_data[_i].value);
			}
		}
		data = {"rates_codes":rates_codes};
		//console.log(data);

		httpSrv.post({
			url 	: "activities/rates/update_reorder_rates",
			data	: $.param(data),
			success : function(response){
				//console.log(response.data);
				if (response.data.status == "SUCCESS"){
					toastr.success("Saved...");
				}
			}
		});

	}
	//---
	
	
	
	
	
	
	
	$scope.productAddEdit = function(){
		
		var product_code_edit = $stateParams.product_code_edit;
		
		$scope.DATA.current_product = {};
		$scope.DATA.current_product.category = [];
		$scope.DATA.current_product.highlight = [];
		
		if (product_code_edit){
			$scope.loadDataProductDetail(product_code_edit, function(product){
				$scope.DATA.current_product = product;
			});
		}
		
		//Load Category
		httpSrv.get({
			url 	: "activities/product/category",
			success : function(response){
				$scope.DATA.category = response.data;
			},
			error : function (response){}
		});
		
	}
	$scope.saveDataProduct = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.current_product.error_desc = [];
		
		var current_product = angular.copy($scope.DATA.current_product);
		
		var data_product = {"product_code":current_product.product_code,
							"name":current_product.name,
							"highlight":current_product.highlight,
							"short_description":current_product.short_description,
							"full_description":current_product.full_description,
							"show_in":current_product.show_in};

		httpSrv.post({
			url 	: "activities/product/"+(($scope.DATA.current_product.product_code)?"update":"create"),
			data	: $.param(data_product),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					window.location = "#/activities/product_detail/"+response.data.product_code;
					toastr.success("Saved...");
				}else{
					$scope.DATA.current_product.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
					$('html, body').animate({scrollTop:200}, 400);
				}
				//$.hideLoading();
			}
		});
	}
	
	
	
	
	
	
	
	
	
	


	
	
	
	
	$scope.scheduleDetailAddItem = function(){
		var last_schedule_index = $scope.DATA.current_schedule.schedule_detail.length;
		var allow_to_add = false;
		var schedule_detail = {	"departure_port_id" : "", 
								"departure_time"	: "",
								"departure_time_hh"	: "",
								"departure_time_mm"	: "",
								"arrival_port_id"	: "",
								"arrival_time"		: "",
								"arrival_time_mm"	: "",
								"arrival_time_hh"	: ""};
		
		if (last_schedule_index == 0){
			allow_to_add = true;
		}else{
			var last_schedule = $scope.DATA.current_schedule.schedule_detail[(last_schedule_index-1)];
			if (last_schedule.departure_port_id != "" && last_schedule.arrival_port_id != ""){
				allow_to_add = true;
				schedule_detail.departure_port_id = last_schedule.arrival_port_id;
			}else{
				alert("Please complete current schedule first...")
			}
		}
		
		if (allow_to_add){
			$scope.DATA.current_schedule.schedule_detail.push(schedule_detail);
		}
	}
	$scope.scheduleDetailRemoveLastItem = function(){
		$scope.DATA.current_schedule.schedule_detail.pop();
	}
	$scope.changeArrivalPort = function(index){
		if ($scope.DATA.current_schedule.schedule_detail[(index+1)]){
			$scope.DATA.current_schedule.schedule_detail[(index+1)].departure_port_id = $scope.DATA.current_schedule.schedule_detail[index].arrival_port_id;
		}
	}
	
	
	//Boat & Inventory
	$scope.loadBoatAndInventory = function(){
		
		if (!$scope.filter_date_search){
			var now = new Date();
			$scope.filter_search = {};
			$scope.filter_date_search = {};
			$scope.filter_date_search.date = now;
			$scope.filter_date_search.str = fn.formatDate(now, "yy-mm-dd");
		}else{
			$scope.filter_date_search.date = new Date($scope.filter_date_search.str);
		}
		
		if (!$scope.DATA.boats){
			httpSrv.post({
				url 	: "transport/boats",
				success : function(response){
					$scope.DATA.boats = response.data;
				},
				error : function (response){}
			});
		}
		
		//Load Data Port
		if (!$scope.DATA.port_list){
			httpSrv.get({
				url 	: "transport/port/available_port",
				success : function(response){
					$scope.DATA.port_list = response.data;
				},
				error : function (response){}
			});
		}
		
		//$scope.DATA.running_boat = false;
		
		var loadBoatAndInventory = function(){
				var departure_port_id	= $scope.filter_search.departure_port_id;
				var arrival_port_id		= $scope.filter_search.arrival_port_id;
			
				var start_date	= $scope.filter_date_search; 
				var end_date	= {};
				end_date.date 	= start_date.date; end_date.date.setDate(start_date.date.getDate() + 30);
				end_date.str	= fn.formatDate(end_date.date, "yy-mm-dd");
				
				var current_schedule = $scope.DATA.current_schedule;
				//Load Data Boat
				httpSrv.post({
					url 	: "transport/schedule/running_boat/"+current_schedule.schedule_code,
					data	: $.param({"start_date":start_date.str, "end_date":end_date.str}),
					success : function(response){
						var running_boat = response.data;
						$scope.DATA.running_boat = running_boat;
					},
					error : function (response){}
				});
				
				//Load Data Calendar
				var param_calendar = {	"schedule_code"		: current_schedule.schedule_code,
										"departure_port_id"	: departure_port_id,
										"arrival_port_id"	: arrival_port_id,
										"start_date"		: start_date.str,
										"end_date"			: end_date.str,
										};
				httpSrv.post({
					url 	: "transport/schedule/availability_calendar/",
					data	: $.param(param_calendar),
					success : function(response){
						var calendar = response.data;
						if (calendar.status == 'SUCCESS'){
							$scope.DATA.calendar = calendar;
						}
					},
					error : function (response){}
				});
				
				//Load Data Close Date
				httpSrv.post({
					url 	: "transport/schedule/closed_date/"+current_schedule.schedule_code,
					data	: $.param({"array_index_by_date":"1","start_date":start_date.str,"end_date":end_date.str}),
					success : function(response){
						var closed_date = response.data;
						$scope.DATA.closed_date = closed_date;
					},
					error : function (response){}
				});
			}
		
		if (!$scope.$parent.DATA.current_schedule){
			schedule_code = $stateParams.schedule_code;
			$scope.loadDataScheduleDetail(schedule_code,loadBoatAndInventory);
		}else{
			$scope.DATA.current_schedule = $scope.$parent.DATA.current_schedule;
			loadBoatAndInventory();
		}
	}
	$scope.loadBoatAndInventoryPrevNext = function(days){
		var start_date = new Date($scope.filter_date_search.str);
		start_date.setDate(start_date.getDate() + days);
		$scope.filter_date_search.str = fn.formatDate(start_date, "yy-mm-dd");
		
		$scope.loadBoatAndInventory();
	}
	
	$scope.setupOpenCloseDate = function(){
		$scope.DATA.editOpenCloseDate = {};
		$scope.DATA.editOpenCloseDate.status = "close";
	}
	$scope.setupOpenCloseCurrentDate = function(date, status){
		var data = {"start_date":date, "end_date":date, "status":((status=="1")?"open":"close")};
		
		httpSrv.post({
			url 	: "transport/schedule/change_closed_date/"+$scope.DATA.current_schedule.schedule_code,
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					if (status == "1"){
						toastr.success(date+" opened..");
						delete $scope.DATA.closed_date.closed_date[date];
					}else{
						toastr.success(date+" closed..");
						$scope.DATA.closed_date.closed_date[date] = {};
						$scope.DATA.closed_date.closed_date[date] = {"date":date, "remarks":""};
					}
				}
			}
		});
	}
	$scope.saveDataOpenCloseDate = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.editOpenCloseDate.error_msg = [];
		
		httpSrv.post({
			url 	: "transport/schedule/change_closed_date/"+$scope.DATA.current_schedule.schedule_code,
			data	: $.param($scope.DATA.editOpenCloseDate),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadBoatAndInventory();
					$("#popup-open-close-date").modal("hide");
					toastr.success("Saved...");
				}else{
					$scope.DATA.editOpenCloseDate.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	
	$scope.setupChangeBoat = function(boat, date){
		if (!$scope.DATA.boats){
			httpSrv.post({
				url 	: "transport/boats",
				success : function(response){
					$scope.DATA.boats = response.data;
				},
				error : function (response){}
			});
		}
		
		$scope.DATA.editChangeBoat = {};
		if (boat && date){
			$scope.DATA.editChangeBoat.start_date = date;
			$scope.DATA.editChangeBoat.end_date = date;
			$scope.DATA.editChangeBoat.boat_id = boat.id;
		}
	}
	$scope.changeCurrentBoat = function(boat, date){
		var data = {"start_date":date, "end_date":date, "boat_id":boat.id};
		
		httpSrv.post({
			url 	: "transport/schedule/change_running_boat/"+$scope.DATA.current_schedule.schedule_code,
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadBoatAndInventory();
					toastr.success("Boat changed to "+boat.name+"...");
				}
			}
		});
	}
	$scope.saveDataChangeBoat = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.editChangeBoat.error_msg = [];

		httpSrv.post({
			url 	: "transport/schedule/change_running_boat/"+$scope.DATA.current_schedule.schedule_code,
			data	: $.param($scope.DATA.editChangeBoat),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadBoatAndInventory();
					$("#popup-change-boat").modal("hide");
					toastr.success("Saved...");
				}else{
					$scope.DATA.editChangeBoat.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	
	
	
	$scope.filterDeparture = function (_item){		
		if ($scope.DATA.rates){
			if ($scope.$parent.filter && $scope.$parent.filter.departure_port != ""){
				var arr_rates = {};
				for (key in _item){
					if ($scope.DATA.rates.rates.trips.departure[key].port.id == $scope.$parent.filter.departure_port){
						arr_rates[key] = _item[key];
					}
				}
				return arr_rates;
			}else{
				return _item;
			}
		}
	}
	$scope.filterArrival = function (_item){		
		if ($scope.DATA.rates){
			if ($scope.$parent.filter && $scope.$parent.filter.arrival_port != ""){
				var arr_rates = {};
				for (key in _item){
					if ($scope.DATA.rates.rates.trips.arrival[key].port.id == $scope.$parent.filter.arrival_port){
						arr_rates[key] = _item[key];
					}
				}
				return arr_rates;
			}else{
				return _item;
			}
		}
	}
	
	
	$scope.smartPricingEdit = function(){
		
		var schedule_code 	= $stateParams.schedule_code;
		var rates_code		= $stateParams.rates_code;
		
		if (!$scope.DATA.current_rates){
			$scope.DATA.current_rates = {};
		}	
		$scope.loadMasterRatesDetail(rates_code,
			function(rates_detail){
				$scope.DATA.current_rates = rates_detail;
			}
		);
		
		
		if (!$scope.DATA.smart_pricing){
			$scope.DATA.smart_pricing= {};
		}
		
		$scope.DATA.smart_pricing.smart_pricing= {};
		
		var data_search = {};
		
		if ($scope.DATA.smart_pricing.search){
			data_search = $scope.DATA.smart_pricing.search;
		}
		
		
		httpSrv.post({
			url 	: "transport/schedule/smart_pricing/"+rates_code,
			data	: $.param(data_search),
			success : function(response){
				var rates = response.data;
				if (response.data.status == 'SUCCESS'){
					$scope.DATA.smart_pricing = response.data;
				}
			},
			error : function (response){}
		});

		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
	}
	$scope.enableDisableSmartPricing = function (rates, status){
		
		if (confirm("Click OK to continue...")){
			var path = (status=='1')?"enable":"disable";
			
			if (status == '1'){
				$("#btn-activate-smart-pricing").attr("disabled","disabled");
			}else{
				$("#btn-activate-smart-pricing").removeAttr("disabled");
			}
			
			httpSrv.get({
				url 	: "transport/schedule/"+path+"_smart_pricing/"+rates.rates_code,
				success : function(response){
					if (response.data.status == "SUCCESS"){
						if (status == '1'){
							toastr.success("Smart Pricing activate...");
						}else{
							toastr.success("Smart Pricing disabled...");
						}
						$scope.smartPricingEdit();
					}else{
						$("#btn-activate-smart-pricing").removeAttr("disabled");
					}
				},
				error : function (response){}
			});
		}
	}
	$scope.addSmartPricingAddLevel = function(){
		var level = {};
		level.oneway = {"qty_1" : 0, "qty_2" : 0, "qty_3" : 0};
		level.return = {"qty_1" : 0, "qty_2" : 0, "qty_3" : 0};
		level.allotment = 0;
		
		$scope.currentSmartPricing.level.push(level);
	}
	$scope.removeSmartPricingAddLevel = function(index){
		$scope.currentSmartPricing.level.splice(index,1);
	}
	$scope.editSmartPricing = function(pricing){
		
		if (pricing){
			
			$scope.currentSmartPricing = {	"start_date" 	: pricing.date,
											"end_date"		: pricing.date,
											"level"			: []};
			for (i in pricing.pricing_level){
				var pricing_level = {};
				
				pricing_level.oneway = {}
				pricing_level.oneway.rates_1 = pricing.pricing_level[i].rates.one_way_rates.rates_1;
				pricing_level.oneway.rates_2 = pricing.pricing_level[i].rates.one_way_rates.rates_2;
				pricing_level.oneway.rates_3 = pricing.pricing_level[i].rates.one_way_rates.rates_3;
				
				pricing_level.return = {};
				pricing_level.return.rates_1 = pricing.pricing_level[i].rates.return_rates.rates_1*2;
				pricing_level.return.rates_2 = pricing.pricing_level[i].rates.return_rates.rates_2*2;
				pricing_level.return.rates_3 = pricing.pricing_level[i].rates.return_rates.rates_3*2;
				
				pricing_level.allotment = pricing.pricing_level[i].allotment;
				
				$scope.currentSmartPricing.level.push(pricing_level);
			}								
		}else{
		
			$scope.currentSmartPricing = {	"start_date" 	: '',
											"end_date"		: '',
											"level"			: []};
			$scope.addSmartPricingAddLevel();
			
		}
	}
	$scope.saveDataSmartPricing = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.currentSmartPricing.error_desc = [];
		
		var data_rates = angular.copy( $scope.currentSmartPricing);
		data_rates.rates_code = $scope.DATA.current_rates.rates_code;
		
		httpSrv.post({
			url 	: "transport/schedule/update_smart_pricing",
			data	: $.param(data_rates),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.smartPricingEdit();
					toastr.success("Saved...");
					$("#add-edit-smart-pricing").modal("hide");
				}else{
					$scope.currentSmartPricing.error_desc = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	
});

function activate_sub_menu_schedule_detail(class_menu){
	$(".schedule-detail ul.nav.sub-nav li").removeClass("active");
	$(".schedule-detail ul.nav.sub-nav li."+class_menu).addClass("active");
}

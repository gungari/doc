angular.module('app').controller('room_inventory_controller', function($scope, $http, httpSrv, fn, $stateParams, $interval){
    
    GeneralJS.activateLeftMenu("roominventory");

    $scope.DATA = {};
    $scope.fn = fn;

    $scope.roomtype_code = "";

    $scope.loadDataRoomType = function() {
         $(".datepicker").datepicker({dateFormat :"dd MM yy"});
        $scope.roomCode = "";

        httpSrv.get({
            url     : "accommodation/room_type",
            success : function(response) {
                var room_type = response.data.roomtype;

                if (response.data.status == "SUCCESS") {
                    $scope.DATA.room_type = room_type;
                } else {
                    alert("Sorry, room type not found..");
                }
            },
            error : function (response){}
        });
    }

    $scope.loadAllotment = function() {
        $(event.target).find("button:submit").attr("disabled","disabled");
        $(".datepicker").datepicker({dateFormat :"dd MM yy"});

        var data = angular.copy($scope.DATA.data_inven);

        var now = new Date();

        var firstdate = new Date();
        var weekdate = now.setDate(now.getDate() + 7);

        var start_date = $scope.fn.formatDate(firstdate, "yy-mm-dd");
        var end_date = $scope.fn.formatDate(weekdate, "yy-mm-dd");


        $scope.DATA.data_inven = {
            "start_date": start_date,
            "end_date": end_date,
            "month":"",
            "year":""
        };

        httpSrv.post({
            url 	: "accommodation/inventory/index_all/",
            data	: $.param(data),
            success : function(response){
                var inventory = response.data.inventorys;

                if (response.data.status == "SUCCESS") {
                    $scope.DATA.inventory = inventory;
                } else {
                    alert("Sorry, room type not found..");
                }
                
            },
            error : function (response){}
        });

        $scope.DATA.dateNow = $scope.fn.formatDate(now, "dd MM yy");
        $scope.DATA.MonthNow = $scope.fn.formatDate(now, "MM yy");
    }

    $scope.loadRate = function() {
        $(".datepicker").datepicker({dateFormat :"dd MM yy"});

        var data = angular.copy($scope.DATA.data_inven);

        var now = new Date();

        var firstdate = new Date();
        var weekdate = now.setDate(now.getDate() + 7);

        var start_date = $scope.fn.formatDate(firstdate, "yy-mm-dd");
        var end_date = $scope.fn.formatDate(weekdate, "yy-mm-dd");

        $scope.DATA.data_inven = {
            "start_date": start_date,
            "end_date": end_date,
            "month":"",
            "year":""
        };

        httpSrv.post({
            url 	: "accommodation/rate/",
            data	: $.param(data),
            success : function(response){
                var rate = response.data.rate;

                if (response.data.status == "SUCCESS") {
                    $scope.DATA.rate = rate;
                } else {
                    alert("Sorry, room type not found..");
                }
                
            },
            error : function (response){}
        });

        $scope.DATA.dateNow = $scope.fn.formatDate(now, "dd MM yy");
        $scope.DATA.MonthNow = $scope.fn.formatDate(now, "MM yy");
    }

    $scope.roomtype_code = {};
   
    $scope.getRoomTypeCode = function(roomtye_code, is_checked) {
        
        if (is_checked) {
            $scope.roomtype_code[roomtye_code] = roomtye_code;
        }else{
            delete $scope.roomtype_code[roomtye_code];
        }
        console.log($scope.roomtype_code);
        
    }

    $scope.updateAllotment = function() {

        var data = angular.copy($scope.DATA.select);

        var start_date = $scope.fn.formatDate(data.start_date, "yy-mm-dd");
        var end_date = $scope.fn.formatDate(data.end_date, "yy-mm-dd");
        var allotment = data.allotment;

        $scope.DATA.pick = {
            "roomtype_code":$scope.roomtype_code,
            "start_date":start_date,
            "end_date":end_date,
            "allotment":allotment
        }

        var xdata = angular.copy($scope.DATA.pick);

        httpSrv.post({
            url 	: "accommodation/inventory/update_allotment",
            data	: $.param(xdata),
            success : function(response){
                if (response.data.status == "SUCCESS"){
                    toastr.success("Update...");
                    $scope.loadAllotment();
                    $("#modal-edit-allotment").modal("hide");
                } else {
                    $(event.target).find("button:submit").removeAttr("disabled");
                    $('html, body').animate({scrollTop:200}, 400);
                }
            }
        });
    }

    $scope.updateClose = function() {

        var data = angular.copy($scope.DATA.select);

        var start_date = $scope.fn.formatDate(data.start_date, "yy-mm-dd");
        var end_date = $scope.fn.formatDate(data.end_date, "yy-mm-dd");
        var close = data.close;

        if (data.close == "cta") {

            $scope.DATA.pick = {
                "roomtype_code":$scope.roomtype_code,
                "start_date":start_date,
                "end_date":end_date,
                "cta":"1"
            }

            var xdata = angular.copy($scope.DATA.pick);

            httpSrv.post({
                url 	: "accommodation/inventory/update_cta",
                data	: $.param(xdata),
                success : function(response){
                    if (response.data.status == "SUCCESS"){
                        toastr.success("Update...");
                        $scope.loadAllotment();
                        $("#modal-edit-date").modal("hide");
                    } else {
                        $(event.target).find("button:submit").removeAttr("disabled");
                        $('html, body').animate({scrollTop:200}, 400);
                    }
                }
            });

        } else if (data.close == "ctd") {

            $scope.DATA.pick = {
                "roomtype_code":$scope.roomtype_code,
                "start_date":start_date,
                "end_date":end_date,
                "ctd":"1"
            }

            var xdata = angular.copy($scope.DATA.pick);
            
            httpSrv.post({
                url 	: "accommodation/inventory/update_ctd",
                data	: $.param(xdata),
                success : function(response){
                    if (response.data.status == "SUCCESS"){
                        toastr.success("Update...");
                        $scope.loadAllotment();
                        $("#modal-edit-date").modal("hide");
                    } else {
                        $(event.target).find("button:submit").removeAttr("disabled");
                        $('html, body').animate({scrollTop:200}, 400);
                    }
                }
            });

        } else {

            $scope.DATA.pick = {
                "roomtype_code":$scope.roomtype_code,
                "start_date":start_date,
                "end_date":end_date,
                "close":close
            }

            var xdata = angular.copy($scope.DATA.pick);

            httpSrv.post({
                url 	: "accommodation/inventory/update_close_date",
                data	: $.param(xdata),
                success : function(response){
                    if (response.data.status == "SUCCESS"){
                        toastr.success("Update...");
                        $scope.loadAllotment();
                        $("#modal-edit-date").modal("hide");
                    } else {
                        $(event.target).find("button:submit").removeAttr("disabled");
                        $('html, body').animate({scrollTop:200}, 400);
                    }
                }
            });
        }
    }


});

function activate_sub_menu_agent_detail(class_menu){
	$("ul.nav.sub-nav.profile li").removeClass("active");
	$("ul.nav.sub-nav.profile li."+class_menu).addClass("active");
}
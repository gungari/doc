angular.module('app').controller('room_type_controller', function($scope, $http, httpSrv, fn, $stateParams, $interval){

    GeneralJS.activateLeftMenu("roomtype");

    $scope.DATA = {};
    $scope.fn = fn;
    
    $scope.loadDataRoomType = function() {
        httpSrv.get({
            url     : "accommodation/room_type",
            success : function(response) {
                var room_type = response.data.roomtype;

                if (response.data.status == "SUCCESS") {
                    $scope.DATA.room_type = room_type;
                } else {
                    alert("Sorry, room type not found..");
                }
            },
            error : function (response){}
        });
    }

    $scope.loadDataRoomTypeDetail = function(roomtype_code) {
        if (!roomtype_code){ roomtype_code = $stateParams.roomtype_code; }

        httpSrv.get({
            url 	: "accommodation/room_type/detail/"+roomtype_code+"/",
            success : function(response){
                var roomtype = response.data;

                if (roomtype.status == "SUCCESS"){
                    $scope.DATA.current_roomtype = roomtype;

                    $scope.DATA.total = [];
                    var total = 10;
                    for (var i=1; i<=total; i++) {
                        $scope.DATA.total.push(i);
                    }

                } else {
                    alert("Sorry, product not found..");
                }
            },
            error : function (response){}
        });
    }

    $scope.publishUnpublishRoomtype = function (roomtype, status) {
        var path = (status=='1')?"publish":"unpublish";

        httpSrv.get({
            url 	: "accommodation/room_type/"+path+"/"+roomtype,
            success : function(response){
                if (response.data.status = "SUCCESS"){
                    console.log(response.data.status);
                    $scope.loadDataRoomType();
                }
            },
            error : function (response){}
        });
    }

    $scope.addDataRoomtype = function (event) {
        $(event.target).find("button:submit").attr("disabled","disabled");

        var data = angular.copy($scope.DATA.current_roomtype);

        httpSrv.post({
            url 	: "accommodation/room_type/create",
            data	: $.param(data),
            success : function(response){
                if (response.data.status == "SUCCESS"){
                    toastr.success("Saved...");
                } else {
					$(event.target).find("button:submit").removeAttr("disabled");
					$('html, body').animate({scrollTop:200}, 400);
				}
            }
        });
    }

    $scope.editDataRoomtype = function (event, roomtype_code) {
        $(event.target).find("button:submit").attr("disabled","disabled");

        var data = angular.copy($scope.DATA.current_roomtype);

        httpSrv.post({
            url 	: "accommodation/room_type/update/"+roomtype_code+"/",
            data	: $.param(data),
            success : function(response){
                if (response.data.status == "SUCCESS"){
                    toastr.success("Update...");
                } else {
                    $(event.target).find("button:submit").removeAttr("disabled");
					$('html, body').animate({scrollTop:200}, 400);
                }
            }
        });
    }

    $scope.deleteRoomtype = function (roomtype) {
        httpSrv.get({
            url 	: "accommodation/room_type/delete/"+roomtype+"/",
            success : function(response){
                if (response.data.status = "SUCCESS"){
                    toastr.success("Deleted...");
                }
            },
            error : function (response){}
        });
    }

    $scope.loadBedtype = function () {
        httpSrv.get({
            url 	: "accommodation/room_type/bed_type",
            success : function(response){
                if (response.data.status = "SUCCESS"){
                    var bed_type = response.data;

                    if (response.data.status == "SUCCESS") {
                        $scope.DATA.bed_type = bed_type.bedtype;
                    } else {
                        alert("Sorry, room type not found..");
                    }
                }
            },
            error : function (response){}
        });
    }

    
});

function activate_sub_menu_agent_detail(class_menu){
	$("ul.nav.sub-nav.profile li").removeClass("active");
	$("ul.nav.sub-nav.profile li."+class_menu).addClass("active");
}
// JavaScript Document 
angular.module('app').controller('agent_controller',function($scope, $http, httpSrv, $stateParams, $interval, fn,$filter, $state){
	
	GeneralJS.activateLeftMenu("agent");	
	
	$scope.DATA = {};
	$scope.fn = fn;

	$scope.delCategory = function(agent_code,category_code){
		if (confirm('Are you sure delete this category?')) {
			httpSrv.post({
				url 	: "agent/delAgentCat",
				data	: $.param({"agent_code":agent_code, "category_code":category_code}),
				success : function(response){
					if (response.data.status == "SUCCESS") {
						$("#add-category").modal("hide");
						toastr.success("Delete Success.");
						$scope.loadDataAgentDetail();
					}else{
						toastr.warning(response.data.error_desc);
					}
					
				}
			});
		}
	}

	$scope.submitAgentCat = function(){
		
		var agent_code = $stateParams.agent_code;
		httpSrv.post({
			url 	: "agent/addAgentCat",
			data	: $.param({"agent_code":agent_code, "category_code":$scope.DATA.real_category}),
			success : function(response){
				if (response.data.status == "SUCCESS") {
					$("#add-category").modal("hide");
					toastr.success("Add Success.");
					$scope.loadDataAgentDetail();
				}else{
					toastr.warning(response.data.error_desc);
				}
				
			}
		});
	}

	$scope.ExcelExport= function (event) {

	    var input = event.target;
	    var reader = new FileReader();
	    var fileTypes = ['xls', 'xlsx'];
	    $scope.DATA.is_excell = false;
	    $scope.show_loading = true;
	    if (input.files && input.files[0]) {
			var extension = input.files[0].name.split('.').pop().toLowerCase(),  //file extension from input file
			isSuccess = fileTypes.indexOf(extension) > -1;  //is extension in acceptable types

			if (isSuccess) {
				reader.onload = function(){

					  $scope.show_loading = false;
				       

				    var fileData = reader.result;

				    var wb = XLSX.read(fileData, {type : 'binary'});

				    wb.SheetNames.forEach(function(sheetName){
				        var rowObj =XLSX.utils.sheet_to_row_object_array(wb.Sheets[sheetName]);
				        var jsonObj = JSON.stringify(rowObj);
				       
				        $scope.DATA.excell = jsonObj;
				        $scope.DATA.is_excell = true;
				        $scope.DATA.excell_data = rowObj;

				    })
				};
				reader.readAsBinaryString(input.files[0]);
			}else{
				toastr.warning("Please upload *xls or *xlsx file.");
			}
	    }
    };


    $scope.submitUploadExcell = function()
    {
    	if ($scope.DATA.is_excell) {
    		httpSrv.post({
	        	url : "agent/create_bacth",
	        	data: $.param({"agent":$scope.DATA.excell,"real_category_id":$scope.DATA.real_category}),
	        	success : function(response){
	        		$("#upload-excell").modal("hide");
	        		toastr.success("Upload Success.");
	        		$scope.loadDataAgent();
	        	}
	        })
    	}else{
    		toastr.warning("Please upload *xls or *xlsx file.");
    	}
    	
    }

	// update gung ari

	$scope.paymentDetail = function (current_payment, delete_payment){
		$scope.myPaymentDetail = angular.copy(current_payment);
		//$scope.myPaymentDetail.amount = Math.abs($scope.myPaymentDetail.amount);
		if (delete_payment == true){
			$scope.myPaymentDetail.delete_payment = true;
			$scope.myPaymentDetail.sure_to_delete = '0';
		}else{
			delete $scope.myPaymentDetail.delete_payment;
		}
	}
	
	$scope.saveDataDeposit = function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
			$scope.DATA.myDeposit.error_msg = [];
		
			httpSrv.post({
				url 	: "agent/"+(($scope.DATA.myDeposit.id)?"update_deposit":"create_deposit"),
				data	: $.param($scope.DATA.myDeposit),
				success : function(response){
					if (response.data.status == "SUCCESS"){
						$scope.loadDataDeposit();
						$("#add-edit-payment").modal("hide");
						toastr.success("Saved...");
					}else{
						$scope.DATA.myDeposit.error_msg = response.data.error_desc;
					}
					$(event.target).find("button:submit").removeAttr("disabled");
					//$.hideLoading();
				}
			});
	}

	$scope.addEditDeposit = function(current_deposit)
	{
		var agent_code = $stateParams.agent_code; 

		$(".datepicker").datepicker({dateFormat :"yy-mm-dd", maxDate:0});
		
		if (!$scope.$root.DATA_payment_method){
			httpSrv.post({
				url 	: "setting/payment_method/in",
				success : function(response){ $scope.$root.DATA_payment_method = response.data.payment_method; },
			});
		}
		
		if (current_deposit){
			$scope.DATA.myDeposit = angular.copy(current_deposit);
		}else{
			$scope.DATA.myDeposit = {};
			$scope.DATA.myDeposit.agent_code = agent_code;
			//$scope.DATA.myDeposit.currency = "IDR";
		}
	}
	
	$scope.changePaymentType = function(){
		
		$scope.DATA.payment_is_cc = false;
		$scope.DATA.payment_is_atm = false;

		for (index in $scope.$root.DATA_payment_method){
			var payment_method = $scope.$root.DATA_payment_method[index];
			if (payment_method.code == $scope.DATA.myDeposit.payment_type){
				if (payment_method.type == 'CC'){
					$scope.DATA.payment_is_cc = true;
					$scope.DATA.payment_is_atm = false;
				}else if (payment_method.type == 'TRANSFER'){
					$scope.DATA.payment_is_cc = false;
					$scope.DATA.payment_is_atm = true;
				}else{
					$scope.DATA.payment_is_cc = false;
					$scope.DATA.payment_is_atm = false;
				}
				break;
			}
		}
		
	}

	$scope.loadDataTransactionTransportDeposit = function(){
			
		var agent_code = $stateParams.agent_code; 
		
		$scope.search = {   "start_date" : "",
							"end_date" : "",
							 "agent_code": agent_code,
							 "payment_method": "DEPOSIT"}; 		
		
		var _search = $scope.search;
		
		httpSrv.post({
			url 	: "transport/transaction",
			data	: $.param({"search":_search}),
			success : function(response){
				
				$scope.DATA.transaction = response.data.transactions;

				$scope.used_diposit($scope.DATA.transaction);
			
			},
			error : function (response){}
		});

	}

	$scope.used_diposit = function(data){
		var total = 0;
		$scope.usd_diposit = 0;
		angular.forEach(data, function(row){
			total += parseInt(row.amount);
		});

		$scope.usd_diposit = total;
	}	
	$scope.loadDataDeposit = function(){
		
		$scope.err_deposit = [];
		$scope.show_loading = true;
		var agent_code = $stateParams.agent_code; 
		
		httpSrv.post({
			url 	: "agent/deposit",
			data	: $.param({"publish_status":"ALL", "agent_code": agent_code}),
			success : function(response){
				//$scope.DATA = response.data;
				$scope.show_loading = false;
				if (response.data.status = "SUCCESS") {
					$scope.DATA = response.data;	
				}
				else if(response.data.error_desc.length > 0){
					$scope.err_deposit = response.data.error_desc;
				}
				
			},
			error : function (response){}
		});

		/*httpSrv.post({
			url 	: "cash_and_bank",
			data	: $.param({"publish_status":"1"}),
			success : function(response){
				$scope.DATA.cnb = response.data.account;
			},
			error : function (response){}
		});*/
	}

	$scope.loadDataAgentDetail = function(agent_code, after_load_handler_function){
		
		if (!agent_code){ agent_code = $stateParams.agent_code; }
		
		httpSrv.get({
			url 	: "agent/detail/"+agent_code,
			success : function(response){
				var agent = response.data;
				
				if (agent.status == "SUCCESS"){
					$scope.DATA.current_agent = agent;
					
					if (typeof after_load_handler_function == 'function') {
						after_load_handler_function(agent);
					}

				
					if($scope.DATA.current_agent.payment_method.payment_code == 'DEPOSIT'){
						httpSrv.post({
							url 	: "agent/deposit",
							data	: $.param({"publish_status":"ALL", "agent_code": agent_code}),
							success : function(response){
								$scope.DATA.deposit = response.data.deposit;
							},
							error : function (response){}
						});
					}

				}else{
					alert("Sorry, agent not found..");
					window.location = "#/agent";
				}
			},
			error : function (response){}
		});
		$scope.dataSearch = {};
		$scope.dataSearch.search = {};
		$scope.dataSearch.search = {
			"agent_code": agent_code,
		};
		//$scope.dataSearch.search.agent_code = agent_code;

		// UnInvoicing
		httpSrv.post({
			url 	: "invoice/unInvoicing/",
			data	: $.param($scope.dataSearch),
			success : function(response){

				if (response.data.status == "SUCCESS"){
					$scope.DATA.uninvoicing = response.data.summary;
				}
			},
			error : function (response){}
		});

	}

	// $scope.loadDataAgentDetail = function(agent_code, after_load_handler_function){
		
	// 	if (!agent_code){ agent_code = $stateParams.agent_code; }
		
	// 	httpSrv.get({
	// 		url 	: "agent/detail/"+agent_code,
	// 		success : function(response){
	// 			var agent = response.data;
				
	// 			if (agent.status == "SUCCESS"){
	// 				$scope.DATA.current_agent = agent;
					
	// 				if (typeof after_load_handler_function == 'function') {
	// 					after_load_handler_function(agent);
	// 				}
	// 				if ($scope.DATA.current_agent.payment_method.payment_code == 'ACL') {
	// 					//Out Standing Invoice
	// 					httpSrv.post({
	// 						url 	: "agent/out_standing_invoice/",
	// 						data	: $.param({"agent_code":agent_code}),
	// 						success : function(response){
	// 							if (response.data.status == "SUCCESS"){
	// 								$scope.DATA.out_standing_invoice = response.data.out_standing_invoice;
	// 							}
	// 						},
	// 						error : function (response){}
	// 					});
	// 				}else if($scope.DATA.current_agent.payment_method.payment_code == 'DEPOSIT'){
	// 					httpSrv.post({
	// 						url 	: "agent/deposit",
	// 						data	: $.param({"publish_status":"ALL", "agent_code": agent_code}),
	// 						success : function(response){
	// 							$scope.DATA.deposit = response.data.deposit;
	// 						},
	// 						error : function (response){}
	// 					});
	// 				}
	// 			}else{
	// 				alert("Sorry, agent not found..");
	// 				window.location = "#/agent";
	// 			}
	// 		},
	// 		error : function (response){}
	// 	});

	// 	$scope.dataSearch = {};
	// 	$scope.dataSearch.search = {};
	// 	$scope.dataSearch.search = {
	// 		"agent_code": agent_code,
	// 	};
	// 	//$scope.dataSearch.search.agent_code = agent_code;

	// 	// UnInvoicing
	// 	httpSrv.post({
	// 		url 	: "invoice/unInvoicing/",
	// 		data	: $.param($scope.dataSearch),
	// 		success : function(response){

	// 			if (response.data.status == "SUCCESS"){
	// 				$scope.DATA.uninvoicing = response.data.uninvoicing;
	// 			}
	// 		},
	// 		error : function (response){}
	// 	});
	// }
	// update gung ari

	$scope.loadDataEmailBcc = function(){
		
		var agent_code = $stateParams.agent_code; 

		httpSrv.post({
			url 	: "setting/email_bcc/"+agent_code,
			data	: $.param({"publish_status":"ALL"}),
			success : function(response){
				$scope.DATA = response.data;
			},
			error : function (response){}
		});
	}


	$scope.emailDetailAddItem = function(){

		if (!$scope.DATA.email_bcc) {
			$scope.DATA.email_bcc = [];
		}
		

		var last_schedule_index = $scope.DATA.email_bcc.length;
		var allow_to_add = false;
		var schedule_detail = {	
								"email" : "",
								"name" : "", "agent_code": ""
							  };


		if (last_schedule_index == 0){
			allow_to_add = true;
		}else{
			var last_schedule = $scope.DATA.email_bcc[(last_schedule_index - 1)];
			
			if (last_schedule.email){
				allow_to_add = true;
				//schedule_detail.email = last_schedule.email;
			}else{
				alert("Please complete current email first...");
			}
		}
		
		if (allow_to_add){
			$scope.DATA.email_bcc.push(schedule_detail);
		}
	}

	$scope.scheduleDetailRemoveLastItem = function(){
		$scope.DATA.email_bcc.pop();
	}

	$scope.saveDataSchedule = function(event){
		//$.showLoading();
		$(event.target).find("input:text").attr("disabled","disabled");
		//$(event.target).find("input:email").attr("disabled","disabled");
		

		// $scope.DATA.email_bcc = [];
		
		var email_bcc = $scope.DATA.email_bcc;
		
		email_bcc.schedule_detail = [];
		
		for (i=0; i<email_bcc.length; i++){
			var _cs_detail = email_bcc[i];
			var agent_code = $stateParams.agent_code;
			var _schedule_detail = {
										"email"	:_cs_detail.email, "name"	:_cs_detail.name, 'agent_code': agent_code
									};
			email_bcc.schedule_detail.push(_schedule_detail);
		}

		httpSrv.post({
			url 	: "setting/addEmailBcc",
			data	: $.param({'email':email_bcc.schedule_detail}),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					// window.location = "#/transport/trips_schedule_detail/"+response.data.schedule_code;
					
					toastr.success("Saved...");
				}else{
					$scope.DATA.email_bcc.error_desc = response.data.error_desc;
					
					$('html, body').animate({scrollTop:200}, 400);
				}
				//$.hideLoading();
			}
		});
	}

	$scope.sendEmailAgentAcc = function(agent_code){

		$scope.account_error_msg = [];
		
		httpSrv.post({
			url : "agent/send_account_information",
			data : $.param({"agent_code":agent_code,"password":$scope.random_pwd, "password_2":$scope.random_pwd2}),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					toastr.success("Email Sent...");
					$(".modal-backdrop.fade.in").hide();
					$(".modal.fade.in").hide();
					//$(event.target).find("button:submit").hide();
				}else{
					$scope.account_error_msg = response.data.error_desc;
				}
			},
			error : function(err){

			}
		})
	}

	$scope.loadDataAccount = function(agent_code){

		$scope.random_pwd = $scope.makeid();
		$scope.random_pwd2 = $scope.random_pwd;
	}

	$scope.makeid = function() {
	  var text = "";
	  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	  for (var i = 0; i < 6; i++)
	    text += possible.charAt(Math.floor(Math.random() * possible.length));

	  return text;
	}

	
	$scope.loadDataAgent = function(page, show_loading){
		if (!$scope.DATA.bookings || show_loading){
			$scope.show_loading_DATA_bookings = true;
		}
		
		if (!$scope.search){
			$scope.search = {};
			$scope.search.publish_status = "1";
		}
		
		if (!page) page = 1;
		
		var _search = $scope.search;
			_search.page = page;
		
		httpSrv.post({
			url 	: "agent",
			data	: $.param({"search":_search}),
			success : function(response){
				$scope.show_loading_DATA_bookings = false;
				$scope.DATA.agents = response.data;
				
				if ($scope.DATA.agents.search){
					$scope.DATA.agents.search.pagination = [];
					for (i=1;i<=$scope.DATA.agents.search.number_of_pages; i++){
						$scope.DATA.agents.search.pagination.push(i);
					}
				}
			},
			error : function (response){}
		});
		
		if (!$scope.$root.DATA_real_categories){
			httpSrv.get({
				url 	: "agent/real_category",
				success : function(response){ $scope.$root.DATA_real_categories = response.data.real_categories; }
			});
		}
		
	}
	
	$scope.publishUnpublishAgent = function (agent, status){
		
		if (confirm("Click OK to continue...")){
			var path = (status=='1')?"publish":"unpublish";
			agent.publish_status = status;
			
			httpSrv.get({
				url 	: "agent/"+path+"/"+agent.agent_code,
				success : function(response){
					if (response.data.status != "SUCCESS"){
						agent.publish_status = !status;
					}
				},
				error : function (response){}
			});
		}
	}
	
	
	$scope.agentAddEdit = function (){
		var agent_code = $stateParams.agent_code;
		
		httpSrv.get({
			url 	: "general/country_list",
			success : function(response){
				$scope.DATA.country_list = response.data;
			},
			error : function (response){}
		});
		httpSrv.get({
			url 	: "agent/real_category",
			success : function(response){
				$scope.DATA.real_categories = response.data;
			},
			error : function (response){}
		});
		
		if (agent_code){
			$scope.loadDataAgentDetail(agent_code, function(agent_detail){
				agent_detail.country_code = agent_detail.country.code;
				$scope.DATA.current_agent = agent_detail;
				if (agent_detail.real_category && agent_detail.real_category.real_category_code){
					$scope.DATA.current_agent.real_category_code = agent_detail.real_category.real_category_code;
				}else{
					$scope.DATA.current_agent.real_category_code = "";
				}
				
				if (agent_detail.category){
					$scope.DATA.current_agent.category_code = agent_detail.category.category_code;
				}else{
					$scope.DATA.current_agent.category_code = "";
				}
				
				$scope.DATA.current_agent.payment_method_code = agent_detail.payment_method.payment_code;
				
				$scope.DATA.current_agent.ready = true;
			});
		}else{
			$scope.DATA.current_agent = {};
			$scope.DATA.current_agent.country_code = "ID";
			$scope.DATA.current_agent.category_code = "";
			$scope.DATA.current_agent.ready = true;
		}
	}

	$scope.saveDataAgent = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.current_agent.error_desc = [];
		
		var data = angular.copy($scope.DATA.current_agent);
		data.payment_method = data.payment_method_code;
		delete data.real_category; delete data.real_categories;
		
		httpSrv.post({
			url 	: "agent/"+(($scope.DATA.current_agent.id)?"update":"create"),
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					window.location = "#/agent/detail/"+response.data.agent_code;
					toastr.success("Saved...");
				}else{
					$scope.DATA.current_agent.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
					$('html, body').animate({scrollTop:200}, 400);
				}
				//$.hideLoading();
			}
		});
	}
	
	$scope.loadDataBookingTransport = function(page){
		$scope.show_loading_DATA_bookings = true;
		var agent_code = $stateParams.agent_code;
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		
		if (!$scope.search_booking){ 
			var now = new Date(),
				start_date = new Date(),
				end_date = new Date();
			
			start_date.setDate(start_date.getDate() - 30);
			
			$scope.search_booking = { "start_date" : $scope.fn.formatDate(start_date, "yy-mm-dd"),
									  "end_date" : $scope.fn.formatDate(end_date, "yy-mm-dd"),
									  "filter_by":"BOOKINGDATE"}; 
		}
		
		if (!page) page = 1;
		
		var _search = $scope.search_booking;
			_search.page = page;
			_search.agent_code = agent_code;
			
		httpSrv.post({
			url 	: "booking/booking_and_voucher_list",
			data	: $.param({"search":_search}),
			success : function(response){
				$scope.show_loading_DATA_bookings = false;
				$scope.DATA.bookings = response.data;
				
				if ($scope.DATA.bookings.search_booking){
					$scope.DATA.bookings.search_booking.pagination = [];
					for (i=1;i<=$scope.DATA.bookings.search_booking.number_of_pages; i++){
						$scope.DATA.bookings.search_booking.pagination.push(i);
					}
				}
			},
			error : function (response){}
		});
		
		//Load Data Booking Source
		if (!$scope.$root.DATA_booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.$root.DATA_booking_source = response.data.booking_source; },
			});
		}
		
		//Load Data Booking Status
		if (!$scope.$root.DATA_booking_status){
			httpSrv.post({
				url 	: "setting/booking_status",
				success : function(response){ $scope.$root.DATA_booking_status = response.data.booking_status; },
			});
		}
	}
	
	$scope.loadDataTransactionTransport = function(page){
		$scope.show_loading_DATA_bookings = true;
		var agent_code = $stateParams.agent_code;
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		
		if (!$scope.search){ 
			var now = new Date(),
				start_date = new Date(),
				end_date = new Date();
			
			start_date.setDate(start_date.getDate() - 30);
			
			$scope.search = { "start_date" : $scope.fn.formatDate(start_date, "yy-mm-dd"),
							  "end_date" : $scope.fn.formatDate(end_date, "yy-mm-dd"),}; 
		}
		
		if (!page) page = 1;
		
		var _search = $scope.search;
			_search.page = page;
			_search.agent_code = agent_code;
		
		httpSrv.post({
			url 	: "transport/transaction",
			data	: $.param({"search":_search}),
			success : function(response){
				$scope.show_loading_DATA_bookings = false;
				$scope.DATA.transaction = response.data;
				
				if ($scope.DATA.transaction.search){
					$scope.DATA.transaction.search.pagination = [];
					for (i=1;i<=$scope.DATA.transaction.search.number_of_pages; i++){
						$scope.DATA.transaction.search.pagination.push(i);
					}
				}
			},
			error : function (response){}
		});
		
		//Load Data Booking Source
		if (!$scope.$root.DATA_booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.$root.DATA_booking_source = response.data.booking_source; },
			});
		}
		
		//Load Data Booking Status
		if (!$scope.$root.DATA_booking_status){
			httpSrv.post({
				url 	: "setting/booking_status",
				success : function(response){ $scope.$root.DATA_booking_status = response.data.booking_status; },
			});
		}
		
		//Load Data Payment Method
		if (!$scope.$root.DATA_payment_method){
			httpSrv.post({
				url 	: "setting/payment_method",
				success : function(response){ $scope.$root.DATA_payment_method = response.data.payment_method; },
			});
		}
		
		//Out Standing Invoice
		httpSrv.post({
			url 	: "agent/out_standing_invoice/",
			data	: $.param({"agent_code":agent_code}),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.DATA.out_standing_invoice = response.data.out_standing_invoice;
				}
			},
			error : function (response){}
		});

		//load summary 
		httpSrv.post({
			url 	: "transport/transaction/transactions_summary",
			data	: $.param({"search":_search}),
			success : function(response){
				if (response.data.status == 'SUCCESS') {
					$scope.DATA.summary = response.data.summary;
				}else{
					$scope.DATA.summary = {};
				}
				
			},
			error : function (response){}
		});
	}

	// $scope.loadDataInvoice = function(page){
	// 	$scope.show_loading_DATA_bookings = true;
	// 	$scope.agent_code = $stateParams.agent_code;
	// 	if (!page) page = 1;
	// 	var myDate = new Date();
	// 	var nextDay = new Date(myDate);
	// 	$scope.DATA.invoice = false;
	// 	nextDay.setDate(myDate.getDate());
	// 	httpSrv.post({
	// 		url 	: "invoice",
	// 		data	: $.param({"page":page,"agent_code":$scope.agent_code}),
	// 		success : function(response){
	// 			$scope.DATA.invoice = response.data;
	// 			if ($scope.DATA.invoice.search){
	// 				$scope.DATA.invoice.search.pagination = [];
	// 				for (i=1;i<=$scope.DATA.invoice.search.number_of_pages; i++){
	// 					$scope.DATA.invoice.search.pagination.push(i);
	// 				}
	// 			}
	// 			$scope.DATA.invoice.now = $filter('date')(nextDay, "yyyy-MM-dd");
	// 		},
	// 		error : function (response){}
	// 	});

	// 	// Out Standing Invoice
	// 	httpSrv.post({
	// 		url 	: "agent/out_standing_invoice/",
	// 		data	: $.param({"agent_code":$scope.agent_code}),
	// 		success : function(response){
	// 			if (response.data.status == "SUCCESS"){
	// 				$scope.DATA.out_standing_invoice = response.data.out_standing_invoice;
	// 			}
	// 		},
	// 		error : function (response){}
	// 	});

	// 	$scope.dataSearch = {};
	// 	$scope.dataSearch.search = {};
	// 	$scope.dataSearch.search = {
	// 		"agent_code": $scope.agent_code,
	// 	};

	// 	// UnInvoicing
	// 	httpSrv.post({
	// 		url 	: "invoice/unInvoicing/",
	// 		data	: $.param($scope.dataSearch),
	// 		success : function(response){

	// 			if (response.data.status == "SUCCESS"){
	// 				$scope.DATA.uninvoicing = response.data.uninvoicing;
	// 			}
	// 		},
	// 		error : function (response){}
	// 	});
	// }

	$scope.loadDataInvoice = function(page){
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		$scope.agent_data = {};

		$scope.show_loading_DATA_bookings = true;
		$scope.agent_code = $stateParams.agent_code;

		if (!page) page = 1;
		var myDate = new Date();
		var nextDay = new Date(myDate);
		$scope.DATA.invoice = false;
		nextDay.setDate(myDate.getDate());

		if (!$scope.search) $scope.search = {};
		var _search = $scope.search;
			_search.page = page;
		

		var data_search = {"search":_search};
		
		data_search.agent_code = $scope.agent_code;

		httpSrv.post({
			url 	: "invoice",
			data	: $.param(data_search),
			success : function(response){
				$scope.DATA.invoice = response.data;
				if ($scope.DATA.invoice.search){
					$scope.DATA.invoice.search.pagination = [];
					for (i=1;i<=$scope.DATA.invoice.search.number_of_pages; i++){
						$scope.DATA.invoice.search.pagination.push(i);
					}
				}
				$scope.DATA.invoice.now = $filter('date')(nextDay, "yyyy-MM-dd");
			},
			error : function (response){}
		});

		$scope.dataSearch = {};
		$scope.dataSearch.search = {};
		$scope.dataSearch.search = {
			"agent_code": $scope.agent_code,
		};

		// UnInvoicing
		httpSrv.post({
			url 	: "invoice/unInvoicing/",
			data	: $.param($scope.dataSearch),
			success : function(response){

				if (response.data.status == "SUCCESS"){
					$scope.DATA.uninvoicing = response.data.summary;
				}
			},
			error : function (response){}
		});
	}
	
	$scope.loadDataAgentContracRatesAssign = function(){
		var agent_code = $stateParams.agent_code;
		httpSrv.post({
			url 	: "agent/contract_rates_assign/"+agent_code,
			success : function(response){
				$scope.DATA.contract_rates_assign = response.data;
			},
			error : function (response){}
		});
	}
	$scope.selectContractRatesGetDetail = function(){ //REFF : ACT
		var category_code = $scope.DATA.myContractRates.category_code;
		
		if (!$scope.DATA.myContractRates.percentage_original){
			$scope.DATA.myContractRates.percentage_original = angular.copy($scope.DATA.myContractRates.percentage);
		}
		
		delete $scope.DATA.myContractRates.contract_rates;
		
		httpSrv.post({
			url 	: "agent/detail_category/"+category_code,
			success : function(response){
				if (response.data.contract_rates || response.data.contract_rates_act){
					$scope.DATA.myContractRates.contract_rates = response.data.contract_rates;
					$scope.DATA.myContractRates.contract_rates_act = response.data.contract_rates_act;
				}
			},
			error : function (response){}
		});
	}
	$scope.myContractRatesChangePercentage = function(){ //REFF : ACT
		if ($scope.DATA.myContractRates.contract_rate_type == 'PERCENT'){
			var percentage_original = $scope.DATA.myContractRates.percentage_original;
			
			//RATE TRANS
			for (x in $scope.DATA.myContractRates.contract_rates){
				
				if ($scope.DATA.myContractRates.contract_rates[x].percentage == percentage_original){
					$scope.DATA.myContractRates.contract_rates[x].percentage = $scope.DATA.myContractRates.percentage;
				}
				
				var percentage	= $scope.DATA.myContractRates.contract_rates[x].percentage;
				
				var nett	= {};
				var _rate 	= $scope.DATA.myContractRates.contract_rates[x].rates;
				
				if (_rate && _rate.one_way_rates){
					nett.one_way_rates = {};
					nett.one_way_rates.rates_1 = _rate.one_way_rates.rates_1 - (_rate.one_way_rates.rates_1 * percentage/100);
					nett.one_way_rates.rates_2 = _rate.one_way_rates.rates_2 - (_rate.one_way_rates.rates_2 * percentage/100);
					nett.one_way_rates.rates_3 = _rate.one_way_rates.rates_3 - (_rate.one_way_rates.rates_3 * percentage/100);
				}
				if (_rate && _rate.return_rates){
					nett.return_rates = {};
					nett.return_rates.rates_1 = _rate.return_rates.rates_1 - (_rate.return_rates.rates_1 * percentage/100);
					nett.return_rates.rates_2 = _rate.return_rates.rates_2 - (_rate.return_rates.rates_2 * percentage/100);
					nett.return_rates.rates_3 = _rate.return_rates.rates_3 - (_rate.return_rates.rates_3 * percentage/100);
				}
				
				$scope.DATA.myContractRates.contract_rates[x].nett_rates = nett;
			}
			
			//RATE ACT
			for (x in $scope.DATA.myContractRates.contract_rates_act){
				
				if ($scope.DATA.myContractRates.contract_rates_act[x].percentage == percentage_original){
					$scope.DATA.myContractRates.contract_rates_act[x].percentage = $scope.DATA.myContractRates.percentage;
				}
				
				var percentage	= $scope.DATA.myContractRates.contract_rates_act[x].percentage;
				
				var nett	= {};
				var _rate 	= $scope.DATA.myContractRates.contract_rates_act[x].rates;
				
				nett = {};
				nett.rates_1 = _rate.rates_1 - (_rate.rates_1 * percentage/100);
				nett.rates_2 = _rate.rates_2 - (_rate.rates_2 * percentage/100);
				nett.rates_3 = _rate.rates_3 - (_rate.rates_3 * percentage/100);

				$scope.DATA.myContractRates.contract_rates_act[x].nett_rates = nett;
			}
			
			$scope.DATA.myContractRates.percentage_original = angular.copy($scope.DATA.myContractRates.percentage);

		}
	}
	$scope.assignAgentContractRates = function(contract_rates, edit){
		//REFF : ACT
		$scope.edit_contract = 0;
		if (edit) {
			$scope.edit_contract = 1;
		}
		if (contract_rates){
			
			$scope.DATA.myContractRates = angular.copy(contract_rates);
			$scope.DATA.myContractRates.is_edit = true;
			
			var agent_code = $stateParams.agent_code;
			var param = {"agent_code" : agent_code, "id" : $scope.DATA.myContractRates.id};
			delete $scope.DATA.agent_contract_rates;
			
			if (contract_rates.real_category_code){
				//Get Contract Rates in Real Category
				$scope.DATA.myContractRates.show_loading = true;
				$scope.loadDataContractRatesDetail(contract_rates, function(contract_rates_detail){
					$scope.DATA.myContractRates = contract_rates_detail;
					$scope.DATA.myContractRates.real_category = contract_rates.real_category;
					$scope.DATA.myContractRates.is_edit = true;;
				});
			}else{
				//Old Contract Rates
				httpSrv.post({
					url 	: "agent/assign_contract_rates_detail/",
					data	: $.param(param),
					success : function(response){
						if (response.data.contract_rates){
							$scope.DATA.myContractRates.contract_rates = response.data.contract_rates;
						}
						if (response.data.contract_rates_act){
							$scope.DATA.myContractRates.contract_rates_act = response.data.contract_rates_act;
						}
						//console.log($scope.DATA.myContractRates);
					},
					error : function (response){}
				});
			}
			
		}else{
			delete $scope.DATA.myContractRates;
			
			if (!$scope.DATA.agent_contract_rates){
				httpSrv.post({
					url 	: "agent/category",
					data	: $.param({"publish_status":"1"}),
					success : function(response){
						$scope.DATA.agent_contract_rates = response.data.categories;
					},
					error : function (response){}
				});
			}
		}
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
	}
	$scope.saveAssignAgentContractRates = function(event){ //REFF : ACT
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		
		if (!$scope.DATA.myContractRates) $scope.DATA.myContractRates = {};
		$scope.DATA.myContractRates.error_msg = [];
		
		var agent_code = $stateParams.agent_code;
		var param = {"agent_code" 			: agent_code,
					 "contact_rates_code" 	: $scope.DATA.myContractRates.category_code,
					 "start_date"			: $scope.DATA.myContractRates.start_date,
					 "end_date"				: $scope.DATA.myContractRates.end_date,
					 "name"					: $scope.DATA.myContractRates.name,
					 "description"			: $scope.DATA.myContractRates.description,
					 "agent_payment_type"	: $scope.DATA.myContractRates.agent_payment_type,};
		
		if ($scope.DATA.myContractRates.is_edit){
			param.id = $scope.DATA.myContractRates.id;
		}
		
		if ($scope.DATA.myContractRates.contract_rate_type == "PERCENT"){
			param.percentage = $scope.DATA.myContractRates.percentage;
		}
		//RATES TRANS
		if ($scope.DATA.myContractRates.contract_rates){
			param.rates = [];
			for (x in $scope.DATA.myContractRates.contract_rates){
				var _rates = {};
				_rates.rates_code = $scope.DATA.myContractRates.contract_rates[x].rates_code;
				if ($scope.DATA.myContractRates.contract_rates[x].percentage){
					_rates.percentage = $scope.DATA.myContractRates.contract_rates[x].percentage;
				}
				param.rates.push(_rates);
			}
		}
		//--
		//RATES ACT
		if ($scope.DATA.myContractRates.contract_rates_act){
			param.rates_act = [];
			for (x in $scope.DATA.myContractRates.contract_rates_act){
				var _rates = {};
				_rates.rates_code = $scope.DATA.myContractRates.contract_rates_act[x].rates_code;
				if ($scope.DATA.myContractRates.contract_rates_act[x].percentage){
					_rates.percentage = $scope.DATA.myContractRates.contract_rates_act[x].percentage;
				}
				param.rates_act.push(_rates);
			}
		}
		//--

		//$scope.DATA.PARAM_ = param;
		//console.log(param);
		//console.log($scope.DATA.myContractRates);
		//return;
		
		httpSrv.post({
			url 	: ($scope.DATA.myContractRates.is_edit?"agent/edit_assign_contract_rates_to_agent":"agent/assign_contract_rates_to_agent"),
			data	: $.param(param),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataAgentContracRatesAssign();
					$("#add-edit-contract-rates").modal("hide");
					toastr.success("Saved...");
				}else{
					$scope.DATA.myContractRates.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	$scope.deleteAssignAgentContractRates = function(category){
		//$.showLoading();
		
		if (confirm("Click OK to continue delete this contract rates...")){
			
			var agent_code = $stateParams.agent_code;
			
		
			if (category.real_category_code) {
				var param = {"agent_code" 	: agent_code,
					 		 "id" 			: category.id,
					 		 "real_category_code" : category.real_category_code};
				
			}else{
				var param = {"agent_code" 	: agent_code,
					 		 "id" 			: category.id};
			}
			
			httpSrv.post({
				url 	: "agent/delete_assign_contract_rates_to_agent",
				data	: $.param(param),
				success : function(response){
					if (response.data.status == "SUCCESS"){
						$scope.loadDataAgentContracRatesAssign();
						toastr.success("Deleted...");
					}else{
						alert(response.data.error_desc[0]);
					}
					//$.hideLoading();
				}
			});
		}
	}
	
	
	$scope.loadDataBookingTransportxxxxxxxxxx = function(){
		var agent_code = $stateParams.agent_code;
		var data = {"search":{
						"agent_code" : agent_code
					}};
		httpSrv.post({
			url 	: "transport/booking/transaction",
			data	: $.param(data),
			success : function(response){
				$scope.DATA.bookings = response.data;
			},
			error : function (response){}
		});
		
		httpSrv.post({
			url 	: "agent/out_standing_invoice/",
			data	: $.param({"agent_code":agent_code}),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.DATA.out_standing_invoice = response.data.out_standing_invoice;
				}
			},
			error : function (response){}
		});
	}

	//----------------------

	$scope.loadDataCategory = function(){
		httpSrv.post({
			url 	: "agent/category",
			data	: $.param({"publish_status":"all"}),
			success : function(response){
				$scope.DATA.categories = response.data;
			},
			error : function (response){}
		});
	}
	$scope.loadDataCategoryDetail = function(){ //REFF : ACT
		var category_code = $stateParams.category_code;
		var selected_agent_rates = {};
		
		$scope.contract_rates_edit_mode = false;

		httpSrv.post({
			url 	: "agent/detail_category/"+category_code,
			success : function(response){
				$scope.DATA.category_detail = response.data;

				//RATES TRANS
				$scope.DATA.contract_rates = {};
				for (x in response.data.contract_rates){
					var _rates = response.data.contract_rates[x];
					$scope.DATA.contract_rates[_rates.id] = _rates;
				}
				//--
				//RATES ACT
				$scope.DATA.contract_rates_act = {};
				for (x in response.data.contract_rates_act){
					var _rates = response.data.contract_rates_act[x];
					$scope.DATA.contract_rates_act[_rates.id] = _rates;
				}
				//--
				
				//Load Data Schedule & Rates
				httpSrv.post({
					url 	: "transport/schedule",
					success : function(response){
						$scope.DATA.schedules = response.data;
						$scope.DATA.rates = {};
						
						for (x in $scope.DATA.category_detail["contract_rates"]){
							var _rates = $scope.DATA.category_detail["contract_rates"][x];
							
							if (!$scope.DATA.rates[_rates.schedule_id]) $scope.DATA.rates[_rates.schedule_id] = {"rates":[]};
							
							$scope.DATA.rates[_rates.schedule_id].rates.push(_rates);
							
						}
					},
					error : function (response){}
				});
				//--
				
				//Load Data Product & Rates (ACT)
				httpSrv.post({
					url 	: "activities/product",
					success : function(response){
						$scope.DATA.products = response.data;
						$scope.DATA.rates_act = {};
						
						for (x in $scope.DATA.category_detail["contract_rates_act"]){
							var _rates = $scope.DATA.category_detail["contract_rates_act"][x];
							
							if (!$scope.DATA.rates_act[_rates.product_id]) $scope.DATA.rates_act[_rates.product_id] = {"rates":[]};
							
							$scope.DATA.rates_act[_rates.product_id].rates.push(_rates);
							
						}
					},
					error : function (response){}
				});
				//--
			
			},
			error : function (response){}
		});
		
	}
	$scope.loadCategoryDetailAddMasterRates = function(){ //REFF : ACT
		var category_code = $stateParams.category_code;
		var selected_agent_rates = {};
		
		$scope.contract_rates_edit_mode = true;

		httpSrv.post({
			url 	: "agent/detail_category/"+category_code,
			success : function(response){
				$scope.DATA.category_detail = response.data;
				
				//RATES TRANSPORT
				$scope.DATA.contract_rates = {};
				for (x in response.data.contract_rates){
					var _rates = response.data.contract_rates[x];
					$scope.DATA.contract_rates[_rates.id] = _rates;
				}
				//--
				
				//RATES ACTIVITIES
				$scope.DATA.contract_rates_act = {};
				for (x in response.data.contract_rates_act){
					var _rates = response.data.contract_rates_act[x];
					$scope.DATA.contract_rates_act[_rates.id] = _rates;
				}
				//--
				
				/*httpSrv.post({
					url 	: "agent/get_contract_rates/"+$scope.DATA.category_detail.id,
					success : function(response){
						$scope.DATA.contract_rates = {};
						
						for (x in response.data.contract_rates){
							var id_rt = response.data.contract_rates[x];
							$scope.DATA.contract_rates[id_rt] = true;
						}
						
					},
					error : function (response){}
				});*/

			},
			error : function (response){}
		});
		
		//Load Data Schedule & Rates
		httpSrv.post({
			url 	: "transport/schedule",
			success : function(response){
				$scope.DATA.schedules = response.data;
				$scope.DATA.rates = {};

				//Get Rates for each schedule
				if (response.data.status == "SUCCESS"){
					for (j in $scope.DATA.schedules.schedules){
						var schedules = angular.copy($scope.DATA.schedules.schedules[j]);
						
						httpSrv.post({
							url 	: "transport/schedule/rates/"+schedules.schedule_code,
							success : function(response2){

								if (response2.data.status == "SUCCESS"){
									//$scope.DATA.schedules.schedules[j].rates = [];
									//$scope.DATA.schedules.schedules[j].rates = response2.data.rates;
									
									$scope.DATA.rates[response2.data.rates[0].schedule_id]  =response2.data.rates;
								}
							},
							error : function (response){}
						});
					}
				}
			},
			error : function (response){}
		});
		//--
		//Load Data Product & Rates (ACT)
		httpSrv.post({
			url 	: "activities/product",
			success : function(response){
				$scope.DATA.products = response.data;
				$scope.DATA.rates_act = {};
				
				//Get Rates for each products
				if (response.data.status == "SUCCESS"){
					for (k in $scope.DATA.products.products){
						var products = angular.copy($scope.DATA.products.products[k]);
						
						httpSrv.post({
							url 	: "activities/rates/product/"+products.product_code,
							success : function(response2){
								if (response2.data.status == "SUCCESS"){
									$scope.DATA.rates_act[response2.data.rates[0].product_id] = response2.data.rates;
								}
							},
							error : function (response){}
						});
					}
				}
			},
			error : function (response){}
		});
		//--
	}
	$scope.saveSelectedRatesToCategory = function (rates, crs_type){ //REFF : ACT
		var data = {};
			data.category_code 	= $scope.DATA.category_detail.category_code;
			data.rates_code		= rates.rates_code;
		
		if (rates.custom_percentage){
			data.custom_percentage = rates.custom_percentage;
		}
		if (crs_type){
			data.crs_type = crs_type;
		}
		
		httpSrv.post({
			url 	: "agent/save_selected_rates_to_category",
			data	: $.param(data),
			success : function(response){
				rates.selected_agent_rates = '1';
				toastr.success("Saved...");
			},
			error : function (response){}
		});
	}
	$scope.removeSelectedRatesToCategory = function (rates, crs_type){ //REFF : ACT
		
		if (confirm("Click OK to continue...")){
			
			var data = {};
				data.category_code 	= $scope.DATA.category_detail.category_code;
				data.rates_code		= rates.rates_code;

			if (crs_type){
				data.crs_type = crs_type;
			}
		
			httpSrv.post({
				url 	: "agent/remove_selected_rates_to_category",
				data	: $.param(data),
				success : function(response){
					delete rates.selected_agent_rates;
					if (crs_type == "ACT"){
						delete $scope.DATA.contract_rates_act[rates.id];
					}else{
						delete $scope.DATA.contract_rates[rates.id];
					}
					toastr.success("Deleted...");
				},
				error : function (response){}
			});
			
		}
	}
	$scope.publishUnpublishCategory = function (category, status){
		
		var path = (status=='1')?"publish_category":"unpublish_category";
		category.publish_status = status;
		
		httpSrv.get({
			url 	: "agent/"+path+"/"+category.category_code,
			success : function(response){
				if (response.data.status != "SUCCESS"){
					alert(response.data.error_desc[0]);
					category.publish_status = !status;
				}
			},
			error : function (response){}
		});
	}
	$scope.addEditCategory = function (current_category){
		//console.log(current_boat);
		if (current_category){
			$scope.DATA.myCategory = angular.copy(current_category);
		}else{
			$scope.DATA.myCategory = {};
			$scope.DATA.myCategory.contract_rate_type = 'PERCENT';
			$scope.DATA.myCategory.percentage = 0;
		}
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
	}
	$scope.saveDataCategory = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myCategory.error_msg = [];
		
		httpSrv.post({
			url 	: "agent/"+(($scope.DATA.myCategory.id)?"update_category":"create_category"),
			data	: $.param($scope.DATA.myCategory),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$("#add-edit-category").modal("toggle");
					toastr.success("Saved...");
					
					if ($scope.DATA.myCategory.id){
						$scope.loadDataCategory();
					}else{
						$(".modal-backdrop.fade.in").hide();
						window.location = "#/agent/category/detail/"+response.data.category_code;
					}
				}else{
					$scope.DATA.myCategory.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	$scope.saveDataContractRatesCategory = function(event){
		///XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		//$scope.DATA.myCategory.error_msg = [];
		
		var rates = $scope.DATA.rates;
		var data_rates_code = [];
		
		for (id in rates){
			for (i in rates[id].rates){
				var _rt = rates[id].rates[i];
				if (_rt.selected_agent_rates){
					data_rates_code.push(_rt.id);
				}
			}
		}
		
		var data = {"category_id" 	: $scope.DATA.category_detail.id,
					"rates_id"		: data_rates_code};		
		
		httpSrv.post({
			url 	: "agent/save_agent_contract_rates",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					toastr.success("Saved...");
					$scope.loadDataCategoryDetail();
					//$(event.target).find("button:submit").hide();
				}else{
					$scope.DATA.myCategory.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	
	//COMMISSION
	$scope.loadDataCommission = function(){
		
		$scope.err_deposit = [];
		$scope.show_loading = true;
		var agent_code = $stateParams.agent_code; 
		var _search = {"search" : {"agent_code": agent_code}};
		
		httpSrv.post({
			url 	: "commission",
			data	: $.param(_search),
			success : function(response){
				//$scope.DATA = response.data;
				$scope.show_loading = false;
				if (response.data.status = "SUCCESS") {
					$scope.DATA = response.data;	
				}
				else if(response.data.error_desc.length > 0){
					$scope.err_commission = response.data.error_desc;
				}
				
			},
			error : function (response){}
		});

	}
	$scope.editCommission = function(commission){
		$scope.DATA.myCommission = commission;
		//$scope.DATA.Commission.approval_status = "1";
		
		//Load Data Payment Method
		if (!$scope.$root.DATA_payment_method_out){
			httpSrv.post({
				url 	: "setting/payment_method/out",
				success : function(response){ $scope.$root.DATA_payment_method_out = response.data.payment_method; },
			});
		}
	}
	$scope.saveCommission = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myCommission.error_desc = [];
		
		var data = {"commission_code" 		: $scope.DATA.myCommission.commission_code,
					"payment_method_code"	: $scope.DATA.myCommission.payment_method_code}
					
		httpSrv.post({
			url 	: "commission/update",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataCommission();
					$("#add-edit-commission").modal("hide");
					toastr.success("Saved...");
				}else{
					$scope.DATA.myCommission.error_desc = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	//--
	
	//REAL CATEGORY
	//Category
	$scope.loadDataRealCategory = function(){
		httpSrv.post({
			url 	: "agent/real_category",
			data	: $.param({"publish_status":"all"}),
			success : function(response){
				$scope.DATA.real_categories = response.data;
				
			},
			error : function (response){}
		});
	}
	$scope.publishUnpublishRealCategory = function (category, status){
		
		var path = (status=='1')?"publish_real_category":"unpublish_real_category";
		category.publish_status = status;
		
		httpSrv.get({
			url 	: "agent/"+path+"/"+category.real_category_code,
			success : function(response){
				if (response.data.status != "SUCCESS"){
					alert(response.data.error_desc[0]);
					category.publish_status = !status;
				}
			},
			error : function (response){}
		});
	}
	$scope.addEditRealCategory = function (current_category){
		//console.log(current_boat);
		if (current_category){
			$scope.DATA.myCategory = angular.copy(current_category);
		}else{
			$scope.DATA.myCategory = {};
		}
	}
	$scope.saveDataRealCategory = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myCategory.error_msg = [];
		
		httpSrv.post({
			url 	: "agent/"+(($scope.DATA.myCategory.id)?"update_real_category":"create_real_category"),
			data	: $.param($scope.DATA.myCategory),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$("#add-edit-category").modal("toggle");
					toastr.success("Saved...");
					$scope.loadDataRealCategory();
					$(".modal-backdrop.fade.in").hide();
				}else{
					$scope.DATA.myCategory.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}

	$scope.deleteRealCategory = function (category){
		if (confirm("Are you sure to remove this rates category?")){
			
			var data = {};
			data.real_category_code = category.real_category_code;
			
			httpSrv.post({
				url 	: "agent_category/delete_real_category",
				data	: $.param(data),
				success : function(response){
					if (response.data.status == "SUCCESS"){
						$scope.loadDataRealCategory();
						toastr.success("Deleted...");
					}
				},
				error : function (response){}
			});
		}
	}

	$scope.saveReorderRealCategory = function(){ 
		var form_data = $("#sortable-rates .real_category_codes").serializeArray(); 
		var real_category_codes = []; 
 
		for(_i in form_data){ 
			if (form_data[_i].name = "real_category_codes[]"){ 
				real_category_codes.push(form_data[_i].value); 
			} 
		} 
		data = {"real_category_codes":real_category_codes}; 
		
		httpSrv.post({ 
			url 	: "agent_category/update_reorder_real_category", 
			data	: $.param(data), 
			success : function(response){ 
				if (response.data.status == "SUCCESS"){ 
					toastr.success("Saved..."); 
					$scope.loadDataRealCategory();
				} 
			} 
		}); 
 
	} 

	$scope.loadDataRealCategoryDetail = function(){
		var real_category_code = $stateParams.real_category_code; 
		$scope.search.real_category_code = real_category_code; 

		httpSrv.get({
			url 	: "agent_category/detail/"+real_category_code,
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.DATA.current_real_category = response.data;
				}else{
					alert(response.data.error_desc[0]);
				}
			},
			error : function (response){}
		});
	}
	
	//Category Contract Rates
	$scope.loadDataContractRates = function(){
		var real_category_code = $stateParams.real_category_code; 
		httpSrv.post({
			url 	: "agent_category/contract_rates/"+real_category_code,
			data	: $.param({"publish_status":"all"}),
			success : function(response){
				$scope.DATA.contract_rates = response.data;
			},
			error : function (response){}
		});
	}
	$scope.loadDataContractRatesDetail = function(current_contract, after_load_handler_function){
		if (!current_contract){
			var contract_rates_code = $stateParams.contract_rates_code; 
			current_contract = {};
			$scope.DATA.current_contract_rates = {"show_loading":true};
		}else{
			contract_rates_code = current_contract.contract_rates_code;
		}
		
		//Load Data Product
		if (!$scope.DATA.products){
			//Load Data Product & Rates (ACT)
			httpSrv.post({
				url 	: "activities/product",
				success : function(response){
					if (response.data.status == 'SUCCESS'){
						$scope.DATA.products = response.data.products;
					}
				},
				error : function (response){}
			});
		}
		//Load Data Trip & Schedule
		if (!$scope.DATA.schedules){
			httpSrv.post({
				url 	: "transport/schedule",
				success : function(response){
					if (response.data.status == 'SUCCESS'){
						$scope.DATA.schedules = response.data.schedules;
					}
				},
				error : function (response){}
			});
		}
		//--
		
		//GET RATES ACT & TRANS
		if (!current_contract.rates_act_by_id_product || !current_contract.rates_trans_by_id_schedule){
			current_contract.show_loading = true;
			httpSrv.post({
				url 	: "agent_category/contract_rates_detail/"+contract_rates_code,
				success : function(response){
					if (response.data.status == 'SUCCESS'){
						if (!current_contract.contract_rates_code){
							current_contract = response.data;
						}
						//Rates ACT
						current_contract.rates_act = response.data.rates_act;
						current_contract.rates_act_by_id_product = {};
						for(x in current_contract.rates_act){
							var rates = current_contract.rates_act[x];
							if (!current_contract.rates_act_by_id_product[rates.product_id]){
								current_contract.rates_act_by_id_product[rates.product_id] = [];
							}
							current_contract.rates_act_by_id_product[rates.product_id].push(rates);
						}
						//
						
						//Rates TRANS
						current_contract.rates_trans = response.data.rates_trans;
						current_contract.rates_trans_by_id_schedule = {};
						for(x in current_contract.rates_trans){
							var rates = current_contract.rates_trans[x];
							if (!current_contract.rates_trans_by_id_schedule[rates.schedule_id]){
								current_contract.rates_trans_by_id_schedule[rates.schedule_id] = [];
							}
							current_contract.rates_trans_by_id_schedule[rates.schedule_id].push(rates);
						}
						//
					}
					current_contract.show_loading = false;
					
					if (fn.isFn(after_load_handler_function)) {
						after_load_handler_function(current_contract);
					}else{
						$scope.DATA.current_contract_rates = current_contract;
					}
				},
				error : function (response){}
			});
		}
		//--
		
		
		
		
	}
	$scope.publishUnpublishContractRates = function (contract, status){
		var path = (status=='1')?"publish_contract_rates":"unpublish_contract_rates";
		contract.publish_status = status;
		
		httpSrv.get({
			url 	: "agent_category/"+path+"/"+contract.contract_rates_code,
			success : function(response){
				if (response.data.status != "SUCCESS"){
					alert(response.data.error_desc[0]);
					contract.publish_status = !status;
				}
			},
			error : function (response){}
		});
	}
	$scope.addEditContractRates = function (current_contract){
		//console.log(current_boat);
		if (current_contract){
			$scope.DATA.myContractRates = angular.copy(current_contract);
		}else{
			$scope.DATA.myContractRates = {};
		}
		var real_category_code = $stateParams.real_category_code; 
		$scope.DATA.myContractRates.real_category_code = real_category_code;
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
	}
	$scope.saveDataContractRates = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myContractRates.error_msg = [];
		
		httpSrv.post({
			url 	: "agent_category/"+(($scope.DATA.myContractRates.id)?"update_contract_rates":"create_contract_rates"),
			data	: $.param($scope.DATA.myContractRates),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$("#add-edit-contract-rates").modal("toggle");
					toastr.success("Saved...");
					
					$scope.loadDataContractRates();
				}else{
					$scope.DATA.myContractRates.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	
	$scope.loadMasterRatesDetailACT = function (rates_code, affter_success_handler){
		httpSrv.post({
			url 	: "activities/rates/rates_detail/"+rates_code,
			success : function(response){
				
				var rates_detail = response.data;
				
				if (fn.isFn(affter_success_handler)){
					affter_success_handler(rates_detail);
				}
				
			},
			error : function (response){}
		});
	}
	$scope.loadMasterRatesDetailForRatesListACT = function(rates){
		if (!rates.already_get_detail){
			var rates_code = rates.rates_code;
			$scope.loadMasterRatesDetailACT(rates_code, function(rates_detail){
				if (rates_detail.status == "SUCCESS"){
					//Check Key One by One and insert new data
					for (var key in rates_detail){
						if (!rates[key]){
							rates[key] = rates_detail[key];
						}
					}
					rates.already_get_detail = true;
				}
			});
		}		
	}
	$scope.publishUnpublishProductRatesACT = function (rates, status){
		
		var path = (status=='1')?"publish":"unpublish";
		rates.publish_status = status;
		
		httpSrv.get({
			url 	: "activities/rates/"+path+"_rates/"+rates.rates_code,
			success : function(response){
				if (response.data.status != "SUCCESS"){
					rates.publish_status = !status;
				}
			},
			error : function (response){}
		});
	}
	$scope.deleteProductRatesACT = function (rates){
		if (confirm("Are you sure to delete this rates?")){
			httpSrv.get({
				url 	: "activities/rates/delete_rates/"+rates.rates_code,
				success : function(response){
					if (response.data.status == "SUCCESS"){
						$scope.loadDataContractRatesDetail();
						toastr.success("Deleted...");
					}
				},
				error : function (response){}
			});
		}
	}
	$scope.removeProductRatesACT = function (rates){
		if (confirm("Are you sure to remove this rates?")){
			
			var data = {};
			data.rates_code = rates.rates_code;
			data.contract_rates_code = $scope.DATA.current_contract_rates.contract_rates_code;
			
			httpSrv.post({
				url 	: "agent_category/remove_rates_for_contract_rates_act",
				data	: $.param(data),
				success : function(response){
					if (response.data.status == "SUCCESS"){
						$scope.loadDataContractRatesDetail();
						toastr.success("Removed...");
					}
				},
				error : function (response){}
			});
		}
	}
	
	$scope.popUpSelectMasterRatesProductACT = function(product){
		$scope.DATA.myMasterRatesProductACT = {};
		$scope.DATA.myMasterRatesProductACT.product = product;
		$scope.search_rates_pop_up_act = '';
		
		//Load Data Product Rates
		httpSrv.post({
			url 	: "activities/rates/product/"+product.product_code,
			data	: $.param({"publish_status":"all"}),
			success : function(response){
				var rates = response.data;
				if (rates.status == 'SUCCESS'){
					$scope.DATA.myMasterRatesProductACT.rates = rates.rates;
				}else{
					$scope.DATA.myMasterRatesProductACT.rates = rates;
				}
			},
			error : function (response){}
		});
		//--
	}
	$scope.popUpSelectThisMasterRatesProductACT = function(rates){
		//for(_x in $scope.DATA.myMasterRatesProductACT.rates){
		//	delete $scope.DATA.myMasterRatesProductACT.rates[_x].selected;
		//}
		if (!$scope.DATA.myMasterRatesProductACT.selected_rates){ $scope.DATA.myMasterRatesProductACT.selected_rates = {};}
		if (rates.selected){
			delete rates.selected;
			delete $scope.DATA.myMasterRatesProductACT.selected_rates[rates.rates_code];
		}else{
			rates.selected = true;
			$scope.DATA.myMasterRatesProductACT.selected_rates[rates.rates_code] = rates;
		}
		$scope.DATA.myMasterRatesProductACT.qty_selected_rates = Object.keys($scope.DATA.myMasterRatesProductACT.selected_rates).length;
		
	}
	$scope.popUpSubmitSelectedMasterRatesProductACT = function(event){
		if ($scope.DATA.myMasterRatesProductACT && $scope.DATA.myMasterRatesProductACT.selected_rates){
			var data = {};
			data.contract_rates_code = $scope.DATA.current_contract_rates.contract_rates_code;
			data.rates_codes = [];
			
			for (_x in $scope.DATA.myMasterRatesProductACT.selected_rates){
				var _rates = $scope.DATA.myMasterRatesProductACT.selected_rates[_x];
				data.rates_codes.push(_rates.rates_code);
			}
			
			$(event.target).find("button:submit").attr("disabled","disabled");
			
			httpSrv.post({
				url 	: "agent_category/add_rates_for_contract_rates_act",
				data	: $.param(data),
				success : function(response){
					if (response.data.status == "SUCCESS"){
						$("#popUpSelectRatesProductAct").modal("toggle");
						toastr.success("Saved...");
						
						$scope.loadDataContractRatesDetail();
					}else{
						$scope.DATA.myMasterRatesProductACT.error_msg = response.data.error_desc;
					}
					$(event.target).find("button:submit").removeAttr("disabled");
					//$.hideLoading();
				}
			});
		}
	}
	
	$scope.loadMasterRatesDetailTRANS = function (rates_code, affter_success_handler){
		httpSrv.post({
			url 	: "transport/schedule/rates_detail/"+rates_code,
			success : function(response){
				
				var rates_detail = response.data;
				
				if (fn.isFn(affter_success_handler)){
					affter_success_handler(rates_detail);
				}
				
			},
			error : function (response){}
		});
	}
	$scope.loadMasterRatesDetailForRatesListTRANS = function(rates){
		if (!rates.already_get_detail){
			var rates_code = rates.rates_code;
			$scope.loadMasterRatesDetailTRANS(rates_code, function(rates_detail){
				if (rates_detail.status == "SUCCESS"){
					//Check Key One by One and insert new data
					for (var key in rates_detail){
						if (!rates[key]){
							rates[key] = rates_detail[key];
						}
					}
					rates.already_get_detail = true;
				}
			});
		}		
	}
	$scope.publishUnpublishScheduleRatesTRANS = function (rates, status){
		
		var path = (status=='1')?"publish":"unpublish";
		rates.publish_status = status;
		
		httpSrv.get({
			url 	: "transport/schedule/"+path+"_rates/"+rates.rates_code,
			success : function(response){
				if (response.data.status != "SUCCESS"){
					rates.publish_status = !status;
				}
			},
			error : function (response){}
		});
	}
	$scope.deleteScheduleRatesTRANS = function (rates){
		if (confirm("Are you sure to delete this rates?")){
			httpSrv.get({
				url 	: "transport/schedule/delete_rates/"+rates.rates_code,
				success : function(response){
					if (response.data.status == "SUCCESS"){
						$scope.loadDataContractRatesDetail();
						toastr.success("Deleted...");
					}
				},
				error : function (response){}
			});
		}
	}
	$scope.removeScheduleRatesTRANS = function (rates){
		if (confirm("Are you sure to remove this rates?")){
			var data = {};
			data.rates_code = rates.rates_code;
			data.contract_rates_code = $scope.DATA.current_contract_rates.contract_rates_code;
			
			httpSrv.post({
				url 	: "agent_category/remove_rates_for_contract_rates_trans",
				data	: $.param(data),
				success : function(response){
					if (response.data.status == "SUCCESS"){
						$scope.loadDataContractRatesDetail();
						toastr.success("Removed...");
					}
				},
				error : function (response){}
			});
		}
	}
	
	$scope.popUpSelectMasterRatesProductTRANS = function(schedule){
		$scope.DATA.myMasterRatesProductTRANS = {};
		$scope.DATA.myMasterRatesProductTRANS.schedule = schedule;
		$scope.search_rates_pop_up_trans = '';
		
		//Load Data Product Rates
		httpSrv.post({
			url 	: "transport/schedule/rates/"+schedule.schedule_code,
			data	: $.param({"publish_status":"all"}),
			success : function(response){
				var rates = response.data;
				if (rates.status == 'SUCCESS'){
					$scope.DATA.myMasterRatesProductTRANS.rates = rates.rates;
				}else{
					$scope.DATA.myMasterRatesProductTRANS.rates = rates;
				}
			},
			error : function (response){}
		});
		//--
	}
	$scope.popUpSelectThisMasterRatesProductTRANS = function(rates){
		//for(_x in $scope.DATA.myMasterRatesProductTRANS.rates){
		//	delete $scope.DATA.myMasterRatesProductTRANS.rates[_x].selected;
		//}
		if (!$scope.DATA.myMasterRatesProductTRANS.selected_rates){ $scope.DATA.myMasterRatesProductTRANS.selected_rates = {};}
		if (rates.selected){
			delete rates.selected;
			delete $scope.DATA.myMasterRatesProductTRANS.selected_rates[rates.rates_code];
		}else{
			rates.selected = true;
			$scope.DATA.myMasterRatesProductTRANS.selected_rates[rates.rates_code] = rates;
		}
		$scope.DATA.myMasterRatesProductTRANS.qty_selected_rates = Object.keys($scope.DATA.myMasterRatesProductTRANS.selected_rates).length;
		//console.log($scope.DATA.myMasterRatesProductTRANS);
		//alert("abc");
	}
	$scope.popUpSubmitSelectedMasterRatesProductTRANS = function(event){
		if ($scope.DATA.myMasterRatesProductTRANS && $scope.DATA.myMasterRatesProductTRANS.selected_rates){
			var data = {};
			data.contract_rates_code = $scope.DATA.current_contract_rates.contract_rates_code;
			data.rates_codes = [];
			for (_x in $scope.DATA.myMasterRatesProductTRANS.selected_rates){
				var _rates = $scope.DATA.myMasterRatesProductTRANS.selected_rates[_x];
				data.rates_codes.push(_rates.rates_code);
			}
			//console.log(data);
			//return;
			$(event.target).find("button:submit").attr("disabled","disabled");

			httpSrv.post({
				url 	: "agent_category/add_rates_for_contract_rates_trans",
				data	: $.param(data),
				success : function(response){
					if (response.data.status == "SUCCESS"){
						$("#popUpSelectRatesProductTrans").modal("toggle");
						toastr.success("Saved...");
						
						$scope.loadDataContractRatesDetail();
					}else{
						$scope.DATA.myMasterRatesProductTRANS.error_msg = response.data.error_desc;
					}
					$(event.target).find("button:submit").removeAttr("disabled");
					//$.hideLoading();
				}
			});
		}
	}
	
	//Agent List
	$scope.popUpRealCategoryAddAgentLoad = function(){
		//Load Data Agent
		httpSrv.post({
			url 	: "agent",
			data	: $.param({"search":{"limit":"all"}}),
			success : function(response){ 
				$scope.DATA.myAgents = response.data; 
				if ($scope.DATA.myAgents.agents){
					var current_real_category = $scope.$parent.DATA.current_real_category;
					for (x in $scope.DATA.myAgents.agents){
						var _agent = $scope.DATA.myAgents.agents[x];
						if (_agent.real_categories){
							for (y in _agent.real_categories){
								var _cat = _agent.real_categories[y];
								if (_cat.real_category_code == current_real_category.real_category_code){
									_agent.selected = true;
									break;
								}
							}
						}
						//console.log(_agent);
					}
				}
			}
		});
	}
	$scope.popUpRealCategoryAddAgentSubmit = function (agent, real_category){
		if (agent && real_category){
			var data = {};
			data.agent_code 		= agent.agent_code;
			data.real_category_code	= real_category.real_category_code;
			agent.selected = true;

			httpSrv.post({
				url 	: "agent_category/add_agent",
				data	: $.param(data),
				success : function(response){
					
					if (response.data.status == "SUCCESS"){
						toastr.success("Added...");
					}else{
						agent.selected = false;
					}
				}
			});
		}
	}
	$scope.popUpRealCategoryRemoveAgentSubmit = function (agent, real_category){
		if (agent && real_category){
			var data = {};
			data.agent_code 		= agent.agent_code;
			data.real_category_code	= real_category.real_category_code;
			agent.selected = false;

			httpSrv.post({
				url 	: "agent_category/remove_agent",
				data	: $.param(data),
				success : function(response){
					
					if (response.data.status == "SUCCESS"){
						toastr.success("Removed...");
					}else{
						agent.selected = true;
					}
				}
			});
		}
	}
	//--END REAL CATEGORY
});
function activate_sub_menu_agent_detail(class_menu){
	$(".agent-detail ul.nav.sub-nav li").removeClass("active");
	$(".agent-detail ul.nav.sub-nav li."+class_menu).addClass("active");
}

// JavaScript Document
angular.module('app').controller('setting_controller',function($scope, $http, httpSrv, $stateParams, $interval, fn){
	
	GeneralJS.activateLeftMenu("setting");	
	
	$scope.DATA = {};
	$scope.fn = fn;
	
	//System User
	$scope.loadDataSystemUser = function(){
		httpSrv.post({
			url 	: "access_role/user",
			success : function(response){
				$scope.DATA.user = response.data;
			},
			error : function (response){}
		});
		
		httpSrv.post({
			url 	: "access_role",
			success : function(response){
				$scope.DATA.access_role = response.data;
			},
			error : function (response){}
		});
	}
	$scope.addEditSystemUser = function (current_user){
		//console.log(current_boat);
		if (current_user){
			$scope.DATA.myUser = angular.copy(current_user);
		}else{
			$scope.DATA.myUser = {};
		}
	}
	$scope.saveDataSystemUser = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myUser.error_msg = [];
		
		httpSrv.post({
			url 	: "access_role/"+(($scope.DATA.myUser.id)?"update_user":"create_user"),
			data	: $.param($scope.DATA.myUser),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataSystemUser();
					$("#add-edit-user").modal("hide");
					toastr.success("Saved...");
				}else{
					$scope.DATA.myUser.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	$scope.publishUnpublishSystemUser = function (user, status){
		
		var path = (status=='1')?"publish_system_user":"unpublish_system_user";
		user.publish_status = status;
		
		httpSrv.get({
			url 	: "access_role/"+path+"/"+user.id,
			success : function(response){
				if (response.data.status != "SUCCESS"){
					user.publish_status = !status;
				}
			},
			error : function (response){}
		});
	}
	//--
	
	//Access ROle
	$scope.loadDataAccessRole = function(){
		httpSrv.post({
			url 	: "access_role",
			data	: $.param({"publish_status":"all"}),
			success : function(response){
				$scope.DATA.access_role = response.data;
			},
			error : function (response){}
		});
		if (!$scope.DATA.menu_link){
			httpSrv.post({
				url 	: "access_role/all_menu_link",
				success : function(response){
					$scope.DATA.menu_link = response.data;
				},
				error : function (response){}
			});
		}
	}
	$scope.viewAccessRoleDetail = function(current_access_role){
		current_access_role.view_detail_menu_link = !current_access_role.view_detail_menu_link;
		if (!current_access_role.menu_link){
			httpSrv.post({
				url 	: "access_role/detail/"+current_access_role.id,
				success : function(response){
					if (response.data.status == "SUCCESS"){
						current_access_role.menu_link = response.data.access_role.menu_link;
					}
				},
				error : function (response){}
			});
		}
	}
	$scope.addEditAccessRole = function (current_access_role){
		//console.log(current_boat);
		if (current_access_role){
			
			var set_detail = function (menu_link){
				current_access_role.detail = {};
				for (x in menu_link){
					current_access_role.detail[menu_link[x].id] = '1';
				}
				$scope.DATA.myAccessRole = current_access_role;
				//console.log(current_access_role);
			}
			
			if (!current_access_role.menu_link){
				httpSrv.post({
					url 	: "access_role/detail/"+current_access_role.id,
					success : function(response){
						if (response.data.status == "SUCCESS"){
							current_access_role.menu_link = response.data.access_role.menu_link;
							set_detail(current_access_role.menu_link);
						}
					},
					error : function (response){}
				});
			}else{
				set_detail(current_access_role.menu_link);
			}
			
			$scope.DATA.myAccessRole = angular.copy(current_access_role);
			//console.log($scope.DATA.myAccessRole);
		}else{
			$scope.DATA.myAccessRole = {};
		}
	}
	$scope.saveDataAccessRole = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myAccessRole.error_msg = [];
		
		var data = angular.copy($scope.DATA.myAccessRole);
		delete data.detail;
		delete data.menu_link;
		data.menu_link_id = [];
		
		for(_id in $scope.DATA.myAccessRole.detail){
			if ($scope.DATA.myAccessRole.detail[_id] == '1'){
				data.menu_link_id.push(_id);
			}
		}
		
		httpSrv.post({
			url 	: "access_role/"+(($scope.DATA.myAccessRole.id)?"update_access_role":"create_access_role"),
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataAccessRole();
					$("#add-edit-access-role").modal("hide");
					toastr.success("Saved...");
				}else{
					$scope.DATA.myAccessRole.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	$scope.publishUnpublishAccessRole = function (access_role, status){
		
		var path = (status=='1')?"publish_access_role":"unpublish_access_role";
		access_role.publish_status = status;
		
		httpSrv.get({
			url 	: "access_role/"+path+"/"+access_role.id,
			success : function(response){
				if (response.data.status != "SUCCESS"){
					access_role.publish_status = !status;
				}
			},
			error : function (response){}
		});
	}
	//--

	//BookkeepingRates
	$scope.loadDataBookkeepingRates = function(){
		httpSrv.post({
			url 	: "currency_converter/bookkeeping_rates",
			success : function(response){
				$scope.DATA.bookkeeping_rates = response.data;
				if (!$scope.DATA.bookkeeping_rates.bookkeeping_rates){
					$scope.DATA.bookkeeping_rates.bookkeeping_rates = [];
				}
			},
			error : function (response){}
		});
	}
	$scope.addBookkeepingRates = function(){
		var br = {	"from"	:{"currency":$scope.DATA.bookkeeping_rates.default_currency},
					"to"	:{"currency":""}}
		$scope.DATA.bookkeeping_rates.bookkeeping_rates.push(br);
	}
	$scope.saveDataBookkeepingRates = function (event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.bookkeeping_rates.error_msg = [];
		
		var data = {};
		data.bookkeeping_rates = angular.copy($scope.DATA.bookkeeping_rates.bookkeeping_rates);
		
		httpSrv.post({
			url 	: "currency_converter/bulk_update_bookkeeping_rates",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataBookkeepingRates();
					toastr.success("Saved...");
				}else{
					$scope.DATA.bookkeeping_rates.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	//--
	
	
	$scope.publishUnpublishAgent = function (agent, status){
		
		if (confirm("Click OK to continue...")){
			var path = (status=='1')?"publish":"unpublish";
			agent.publish_status = status;
			
			httpSrv.get({
				url 	: "agent/"+path+"/"+agent.agent_code,
				success : function(response){
					if (response.data.status != "SUCCESS"){
						agent.publish_status = !status;
					}
				},
				error : function (response){}
			});
		}
	}
	
	$scope.loadDataAgentDetail = function(agent_code, after_load_handler_function){
		
		if (!agent_code){ agent_code = $stateParams.agent_code; }
		
		httpSrv.get({
			url 	: "agent/detail/"+agent_code,
			success : function(response){
				var agent = response.data;
				
				if (agent.status == "SUCCESS"){
					$scope.DATA.current_agent = agent;
					
					if (typeof after_load_handler_function == 'function') {
						after_load_handler_function(agent);
					}
				}else{
					alert("Sorry, agent not found..");
					window.location = "#/agent";
				}
			},
			error : function (response){}
		});
	}
	
	$scope.agentAddEdit = function (){
		var agent_code = $stateParams.agent_code;
		
		httpSrv.get({
			url 	: "general/country_list",
			success : function(response){
				$scope.DATA.country_list = response.data;
			},
			error : function (response){}
		});
		httpSrv.get({
			url 	: "agent/category",
			success : function(response){
				$scope.DATA.categories = response.data;
			},
			error : function (response){}
		});
		
		if (agent_code){
			$scope.loadDataAgentDetail(agent_code, function(agent_detail){
				agent_detail.country_code = agent_detail.country.code;
				$scope.DATA.current_agent = agent_detail;
				
				if (agent_detail.category){
					$scope.DATA.current_agent.category_code = agent_detail.category.category_code;
				}else{
					$scope.DATA.current_agent.category_code = "";
				}
				
				$scope.DATA.current_agent.payment_method_code = agent_detail.payment_method.payment_code;
				
				$scope.DATA.current_agent.ready = true;
			});
		}else{
			$scope.DATA.current_agent = {};
			$scope.DATA.current_agent.country_code = "ID";
			$scope.DATA.current_agent.category_code = "";
			$scope.DATA.current_agent.ready = true;
		}
	}

	$scope.saveDataAgent = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.current_agent.error_desc = [];
		
		var data = angular.copy($scope.DATA.current_agent);
		data.payment_method = data.payment_method_code;
		
		httpSrv.post({
			url 	: "agent/"+(($scope.DATA.current_agent.id)?"update":"create"),
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					window.location = "#/agent/detail/"+response.data.agent_code;
					toastr.success("Saved...");
				}else{
					$scope.DATA.current_agent.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
					$('html, body').animate({scrollTop:200}, 400);
				}
				//$.hideLoading();
			}
		});
	}

	$scope.loadDataBookingTransport = function(){
		var agent_code = $stateParams.agent_code;
		var data = {"search":{
						"agent_code" : agent_code
					}};
		httpSrv.post({
			url 	: "transport/booking/transaction",
			data	: $.param(data),
			success : function(response){
				$scope.DATA.bookings = response.data;
			},
			error : function (response){}
		});
		
		httpSrv.post({
			url 	: "agent/out_standing_invoice/",
			data	: $.param({"agent_code":agent_code}),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.DATA.out_standing_invoice = response.data.out_standing_invoice;
				}
			},
			error : function (response){}
		});
	}

	//----------------------

	$scope.loadDataCategory = function(){
		httpSrv.post({
			url 	: "agent/category",
			data	: $.param({"publish_status":"all"}),
			success : function(response){
				$scope.DATA.categories = response.data;
			},
			error : function (response){}
		});
	}
	$scope.loadDataCategoryDetail = function(){
		var category_code = $stateParams.category_code;
		var selected_agent_rates = {};

		httpSrv.post({
			url 	: "agent/detail_category/"+category_code,
			success : function(response){
				$scope.DATA.category_detail = response.data;

				httpSrv.post({
					url 	: "agent/get_contract_rates/"+$scope.DATA.category_detail.id,
					success : function(response){
						$scope.DATA.contract_rates = {};
						
						for (x in response.data.contract_rates){
							var id_rt = response.data.contract_rates[x];
							$scope.DATA.contract_rates[id_rt] = true;
						}
						
					},
					error : function (response){}
				});

			},
			error : function (response){}
		});
		
		httpSrv.post({
			url 	: "transport/schedule",
			success : function(response){
				$scope.DATA.schedules = response.data;
				$scope.DATA.rates = {};
				
				//Get Rates for each schedule
				if (response.data.status == "SUCCESS"){
					for (j in $scope.DATA.schedules.schedules){
						var schedules = angular.copy($scope.DATA.schedules.schedules[j]);

						httpSrv.post({
							url 	: "transport/schedule/rates/"+schedules.schedule_code,
							success : function(response2){
								if (response2.data.status == "SUCCESS"){
									$scope.DATA.rates[response2.data.rates[0].schedule_id]=response2.data;
								}
							},
							error : function (response){}
						});
					}
				}
			},
			error : function (response){}
		});
		
	}
	
	
	$scope.saveDataContractRatesCategory = function(event){
		///XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		//$scope.DATA.myCategory.error_msg = [];
		
		var rates = $scope.DATA.rates;
		var data_rates_code = [];
		
		for (id in rates){
			for (i in rates[id].rates){
				var _rt = rates[id].rates[i];
				if (_rt.selected_agent_rates){
					data_rates_code.push(_rt.id);
				}
			}
		}
		
		var data = {"category_id" 	: $scope.DATA.category_detail.id,
					"rates_id"		: data_rates_code};		
		
		httpSrv.post({
			url 	: "agent/save_agent_contract_rates",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					toastr.success("Saved...");
					$scope.loadDataCategoryDetail();
					$(event.target).find("button:submit").hide();
				}else{
					$scope.DATA.myCategory.error_msg = response.data.error_desc;
				}
				//$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}

	// template
	$scope.loadDataVendor = function(){
		
		httpSrv.get({
			url 	: "vendor",
			success : function(response){
				$scope.DATA.vendor = response.data;
				var address = response.data.address;
				var area = response.data.area;
				var regency = response.data.regency;
				var province = response.data.province;
				var country = response.data.country;

				var a = new Array(address,area,regency,province,country);

				var address_ = a.join(", ");
				$scope.DATA.vendor.full_address = address_;

			},
			error : function (response){}
		});
	}
	
	//TAMPLATE AGENT
	$scope.saveUpdateTemplate = function(event){

		$(event.target).find("button:submit").attr("disabled","disabled");
		
		var data = $scope.code_vendor;		
		// console.log(data);
		
		httpSrv.post({
			url 	: "vendor/update_template_agent",
			data	: $.param({"code" : data}),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataVendor();
					$("#update_template").modal("hide");
					// window.location = "#/setting/template";
					toastr.success("Saved...");
				}
				// else{
				// 	$scope.DATA.myCategory.error_msg = response.data.error_desc;
				// }
				//$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	// template
	
});
function activate_sub_menu_agent_detail(class_menu){
	$(".agent-detail ul.nav.sub-nav li").removeClass("active");
	$(".agent-detail ul.nav.sub-nav li."+class_menu).addClass("active");
}

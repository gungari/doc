// JavaScript Document
angular.module('app').controller('trans_reservation_controller',function($scope, $http, httpSrv, $stateParams, $interval, fn){
	
	GeneralJS.activateLeftMenu("reservation");	
	
	$scope.DATA = {};
	$scope.fn = fn;
	
	$scope.newReservationTransport = function(){
		$scope.DATA.data_rsv = {"booking_source" : "OFFLINE",
								"booking_status" : "DEFINITE",
								"selected_trips" : []};
		
		$scope.DATA.data_rsv = {"booking_source":"OFFLINE","booking_status":"DEFINITE","selected_trips":[{"date":"2017-05-30","adult":3,"child":0,"infant":0,"trips":{"id":"710","rates_code":"TDQ5341","name":"Sanur Lembongan Publish Rates","description":"","booking_handling":"INSTANT","departure":{"port":{"id":"10","name":"Sanur","port_code":"SNR"},"time":"09:30","trip_number":"1"},"arrival":{"port":{"id":"3","name":"Lembongan","port_code":"LEM"},"time":"10:00","trip_number":"1"},"start_date":"2017-05-01","end_date":"2017-12-31","available_on":"all_day","min_order":1,"currency":"IDR","rates":{"one_way_rates":{"rates_1":350000,"rates_2":300000,"rates_3":0},"return_rates":{"rates_1":300000,"rates_2":275000,"rates_3":0}},"cut_of_booking":0,"pickup_service":"yes","dropoff_service":"yes","publish_status":"1","lang":"EN","aplicable_rates":{"rates_1":300000,"rates_2":275000,"rates_3":0},"schedule":{"id":"9","schedule_code":"TDQ0507","name":"Sanur, Lembongan, Lombok, Inter Island","description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. \n\nIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.","publish_status":"1","lang":"EN","boat":{"id":"62","boat_code":"TDQ9954","name":"Bima X 01.50","capacity":"50","main_image":"https://bes.hybridbooking.com/image/100/500/0/public/images/no_image.jpg","available_space":32}},"is_hidden":false,"is_selected":true},"sub_total":900000},{"date":"2017-06-02","adult":3,"child":0,"infant":0,"trips":{"id":"712","rates_code":"TDQ5181","name":"Lembongan Sanur Publish Rates","description":"","booking_handling":"INSTANT","departure":{"port":{"id":"3","name":"Lembongan","port_code":"LEM"},"time":"16:00","trip_number":"5"},"arrival":{"port":{"id":"10","name":"Sanur","port_code":"SNR"},"time":"17:00","trip_number":"5"},"start_date":"2017-05-01","end_date":"2017-12-31","available_on":"all_day","min_order":1,"currency":"IDR","rates":{"one_way_rates":{"rates_1":350000,"rates_2":300000,"rates_3":0},"return_rates":{"rates_1":300000,"rates_2":275000,"rates_3":0}},"cut_of_booking":0,"pickup_service":"yes","dropoff_service":"yes","publish_status":"1","lang":"EN","aplicable_rates":{"rates_1":300000,"rates_2":275000,"rates_3":0},"schedule":{"id":"9","schedule_code":"TDQ0507","name":"Sanur, Lembongan, Lombok, Inter Island","description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. \n\nIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.","publish_status":"1","lang":"EN","boat":{"id":"43","boat_code":"TDQ3431","name":"Bima X 01.100","capacity":"100","main_image":"https://bes.hybridbooking.com/image/100/500/0/webimages/trans_boat/gallery/1473126809.jpg","available_space":82}},"is_hidden":false,"is_selected":true},"sub_total":900000}],"TOTAL":{"total":1800000,"discount":0,"grand_total":1800000}}
		$scope.calculate_total();
		
		fn.pre($scope.DATA.data_rsv);
		
		//Load Data Agent
		httpSrv.post({
			url 	: "agent",
			success : function(response){
				$scope.DATA.agents = response.data;
			},
			error : function (response){}
		});
		
		//Load Data Country List
		httpSrv.post({
			url 	: "general/country_list",
			success : function(response){
				$scope.DATA.country_list = response.data;
			},
			error : function (response){}
		});
		
		//Load Data Available Port
		httpSrv.post({
			url 	: "transport/port/available_port",
			success : function(response){
				$scope.DATA.available_port = response.data;
			},
			error : function (response){}
		});
	}
	
	$scope.add_new_trip = function(){
		$scope.add_trip = {};
		$scope.add_trip = {"trip_model":"ONEWAY","adult":1,"child":0,"infant":0};
		
		$scope.add_trip = {"trip_model":"RETURN","adult":1,"child":0,"infant":0,"departure_port_id":"10","destination_port_id":"7","departure_date":"2017-05-30","return_date":"2017-06-02","arrival_port_id":"3"};
		
		$scope.check_availabilities(event);
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
	}
	
	$scope.check_availabilities = function(event){
		//$.showLoading();
		
		var data = angular.copy($scope.add_trip);
		
		//fn.pre(data, "check_availabilities");
		
		if (data.trip_model == "ONEWAY"){
			delete data.return_date;
		}
		
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.add_trip.error_desc = [];
		$scope.check_availabilities_data = {};
		$scope.check_availabilities_show_loading = true;
		
		httpSrv.post({
			url 	: "transport/schedule/check_availabilities",
			data	: $.param(data),
			success : function(response){
				$(event.target).find("button:submit").removeAttr("disabled");
				$scope.check_availabilities_show_loading = false;
				$scope.check_availabilities_show_searching_form = false;
				
				if (response.data.status == "SUCCESS"){
					$scope.check_availabilities_data = response.data;
				}else{
					$scope.add_trip.error_desc = response.data.error_desc;
					//$('html, body').animate({scrollTop:200}, 400);
				}
				
				fn.pre($scope.check_availabilities_data, "check_availabilities_data");
				//$.hideLoading();
			}
		});
		
	}
	
	$scope.check_availabilities_select_this_trip = function (trip, trips_parent){
		fn.pre(trip);
		for (index in trips_parent.availabilities){
			if (trips_parent.selected_trip){
				delete trips_parent.availabilities[index].is_hidden;
			}else{
				trips_parent.availabilities[index].is_hidden = true;
			}
			delete trips_parent.availabilities[index].is_selected;
		}
		if (trips_parent.selected_trip){
			delete trips_parent.selected_trip;
		}else{
			trip.is_selected = true;
			trip.is_hidden = false;
			
			trips_parent.selected_trip = trip;
		}
		
		$scope.check_availabilities_show_OK_button = false;
		if ($scope.add_trip.trip_model == "RETURN"){
			if ($scope.check_availabilities_data.departure.selected_trip && $scope.check_availabilities_data.return.selected_trip){
				$scope.check_availabilities_show_OK_button = true;
			}
		}else{
			if ($scope.check_availabilities_data.departure.selected_trip){
				$scope.check_availabilities_show_OK_button = true;
			}
		}
	}
	
	$scope._check_availabilities_add_to_data_rsv = function(trip){
		
		var selected_trips = {};
		
		selected_trips.date		= trip.check.date;
		
		selected_trips.adult 	= trip.check.adult;
		selected_trips.child 	= trip.check.child;
		selected_trips.infant 	= trip.check.infant;
		
		selected_trips.trips = trip.selected_trip;
		
		selected_trips.sub_total =  (selected_trips.adult*selected_trips.trips.aplicable_rates.rates_1) +
									(selected_trips.child*selected_trips.trips.aplicable_rates.rates_2) +
									(selected_trips.infant*selected_trips.trips.aplicable_rates.rates_3);
		
		$scope.DATA.data_rsv.selected_trips.push(selected_trips);
	}
	$scope.check_availabilities_add_to_data_rsv = function(){
		
		if ($scope.check_availabilities_data.departure && $scope.check_availabilities_data.departure.selected_trip){
			$scope._check_availabilities_add_to_data_rsv($scope.check_availabilities_data.departure);
		}
		if ($scope.check_availabilities_data.return && $scope.check_availabilities_data.return.selected_trip){
			$scope._check_availabilities_add_to_data_rsv($scope.check_availabilities_data.return);
		}
		
		$scope.calculate_total();
		$("#modal-add-trip").modal("hide");
	}
	$scope.calculate_total = function(){
		var total = 0;
		for (x in $scope.DATA.data_rsv.selected_trips){
			total += $scope.DATA.data_rsv.selected_trips[x].sub_total;
		}
		$scope.DATA.data_rsv.TOTAL = {"total":0, "discount":0};
		$scope.DATA.data_rsv.TOTAL.total = total;
		
		$scope.DATA.data_rsv.TOTAL.grand_total = total - $scope.DATA.data_rsv.TOTAL.discount;
	}
	$scope.remove_trip = function (index){
		if (confirm("Are you sure to delete this trip?")){
			$scope.DATA.data_rsv.selected_trips.splice(index,1);
			$scope.calculate_total();
		}
	}
});

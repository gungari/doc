angular.module('app').controller('report_home_controller',function($scope, $http, httpSrv, $stateParams, $interval, fn, $filter){
	
	GeneralJS.activateLeftMenu("reports");	
	
	$scope.DATA = {};
	$scope.fn = fn;
	$scope.search = [];

	$scope.loadReportHome = function()
	{

		$scope.show_loading_DATA = true;
		// $scope.search.year = $filter('date')(new Date(), 'yyyy');
		// $scope.search.month = $filter('date')(new Date(), 'MM');
		$scope.search.year = '2017';
		$scope.search.month = '09';
		$scope.search.date = $filter('date')(new Date(), 'dd');
		$scope.search.view = "month";
		$scope.show_graph = false;
		$scope.date_data = true;
		$scope.show_error = false;
		$scope.loadSalesHome();
		$scope.loadCheckinHome();
		$scope.loadReportRevenueTrans();
		$scope.loadReportAgentData();
		$scope.loadReportCancellData();
	}

	$scope.loadReportAgentData = function()
	{
		
		if (!$scope.search.month) $scope.search.month = $scope.month.toString();
		if (!$scope.search.year) $scope.search.year = $scope.year.toString();
		if (!$scope.search.date) $scope.search.date = $scope.date.toString();

		$scope.show_graph = true;

		var _search = {	
						"month" 	: $scope.search.month,
						"year"		: $scope.search.year,
						"date" 		: $scope.search.date,
						"view" 		: $scope.search.view
					  };
		
		httpSrv.post({
			url 	: "transport/report/agent_home",
			data	: $.param(_search),
			success : function(response){
				
				//$scope.show_loading_DATA = false;
				if (response.data.status == "SUCCESS") {
					$scope.graph_agent(response.data.graph);
				}else{

				}
				
			},
			error : function (error){
				$scope.show_error = true;
				$scope.show_loading_DATA = false;
			}
		})
	}

	$scope.getTotalCancell = function(data){
	    var total = 0;
	   
	    angular.forEach(data, function(row){

	    	total += parseInt(row.sources.total); 

			currency = row.sources.currency;
	    
	    });
	   
	    $scope.total_cancell = total;
	    $scope.currency_cancell = currency;
	 
	}

	$scope.graph_agent = function($object){
		
		$scope.chartObject_agent = {};
		
	   $scope.chartObject_agent.data = $object;

	   
	    $scope.chartObject_agent.type = "BarChart";
	    $scope.chartObject_agent.options = {
	    	
	        'title': 'Most Active Agencies',
	        legend: { position: 'bottom' },
	        fontSize: 10,
	        responsive: true,
	        chartArea: {width: '40%', heigth: '100%'},
	       
	    }
	}

	$scope.loadReportCancellData = function()
	{
		
		if (!$scope.search.month) $scope.search.month = $scope.month.toString();
		if (!$scope.search.year) $scope.search.year = $scope.year.toString();
		if (!$scope.search.date) $scope.search.date = $scope.date.toString();


		var _search = {	
						"month" 	: $scope.search.month,
						"year"		: $scope.search.year,
						"date" 		: $scope.search.date,
						"view" 		: $scope.search.view
					  };
		
		httpSrv.post({
			url 	: "transport/report/sales_cancell_home",
			data	: $.param(_search),
			success : function(response){
				
				if (response.data.status == "SUCCESS") {

					$scope.graph_cancel(response.data.graph);
					$scope.getTotalCancell(response.data.sales);
					
				}else{
					$scope.graph_cancel(response.data.graph);
					
				}
				
			},
			error : function (response){}
			})
	}

	$scope.loadSalesHome = function()
	{
		
		if (!$scope.search.month) $scope.search.month = $scope.month.toString();
		if (!$scope.search.year) $scope.search.year = $scope.year.toString();
		if (!$scope.search.date) $scope.search.date = $scope.date.toString();
		$scope.show_loading_DATA = true;
		var _search = {	
						"month" 	: $scope.search.month,
						"year"		: $scope.search.year,
						"date" 		: $scope.search.date,
						"view" 		: $scope.search.view
					  };
		if ($scope.search.view != 'date') {
			$scope.show_graph = true;
			$scope.date_data = false;
			$scope.date_data_agent = false;
		}
		else{
			$scope.show_graph = false;
			$scope.date_data = true;
			$scope.date_data_agent = false;
		}
		httpSrv.post({
			url 	: "report/Sales_home",
			data	: $.param(_search),
			success : function(response){
				
				$scope.graph(response.data.graph);
				
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					
					$scope.getTotal2(response.data.sales);
												
				}else{
					$scope.report = [];
					$scope.show_loading_DATA = false;
					$scope.show_graph = false;
					$scope.show_error = true;
					
				}
				
			},
			error : function (response){}
			})


	}

	$scope.getTotal2 = function(data){

		console.log("dataaaa", data);

	    var total = 0;
	    angular.forEach(data, function(row){
	    	total += parseInt(row.sources.total); 
	    	$scope.currency_home = row.sources.currency;
	    });
	   
	    $scope.total_home = total;
	   
	}

	$scope.loadCheckinHome = function()
	{
		
		$scope.show_loading_DATA = true;
		var _search = {	
						"month" 	: $scope.search.month,
						"year"		: $scope.search.year,
						"date" 		: $scope.search.date,
						"view" 		: $scope.search.view
					  };
		if ($scope.search.view != 'date') {
			$scope.show_graph = true;
			$scope.date_data = false;
		}
		else{
			$scope.show_graph = false;
			$scope.date_data = true;
		}

		httpSrv.post({
			url 	: "report/checkin_home",
			data	: $.param(_search),
			success : function(response){
				if (response.data.status == "SUCCESS") {
				
					$scope.report = response.data;
					
					$scope.graph_check(response.data.graph);
					$scope.totalCheckin(response.data.sales);
					
				}else{
					$scope.report = [];
					$scope.show_loading_DATA = false;
					$scope.show_graph = false;
					$scope.show_error = true;
					
				}
				
			},
			error : function (response){}
			})
	}

	$scope.loadReportRevenueTrans = function(page){

		$scope.show_loading_DATA = true;
		$scope.show_error = false;
		if (!$scope.search) $scope.search = {}
		
		if (!$scope.$root.DATA_booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.$root.DATA_booking_source = response.data.booking_source;},
			});
		}
		
		if (!page) page = 1;
		var _search = {	"page" 				: page,
						"start_date" 		: ($scope.search.date?$scope.search.date:""),
						"group_by_trip"		: "1",
						"booking_source"	: ($scope.search.booking_source?$scope.search.booking_source:""),
						"month" 			: $scope.search.month,
						"year"				: $scope.search.year,
						"date" 				: $scope.search.date,
						"view" 				: $scope.search.view
		};

		if ($scope.search.booking_source) _search.booking_source = $scope.search.booking_source;
		if ($scope.search.view == 'date') {
			$scope.date_data = true;
			$scope.month_data = false;
			$scope.show_graph = false;
		}else{
			$scope.date_data = false;
			$scope.month_data = true;
			$scope.show_graph = true;
		}
		httpSrv.post({
			url 	: "transport/report/revenue_home",
			data	: $.param({"search":_search}),
			success : function(response){
				console.log('revenue', response.data);
				$scope.show_loading_DATA = false;
				if (response.data.status == "SUCCESS") {
					if (response.data.search){
						$scope.DATA = response.data;
						$scope.filter_date = $scope.DATA.search.start_date;
						$scope.date_data = true;
						$scope.month_data = false;
						$scope.show_graph = false;
					}else{
						$scope.DATA.sales = response.data.sales;
						$scope.graph3(response.data.graph);
						$scope.totalRevenue(response.data.sales);
					}
				}else{
					$scope.show_error = true;
					$scope.month_data = false;
					$scope.date_data = false;
				}
				
			},
			error : function (response){}
		});
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
	}

	$scope.graph = function($object){
		console.log("graph in", $object.rows[0]['c']);
		$scope.chartObject = {};
		var maxNumber = 10;
		for(var i=0; i<$object.rows.length; i++){
			
			for(var j=1; j<$object.rows[i]['c'].length; j++){
				if($object.rows[i]['c'][j]['v'] > maxNumber){
					maxNumber = $object.rows[i]['c'][j]['v'];
				}
			}
		}

	    $scope.chartObject.data = $object;
	
	    $scope.chartObject.type = "LineChart";
	    $scope.chartObject.options = {
	    	vAxis: {format: 'decimal', minValue: 0, maxValue: maxNumber, gridlines: {count: -1}, textStyle: {fontSize: 10}},
			hAxis: {textStyle: {fontSize: 8}},
	        'title': 'Booking',
	        legend: { position: 'bottom' },
	        fontSize: 10,
	        responsive: true,
	        chartArea: {width: '85%', heigth: '100%'},
	       
	    }

	}

	$scope.graph3 = function($object){
		
		$scope.chartObject3 = {};

		var maxNumber = 10;

	   	for(var i=0; i<$object.rows.length; i++){
			
			for(var j=1; j<$object.rows[i]['c'].length; j++){
				if($object.rows[i]['c'][j]['v'] > maxNumber){
					maxNumber = $object.rows[i]['c'][j]['v'];
				}
			}
		}
		
	 	$scope.chartObject3.data = $object;
	    
	    $scope.chartObject3.type = "LineChart";
	    $scope.chartObject3.options = {
	    	vAxis: {format: 'decimal', minValue: 0, maxValue: maxNumber, gridlines: {count: -1}, textStyle: {fontSize: 10}},
			hAxis: {textStyle: {fontSize: 8}},
	        'title': 'Revenue',
	        legend: { position: 'bottom' },
	        fontSize: 10,
	        responsive: true,
	        chartArea: {width: '85%', heigth: '100%'},
	       
	    }
	}

	$scope.totalCheckin = function(data)
	{
		console.info("Info: ",data); 
		var total = 0;
		var total_book = 0;
		var total_guest = 0;
	    angular.forEach(data, function(row){
	    	total += parseInt(row.sources.total_checkin);
	    	total_book += parseInt(row.sources.total_book);
	    	total_guest += parseInt(row.sources.total_guest);
	    
	   	});
	   
	    $scope.total_checkin = total;
	    $scope.total_book = total_book;
	    $scope.total_guest = total_guest;
	}

	$scope.graph_cancel = function($object){
		console.log("graph in", $object.rows[0]['c']);
		$scope.chartObject_cancell = {};
		var maxNumber = 10;
		for(var i=0; i<$object.rows.length; i++){
			
			for(var j=1; j<$object.rows[i]['c'].length; j++){
				if($object.rows[i]['c'][j]['v'] > maxNumber){
					maxNumber = $object.rows[i]['c'][j]['v'];
				}
			}
		}
		
	    $scope.chartObject_cancell.data = $object;
	   
	    $scope.chartObject_cancell.type = "LineChart";
	    $scope.chartObject_cancell.options = {
	    	vAxis: {format: 'decimal', minValue: 0, maxValue: maxNumber, gridlines: {count: -1}, textStyle: {fontSize: 10}},
			hAxis: {textStyle: {fontSize: 8}},
	        'title': 'Booking',
	        legend: { position: 'bottom' },
	        fontSize: 10,
	        responsive: true,
	        chartArea: {width: '85%', heigth: '100%'},
	       
	    }

	}

	$scope.graph_check = function($object){
		$scope.chartObject2 = {};

	   	var maxNumber = 10;

	   	for(var i=0; i<$object.rows.length; i++){
			
			for(var j=1; j<$object.rows[i]['c'].length; j++){
				if($object.rows[i]['c'][j]['v'] > maxNumber){
					maxNumber = $object.rows[i]['c'][j]['v'];
				}
			}
		}
		
	    $scope.chartObject2.data = $object;

	    $scope.chartObject2.type = "LineChart";
	    $scope.chartObject2.options = {
	    	vAxis: {format: 'decimal', minValue: 0, maxValue: maxNumber, gridlines: {count: -1}, textStyle: {fontSize: 10}},
			hAxis: {textStyle: {fontSize: 8}},
	        'title': 'Booking',
	        legend: { position: 'bottom' },
	        fontSize: 10,
	        responsive: true,
	        chartArea: {width: '85%', heigth: '100%'},
	        
	    }
	}


	$scope.totalRevenue = function(data)
	{
		
		var total = 0;
		var currency = '';
	    angular.forEach(data, function(row){
	    	total += parseInt(row.sources.total);
	    	if (currency == '') { currency = row.sources.currency }
	    
	   	});
	   
	    $scope.total_revenue = total;
	    $scope.currency = currency;
	    
	}

});
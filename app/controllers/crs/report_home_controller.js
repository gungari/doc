angular.module('app').controller('report_home_controller',function($scope, $http, httpSrv, $stateParams, $interval, fn, $filter){
	
	GeneralJS.activateLeftMenu("reports");	
	
	$scope.DATA = {};
	$scope.fn = fn;
	$scope.home = [];

	$scope.graph = function($object,id_name) {

		google.charts.load('current', {'packages':['corechart']});
		var data = new google.visualization.DataTable();
		angular.forEach($object.cols, function(arr){
				
			data.addColumn(arr.type, arr.label);
		});

		angular.forEach($object.rows, function(arr){
			if (id_name == 'revenueLine_home') {
				data.addRows([
					[arr.c[0].v, Number(arr.c[1].v)],
				]);;
			}else{
				data.addRows([
					[arr.c[0].v, Number(arr.c[1].v), Number(arr.c[2].v), Number(arr.c[3].v)],
				]);;
			}
			
			
		});

		var maxNumber = 10;
		for(var i=0; i<$object.rows.length; i++){
			
			for(var j=1; j<$object.rows[i]['c'].length; j++){
				if($object.rows[i]['c'][j]['v'] > maxNumber){
					maxNumber = $object.rows[i]['c'][j]['v'];
				}
			}
		}
		var options = {
			vAxis: {format: 'decimal', minValue: 0, maxValue: maxNumber, gridlines: {count: -1}, textStyle: {fontSize: 10}},
			hAxis: {textStyle: {fontSize: 8}},
	        'title': 'Booking',
	        legend: { position: 'bottom' },
	        fontSize: 10,
	        responsive: true,
	        chartArea: {width: '80%', heigth: '100%'},
		};

		var chart = new google.visualization.LineChart(document.getElementById(id_name));
		
		
		chart.draw(data, options);
	}

	$scope.loadReportHome = function()
	{
		
		$scope.show_loading_DATA = true;
		$scope.home.year = $filter('date')(new Date(), 'yyyy');
		$scope.home.month = $filter('date')(new Date(), 'MM');
		$scope.home.date = $filter('date')(new Date(), 'dd');
		$scope.home.view = "month";
		$scope.show_graph = false;
		$scope.date_data = true;
		$scope.show_error = false;
		$scope.loadSalesHome();
		$scope.loadCheckinHome();
		$scope.loadReportRevenueTrans();
		$scope.loadReportAgentData();
		$scope.loadReportCancellData();
	}

	$scope.loadReportAgentData = function()
	{
		
		if (!$scope.home.month) $scope.home.month = $scope.month.toString();
		if (!$scope.home.year) $scope.home.year = $scope.year.toString();
		if (!$scope.home.date) $scope.home.date = $scope.date.toString();

		$scope.show_graph = true;
		$scope.show_loading_agent = true;
		$scope.error_agent = false;
		var _search = {	
						"month" 	: $scope.home.month,
						"year"		: $scope.home.year,
						"date" 		: $scope.home.date,
						"view" 		: $scope.home.view
					  };
		
		httpSrv.post({
			url 	: "transport/report/agent_home",
			data	: $.param(_search),
			success : function(response){
				
				

				if (response.data.status == "SUCCESS") {
					$scope.show_loading_agent = false;
					$scope.graph_agent(response.data.graph);
				}else{
					$scope.show_loading_agent = false;
					$scope.error_agent = true;
				}
				
			},
			error : function (error){
				$scope.show_error = true;
				
			}
		})
	}

	$scope.getTotalCancell = function(data){

	    var total = 0;
	    var currency = '';
	   	var cancell = 0;

	    angular.forEach(data, function(row){

	    	total += parseInt(row.sources.total); 

	    	cancell += parseInt(row.sources.total_book); 

			currency = row.sources.currency;
	    
	    });
	   
	    $scope.amount_cancell = total;
	    $scope.total_cancell = cancell;
	    $scope.currency_cancell = currency;
	 
	}

	google.charts.load('current', {'packages':['corechart']});

	$scope.graph_agent = function($object){

	    var data = new google.visualization.DataTable();
		angular.forEach($object.cols, function(arr){
				
			data.addColumn(arr.type, arr.label);
		});

		angular.forEach($object.rows, function(arr){
				data.addRows([
					[arr.c[0].v, Number(arr.c[1].v), Number(arr.c[2].v)],
				]);;
		});
		
		var maxNumber = 10;
		for(var i=0; i<$object.rows.length; i++){
			
			for(var j=1; j<$object.rows[i]['c'].length; j++){
				if($object.rows[i]['c'][j]['v'] > maxNumber){
					maxNumber = $object.rows[i]['c'][j]['v'];
				}
			}
		}
		var options = {
			'title': 'Most Active Agencies',
	        legend: { position: 'bottom' },
	        fontSize: 10,
	        responsive: true,
	        chartArea: {width: '45%', heigth: '100%'},
		};
		
		var chart = new google.visualization.BarChart(document.getElementById('agentLine_home'));
		
		chart.draw(data, options);
	}

	$scope.loadReportCancellData = function()
	{
		
		if (!$scope.home.month) $scope.home.month = $scope.month.toString();
		if (!$scope.home.year) $scope.home.year = $scope.year.toString();
		if (!$scope.home.date) $scope.home.date = $scope.date.toString();

		$scope.show_loading_cancel = true;
		$scope.error_cancel = false;
		var _search = {	
						"month" 	: $scope.home.month,
						"year"		: $scope.home.year,
						"date" 		: $scope.home.date,
						"view" 		: $scope.home.view
					  };
		
		httpSrv.post({
			url 	: "transport/report/sales_cancell_home",
			data	: $.param(_search),
			success : function(response){
				

				if (response.data.status == "SUCCESS") {
					$scope.show_loading_cancel = false;
					$scope.graph(response.data.graph, "cancelLine_home");
					$scope.getTotalCancell(response.data.sales);
					
				}else{
					$scope.show_loading_cancel = false;
					$scope.error_cancel = true;
					
				}
				
			},
			error : function (response){}
			})
	}

	$scope.loadSalesHome = function()
	{
		
		if (!$scope.home.month) $scope.home.month = $scope.month.toString();
		if (!$scope.home.year) $scope.home.year = $scope.year.toString();
		if (!$scope.home.date) $scope.home.date = $scope.date.toString();
		$scope.show_loading_sales = true;
		$scope.error_sales = false;

		var _search = {	
						"month" 	: $scope.home.month,
						"year"		: $scope.home.year,
						"date" 		: $scope.home.date,
						"view" 		: $scope.home.view
					  };
		if ($scope.home.view != 'date') {
			$scope.show_graph = true;
			$scope.date_data = false;
			$scope.date_data_agent = false;
		}
		else{
			$scope.show_graph = false;
			$scope.date_data = true;
			$scope.date_data_agent = false;
		}
		httpSrv.post({
			url 	: "report/Sales_home",
			data	: $.param(_search),
			success : function(response){
				
				$scope.graph(response.data.graph, 'chartLine_home');
				$scope.show_loading_DATA = false;
				if (response.data.status == "SUCCESS") {
					
					$scope.show_loading_sales = false;
					$scope.getTotal2(response.data.sales);
												
				}else{
					$scope.report = [];
					$scope.show_loading_sales = false;
					//$scope.show_graph = false;
					$scope.error_sales = true;
					
				}
				
			},
			error : function (response){}
			})


	}

	$scope.getTotal2 = function(data){

		

	    var total = 0;
	    angular.forEach(data, function(row){
	    	total += parseInt(row.sources.total); 
	    	$scope.currency_home = row.sources.currency;
	    });
	   
	    $scope.total_home = total;
	   
	}

	$scope.loadCheckinHome = function()
	{
		
		var _search = {	
			"month" 	: $scope.home.month,
			"year"		: $scope.home.year,
			"date" 		: $scope.home.date,
			"view" 		: $scope.home.view
		};

		$scope.show_loading_check = true;
		$scope.error_check = false;

		if ($scope.home.view != 'date') {
			$scope.show_graph = true;
			$scope.date_data = false;
		}
		else{
			$scope.show_graph = false;
			$scope.date_data = true;
		}

		httpSrv.post({
			url 	: "report/checkin_home",
			data	: $.param(_search),
			success : function(response){
				
				
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_check = false;
					$scope.report = response.data;
					
					$scope.graph(response.data.graph, 'checkLine_home');
					$scope.totalCheckin(response.data.sales);
					
				}else{
					$scope.report = [];
					$scope.show_loading_check = false;
					// $scope.show_graph = false;
					// $scope.show_error = true;
					$scope.error_check = true;
				}
				
			},
			error : function (response){}
			})
	}

	$scope.loadReportRevenueTrans = function(page){

		$scope.show_loading_trans = true;
		$scope.error_trans = false;

		$scope.show_error = false;
		if (!$scope.home) $scope.home = {}
		
		if (!$scope.$root.DATA_booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.$root.DATA_booking_source = response.data.booking_source;},
			});
		}
		
		if (!page) page = 1;
		var _search = {	"page" 				: page,
						"start_date" 		: ($scope.home.date?$scope.home.date:""),
						"group_by_trip"		: "1",
						"booking_source"	: ($scope.home.booking_source?$scope.home.booking_source:""),
						"month" 			: $scope.home.month,
						"year"				: $scope.home.year,
						"date" 				: $scope.home.date,
						"view" 				: $scope.home.view
		};

		if ($scope.home.booking_source) _search.booking_source = $scope.home.booking_source;
		if ($scope.home.view == 'date') {
			$scope.date_data = true;
			$scope.month_data = false;
			$scope.show_graph = false;
		}else{
			$scope.date_data = false;
			$scope.month_data = true;
			$scope.show_graph = true;
		}
		httpSrv.post({
			url 	: "transport/report/revenue_home",
			data	: $.param({"search":_search}),
			success : function(response){
				

				
				if (response.data.status == "SUCCESS") {

					$scope.show_loading_trans = false;

					if (response.data.home){
						$scope.DATA = response.data;
						$scope.filter_date = $scope.DATA.home.start_date;
						$scope.date_data = true;
						$scope.month_data = false;
						$scope.show_graph = false;
					}else{
						$scope.DATA.sales = response.data.sales;
						$scope.graph(response.data.graph, 'revenueLine_home');
						$scope.totalRevenue(response.data.sales);
					}
				}else{
					$scope.show_loading_trans = false;
					$scope.show_error = true;
					$scope.month_data = false;
					$scope.date_data = false;
					$scope.error_trans = true;
				}
				
			},
			error : function (response){}
		});
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
	}



	$scope.totalCheckin = function(data)
	{
		 
		var total = 0;
		var total_book = 0;
		var total_guest = 0;
	    angular.forEach(data, function(row){
	    	total += parseInt(row.total_checkin);
	    	total_book += parseInt(row.total_book);
	    	total_guest += parseInt(row.total_guest);
	    
	   	});
	   
	    $scope.total_checkin = total;
	    $scope.total_book = total_book;
	    $scope.total_guest = total_guest;
	}

	
	$scope.totalRevenue = function(data)
	{
		
		var total = 0;
		var currency = '';
	    angular.forEach(data, function(row){
	    	total += parseInt(row.sources.total);
	    	if (currency == '') { currency = row.sources.currency }
	    
	   	});
	   
	    $scope.total_revenue = total;
	    $scope.currency = currency;
	    
	}

});
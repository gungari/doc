// JavaScript Document
angular.module('app').controller('ar_controller',function($scope, $http, httpSrv, $stateParams, $interval, fn,$filter,$window){
	
	GeneralJS.activateLeftMenu("invoice");	
	
	$scope.DATA = {};
	$scope.fn = fn;

	//Create Proforma Invoice
	$scope.createProformaInvoice = function (){

		var agent_code = $stateParams.agent_code;
		var invoice_code = $stateParams.invoice_code;
		
		/*if (!$scope.DATA.agents){
			httpSrv.post({
				url 	: "agent",
				data	: $.param({"search":{"limit":"all","publish_status":"all"}}),
				success : function(response){
					$scope.DATA.agents = response.data.agents;
				},
				error : function (response){}
			});
		}*/
		
		$scope.DATA.current_invoice = {};
		$scope.DATA.current_invoice.ready = true;
		$scope.DATA.search = {};
		$scope.DATA.search.page = 1;
		
		$scope.DATA.selected_booking_list = {};
		
		if (invoice_code){
			/*$scope.loadDataAgentDetail(invoice_code, function(agent_detail){
				agent_detail.country_code = agent_detail.country.code;
				$scope.DATA.current_invoice = agent_detail;
				
				if (agent_detail.category){
					$scope.DATA.current_invoice.category_code = agent_detail.category.category_code;
				}else{
					$scope.DATA.current_invoice.category_code = "";
				}
				
				$scope.DATA.current_invoice.payment_method_code = agent_detail.payment_method.payment_code;				
				$scope.DATA.current_invoice.ready = true;
			});*/
		}else{
			if ($scope.$root.data_search_ar_load_data_unpaid_booking){
				$scope.DATA.search = angular.copy($scope.$root.data_search_ar_load_data_unpaid_booking);
			}else{
				if (agent_code) {
					$scope.DATA.search.type = "RESERVATION";
					$scope.DATA.search.filter_by = "BOOKINGDATE";
					
					$scope.DATA.search.start_date = '';
					$scope.DATA.search.end_date = '';
					$scope.DATA.search.agent_code = agent_code;
					$scope.loadDataUnpaidBooking(1, true);
				}else{
					$scope.DATA.search.ready = true;
					$scope.DATA.search.type = "RESERVATION";
					$scope.DATA.search.filter_by = "BOOKINGDATE";
					
					$scope.DATA.search.agent_code = "";//"AG-IHD5051";
					
					var myDate = new Date();
					var ownDay = new Date(myDate);
					ownDay.setDate(myDate.getDate()-30);
	
					$scope.DATA.search.start_date = $filter('date')(ownDay, "yyyy-MM-dd");;
					$scope.DATA.search.end_date = $filter('date')(myDate, "yyyy-MM-dd");;
				}
			}
		}
		
		//Load Rek Number
		if (!$scope.DATA.vendor_account_number){
			httpSrv.post({
				url 	: "vendor/getRekeningData",
				success : function(response){
					if (response.data.status == 'SUCCESS'){
						$scope.DATA.vendor_account_number = response.data.bank;
					}
				},
				error : function (response){}
			});
		}
		//--
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		$scope.loadDataUnpaidBooking(1, true);
	}	
	$scope.ProformaInvoiceSearchAgentPopUp = function(){
		if ($scope.DATA.search.agent_code == 'SELECTAGENT'){
			$scope.DATA.selected_agents = [];
			
			$("#modal-search-agent").modal("show");
			
			//Load Data Agent
			if (!$scope.DATA.agents){
				httpSrv.post({
					url 	: "agent",
					data	: $.param({"search":{"limit":"all"}}),
					success : function(response){ $scope.DATA.agents = response.data; }
				});
			}
			//Load Data Real Category Agent
			if (!$scope.DATA.agent_real_categories){
				httpSrv.get({
					url 	: "agent/real_category",
					success : function(response){ $scope.DATA.agent_real_categories = response.data.real_categories; }
				});
			}
			//
		}
	}
	$scope.ProformaInvoiceSearchAgentPopUpSelectAgent = function(agent){
		$scope.DATA.selected_agents = [];
		$scope.DATA.selected_agents.push(agent);
		$scope.DATA.search.agent_code = agent.agent_code;
	}
	$scope.loadDataUnpaidBooking = function(page, reset_selected_booking_list){
		
		var data_search = angular.copy($scope.DATA.search);
		data_search.page = (page)?page:1;
		
		$scope._check_all = false;
		$scope.DATA.UnpaidBookings = {};
		$scope.DATA.UnpaidBookings.ready = false;
		$scope.DATA.UnpaidBookings.ready_to_create_invoice = false;
		
		if (reset_selected_booking_list){
			$scope.DATA.selected_booking_list = {};
			$scope.chk_booking_list_count_grand_total();
		}
		
		$scope.$root.data_search_ar_load_data_unpaid_booking = angular.copy(data_search);
		delete $scope.$root.data_search_ar_load_data_unpaid_booking.pagination;
		
		httpSrv.post({
			url 	: "ar/unpaid_booking_list",
			data	: $.param({"search":data_search}),
			success : function(response){
				$scope.DATA.UnpaidBookings = response.data;
				$scope.DATA.UnpaidBookings.ready = true;
				
				if (response.data.status == 'SUCCESS'){
					$scope.DATA.search = angular.copy($scope.DATA.UnpaidBookings.search);
					$scope.DATA.search.pagination = [];
					for (i=1;i<=$scope.DATA.UnpaidBookings.search.number_of_pages; i++){
						$scope.DATA.search.pagination.push(i);
					}
				}
				
				if (data_search.agent_code && data_search.agent_code != ''){
					$scope.DATA.UnpaidBookings.ready_to_create_invoice = true;
				}
			},
			error : function (response){}
		});
		
		if (data_search.agent_code && data_search.agent_code != ''){
			// UnInvoicing
			httpSrv.post({
				url 	: "invoice/unInvoicing/",
				data	: $.param({"search":{"agent_code":data_search.agent_code}}),
				success : function(response){
					//console.log(response.data);
					if (response.data.status == "SUCCESS"){
						$scope.DATA.uninvoicing = response.data.summary;
					}
				},
				error : function (response){}
			});
			
			// Load Data Agent
			httpSrv.post({
				url 	: "agent/detail/"+data_search.agent_code,
				success : function(response){
					if (response.data.status == "SUCCESS"){
						$scope.DATA.AGENT = response.data;
						if (!$scope.DATA.selected_agents){
							$scope.DATA.selected_agents = [];
							$scope.DATA.selected_agents.push(response.data);
						}
					}
				},
				error : function (response){}
			});
		}
	}
	$scope.chk_booking_list_uncheck_all = function(){
		if ($scope.DATA.UnpaidBookings.bookings){
			for (x in $scope.DATA.UnpaidBookings.bookings){
				var _booking = angular.copy($scope.DATA.UnpaidBookings.bookings[x]);
				delete $scope.DATA.selected_booking_list[_booking.booking_code];
			}
		}
		$scope.chk_booking_list_count_grand_total();
	}
	$scope.chk_booking_list_check_all = function(){
		if ($scope.DATA.UnpaidBookings.bookings){
			for (x in $scope.DATA.UnpaidBookings.bookings){
				var _booking = angular.copy($scope.DATA.UnpaidBookings.bookings[x]);
				delete _booking.detail;
				$scope.DATA.selected_booking_list[_booking.booking_code] = _booking;
			}
		}
		$scope.chk_booking_list_count_grand_total();
	}
	$scope.chk_booking_list = function(event, booking){
		if (event.target.checked){
			var _booking = angular.copy(booking);
			delete _booking.detail;
			$scope.DATA.selected_booking_list[booking.booking_code] = _booking;
		}else{
			if ($scope.DATA.selected_booking_list[booking.booking_code]){
				delete $scope.DATA.selected_booking_list[booking.booking_code];
			}
		}
		$scope.chk_booking_list_count_grand_total();
	}
	$scope.chk_booking_list_count_grand_total = function(){
		
		$scope.DATA.GrandTotalSelectedBooking = 0;
		
		for (booking_code in $scope.DATA.selected_booking_list){
			var _booking = $scope.DATA.selected_booking_list[booking_code];
			$scope.DATA.GrandTotalSelectedBooking += _booking.balance;
		}
	}
	$scope.save_proforma_invoice = function(){
		if ($scope.DATA.GrandTotalSelectedBooking > 0){
			
			var data_submit = {};
			data_submit.remarks 	= $scope.DATA.UnpaidBookings.remarks;
			data_submit.bank_id 	= $scope.DATA.UnpaidBookings.bank_id;
			data_submit.agent_code 	= $scope.DATA.AGENT.agent_code;
			data_submit.type		= $scope.DATA.search.type;
			data_submit.selected_booking_code = [];
			
			for (booking_code in $scope.DATA.selected_booking_list){
				data_submit.selected_booking_code.push(booking_code);
			}
			
			$scope.DATA.UnpaidBookings.is_submit_data = true;
			
			//console.log(data_submit);
			//return;
			httpSrv.post({
				url 	: "ar/create_proforma_invoice",
				data	: $.param(data_submit),
				success : function(response){
					if (response.data.status == "SUCCESS"){
						if($scope.DATA.search.type=="OPENVOUCHER"){
							window.location = "#/invoice/openvoucher/"+response.data.invoice_code;
						}else{
							window.location = "#/ar/proforma_invoice_detail/"+response.data.invoice_code;
						}
						toastr.success("Saved...");
					}else{
						$scope.DATA.current_invoice.error_desc = response.data.error_desc;
						$(event.target).find("button:submit").removeAttr("disabled");
						$('html, body').animate({scrollTop:200}, 400);
						$scope.DATA.UnpaidBookings.is_submit_data = false;
					}
				}
			});
			//console.log(data_submit);
		}else{
			alert("Please select at least 1 booking.");
		}
	}
	//--
	
	$scope.InvoiceListSearchAgentPopUp = function(){
		if ($scope.search.booking_source == 'SELECTAGENT'){
			$scope.DATA.selected_agents = [];
			
			$("#modal-search-agent").modal("show");
			
			//Load Data Agent
			if (!$scope.DATA.agents){
				httpSrv.post({
					url 	: "agent",
					data	: $.param({"search":{"limit":"all"}}),
					success : function(response){ $scope.DATA.agents = response.data; }
				});
			}
			//Load Data Real Category Agent
			if (!$scope.DATA.agent_real_categories){
				httpSrv.get({
					url 	: "agent/real_category",
					success : function(response){ $scope.DATA.agent_real_categories = response.data.real_categories; }
				});
			}
			//
		}
	}
	$scope.InvoiceListSearchAgentPopUpSelectAgent = function(agent){
		$scope.DATA.selected_agents = [];
		$scope.DATA.selected_agents.push(agent);
		$scope.search.booking_source = agent.agent_code;
	}
	$scope.loadDataInvoices = function(page, is_get_proforma_invoice){
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		
		if (!page) page = 1;
		
		var myDate = new Date();
		var nextDay = new Date(myDate);
		$scope.DATA.invoice = false;
		nextDay.setDate(myDate.getDate());
		
		if (!$scope.search) $scope.search = {};
		var _search = $scope.search;
			_search.page = page;
		
		var data_search = {"search":_search};
		if (_search.booking_source && _search.booking_source != ''){
			data_search.agent_code = _search.booking_source;
		}
		if (is_get_proforma_invoice){
			delete data_search.search.status;
		}
		httpSrv.post({
			url 	: "ar/"+(is_get_proforma_invoice?"proforma_invoice":"invoice"),
			data	: $.param(data_search),
			success : function(response){
				
				$scope.DATA.invoice = response.data;
				
				if ($scope.DATA.invoice.search){
					$scope.DATA.invoice.search.pagination = [];
					for (i=1;i<=$scope.DATA.invoice.search.number_of_pages; i++){
						$scope.DATA.invoice.search.pagination.push(i);
					}
				}
			},
			error : function (response){}
		});
		
		/*$scope.dataSearch = {};
		$scope.dataSearch.search = {};
		$scope.dataSearch.search = {
			"agent_code": 'ALL',
			"q"	: _search.q,
		};*/
		
		// Invoice Summary
		$scope.DATA.invoice_summary = false;
		httpSrv.post({
			url 	: "ar/invoice_summary/",
			data	: $.param(data_search),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.DATA.invoice_summary = response.data;
				}
			},
			error : function (response){}
		});

		// UnInvoicing
		/*httpSrv.post({
			url 	: "invoice/unInvoicing/",
			data	: $.param($scope.dataSearch),
			success : function(response){
				
				if (response.data.status == "SUCCESS"){
					$scope.DATA.uninvoicing = response.data.summary;
					
				}
			},
			error : function (response){}
		});*/
	}
	$scope.loadDataInvoiceDetail = function(is_reload,after_load_handler_function){
		if(is_reload){$scope.DATA.current_invoice = false;}
		invoice_code = $stateParams.invoice_code; 
		
		httpSrv.post({
			url 	: "ar/detail/" + invoice_code,
			success : function(response){
				var invoice = response.data;
				invoice.type = "RESERVATION";
				if(invoice.invoice){
					var regex = /<br\s*[\/]?>/gi;
					var remarks = invoice.invoice.remarks;
					invoice.invoice.remarks = remarks.replace(regex, "\n")
				}
				if (fn.isFn(after_load_handler_function)) {
					if (invoice.status == "SUCCESS"){
						$scope.DATA.current_invoice = invoice;
						after_load_handler_function(invoice);
					}
				}else{
					$scope.DATA.current_invoice = invoice;
					if (invoice.status != "SUCCESS"){
						alert("Sorry, invoice not found..");
						window.location = "#/invoice";
					}
				}
			},
			error : function (response){}
		});
	}
	
	//Create Invoice From Proforma Invoice
	$scope.createInvoiceFromProformaInvoice = function(){
		$scope.DATA.myInvoice = {};
		$scope.DATA.myInvoice.invoice_code = $scope.DATA.current_invoice.invoice.invoice_code;
		$scope.DATA.myInvoice.bank_id = ($scope.DATA.current_invoice.invoice.bank_account?$scope.DATA.current_invoice.invoice.bank_account.id:"");
		$scope.DATA.myInvoice.remarks = $scope.DATA.current_invoice.invoice.remarks;
		
		var myDate = new Date();
		var due_date = new Date(myDate);
			due_date.setDate(myDate.getDate()+7);
				
		$scope.DATA.myInvoice.due_date = fn.formatDate(due_date, "yy-mm-dd");
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd",minDate:0});
		
		//Load Rek Number
		if (!$scope.DATA.vendor_account_number){
			httpSrv.post({
				url 	: "vendor/getRekeningData",
				success : function(response){
					if (response.data.status == 'SUCCESS'){
						$scope.DATA.vendor_account_number = response.data.bank;
					}
				},
				error : function (response){}
			});
		}
		//--
	
	}
	$scope.saveCreateInvoice = function(event){
		
		var data_submit = angular.copy($scope.DATA.myInvoice);
		$scope.DATA.myInvoice.error_msg = [];
		$(event.target).find("button:submit").attr("disabled","disabled");
		
		httpSrv.post({
			url 	: "ar/create_invoice",
			data	: $.param(data_submit),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					//if($scope.DATA.search.type=="OPENVOUCHER"){
					//	window.location = "#/invoice/openvoucher/"+response.data.invoice_code;
					//}else{
					$("#modal-create-invoice").modal("hide");
					$('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
					alert("Success...");
					window.location = "#/ar/invoice_detail/"+response.data.invoice_code;
					//}
					toastr.success("Saved...");
				}else{
					$scope.DATA.myInvoice.error_desc = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
			}
		});
	}
	//--
	
	//Invoice Payment
	$scope.loadDataInvoicePayment = function(){
		invoice_code = $stateParams.invoice_code;
		
		/*if ($scope.$parent.DATA.current_invoice){
			$scope.DATA.current_invoice = $scope.$parent.DATA.current_invoice;
		}else{
			$scope.loadDataInvoiceDetail(invoice_code, function(invoice_detail){
				$scope.DATA.current_invoice = invoice_detail;
			});
		}*/
		
		httpSrv.get({
			url 	: "ar/invoice_payment_information/" + invoice_code,
			success : function(response){
				$scope.DATA.payment = response.data;
				if($scope.DATA.payment.payment.invoice_type=="OPENVOUCHER"){
					$scope.loadDataInvoiceDetailOT();
				}else{
					$scope.loadDataInvoiceDetail();
				}
			},
			error : function (response){}
		});

	}
	$scope.addEditPayment = function (current_payment){
		invoice_code = $stateParams.invoice_code;
		/*if($scope.DATA.payment.payment.invoice_type=="OPENVOUCHER"){
			$scope.loadDataInvoiceDetailOT();
		}else{
			$scope.loadDataInvoiceDetail();
		}*/
		if (current_payment){
			$scope.DATA.myPayment = angular.copy(current_payment);
		}else{
			/*$scope.DATA.myPayment = {"booking_code" : $scope.DATA.current_invoice.invoice.invoice_code,
									 "date" : fn.formatDate(new Date(),"yy-mm-dd"),
									 "description":"Payment #"+$scope.DATA.current_invoice.invoice.invoice_code,
									 "currency" : $scope.DATA.payment.payment.currency,
									 "amount" : $scope.DATA.payment.payment.balance};
			*/
			$scope.DATA.myPayment = {"booking_code" : invoice_code,
									 "date" : fn.formatDate(new Date(),"yy-mm-dd"),
									 "description":"Payment #"+invoice_code,
									 "currency" : $scope.DATA.payment.payment.currency,
									 "amount" : $scope.DATA.payment.payment.balance};
		}
		
		//Check Agent Deposit
		var agent_code = $scope.DATA.current_invoice.invoice.agent.agent_code;
		$scope.DATA.myPayment.deposit = false;
		httpSrv.post({
			url 	: "agent/deposit",
			data	: $.param({"publish_status":"ALL", "agent_code":agent_code}),
			success : function(response){
				if (response.data.status == 'SUCCESS'){
					if (response.data.deposit.current_deposit > 0){
						$scope.DATA.myPayment.deposit = response.data.deposit;
					}
				}
			},
			error : function (response){}
		});
			
		//Load Data Payment Method
		if (!$scope.$root.DATA_payment_method){
			httpSrv.post({
				url 	: "setting/payment_method/in",
				success : function(response){ $scope.$root.DATA_payment_method = response.data.payment_method; },
			});
		}
		
		//Load Data Available Currency
		delete $scope.DATA.currency_converter;
		if (!$scope.$root.DATA_available_currency){
			httpSrv.post({
				url 	: "currency_converter/available_currency",
				success : function(response){ 
					if (response.data.status == 'SUCCESS' && response.data.currency && response.data.currency.length > 0){
						$scope.$root.DATA_available_currency 	= response.data; 
						$scope.DATA.myPayment.payment_currency 	= angular.copy(response.data.default_currency);
						$scope.DATA.myPayment.payment_amount	= angular.copy($scope.DATA.myPayment.amount);
						$scope.DATA.myPayment.default_currency 	= angular.copy(response.data.default_currency);
					}
				},
			});
		}else{
			$scope.DATA.myPayment.payment_currency 	= $scope.$root.DATA_available_currency.default_currency;
			$scope.DATA.myPayment.payment_amount	= angular.copy($scope.DATA.myPayment.amount);
			$scope.DATA.myPayment.default_currency 	= $scope.$root.DATA_available_currency.default_currency;
		}
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
	}
	$scope.addEditPaymentPayWithDeposit = function(){
		if ($scope.DATA.myPayment.deposit){
			$scope.DATA.myPayment.payment_type = 'DEPOSIT';
			
			var balance_payment = $scope.DATA.payment.payment.balance;
			var deposit_remaining = $scope.DATA.myPayment.deposit.current_deposit;
			
			$scope.DATA.myPayment.amount = (deposit_remaining<balance_payment)?deposit_remaining:balance_payment;
			
		}
	}
	$scope.convert_currency = function (){
		if ($scope.$root.DATA_available_currency.currency){
			var data = {"from_currency"	: $scope.DATA.myPayment.default_currency,
						"to_currency"	: $scope.DATA.myPayment.payment_currency,
						"amount"		: $scope.DATA.myPayment.amount}
			$scope.DATA.myPayment.payment_currency_loading = true;
			httpSrv.post({
					url 	: "currency_converter/convert",
					data	: $.param(data),
					success : function(response){ 
						if (response.data.status == 'SUCCESS'){
							$scope.DATA.currency_converter = response.data;
							$scope.DATA.myPayment.payment_amount = response.data.convertion.convertion_amount;
							$scope.DATA.myPayment.payment_currency = response.data.convertion.to;
						}
						$scope.DATA.myPayment.payment_currency_loading = false;
					},
				});
		}
	}
	$scope.changePaymentType = function(){
		
		$scope.DATA.payment_is_cc = false;
		$scope.DATA.payment_is_atm = false;
		
		
		for (index in $scope.$root.DATA_payment_method){
			var payment_method = $scope.$root.DATA_payment_method[index];
			if (payment_method.code == $scope.DATA.myPayment.payment_type){
				if (payment_method.type == 'CC'){
					$scope.DATA.payment_is_cc = true;
					$scope.DATA.payment_is_atm = false;
				}else if (payment_method.type == 'TRANSFER'){
					$scope.DATA.payment_is_cc = false;
					$scope.DATA.payment_is_atm = true;
				}else{
					$scope.DATA.payment_is_cc = false;
					$scope.DATA.payment_is_atm = false;
				}
				break;
			}
		}
			
	}
	$scope.saveDataPayment = function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myPayment.error_msg = [];
		
		var data = angular.copy($scope.DATA.myPayment);
		delete data.deposit;

		httpSrv.post({
			url 	: "ar/invoice_add_payment/",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataInvoicePayment();
					$("#add-edit-payment").modal("hide");
					toastr.success("Saved...");
					$scope.loadDataInvoiceDetail(true, function(invoice_detail){
						$scope.$parent.DATA.current_invoice = invoice_detail;
					});
				}else{
					$scope.DATA.myPayment.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
			}
		});
	}
	
	$scope.submitDeletePayment = function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.myPaymentDetail.error_msg = [];
		
		var data = {
					"booking_code"  : $scope.DATA.current_invoice.invoice.invoice_code,
					"payment_code" 	: $scope.myPaymentDetail.payment_code,
					"delete_remarks": $scope.myPaymentDetail.delete_remarks
					};
		
		httpSrv.post({
			url 	: "ar/invoice_delete_payment/",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataInvoicePayment();
					$("#payment-detail").modal("hide");
					toastr.success("Deleted...");
					$scope.loadDataInvoiceDetail(true, function(invoice_detail){
						$scope.$parent.DATA.current_invoice = invoice_detail;
					});
				}else{
					$scope.myPaymentDetail.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
			}
		});
	}
	$scope.paymentDetail = function (current_payment, delete_payment){
		$scope.myPaymentDetail = angular.copy(current_payment);
		$scope.myPaymentDetail.amount = Math.abs($scope.myPaymentDetail.amount);
		if (delete_payment == true){
			$scope.myPaymentDetail.delete_payment = true;
			$scope.myPaymentDetail.sure_to_delete = '0';
		}else{
			delete $scope.myPaymentDetail.delete_payment;
		}
	}
	//--
	
	//Edit Bank Account
	$scope.invoiceDetailEditBankAccount = function(){
		var current_bank_account = {"id" : ""};
		if ($scope.DATA.current_invoice.invoice.bank_account){
			current_bank_account = angular.copy($scope.DATA.current_invoice.invoice.bank_account);
		}
		$scope.DATA.current_invoice.invoice.edit_bank_account = current_bank_account;
		
		if (!$scope.DATA.bank_account){
			httpSrv.post({
				url 	: "vendor/getRekeningData",
				success : function(response){
					$scope.DATA.bank_account = response.data.bank;
				},
				error : function (response){}
			});
		}
		
	}
	$scope.invoiceDetailUpdateBankAccount = function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.current_invoice.invoice.edit_bank_account.error_msg = [];
		
		var data = {"invoice_code" 	: $scope.DATA.current_invoice.invoice.invoice_code,
					"bank_id" 		: $scope.DATA.current_invoice.invoice.edit_bank_account.id}
		
		httpSrv.post({
			url 	: "ar/update_bank_account",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					toastr.success("Saved...");
					$scope.loadDataInvoiceDetail(true);
					$("#edit_bank_account").modal("hide");
				}else{
					$scope.DATA.current_invoice.invoice.edit_bank_account.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	//--
	
	//PRINT & SEND EMAIL INVOICE
	$scope.printInvoiceDetail = function(){
		invoice_code = $stateParams.invoice_code;
		var myDate = new Date();
		var nextDay = new Date(myDate);
		$scope.loadDataInvoiceDetail(false, function(){
			$scope.$parent.$parent.show_print_button = true;
			$scope.DATA.current_invoice.now = $filter('date')(nextDay, "yyyy-MM-dd");
		});
		
	}
	$scope.sendEmailInvoice = function(){
		var invoice = $scope.DATA.current_invoice.invoice;
		
		var agent_code = invoice.agent.agent_code; 
		
		if (!$scope.DATA.current_invoice.email_list_agent){
			httpSrv.post({
				url 	: "setting/email_bcc/"+agent_code,
				data	: $.param({"publish_status":"ALL"}),
				success : function(response){
					if (response.data.status == 'SUCCESS'){
						$scope.DATA.current_invoice.email_list_agent = response.data.email_bcc;
					}
				},
				error : function (response){}
			});
		}
	}
	$scope.sendEmailInvoiceSubmit = function(event){
		var invoice = $scope.DATA.current_invoice.invoice;
		$(event.target).find("button:submit").attr("disabled","disabled");
		
		var data_submit = {};
		data_submit.invoice_code = invoice.invoice_code;
		
		var email_list = [];
		for(x in $scope.DATA.current_invoice.email_list_agent){
			var _email = $scope.DATA.current_invoice.email_list_agent[x];
			if (_email.selected){
				email_list.push({"name":_email.name, "email":_email.email});
			}
		}
		if (email_list.length > 0){
			data_submit.email_list = email_list;
		}

		httpSrv.post({
			url 	: "ar/send_email_invoice/"+data_submit.invoice_code,
			data	: $.param(data_submit),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$("#frm-invoice-send-email").modal("hide");
					toastr.success("Email sent...");
				}else{
					$("#frm-invoice-send-email").modal("hide");
					toastr.warning("Email not sent...");
					//$scope.DATA.myPayment.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
			}
		});

	}
	//--
	
	$scope.deleteInvoice = function (invoice){
		
		if (confirm("Are you sure to delete this invoice ? \n\rClick OK to continue delete Invoice...")){
			
			httpSrv.get({
				url 	: "ar/delete_invoice/"+invoice.invoice_code,
				success : function(response){
					if (response.data.status == "SUCCESS"){
						window.location = "#/ar";
						toastr.success("Deleted...");
					}
				},
				error : function (response){}
			});
		}
	}
	
	
	//AR AGING
	$scope.loadDataAgentARAging = function(page, show_loading){
		if (!$scope.DATA.bookings || show_loading){
			$scope.show_loading_DATA_bookings = true;
		}
		
		if (!$scope.search){
			$scope.search = {};
			$scope.search.publish_status = "1";
		}
		
		if (!page) page = 1;
		
		var _search = $scope.search;
			_search.page = page;
		
		//Sumary Each Agent
		httpSrv.post({
			url 	: "ar/aging",
			data	: $.param({"search":_search}),
			success : function(response){
				$scope.show_loading_DATA_bookings = false;
				$scope.DATA.agents = response.data;
				
				if ($scope.DATA.agents.search){
					$scope.DATA.agents.search.pagination = [];
					for (i=1;i<=$scope.DATA.agents.search.number_of_pages; i++){
						$scope.DATA.agents.search.pagination.push(i);
					}
				}
			},
			error : function (response){}
		});
		//--
		
		//Summary Vendor
		httpSrv.post({
			url 	: "ar/aging_summary",
			//data	: $.param({"search":_search}),
			success : function(response){
				if (response.data.status == 'SUCCESS'){
					$scope.DATA.ar_aging_summary = response.data.ar_aging_summary;
				}else{
					$scope.DATA.ar_aging_summary = false;
				}
			},
			error : function (response){}
		});
		//--
		
	}
	//---
	
	
	
	
	
	
	$scope.xxloadDataUnpaidBooking = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.current_invoice.error_desc = [];
		var data = {};
		data.invoice_type = $scope.DATA.current_invoice.type;
		data.agent_code = $scope.DATA.current_invoice.agent_code;
		data.user_edit = $('input[name]').val();
		data.due_date = $scope.DATA.bookings.due;
		data.remarks = $scope.DATA.bookings.remarks;
		data.selected_booking = $scope.DATA.selectedlist;
		data.bank = $scope.DATA.bookings.bank;

		httpSrv.post({
			url 	: "invoice/"+(($scope.DATA.current_invoice.id)?"update":"create"),
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					if($scope.DATA.current_invoice.type=="OPENVOUCHER"){
						window.location = "#/invoice/openvoucher/"+response.data.invoice_code;
					}else{
						window.location = "#/invoice/detail/"+response.data.invoice_code;
					}
					toastr.success("Saved...");
				}else{
					$scope.DATA.current_invoice.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
					$('html, body').animate({scrollTop:200}, 400);
				}
			}
		});
	}
	
	
	
	$scope.xxloadDataInvoiceDetail = function(is_reload,after_load_handler_function){
		if(is_reload){$scope.DATA.current_invoice = false;}
		invoice_code = $stateParams.invoice_code; 
		
		httpSrv.post({
			url 	: "invoice/detail/" + invoice_code,
			success : function(response){
				var invoice = response.data;
				invoice.type = "RESERVATION";
				if(invoice.invoice){
					var regex = /<br\s*[\/]?>/gi;
					var remarks = invoice.invoice.remarks;
					invoice.invoice.remarks = remarks.replace(regex, "\n")
				}
				if (fn.isFn(after_load_handler_function)) {
					if (invoice.status == "SUCCESS"){
						$scope.DATA.current_invoice = invoice;
						after_load_handler_function(invoice);
					}
				}else{
					$scope.DATA.current_invoice = invoice;
					if (invoice.status != "SUCCESS"){
						alert("Sorry, invoice not found..");
						window.location = "#/invoice";
					}
				}
			},
			error : function (response){}
		});
	}
	$scope.xxloadDataInvoiceDetailOT = function(is_reload,after_load_handler_function){
		if(is_reload){$scope.DATA.current_invoice = false;}
		invoice_code = $stateParams.invoice_code; 
		
		httpSrv.post({
			url 	: "invoice/detail_open_voucher/" + invoice_code,
			success : function(response){
				var invoice = response.data;
				invoice.type = "OPENVOUCHER";
				if(invoice.invoice){
					var regex = /<br\s*[\/]?>/gi;
					var remarks = invoice.invoice.remarks;
					invoice.invoice.remarks = remarks.replace(regex, "\n")
				}
				if (fn.isFn(after_load_handler_function)) {
					if (invoice.status == "SUCCESS"){
						$scope.DATA.current_invoice = invoice;
						after_load_handler_function(invoice);
					}
				}else{
					$scope.DATA.current_invoice = invoice;
					if (invoice.status != "SUCCESS"){
						alert("Sorry, invoice not found..");
						window.location = "#/invoice";
					}
				}
			},
			error : function (response){}
		});
	}	
	$scope.xxremoveInvoice = function (invoice){
		
		if (confirm("Are you sure to delete this invoice ? \n\rClick Ok to continue delete Invoice...")){
			
			httpSrv.get({
				url 	: "invoice/remove/"+invoice.invoice_code,
				success : function(response){
					if (response.data.status == "SUCCESS"){
						window.location = "#/invoice";
					}
				},
				error : function (response){}
			});
		}
	}
	$scope.xxremoveInvoiceOT = function (invoice){
		
		if (confirm("Are you sure to delete this invoice ? \n\rClick Ok to continue delete Invoice...")){
			
			httpSrv.get({
				url 	: "invoice/remove_open_voucher/"+invoice.invoice_code,
				success : function(response){
					if (response.data.status == "SUCCESS"){
						window.location = "#/invoice";
					}
				},
				error : function (response){}
			});
		}
	}
	
	$scope.xxloadRekNumber = function(){
		httpSrv.post({
			url 	: "vendor/getRekeningData",
			success : function(response){
				$scope.DATA.rekening = response.data.bank;
				//console.log($scope.DATA.rekening);
			},
			error : function (response){}
		});
	}
	
	$scope.xxEditRekNumber = function(send_email){
		nvoice_code = $stateParams.invoice_code;
		$scope.send_this_to_email = 0;
		if(send_email){
			$scope.send_this_to_email = 1;
		}
		httpSrv.get({
			url 	: "invoice/getbankaccount/" + invoice_code,
			success : function(response){
				$scope.DATA.bank_account_inv = response.data;
			},
			error : function (response){}
		});

		$scope.loadRekNumber();
	}
	$scope.xxsaveBankAccount = function(event,sendEmail){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.bank_account_inv.error_msg = [];
		var data = {};
		data.invoice_code = $scope.DATA.current_invoice.invoice.invoice_code;
		data.bank = $scope.DATA.bank_account_inv.bank_account.id;
		
		httpSrv.post({
			url 	: "invoice/bankSave",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataBookingPayment();
					$("#edit_bank_account").modal("hide");
					toastr.success("Saved...");
					if(sendEmail==1){
						$window.open('resend_email/#/email/invoice_agent/'+data.invoice_code, '_blank');
					}
				}else{
					$scope.DATA.bank_account_inv.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
			}
		});
	}
	// init payment detail
	$scope.xxloadDataInvoicePayment = function(){
		invoice_code = $stateParams.invoice_code;
		
		/*if ($scope.$parent.DATA.current_invoice){
			$scope.DATA.current_invoice = $scope.$parent.DATA.current_invoice;
		}else{
			$scope.loadDataInvoiceDetail(invoice_code, function(invoice_detail){
				$scope.DATA.current_invoice = invoice_detail;
			});
		}*/
		
		httpSrv.get({
			url 	: "invoice/reservation/" + invoice_code,
			success : function(response){
				$scope.DATA.payment = response.data;
				if($scope.DATA.payment.payment.invoice_type=="OPENVOUCHER"){
					$scope.loadDataInvoiceDetailOT();
				}else{
					$scope.loadDataInvoiceDetail();
				}
			},
			error : function (response){}
		});

	}
	// init payment detail
	$scope.xxloadDataInvoicePaymentOT = function(){
		invoice_code = $stateParams.invoice_code;
		
		/*if ($scope.$parent.DATA.current_invoice){
			$scope.DATA.current_invoice = $scope.$parent.DATA.current_invoice;
		}else{
			$scope.loadDataInvoiceDetail(invoice_code, function(invoice_detail){
				$scope.DATA.current_invoice = invoice_detail;
			});
		}*/
		
		httpSrv.get({
			url 	: "invoice/reservation/" + invoice_code,
			success : function(response){
				$scope.DATA.payment = response.data;
			},
			error : function (response){}
		});
		$scope.loadDataInvoiceDetailOT();
	}

	// button + Add Payment clicked
	$scope.xxaddEditPayment = function (current_payment){
		invoice_code = $stateParams.invoice_code;
		/*if($scope.DATA.payment.payment.invoice_type=="OPENVOUCHER"){
			$scope.loadDataInvoiceDetailOT();
		}else{
			$scope.loadDataInvoiceDetail();
		}*/
		if (current_payment){
			$scope.DATA.myPayment = angular.copy(current_payment);
		}else{
			/*$scope.DATA.myPayment = {"booking_code" : $scope.DATA.current_invoice.invoice.invoice_code,
									 "date" : fn.formatDate(new Date(),"yy-mm-dd"),
									 "description":"Payment #"+$scope.DATA.current_invoice.invoice.invoice_code,
									 "currency" : $scope.DATA.payment.payment.currency,
									 "amount" : $scope.DATA.payment.payment.balance};
			*/
			$scope.DATA.myPayment = {"booking_code" : invoice_code,
									 "date" : fn.formatDate(new Date(),"yy-mm-dd"),
									 "description":"Payment #"+invoice_code,
									 "currency" : $scope.DATA.payment.payment.currency,
									 "amount" : $scope.DATA.payment.payment.balance};
		}
		//Load Data Payment Method
		if (!$scope.$root.DATA_payment_method){
			httpSrv.post({
				url 	: "setting/payment_method",
				success : function(response){ $scope.$root.DATA_payment_method = response.data.payment_method; },
			});
		}
		
		//Load Data Available Currency
		delete $scope.DATA.currency_converter;
		if (!$scope.$root.DATA_available_currency){
			httpSrv.post({
				url 	: "currency_converter/available_currency",
				success : function(response){ 
					if (response.data.status == 'SUCCESS' && response.data.currency && response.data.currency.length > 0){
						$scope.$root.DATA_available_currency 	= response.data; 
						$scope.DATA.myPayment.payment_currency 	= angular.copy(response.data.default_currency);
						$scope.DATA.myPayment.payment_amount	= angular.copy($scope.DATA.myPayment.amount);
						$scope.DATA.myPayment.default_currency 	= angular.copy(response.data.default_currency);
					}
				},
			});
		}else{
			$scope.DATA.myPayment.payment_currency 	= $scope.$root.DATA_available_currency.default_currency;
			$scope.DATA.myPayment.payment_amount	= angular.copy($scope.DATA.myPayment.amount);
			$scope.DATA.myPayment.default_currency 	= $scope.$root.DATA_available_currency.default_currency;
		}
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
	}
	$scope.xxconvert_currency = function (){
		if ($scope.$root.DATA_available_currency.currency){
			var data = {"from_currency"	: $scope.DATA.myPayment.default_currency,
						"to_currency"	: $scope.DATA.myPayment.payment_currency,
						"amount"		: $scope.DATA.myPayment.amount}
			$scope.DATA.myPayment.payment_currency_loading = true;
			httpSrv.post({
					url 	: "currency_converter/convert",
					data	: $.param(data),
					success : function(response){ 
						if (response.data.status == 'SUCCESS'){
							$scope.DATA.currency_converter = response.data;
							$scope.DATA.myPayment.payment_amount = response.data.convertion.convertion_amount;
							$scope.DATA.myPayment.payment_currency = response.data.convertion.to;
						}
						$scope.DATA.myPayment.payment_currency_loading = false;
					},
				});
		}
	}
	$scope.xxchangePaymentType = function(){
		
		$scope.DATA.payment_is_cc = false;
		$scope.DATA.payment_is_atm = false;
		
		
		for (index in $scope.$root.DATA_payment_method){
			var payment_method = $scope.$root.DATA_payment_method[index];
			if (payment_method.code == $scope.DATA.myPayment.payment_type){
				if (payment_method.type == 'CC'){
					$scope.DATA.payment_is_cc = true;
					$scope.DATA.payment_is_atm = false;
				}else if (payment_method.type == 'TRANSFER'){
					$scope.DATA.payment_is_cc = false;
					$scope.DATA.payment_is_atm = true;
				}else{
					$scope.DATA.payment_is_cc = false;
					$scope.DATA.payment_is_atm = false;
				}
				break;
			}
		}
			
	}
	// save payment
	$scope.xxsaveDataPayment = function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myPayment.error_msg = [];
		httpSrv.post({
			url 	: "invoice/payment/",
			data	: $.param($scope.DATA.myPayment),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataBookingPayment();
					$("#add-edit-payment").modal("hide");
					toastr.success("Saved...");
					$scope.loadDataInvoiceDetail(true, function(invoice_detail){
						$scope.$parent.DATA.current_invoice = invoice_detail;
					});
				}else{
					$scope.DATA.myPayment.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
			}
		});
	}

	// reload payment detail after submission
	$scope.xxloadDataBookingPayment = function(){
		invoice_code = $stateParams.invoice_code;
		
		/*if ($scope.$parent.DATA.current_invoice){
			$scope.DATA.current_invoice = $scope.$parent.DATA.current_invoice;
		}else{
			$scope.loadDataInvoiceDetail(booking_code, function(invoice_detail){
				$scope.DATA.current_invoice = invoice_detail;
			});
		}*/
		httpSrv.get({
			url 	: "invoice/reservation/"+invoice_code,
			success : function(response){
				$scope.DATA.payment = response.data;
				if($scope.DATA.payment.payment.invoice_type=="OPENVOUCHER"){
					$scope.loadDataInvoiceDetailOT();
				}else{
					$scope.loadDataInvoiceDetail();
				}
			},
			error : function (response){}
		});
		//console.log($scope.DATA);

	}

	// delete payment
	$scope.xxsubmitDeletePayment = function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.myPaymentDetail.error_msg = [];
		
		var data = {
					"booking_code"  : $scope.DATA.current_invoice.invoice.invoice_code,
					"payment_code" 	: $scope.myPaymentDetail.payment_code,
					"delete_remarks": $scope.myPaymentDetail.delete_remarks
					};
		
		httpSrv.post({
			url 	: "invoice/delete_payment/",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataBookingPayment();
					$("#payment-detail").modal("hide");
					toastr.success("Deleted...");
					$scope.loadDataInvoiceDetail(true, function(invoice_detail){
						$scope.$parent.DATA.current_invoice = invoice_detail;
					});
				}else{
					$scope.myPaymentDetail.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
			}
		});
	}
	$scope.xxpaymentDetail = function (current_payment, delete_payment){
		$scope.myPaymentDetail = angular.copy(current_payment);
		$scope.myPaymentDetail.amount = Math.abs($scope.myPaymentDetail.amount);
		if (delete_payment == true){
			$scope.myPaymentDetail.delete_payment = true;
			$scope.myPaymentDetail.sure_to_delete = '0';
		}else{
			delete $scope.myPaymentDetail.delete_payment;
		}
	}
	$scope.xxprintInvoiceDetailOpenVoucher = function(){
		invoice_code = $stateParams.invoice_code;
		var myDate = new Date();
		var nextDay = new Date(myDate);
		$scope.loadDataInvoiceDetailOT(false, function(){
			$scope.$parent.$parent.show_print_button = true;
			$scope.DATA.current_invoice.now = $filter('date')(nextDay, "yyyy-MM-dd");
		});
		
	}
	$scope.xxsendEmail = function(){
		invoice_code = $stateParams.invoice_code;
		$scope.DATA.invoice_code = invoice_code;
		var data = {};
		data.merchant_code = MC;
		data.merchant_key = MK;
		var param = $.param(data);	
		$scope.DATA.url = API_URL+"invoice/send_email/"+invoice_code+"?"+param;
	}
	
	
});
function activate_sub_menu_invioce_detail(class_menu){
	$(".invoice ul.nav.sub-nav li").removeClass("active");
	$(".invoice ul.nav.sub-nav li."+class_menu).addClass("active");
}

function activate_sub_menu_invoice(class_menu){
	$(".invoice ul.nav.sub-nav li").removeClass("active");
	$(".invoice ul.nav.sub-nav li."+class_menu).addClass("active");
}

// JavaScript Document 

angular.module('app').controller('refund_controller',function($scope, $http, httpSrv, $stateParams, $interval, fn){
	
	GeneralJS.activateLeftMenu("refund");	
	
	$scope.DATA = {};
	$scope.fn = fn;
	
	$scope.loadDataTefund = function(page){
		$scope.show_loading_DATA_bookings = true;
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		
		if (!$scope.search){ 
			var now = new Date(),
				start_date = new Date(),
				end_date = new Date();
			
			start_date.setDate(start_date.getDate() - 30);
			
			$scope.search = { "xstart_date" : $scope.fn.formatDate(start_date, "yy-mm-dd"),
							  "xend_date" : $scope.fn.formatDate(end_date, "yy-mm-dd"),}; 
		}
		
		if (!page) page = 1;
		
		var _search = $scope.search;
			_search.page = page;

		httpSrv.post({
			url 	: "refund",
			data	: $.param({"search":_search}),
			success : function(response){
				$scope.show_loading_DATA_bookings = false;
				$scope.DATA.refund = response.data;
				
				if ($scope.DATA.refund.search){
					$scope.DATA.refund.search.pagination = [];
					for (i=1;i<=$scope.DATA.refund.search.number_of_pages; i++){
						$scope.DATA.refund.search.pagination.push(i);
					}
				}
				//console.log($scope.DATA.refund);
			},
			error : function (response){}
		});
		
		//Load Data Booking Source
		if (!$scope.DATA.booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.DATA.booking_source= response.data; },
			});
		}
		
		//Load Data Booking Status
		if (!$scope.DATA.booking_status){
			httpSrv.post({
				url 	: "setting/booking_status",
				success : function(response){ $scope.DATA.booking_status = response.data; },
			});
		}
	}
	
	$scope.editDataRefund = function(refund){
		if (!refund.booking_info){
			
			booking_code = refund.booking_code;
		
			httpSrv.get({
				url 	: "booking/detail/"+booking_code,
				success : function(response){
					var booking = response.data;
					if (booking.status == "SUCCESS"){
						refund.booking_info = booking.booking;
					}
				},
				error : function (response){}
			});
		}
		$scope.DATA.myRefund = refund;
		$scope.DATA.myRefund.approval_status = "1";

		//Load Data Payment Method
		if (!$scope.$root.DATA_payment_method_out){
			httpSrv.post({
				url 	: "setting/payment_method/out",
				success : function(response){ $scope.$root.DATA_payment_method_out = response.data.payment_method; },
			});
		}
	}
	
	$scope.saveRefunds = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myRefund.error_desc = [];
		
		var data = {"refund_code" 			: $scope.DATA.myRefund.refund_code,
					"approval_status" 		: $scope.DATA.myRefund.approval_status,
					"payment_method_code"	: $scope.DATA.myRefund.payment_method_code}
		
		if ($scope.DATA.myRefund.booking_info.agent && $scope.DATA.myRefund.refund_to_deposit == '1'){
			delete data.payment_method_code;
			data.refund_to_deposit = '1';
		}
		
		httpSrv.post({
			url 	: "refund/update",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataTefund();
					$("#refund-detail-form").modal("hide");
					toastr.success("Saved...");
				}else{
					$scope.DATA.myRefund.error_desc = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	
});

function activate_sub_menu_agent_detail(class_menu){
	$(".reservation-detail ul.nav.sub-nav li").removeClass("active");
	$(".reservation-detail ul.nav.sub-nav li."+class_menu).addClass("active");
}


// JavaScript Document

angular.module('app').controller('checkin_controller',function($scope, $http, httpSrv, $stateParams, $interval, fn, $rootScope, $window){
	
	GeneralJS.activateLeftMenu("checkin");	
	
	$scope.DATA = {};
	$scope.fn = fn;


	// --> revisi gung ari
	$scope.loadDataBookingTransport_2 = function(page, show_loading){
		
		if (!$scope.DATA.bookings || show_loading){
			$scope.show_loading_DATA_bookings = true;
		}
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		
		if (!$scope.search){ 
			var now = new Date(),
				start_date = new Date(),
				end_date = new Date();
			
			//start_date.setDate(start_date.getDate() - 10);
			
			$scope.search = { "start_date" : $scope.fn.formatDate(start_date, "yy-mm-dd"),
							  "end_date" : $scope.fn.formatDate(end_date, "yy-mm-dd"),
							  "filter_by":"TRIPDATE"}; 
		}
		
		if ($scope.search.booking_source == 'SELECTAGENT'){
			$scope.search.booking_source = '';
		}

		if (!page) page = 1;
		
		var _search = angular.copy($scope.search);
			_search.page = page;
		
		if (_search.booking_source){
			var cek_agent = _search.booking_source.split("--");
			if (cek_agent[0] == "AGENT"){
				delete _search.booking_source;
				_search.agent_code = cek_agent[1];
			}
		}

		httpSrv.post({
			url 	: "transport/checkin/booking_and_voucher_list",
			data	: $.param({"search":_search}),
			success : function(response){
				$scope.show_loading_DATA_bookings = false;
				$scope.DATA.bookings = response.data;
				if($scope.DATA.bookings.status=="SUCCESS"){
					if ($scope.DATA.bookings.search){
						$scope.DATA.bookings.search.pagination = [];
						for (i=1;i<=$scope.DATA.bookings.search.number_of_pages; i++){
							$scope.DATA.bookings.search.pagination.push(i);
						}
						if ($scope.DATA.bookings.search.agent_code){
							_search.booking_source = 'AGENT--'+$scope.DATA.bookings.search.agent_code;
						}
					}
				}else{
					$scope.DATA.bookings.error_msg = $scope.DATA.bookings.error_desc;
				}
			},
			error : function (response){}
		});
		
		//Load Data Booking Source
		if (!$scope.$root.DATA_booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.$root.DATA_booking_source = response.data.booking_source; },
			});
		}
		
		//Load Data Booking Status
		if (!$scope.$root.DATA_booking_status){
			httpSrv.post({
				url 	: "setting/booking_status",
				success : function(response){ $scope.$root.DATA_booking_status = response.data.booking_status; },
			});
		}
	}

	$scope.ReservationListSearchAgentPopUp = function(){
		

		if ($scope.search.booking_source == 'SELECTAGENT'){
			$scope.DATA.selected_agents = [];
			
			$("#modal-search-agent").modal("show");
			
			//Load Data Agent
			if (!$scope.DATA.agents){
				httpSrv.post({
					url 	: "agent",
					data	: $.param({"search":{"limit":"all"}}),
					success : function(response){ $scope.DATA.agents = response.data; }
				});
			}
			//Load Data Real Category Agent
			if (!$scope.DATA.agent_real_categories){
				httpSrv.get({
					url 	: "agent/real_category",
					success : function(response){ $scope.DATA.agent_real_categories = response.data.real_categories; }
				});
			}
			//
		}
	}

	$scope.ReservationListSearchAgentPopUpSelectAgent = function(agent){

		$scope.DATA.selected_agents = [];
		$scope.DATA.selected_agents.push(agent);
		$scope.search.booking_source = "AGENT--"+agent.agent_code;
	}
	// revisi gung ari <--

	$scope.checkInVoucherTrans = function(){

		//$scope.check_voucher_code = "OT-BEE1709NC9E";
		//$scope.getVoucherInformation();
		//$scope.check_voucher_code = "OT-BEE1709KR79";
		$scope.show_getVoucherInformation = false;
	}

	//Booking Detail
	$scope.loadDataBookingDetail = function(booking_code, after_load_handler_function){
		
		if (!booking_code){ booking_code = $stateParams.booking_code; }
		
		httpSrv.get({
			url 	: "booking/detail/"+booking_code,
			success : function(response){
				var booking = response.data;
				//console.log(booking);
				if (booking.status == "SUCCESS"){
					$scope.DATA.current_booking = booking;
					
					if (fn.isFn(after_load_handler_function)) {
						after_load_handler_function(booking);
					}
				}else{
					alert("Sorry, data not found..");
					window.location = "#/reservation";
				}
			},
			error : function (response){}
		});
	}

	$scope.checkinPassenger = function(passenger_code){

		httpSrv.post({
			url 	: "transport/checkin/checkin_passenger/",
			data	: $.param({"passenger_code":passenger_code}),
			success : function(response){
				if (response.data.status == "SUCCESS") {
					toastr.success("Check-In Success");
				}
			},
			error : function (response){}
		});

	}

	//Billing Statement & Payment
	$scope.loadDataPayment = function(data){

		$scope.loadDataBookingDetail(data.voucher.booking_code, function(booking_detail){
			$scope.DATA.current_booking = booking_detail;
		});
		
		httpSrv.get({
			url 	: "payment/reservation/"+data.voucher.booking_code,
			success : function(response){
				$scope.DATA.payment = response.data;
				

				if ($scope.DATA.payment.payment.balance_real_payment_with_invoice == 0) {
					$scope.checkinPassenger(data.voucher.detail[0].passenger.passenger_code);
				}else{

					toastr.warning("Please complete the payment");
					toastr.warning("Check-In failed");
				}
				//console.log($scope.DATA.payment);
			},
			error : function (response){}
		});
		
		httpSrv.get({
			url 	: "refund/reservation/"+ data.voucher.booking_code,
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.DATA.refund = response.data;
				}
			},
			error : function (response){}
		});
	}
	
	$scope.trustAsUrl = function(url, code){
		
     	return url + code;
     }

	$scope.printReceipt = function(){
		var passenger_code = $stateParams.passenger_code;
		$scope.DATA.voucher = {};
		$scope.show_getVoucherInformation = false;
		$scope.loading_print = true;

		httpSrv.post({
			url 	: "transport/checkin/print_participant/",
			data	: $.param({"passenger_code":passenger_code}),
			success : function(response){
				var voucher = response.data;

				if (voucher.status == "SUCCESS"){
					//If the code is voucher booking
					$scope.show_getVoucherInformation = true;
					if (voucher.voucher){
						$scope.DATA.voucher = voucher;
					}
					$scope.PARTICIPANT_CODE = $scope.DATA.voucher.voucher.detail[0].passenger.passenger_code;
					
					$scope.printFunction();
					$scope.loading_print = false;
				}else{
					$scope.show_getVoucherInformation = true;
					angular.forEach(response.data.error_desc, function(row){
						toastr.error(row);
					});

					$("#check_voucher_code").focus();
					$("#check_voucher_code").val("");
				}
				//$scope.show_getVoucherInformation = false;
			},
			error : function (response){}
		});
	}

	$scope.printFunction = function(){

		this.afterRender = function () {
	        $window.print();
	    },
		
		setTimeout(this.afterRender.bind(this), 0);
	    
	}


	$scope.getVoucherParticipant = function(){
		var voucher_code = $scope.check_voucher_code;
		$scope.DATA.voucher = {};
		$scope.show_getVoucherInformation = false;
		
		httpSrv.post({
			url 	: "transport/checkin/participant_checkin/",
			data	: $.param({"passcode":voucher_code}),
			success : function(response){
				var voucher = response.data;

				if (voucher.status == "SUCCESS"){
					//If the code is voucher booking
					$scope.show_getVoucherInformation = true;
					if (voucher.voucher){
						$scope.DATA.voucher = voucher;

					}

					$scope.loadDataPayment($scope.DATA.voucher);

					
					
				}else{
					$scope.show_getVoucherInformation = true;
					angular.forEach(response.data.error_desc, function(row){
						toastr.error(row);
					});

					$("#check_voucher_code").focus();
					$("#check_voucher_code").val("");
				}
				//$scope.show_getVoucherInformation = false;
			},
			error : function (response){}
		});
	}

	$scope.getVoucherInformation = function(){
		var voucher_code = $scope.check_voucher_code;
		$scope.DATA.voucher = {};
		$scope.show_getVoucherInformation = true;
		
		httpSrv.post({
			url 	: "transport/checkin/voucher/",
			data	: $.param({"voucher_code":voucher_code}),
			success : function(response){
				var voucher = response.data;

				if (voucher.status == "SUCCESS"){
					//If the code is voucher booking
					if (voucher.voucher){
						$scope.DATA.voucher = voucher;
						if ($scope.DATA.voucher.voucher.booking.balance > 0){
							$scope.DATA.voucher.allow_to_checkin = false;
							if($scope.DATA.voucher.voucher.booking.agent){
								if ($scope.DATA.voucher.voucher.booking.agent.payment_type_code=="ACL"){
									$scope.DATA.voucher.allow_to_checkin = true;
								}
							}
						}else if ($scope.DATA.voucher.voucher.booking.status_code == 'CANCEL' || $scope.DATA.voucher.voucher.voucher.booking_detail_status_code == 'CANCEL'){
							$scope.DATA.voucher.allow_to_checkin = false;
							$scope.DATA.voucher.is_canceled = true;
						}else{
							$scope.DATA.voucher.allow_to_checkin = true;
							
							for (i in $scope.DATA.voucher.voucher.voucher.passenger){
								if ($scope.DATA.voucher.voucher.voucher.passenger[i].checkin_status == '1'){
									$scope.DATA.voucher.voucher.voucher.passenger[i].already_checkin = '1';
								}
							}
							
						}
						//console.log($scope.DATA.voucher.voucher.booking.balance);
						$scope.is_checkedin_proccessed = false;
					}
					//If the code is open voucher
					else if (voucher.openticket){
						$scope.DATA.openticket = voucher.openticket;
						
						//If has been used
						if ($scope.DATA.openticket.used){
							alert("This code has been used before...");
						}
						//If Still available (Not used)
						else if ($scope.DATA.openticket.notyet_used){
							$("#frm-openticket-checkin").modal("show");
							
							$scope.DATA.data_rsv_OT = {};
							$scope.DATA.data_rsv_OT.checkin_now = '1';
							$scope.check_availabilities_data = {};
							$scope.add_trip = {};
							$scope.add_trip.departure_date = fn.formatDate(new Date(), "yy-mm-dd");
							
							//$scope.DATA.data_rsv_OT = {"customer":{"first_name":"Suta","last_name":"Darmayasa","country_code":"ID"},"remarks":"test"};
							
							//$scope.add_trip.arrival_port_id = '13';				// <-----------------
							//$scope.add_trip.departure_port_id = '1';			// <-----------------
							
							//Load Data Available Port
							if (!$scope.$root.DATA_available_port){
								httpSrv.post({
									url 	: "transport/port/available_port",
									success : function(response){ $scope.$root.DATA_available_port = response.data.ports;},
								});
							}
							//--
							
							if (!$scope.$root.DATA_country_list){
								httpSrv.get({
									url 	: "general/country_list",
									success : function(response){ $scope.$root.DATA_country_list = response.data.country_list; }
								});
							}
							
							$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
						}
					}
				}else{
					alert("Sorry, voucher not found..");
					$("#check_voucher_code").focus();
					$("#check_voucher_code").val("");
				}
				$scope.show_getVoucherInformation = false;
			},
			error : function (response){}
		});
	}
	
	$scope.check_availability_openticket = function(event){
		//$.showLoading();
		
		var data = angular.copy($scope.add_trip);
			data.adult = 1;
		
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.add_trip.error_desc = [];
		$scope.check_availabilities_data = {};
		$scope.check_availabilities_show_loading = true;
		
		httpSrv.post({
			url 	: "transport/checkin/check_availabilities",
			data	: $.param(data),
			success : function(response){
				$(event.target).find("button:submit").removeAttr("disabled");
				$scope.check_availabilities_show_loading = false;
				$scope.check_availabilities_show_searching_form = false;
				
				if (response.data.status == "SUCCESS"){
					$scope.check_availabilities_data = response.data;
					
					//console.log($scope.check_availabilities_data);
				}else{
					$scope.add_trip.error_desc = response.data.error_desc;
					//$('html, body').animate({scrollTop:200}, 400);
				}
				
				//fn.pre($scope.check_availabilities_data, "check_availabilities_data");
				//$.hideLoading();
			}
		});
	
	}
	$scope.check_availabilities_select_this_trip = function (trip, trips_parent){
		//fn.pre(trip);
		for (index in trips_parent.availabilities){
			if (trips_parent.selected_trip){
				delete trips_parent.availabilities[index].is_hidden;
			}else{
				trips_parent.availabilities[index].is_hidden = true;
			}
			delete trips_parent.availabilities[index].is_selected;
		}
		if (trips_parent.selected_trip){
			delete trips_parent.selected_trip;
			delete $scope.DATA.data_rsv_OT.selected_trip;
		}else{
			trip.is_selected = true;
			trip.is_hidden = false;
			
			trips_parent.selected_trip = trip;
			$scope.DATA.data_rsv_OT.selected_trip = trip;
		}
		
		$scope.check_availabilities_show_OK_button = false;
		if ($scope.check_availabilities_data.departure.selected_trip){
			$scope.check_availabilities_show_OK_button = true;
		}
	}
	
	$scope.submit_booking_open_ticket = function(event){
		var data_rsv = {};
		
		data_rsv.booking_source = "OFFLINE";
		data_rsv.booking_status = "DEFINITE";
		data_rsv.voucher_reff_number = $scope.DATA.openticket.openticket_code;
		data_rsv.customer 	= $scope.DATA.data_rsv_OT.customer;
		data_rsv.remarks	= $scope.DATA.data_rsv_OT.remarks;
		
		data_rsv.trips = {	"date"			: $scope.check_availabilities_data.departure.check.date,
							"adult"			: $scope.check_availabilities_data.departure.check.adult,
							"rates_code"	: $scope.DATA.data_rsv_OT.selected_trip.rates_code,
							"schedule_code"	: $scope.DATA.data_rsv_OT.selected_trip.schedule.schedule_code,
							};
		
		if ($scope.DATA.data_rsv_OT.checkin_now && $scope.DATA.data_rsv_OT.checkin_now == '1'){
			data_rsv.checkin_now = '1';
		}else{
			data_rsv.checkin_now = '0';
		}
		
		data_rsv.openvoucher_code = $scope.DATA.openticket.openticket_code;
		
		httpSrv.post({
			url 	: "transport/checkin/openvoucher_checkin",
			data	: $.param(data_rsv),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					alert ("Chekin Success...");
					$('html, body').animate({scrollTop:0}, 400);
					$scope.DATA.voucher = {};
					$scope.check_voucher_code = "";
					
					$("#frm-openticket-checkin").modal("hide");
					
					$("#check_voucher_code").focus();
				}
			},
			error : function (response){}
		});
	}
	
	
	$scope.getClickVoucherInformation = function(voucher_code){
		$scope.check_voucher_code = voucher_code;
		$("#checkin-popup-info").modal("hide");
		$scope.getVoucherInformation();
	}
	$scope.selectToCheckIn = function(passenger){
		if (!passenger.already_checkin){
			if (passenger.checkin_status=='1'){
				passenger.checkin_status = '0';
			}else{
				passenger.checkin_status = '1';
			}
		}
	}
	$scope.checkUncheckParticipant = function(status, participant){
		for (i in participant){
			if (!participant[i].already_checkin){
				if (status == 1){
					participant[i].checkin_status = 1;
				}else{
					participant[i].checkin_status = 0;
				}
			}
		}
	}
	$scope.checkInParticipantNow = function(){
		var participants =  $scope.DATA.voucher.voucher.voucher.passenger;
		var data = {"voucher_code" : $scope.DATA.voucher.voucher.voucher.voucher_code,
					"participant"  : []};
		
		for (i in participants){
			data.participant.push({	"participant_code"	:participants[i].passenger_code,
									"pass_code"			:participants[i].pass_code,
									"checkin_status"	:participants[i].checkin_status});
		}
		$scope.is_checkedin_proccessed = true;
		httpSrv.post({
			url 	: "transport/checkin/checkin_participant",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					alert ("Chekin Success...");
					$('html, body').animate({scrollTop:0}, 400);
					$scope.DATA.voucher = {};
					$scope.check_voucher_code = "";
					$("#check_voucher_code").focus();
				}
			},
			error : function (response){}
		});
		
	}
	$scope.openPopUpWindowVoucher = function (base_url, booking_code, voucher_code){
		var myWindow = window.open(base_url+booking_code+'/'+voucher_code, 'VoucherWindow', 'width=800,height=600');
	}
	
	
});

function activate_sub_menu_agent_detail(class_menu){
	$("ul.nav.sub-nav.profile li").removeClass("active");
	$("ul.nav.sub-nav.profile li."+class_menu).addClass("active");
}
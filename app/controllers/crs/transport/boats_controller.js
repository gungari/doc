// JavaScript Document
angular.module('app').controller('boats_controller',function($scope, $http, httpSrv, $stateParams, $interval){
	
	GeneralJS.activateLeftMenu("trips_schedule");
	GeneralJS.activateSubMenu(".nav-pills", "li", ".boats")
	
	$scope.DATA = {};

	$scope.loadDataBoat = function(){
		httpSrv.post({
			url 	: "transport/boats",
			data	: $.param({"publish_status":"all"}),
			success : function(response){
				$scope.DATA.boats = response.data;
			},
			error : function (response){}
		});
	}
	$scope.publishUnpublishBoat = function (boats, status){
		
		var path = (status=='1')?"publish":"unpublish";
		
		boats.publish_status = status;
		
		httpSrv.get({
			url 	: "transport/boats/"+path+"/"+boats.boat_code,
			success : function(response){
				if (response.data.status != "SUCCESS"){
					boats.publish_status = !status;
				}
			},
			error : function (response){}
		});
	}
	$scope.addEditBoat = function (current_boat){
		
		if (current_boat){
			$scope.DATA.myBoat = angular.copy(current_boat);
		}else{
			$scope.DATA.myBoat = {};
		}
	}
	$scope.change_check = function(value){
		if (value == '0') {
			$scope.DATA.myBoat.extraset = false;
		}else{
			$scope.DATA.myBoat.extraset = true;
		}
	}
	$scope.saveDataBoat = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myBoat.error_msg = [];
		
		httpSrv.post({
			url 	: "transport/boats/"+(($scope.DATA.myBoat.id)?"update":"create"),
			data	: $.param($scope.DATA.myBoat),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataBoat();
					$("#add-edit-boat").modal("hide");
					toastr.success("Saved...");
				}else{
					$scope.DATA.myBoat.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
});
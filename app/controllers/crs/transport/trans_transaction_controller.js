// JavaScript Document

angular.module('app').controller('trans_transaction_controller',function($scope, $http, httpSrv, $stateParams, $interval, fn,  $filter){
	
	GeneralJS.activateLeftMenu("transaction");	
	
	$scope.DATA = {};
	$scope.fn = fn;
	$scope.search = [];

	$scope.loadReportTrasc = function()
	{
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});

		var now = new Date(),
		start_date = new Date(),
		end_date = new Date();
		start_date.setDate(start_date.getDate() - 1);

		$scope.show_loading = true;
		$scope.search.year = $filter('date')(new Date(), 'yyyy');
		$scope.search.month = $filter('date')(new Date(), 'M');
		$scope.search.date = $filter('date')(new Date(), 'd');
		$scope.search.start_date = $scope.fn.formatDate(start_date, "yy-mm-dd");
		$scope.search.end_date = $scope.fn.formatDate(end_date, "yy-mm-dd");
		$scope.search.view = "month";
		$scope.search.product = '';
		$scope.show_error = false;

		$scope.loadReportTrascData();
	}

	$scope.loadReportTrascData = function($data = '', $view = '')
	{
		$scope.show_loading = true;

		if ($data == '') {
			if (!$scope.search.month) $scope.search.month = $scope.month.toString();
			if (!$scope.search.year) $scope.search.year = $scope.year.toString();
			if (!$scope.search.date) $scope.search.date = $scope.date.toString();
		}else{
			if ($view=='date') {
				$scope.search.start_date = $data;
				$scope.search.end_date = $data;
				
			}else{
				$scope.search.year = $filter('date')($data, 'yyyy');
				$scope.search.month = $filter('date')($data, 'M');
				$scope.search.date = $filter('date')($data, 'd');
			}
			
			$scope.search.view = $view;
		}
		

		var _search = {	
						"month" 	: $scope.search.month,
						"year"		: $scope.search.year,
						"start_date" : $scope.search.start_date,
						"end_date"  : $scope.search.end_date,
						"view" 		: $scope.search.view,
						"product" 	: $scope.search.product,
					  };
		
		httpSrv.post({
			url 	: "transport/transaction/report",
			data	: $.param({"search":_search}),
			success : function(response){
				$scope.show_loading = false;
				if (response.data.status == 'SUCCESS') {
					$scope.show_error = false;
					$scope.transaction = response.data.transactions;
					$scope.summary = response.data.summary;
				}else{
					$scope.show_error = true;
				}
				
			},
			error : function (response){}
		})
	}

	$scope.printTransactionDeposit = function(page){
		$scope.show_loading_DATA_bookings = true;
		
		
		if ($stateParams.agent_code){
			var cek_agent = $stateParams.agent_code.split("--");
			if (cek_agent[0] == "AGENT"){
				delete $stateParams.agent_code;
				var agent_code = cek_agent[1];
			}
		}else{
			var agent_code = '';
		}
		
		httpSrv.post({
			url 	: "agent/deposit_print",
			data	: $.param({"publish_status":"ALL", "agent_code": agent_code}),
			success : function(response){
				//$scope.DATA = response.data;
				$scope.show_loading = false;
				if (response.data.status = "SUCCESS") {
					$scope.DATA = response.data;	
					$scope.$parent.$parent.show_print_button = true;
				}
				else if(response.data.error_desc.length > 0){
					$scope.err_deposit = response.data.error_desc;
				}
				
			},
			error : function (response){}
		});
		
	}

	$scope.printTransactionTransport = function(page){
		$scope.show_loading_DATA_bookings = true;
		
		$scope.search = { "start_date" : $stateParams.start_date,
							  "end_date" : $stateParams.finish_date}; 

		$scope.start_date =  $stateParams.start_date;
		$scope.end_date =  $stateParams.finish_date;
		
		if (!page) page = 1;
		
		var _search = angular.copy($scope.search);
			_search.page = page;
		

		httpSrv.post({
			url 	: "transport/transaction/print_",
			data	: $.param({"search":_search}),
			success : function(response){
				$scope.show_loading_DATA_bookings = false;
				$scope.DATA.transaction = response.data;
				$scope.$parent.$parent.show_print_button = true;
				if ($scope.DATA.transaction.search){
					$scope.DATA.transaction.search.pagination = [];
					for (i=1;i<=$scope.DATA.transaction.search.number_of_pages; i++){
						$scope.DATA.transaction.search.pagination.push(i);
					}
				}
			},
			error : function (response){}
		});
		
		
	}
	
	$scope.loadDataTransactionTransport = function(page){
		$scope.show_loading_DATA_bookings = true;
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		
		if (!$scope.search){ 
			var now = new Date(),
				start_date = new Date(),
				end_date = new Date();
			
			start_date.setDate(start_date.getDate() - 1);
			
			$scope.search = { "start_date" : $scope.fn.formatDate(start_date, "yy-mm-dd"),
							  "end_date" : $scope.fn.formatDate(end_date, "yy-mm-dd"),}; 
		}
		
		if (!page) page = 1;
		
		var _search = angular.copy($scope.search);
			_search.page = page;
		
		if (_search.booking_source){
			var cek_agent = _search.booking_source.split("--");
			if (cek_agent[0] == "AGENT"){
				delete _search.booking_source;
				_search.agent_code = cek_agent[1];
			}
		}
		
		httpSrv.post({
			url 	: "transport/transaction",
			data	: $.param({"search":_search}),
			success : function(response){
				$scope.show_loading_DATA_bookings = false;
				$scope.DATA.transaction = response.data;
				
				if ($scope.DATA.transaction.search){
					$scope.DATA.transaction.search.pagination = [];
					for (i=1;i<=$scope.DATA.transaction.search.number_of_pages; i++){
						$scope.DATA.transaction.search.pagination.push(i);
					}
				}
			},
			error : function (response){}
		});

		
		//Load Data Booking Source
		if (!$scope.$root.DATA_booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.$root.DATA_booking_source = response.data.booking_source; },
			});
		}
		
		//Load Data Booking Status
		if (!$scope.$root.DATA_booking_status){
			httpSrv.post({
				url 	: "setting/booking_status",
				success : function(response){ $scope.$root.DATA_booking_status = response.data.booking_status; },
			});
		}
		
		//Load Data Payment Method
		if (!$scope.$root.DATA_payment_method){
			httpSrv.post({
				url 	: "setting/payment_method",
				success : function(response){ $scope.$root.DATA_payment_method = response.data.payment_method; },
			});
		}
		
		//Load Data Agent
		if (!$scope.DATA.agents){
			httpSrv.post({
				url 	: "agent",
				data	: $.param({"search":{"limit":"all"}}),
				success : function(response){ $scope.DATA.agents = response.data; }
			});
		}

		//load summary 
		// httpSrv.post({
		// 	url 	: "transport/transaction/transactions_summary",
		// 	data	: $.param({"search":_search}),
		// 	success : function(response){
		// 		if (response.data.status == 'SUCCESS') {
		// 			$scope.DATA.summary = response.data.summary;
		// 		}else{
		// 			$scope.DATA.summary = {};
		// 		}
				
		// 	},
		// 	error : function (response){}
		// });
	}
	
});

function activate_sub_menu_agent_detail(class_menu){
	$(".reservation-detail ul.nav.sub-nav li").removeClass("active");
	$(".reservation-detail ul.nav.sub-nav li."+class_menu).addClass("active");
}

// JavaScript Document
//app.controller('profile_controller',function($scope, $http, httpSrv, $stateParams, $interval){
angular.module('app').controller('trips_schedule_controller',function($scope, $http, httpSrv, fn, $stateParams, $interval){

	GeneralJS.activateLeftMenu("trips_schedule");
	GeneralJS.activateSubMenu(".nav-pills", "li", ".schedules")
	
	$scope.DATA = {};
	$scope.fn = fn;
	
	$scope.loadDataSchedule = function(){
		httpSrv.post({
			url 	: "transport/schedule",
			data	: $.param({"publish_status":"all"}),
			success : function(response){
				$scope.DATA.schedules = response.data;
				
				if ($scope.DATA.schedules.status == "SUCCESS"){
					
					$scope.DATA.schedules.schedules.forEach(function(_item, index) {
						var schedule = angular.copy(_item);
						httpSrv.get({
							url 	: "transport/schedule/detail/"+schedule.schedule_code,
							success : function(response_detail){
								_item.schedule_detail = response_detail.data;
							},
							error : function (response){}
						});
					});
				}
			},
			error : function (response){}
		});
	}
	$scope.loadDataScheduleDetail = function(schedule_code, after_load_handler_function){
		
		if (!schedule_code){ schedule_code = $stateParams.schedule_code; }
		
		httpSrv.get({
			url 	: "transport/schedule/detail/"+schedule_code,
			success : function(response){
				var schedule = response.data;
				
				if (schedule.status == "SUCCESS"){
					$scope.DATA.current_schedule = schedule;
					
					if (typeof after_load_handler_function == 'function') {
						after_load_handler_function(schedule);
					}
				}else{
					alert("Sorry, schedule not found..");
					window.location = "#/transport/trips_schedule_add";
				}
			},
			error : function (response){}
		});
	}
	
	$scope.publishUnpublishSchedule = function (schedule, status){
		
		var path = (status=='1')?"publish":"unpublish";
		schedule.publish_status = status;
		
		httpSrv.get({
			url 	: "transport/schedule/"+path+"/"+schedule.schedule_code,
			success : function(response){
				if (response.data.status != "SUCCESS"){
					schedule.publish_status = !status;
				}
			},
			error : function (response){}
		});
	}
	
	$scope.scheduleDetailAddItem = function(){
		var last_schedule_index = $scope.DATA.current_schedule.schedule_detail.length;
		var allow_to_add = false;
		var schedule_detail = {	"departure_port_id" : "", 
								"departure_time"	: "",
								"departure_time_hh"	: "",
								"departure_time_mm"	: "",
								"arrival_port_id"	: "",
								"arrival_time"		: "",
								"arrival_time_mm"	: "",
								"arrival_time_hh"	: ""};
		
		if (last_schedule_index == 0){
			allow_to_add = true;
		}else{
			var last_schedule = $scope.DATA.current_schedule.schedule_detail[(last_schedule_index-1)];
			if (last_schedule.departure_port_id != "" && last_schedule.arrival_port_id != ""){
				allow_to_add = true;
				schedule_detail.departure_port_id = last_schedule.arrival_port_id;
			}else{
				alert("Please complete current schedule first...")
			}
		}
		
		if (allow_to_add){
			$scope.DATA.current_schedule.schedule_detail.push(schedule_detail);
		}
	}
	$scope.scheduleDetailRemoveLastItem = function(){
		$scope.DATA.current_schedule.schedule_detail.pop();
	}
	$scope.changeArrivalPort = function(index){
		if ($scope.DATA.current_schedule.schedule_detail[(index+1)]){
			$scope.DATA.current_schedule.schedule_detail[(index+1)].departure_port_id = $scope.DATA.current_schedule.schedule_detail[index].arrival_port_id;
		}
	}
	$scope.scheduleAddEdit = function(){
		
		var schedule_code_edit = $stateParams.schedule_code_edit;
		
		$scope.DATA.current_schedule = {};
		$scope.DATA.current_schedule.schedule_detail = [];
		
		if (schedule_code_edit){
			$scope.loadDataScheduleDetail(schedule_code_edit, function(schedule){
				schedule.boat_id = schedule.boat.id;
						
				for (i=0; i<schedule.schedule_detail.length; i++){
					schedule.schedule_detail[i].departure_port_id = schedule.schedule_detail[i].departure_port.id;
					schedule.schedule_detail[i].arrival_port_id = schedule.schedule_detail[i].arrival_port.id;
					
					var departure_time = schedule.schedule_detail[i].departure_time.split(":");
					schedule.schedule_detail[i].departure_time_hh = departure_time[0];
					schedule.schedule_detail[i].departure_time_mm = departure_time[1];
					
					var arrival_time = schedule.schedule_detail[i].arrival_time.split(":");
					schedule.schedule_detail[i].arrival_time_hh = arrival_time[0];
					schedule.schedule_detail[i].arrival_time_mm = arrival_time[1];
				}
				$scope.DATA.current_schedule = schedule;
			});
			
			/*httpSrv.get({
				url 	: "transport/schedule/detail/"+schedule_code_edit,
				success : function(response){
					
					
					
					var schedule = response.data;
					
					if (schedule.status == "SUCCESS"){
						schedule.boat_id = schedule.boat.id;
						
						for (i=0; i<schedule.schedule_detail.length; i++){
							schedule.schedule_detail[i].departure_port_id = schedule.schedule_detail[i].departure_port.id;
							schedule.schedule_detail[i].arrival_port_id = schedule.schedule_detail[i].arrival_port.id;
							
							var departure_time = schedule.schedule_detail[i].departure_time.split(":");
							schedule.schedule_detail[i].departure_time_hh = departure_time[0];
							schedule.schedule_detail[i].departure_time_mm = departure_time[1];
							
							var arrival_time = schedule.schedule_detail[i].arrival_time.split(":");
							schedule.schedule_detail[i].arrival_time_hh = arrival_time[0];
							schedule.schedule_detail[i].arrival_time_mm = arrival_time[1];
						}
						$scope.DATA.current_schedule = schedule;
					}else{
						alert("Sorry, schedule not found..");
						window.location = "#/transport/trips_schedule_add";
					}
				},
				error : function (response){}
			});*/
		}else{
			$scope.scheduleDetailAddItem();
		}
		
		//Load Data Boat
		httpSrv.get({
			url 	: "transport/boats",
			success : function(response){
				$scope.DATA.boats = response.data;
			},
			error : function (response){}
		});
		
		//Load Data Port
		httpSrv.get({
			url 	: "transport/port/available_port",
			success : function(response){
				$scope.DATA.port_list = response.data;
			},
			error : function (response){}
		});
	}
	$scope.saveDataSchedule = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.current_schedule.error_desc = [];
		
		var current_schedule = $scope.DATA.current_schedule;
		var data_schedule = {	"name"			:current_schedule.name,
								"boat_id"		:current_schedule.boat_id,
								"description"	:current_schedule.description,};
		
		if ($scope.DATA.current_schedule.schedule_code){
			data_schedule.schedule_code = $scope.DATA.current_schedule.schedule_code;
		}
		
		data_schedule.schedule_detail = [];
		
		for (i=0; i<current_schedule.schedule_detail.length; i++){
			var _cs_detail = current_schedule.schedule_detail[i];
			
			var _schedule_detail = {"departure_port_id"	:_cs_detail.departure_port_id,
									"departure_time"	:_cs_detail.departure_time_hh+":"+_cs_detail.departure_time_mm,
									"arrival_port_id"	:_cs_detail.arrival_port_id,
									"arrival_time"		:_cs_detail.arrival_time_hh+":"+_cs_detail.arrival_time_mm};
			data_schedule.schedule_detail.push(_schedule_detail);
		}
		
		httpSrv.post({
			url 	: "transport/schedule/"+(($scope.DATA.current_schedule.schedule_code)?"update":"create"),
			data	: $.param(data_schedule),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					window.location = "#/transport/trips_schedule_detail/"+response.data.schedule_code;
					toastr.success("Saved...");
				}else{
					$scope.DATA.current_schedule.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
					$('html, body').animate({scrollTop:200}, 400);
				}
				//$.hideLoading();
			}
		});
	}
	
	//Boat & Inventory
	$scope.loadBoatAndInventory = function(){
		
		if (!$scope.filter_date_search){
			var now = new Date();
			$scope.filter_search = {};
			$scope.filter_date_search = {};
			$scope.filter_date_search.date = now;
			$scope.filter_date_search.str = fn.formatDate(now, "yy-mm-dd");
		}else{
			$scope.filter_date_search.date = new Date($scope.filter_date_search.str);
		}
		
		if (!$scope.DATA.boats){
			httpSrv.post({
				url 	: "transport/boats",
				success : function(response){
					$scope.DATA.boats = response.data;
				},
				error : function (response){}
			});
		}
		
		//Load Data Port
		if (!$scope.DATA.port_list){
			httpSrv.get({
				url 	: "transport/port/available_port",
				success : function(response){
					$scope.DATA.port_list = response.data;
				},
				error : function (response){}
			});
		}
		
		//$scope.DATA.running_boat = false;
		
		var loadBoatAndInventory = function(){
				var departure_port_id	= $scope.filter_search.departure_port_id;
				var arrival_port_id		= $scope.filter_search.arrival_port_id;
			
				var start_date	= $scope.filter_date_search; 
				var end_date	= {};
				end_date.date 	= start_date.date; end_date.date.setDate(start_date.date.getDate() + 30);
				end_date.str	= fn.formatDate(end_date.date, "yy-mm-dd");
				
				var current_schedule = $scope.DATA.current_schedule;
				//Load Data Boat
				httpSrv.post({
					url 	: "transport/schedule/running_boat/"+current_schedule.schedule_code,
					data	: $.param({"start_date":start_date.str, "end_date":end_date.str}),
					success : function(response){
						var running_boat = response.data;
						$scope.DATA.running_boat = running_boat;
					},
					error : function (response){}
				});
				
				//Load Data Calendar
				var param_calendar = {	"schedule_code"		: current_schedule.schedule_code,
										"departure_port_id"	: departure_port_id,
										"arrival_port_id"	: arrival_port_id,
										"start_date"		: start_date.str,
										"end_date"			: end_date.str,
										};
				httpSrv.post({
					url 	: "transport/schedule/availability_calendar/",
					data	: $.param(param_calendar),
					success : function(response){
						var calendar = response.data;
						if (calendar.status == 'SUCCESS'){
							$scope.DATA.calendar = calendar;
						}
					},
					error : function (response){}
				});
				
				//Load Data Close Date
				httpSrv.post({
					url 	: "transport/schedule/closed_date/"+current_schedule.schedule_code,
					data	: $.param({"array_index_by_date":"1","start_date":start_date.str,"end_date":end_date.str}),
					success : function(response){
						var closed_date = response.data;
						$scope.DATA.closed_date = closed_date;
					},
					error : function (response){}
				});
			}
		
		if (!$scope.$parent.DATA.current_schedule){
			schedule_code = $stateParams.schedule_code;
			$scope.loadDataScheduleDetail(schedule_code,loadBoatAndInventory);
		}else{
			$scope.DATA.current_schedule = $scope.$parent.DATA.current_schedule;
			loadBoatAndInventory();
		}
	}
	$scope.loadBoatAndInventoryPrevNext = function(days){
		var start_date = new Date($scope.filter_date_search.str);
		start_date.setDate(start_date.getDate() + days);
		$scope.filter_date_search.str = fn.formatDate(start_date, "yy-mm-dd");
		
		$scope.loadBoatAndInventory();
	}
	
	$scope.setupOpenCloseDate = function(){
		$scope.DATA.editOpenCloseDate = {};
		$scope.DATA.editOpenCloseDate.status = "close";
	}
	$scope.setupOpenCloseCurrentDate = function(date, status){
		var data = {"start_date":date, "end_date":date, "status":((status=="1")?"open":"close")};
		
		httpSrv.post({
			url 	: "transport/schedule/change_closed_date/"+$scope.DATA.current_schedule.schedule_code,
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					if (status == "1"){
						toastr.success(date+" opened..");
						delete $scope.DATA.closed_date.closed_date[date];
					}else{
						toastr.success(date+" closed..");
						$scope.DATA.closed_date.closed_date[date] = {};
						$scope.DATA.closed_date.closed_date[date] = {"date":date, "remarks":""};
					}
				}
			}
		});
	}
	$scope.saveDataOpenCloseDate = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.editOpenCloseDate.error_msg = [];
		
		httpSrv.post({
			url 	: "transport/schedule/change_closed_date/"+$scope.DATA.current_schedule.schedule_code,
			data	: $.param($scope.DATA.editOpenCloseDate),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadBoatAndInventory();
					$("#popup-open-close-date").modal("hide");
					toastr.success("Saved...");
				}else{
					$scope.DATA.editOpenCloseDate.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	
	$scope.setupChangeBoat = function(boat, date){
		if (!$scope.DATA.boats){
			httpSrv.post({
				url 	: "transport/boats",
				success : function(response){
					$scope.DATA.boats = response.data;
				},
				error : function (response){}
			});
		}
		
		$scope.DATA.editChangeBoat = {};
		if (boat && date){
			$scope.DATA.editChangeBoat.start_date = date;
			$scope.DATA.editChangeBoat.end_date = date;
			$scope.DATA.editChangeBoat.boat_id = boat.id;
		}
	}
	$scope.changeCurrentBoat = function(boat, date){
		var data = {"start_date":date, "end_date":date, "boat_id":boat.id};
		
		httpSrv.post({
			url 	: "transport/schedule/change_running_boat/"+$scope.DATA.current_schedule.schedule_code,
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadBoatAndInventory();
					toastr.success("Boat changed to "+boat.name+"...");
				}
			}
		});
	}
	$scope.saveDataChangeBoat = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.editChangeBoat.error_msg = [];

		httpSrv.post({
			url 	: "transport/schedule/change_running_boat/"+$scope.DATA.current_schedule.schedule_code,
			data	: $.param($scope.DATA.editChangeBoat),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadBoatAndInventory();
					$("#popup-change-boat").modal("hide");
					toastr.success("Saved...");
				}else{
					$scope.DATA.editChangeBoat.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	
	//RATES
	$scope.loadMasterRates = function(){
		
		var loadMasterRates = function(){
				var current_schedule = $scope.DATA.current_schedule;
				//Load Data Rates
				httpSrv.post({
					url 	: "transport/schedule/rates/"+current_schedule.schedule_code,
					data	: $.param({"publish_status":"all", "group_by_trip" : "1"}),
					success : function(response){
						var rates = response.data;
						$scope.DATA.rates = rates;
					},
					error : function (response){}
				});
			}
		
		if (!$scope.$parent.DATA.current_schedule){
			schedule_code = $stateParams.schedule_code;
			$scope.loadDataScheduleDetail(schedule_code,loadMasterRates);
		}else{
			$scope.DATA.current_schedule = $scope.$parent.DATA.current_schedule;
			loadMasterRates();
		}
		
		if (!$scope.$parent.filter){
			$scope.$parent.filter = {departure_port:'',arrival_port:''};
		}
		
	}
	$scope.loadMasterRatesDetail = function (rates_code, affter_success_handler){
		httpSrv.post({
			url 	: "transport/schedule/rates_detail/"+rates_code,
			success : function(response){
				
				var rates_detail = response.data;
				
				if (fn.isFn(affter_success_handler)){
					affter_success_handler(rates_detail);
				}
				
			},
			error : function (response){}
		});
	}
	$scope.loadMasterRatesDetailForRatesList = function(rates){
		if (!rates.already_get_detail){
			var rates_code = rates.rates_code;
			$scope.loadMasterRatesDetail(rates_code, function(rates_detail){
				if (rates_detail.status == "SUCCESS"){
					//Check Key One by One and insert new data
					for (var key in rates_detail){
						if (!rates[key]){
							rates[key] = rates_detail[key];
						}
					}
					rates.already_get_detail = true;
				}
			});
		}		
	}
	$scope.publishUnpublishScheduleRates = function (rates, status){
		
		var path = (status=='1')?"publish":"unpublish";
		rates.publish_status = status;
		
		httpSrv.get({
			url 	: "transport/schedule/"+path+"_rates/"+rates.rates_code,
			success : function(response){
				if (response.data.status != "SUCCESS"){
					schedule.publish_status = !status;
				}
			},
			error : function (response){}
		});
	}
	$scope.deleteScheduleRates = function (rates){
		if (confirm("Are you sure to delete this rates?")){
			httpSrv.get({
				url 	: "transport/schedule/delete_rates/"+rates.rates_code,
				success : function(response){
					if (response.data.status == "SUCCESS"){
						$scope.loadMasterRates();
						toastr.success("Deleted...");
					}
				},
				error : function (response){}
			});
		}
	}
	$scope.duplicateScheduleRates = function (rates){
		
		rates_detail = angular.copy(rates);
		
		if (!confirm("Click OK to continue duplicate this rates...")){
			return;
		}
		
		var duplicateRates = function(rates_detail){
			rates_detail.schedule_code = $scope.DATA.current_schedule.schedule_code;
			rates_detail.departure_trip_number = rates_detail.departure.trip_number;
			rates_detail.arrival_trip_number = rates_detail.arrival.trip_number;
			if (rates_detail.rates.return_rates){
				rates_detail.rates.return_rates.rates_1 = rates_detail.rates.return_rates.rates_1*2;
				rates_detail.rates.return_rates.rates_2 = rates_detail.rates.return_rates.rates_2*2;
				rates_detail.rates.return_rates.rates_3 = rates_detail.rates.return_rates.rates_3*2;
			}
			rates_detail.name = rates_detail.name+' - (Copy)';
			
			delete rates_detail.departure;
			delete rates_detail.arrival;
			delete rates_detail.id;
			delete rates_detail.rates_code;
			delete rates_detail.already_get_detail;

			httpSrv.post({
				url 	: "transport/schedule/create_rates",
				data	: $.param(rates_detail),
				success : function(response){
					if (response.data.status == "SUCCESS"){
						$scope.loadMasterRates();
						toastr.success("Duplicated...");
					}
				}
			});
		}
		
		if (rates_detail.already_get_detail){
			duplicateRates(rates_detail);
		}else{
			$scope.loadMasterRatesDetail(rates_detail.rates_code, 
				function(rates_detail){
					duplicateRates(rates_detail);
				}
			);
		}
		//console.log(rates_detail);
	}
	
	$scope.filterDeparture = function (_item){		
		if ($scope.DATA.rates){
			if ($scope.$parent.filter && $scope.$parent.filter.departure_port != ""){
				var arr_rates = {};
				for (key in _item){
					if ($scope.DATA.rates.rates.trips.departure[key].port.id == $scope.$parent.filter.departure_port){
						arr_rates[key] = _item[key];
					}
				}
				return arr_rates;
			}else{
				return _item;
			}
		}
	}
	$scope.filterArrival = function (_item){		
		if ($scope.DATA.rates){
			if ($scope.$parent.filter && $scope.$parent.filter.arrival_port != ""){
				var arr_rates = {};
				for (key in _item){
					if ($scope.DATA.rates.rates.trips.arrival[key].port.id == $scope.$parent.filter.arrival_port){
						arr_rates[key] = _item[key];
					}
				}
				return arr_rates;
			}else{
				return _item;
			}
		}
	}
	
	$scope.loadDateChange = function(date){
		$('.datepicker.end_date').datepicker({dateFormat:"yy-mm-dd", minDate: date});
	}

	$scope.loadDate = function(id){
		$('.datepicker.start_date'+id).datepicker({dateFormat:"yy-mm-dd"});
	}

	$scope.addItem = function(event, range){

		$('.datepicker.start_date1').datepicker({dateFormat:"yy-mm-dd"});
		$scope.range = 0;

		if (!$scope.DATA.date_picker) {
			$scope.DATA.date_picker = [];
		}

		if (range == '0') {
			
			var date_picker = {	
				"id" : $scope.DATA.current_rates.id,
				"date" : ''
			};

			$scope.DATA.date_picker.push(date_picker);


		}else {
			$scope.range = 1;
			$scope.start_date = '';
			$scope.end_date = '';
		}	
	}

	$scope.remove=function($index){ 

	  $scope.DATA.date_picker.splice($index,1);    
	  
	}

	$scope.ratesAddEdit = function(){

		//$('.datepicker.start_date').datepicker({dateFormat:"yy-mm-dd"});
		$('.datepicker').datepicker({dateFormat:"yy-mm-dd"});
		
		var schedule_code = $stateParams.schedule_code;
		var rates_code = $stateParams.rates_code;
		
		$scope.DATA.current_rates = {};
		$scope.DATA.date_exception = {};
		$scope.DATA.date_picker = [];
		
		var generateDataEditRates = function(){
			var generate_data_important_info = function (data){
				var arr_data = [];
				if (data){
					for (index in data){
						arr_data.push({"str":data[index]});
					}
				}
				return arr_data;
			}
			$scope.DATA.current_rates.inclusion = generate_data_important_info($scope.DATA.current_rates.inclusion);
			$scope.DATA.current_rates.exclusion = generate_data_important_info($scope.DATA.current_rates.exclusion);
			$scope.DATA.current_rates.additional_info = generate_data_important_info($scope.DATA.current_rates.additional_info);
			
			if ($scope.DATA.current_rates.available_on == "all_day"){
				var days = ["sun", "mon", "tue", "wed", "thu", "fri", "sat"];
				$scope.DATA.current_rates.available_on = days;
			}
		}
		
		if (rates_code){
			//Edit Rates
			$scope.loadMasterRatesDetail(rates_code,function(rates_detail){
				rates_detail.departure_trip_number = rates_detail.departure.trip_number;
				rates_detail.arrival_trip_number = rates_detail.arrival.trip_number;
				rates_detail.is_rates_return_active = (rates_detail.is_return_rates=="yes")?1:0;
				$scope.DATA.current_rates = rates_detail;

				//load data close date/exception date
				$scope.DATA.date_picker = angular.copy($scope.DATA.current_rates.close_date);
				
				if (rates_detail.rates.return_rates){
					rates_detail.rates.return_rates.rates_1 *= 2;
					rates_detail.rates.return_rates.rates_2 *= 2;
					rates_detail.rates.return_rates.rates_3 *= 2;
				}
				
				if (!rates_detail.pickup_area) rates_detail.pickup_area = [];
				if (!rates_detail.dropoff_area) rates_detail.dropoff_area = [];
				
				generateDataEditRates();
			});
		}else{
			//Create New Rates
			$scope.DATA.current_rates = {	"booking_handling":"INSTANT",
											"available_on":"all_day",
											"min_order":1,
											"is_rates_return_active":false,
											"cut_of_booking":0,
											"pickup_service":"no", "pickup_area":[],
											"dropoff_service":"no","dropoff_area":[],
											"inclusion":[],"exclusion":[],"additional_info":[],
											"rates":{"one_way_rates":{"rates_1":0,"rates_2":0,"rates_3":0},
													 "return_rates":{"rates_1":0,"rates_2":0,"rates_3":0}}
										};
			$scope.DATA.enable_copy_rates = true;
			generateDataEditRates();
		}
		
				
		$scope.loadDataScheduleDetail(schedule_code, 
			function(schedule_detail){
				$scope.DATA.schedule_detail = schedule_detail;
			}
		);
		
		//$scope.DATA.current_rates = {"name":"Test Promo Rates","booking_handling":"INSTANT","start_date":"2017-05-01","end_date":"2017-12-31","min_order":1,"rates":{"one_way_rates":{"rates_1":650000,"rates_2":550000,"rates_3":500000}}};
		//$scope.DATA.current_rates = {"booking_handling":"INSTANT","available_on":["sun","mon","wed","fri"],"min_order":1,"is_rates_return_active":false,"cut_of_booking":0,"pickup_service":"yes","pickup_area":[{"area":"Sanur","time":"10:00 AM","price":0,"type":"way"},{"area":"Kuta","time":"10:00","price":150000,"type":"way"}],"dropoff_service":"yes","dropoff_area":[{"area":"Sanur Drop","time":"15:00 PM","price":0,"type":"way"},{"area":"Kuta Drop","time":"15:00 PM","price":150000,"type":"way"}],"inclusion":[{"str":"i"},{"str":"ii"},{"str":"iii"}],"exclusion":[{"str":"e"},{"str":"ee"},{"str":"eee"}],"additional_info":[{"str":"a"},{"str":"aa"},{"str":"aaa"}],"name":"Test Input Promo Rates","departure_trip_number":"4","arrival_trip_number":"1","start_date":"2017-05-01","end_date":"2017-12-31","rates":{"one_way_rates":{"rates_1":500000,"rates_2":450000,"rates_3":0},"is_rates_return_active":true,"return_rates":{"rates_1":900000,"rates_2":850000,"rates_3":0}}};
		
		//$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
	}
	$scope.saveDataScheduleRates = function(event){

		$scope.range = 0;

		if ($scope.end_date != '' && $scope.start_date != '') {
			var now = new Date($scope.end_date);
			var daysOfYear = [];

			for (var d = new Date($scope.start_date); d <= now; d.setDate(d.getDate() + 1)) {

				var date_picker = {	
					"id" : $scope.DATA.current_rates.id,
					"date" : fn.formatDate(new Date(d), "yy-mm-dd")
				};

			    $scope.DATA.date_picker.push(date_picker);
			}
		}		

		$scope.DATA.current_rates.close_date = angular.copy($scope.DATA.date_picker);

		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.current_rates.error_desc = [];
		
		var current_rates =angular.copy($scope.DATA.current_rates);
		var data_rates = current_rates;
		data_rates.schedule_code = $scope.DATA.current_schedule.schedule_code;
		
		var get_arr = function(data_str){
			var data = [];
			for (key in data_str){ 
				data.push(data_str[key].str); 
			}
			return data;
		}
		if (!data_rates.is_rates_return_active){
			delete data_rates.rates.return_rates;
		}
		data_rates.inclusion = get_arr(data_rates.inclusion);
		data_rates.exclusion = get_arr(data_rates.exclusion);
		data_rates.additional_info = get_arr(data_rates.additional_info);
		
		delete data_rates.rates_for.all;
		if (!data_rates.rates_for.offline) delete data_rates.rates_for.offline;
		if (!data_rates.rates_for.walkin) delete data_rates.rates_for.walkin;
		if (!data_rates.rates_for.agent) delete data_rates.rates_for.agent;
		
		//console.log(data_rates);
		//return;
		httpSrv.post({
			url 	: "transport/schedule/"+(($scope.DATA.current_rates.rates_code)?"update_rates":"create_rates"),
			data	: $.param(data_rates),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					window.location = "#/transport/trips_schedule_detail/"+$scope.DATA.current_schedule.schedule_code+"/rates";
					toastr.success("Saved...");
				}else{
					$scope.DATA.current_rates.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
					$('html, body').animate({scrollTop:200}, 400);
				}
				//$.hideLoading();
			}
		});
	}
	$scope.ratesAddImportantInfo = function(var_to_add){
		var str = {"str":""};
		var_to_add.push(str);
	}
	$scope.ratesAddPickUpOrDropOffArea = function(var_to_add){
		var add = {	"area" : "",
					"time" : "",
					"price": 0,
					"type" : "way"};
		var_to_add.push(add);
	}
	$scope.ratesCheckAvailableDayInWeek = function (day){
		var index = $scope.DATA.current_rates.available_on.indexOf(day);
		if (index >= 0){
			$scope.DATA.current_rates.available_on.splice(index,1);
		}else{
			$scope.DATA.current_rates.available_on.push(day);
		}
		console.log($scope.DATA.current_rates.available_on);
	}
	
	//Copy Rates From other product
	$scope.copyRatesGetScheduleRates = function(){
		
		var current_schedule = $scope.DATA.copy_rates_from.schedule;
		//Load Data Rates
		httpSrv.post({
			url 	: "transport/schedule/rates/"+current_schedule.schedule_code,
			data	: $.param({"publish_status":"all", "xgroup_by_trip" : "1"}),
			success : function(response){
				var rates = response.data;
				$scope.DATA.copy_rates_from.schedule.rates = rates;
			},
			error : function (response){}
		});
	}
	$scope.copySelectedRates = function(rates){
		$scope.loadMasterRatesDetail(rates.rates_code, function(detail_rates){
			var product_detail_rates = angular.copy(detail_rates);
			delete product_detail_rates.rates_code;
			$scope.DATA.current_rates = product_detail_rates;
			
			var generateDataEditRates = function(){
				var generate_data_important_info = function (data){
					var arr_data = [];
					if (data){
						for (index in data){
							arr_data.push({"str":data[index]});
						}
					}
					return arr_data;
				}
				$scope.DATA.current_rates.inclusion = generate_data_important_info($scope.DATA.current_rates.inclusion);
				$scope.DATA.current_rates.exclusion = generate_data_important_info($scope.DATA.current_rates.exclusion);
				$scope.DATA.current_rates.additional_info = generate_data_important_info($scope.DATA.current_rates.additional_info);
				
				if ($scope.DATA.current_rates.available_on == "all_day"){
					var days = ["sun", "mon", "tue", "wed", "thu", "fri", "sat"];
					$scope.DATA.current_rates.available_on = days;
				}
			}
			generateDataEditRates();
			delete $scope.DATA.copy_rates_from.product;
			$("#form-schedule-list").modal("toggle");
		});
	}
	//--
	
	$scope.smartPricingEdit = function(){
		
		var schedule_code 	= $stateParams.schedule_code;
		var rates_code		= $stateParams.rates_code;
		
		if (!$scope.DATA.current_rates){
			$scope.DATA.current_rates = {};
		}	
		$scope.loadMasterRatesDetail(rates_code,
			function(rates_detail){
				$scope.DATA.current_rates = rates_detail;
			}
		);
		
		
		if (!$scope.DATA.smart_pricing){
			$scope.DATA.smart_pricing= {};
		}
		
		$scope.DATA.smart_pricing.smart_pricing= {};
		
		var data_search = {};
		
		if ($scope.DATA.smart_pricing.search){
			data_search = $scope.DATA.smart_pricing.search;
		}
		
		
		httpSrv.post({
			url 	: "transport/schedule/smart_pricing/"+rates_code,
			data	: $.param(data_search),
			success : function(response){
				var rates = response.data;
				if (response.data.status == 'SUCCESS'){
					$scope.DATA.smart_pricing = response.data;
				}
			},
			error : function (response){}
		});

		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
	}
	$scope.enableDisableSmartPricing = function (rates, status){
		
		if (confirm("Click OK to continue...")){
			var path = (status=='1')?"enable":"disable";
			
			if (status == '1'){
				$("#btn-activate-smart-pricing").attr("disabled","disabled");
			}else{
				$("#btn-activate-smart-pricing").removeAttr("disabled");
			}
			
			httpSrv.get({
				url 	: "transport/schedule/"+path+"_smart_pricing/"+rates.rates_code,
				success : function(response){
					if (response.data.status == "SUCCESS"){
						if (status == '1'){
							toastr.success("Smart Pricing activate...");
						}else{
							toastr.success("Smart Pricing disabled...");
						}
						$scope.smartPricingEdit();
					}else{
						$("#btn-activate-smart-pricing").removeAttr("disabled");
					}
				},
				error : function (response){}
			});
		}
	}
	$scope.addSmartPricingAddLevel = function(){
		var level = {};
		level.oneway = {"qty_1" : 0, "qty_2" : 0, "qty_3" : 0};
		level.return = {"qty_1" : 0, "qty_2" : 0, "qty_3" : 0};
		level.allotment = 0;
		
		$scope.currentSmartPricing.level.push(level);
	}
	$scope.removeSmartPricingAddLevel = function(index){
		$scope.currentSmartPricing.level.splice(index,1);
	}
	$scope.editSmartPricing = function(pricing){
		
		if (pricing){
			
			$scope.currentSmartPricing = {	"start_date" 	: pricing.date,
											"end_date"		: pricing.date,
											"level"			: []};
			for (i in pricing.pricing_level){
				var pricing_level = {};
				
				pricing_level.oneway = {}
				pricing_level.oneway.rates_1 = pricing.pricing_level[i].rates.one_way_rates.rates_1;
				pricing_level.oneway.rates_2 = pricing.pricing_level[i].rates.one_way_rates.rates_2;
				pricing_level.oneway.rates_3 = pricing.pricing_level[i].rates.one_way_rates.rates_3;
				
				pricing_level.return = {};
				pricing_level.return.rates_1 = pricing.pricing_level[i].rates.return_rates.rates_1*2;
				pricing_level.return.rates_2 = pricing.pricing_level[i].rates.return_rates.rates_2*2;
				pricing_level.return.rates_3 = pricing.pricing_level[i].rates.return_rates.rates_3*2;
				
				pricing_level.allotment = pricing.pricing_level[i].allotment;
				
				$scope.currentSmartPricing.level.push(pricing_level);
			}								
		}else{
		
			$scope.currentSmartPricing = {	"start_date" 	: '',
											"end_date"		: '',
											"level"			: []};
			$scope.addSmartPricingAddLevel();
			
		}
	}
	$scope.saveDataSmartPricing = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.currentSmartPricing.error_desc = [];
		
		var data_rates = angular.copy( $scope.currentSmartPricing);
		data_rates.rates_code = $scope.DATA.current_rates.rates_code;
		
		httpSrv.post({
			url 	: "transport/schedule/update_smart_pricing",
			data	: $.param(data_rates),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.smartPricingEdit();
					toastr.success("Saved...");
					$("#add-edit-smart-pricing").modal("hide");
				}else{
					$scope.currentSmartPricing.error_desc = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	
});

function activate_sub_menu_schedule_detail(class_menu){
	$(".schedule-detail ul.nav.sub-nav li").removeClass("active");
	$(".schedule-detail ul.nav.sub-nav li."+class_menu).addClass("active");
}

// JavaScript Document
angular.module('app').controller('port_controller',function($scope, $http, httpSrv, $stateParams, $interval){
	
	GeneralJS.activateLeftMenu("trips_schedule");
	GeneralJS.activateSubMenu(".nav-pills", "li", ".port")
	
	$scope.DATA = {};
	
	$scope.loadDataAvailablePort = function(){
		httpSrv.post({
			url 	: "transport/port/available_port",
			success : function(response){
				$scope.DATA.availablePort = response.data;
			},
			error : function (response){}
		});
	}
	
	$scope.loadDataPortList = function(){
		httpSrv.post({
			url 	: "transport/port/port_list",
			success : function(response){
				$scope.DATA.portList = response.data;
			},
			error : function (response){}
		});
	}
	
	$scope.removeAvailablePort = function(port){
		if (confirm("Are you sure to remove this port?")){
			httpSrv.post({
				url 	: "transport/port/remove_port",
				data	: $.param({"port_id":port.id}),
				success : function(response){
					toastr.success("Deleted...");
					$scope.loadDataAvailablePort();
				}
			});
		}
	}
	$scope.addAvailablePort = function(){
		httpSrv.post({
			url 	: "transport/port/add_port",
			data	: $.param({"port_id":$scope.add_port_id}),
			success : function(response){
				$scope.add_port_id = "";
				toastr.success("Port added...");
				$scope.loadDataAvailablePort();
			}
		});
	}
});
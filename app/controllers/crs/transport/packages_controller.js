// JavaScript Document
angular.module('app').controller('packages_controller',function($scope, $http, httpSrv, fn, $stateParams, $interval){
	
	GeneralJS.activateLeftMenu("trips_schedule");
	GeneralJS.activateSubMenu(".nav-pills", "li", ".packages")
	
	$scope.DATA = {};
	$scope.fn = fn;
	
	//Load Data Packages
	$scope.loadDataPackages = function(){
		
		//Load Data Rates
		httpSrv.post({
			url 	: "transport/packages",
			data	: $.param({"publish_status":"all"}),
			success : function(response){
				var rates = response.data;
				$scope.DATA.packages = rates;
			},
			error : function (response){}
		});
	}
	
	$scope.loadPackagesDetail = function (package_code, affter_success_handler){
		httpSrv.post({
			url 	: "transport/packages/detail/"+package_code,
			success : function(response){
				
				var package_detail = response.data;
				
				if (fn.isFn(affter_success_handler)){
					affter_success_handler(package_detail);
				}
				
			},
			error : function (response){}
		});
	}
	$scope.loadPackagesDetailForRatesList = function(package){
		if (!package.already_get_detail){
			var package_code = package.package_code;
			$scope.loadPackagesDetail(package_code, function(package_detail){
				if (package_detail.status == "SUCCESS"){
					//Check Key One by One and insert new data
					for (var key in package_detail){
						if (!package[key]){
							package[key] = package_detail[key];
						}
					}
					package.already_get_detail = true;
				}
			});
		}		
	}
	$scope.publishUnpublishPackages = function (package, status){
		
		var path = (status=='1')?"publish":"unpublish";
		package.publish_status = status;
		
		httpSrv.get({
			url 	: "transport/packages/"+path+"/"+package.package_code,
			success : function(response){
				if (response.data.status != "SUCCESS"){
					schedule.publish_status = !status;
				}
			},
			error : function (response){}
		});
	}
	$scope.deletePackages = function (package){
		if (confirm("Are you sure to delete this package?")){
			httpSrv.get({
				url 	: "transport/packages/delete/"+package.package_code,
				success : function(response){
					if (response.data.status == "SUCCESS"){
						$scope.loadDataPackages();
						toastr.success("Deleted...");
					}
				},
				error : function (response){}
			});
		}
	}
	$scope.duplicatePackages__ = function (rates){
		
		rates_detail = angular.copy(rates);
		
		if (!confirm("Click OK to continue duplicate this rates...")){
			return;
		}
		
		var duplicateRates = function(rates_detail){
			rates_detail.schedule_code = $scope.DATA.current_schedule.schedule_code;
			rates_detail.departure_trip_number = rates_detail.departure.trip_number;
			rates_detail.arrival_trip_number = rates_detail.arrival.trip_number;
			if (rates_detail.rates.return_rates){
				rates_detail.rates.return_rates.rates_1 = rates_detail.rates.return_rates.rates_1*2;
				rates_detail.rates.return_rates.rates_2 = rates_detail.rates.return_rates.rates_2*2;
				rates_detail.rates.return_rates.rates_3 = rates_detail.rates.return_rates.rates_3*2;
			}
			rates_detail.name = rates_detail.name+' - (Copy)';
			
			delete rates_detail.departure;
			delete rates_detail.arrival;
			delete rates_detail.id;
			delete rates_detail.rates_code;
			delete rates_detail.already_get_detail;

			httpSrv.post({
				url 	: "transport/schedule/create_rates",
				data	: $.param(rates_detail),
				success : function(response){
					if (response.data.status == "SUCCESS"){
						$scope.loadMasterRates();
						toastr.success("Duplicated...");
					}
				}
			});
		}
		
		if (rates_detail.already_get_detail){
			duplicateRates(rates_detail);
		}else{
			$scope.loadMasterRatesDetail(rates_detail.rates_code, 
				function(rates_detail){
					duplicateRates(rates_detail);
				}
			);
		}
		//console.log(rates_detail);
	}
	//--

	//Add / Edit Packages
	$scope.packageAddEdit = function(){
		
		$('.datepicker').datepicker({dateFormat:"yy-mm-dd"});
		
		//Load Data Port
		if (!$scope.$root.DATA_port_list){
			httpSrv.get({
				url 	: "transport/port/available_port",
				success : function(response){
					$scope.$root.DATA_port_list = response.data.ports;
				},
				error : function (response){}
			});
		}
		
		var package_code = $stateParams.package_code;
		
		$scope.DATA.current_package = {};
		
		var generateDataEditRates = function(){
			var generate_data_important_info = function (data){
				var arr_data = [];
				if (data){
					for (index in data){
						arr_data.push({"str":data[index]});
					}
				}
				return arr_data;
			}
			$scope.DATA.current_package.additional_item = generate_data_important_info($scope.DATA.current_package.additional_item);
			$scope.DATA.current_package.inclusion = generate_data_important_info($scope.DATA.current_package.inclusion);
			$scope.DATA.current_package.exclusion = generate_data_important_info($scope.DATA.current_package.exclusion);
			$scope.DATA.current_package.additional_info = generate_data_important_info($scope.DATA.current_package.additional_info);
			
			if ($scope.DATA.current_package.available_on == "all_day"){
				var days = ["sun", "mon", "tue", "wed", "thu", "fri", "sat"];
				$scope.DATA.current_package.available_on = days;
			}
		}
		
		//Load Data Schedule
		httpSrv.post({
			url 	: "transport/schedule",
			//data	: $.param({"publish_status":"all"}),
			success : function(response){
				$scope.DATA.schedules = response.data;
				
				if ($scope.DATA.schedules.status == "SUCCESS"){
					
					$scope.DATA.schedules.schedules.forEach(function(_item, index) {
						var schedule = angular.copy(_item);
						httpSrv.get({
							url 	: "transport/schedule/detail/"+schedule.schedule_code,
							success : function(response_detail){
								_item.schedule_detail = response_detail.data;
							},
							error : function (response){}
						});
					});
				}
				//console.log($scope.DATA.schedules);
			},
			error : function (response){}
		});
		//-----
		
		if (package_code){
			//Edit Rates
			$scope.loadPackagesDetail(package_code,function(package_detail){
				$scope.DATA.current_package = package_detail;

				generateDataEditRates();
			});
		}else{
			//Create New Rates
			$scope.DATA.current_package = {	"available_on":"all_day",
											"min_order":1,
											"cut_of_booking":0,
											//"pickup_service":"no", "pickup_area":[],
											//"dropoff_service":"no","dropoff_area":[],
											"additional_item":[],"inclusion":[],"exclusion":[],"additional_info":[],
											"rates":{"rates_1":0,"rates_2":0,"rates_3":0},
											"routes_option":[]
										};
			
			generateDataEditRates();
			$scope.packagesAddRoutesOption();
			
			//$scope.DATA.current_package = {"available_on":["sun","mon","tue","wed","thu","fri","sat"],"min_order":1,"cut_of_booking":0,"additional_item":[{"str":"Free Snorkling"},{"str":"Free Diving"},{"str":"Lunch"}],"inclusion":[{"str":"Inclusion 01"},{"str":"Inclusion 02"},{"str":"Inclusion 03"}],"exclusion":[{"str":"Exclusion 01"},{"str":"Exclusion 02"},{"str":"Exclusion 03"}],"additional_info":[{"str":"Additional Info 01"},{"str":"Additional Info 02"},{"str":"Additional Info 03"}],"rates":{"rates_1":1500000,"rates_2":1500000,"rates_3":0},"routes_option":[{"trip":[{"number":1,"departure_port_id":"16","arrival_port_id":"8","time":"09:30","available_time_list":[{"value":"09:30","text":"09:30"}],"departure_port_list":[{"id":"16","port_code":"BNA","name":"Benoa Harbour"},{"id":"5","port_code":"GLA","name":"Gili Air"},{"id":"6","port_code":"GLM","name":"Gili Meno"},{"id":"7","port_code":"GLT","name":"Gili Trawangan"},{"id":"3","port_code":"LEM","name":"Lembongan"},{"id":"8","port_code":"LBK","name":"Lombok"},{"id":"1","port_code":"PDB","name":"Padang Bai"}],"arrival_port_list":[{"id":"1","port_code":"PDB","name":"Padang Bai"},{"id":"3","port_code":"LEM","name":"Lembongan"},{"id":"8","port_code":"LBK","name":"Lombok"}]},{"number":2,"departure_port_id":"8","arrival_port_id":"7","time":"10:30","available_time_list":[{"value":"10:30","text":"10:30"}],"departure_port_list":[{"id":"16","port_code":"BNA","name":"Benoa Harbour"},{"id":"5","port_code":"GLA","name":"Gili Air"},{"id":"6","port_code":"GLM","name":"Gili Meno"},{"id":"7","port_code":"GLT","name":"Gili Trawangan"},{"id":"3","port_code":"LEM","name":"Lembongan"},{"id":"8","port_code":"LBK","name":"Lombok"},{"id":"1","port_code":"PDB","name":"Padang Bai"}],"arrival_port_list":[{"id":"3","port_code":"LEM","name":"Lembongan"},{"id":"1","port_code":"PDB","name":"Padang Bai"},{"id":"16","port_code":"BNA","name":"Benoa Harbour"},{"id":"7","port_code":"GLT","name":"Gili Trawangan"},{"id":"5","port_code":"GLA","name":"Gili Air"},{"id":"6","port_code":"GLM","name":"Gili Meno"}]},{"number":3,"departure_port_id":"7","arrival_port_id":"8","time":"12:00","available_time_list":[{"value":"12:00","text":"12:00"}],"departure_port_list":[{"id":"16","port_code":"BNA","name":"Benoa Harbour"},{"id":"5","port_code":"GLA","name":"Gili Air"},{"id":"6","port_code":"GLM","name":"Gili Meno"},{"id":"7","port_code":"GLT","name":"Gili Trawangan"},{"id":"3","port_code":"LEM","name":"Lembongan"},{"id":"8","port_code":"LBK","name":"Lombok"},{"id":"1","port_code":"PDB","name":"Padang Bai"}],"arrival_port_list":[{"id":"1","port_code":"PDB","name":"Padang Bai"},{"id":"5","port_code":"GLA","name":"Gili Air"},{"id":"6","port_code":"GLM","name":"Gili Meno"},{"id":"8","port_code":"LBK","name":"Lombok"}]},{"number":4,"departure_port_id":"8","arrival_port_id":"16","time":"15:00","available_time_list":[{"value":"15:00","text":"15:00"}],"departure_port_list":[{"id":"16","port_code":"BNA","name":"Benoa Harbour"},{"id":"5","port_code":"GLA","name":"Gili Air"},{"id":"6","port_code":"GLM","name":"Gili Meno"},{"id":"7","port_code":"GLT","name":"Gili Trawangan"},{"id":"3","port_code":"LEM","name":"Lembongan"},{"id":"8","port_code":"LBK","name":"Lombok"},{"id":"1","port_code":"PDB","name":"Padang Bai"}],"arrival_port_list":[{"id":"3","port_code":"LEM","name":"Lembongan"},{"id":"1","port_code":"PDB","name":"Padang Bai"},{"id":"16","port_code":"BNA","name":"Benoa Harbour"},{"id":"7","port_code":"GLT","name":"Gili Trawangan"},{"id":"5","port_code":"GLA","name":"Gili Air"},{"id":"6","port_code":"GLM","name":"Gili Meno"}]}]}],"rates_for":{"offline":1,"walkin":1,"agent":1},"name":"First Packages Special Promo","description":"Lorem ipsum First Packages Special Promo","start_date":"2018-11-01","end_date":"2018-12-31"};

		}
	}
	
	$scope.packagesAddRoutesOption = function(){
		var route_option = {"trip":[]};
		$scope.DATA.current_package.routes_option.push(route_option);
		
		var last_index = $scope.DATA.current_package.routes_option.length - 1;
		$scope.packagesRoutesOptionAddTrip($scope.DATA.current_package.routes_option[last_index]);
		$scope.packagesRoutesOptionAddTrip($scope.DATA.current_package.routes_option[last_index]);
	}
	$scope.packagesRoutesOptionAddTrip = function(route_option){
		var number_of_trip = route_option.trip.length;
		var trip = { "number" : (number_of_trip+1),
					 "departure_port_id":"",
					 "arrival_port_id":"",
					 "time":"",
					 "available_time_list":[]};
		
		//Load Data Port
		if (!$scope.$root.DATA_port_list){
			httpSrv.get({
				url 	: "transport/port/available_port",
				success : function(response){
					$scope.$root.DATA_port_list = response.data.ports;
					trip.departure_port_list = angular.copy($scope.$root.DATA_port_list);
					trip.arrival_port_list	 = angular.copy($scope.$root.DATA_port_list);
				},
				error : function (response){}
			});
		}else{
			trip.departure_port_list = angular.copy($scope.$root.DATA_port_list);
			trip.arrival_port_list	 = angular.copy($scope.$root.DATA_port_list);
		}
		
		route_option.trip.push(trip);
		//console.log($scope.DATA.current_package);
	}
	$scope.packagesRemoveRoutesOption = function(index){
		$scope.DATA.current_package.routes_option.splice(index,1);
		if ($scope.DATA.current_package.routes_option.length == 0){
			$scope.packagesAddRoutesOption();
		}
	}
	
	$scope.packageTripOptionSelectDeparturePort = function(trip){
		
		var arr_arrival_port_id	= [];
		var arr_departure_time	= [];
		
		//Array Port List Index by Port ID
		if (!$scope.$root.DATA_port_list_by_port_id){
			$scope.$root.DATA_port_list_by_port_id = {};
			for (xx in $scope.$root.DATA_port_list){
				var port = $scope.$root.DATA_port_list[xx];
				$scope.$root.DATA_port_list_by_port_id[port.id] = port;
			}
		}
		//--
		
		//Get Available Arrival Port
		trip.arrival_port_list = [];
		for(i in $scope.DATA.schedules.schedules){
			var _schedule = $scope.DATA.schedules.schedules[i];
			var trip_number = 0;
			for (j in _schedule.schedule_detail.schedule_detail){
				var _trip = _schedule.schedule_detail.schedule_detail[j];
				
				//Departure Time
				if (trip.departure_port_id == _trip.departure_port.id){
					arr_departure_time.push({"value":_trip.departure_time,"text":_trip.departure_time});
				}
				
				//Get trip number
				if (trip.departure_port_id == _trip.departure_port.id && trip_number == 0){
					trip_number = _trip.trip_number;
				}
				//console.log(trip_number);
				if (trip_number > 0 && _trip.trip_number >= trip_number && trip.departure_port_id != _trip.arrival_port.id){
					//console.log(_trip.arrival_port.port_code);
					if (arr_arrival_port_id.indexOf(_trip.arrival_port.id) < 0){
						//console.log(arr_arrival_port_id.indexOf(_trip.arrival_port.id));
						arr_arrival_port_id.push(_trip.arrival_port.id);
						trip.arrival_port_list.push($scope.$root.DATA_port_list_by_port_id[_trip.arrival_port.id]);
					}
				}
				//console.log(_trip);
			}
			//console.log(_schedule);
		}
		
		trip.available_time_list = arr_departure_time;
		
		//console.log(arr_arrival_port_id);
		//console.log(arr_departure_time);
		//console.log(trip);
	}
	$scope.packageTripOptionSelectArrivalPort = function(trip){
		var arr_departure_time	= [];

		for(i in $scope.DATA.schedules.schedules){

			var _schedule = $scope.DATA.schedules.schedules[i];
			var trip_number = 0;

			for (j in _schedule.schedule_detail.schedule_detail){
				var _trip = _schedule.schedule_detail.schedule_detail[j];
				
				//Get trip number
				if (trip.departure_port_id == _trip.departure_port.id && trip_number == 0){
					trip_number = _trip.trip_number;
				}
				//console.log(trip_number);
				if (trip_number > 0 && _trip.trip_number >= trip_number && trip.departure_port_id != _trip.arrival_port.id){
					//console.log(_trip.arrival_port.port_code);
					if (trip.arrival_port_id == _trip.arrival_port.id){
						arr_departure_time.push({"value":_trip.departure_time,"text":_trip.departure_time});
					}
				}
				//console.log(_trip);
			}
			//console.log(_schedule);
		}
		
		trip.available_time_list = arr_departure_time;
	}
	
	$scope.ratesAddImportantInfo = function(var_to_add){
		var str = {"str":""};
		var_to_add.push(str);
	}
	$scope.ratesAddPickUpOrDropOffArea = function(var_to_add){
		var add = {	"area" : "",
					"time" : "",
					"price": 0,
					"type" : "way"};
		var_to_add.push(add);
	}
	$scope.packageCheckAvailableDayInWeek = function (day){
		var index = $scope.DATA.current_package.available_on.indexOf(day);
		if (index >= 0){
			$scope.DATA.current_package.available_on.splice(index,1);
		}else{
			$scope.DATA.current_package.available_on.push(day);
		}
	}
	
	$scope.saveDataPackages = function(event){

		// console.log($scope.DATA.current_rates);
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.current_package.error_desc = [];
		
		var current_package = angular.copy($scope.DATA.current_package);
		var data_package = current_package;
		
		var get_arr = function(data_str){
			var data = [];
			for (key in data_str){ 
				data.push(data_str[key].str); 
			}
			return data;
		}

		data_package.additional_item 	= get_arr(data_package.additional_item);
		data_package.inclusion			= get_arr(data_package.inclusion);
		data_package.exclusion 			= get_arr(data_package.exclusion);
		data_package.additional_info 	= get_arr(data_package.additional_info);
		
		delete data_package.rates_for.all;
		if (!data_package.rates_for.offline) delete data_package.rates_for.offline;
		if (!data_package.rates_for.walkin) delete data_package.rates_for.walkin;
		if (!data_package.rates_for.agent) delete data_package.rates_for.agent;
		
		for (_i in data_package.routes_option){
			var _routes_option = data_package.routes_option[_i];
			for (_j in _routes_option.trip){
				delete _routes_option.trip[_j].number;
				delete _routes_option.trip[_j].arrival_port_list;
				delete _routes_option.trip[_j].available_time_list;
				delete _routes_option.trip[_j].departure_port_list;
			}
		}
		
		httpSrv.post({
			url 	: "transport/packages/"+(($scope.DATA.current_package.package_code)?"update":"create"),
			data	: $.param(data_package),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					window.location = "#/transport/packages";
					toastr.success("Saved...");
				}else{
					$scope.DATA.current_package.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
					$('html, body').animate({scrollTop:200}, 400);
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	//--
});
// JavaScript Document
angular.module('app').controller('report_controller',function($scope, $http, httpSrv, $stateParams, $interval, fn, $filter){
	
	GeneralJS.activateLeftMenu("reports");	
	
	$scope.DATA = {};
	$scope.fn = fn;
	$scope.search = [];
	

	$scope.openPopUpWindowVoucher = function (base_url, booking_code, voucher_code){
		var myWindow = window.open(base_url+booking_code+'/'+voucher_code, 'VoucherWindow', 'width=800,height=600');
	}
	
	$scope.loadReportRevenueTrans = function(page){


    	//console.log('date',$scope.date);
		$scope.show_loading_DATA = true;
		if (!$scope.search) $scope.search = {}
		
		if (!$scope.$root.DATA_booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.$root.DATA_booking_source = response.data.booking_source;},
			});
		}
		
		if (!page) page = 1;
		var _search = {	"page" 			: page,
						"start_date" 	: ($scope.search.date?$scope.search.date:""),
						"group_by_trip"	: "1"
					  };
		if ($scope.search.booking_source) _search.booking_source = $scope.search.booking_source;
		
		httpSrv.post({
			url 	: "transport/report/revenue",
			data	: $.param({"search":_search}),
			success : function(response){
				$scope.show_loading_DATA = false;
				$scope.DATA = response.data;

				if (response.data.search){
					$scope.filter_date = $scope.DATA.search.start_date;
				}
			},
			error : function (response){}
		});
		
		$(".datepicker").datepicker({dateFormat: 'dd MM yy'});

	}

	$scope.loadReportForecast = function()
	{
		$scope.show_loading_DATA = true;
		$scope.now = $filter('date')(new Date(), 'yyyy');
		$scope.month = $filter('date')(new Date(), 'MM');
		$scope.year = [];
		$scope.not_found = false;

		for (var i = 0; i < 2; i++) {
			$scope.year[i] = $scope.now - i;
		}

		//console.log('year', $scope.month);

		if (!$scope.search.month) $scope.search.month = $scope.month.toString();
		if (!$scope.search.year) $scope.search.year = $scope.now.toString();

		var _search = {	
						"month" 	: $scope.search.month,
						"year"		: $scope.search.year
					  };

		//console.log(_search);

		httpSrv.post({
			url 	: "report/forcast",
			data	: $.param(_search),
			success : function(response){
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.report = response.data;
					$scope.total_forecast(response.data.total);
				}else{
					$scope.show_loading_DATA = false;
					$scope.not_found = true;
					$scope.report = [];
				}
				
				
			},
			error : function (response){}
			})
	}

	$scope.total_forecast = function(data)
	{
		var total = 0;
		angular.forEach(data, function(row){
			total += parseInt(row);
		});

		$scope.total_forcst = total;
		console.log('Total: ',$scope.total_forcst);
	}
	$scope.loadReport = function()
	{

		$scope.show_loading_DATA = true;
		$scope.search.year = $filter('date')(new Date(), 'yyyy');
		$scope.search.month = $filter('date')(new Date(), 'MM');
		$scope.search.date = $filter('date')(new Date(), 'dd');
		$scope.search.view = "month";
		$scope.show_graph = false;
		$scope.date_data = true;
		$scope.show_error = false;
		$scope.loadReportSales();
	}

	$scope.loadReportSalesData = function(date)
	{
		$scope.search = [];
		$scope.show_loading_DATA = true;
		date = $filter('date')(date, 'yyyy-M-d');
		var date2 = date.split("-", 3);
				
		$scope.search.date = date2[2];
		$scope.search.year = date2[0];
		$scope.search.month = date2[1];

		$scope.search.view = "date";

		$scope.date_data = false;

		httpSrv.post({
			url 	: "report/sales_date",
			data	: $.param({"date":date}),
			success : function(response){
				$scope.show_graph = false;
				

				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.report = response.data;
					$scope.getTotalDate(response.data);
					$scope.date_data = true;

					
				}else{
					$scope.show_loading_DATA = false;
					$scope.date_data = false;
					$scope.show_error = true;
				}
				
			},
			error : function (response){}
			})
	}

	$scope.loadOnlineDate = function(date){


		$scope.search = [];
		$scope.show_loading_DATA = true;
		date = $filter('date')(date, 'yyyy-M-d');
		var date2 = date.split("-", 3);
				
		$scope.search.date = date2[2];
		$scope.search.year = date2[0];
		$scope.search.month = date2[1];

		$scope.search.view = "date";

		
		$scope.date_data_agent = false;

		httpSrv.post({
			url 	: "report/sales_online_date",
			data	: $.param({"date":date}),
			success : function(response){

			
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.online = response.data;
					$scope.show_graph = false;
					$scope.date_data_agent = true;
					$scope.getTotalDate(response.data);
					console.log("agent", $scope.online);
					
					
				}else{
					$scope.show_loading_DATA = false;
					$scope.show_error = true;
				}
				
			},
			error : function (response){}
			})


	}

	$scope.loadOnlineMonth = function(month){


		$scope.search = [];
		$scope.show_loading_DATA = true;
		month = $filter('date')(month + '-01', 'yyyy-M-d');
		var date3 = month.split("-", 2);	

		$scope.search.year = date3[0];
		$scope.search.month = date3[1];
		$scope.search.date =  $filter('date')(new Date(), 'MM');
		$scope.search.view = "month";

		
		$scope.date_data_agent = false;

		httpSrv.post({
			url 	: "report/sales_online_month",
			data	: $.param({"month":month}),
			success : function(response){

			
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.online = response.data;
					$scope.show_graph = false;
					$scope.date_data_agent = true;
					$scope.getTotal(response.data.sales);
					$scope.getTotalAgent(response.data.agent);
					$scope.getTotalSales(response.data.sales);
					$scope.graph(response.data.graph);
					console.log("agent", $scope.online);
					
				}else{
					$scope.show_loading_DATA = false;
					$scope.show_error = true;
				}
				
			},
			error : function (response){}
			})


	}

	$scope.loadAgentDate = function(date){


		$scope.search = [];
		$scope.show_loading_DATA = true;
		date = $filter('date')(date, 'yyyy-M-d');
		var date2 = date.split("-", 3);
				
		$scope.search.date = date2[2];
		$scope.search.year = date2[0];
		$scope.search.month = date2[1];

		$scope.search.view = "date";

		
		$scope.date_data_agent = false;

		httpSrv.post({
			url 	: "report/sales_agent_date",
			data	: $.param({"date":date}),
			success : function(response){

			
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.agent = response.data;
					$scope.show_graph = false;
					$scope.date_data_agent = true;
					$scope.total_agentt(response.data);
					
				}else{
					$scope.show_loading_DATA = false;
					$scope.show_error = true;
				}
				
			},
			error : function (response){}
			})


	}

	$scope.loadAgentMonth = function(month){

		$scope.search = [];
		$scope.show_loading_DATA = true;
		month = $filter('date')(month + '-01', 'yyyy-M-d');
		var date3 = month.split("-", 2);	

		$scope.search.year = date3[0];
		$scope.search.month = date3[1];
		$scope.search.date =  $filter('date')(new Date(), 'MM');
		$scope.search.view = "month";

		
		$scope.date_data_agent = false;

		httpSrv.post({
			url 	: "report/sales_agent_month",
			data	: $.param({"month":month}),
			success : function(response){

				
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.agent = response.data;
					$scope.show_graph = false;
					$scope.date_data_agent = true;
					$scope.total_agentt(response.data);
					
				}else{
					$scope.show_loading_DATA = false;
					$scope.show_error = true;
				}
				
			},
			error : function (response){}
			})
	}

	$scope.total_agentt = function(data){

		$scope.total_agent = 0;
		var total = 0;
	    angular.forEach(data.sales, function(row){
	    	total += parseInt(row.total); 
	   	});
	   
	    $scope.total_agent = total;
	    console.log('Total Agent: ',$scope.total_agent);
	}
	$scope.loadReportSalesMonth = function(month)
	{
		$scope.search = [];
		$scope.show_loading_DATA = true;
		month = $filter('date')(month + '-01', 'yyyy-M-d');
		var date3 = month.split("-", 2);	

		$scope.search.year = date3[0];
		$scope.search.month = date3[1];
		$scope.search.date =  $filter('date')(new Date(), 'MM');
		$scope.search.view = "month";

		
		$scope.date_data = false;
		$scope.date_data_agent = false;
		httpSrv.post({
			url 	: "report/sales_month",
			data	: $.param({"month":month}),
			success : function(response){
				
				$scope.show_graph = false;
				var date = month.split("-", 2);
				
				$scope.search.year = date[0];
				$scope.search.month = date[1];
			
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.report = response.data;
					$scope.show_graph = true;
					//console.log(response.data.sales);
					//$scope.getTotalMonth(response.data.sales);
					$scope.getTotal(response.data.sales);
					$scope.getTotalAgent(response.data.agent);
					$scope.getTotalSales(response.data.sales);
					$scope.graph(response.data.graph);
					//$scope.getTotalDate(response.data);
					
				}else{
					$scope.show_loading_DATA = false;
					$scope.show_error = true;
				}
				
			},
			error : function (response){}
			})
	}

	$scope.loadReportSales = function()
	{
		
		if (!$scope.search.month) $scope.search.month = $scope.month.toString();
		if (!$scope.search.year) $scope.search.year = $scope.year.toString();
		if (!$scope.search.date) $scope.search.date = $scope.date.toString();
		$scope.show_loading_DATA = true;
		var _search = {	
						"month" 	: $scope.search.month,
						"year"		: $scope.search.year,
						"date" 		: $scope.search.date,
						"view" 		: $scope.search.view
					  };
		if ($scope.search.view != 'date') {
			$scope.show_graph = true;
			$scope.date_data = false;
			$scope.date_data_agent = false;
		}
		else{
			$scope.show_graph = false;
			$scope.date_data = true;
			$scope.date_data_agent = true;
		}
		httpSrv.post({
			url 	: "report/sales",
			data	: $.param(_search),
			success : function(response){
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.report = response.data;
					$scope.getTotal(response.data.sales);
					$scope.getTotalAgent(response.data.agent);
					//$scope.chartObject.data = response.graph;
					$scope.graph(response.data.graph);
					$scope.show_error = false;
					
					if ($scope.search.view == 'date') {
						$scope.getTotalDate(response.data);
					}else{
						$scope.getTotalSales(response.data.sales);
					}
					console.log('report', $scope.report);
				}else{
					$scope.report = [];
					$scope.show_loading_DATA = false;
					$scope.show_graph = false;
					$scope.show_error = true;
					
				}
				
			},
			error : function (response){}
			})


	}

	$scope.printReport =  function()
	{
		 var month = $stateParams.month;
		 var year = $stateParams.year;

		 $scope.month = $filter('date')(year+"-"+month+"-01", 'MMMM');
		 $scope.year = $filter('date')(year+"-"+month+"-01", 'yyyy');

		 var _search = {	
						"month" 	: month,
						"year"		: year
					  };

		httpSrv.post({
			url 	: "report/forcast",
			data	: $.param(_search),
			success : function(response){
				if (response.data.status == "SUCCESS") {
					$scope.$parent.$parent.show_print_button = true;
					$scope.report = response.data;
				}else{
					
					$scope.report = [];
				}
				
				
			},
			error : function (response){}
			})
	}
	$scope.total = 0;
	$scope.total_sales = 0;
	$scope.total_guest = 0;
	$scope.total_book = 0;

	$scope.getTotal = function(data){
	    var total = 0;
	    angular.forEach(data.sources, function(row){
	    total += parseInt(row.total); 
	    //console.log("total", row.sales);
	    	   });
	   
	    $scope.total = total;
	   
	}
	$scope.getTotalMonth = function(data){

		var total = 0;
	    var total_guest = 0;
	    var total_book = 0;
	     var currency = '';

		angular.forEach(data.sources, function(row){
			total += parseInt(row.sources.total); 
	    	total_guest += parseInt(row.sources.total_guest);
			total_book += parseInt(row.sources.total_book);

			currency = row.sources.currency;
	    
		})
		 $scope.total_sales = total;
	    $scope.total_guest = total_guest;
	    $scope.total_book = total_book;
	    $scope.currency = currency;
	}
	$scope.getTotalSales = function(data){
	    var total = 0;
	    var total_guest = 0;
	    var total_book = 0;
	    var currency = '';


	    angular.forEach(data, function(row){

	    	total += parseInt(row.sources.total); 
	    	total_guest += parseInt(row.sources.total_guest);
			total_book += parseInt(row.sources.total_book);

			currency = row.sources.currency;
	    
	    });
	   
	    $scope.total_sales = total;
	    $scope.total_guest = total_guest;
	    $scope.total_book = total_book;
	    $scope.currency = currency;
	   // console.log('total',  data);
	   
	}
	$scope.getTotalDate = function(data){
	    var total = 0;
	     var total_trip = 0;
	     var currency = '';

	    angular.forEach(data.sales, function(row){
	    	total += parseInt(row.total);
	    	total_trip += parseInt(row.book);

	    	currency = row.currency;
	    });
	   
	    $scope.total_date = total;
	    $scope.total_trip = total_trip;

	    $scope.currency = currency;
	  //  console.log('sales',data.sales);
	   
	}
	$scope.getTotalAgent = function(data){

	    var total = 0;
	    angular.forEach(data, function(row){
	    total += parseInt(row.total);

	    });

	    $scope.totalagent = total;
	}

	$scope.graph = function($object){
		$scope.chartObject = {};

	    // $scope.onions = [
	    //     {v: "Feb"},
	    //     {v: 3},
	    // ];
	    $scope.chartObject.data = $object;
	    // console.log('data 1',$object);
	    // $scope.chartObject.data = {"cols": [
	    //     {id: 1, label: "Online Booking", type: "string"},
	    //     {id: 2, label: "Online Booking", type: "number"},
	    //      //{id: "t", label: "Booking", type: "string"},
	    //     {id: 3, label: "Booking", type: "number"}
	    // ], "rows": [
	    //     {c: [
	    //         {v: "Jan"},
	    //         {v: 3},
	    //         {v:14},
	    //     ]},

	    //     {c: [
	    //         {v: "Feb"},
	    //         {v: 31}
	    //     ]},
	    //     {c: [
	    //         {v: "Mar"},
	    //         {v: 1},
	    //     ]},
	    //     {c: [
	    //         {v: "Apr"},
	    //         {v: 2},
	    //     ]},
	    //     {c: [
	    //         {v: "Apr"},
	    //         {v: 2},
	    //     ]},
	    //     {c: [
	    //         {v: "Apr"},
	    //         {v: 2},
	    //     ]},
	    //     {c: [
	    //         {v: "Apr"},
	    //         {v: 2},
	    //     ]},
	    //     {c: [
	    //         {v: "Apr"},
	    //         {v: 2},
	    //     ]},
	    //     {c: [
	    //         {v: "Apr"},
	    //         {v: 2},
	    //     ]},
	    //     {c: [
	    //         {v: "Apr"},
	    //         {v: 2},
	    //     ]},
	    //     {c: [
	    //         {v: "Apr"},
	    //         {v: 2},
	    //     ]},
	    //     {c: [
	    //         {v: "Apr"},
	    //         {v: 2},
	    //     ]},

	    // ]};
	    
	    // console.log('data 2',$scope.chartObject.data);


	    // $scope.chartObject.type = $stateParams.chartType;
	    $scope.chartObject.type = "LineChart";
	    $scope.chartObject.options = {
	        'title': 'Booking',
	        legend: { position: 'bottom' },
	        fontSize: 10,
	    }
	}
	

	$scope.loadReportCheckin = function()
	{

		$scope.search = [];
		$scope.show_loading_DATA = true;
		$scope.search.year = $filter('date')(new Date(), 'yyyy');
		$scope.search.month = $filter('date')(new Date(), 'MM');
		$scope.search.date = $filter('date')(new Date(), 'd');

		$scope.search.view = "month";
		$scope.show_graph = false;
		$scope.date_data = true;
		$scope.show_error = false;
		$scope.loadCheckin();
	}

	$scope.loadReportCheckMonth = function(month)
	{
		
		$scope.search = [];
		$scope.show_loading_DATA = true;
		month = $filter('date')(month + '-01', 'yyyy-M-d');
		var date3 = month.split("-", 2);	

		$scope.search.year = date3[0];
		$scope.search.month = date3[1];
		$scope.search.date =  $filter('date')(new Date(), 'MM');
		$scope.search.view = "month";

		console.log("search1: ", date3);
		$scope.date_data = false;

		httpSrv.post({
			url 	: "report/checkin_month",
			data	: $.param({"month":month}),
			success : function(response){
				
				$scope.show_graph = false;
				
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.report = response.data;
					$scope.show_graph = true;
					$scope.totalCheckin(response.data.sales);
					$scope.graph(response.data.graph);
					
				}else{
					$scope.show_loading_DATA = false;
					$scope.show_error = true;
				}
				
			},
			error : function (response){}
			})
	}

	$scope.loadReportCheckDate = function(date)
	{
		

		$scope.search = [];
		$scope.show_loading_DATA = true;
		date = $filter('date')(date, 'yyyy-M-d');
		var date2 = date.split("-", 3);
		// if (date2[1].length == 2) {date2[1] = date2[1].replace('0','')}
		// if (date2[2].length == 2) {
		// 	if(date2[2].charAt(1) != '0'){

		// 		date2[2] = date2[2].replace('0','')
		// 	}
		// }		
		$scope.search.date = date2[2];
		$scope.search.year = date2[0];
		$scope.search.month = date2[1];

		
		//console.log("search2: ", date2[2]);


		$scope.search.view = "date";
		$scope.date_data = false;
		console.info('dateee', date);
		httpSrv.post({
			url 	: "report/checkin_date",
			data	: $.param({"date":date}),
			success : function(response){
				
				$scope.show_graph = false;
				

				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.report = response.data;
					$scope.totalDateCheck(response.data.sales);
					$scope.date_data = true;

					
				}else{
					$scope.show_loading_DATA = false;
					$scope.date_data = false;
					$scope.show_error = true;
				}
				
			},
			error : function (error){
				console.log(error.data);
			}
			})
	}

	$scope.loadCheckin = function()
	{
		
		// if (!$scope.search.month) $scope.search.month = $scope.month.toString();
		// if (!$scope.search.year) $scope.search.year = $scope.year.toString();
		// if (!$scope.search.date) $scope.search.date = $scope.date.toString();


		$scope.show_loading_DATA = true;
		var _search = {	
						"month" 	: $scope.search.month,
						"year"		: $scope.search.year,
						"date" 		: $scope.search.date,
						"view" 		: $scope.search.view
					  };
		if ($scope.search.view != 'date') {
			$scope.show_graph = true;
			$scope.date_data = false;
		}
		else{
			$scope.show_graph = false;
			$scope.date_data = true;
		}

		httpSrv.post({
			url 	: "report/checkin",
			data	: $.param(_search),
			success : function(response){
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.report = response.data;
					// $scope.getTotal(response.data.sales);
					// $scope.getTotalAgent(response.data.agent);
					//$scope.chartObject.data = response.graph;
					$scope.graph(response.data.graph);
					$scope.show_error = false;
					
					
					if ($scope.search.view == 'date') {
						$scope.totalDateCheck(response.data.sales);
					}
					else{
						$scope.totalCheckin(response.data.sales);
					}
				}else{
					$scope.report = [];
					$scope.show_loading_DATA = false;
					$scope.show_graph = false;
					$scope.show_error = true;
					
				}
				
			},
			error : function (response){}
			})


	}
	$scope.totalDateCheck = function(data){
		
		var total = 0;
		var total_check = 0;
		angular.forEach(data, function(row){
			total += parseInt(row.qty1) + parseInt(row.qty2) + parseInt(row.qty3);
			total_check += parseInt(row.check1) + parseInt(row.check2) + parseInt(row.check3);
		});

		$scope.total_date = total;
		$scope.total_check = total_check;
	}

	$scope.totalCheckin = function(data)
	{
		console.info("Info: ",data); 
		var total = 0;
		var total_book = 0;
		var total_guest = 0;
	    angular.forEach(data, function(row){
	    	total += parseInt(row.sources.total_checkin);
	    	total_book += parseInt(row.sources.total_book);
	    	total_guest += parseInt(row.sources.total_guest);
	    	
	    	//console.log("total", row.sales);
	   	});
	   
	    $scope.total_checkin = total;
	    $scope.total_book = total_book;
	    $scope.total_guest = total_guest;


	}

});
function activate_sub_menu_agent_detail(class_menu){
	$(".agent-detail ul.nav.sub-nav li").removeClass("active");
	$(".agent-detail ul.nav.sub-nav li."+class_menu).addClass("active");
}

// untuk dapat menjalankan 2 ng-map
// angular.bootstrap(document.getElementById("Map"), ['ngMap']);
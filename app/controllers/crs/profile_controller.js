// JavaScript Document
angular.module('app').controller('profile_controller',function($scope, $http, httpSrv, $stateParams, $interval, fn){
	
	GeneralJS.activateLeftMenu("profile");
	
	$scope.DATA = {};
	$scope.fn = fn;

	$scope.loadDataVendor = function(){
		
		httpSrv.get({
			url 	: "vendor",
			success : function(response){
				$scope.DATA.vendor = response.data;
				var address = response.data.address;
				var area = response.data.area;
				var regency = response.data.regency;
				var province = response.data.province;
				var country = response.data.country;

				var a = new Array(address,area,regency,province,country);

				var address_ = a.join(", ");
				$scope.DATA.vendor.full_address = address_;

			},
			error : function (response){}
		});
		$scope.loadDataVendorSpokenLang();
	}
	$scope.loadDataVendorSpokenLang = function(){
		httpSrv.get({
			url 	: "vendor/getSpokenLanguage",
			success : function(response){
				$scope.DATA.vendor_lang_spoken = response.data.spokenlanguage;
			},
			error : function (response){}
		});
		$scope.loadDataVendorLang();
	}

	$scope.loadDataVendorLang = function(){
		httpSrv.get({
			url 	: "vendor/getLanguage",
			success : function(response){
				$scope.DATA.vendor_lang = response.data.language;
			},
			error : function (response){}
		});
	}
	
	$scope.loadTimezoneList = function(){
		httpSrv.get({
			url 	: "general/timezone_list",
			success : function(response){
				$scope.DATA.timezone_list = response.data.timezone_list;
			},
			error : function (response){}
		});
	}
	$scope.loadPhoneCodeList = function(){
		httpSrv.get({
			url 	: "general/phone_code_list",
			success : function(response){
				$scope.DATA.phone_code_list = response.data.phone_code_list;
			},
			error : function (response){}
		});
	}
	$scope.loadSosmedList = function(){
		httpSrv.get({
			url 	: "general/sosmed_list",
			success : function(response){
				$scope.DATA.sosmed_list = response.data.sosmed_list;
			},
			error : function (response){}
		});
	}
	$scope.loaddataChangePwd = function(email,code=''){
		var data_pwd = {
			email : email,
			code : code,
			old_pwd : "",
			new_pwd : "",
			ret_pwd : ""
		}
		$scope.DATA.dataPwd = data_pwd;
	}
	$scope.loadLanguageList = function(){
		httpSrv.get({
			url 	: "general/getLanguage",
			success : function(response){
				$scope.DATA.language = response.data.languages;
			},
			error : function (response){}
		});
	}
	$scope.loadSpokenLanguageList = function(){
		httpSrv.get({
			url 	: "general/getLanguageSpoken",
			success : function(response){
				$scope.DATA.spoken_language = response.data.spoken_languages;
			},
			error : function (response){}
		});
	}
	$scope.loadLanguageList = function(){
		httpSrv.get({
			url 	: "general/getLanguage",
			success : function(response){
				$scope.DATA.language = response.data.languages;
			},
			error : function (response){}
		});
	}
	$scope.removeItem = function (itemIndex) {
	  $scope.DATA.vendor.contact.other.splice($scope.DATA.vendor.contact.other.indexOf(itemIndex),1);
	};
	$scope.addItem = function (){
		$scope.inputCounter = 0;
		$scope.inputTemplate = {
			code:'',
            type:'',
            icon:'', 
            url:'',
            account:''
		};
		$scope.inputCounter += 1;
		$scope.DATA.vendor.contact.other.push($scope.inputTemplate);
	}
	$scope.save_profile = function(event,type){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$(event.target).find('input').trigger('input');
		$scope.DATA.vendor.error_msg = [];
		
		httpSrv.post({
			url 	: "vendor/profile_save/"+type,
			data	: $.param($scope.DATA.vendor),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataVendor();
					$scope.loadTimezoneList();
					$scope.loadPhoneCodeList();
					$scope.loadSosmedList();
					$(event.target).closest(".inline-edit").find(".edit-text").toggle();
					$(event.target).closest(".inline-edit").find(".main-text").toggle();
					toastr.success("Saved...");

				}else{
					$scope.DATA.vendor.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	$scope.save_lang = function(event,type){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$(event.target).find('input').trigger('input');
		$scope.DATA.vendor.error_msg = [];
		$scope.data_ = {};
		if(type=='language'){
			$scope.data_.language = $scope.DATA.vendor_lang;
			$scope.data_.default_language = $scope.DATA.vendor.default_language;
		}else if(type=='spoken_language'){
			$scope.data_.spoken_language = $scope.DATA.vendor_lang_spoken;
		}

		httpSrv.post({
			url 	: "vendor/profile_save/"+type,
			data	: $.param($scope.data_),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataVendor();
					$scope.loadTimezoneList();
					$scope.loadPhoneCodeList();
					$scope.loadSosmedList();
					$(event.target).closest(".inline-edit").find(".edit-text").toggle();
					$(event.target).closest(".inline-edit").find(".main-text").toggle();
					toastr.success("Saved...");

				}else{
					$scope.DATA.vendor.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	$scope.upload_logo = function(event){
	  	var file = $('#logo_croped').val();
	  	$scope.file = {"base64" : file};
		$(event.target).find("button:submit").attr("disabled","disabled");
		$(event.target).find('input').trigger('input');;
		$scope.DATA.vendor.error_msg = [];
	  	httpSrv.post({
			url 	: "vendor/upload_image/",
			data	: $.param($scope.file),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					location.reload();
					toastr.success("Logo changed...");

				}else{
					$scope.DATA.vendor.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
  	}

	
	$scope.min = [];
	$scope.min[0] = 0;

	$scope.num = function(data, index){
		
		if (!data) {
			$scope.tumb = 0;
		}else{
			$scope.tumb = parseInt(data) + 1;
		}
		
		$scope.min[index+1] = $scope.tumb;
		
	}

	$scope.loadDataTermsAndConditions = function(){

		$scope.is_cancellation = false;
		$scope.item = 0;

		httpSrv.get({
			url 	: "setting/terms_and_conditions",
			success : function(response){
				$scope.terms_and_conditions = response.data.terms_and_conditions;
				$scope.advanced_setting = response.data.advanced_setting;
				$scope.rules = JSON.parse(response.data.advanced_setting.cancelation_rule_crs);

				$scope.convert_int($scope.rules);
				
				if ($scope.advanced_setting.is_cancellation_crs && $scope.advanced_setting.is_cancellation_crs == 1) {
					$scope.is_cancellation = true;
				}
				if ($scope.is_cancellation == true) {
					$scope.check = 1;
				}else{
					$scope.check = 0;
				}

				$scope.DATA = response.data;
			},
			error : function (response){}
		});
	}

	$scope.convert_int = function(data){
		
		var array = [];

		angular.forEach(data, function(row){
			array.push({f_min_activation_days: parseInt(row.f_min_activation_days), f_percentage: parseInt(row.f_percentage)});
		});

		$scope.rules = array;
	}

	$scope.change_check = function(value){
		if (value == '0') {
			$scope.check_edit = false;
		}else{
			$scope.check_edit = true;
		}
	}

	$scope.addItem = function(){

		$scope.item = $scope.tumb + 1;
		var min_value = 0;

		var last_schedule_index = $scope.rules_edit.length;
		var allow_to_add = false;
		
		var last_schedule = $scope.rules_edit[(last_schedule_index - 1)];

		if (!$scope.rules_edit) {
			$scope.rules_edit = [];
		}
		
		if (last_schedule_index == 0){
			allow_to_add = true;
		}else{
			
			
			if (last_schedule.f_min_activation_days){
				
				allow_to_add = true;

				min_value = last_schedule.f_min_activation_days + 1;
				
			}else{
				alert("Please complete current email first...");
			}
		}

		var schedule_detail = {	
								"f_min_activation_days" : min_value,
								"f_percentage" : 0
							  };
		
		if (allow_to_add){
			$scope.rules_edit.push(schedule_detail);
		}
		
	}

	$scope.scheduleDetailRemoveLastItem = function(){
		$scope.rules_edit.pop();
	}

	$scope.saveAdvanceSetting = function(event){
		
		var rule = $scope.rules_edit;

		rule.add_data = [];
		
		for (i=0; i<rule.length; i++){
			var _cs_detail = rule[i];
			
			var _schedule_detail = {
										"f_min_activation_days" : _cs_detail['f_min_activation_days'],
										"f_percentage" : _cs_detail['f_percentage']
									};
			rule.add_data.push(_schedule_detail);
		}
		var data = 1;

		if ($scope.check_edit  == 0) {
			data = 0;	
			rule.add_data = [];
		}

		httpSrv.post({
			url 	: "setting/save_advanced_setting",
			data	: $.param({'setting': rule.add_data, 'is_cancellation': data}),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					
					$(".modal-backdrop.fade.in").hide();
					$(".modal.fade.in").hide();
					toastr.success("Saved...");
					$scope.loadDataTermsAndConditions();
				}else{
					
					
					$('html, body').animate({scrollTop:200}, 400);
				}
				
			}
		});

	}

	$scope.edit_terms_and_conditions = function(terms_and_conditions){
		$scope.my_terms_and_conditions = angular.copy(terms_and_conditions);
	}
	$scope.save_term_and_condition = function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$(event.target).find('input').trigger('input');
		$scope.my_terms_and_conditions.error_msg = [];
		var data = angular.copy($scope.my_terms_and_conditions);
		data.terms_and_conditions = data;
		var datas = {"terms_and_conditions":data};

		httpSrv.post({
			url 	: "setting/save_terms_and_conditions",
			data	:$.param(datas),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataTermsAndConditions();
					$("#modal-edit-tac").modal("hide");
					toastr.success("Saved...");
				}else{
					$scope.my_terms_and_conditions.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
			}
		});
	}
	
	$scope.edit_voucher_notes = function(voucher_notes){
		$scope.my_voucher_notes = angular.copy(voucher_notes);
		$scope.rules_edit = angular.copy($scope.rules);
		$scope.check_edit = angular.copy($scope.is_cancellation);
	}
	$scope.save_voucher_notes = function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$(event.target).find('input').trigger('input');
		$scope.my_voucher_notes.error_msg = [];
		
		var data = {};
		data.type = $scope.my_voucher_notes.type;
		if (data.type == "LIST"){
			data.voucher_notes_list = [];
			for (x in $scope.my_voucher_notes.list){
				if ($scope.my_voucher_notes.list[x] != ""){
					data.voucher_notes_list.push($scope.my_voucher_notes.list[x]);
				}
			}
		}else{
			data.voucher_notes_text = $scope.my_voucher_notes.text;
		}
		
		httpSrv.post({
			url 	: "setting/save_voucher_notes",
			data	:$.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataTermsAndConditions();
					$("#modal-edit-voucher-notes").modal("hide");
					toastr.success("Saved...");
				}else{
					$scope.my_voucher_notes.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}

	$scope.change_password = function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$(event.target).find('input').trigger('input');;
		$scope.DATA.vendor.error_msg = [];
		
		httpSrv.post({
			url 	: "vendor/change_password/",
			data	: $.param($scope.DATA.dataPwd),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataVendor();
					$scope.loadTimezoneList();
					$scope.loadPhoneCodeList();
					$scope.loadSosmedList();
					$scope.loaddataChangePwd($scope.DATA.dataPwd.email,$scope.DATA.dataPwd.code);
					$(event.target).closest(".inline-edit").find(".edit-text").toggle();
					$(event.target).closest(".inline-edit").find(".main-text").toggle();
					toastr.success("Password Changed...");

				}else{
					$scope.DATA.vendor.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	
}).directive("checkboxLang", function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
            // Determine initial checked boxes
            if (scope.DATA.vendor_lang.indexOf(scope.Lang.code) !== -1) {
                elem[0].checked = true;
            }

            // Update array on click
            elem.bind('click', function() {
                var index = scope.DATA.vendor_lang.indexOf(scope.Lang.code);
                // Add if checked
                if (elem[0].checked) {
                    if (index === -1) scope.DATA.vendor_lang.push(scope.Lang.code);
                }
                // Remove if unchecked
                else {
                    if (index !== -1) scope.DATA.vendor_lang.splice(index, 1);
                }
                // Sort and update DOM display
                scope.$apply(scope.DATA.vendor_lang.sort(function(a, b) {
                    return a - b
                }));
            });
        }
    }
}).directive("checkboxSpokenlang", function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
            // Determine initial checked boxes
            if (scope.DATA.vendor_lang_spoken.indexOf(scope.SpokenLang.code) !== -1) {
                elem[0].checked = true;
            }

            // Update array on click
            elem.bind('click', function() {
                var index = scope.DATA.vendor_lang_spoken.indexOf(scope.SpokenLang.code);
                // Add if checked
                if (elem[0].checked) {
                    if (index === -1) scope.DATA.vendor_lang_spoken.push(scope.SpokenLang.code);
                }
                // Remove if unchecked
                else {
                    if (index !== -1) scope.DATA.vendor_lang_spoken.splice(index, 1);
                }
                // Sort and update DOM display
                scope.$apply(scope.DATA.vendor_lang_spoken.sort(function(a, b) {
                    return a - b
                }));
            });
        }
    }
});

function activate_sub_menu_agent_detail(class_menu){
	$("ul.nav.sub-nav.profile li").removeClass("active");
	$("ul.nav.sub-nav.profile li."+class_menu).addClass("active");
}
// // JavaScript Document
// angular.module('app').controller('profile_controller',function($scope, $http, httpSrv, $stateParams, $interval){
	
// 	GeneralJS.activateLeftMenu("profile");
	
// 	$scope.DATA = {};
	
// 	$scope.loadDataVendor = function(){
// 		httpSrv.get({
// 			url 	: "vendor",
// 			success : function(response){
// 				$scope.DATA.vendor = response.data;
// 			},
// 			error : function (response){}
// 		});
// 	}
	
// 	// $scope.loadDataTermsAndConditions = function(){
// 	// 	httpSrv.get({
// 	// 		url 	: "setting/terms_and_conditions",
// 	// 		success : function(response){
// 	// 			$scope.terms_and_conditions = response.data.terms_and_conditions;
// 	// 			$scope.DATA = response.data;
// 	// 		},
// 	// 		error : function (response){}
// 	// 	});
// 	// }
// 	$scope.min = [];
// 	$scope.min[0] = 0;

// 	$scope.num = function(data, index){
		
// 		if (!data) {
// 			$scope.tumb = 0;
// 		}else{
// 			$scope.tumb = parseInt(data) + 1;
// 		}
		
// 		$scope.min[index+1] = $scope.tumb;
		
// 	}

// 	$scope.loadDataTermsAndConditions = function(){

// 		$scope.is_cancellation = false;
// 		$scope.item = 0;

// 		httpSrv.get({
// 			url 	: "setting/terms_and_conditions",
// 			success : function(response){
// 				$scope.terms_and_conditions = response.data.terms_and_conditions;
// 				$scope.advanced_setting = response.data.advanced_setting;
// 				$scope.rules = JSON.parse(response.data.advanced_setting.f_cancelation_rule_crs);

// 				$scope.convert_int($scope.rules);
				
// 				if ($scope.advanced_setting.f_is_cancellation_crs && $scope.advanced_setting.f_is_cancellation_crs == 1) {
// 					$scope.is_cancellation = true;
// 				}
// 				if ($scope.is_cancellation == true) {
// 					$scope.check = 1;
// 				}else{
// 					$scope.check = 0;
// 				}

// 				$scope.DATA = response.data;
// 			},
// 			error : function (response){}
// 		});
// 	}

// 	$scope.saveAdvanceSetting = function(event){
		
// 		var rule = $scope.rules_edit;

// 		rule.add_data = [];
		
// 		for (i=0; i<rule.length; i++){
// 			var _cs_detail = rule[i];
			
// 			var _schedule_detail = {
// 										"f_min_activation_days" : _cs_detail['f_min_activation_days'],
// 										"f_percentage" : _cs_detail['f_percentage']
// 									};
// 			rule.add_data.push(_schedule_detail);
// 		}
// 		var data = 1;

// 		if ($scope.check_edit  == 0) {
// 			data = 0;	
// 			rule.add_data = [];
// 		}

// 		httpSrv.post({
// 			url 	: "setting/save_advanced_setting",
// 			data	: $.param({'setting': rule.add_data, 'is_cancellation': data}),
// 			success : function(response){
// 				if (response.data.status == "SUCCESS"){
					
// 					$(".modal-backdrop.fade.in").hide();
// 					$(".modal.fade.in").hide();
// 					toastr.success("Saved...");
// 					$scope.loadDataTermsAndConditions();
// 				}else{
					
					
// 					$('html, body').animate({scrollTop:200}, 400);
// 				}
				
// 			}
// 		});

// 	}

// 	$scope.addItem = function(){

// 		$scope.item = $scope.tumb + 1;
// 		var min_value = 0;

// 		var last_schedule_index = $scope.rules_edit.length;
// 		var allow_to_add = false;
		
// 		var last_schedule = $scope.rules_edit[(last_schedule_index - 1)];

// 		if (!$scope.rules_edit) {
// 			$scope.rules_edit = [];
// 		}
		
// 		if (last_schedule_index == 0){
// 			allow_to_add = true;
// 		}else{
			
			
// 			if (last_schedule.f_min_activation_days){
				
// 				allow_to_add = true;

// 				min_value = last_schedule.f_min_activation_days + 1;
				
// 			}else{
// 				alert("Please complete current email first...");
// 			}
// 		}

// 		var schedule_detail = {	
// 								"f_min_activation_days" : min_value,
// 								"f_percentage" : 0
// 							  };
		
// 		if (allow_to_add){
// 			$scope.rules_edit.push(schedule_detail);
// 		}
		
// 	}

// 	$scope.change_check = function(value){
// 		if (value == '0') {
// 			$scope.check_edit = false;
// 		}else{
// 			$scope.check_edit = true;
// 		}
// 	}

// 	$scope.convert_int = function(data){
		
// 		var array = [];

// 		angular.forEach(data, function(row){
// 			array.push({f_min_activation_days: parseInt(row.f_min_activation_days), f_percentage: parseInt(row.f_percentage)});
// 		});

// 		$scope.rules = array;
// 	}

// 	$scope.edit_terms_and_conditions = function(terms_and_conditions){
// 		$scope.my_terms_and_conditions = angular.copy(terms_and_conditions);
// 	}
// 	$scope.save_term_and_condition = function(event){
// 		$(event.target).find("button:submit").attr("disabled","disabled");
// 		$(event.target).find('input').trigger('input');
// 		$scope.my_terms_and_conditions.error_msg = [];
// 		var data = angular.copy($scope.my_terms_and_conditions);
// 		data.terms_and_conditions = data;
// 		var datas = {"terms_and_conditions":data};

// 		httpSrv.post({
// 			url 	: "setting/save_terms_and_conditions",
// 			data	:$.param(datas),
// 			success : function(response){
// 				if (response.data.status == "SUCCESS"){
// 					$scope.loadDataTermsAndConditions();
// 					$("#modal-edit-tac").modal("hide");
// 					toastr.success("Saved...");
// 				}else{
// 					$scope.my_terms_and_conditions.error_msg = response.data.error_desc;
// 				}
// 				$(event.target).find("button:submit").removeAttr("disabled");
// 				//$.hideLoading();
// 			}
// 		});
// 	}
	
// 	$scope.edit_voucher_notes = function(voucher_notes){
// 		$scope.my_voucher_notes = angular.copy(voucher_notes);
// 	}
// 	$scope.save_voucher_notes = function(event){
// 		$(event.target).find("button:submit").attr("disabled","disabled");
// 		$(event.target).find('input').trigger('input');
// 		$scope.my_voucher_notes.error_msg = [];
		
// 		var data = {};
// 		data.type = $scope.my_voucher_notes.type;
// 		if (data.type == "LIST"){
// 			data.voucher_notes_list = [];
// 			for (x in $scope.my_voucher_notes.list){
// 				if ($scope.my_voucher_notes.list[x] != ""){
// 					data.voucher_notes_list.push($scope.my_voucher_notes.list[x]);
// 				}
// 			}
// 		}else{
// 			data.voucher_notes_text = $scope.my_voucher_notes.text;
// 		}
		
// 		httpSrv.post({
// 			url 	: "setting/save_voucher_notes",
// 			data	:$.param(data),
// 			success : function(response){
// 				if (response.data.status == "SUCCESS"){
// 					$scope.loadDataTermsAndConditions();
// 					$("#modal-edit-voucher-notes").modal("hide");
// 					toastr.success("Saved...");
// 				}else{
// 					$scope.my_voucher_notes.error_msg = response.data.error_desc;
// 				}
// 				$(event.target).find("button:submit").removeAttr("disabled");
// 				//$.hideLoading();
// 			}
// 		});
// 	}
	
// });

// function activate_sub_menu_agent_detail(class_menu){
// 	$("ul.nav.sub-nav.profile li").removeClass("active");
// 	$("ul.nav.sub-nav.profile li."+class_menu).addClass("active");
// }

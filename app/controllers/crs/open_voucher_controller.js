// JavaScript Document
angular.module('app').controller('open_voucher_controller',function($scope, $http, httpSrv, $stateParams, $interval, fn){
	
	GeneralJS.activateLeftMenu("open_voucher");
	
	$scope.DATA = {};
	$scope.fn = fn;
	
	$scope.loadDataOpenVoucherRates = function(){
		httpSrv.post({
			url 	: "transport/open_voucher",
			data	: $.param({"publish_status":"all"}),
			success : function(response){
				$scope.DATA.open_vouchers = response.data;
			},
			error : function (response){}
		});
	}
	$scope.loadDataOpenVoucherBooking = function(page){
		$scope.show_loading_DATA_bookings = true;
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		
		if (!$scope.search){ 
			var now = new Date(),
				start_date = new Date(),
				end_date = new Date();
			
			start_date.setDate(start_date.getDate() - 30);
			
			$scope.search = { "start_date" : $scope.fn.formatDate(start_date, "yy-mm-dd"),
							  "end_date" : $scope.fn.formatDate(end_date, "yy-mm-dd"),}; 
		}
		
		if (!page) page = 1;
		
		var _search = $scope.search;
			_search.page = page;
		
		httpSrv.post({
			url 	: "transport/open_voucher/booking",
			data	: $.param({"search":_search}),
			success : function(response){
				$scope.show_loading_DATA_bookings = false;
				$scope.DATA.bookings = response.data;
				
				if ($scope.DATA.bookings.search){
					$scope.DATA.bookings.search.pagination = [];
					for (i=1;i<=$scope.DATA.bookings.search.number_of_pages; i++){
						$scope.DATA.bookings.search.pagination.push(i);
					}
				}
			},
			error : function (response){}
		});
		
		if (!$scope.search){ 
			var now = new Date(),
				start_date = new Date(),
				end_date = new Date();
			
			start_date.setDate(start_date.getDate() - 30);
			
			$scope.search = { "start_date" : $scope.fn.formatDate(start_date, "yy-mm-dd"),
							  "end_date" : $scope.fn.formatDate(end_date, "yy-mm-dd"),}; 
		}
		
		if (!page) page = 1;
		
		var _search = $scope.search;
			_search.page = page;
		
		//Load Data Booking Source
		if (!$scope.DATA.booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.DATA.booking_source= response.data; },
			});
		}
		
		//Load Data Booking Status
		if (!$scope.DATA.booking_status){
			httpSrv.post({
				url 	: "setting/booking_status",
				success : function(response){ $scope.DATA.booking_status = response.data; },
			});
		}
	}
	
	$scope.publishUnpublishOpenVoucherRates = function (open_vouchers, status){
		
		var path = (status=='1')?"publish":"unpublish";
		open_vouchers.publish_status = status;
		
		httpSrv.get({
			url 	: "transport/open_voucher/"+path+"/"+open_vouchers.openvoucher_code,
			success : function(response){
				if (response.data.status != "SUCCESS"){
					open_vouchers.publish_status = !status;
				}
			},
			error : function (response){}
		});
	}
	
	$scope.addEditOpenVouchersRates = function (current_open_voucher){
		//console.log(current_boat);
		if (current_open_voucher){
			$scope.DATA.myOpen_voucher = angular.copy(current_open_voucher);
		}else{
			$scope.DATA.myOpen_voucher = {};
			$scope.DATA.myOpen_voucher.minimum_purchase = 1;
			$scope.DATA.myOpen_voucher.price = 0;
		}
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
	}
	$scope.saveDataOpenVouchersRates = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myOpen_voucher.error_msg = [];
		
		httpSrv.post({
			url 	: "transport/open_voucher/"+(($scope.DATA.myOpen_voucher.id)?"update":"create"),
			data	: $.param($scope.DATA.myOpen_voucher),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataOpenVoucherRates();
					$("#add-edit-open-vouchers").modal("hide");
					toastr.success("Saved...");
				}else{
					$scope.DATA.myOpen_voucher.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	
	$scope.newBookingOpenVoucher = function(event){
		$scope.DATA.data_rsv = {"booking_source" : "AGENT",
								"booking_status" : "DEFINITE",
								"discount_type"	 : "%",
								"discount_amount": 0,
								"qty"			 : 1,
								"TOTAL"			 : {"total":0,"discount":0}};
								
		//Load Open Voucher Rates
		httpSrv.post({
			url 	: "transport/open_voucher",
			success : function(response){
				$scope.DATA.open_vouchers = response.data;
			},
			error : function (response){}
		});
		
		//Load Data Agent
		httpSrv.post({
			url 	: "agent",
			data	: $.param({"search":{"limit":"all"}}),
			success : function(response){ $scope.DATA.agents = response.data; }
		});
		
		//Load Data Country List
		httpSrv.post({
			url 	: "general/country_list",
			success : function(response){ $scope.DATA.country_list = response.data; },
		});
		
		//Load Data Booking Source
		httpSrv.post({
			url 	: "setting/booking_source",
			success : function(response){ $scope.DATA.booking_source= response.data; },
		});
		
		//Load Data Booking Status
		httpSrv.post({
			url 	: "setting/booking_status",
			success : function(response){ $scope.DATA.booking_status = response.data; },
		});
		
		//Load Data Payment Method
		if (!$scope.$root.DATA_payment_method){
			httpSrv.post({
				url 	: "setting/payment_method",
				success : function(response){ $scope.$root.DATA_payment_method = response.data.payment_method; },
			});
		}
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
	}
	$scope.calculateTotal = function(){
		$scope.DATA.data_rsv.TOTAL.total = $scope.DATA.data_rsv.rates.price * $scope.DATA.data_rsv.qty;
		$scope.DATA.data_rsv.TOTAL.grand_total = $scope.DATA.data_rsv.TOTAL.total;
		
		if (!$scope.DATA.data_rsv.payment){
			$scope.DATA.data_rsv.payment = {};
		}
		
		$scope.DATA.data_rsv.payment.payment_amount = $scope.DATA.data_rsv.TOTAL.grand_total;
		$scope.DATA.data_rsv.payment.balance = $scope.DATA.data_rsv.TOTAL.grand_total - $scope.DATA.data_rsv.payment.payment_amount;
		//console.log($scope.DATA.data_rsv.payment);
	}
	
	$scope.submit_booking = function(event){

		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.data_rsv.error_desc = [];

		var data_rsv = {};
		var ss_data_rsv = angular.copy($scope.DATA.data_rsv);

		data_rsv.booking_source = ss_data_rsv.booking_source;
		if (data_rsv.booking_source == "AGENT"){
			data_rsv.agent_code = ss_data_rsv.agent.agent_code;
		}
		data_rsv.booking_status = ss_data_rsv.booking_status;
		data_rsv.discount 		= {"type":ss_data_rsv.discount_type, "amount":ss_data_rsv.discount_amount};
		data_rsv.remarks 		= ss_data_rsv.remarks;
		data_rsv.customer 		= ss_data_rsv.customer;

		data_rsv.openvoucher_code 	= ss_data_rsv.rates.openvoucher_code;
		data_rsv.qty				= ss_data_rsv.qty;

		if ($scope.DATA.data_rsv.add_payment == '1'){
			var payment = $scope.DATA.data_rsv.payment;
			data_rsv.payment = {"amount" 		: payment.payment_amount,
								"payment_type"	: payment.payment_type,
								"account_number": payment.account_number,
								"name_on_card"	: payment.name_on_card,
								"bank_name"		: payment.bank_name,
								"description"	: payment.description};
		}
		
		//$scope.DATA_RSV = data_rsv;
		//return;
		
		httpSrv.post({
			url 	: "transport/open_voucher/create_booking",
			data	: $.param(data_rsv),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					window.location = "#/open_voucher/booking_detail/"+response.data.booking_code+"/voucher_code";
					//window.location = "#/open_voucher";
					toastr.success("Saved...");
				}else{
					$(event.target).find("button:submit").attr("disabled","disabled");
					$scope.DATA.data_rsv.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
					$('html, body').animate({scrollTop:200}, 400);
				}
				//$.hideLoading();
			}
		});
		
		$scope.DATA_RSV = data_rsv;
		
		//fn.pre(ss_data_rsv,"ss_data_rsv");
		//fn.pre(data_rsv,"data_rsv");
	}

	$scope.loadDataBookingOpenVoucherDetail = function(booking_code, after_load_handler_function){

		if (!booking_code){ booking_code = $stateParams.booking_code; }
		
		httpSrv.get({
			url 	: "transport/open_voucher/booking_detail/"+booking_code,
			success : function(response){
				var booking = response.data;
				//console.log(booking);
				if (booking.status == "SUCCESS"){
					$scope.DATA.current_booking = booking;
					
					if (fn.isFn(after_load_handler_function)) {
						after_load_handler_function(booking);
					}
				}else{
					alert("Sorry, data not found..");
					window.location = "#/trans_reservation";
				}
			},
			error : function (response){}
		});
	}
	$scope.loadDataBookingOpenVoucherCode = function(){
		
		booking_code = $stateParams.booking_code;
		
		if ($scope.$parent.DATA.current_booking){
			
			$scope.DATA.current_booking = $scope.$parent.DATA.current_booking;
			
		}else{
			$scope.loadDataBookingOpenVoucherDetail(booking_code, function(current_booking){
				$scope.DATA.current_booking = current_booking;
			});
		}
	}
	$scope.blockOpenTicketCode = function(voucher_code){
		$scope.DATA.myOpenTicketCode = angular.copy(voucher_code);
		//console.log($scope.DATA.myOpenTicketCode);
	}
	$scope.saveBlockOpenTicketCOde = function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myOpenTicketCode.error_desc = [];
		
		var data = {"openticket_code"	: $scope.DATA.myOpenTicketCode.code,
					"blocked_reason"	: $scope.DATA.myOpenTicketCode.blocked_reason}
		
		//console.log(data);
		//return;
		httpSrv.post({
			url 	: "transport/open_voucher/block_openticket_code",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					toastr.success("Saved...");
					$scope.loadDataBookingOpenVoucherDetail();
					$("#block-open-ticket-form").modal("hide");
				}else{
					$(event.target).find("button:submit").attr("disabled","disabled");
					$scope.DATA.myOpenTicketCode.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
				}
				//$.hideLoading();
			}
		});
	}
	
	$scope.cancelBooking = function(booking){
		$scope.DATA.cancelation = {};
	}
	$scope.saveCancelation = function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.cancelation.error_desc = [];
		
		var data = {"booking_code" 			: $scope.DATA.current_booking.booking.booking_code,
					"booking_type"			: "OPENVOUCHER",
					"cancelation_reason"	: $scope.DATA.cancelation.cancelation_reason}
		
		//console.log(data);
		httpSrv.post({
			url 	: "transport/booking/cancel",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					toastr.success("Saved...");
					$scope.loadDataBookingOpenVoucherDetail();
					$("#cancel-booking-form").modal("hide");
				}else{
					$(event.target).find("button:submit").attr("disabled","disabled");
					$scope.DATA.cancelation.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
				}
				//$.hideLoading();
			}
		});
	}
	
	$scope.loadDataBookingPayment = function(){
		booking_code = $stateParams.booking_code;

		if ($scope.$parent.DATA.current_booking){
			$scope.DATA.current_booking = $scope.$parent.DATA.current_booking;
		}else{
			$scope.loadDataBookingOpenVoucherDetail(booking_code, function(booking_detail){
				$scope.DATA.current_booking = booking_detail;
			});
		}
		
		httpSrv.get({
			url 	: "payment/reservation/"+booking_code,
			success : function(response){
				$scope.DATA.payment = response.data;
			},
			error : function (response){}
		});
	}
	$scope.addEditPayment = function (current_payment){
		
		//Load Data Payment Method
		if (!$scope.$root.DATA_payment_method){
			httpSrv.post({
				url 	: "setting/payment_method",
				success : function(response){ $scope.$root.DATA_payment_method = response.data.payment_method; },
			});
		}
		
		if (current_payment){
			$scope.DATA.myPayment = angular.copy(current_payment);
		}else{
			var booking_code = (!$scope.DATA.current_booking.booking.booking_code)?$stateParams.booking_code:$scope.DATA.current_booking.booking.booking_code;
			
			$scope.DATA.myPayment = {"booking_code" : booking_code,
									 "date" : fn.formatDate(new Date(),"yy-mm-dd"),
									 "description":"Payment #"+$scope.DATA.current_booking.booking.booking_code,
									 "currency" : $scope.DATA.payment.payment.currency,
									 "amount" : $scope.DATA.payment.payment.balance};
		}
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
	}
	$scope.saveDataPayment = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myPayment.error_msg = [];
		
		httpSrv.post({
			url 	: "payment/create/",
			data	: $.param($scope.DATA.myPayment),
			success : function(response){
				//console.log(response.data);
				if (response.data.status == "SUCCESS"){
					$scope.loadDataBookingPayment();
					$("#add-edit-payment").modal("hide");
					toastr.success("Saved...");
				}else{
					$scope.DATA.myPayment.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	$scope.submitDeletePayment = function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.myPaymentDetail.error_msg = [];
		
		var data = {"payment_code" 	: $scope.myPaymentDetail.payment_code,
					"delete_remarks": $scope.myPaymentDetail.delete_remarks,
					"booking_code"  : $scope.DATA.current_booking.booking.booking_code};
		
		httpSrv.post({
			url 	: "payment/delete/",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataBookingPayment();
					$("#payment-detail").modal("hide");
					toastr.success("Deleted...");
				}else{
					$scope.myPaymentDetail.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
			}
		});
	}
	$scope.paymentDetail = function (current_payment, delete_payment){
		$scope.myPaymentDetail = angular.copy(current_payment);
		$scope.myPaymentDetail.amount = Math.abs($scope.myPaymentDetail.amount);
		if (delete_payment == true){
			$scope.myPaymentDetail.delete_payment = true;
			$scope.myPaymentDetail.sure_to_delete = '0';
		}else{
			delete $scope.myPaymentDetail.delete_payment;
		}
	}
	
	$scope.printReceiptOpenVoucher = function(){
		booking_code = $stateParams.booking_code;
		$scope.loadDataBookingOpenVoucherDetail(booking_code, function(){
			$scope.$parent.$parent.show_print_button = true;
		});
	}
});

function activate_sub_menu_agent_detail(class_menu){
	$(".reservation-detail ul.nav.sub-nav li").removeClass("active");
	$(".reservation-detail ul.nav.sub-nav li."+class_menu).addClass("active");
}
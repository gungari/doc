// JavaScript Document
angular.module('app').controller('report_controller',function($scope, $http, httpSrv, $stateParams, $interval, fn, $filter){
	
	GeneralJS.activateLeftMenu("reports");	
	
	$scope.DATA = {};
	$scope.fn = fn;
	$scope.search = [];

		$scope.printReportSales =  function()
		{
			$scope.show_loading_DATA = true;
			var month = $stateParams.month;
			var year = $stateParams.year;
			var date = $stateParams.date;
			var view = $stateParams.view;
			var product = $stateParams.product;

			$scope.month = month;
			$scope.year = year;
			$scope.view = view;
			$scope.date = date;
			
			var _search = {	
							"month" 	: month,
							"year"		: year,
							"date" 		: date,
							"view" 		: view,
							"product" 	: product,
						  };

			if (view != 'date') {
				$scope.show_graph = true;
				$scope.date_data = false;
				$scope.date_data_agent = true;
			}
			else{
				$scope.show_graph = false;
				$scope.date_data = true;
				$scope.date_data_agent = false;
			}

			if($scope.trips_isChecked) {

	       		httpSrv.post({
	       			url 	: "report/sales_trips",
	       			data	: $.param(_search),
	       			success : function(response){
	       				$scope.show_error = false;
	       				$scope.TRIP = response.data.report;
	       				
	       				$scope.total_trips(response.data.report);
	       				$scope.show_loading_DATA = false;
	       				$scope.show_graph = false;
	       			},
	       			error : function (response){
	       				$scope.TRIP = [];
	       				$scope.show_error = true;
	       				$scope.report = [];
	       			}
	       		});

			} else {
			   httpSrv.post({
				url 	: "report/sales",
				data	: $.param(_search),
				success : function(response){

					// $scope.getTotalSales(response.data.sales);
					// $scope.total_books(response.data.sales);
					// $scope.total_guests(response.data.sales);
					// $scope.sum(response.data.sales);
					
					if (response.data.status == "SUCCESS") {

						$scope.show_loading_DATA = false;
						$scope.report = response.data;
						
						$scope.getTotal(response.data.sales);
						$scope.getTotalAgent(response.data.agent);
						
						
						$scope.show_error = false;
						$scope.$parent.$parent.show_print_button = true;
						if (view == 'date') {
							
							$scope.getTotalDate(response.data);
							$scope.date_data_agent = true;
						}else{
							
							$scope.getTotalSales(response.data.sales);
							$scope.total_books(response.data.sales);
							$scope.total_guests(response.data.sales);
							$scope.sum(response.data.sales);
							$scope.show_graph = true;
						}
						
					}else{
						$scope.report = [];
						$scope.show_loading_DATA = false;
						$scope.show_graph = false;
						$scope.show_error = true;
						
					}
					
				},
				error : function (response){}
				})
			}

		}

	$scope.printReportCheck =  function()
	{
		$scope.show_loading_DATA = true;
		var month = $stateParams.month;
		var year = $stateParams.year;
		var date = $stateParams.date;
		var view = $stateParams.view;
		var product = $stateParams.product;

		$scope.month = month;
		$scope.year = year;
		$scope.view = view;
		$scope.date = date;
		
		var _search = {	
						"month" 	: month,
						"year"		: year,
						"date" 		: date,
						"view" 		: view,
						"product"	: product,
					  };

		if (view != 'date') {
			$scope.show_graph = true;
			$scope.date_data = false;
		}
		else{
			$scope.show_graph = false;
			$scope.date_data = true;
		}


		httpSrv.post({
			url 	: "report/checkin",
			data	: $.param(_search),
			success : function(response){
			
				if (response.data.status == "SUCCESS") {
					//$scope.totalCheckin(response.data.sales);
					$scope.$parent.$parent.show_print_button = true;
					$scope.show_loading_DATA = false;
					$scope.report = response.data;
					$scope.getTotalCheck(response.data.sales);
					
					$scope.show_error = false;
					
					if (view == 'date') {
						$scope.totalDateCheck(response.data.sales);
					}
					else{

						//$scope.graph_check(response.data.graph, $scope.search.check);
						$scope.totalCheckin(response.data.sales);

					}
				}else{
					$scope.report = [];
					$scope.show_loading_DATA = false;
					$scope.show_graph = false;
					$scope.show_error = true;
					
				}
				
			},
			error : function (response){}
			})

	}
	
	$scope.printActReport =  function()
	{
		 var month = $stateParams.month;
		 var year = $stateParams.year;
		 var product_code = $stateParams.product_code;

		 $scope.month = $filter('date')(year+"-"+month+"-01", 'MMMM');
		 $scope.year = $filter('date')(year+"-"+month+"-01", 'yyyy');
		 $scope.product_code = product_code;

		 var _search = {	
						"month" 	: month,
						"year"		: year,
						"product_code"	: product_code
					  };

		httpSrv.post({
			url 	: "report/forcast_act",
			data	: $.param(_search),
			success : function(response){
				if (response.data.status == "SUCCESS") {
					$scope.$parent.$parent.show_print_button = true;
					$scope.report = response.data;
				}else{
					
					$scope.report = [];
				}
				
				
			},
			error : function (response){}
			})
	}

	$scope.loadReportActForecast = function()
	{
		

		$scope.show_loading_DATA = true;
		$scope.now = $filter('date')(new Date(), 'yyyy');
		$scope.month = $filter('date')(new Date(), 'MM');
		$scope.product = '';
		$scope.year = [];
		$scope.not_found = false;
		$scope.year[0] = 2019;
		$scope.year[1] = $scope.now;
		$scope.year[2] = $scope.now - 1;
		// for (var i = 2; i < 3; i++) {
		// 	$scope.year[i] = $scope.now - i;
		// }


		if (!$scope.search.month) $scope.search.month = $scope.month.toString();
		if (!$scope.search.year) $scope.search.year = $scope.now.toString();

		var _search = {	
						"month" 	: $scope.search.month,
						"year"		: $scope.search.year,
						"product_code" 	: $scope.search.product
					  };

					 
		httpSrv.post({
			url 	: "report/forcast_act",
			data	: $.param(_search),
			success : function(response){
				if (response.data.status == "SUCCESS") {
					
					$scope.show_loading_DATA = false;
					$scope.report = response.data;
					$scope.total_forecast(response.data.total);
				}else{
					$scope.show_loading_DATA = false;
					$scope.not_found = true;
					$scope.report = [];
				}
				
				
			},
			error : function (response){}
		})

	}

	$scope.loadProduct = function(){

		httpSrv.post({
			url : "activities/product",
			success : function(response){
				$scope.DATA.products = response.data.products;
			},	
			error : function(err){}
		});

	}

	$scope.loadReportRevenueAct = function(page){
		$scope.show_loading_DATA = true;
		if (!$scope.search) $scope.search = {}
		
		if (!$scope.$root.DATA_booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.$root.DATA_booking_source = response.data.booking_source;},
			});
		}
		
		if (!page) page = 1;
		var _search = {	"page" 			: page,
						"start_date" 	: ($scope.search.date?$scope.search.date:""),
						"group_by_trip"	: "1"
					  };
		if ($scope.search.booking_source) _search.booking_source = $scope.search.booking_source;
		
		httpSrv.post({
			url 	: "transport/report/revenue",
			data	: $.param({"search":_search}),
			success : function(response){
				$scope.show_loading_DATA = false;
				$scope.DATA = response.data;

				if (response.data.search){
					$scope.filter_date = $scope.DATA.search.start_date;
				}
			},
			error : function (response){}
		});


		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
	}


	$scope.graph = function($object,id_name ) {

		google.charts.load('current', {'packages':['corechart']});
		var data = new google.visualization.DataTable();
		angular.forEach($object.cols, function(arr){
				
			data.addColumn(arr.type, arr.label);
		});

		angular.forEach($object.rows, function(arr){
			if (id_name == 'revenueLine') {
				data.addRows([
					[arr.c[0].v, Number(arr.c[1].v)],
				]);;
			}else{
				data.addRows([
					[arr.c[0].v, Number(arr.c[1].v), Number(arr.c[2].v), Number(arr.c[3].v)],
				]);;
			}
			
		});
		var title = 'Booking';
		
		if (id_name == 'checkLine') {
			title = "Guest";
		}
	
		var maxNumber = 10;
		for(var i=0; i<$object.rows.length; i++){
			
			for(var j=1; j<$object.rows[i]['c'].length; j++){
				if($object.rows[i]['c'][j]['v'] > maxNumber){
					maxNumber = $object.rows[i]['c'][j]['v'];
				}
			}
		}
		var options = {
			vAxis: {format: 'decimal', minValue: 0, maxValue: maxNumber, gridlines: {count: -1}, textStyle: {fontSize: 10}},
			hAxis: {textStyle: {fontSize: 8}},
	        'title': title,
	        legend: { position: 'bottom' },
	        fontSize: 10,
	        responsive: true,
	        chartArea: {width: '85%', heigth: '100%'},
		};
		if (id_name) {
			var chart = new google.visualization.LineChart(document.getElementById(id_name));
		}else{
			var chart = new google.visualization.LineChart(document.getElementById('chartLine'));
		}
		
		chart.draw(data, options);
	}

	$scope.openPopUpWindowVoucher = function (base_url, booking_code, voucher_code){
		var myWindow = window.open(base_url+booking_code+'/'+voucher_code, 'VoucherWindow', 'width=800,height=600');
	}

	$scope.loadReportRevenueTrans = function(page){
		$scope.show_loading_DATA = true;
		if (!$scope.search) $scope.search = {}
		
		if (!$scope.$root.DATA_booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.$root.DATA_booking_source = response.data.booking_source;},
			});
		}
		
		if (!page) page = 1;
		var _search = {	"page" 			: page,
						"start_date" 	: ($scope.search.date?$scope.search.date:""),
						"group_by_trip"	: "1"
					  };
		if ($scope.search.booking_source) _search.booking_source = $scope.search.booking_source;
		
		httpSrv.post({
			url 	: "transport/report/revenue",
			data	: $.param({"search":_search}),
			success : function(response){
				$scope.show_loading_DATA = false;
				$scope.DATA = response.data;
				
				
				if (response.data.search){
					$scope.filter_date = $scope.DATA.search.start_date;
				}
			},
			error : function (response){}
		});
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
	}

	// $scope.loadReportForecast = function()
	// {
	// 	$scope.show_loading_DATA = true;
	// 	$scope.now = $filter('date')(new Date(), 'yyyy');
	// 	$scope.month = $filter('date')(new Date(), 'MM');
	// 	$scope.year = [];
	// 	$scope.not_found = false;

	// 	for (var i = 0; i < 2; i++) {
	// 		$scope.year[i] = $scope.now - i;
	// 	}


	// 	if (!$scope.search.month) $scope.search.month = $scope.month.toString();
	// 	if (!$scope.search.year) $scope.search.year = $scope.now.toString();

	// 	var _search = {	
	// 					"month" 	: $scope.search.month,
	// 					"year"		: $scope.search.year
	// 				  };


	// 	httpSrv.post({
	// 		url 	: "report/forcast",
	// 		data	: $.param(_search),
	// 		success : function(response){
	// 			if (response.data.status == "SUCCESS") {
	// 				console.log('frocast',response.data);
	// 				$scope.show_loading_DATA = false;
	// 				$scope.report = response.data;
	// 				$scope.total_forecast(response.data.total);
	// 			}else{
	// 				$scope.show_loading_DATA = false;
	// 				$scope.not_found = true;
	// 				$scope.report = [];
	// 			}
				
				
	// 		},
	// 		error : function (response){}
	// 		})
	// }
	$scope.loadReportForecast = function()
	{

		$scope.show_loading_DATA = true;
		$scope.now = $filter('date')(new Date(), 'yyyy');
		$scope.month = $filter('date')(new Date(), 'MM');
		$scope.boat_id = '';
		$scope.year = [];
		$scope.not_found = false;
		
		for (var i = 0; i < 3; i++) {
			$scope.year[i] = $scope.now - i;
		}
	

		if (!$scope.search.month) $scope.search.month = $scope.month.toString();
		if (!$scope.search.year) $scope.search.year = $scope.now.toString();

		var _search = {	
						"month" 	: $scope.search.month,
						"year"		: $scope.search.year,
						"boat_id" 	: $scope.search.boat_id
					  };

					 
		httpSrv.post({
			url 	: "report/forcast",
			data	: $.param(_search),
			success : function(response){
				if (response.data.status == "SUCCESS") {
					
					$scope.show_loading_DATA = false;
					$scope.DATA = response.data;
					$scope.total_forecast(response.data.total);
				}else{
					$scope.show_loading_DATA = false;
					$scope.not_found = true;
					$scope.DATA = [];
				}
				
				
			},
			error : function (response){}
		})

		//Load Data Boat
		httpSrv.get({
			url 	: "transport/boats",
			success : function(response){
				$scope.DATA.boats = response.data;
			},
			error : function (response){}
		});

	}

	$scope.loadTransport = function(){
		//Load Data Boat
		httpSrv.get({
			url 	: "transport/boats",
			success : function(response){
				$scope.DATA.boats = response.data;
			},
			error : function (response){}
		});
	}

	$scope.total_forecast = function(data)
	{
		var total = 0;
		angular.forEach(data, function(row){
			total += parseInt(row);
		});

		$scope.total_forcst = total;
		
	}

	$scope.total_books = function(data)
	{
		var total_agent = 0;
		var total_offline = 0;
		var total_online = 0;
		angular.forEach(data, function(row){
			//total += parseInt(row.sources);
			total_agent += parseInt(row.sources['AGENT'].book);
			total_offline += parseInt(row.sources['OFFLINE'].book);
			total_online += parseInt(row.sources['ONLINE'].book);
		});

		$scope.total_book_agent = total_agent;
		$scope.total_book_offline = total_offline;
		$scope.total_book_online = total_online;
	}

	$scope.sum = function(data)
	{
		var total_agent = 0;
		var total_offline = 0;
		var total_online = 0;
		angular.forEach(data, function(row){
			//total += parseInt(row.sources);
			total_agent += parseInt(row.sources['AGENT'].total);
			total_offline += parseInt(row.sources['OFFLINE'].total);
			total_online += parseInt(row.sources['ONLINE'].total);
		});

		$scope.sum_agent = total_agent;
		$scope.sum_offline = total_offline;
		$scope.sum_online = total_online;
	}

	$scope.total_guests = function(data)
	{
		var total_agent = 0;
		var total_offline = 0;
		var total_online = 0;
		angular.forEach(data, function(row){
			//total += parseInt(row.sources['AGENT'].total_guest);
			total_agent += parseInt(row.sources['AGENT'].guest);
			total_offline += parseInt(row.sources['OFFLINE'].guest);
			total_online += parseInt(row.sources['ONLINE'].guest);
		});
		
		$scope.total_guest_agent = total_agent;
		$scope.total_guest_offline = total_offline;
		$scope.total_guest_online = total_online;
	}


	$scope.loadReport = function()
	{

		$scope.show_loading_DATA = true;
		$scope.search.year = $filter('date')(new Date(), 'yyyy');
		$scope.search.month = $filter('date')(new Date(), 'M');
		$scope.search.date = $filter('date')(new Date(), 'd');
		$scope.search.view = "month";
		$scope.search.product = '';
		$scope.show_graph = false;
		$scope.date_data = true;
		$scope.show_error = false;
		$scope.loadReportSales();
	}

	$scope.loadReportSalesData = function(date)
	{
	
		$scope.search = [];
		$scope.show_loading_DATA = true;
		date = $filter('date')(date, 'yyyy-M-d');
		var date2 = date.split("-", 3);
				
		$scope.search.date = date2[2];
		$scope.search.year = date2[0];
		$scope.search.month = date2[1];

		$scope.search.view = "date";

		$scope.date_data = false;
		$scope.date_data_agent = false;
		httpSrv.post({
			url 	: "report/sales_date",
			data	: $.param({"date":date}),
			success : function(response){
				$scope.show_graph = false;
				

				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.report = response.data;
					$scope.getTotalDate(response.data);
					$scope.date_data = true;
					$scope.total_guests(response.data);
					$scope.total_books(response.data);
					
				}else{
					$scope.show_loading_DATA = false;
					$scope.date_data = false;
					$scope.show_error = true;
				}
				
			},
			error : function (response){}
			})
	}

	$scope.loadOnlineDate = function(date){


		$scope.search = [];
		//$scope.show_loading_DATA = true;
		date = $filter('date')(date, 'yyyy-M-d');
		var date2 = date.split("-", 3);
				
		$scope.search.date = date2[2];
		$scope.search.year = date2[0];
		$scope.search.month = date2[1];

		$scope.search.view = "month";

		
		// $scope.date_data_agent = false;

		httpSrv.post({
			url 	: "report/sales_online_date",
			data	: $.param({"date":date}),
			success : function(response){

			
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.online = response.data;
					// $scope.show_graph = false;
					// $scope.date_data_agent = true;
					$scope.getTotalDate(response.data);
					
				}else{
					$scope.show_loading_DATA = false;
					$scope.show_error = true;
				}
				
			},
			error : function (response){}
			})


	}

	$scope.loadOnlineMonth = function(month){


		$scope.search = [];
		//$scope.show_loading_DATA = true;
		month = $filter('date')(month + '-01', 'yyyy-M-d');
		var date3 = month.split("-", 2);	

		$scope.search.year = date3[0];
		$scope.search.month = date3[1];
		$scope.search.date =  $filter('date')(new Date(), 'MM');
		$scope.search.view = "year";

		
		// $scope.date_data_agent = false;

		httpSrv.post({
			url 	: "report/sales_online_month",
			data	: $.param({"month":month}),
			success : function(response){

			
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.online = response.data;
					// $scope.show_graph = false;
					// $scope.date_data_agent = true;
					$scope.getTotal(response.data.sales);
					$scope.getTotalAgent(response.data.agent);
					$scope.getTotalSales(response.data.sales);
					$scope.graph(response.data.graph, "chartLine");
				
					
				}else{
					$scope.show_loading_DATA = false;
					$scope.show_error = true;
				}
				
			},
				error : function (response){}
			})
	}

	$scope.loadAgentDate = function(date){


		$scope.search = [];
		//$scope.show_loading_DATA = true;
		date = $filter('date')(date, 'yyyy-M-d');
		var date2 = date.split("-", 3);
				
		$scope.search.date = date2[2];
		$scope.search.year = date2[0];
		$scope.search.month = date2[1];

		$scope.search.view = "month";

		
		// $scope.date_data_agent = false;

		httpSrv.post({
			url 	: "report/sales_agent_date",
			data	: $.param({"date":date}),
			success : function(response){

			
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.agent = response.data;
					// $scope.show_graph = false;
					// $scope.date_data_agent = true;
					$scope.total_agentt(response.data);
					
				}else{
					$scope.show_loading_DATA = false;
					$scope.show_error = true;
				}
				
			},
			error : function (response){}
			})


	}

	$scope.loadAgentMonth = function(month){

		$scope.search = [];
		//$scope.show_loading_DATA = true;
		month = $filter('date')(month + '-01', 'yyyy-M-d');
		var date3 = month.split("-", 2);	

		$scope.search.year = date3[0];
		$scope.search.month = date3[1];
		$scope.search.date =  $filter('date')(new Date(), 'MM');
		$scope.search.view = "year";

		
		// $scope.date_data_agent = false;

		httpSrv.post({
			url 	: "report/sales_agent_month",
			data	: $.param({"month":month}),
			success : function(response){

				
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.agent = response.data;
					// $scope.show_graph = false;
					// $scope.date_data_agent = true;
					$scope.total_agentt(response.data);
					
				}else{
					$scope.show_loading_DATA = false;
					$scope.show_error = true;
				}
				
			},
			error : function (response){}
			})
	}

	$scope.total_agentt = function(data){

		$scope.total_agent = 0;
		var total = 0;
	    angular.forEach(data.sales, function(row){
	    	total += parseInt(row.total); 
	   	});
	   
	    $scope.total_agent = total;
	
	}
	$scope.loadReportSalesMonth = function(month)
	{
		$scope.search = [];
		$scope.show_loading_DATA = true;
		month = $filter('date')(month + '-01', 'yyyy-M-d');
		var date3 = month.split("-", 2);	

		$scope.search.year = date3[0];
		$scope.search.month = date3[1];
		$scope.search.date =  $filter('date')(new Date(), 'MM');
		$scope.search.view = "month";

		
		$scope.date_data = false;
		$scope.date_data_agent = false;
		httpSrv.post({
			url 	: "report/sales_month",
			data	: $.param({"month":month}),
			success : function(response){
				
				$scope.show_graph = false;
				var date = month.split("-", 2);
				
				$scope.search.year = date[0];
				$scope.search.month = date[1];
			
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.report = response.data;
					$scope.show_graph = true;
					
					$scope.getTotal(response.data.sales);
					$scope.getTotalAgent(response.data.agent);
					$scope.getTotalSales(response.data.sales);
					$scope.graph(response.data.graph, "chartLine");
					//$scope.getTotalDate(response.data);
					
				}else{
					$scope.show_loading_DATA = false;
					$scope.show_error = true;
				}
				
			},
			error : function (response){}
			})
	}

	$scope.loadReportSales = function()
	{
		
		if (!$scope.search.month) $scope.search.month = $scope.month.toString();
		if (!$scope.search.year) $scope.search.year = $scope.year.toString();
		if (!$scope.search.date) $scope.search.date = $scope.date.toString();
		$scope.show_loading_DATA = true;
		$scope.show_graph = false;

		var _search = {	
						"month" 	: $scope.search.month,
						"year"		: $scope.search.year,
						"date" 		: $scope.search.date,
						"view" 		: $scope.search.view,
						"product" 	: $scope.search.product,
					  };
		if ($scope.search.view != 'date') {
			$scope.show_graph = true;
			$scope.date_data = false;
			$scope.date_data_agent = false;
		}
		else{
			$scope.show_graph = false;
			$scope.date_data = true;
			$scope.date_data_agent = false;
		}
		httpSrv.post({
			url 	: "report/sales",
			data	: $.param(_search),
			success : function(response){
				
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.report = response.data;
					$scope.getTotal(response.data.sales);
					$scope.getTotalAgent(response.data.agent);
					//$scope.chartObject.data = response.graph;
					$scope.graph(response.data.graph, "chartLine");
					$scope.show_error = false;
					
					
					if ($scope.search.view == 'date') {
						
						$scope.getTotalDate(response.data);
						$scope.date_data_agent = false;
					}else{
						
						$scope.getTotalSales(response.data.sales);
						$scope.total_books(response.data.sales);
						$scope.total_guests(response.data.sales);
						$scope.sum(response.data.sales);
						$scope.show_graph = true;
					}
					
				}else{
					$scope.report = [];
					$scope.show_loading_DATA = false;
					$scope.show_graph = false;
					$scope.show_error = true;
					
				}
				
			},
			error : function (response){}
			})


	}

	$scope.printReport =  function()
	{
		 var month = $stateParams.month;
		 var year = $stateParams.year;
		 var boat_id = $stateParams.boat_id;

		 $scope.month = $filter('date')(year+"-"+month+"-01", 'MMMM');
		 $scope.year = $filter('date')(year+"-"+month+"-01", 'yyyy');
		 $scope.boat_id = boat_id;

		 var _search = {	
						"month" 	: month,
						"year"		: year,
						"boat_id"	: boat_id
					  };

		httpSrv.post({
			url 	: "report/forcast",
			data	: $.param(_search),
			success : function(response){
				if (response.data.status == "SUCCESS") {
					$scope.$parent.$parent.show_print_button = true;
					$scope.DATA = response.data;
				}else{
					
					$scope.DATA = [];
				}
				
				
			},
			error : function (response){}
			})
	}
	

	$scope.total = 0;
	$scope.total_sales = 0;
	$scope.total_guest = 0;
	$scope.total_book = 0;
	$scope.total_date = 0;
	$scope.total_trip = 0;
	$scope.total_not_check = 0;
	$scope.total_check = 0;

	$scope.getTotal = function(data){
		    var total = 0;
		    angular.forEach(data.sources, function(row){
		    total += parseInt(row.total); 
	});
	   
	$scope.total = total;
	   
	}
	
	$scope.getTotalMonth = function(data){

		var total = 0;
	    var total_guest = 0;
	    var total_book = 0;
	     var currency = '';

		angular.forEach(data.sources, function(row){
			total += parseInt(row.sources.total); 
	    	total_guest += parseInt(row.sources.total_guest);
			total_book += parseInt(row.sources.total_book);

			currency = row.sources.currency;
	    
		})
		 $scope.total_sales = total;
	    $scope.total_guest = total_guest;
	    $scope.total_book = total_book;
	    $scope.currency = currency;
	}
	$scope.getTotalSales = function(data){
	    var total = 0;
	    var total_guest = 0;
	    var total_book = 0;
	    var currency = '';


	    angular.forEach(data, function(row){

	    	total += parseInt(row.sources.total); 
	    	total_guest += parseInt(row.sources.total_guest);
			total_book += parseInt(row.sources.total_book);

			currency = row.sources.currency;
	    
	    });
	   
	    $scope.total_sales = total;
	    $scope.total_guest = total_guest;
	    $scope.total_book = total_book;
	    $scope.currency = currency;
	  
	   
	}
	$scope.getTotalDate = function(data){
	    var total = 0;
	     var total_trip = 0;
	     var currency = '';

	    angular.forEach(data.sales, function(row){
	    	total += parseInt(row.total);
	    	total_trip += parseInt(row.book);

	    	currency = row.currency;
	    });
	   
	    $scope.total_date = total;
	    $scope.total_trip = total_trip;

	    $scope.currency = currency;
	 
	   
	}
	$scope.getTotalAgent = function(data){

	    var total = 0;
	    angular.forEach(data, function(row){
	    total += parseInt(row.total);

	    });

	    $scope.totalagent = total;
	}

	
	$scope.loading = function(revert)
	{
		if(revert===true) {
			$('.report-content').show();
			$(".loading-animation").hide();
		} else {
			$('.report-content').hide();
			$(".loading-animation").show();
		}
	}

	$scope.loadReportCheckin = function()
	{

		$scope.search = [];
		$scope.show_loading_DATA = true;
		$scope.search.year = $filter('date')(new Date(), 'yyyy');
		$scope.search.month = $filter('date')(new Date(), 'M');
		$scope.search.date = $filter('date')(new Date(), 'd');
		$scope.search.product = '';

		$scope.search.view = "month";
		$scope.search.product = '';

		$scope.show_graph = false;
		$scope.date_data = true;
		$scope.show_error = false;
		$scope.loadCheckin();
		$scope.loadProduct();
	}

	$scope.loadReportCheckMonth = function(month)
	{
		
		$scope.search = [];
		$scope.show_loading_DATA = true;
		month = $filter('date')(month + '-01', 'yyyy-M-d');
		var date3 = month.split("-", 2);	

		$scope.search.year = date3[0];
		$scope.search.month = date3[1];
		$scope.search.date =  $filter('date')(new Date(), 'MM');
		$scope.search.view = "month";

		
		$scope.date_data = false;

		httpSrv.post({
			url 	: "report/checkin_month",
			data	: $.param({"month":month}),
			success : function(response){
				
				$scope.show_graph = false;
				
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.report = response.data;
					$scope.show_graph = true;
					$scope.totalCheckin(response.data.sales);
					$scope.graph(response.data.graph, "chartLine");
					
				}else{
					$scope.show_loading_DATA = false;
					$scope.show_error = true;
				}
				
			},
			error : function (response){}
			})
	}

	$scope.loadReportCheckDate = function(date)
	{
		

		$scope.search = [];
		$scope.show_loading_DATA = true;
		date = $filter('date')(date, 'yyyy-M-d');
		var date2 = date.split("-", 3);
		// if (date2[1].length == 2) {date2[1] = date2[1].replace('0','')}
		// if (date2[2].length == 2) {
		// 	if(date2[2].charAt(1) != '0'){

		// 		date2[2] = date2[2].replace('0','')
		// 	}
		// }		
		$scope.search.date = date2[2];
		$scope.search.year = date2[0];
		$scope.search.month = date2[1];

		
		$scope.search.view = "date";
		$scope.date_data = false;
		
		httpSrv.post({
			url 	: "report/checkin_date",
			data	: $.param({"date":date}),
			success : function(response){
				
				$scope.show_graph = false;
				

				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.report = response.data;
					$scope.totalDateCheck(response.data.sales);
					$scope.date_data = true;

					
				}else{
					$scope.show_loading_DATA = false;
					$scope.date_data = false;
					$scope.show_error = true;
				}
				
			},
			error : function (error){
				console.log(error.data);
			}
			})
	}

	$scope.loadCheckin = function()
	{
		
		$scope.report = [];
		
		$scope.show_loading_DATA = true;
		var _search = {	
						"month" 	: $scope.search.month,
						"year"		: $scope.search.year,
						"date" 		: $scope.search.date,
						"view" 		: $scope.search.view,
						"product"	: $scope.search.product,
					  };
		if ($scope.search.view != 'date') {
			$scope.show_graph = true;
			$scope.date_data = false;
		}
		else{
			$scope.show_graph = false;
			$scope.date_data = true;
		}

		httpSrv.post({
			url 	: "report/checkin",
			data	: $.param(_search),
			success : function(response){
				
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.report = response.data;
					$scope.getTotalCheck(response.data.sales);
					
					
					$scope.show_error = false;
					
					
					if ($scope.search.view == 'date') {
						$scope.totalDateCheck(response.data.sales);
					}
					else{
						$scope.graph(response.data.graph, 'checkLine');
						$scope.totalCheckin(response.data.sales);
					}
				}else{
					$scope.report = [];
					$scope.show_loading_DATA = false;
					$scope.show_graph = false;
					$scope.show_error = true;
					
				}
				
			},
			error : function (response){}
			})
	}

	$scope.getTotalCheck = function(data){
		    var total = 0;
		    angular.forEach(data.sources, function(row){
		    total += parseInt(row.total); 
		});
   
		$scope.total_check = total;
	   
	}

	$scope.totalDateCheck = function(data){
		
		var total = 0;
		var total_check = 0;
		angular.forEach(data, function(row){
			total += parseInt(row.qty1) + parseInt(row.qty2) + parseInt(row.qty3);
			total_check += parseInt(row.check1) + parseInt(row.check2) + parseInt(row.check3);
		});

		$scope.total_date = total;
		$scope.total_check = total_check;
	}

	$scope.totalCheckin = function(data)
	{
		
		var total = 0;
		var total_book = 0;
		var total_guest = 0;
		var total_not_check = 0;

		$scope.total_checkin = 0;
	    $scope.total_book = 0;
	    $scope.total_guest = 0;
	    $scope.total_not_check = 0;

	    angular.forEach(data, function(row){
	    	total += parseInt(row.total_checkin);
	    	total_book += parseInt(row.total_book);
	    	total_guest += parseInt(row.total_guest);
	    	total_not_check += parseInt(row.total_not_check);
	    	//console.log("total", row.sales);
	   	});
	   
	    $scope.total_checkin = total;
	    $scope.total_book = total_book;
	    $scope.total_guest = total_guest;
	    $scope.total_not_check = total_not_check;
	}


});
function activate_sub_menu_agent_detail(class_menu){
	$(".agent-detail ul.nav.sub-nav li").removeClass("active");
	$(".agent-detail ul.nav.sub-nav li."+class_menu).addClass("active");
}

// untuk dapat menjalankan 2 ng-map
// angular.bootstrap(document.getElementById("Map"), ['ngMap']);
// JavaScript Document

angular.module('app').controller('cash_and_bank_controller',function($scope, $http, httpSrv, $stateParams, $interval, fn){
	
	GeneralJS.activateLeftMenu("setting");	
	
	$scope.DATA = {};
	$scope.fn = fn;
	
	$scope.loadDataCashBank = function(page,status){
		$scope.show_loading_DATA_bookings = true;
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		
		if (!$scope.search){ 
			var now = new Date(),
				start_date = new Date(),
				end_date = new Date();
			
			start_date.setDate(start_date.getDate() - 30);
			
			$scope.search = { "start_date" : $scope.fn.formatDate(start_date, "yy-mm-dd"),
							  "end_date" : $scope.fn.formatDate(end_date, "yy-mm-dd"),}; 
		}
		
		if (!page) page = 1;
		
		var _search = {};
			_search.page = page;
			if(!status){
				_search.publish_status = "ALL";
			}else{
				_search.publish_status = status;
			}
		
		httpSrv.post({
			url 	: "cash_and_bank",
			data	: $.param(_search),
			success : function(response){
				$scope.show_loading_DATA_bookings = false;
				$scope.DATA.account = response.data;
				
				if ($scope.DATA.account.search){
					$scope.DATA.account.search.pagination = [];
					for (i=1;i<=$scope.DATA.account.search.number_of_pages; i++){
						$scope.DATA.account.search.pagination.push(i);
					}
				}
			},
			error : function (response){}
		});
		
		//Load Data Booking Source
		if (!$scope.DATA.booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.DATA.booking_source= response.data; },
			});
		}
		
		//Load Data Booking Status
		if (!$scope.DATA.booking_status){
			httpSrv.post({
				url 	: "setting/booking_status",
				success : function(response){ $scope.DATA.booking_status = response.data; },
			});
		}
	}
	
	$scope.publishUnpublishAccount = function (account, status){
		
			if (confirm("Click OK to continue...")){
				var path = (status=='1')?"publish":"unpublish";
				account.publish_status = status;
				
				httpSrv.get({
					url 	: "cash_and_bank/"+path+"/"+account.cash_and_bank_code,
					success : function(response){
						if (response.data.status != "SUCCESS"){
							account.publish_status = !status;
						}
					},
					error : function (response){}
				});
			}
	}

	$scope.addEditCashBank = function(data){
		if (data){
			
			$scope.DATA.CBAccount = angular.copy(data);
			//console.log($scope.DATA.myAccessRole);
		}else{
			$scope.DATA.CBAccount = {};
			$scope.DATA.CBAccount.cash_in = '1';
		}
	}

	$scope.saveDataCashBank = function(event)
	{
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.CBAccount.error_msg = [];
		
		var data = angular.copy($scope.DATA.CBAccount);
		
		
		httpSrv.post({
			url 	: "cash_and_bank/"+(($scope.DATA.CBAccount.cash_and_bank_code)?"update":"create"),
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataCashBank();
					$("#add-edit-cash-bank").modal("hide");
					toastr.success("Saved...");
				}else{
					$scope.DATA.CBAccount.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}

});

function activate_sub_menu_agent_detail(class_menu){
	$(".reservation-detail ul.nav.sub-nav li").removeClass("active");
	$(".reservation-detail ul.nav.sub-nav li."+class_menu).addClass("active");
}

// JavaScript Document
angular.module('app').controller('invoice_controller',function($scope, $http, httpSrv, $stateParams, $interval, fn,$filter,$window){
	
	GeneralJS.activateLeftMenu("invoice");	
	
	$scope.DATA = {};
	$scope.fn = fn;
	
	$scope.loadDataInvoice = function(page){
		if (!page) page = 1;
		
		var myDate = new Date();
		var nextDay = new Date(myDate);
		$scope.DATA.invoice = false;
		nextDay.setDate(myDate.getDate());
		
		if (!$scope.search) $scope.search = {};
		var _search = $scope.search;
			_search.page = page;
		
		httpSrv.post({
			url 	: "invoice",
			data	: $.param({"page":page, "search":_search}),
			success : function(response){
				$scope.DATA.invoice = response.data;
				if ($scope.DATA.invoice.search){
					$scope.DATA.invoice.search.pagination = [];
					for (i=1;i<=$scope.DATA.invoice.search.number_of_pages; i++){
						$scope.DATA.invoice.search.pagination.push(i);
					}
				}
				$scope.DATA.invoice.now = $filter('date')(nextDay, "yyyy-MM-dd");
			},
			error : function (response){}
		});

		$scope.dataSearch = {};
		$scope.dataSearch.search = {};
		$scope.dataSearch.search = {
			"agent_code": 'ALL',
			"q"	: _search.q,
		};

		// UnInvoicing
		httpSrv.post({
			url 	: "invoice/unInvoicing/",
			data	: $.param($scope.dataSearch),
			success : function(response){
				
				if (response.data.status == "SUCCESS"){
					$scope.DATA.uninvoicing = response.data.summary;
					
				}
			},
			error : function (response){}
		});
	}
	$scope.loadDataInvoiceDetail = function(is_reload,after_load_handler_function){
		if(is_reload){$scope.DATA.current_invoice = false;}
		invoice_code = $stateParams.invoice_code; 
		
		httpSrv.post({
			url 	: "invoice/detail/" + invoice_code,
			success : function(response){
				var invoice = response.data;
				invoice.type = "RESERVATION";
				if(invoice.invoice){
					var regex = /<br\s*[\/]?>/gi;
					var remarks = invoice.invoice.remarks;
					invoice.invoice.remarks = remarks.replace(regex, "\n")
				}
				if (fn.isFn(after_load_handler_function)) {
					if (invoice.status == "SUCCESS"){
						$scope.DATA.current_invoice = invoice;
						after_load_handler_function(invoice);
					}
				}else{
					$scope.DATA.current_invoice = invoice;
					if (invoice.status != "SUCCESS"){
						alert("Sorry, invoice not found..");
						window.location = "#/invoice";
					}
				}
			},
			error : function (response){}
		});
	}
	$scope.loadDataInvoiceDetailOT = function(is_reload,after_load_handler_function){
		if(is_reload){$scope.DATA.current_invoice = false;}
		invoice_code = $stateParams.invoice_code; 
		
		httpSrv.post({
			url 	: "invoice/detail_open_voucher/" + invoice_code,
			success : function(response){
				var invoice = response.data;
				invoice.type = "OPENVOUCHER";
				if(invoice.invoice){
					var regex = /<br\s*[\/]?>/gi;
					var remarks = invoice.invoice.remarks;
					invoice.invoice.remarks = remarks.replace(regex, "\n")
				}
				if (fn.isFn(after_load_handler_function)) {
					if (invoice.status == "SUCCESS"){
						$scope.DATA.current_invoice = invoice;
						after_load_handler_function(invoice);
					}
				}else{
					$scope.DATA.current_invoice = invoice;
					if (invoice.status != "SUCCESS"){
						alert("Sorry, invoice not found..");
						window.location = "#/invoice";
					}
				}
			},
			error : function (response){}
		});
	}	
	$scope.removeInvoice = function (invoice){
		
		if (confirm("Are you sure to delete this invoice ? \n\rClick Ok to continue delete Invoice...")){
			
			httpSrv.get({
				url 	: "invoice/remove/"+invoice.invoice_code,
				success : function(response){
					if (response.data.status == "SUCCESS"){
						window.location = "#/invoice";
					}
				},
				error : function (response){}
			});
		}
	}
	$scope.removeInvoiceOT = function (invoice){
		
		if (confirm("Are you sure to delete this invoice ? \n\rClick Ok to continue delete Invoice...")){
			
			httpSrv.get({
				url 	: "invoice/remove_open_voucher/"+invoice.invoice_code,
				success : function(response){
					if (response.data.status == "SUCCESS"){
						window.location = "#/invoice";
					}
				},
				error : function (response){}
			});
		}
	}
	$scope.invoiceAddEdit = function (){
		var agent_code = $stateParams.agent_code;
		var invoice_code = $stateParams.invoice_code;
		httpSrv.post({
			url 	: "agent",
			data	: $.param({"search":{"limit":"all","publish_status":"all"}}),


			//data	: $.param({"publish_status":"all","limit":"all"}),
			success : function(response){
				$scope.DATA.agents = response.data.agents;
			},
			error : function (response){}
		});
		
		if (invoice_code){
			$scope.loadDataAgentDetail(invoice_code, function(agent_detail){
				agent_detail.country_code = agent_detail.country.code;
				$scope.DATA.current_invoice = agent_detail;
				
				if (agent_detail.category){
					$scope.DATA.current_invoice.category_code = agent_detail.category.category_code;
				}else{
					$scope.DATA.current_invoice.category_code = "";
				}
				
				$scope.DATA.current_invoice.payment_method_code = agent_detail.payment_method.payment_code;				
				$scope.DATA.current_invoice.ready = true;
			});
		}else{
			if (agent_code) {
				$scope.DATA.current_invoice = {};
				$scope.DATA.current_invoice.country_code = "ID";
				$scope.DATA.current_invoice.category_code = "";
				$scope.DATA.current_invoice.ready = true;
				$scope.DATA.current_invoice.type = "RESERVATION";
				$scope.DATA.current_invoice.filter_by = "BOOKINGDATE";
				
				$scope.DATA.current_invoice.start_date = '';;
				$scope.DATA.current_invoice.finish_date = '';;
				$scope.DATA.current_invoice.agent_code = agent_code;
				$scope.loadBookinganAgent(agent_code);
			}else{
				$scope.DATA.current_invoice = {};
				$scope.DATA.current_invoice.country_code = "ID";
				$scope.DATA.current_invoice.category_code = "";
				$scope.DATA.current_invoice.ready = true;
				$scope.DATA.current_invoice.type = "RESERVATION";
				$scope.DATA.current_invoice.filter_by = "BOOKINGDATE";
				
				var myDate = new Date();
				var ownDay = new Date(myDate);
				ownDay.setDate(myDate.getDate()-30);

				$scope.DATA.current_invoice.start_date = $filter('date')(ownDay, "yyyy-MM-dd");;
				$scope.DATA.current_invoice.finish_date = $filter('date')(myDate, "yyyy-MM-dd");;
			}
		}
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		$scope.loadBookinganAgent();
	}
	$scope.loadRekNumber = function(){
		httpSrv.post({
			url 	: "vendor/getRekeningData",
			success : function(response){
				$scope.DATA.rekening = response.data.bank;
				//console.log($scope.DATA.rekening);
			},
			error : function (response){}
		});
	}

	$scope.loadBookinganAgent = function(agent_code,page,total_amount){
		
		var is_real_one = false;
		//var is_real_one = true;
		
		if(!page){
			is_real_one = true;
		}
		if(!page){page=1;}
		//console.log(is_real_one);
		$scope.DATA.loadingBookingan = true;
		$scope.dataSearch = {};
		$scope.dataSearch.search = {};
		var is_disabled = false;
		if(!agent_code){ 
			agent_code = "ALL";
			is_disabled = true;
		}

		var start = $scope.DATA.current_invoice.start_date;
		var finish = $scope.DATA.current_invoice.finish_date;


		$scope.dataSearch.search.start_date = $scope.DATA.current_invoice.start_date;
		$scope.dataSearch.search.finish_date = $scope.DATA.current_invoice.finish_date;
		
		$scope.dataSearch.search.agent_code = agent_code;
		$scope.dataSearch.search.filter_by = $scope.DATA.current_invoice.filter_by;
		$scope.dataSearch.search.page = page;
		
		var url_go = "invoice/getBookingAgentTrans";
		if($scope.DATA.current_invoice.type=="OPENVOUCHER"){
			url_go = "invoice/getBookingAgentOpenT";
		}
		httpSrv.post({
			url 	: url_go,
			data	: $.param($scope.dataSearch),
			success : function(response){

				$scope.DATA.loadingBookingan = false;
				$scope.DATA.bookings = response.data;
				//console.log($scope.DATA.bookings);
				$scope.DATA.bookings.search = $scope.DATA.bookings.bookings.search;
				if ($scope.DATA.bookings.search){
					$scope.DATA.bookings.search.pagination = [];
					for (i=1;i<=$scope.DATA.bookings.search.number_of_pages; i++){
						$scope.DATA.bookings.search.pagination.push(i);
					}
				}
				if(is_real_one){
					$scope.DATA.selectedlist = [];
					$scope.DATA.bookings.total_amount = 0;
				}
				if(total_amount){
					$scope.DATA.bookings.total_amount = total_amount;
				}
				//console.log($scope.DATA.bookings.total_amount);
				$scope.DATA.bookings.ready = true; 

				if(!$scope.DATA.bookings.currency){
					$scope.DATA.bookings.currency = "IDR";
				}
				var myDate = new Date();
				var nextDay = new Date(myDate);
				nextDay.setDate(myDate.getDate()+7);
				$scope.DATA.current_invoice.disabled = is_disabled;
				if($scope.DATA.current_invoice.type=="OPENVOUCHER"){
					$scope.DATA.bookings.colspan = 4;
					if(is_disabled){
						$scope.DATA.bookings.colspan = 5;
					}
				}else{
					$scope.DATA.bookings.colspan = 6;
					if(is_disabled){
						$scope.DATA.bookings.colspan = 7;
					}
				}
				$scope.DATA.bookings.due = "";//$filter('date')(nextDay, "yyyy-MM-dd");
				$scope.DATA.bookings.type = $scope.DATA.current_invoice.type;
				$scope.DATA.bookings.remarks = "";
				var selected_list = $scope.DATA.selectedlist;
				$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
			},
			error : function (response){}
		});

		$scope.dataSearch = {};
		$scope.dataSearch.search = {};
		$scope.dataSearch.search = {
			"agent_code": agent_code,
		};

		// UnInvoicing
		httpSrv.post({
			url 	: "invoice/unInvoicing/",
			data	: $.param($scope.dataSearch),
			success : function(response){
				//console.log(response.data);
				if (response.data.status == "SUCCESS"){
					$scope.DATA.uninvoicing = response.data.summary;
				}
			},
			error : function (response){}
		});
	}
	
	$scope.saveDataInvoice = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.current_invoice.error_desc = [];
		var data = {};
		data.invoice_type = $scope.DATA.current_invoice.type;
		data.agent_code = $scope.DATA.current_invoice.agent_code;
		data.user_edit = $('input[name]').val();
		data.due_date = $scope.DATA.bookings.due;
		data.remarks = $scope.DATA.bookings.remarks;
		data.selected_booking = $scope.DATA.selectedlist;
		data.bank = $scope.DATA.bookings.bank;

		httpSrv.post({
			url 	: "invoice/"+(($scope.DATA.current_invoice.id)?"update":"create"),
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					if($scope.DATA.current_invoice.type=="OPENVOUCHER"){
						window.location = "#/invoice/openvoucher/"+response.data.invoice_code;
					}else{
						window.location = "#/invoice/detail/"+response.data.invoice_code;
					}
					toastr.success("Saved...");
				}else{
					$scope.DATA.current_invoice.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
					$('html, body').animate({scrollTop:200}, 400);
				}
			}
		});
	}
	$scope.EditRekNumber = function(send_email){
		nvoice_code = $stateParams.invoice_code;
		$scope.send_this_to_email = 0;
		if(send_email){
			$scope.send_this_to_email = 1;
		}
		httpSrv.get({
			url 	: "invoice/getbankaccount/" + invoice_code,
			success : function(response){
				$scope.DATA.bank_account_inv = response.data;
			},
			error : function (response){}
		});

		$scope.loadRekNumber();
	}
	$scope.saveBankAccount = function(event,sendEmail){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.bank_account_inv.error_msg = [];
		var data = {};
		data.invoice_code = $scope.DATA.current_invoice.invoice.invoice_code;
		data.bank = $scope.DATA.bank_account_inv.bank_account.id;
		
		httpSrv.post({
			url 	: "invoice/bankSave",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataBookingPayment();
					$("#edit_bank_account").modal("hide");
					toastr.success("Saved...");
					if(sendEmail==1){
						$window.open('resend_email/#/email/invoice_agent/'+data.invoice_code, '_blank');
					}
				}else{
					$scope.DATA.bank_account_inv.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
			}
		});
	}
	// init payment detail
	$scope.loadDataInvoicePayment = function(){
		invoice_code = $stateParams.invoice_code;
		
		/*if ($scope.$parent.DATA.current_invoice){
			$scope.DATA.current_invoice = $scope.$parent.DATA.current_invoice;
		}else{
			$scope.loadDataInvoiceDetail(invoice_code, function(invoice_detail){
				$scope.DATA.current_invoice = invoice_detail;
			});
		}*/
		
		httpSrv.get({
			url 	: "invoice/reservation/" + invoice_code,
			success : function(response){
				$scope.DATA.payment = response.data;
				if($scope.DATA.payment.payment.invoice_type=="OPENVOUCHER"){
					$scope.loadDataInvoiceDetailOT();
				}else{
					$scope.loadDataInvoiceDetail();
				}
			},
			error : function (response){}
		});

	}
	// init payment detail
	$scope.loadDataInvoicePaymentOT = function(){
		invoice_code = $stateParams.invoice_code;
		
		/*if ($scope.$parent.DATA.current_invoice){
			$scope.DATA.current_invoice = $scope.$parent.DATA.current_invoice;
		}else{
			$scope.loadDataInvoiceDetail(invoice_code, function(invoice_detail){
				$scope.DATA.current_invoice = invoice_detail;
			});
		}*/
		
		httpSrv.get({
			url 	: "invoice/reservation/" + invoice_code,
			success : function(response){
				$scope.DATA.payment = response.data;
			},
			error : function (response){}
		});
		$scope.loadDataInvoiceDetailOT();
	}

	// button + Add Payment clicked
	$scope.addEditPayment = function (current_payment){
		invoice_code = $stateParams.invoice_code;
		/*if($scope.DATA.payment.payment.invoice_type=="OPENVOUCHER"){
			$scope.loadDataInvoiceDetailOT();
		}else{
			$scope.loadDataInvoiceDetail();
		}*/
		if (current_payment){
			$scope.DATA.myPayment = angular.copy(current_payment);
		}else{
			/*$scope.DATA.myPayment = {"booking_code" : $scope.DATA.current_invoice.invoice.invoice_code,
									 "date" : fn.formatDate(new Date(),"yy-mm-dd"),
									 "description":"Payment #"+$scope.DATA.current_invoice.invoice.invoice_code,
									 "currency" : $scope.DATA.payment.payment.currency,
									 "amount" : $scope.DATA.payment.payment.balance};
			*/
			$scope.DATA.myPayment = {"booking_code" : invoice_code,
									 "date" : fn.formatDate(new Date(),"yy-mm-dd"),
									 "description":"Payment #"+invoice_code,
									 "currency" : $scope.DATA.payment.payment.currency,
									 "amount" : $scope.DATA.payment.payment.balance};
		}
		//Load Data Payment Method
		if (!$scope.$root.DATA_payment_method){
			httpSrv.post({
				url 	: "setting/payment_method",
				success : function(response){ $scope.$root.DATA_payment_method = response.data.payment_method; },
			});
		}
		
		//Load Data Available Currency
		delete $scope.DATA.currency_converter;
		if (!$scope.$root.DATA_available_currency){
			httpSrv.post({
				url 	: "currency_converter/available_currency",
				success : function(response){ 
					if (response.data.status == 'SUCCESS' && response.data.currency && response.data.currency.length > 0){
						$scope.$root.DATA_available_currency 	= response.data; 
						$scope.DATA.myPayment.payment_currency 	= angular.copy(response.data.default_currency);
						$scope.DATA.myPayment.payment_amount	= angular.copy($scope.DATA.myPayment.amount);
						$scope.DATA.myPayment.default_currency 	= angular.copy(response.data.default_currency);
					}
				},
			});
		}else{
			$scope.DATA.myPayment.payment_currency 	= $scope.$root.DATA_available_currency.default_currency;
			$scope.DATA.myPayment.payment_amount	= angular.copy($scope.DATA.myPayment.amount);
			$scope.DATA.myPayment.default_currency 	= $scope.$root.DATA_available_currency.default_currency;
		}
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
	}
	$scope.convert_currency = function (){
		if ($scope.$root.DATA_available_currency.currency){
			var data = {"from_currency"	: $scope.DATA.myPayment.default_currency,
						"to_currency"	: $scope.DATA.myPayment.payment_currency,
						"amount"		: $scope.DATA.myPayment.amount}
			$scope.DATA.myPayment.payment_currency_loading = true;
			httpSrv.post({
					url 	: "currency_converter/convert",
					data	: $.param(data),
					success : function(response){ 
						if (response.data.status == 'SUCCESS'){
							$scope.DATA.currency_converter = response.data;
							$scope.DATA.myPayment.payment_amount = response.data.convertion.convertion_amount;
							$scope.DATA.myPayment.payment_currency = response.data.convertion.to;
						}
						$scope.DATA.myPayment.payment_currency_loading = false;
					},
				});
		}
	}
	$scope.changePaymentType = function(){
		
		$scope.DATA.payment_is_cc = false;
		$scope.DATA.payment_is_atm = false;
		
		
		for (index in $scope.$root.DATA_payment_method){
			var payment_method = $scope.$root.DATA_payment_method[index];
			if (payment_method.code == $scope.DATA.myPayment.payment_type){
				if (payment_method.type == 'CC'){
					$scope.DATA.payment_is_cc = true;
					$scope.DATA.payment_is_atm = false;
				}else if (payment_method.type == 'TRANSFER'){
					$scope.DATA.payment_is_cc = false;
					$scope.DATA.payment_is_atm = true;
				}else{
					$scope.DATA.payment_is_cc = false;
					$scope.DATA.payment_is_atm = false;
				}
				break;
			}
		}
			
	}
	// save payment
	$scope.saveDataPayment = function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myPayment.error_msg = [];
		httpSrv.post({
			url 	: "invoice/payment/",
			data	: $.param($scope.DATA.myPayment),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataBookingPayment();
					$("#add-edit-payment").modal("hide");
					toastr.success("Saved...");
					$scope.loadDataInvoiceDetail(true, function(invoice_detail){
						$scope.$parent.DATA.current_invoice = invoice_detail;
					});
				}else{
					$scope.DATA.myPayment.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
			}
		});
	}

	// reload payment detail after submission
	$scope.loadDataBookingPayment = function(){
		invoice_code = $stateParams.invoice_code;
		
		/*if ($scope.$parent.DATA.current_invoice){
			$scope.DATA.current_invoice = $scope.$parent.DATA.current_invoice;
		}else{
			$scope.loadDataInvoiceDetail(booking_code, function(invoice_detail){
				$scope.DATA.current_invoice = invoice_detail;
			});
		}*/
		httpSrv.get({
			url 	: "invoice/reservation/"+invoice_code,
			success : function(response){
				$scope.DATA.payment = response.data;
				if($scope.DATA.payment.payment.invoice_type=="OPENVOUCHER"){
					$scope.loadDataInvoiceDetailOT();
				}else{
					$scope.loadDataInvoiceDetail();
				}
			},
			error : function (response){}
		});
		//console.log($scope.DATA);

	}

	// delete payment
	$scope.submitDeletePayment = function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.myPaymentDetail.error_msg = [];
		
		var data = {
					"booking_code"  : $scope.DATA.current_invoice.invoice.invoice_code,
					"payment_code" 	: $scope.myPaymentDetail.payment_code,
					"delete_remarks": $scope.myPaymentDetail.delete_remarks
					};
		
		httpSrv.post({
			url 	: "invoice/delete_payment/",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataBookingPayment();
					$("#payment-detail").modal("hide");
					toastr.success("Deleted...");
					$scope.loadDataInvoiceDetail(true, function(invoice_detail){
						$scope.$parent.DATA.current_invoice = invoice_detail;
					});
				}else{
					$scope.myPaymentDetail.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
			}
		});
	}
	$scope.paymentDetail = function (current_payment, delete_payment){
		$scope.myPaymentDetail = angular.copy(current_payment);
		$scope.myPaymentDetail.amount = Math.abs($scope.myPaymentDetail.amount);
		if (delete_payment == true){
			$scope.myPaymentDetail.delete_payment = true;
			$scope.myPaymentDetail.sure_to_delete = '0';
		}else{
			delete $scope.myPaymentDetail.delete_payment;
		}
	}
	$scope.printInvoiceDetail = function(){
		invoice_code = $stateParams.invoice_code;
		var myDate = new Date();
		var nextDay = new Date(myDate);
		$scope.loadDataInvoiceDetail(false, function(){
			$scope.$parent.$parent.show_print_button = true;
			$scope.DATA.current_invoice.now = $filter('date')(nextDay, "yyyy-MM-dd");
		});
		
	}
	$scope.printInvoiceDetailOpenVoucher = function(){
		invoice_code = $stateParams.invoice_code;
		var myDate = new Date();
		var nextDay = new Date(myDate);
		$scope.loadDataInvoiceDetailOT(false, function(){
			$scope.$parent.$parent.show_print_button = true;
			$scope.DATA.current_invoice.now = $filter('date')(nextDay, "yyyy-MM-dd");
		});
		
	}
	$scope.sendEmail = function(){
		invoice_code = $stateParams.invoice_code;
		$scope.DATA.invoice_code = invoice_code;
		var data = {};
		data.merchant_code = MC;
		data.merchant_key = MK;
		var param = $.param(data);	
		$scope.DATA.url = API_URL+"invoice/send_email/"+invoice_code+"?"+param;
	}
	
	
}).directive("checkboxBooking", function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
            // Determine initial checked boxes
            if (scope.DATA.selectedlist.indexOf(scope.booking.id) !== -1) {
                elem[0].checked = true;
            }

            // Update array on click
            elem.bind('click', function() {
                var index = scope.DATA.selectedlist.indexOf(scope.booking.id);
                var sum = parseFloat(scope.DATA.bookings.total_amount);
                // Add if checked
                if (elem[0].checked) {
                    if (index === -1) scope.DATA.selectedlist.push(scope.booking.id);
                     sum +=scope.booking.balance;
                }
                // Remove if unchecked
                else {
                    if (index !== -1) scope.DATA.selectedlist.splice(index, 1);
                    sum -=scope.booking.balance;
                }
                scope.DATA.bookings.total_amount = sum;
                // Sort and update DOM display
                //console.log(sum);
                scope.$apply(scope.DATA.selectedlist.sort(function(a, b) {
                    return a - b
                }));
            });
        }
    }
}).directive("checkboxBookopticket", function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
            // Determine initial checked boxes
            if (scope.DATA.selectedlist.indexOf(scope.booking.open_voucher_code) !== -1) {
                elem[0].checked = true;
            }

            // Update array on click
            elem.bind('click', function() {
                var index = scope.DATA.selectedlist.indexOf(scope.booking.open_voucher_code);
                var sum = parseFloat(scope.DATA.bookings.total_amount);
                // Add if checked
                if (elem[0].checked) {
                    if (index === -1) scope.DATA.selectedlist.push(scope.booking.open_voucher_code);
                    sum +=scope.booking.balance;
                }
                // Remove if unchecked
                else {
                    if (index !== -1) scope.DATA.selectedlist.splice(index, 1);
                    sum -=scope.booking.balance;
                }
                scope.DATA.bookings.total_amount = sum;
                // Sort and update DOM display
                scope.$apply(scope.DATA.selectedlist.sort(function(a, b) {
                    return a - b
                }));
            });
            
        }
    }
})
.directive('selectAll', function($timeout, $parse) {
	return {
		restrict: 'A',
		link: function (scope, masterElement, attrs) {
			var slaveName = attrs.selectAll;
			var slaveSelector = ':checkbox[rel="' + slaveName + '"]';
			masterElement.bind('click', function() {
				var sum = parseFloat(scope.DATA.bookings.total_amount);
				angular.element(slaveSelector).each(function(i, elem) {
					var ID = angular.element(elem).attr('id');
					if(ID){
						var amount = parseFloat(angular.element(elem).attr('data-amount'));
						var index = scope.DATA.selectedlist.indexOf(ID);						
						if (index !== -1){
							scope.DATA.selectedlist.splice(index, 1);
							sum -= amount;
						}
						scope.DATA.selectedlist.push(ID);
						sum += amount;
						
					}
				});				
				scope.DATA.bookings.total_amount = sum;
				scope.$apply(scope.DATA.selectedlist.sort(function(a, b) {
                    return a - b
                }));
			});      
		}
	};
})
.directive('selectAllun', function($timeout, $parse) {
	return {
		restrict: 'A',
		link: function (scope, masterElement, attrs) {
			var slaveName = attrs.selectAllun;
			var slaveSelector = ':checkbox[rel="' + slaveName + '"]';
			masterElement.bind('click', function() {
				var sum = parseFloat(scope.DATA.bookings.total_amount);
				angular.element(slaveSelector).each(function(i, elem) {
					var ID = angular.element(elem).attr('id');
					if(ID){
						var amount = parseFloat(angular.element(elem).attr('data-amount'));
						var index = scope.DATA.selectedlist.indexOf(ID);						
						if (index !== -1){
							scope.DATA.selectedlist.splice(index, 1);
							sum -= amount;
						}
						
					}
				});		
				scope.DATA.bookings.total_amount = sum;		
				scope.$apply(scope.DATA.selectedlist.sort(function(a, b) {
                    return a - b
                }));
			});      
		}
	};
})
.directive('selectAllot', function($timeout, $parse) {
	return {
		restrict: 'A',
		link: function (scope, masterElement, attrs) {
			var slaveName = attrs.selectAllot;
			var slaveSelector = ':checkbox[rel="' + slaveName + '"]';
			masterElement.bind('click', function() {
				var sum = parseFloat(scope.DATA.bookings.total_amount);
				angular.element(slaveSelector).each(function(i, elem) {
					var ID = angular.element(elem).attr('id');
					if(ID){
						var amount = parseFloat(angular.element(elem).attr('data-amount'));
						var index = scope.DATA.selectedlist.indexOf(ID);						
						if (index !== -1){
							scope.DATA.selectedlist.splice(index, 1);
							sum -= amount;
						}
						scope.DATA.selectedlist.push(ID);
						sum += amount;
						
					}
				});				
				scope.DATA.bookings.total_amount = sum;
				scope.$apply(scope.DATA.selectedlist.sort(function(a, b) {
                    return a - b
                }));
			});      
		}
	};
})
.directive('selectAllunot', function($timeout, $parse) {
	return {
		restrict: 'A',
		link: function (scope, masterElement, attrs) {
			var slaveName = attrs.selectAllunot;
			var slaveSelector = ':checkbox[rel="' + slaveName + '"]';
			masterElement.bind('click', function() {
				var sum = parseFloat(scope.DATA.bookings.total_amount);
				angular.element(slaveSelector).each(function(i, elem) {
					var ID = angular.element(elem).attr('id');
					if(ID){
						var amount = parseFloat(angular.element(elem).attr('data-amount'));
						var index = scope.DATA.selectedlist.indexOf(ID);						
						if (index !== -1){
							scope.DATA.selectedlist.splice(index, 1);
							sum -= amount;
						}
						
					}
				});		
				scope.DATA.bookings.total_amount = sum;		
				scope.$apply(scope.DATA.selectedlist.sort(function(a, b) {
                    return a - b
                }));
			});      
		}
	};
});
function activate_sub_menu_invioce_detail(class_menu){
	$(".invoice-detail ul.nav.sub-nav li").removeClass("active");
	$(".invoice-detail ul.nav.sub-nav li."+class_menu).addClass("active");
}

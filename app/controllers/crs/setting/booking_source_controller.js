 // JavaScript Document

angular.module('app').controller('booking_source_controller',function($scope, $http, httpSrv, $stateParams, $interval, fn){
	
	GeneralJS.activateLeftMenu("setting");	
	
	$scope.DATA = {};
	$scope.fn = fn;
	
	$scope.loadDataBookingSource = function(page,status){
		httpSrv.post({
			url 	: "setting/booking_source",
			data	: $.param({"publish_status":"ALL"}),
			success : function(response){
				$scope.DATA = response.data;
			},
			error : function (response){}
		});
	}
	
	$scope.publishUnpublishBookingSource = function (booking_source_sub, status){
		
		var path = (status=='1')?"publish_booking_source":"unpublish_booking_source";
		booking_source_sub.publish_status = status;
		
		httpSrv.get({
			url 	: "setting/"+path+"/"+booking_source_sub.code,
			success : function(response){
				if (response.data.status != "SUCCESS"){
					booking_source_sub.publish_status = !status;
				}
			},
			error : function (response){}
		});
	}
	
	$scope.addEditBookingSource = function (current_booking_source){
		if (current_booking_source){
			$scope.DATA.myBookingSource = angular.copy(current_booking_source);
		}else{
			$scope.DATA.myBookingSource = {};
		}
	}
	$scope.saveDataBookingSource = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myBookingSource.error_msg = [];
		
		var data = angular.copy($scope.DATA.myBookingSource);
		
		httpSrv.post({
			url 	: "setting/"+(($scope.DATA.myBookingSource.code)?"update_booking_source":"create_booking_source"),
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataBookingSource();
					$("#add-edit-booking-source").modal("hide");
					toastr.success("Saved...");
				}else{
					$scope.DATA.myBookingSource.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
			}
		});
	}

});

function activate_sub_menu_agent_detail(class_menu){
	$(".reservation-detail ul.nav.sub-nav li").removeClass("active");
	$(".reservation-detail ul.nav.sub-nav li."+class_menu).addClass("active");
}

 // JavaScript Document

angular.module('app').controller('email_bcc_controller',function($scope, $http, httpSrv, $stateParams, $interval, fn){
	
	GeneralJS.activateLeftMenu("setting");	
	
	$scope.DATA = {};
	$scope.fn = fn;
	

	$scope.loadDataEmailBcc = function(){

		httpSrv.post({
			url 	: "setting/email_bcc",
			data	: $.param({"publish_status":"ALL"}),
			success : function(response){
				$scope.DATA = response.data;
			},
			error : function (response){}
		});
	}

	$scope.emailDetailAddItem = function(){
		var last_schedule_index = $scope.DATA.email_bcc.length;
		var allow_to_add = false;
		var schedule_detail = {	
								"email" : "",
								"name" : "" 
							  };


		if (last_schedule_index == 0){
			allow_to_add = true;
		}else{
			var last_schedule = $scope.DATA.email_bcc[(last_schedule_index - 1)];
			
			if (last_schedule.email){
				allow_to_add = true;
				//schedule_detail.email = last_schedule.email;
			}else{
				alert("Please complete current email first...");
			}
		}
		
		if (allow_to_add){
			$scope.DATA.email_bcc.push(schedule_detail);
		}
	}

	$scope.scheduleDetailRemoveLastItem = function(){
		$scope.DATA.email_bcc.pop();
	}

	$scope.saveDataSchedule = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$(event.target).find("input:text").attr("disabled","disabled");
		$(event.target).find("input:email").attr("disabled","disabled");
		$scope.DATA.email_bcc.error_desc = [];
		
		var email_bcc = $scope.DATA.email_bcc;
		
		
		email_bcc.schedule_detail = [];
		
		for (i=0; i<email_bcc.length; i++){
			var _cs_detail = email_bcc[i];
			
			var _schedule_detail = {
										"email"	:_cs_detail.email, "name"	:_cs_detail.name
									};
			email_bcc.schedule_detail.push(_schedule_detail);
		}
		//console.info(email_bcc);
		httpSrv.post({
			url 	: "setting/addEmailBcc",
			data	: $.param({'email':email_bcc}),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					// window.location = "#/transport/trips_schedule_detail/"+response.data.schedule_code;
					$(event.target).find("button:submit").removeAttr("disabled");
					toastr.success("Saved...");
				}else{
					$scope.DATA.email_bcc.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
					$('html, body').animate({scrollTop:200}, 400);
				}
				//$.hideLoading();
			}
		});
	}
	

});

function activate_sub_menu_agent_detail(class_menu){
	$(".reservation-detail ul.nav.sub-nav li").removeClass("active");
	$(".reservation-detail ul.nav.sub-nav li."+class_menu).addClass("active");
}

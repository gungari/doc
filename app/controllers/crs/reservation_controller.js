// JavaScript Document

angular.module('app').controller('reservation_controller',function($scope, $http, httpSrv, $stateParams, $interval, fn, $window){

	GeneralJS.activateLeftMenu("reservation");	
	
	$scope.DATA = {};
	$scope.fn = fn;

	// Gung Ari Update
	$scope.INFO = [];
	$scope.INFO.discount = [];
	$scope.INFO.fee = [];
	$scope.disc = [];
	$scope.DATA.add_fee = [];
	$scope.barcode_edit = {};
	$scope.loading_print = false;

	$scope.printBoardingPass = function(data){
		this.afterRender = function () {
			$window.document[data].focus();
			$window.document[data].print();
		},
		setTimeout(this.afterRender.bind(this), 0);
	}
	
	$scope.trustAsUrl = function(url, code, code2){
		
     	return url + code + '/' +code2;
    }

	$scope.loadDataBoarding = function(){
		var passenger_code = $stateParams.passenger_code;
		var booking_code = $stateParams.booking_code;
		$scope.DATA.voucher = {};
		$scope.show_getVoucherInformation = false;

		httpSrv.post({
			url 	: "transport/passenger_list/print_boarding",
			data	: $.param({"passenger_code":passenger_code, "booking_code":booking_code}),
			success : function(response){
				var voucher = response.data.voucher;
				//$scope.printFunction();
				$scope.loading_print = true;
				if (voucher.status == "SUCCESS"){
					//If the code is voucher booking
					$scope.DATA.voucher = voucher;
				}else{
					$scope.DATA.voucher = voucher;
				}
				$scope.show_getVoucherInformation = false;
			},
			error : function (response){}
		});
	}

	$scope.printReceipt = function(){
		var passenger_code = $stateParams.passenger_code;
		$scope.DATA.voucher = {};
		$scope.show_getVoucherInformation = false;
		$scope.loading_print = true;

		httpSrv.post({
			url 	: "transport/checkin/print_participant/",
			data	: $.param({"passenger_code":passenger_code}),
			success : function(response){
				var voucher = response.data;
				
				if (voucher.status == "SUCCESS"){
					//If the code is voucher booking
					$scope.show_getVoucherInformation = true;
					if (voucher.voucher){
						$scope.DATA.voucher = voucher;
					}
					$scope.PARTICIPANT_CODE = $scope.DATA.voucher.voucher.detail[0].passenger.passenger_code;
					
					$scope.printFunction();
					$scope.loading_print = false;
				}else{
					$scope.show_getVoucherInformation = true;
					angular.forEach(response.data.error_desc, function(row){
						toastr.error(row);
					});

					$("#check_voucher_code").focus();
					$("#check_voucher_code").val("");
				}
				//$scope.show_getVoucherInformation = false;
			},
			error : function (response){}
		});
	}
	
	$scope.printFunction = function(){

		this.afterRender = function () {
	        $window.print();
	    },
		
		setTimeout(this.afterRender.bind(this), 0);
	    
	}

	$scope.generateBarcode = function(){
		if (confirm('Click OK to continue...')){
			var booking_code = $stateParams.booking_code;
			httpSrv.post({
				url 	: "transport/passenger_list/auto_generate_passcode",
				data 	: $.param({"booking_code": booking_code}),
				success : function(response){
					if (response.data.status == "SUCCESS") {
						toastr.success("Generate passcode success");
						$scope.loadDataBookingDetail();
						$scope.is_generate =  false;
					}else{
						toastr.warning("Already generate passcode");
						$scope.is_generate =  false;
					}
					
				},
			    error : function (response){
			    	toastr.warning(response);
			    }
			});	
		}
		
	}

	$scope.auto_generate = false;
	$scope.autoGeneratePassocde = function (status){
		
		if(status == 1){
			
			$scope.auto_generate = true;
		}else{

			$scope.auto_generate = false;
		}
	}

	$scope.send_email_voucher = function(id, is_agent, payment_code){

		var voucher_code = $stateParams.voucher_code;
		
		httpSrv.post({
			url 	: "invoice/send_email_voucher",
			data 	: $.param({"booking_code": id, "is_agent": is_agent, 'voucher_code': voucher_code}),
			success : function(response){
				if (response.data.status == "SUCCESS") {
					toastr.success("Email Send...");
				}else{
					toastr.warning("Email Send Failed...");
				}
			},
			error : function (response){}
		});

	}

	$scope.publishUnpublishAccount = function (status){
		
		if(status == 1){
			
			$scope.auto_login = true;
		}else{

			$scope.auto_login = false;
		}
	}

	$scope.insert_pascode_sure = function(passcode, passenger_code, index, status){

		$("#add_code input#check_voucher_code").attr("disabled",true);
		$scope.showLoading = true;
		$scope.passenger_code = passenger_code;
		
		httpSrv.post({
			url : "transport/passenger_list/passcode",
			data : $.param({"passcode": passcode, "passenger_code": $scope.passenger_code, "is_autocheckin" : status}),
			success : function(response){
				
				$scope.showLoading = false;
				if (response.data.status == "SUCCESS") {
					
				$scope.barcode_edit.passenger[index].pass_code = passcode;	

				$scope.loadDataBookingDetail();
					
					
					if ($scope.barcode_edit.adult == $scope.barcode_edit.qty_1) {
						
						if (($scope.barcode_edit.child) == $scope.barcode_edit.qty_2) {
							$scope.barcode_edit.infant = $scope.barcode_edit.infant + 1;
						}else{
							$scope.barcode_edit.child = $scope.barcode_edit.child + 1;
						}
						
					}else{
						$scope.barcode_edit.adult = $scope.barcode_edit.adult + 1;

					}

					toastr.success("Success....");
					

					if (($scope.barcode_edit.adult + $scope.barcode_edit.child + $scope.barcode_edit.infant) == ($scope.barcode_edit.qty_1 + $scope.barcode_edit.qty_2 + $scope.barcode_edit.qty_3)) {
						$("#add_code input#check_voucher_code").attr("disabled",true);
						$("#add_code").modal("hide");
					}else{
						$("#add_code input#check_voucher_code").removeAttr("disabled");
					}
					
				}else{
					//$scope.barcode_edit.passenger[index].pass_code = '';
					$("#add_code input#check_voucher_code").removeAttr("disabled");
					angular.forEach(response.data.error_desc, function(row){
						toastr.error(row);
					});
					
				}
			},
			error : function(err){}
		});
	}

	$scope.getVoucherParticipant = function(){

		var keepGoing = true;
		var availabe = false;
		$scope.process = true;
		$scope.continue = false;

		$scope.data_passenger = angular.copy($scope.barcode_edit.passenger);

		angular.forEach($scope.data_passenger, function(row, index){
			
			if ((row.pass_code == null || row.pass_code == '') && keepGoing) {
				
				
				$scope.insert_pascode_sure($scope.passcode, row.passenger_code, index, $scope.auto_login);
				$scope.process = false;
				
				keepGoing = false;
				$scope.passcode = '';

			}

		});
	}

	$scope.count_passenger =  function(detail){
		
		angular.forEach(detail.passenger, function(row, index){
			
			if (( (row.pass_code != "") && (row.pass_code != null) )) {
				
				if ($scope.barcode_edit.adult == detail.qty_1) {
					
					if (($scope.barcode_edit.child) == detail.qty_2) {
						$scope.barcode_edit.infant = $scope.barcode_edit.infant + 1;
					}else{
						$scope.barcode_edit.child = $scope.barcode_edit.child + 1;
					}
					
				}else{
					$scope.barcode_edit.adult = $scope.barcode_edit.adult + 1;

				}

			}
			
		});
	}

	$scope.addBarcode = function (voucher){
		
		$scope.auto_login = true;
		
		//$("#add_code input#check_voucher_code").focus('true');
		$("#add_code input#check_voucher_code").focus();
		if ($scope.barcode_edit) {
			$scope.barcode_edit = angular.copy(voucher);
		}
		
		$scope.barcode_edit.adult = 0;
		$scope.barcode_edit.child = 0;
		$scope.barcode_edit.infant = 0;

		var keepGoing = true;
		$scope.count_passenger(voucher);

		if (($scope.barcode_edit.adult + $scope.barcode_edit.child + $scope.barcode_edit.infant) == ($scope.barcode_edit.qty_1 + $scope.barcode_edit.qty_2 + $scope.barcode_edit.qty_3)) {
			$("#add_code input#check_voucher_code").attr("disabled",true);
		}else{
			$("#add_code input#check_voucher_code").removeAttr("disabled");
		}
				
		if (!$scope.DATA.country_list){
			httpSrv.get({
				url 	: "general/country_list",
				success : function(response){
					$scope.DATA.country_list = response.data;
				},
				error : function (response){}
			});
		}
	}

	$scope.printReceiptTrans = function(){
		booking_code = $stateParams.booking_code;
		$scope.loadDataBookingTransportDetail(booking_code, function(){
			$scope.$parent.$parent.show_print_button = true;
		});
	}
	
	//Booking & Voucher List
	$scope.loadDataBooking_2 = function(page, show_loading){
		
		if (!$scope.DATA.bookings || show_loading){
			$scope.show_loading_DATA_bookings = true;
		}
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		
		if (!$scope.search){ 
			var now = new Date(),
				start_date = new Date(),
				end_date = new Date();
			
			start_date.setDate(start_date.getDate() - 10);
			
			$scope.search = { "start_date" : $scope.fn.formatDate(start_date, "yy-mm-dd"),
							  "end_date" : $scope.fn.formatDate(end_date, "yy-mm-dd"),
							  "filter_by":"BOOKINGDATE"}; 
		}
		
		if ($scope.search.booking_source == 'SELECTAGENT'){
			$scope.search.booking_source = '';
		}

		if (!page) page = 1;
		
		var _search = angular.copy($scope.search);
			_search.page = page;
		
		if (_search.booking_source){
			var cek_agent = _search.booking_source.split("--");
			if (cek_agent[0] == "AGENT"){
				delete _search.booking_source;
				_search.agent_code = cek_agent[1];
			}
		}
		
		httpSrv.post({
			url 	: "booking/booking_and_voucher_list",
			data	: $.param({"search":_search}),
			success : function(response){
				$scope.show_loading_DATA_bookings = false;
				$scope.DATA.bookings = response.data;
				
				if ($scope.DATA.bookings.search){
					$scope.DATA.bookings.search.pagination = [];
					for (i=1;i<=$scope.DATA.bookings.search.number_of_pages; i++){
						$scope.DATA.bookings.search.pagination.push(i);
					}
					if ($scope.DATA.bookings.search.agent_code){
						_search.booking_source = 'AGENT--'+$scope.DATA.bookings.search.agent_code;
					}
				}
			},
			error : function (response){}
		});
		
		//Load Data Booking Source
		if (!$scope.$root.DATA_booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.$root.DATA_booking_source = response.data.booking_source; },
			});
		}
		
		//Load Data Booking Status
		if (!$scope.$root.DATA_booking_status){
			httpSrv.post({
				url 	: "setting/booking_status",
				success : function(response){ $scope.$root.DATA_booking_status = response.data.booking_status; },
			});
		}
		
		//Load Data Agent
		/*if (!$scope.DATA.agents){
			httpSrv.post({
				url 	: "agent",
				data	: $.param({"search":{"limit":"all"}}),
				success : function(response){ $scope.DATA.agents = response.data; }
			});
		}*/
	}
	$scope.ReservationListSearchAgentPopUp = function(){
		if ($scope.search.booking_source == 'SELECTAGENT'){
			$scope.DATA.selected_agents = [];
			
			$("#modal-search-agent").modal("show");
			
			//Load Data Agent
			if (!$scope.DATA.agents){
				httpSrv.post({
					url 	: "agent",
					data	: $.param({"search":{"limit":"all"}}),
					success : function(response){ $scope.DATA.agents = response.data; }
				});
			}
			//Load Data Real Category Agent
			if (!$scope.DATA.agent_real_categories){
				httpSrv.get({
					url 	: "agent/real_category",
					success : function(response){ $scope.DATA.agent_real_categories = response.data.real_categories; }
				});
			}
			//
		}
	}
	$scope.ReservationListSearchAgentPopUpSelectAgent = function(agent){
		$scope.DATA.selected_agents = [];
		$scope.DATA.selected_agents.push(agent);
		$scope.search.booking_source = "AGENT--"+agent.agent_code;
	}
	//--
	
	//Booking Detail
	$scope.loadDataBookingDetail = function(booking_code, after_load_handler_function){
		
		if (!booking_code){ booking_code = $stateParams.booking_code; }
		
		httpSrv.get({
			url 	: "booking/detail/"+booking_code,
			success : function(response){
				var booking = response.data;
				//console.log(booking);
				if (booking.status == "SUCCESS"){
					$scope.DATA.current_booking = booking;
					
					if (fn.isFn(after_load_handler_function)) {
						after_load_handler_function(booking);
					}
				}else{
					alert("Sorry, data not found..");
					window.location = "#/reservation";
				}
			},
			error : function (response){}
		});
	}
	//--
	
	//Passenger---
	$scope.loadDataBookingPassenger = function(){
		
		booking_code = $stateParams.booking_code;
		
		var load_data_passenger = function (booking_detail){
			//console.log(booking_detail);
		}
		
		if ($scope.$parent.DATA.current_booking){
			
			$scope.DATA.current_booking = $scope.$parent.DATA.current_booking;
			
			load_data_passenger($scope.DATA.current_booking);
			
		}else{
			$scope.loadDataBookingDetail(booking_code, function(current_booking){
				$scope.DATA.current_booking = current_booking;
				load_data_passenger($scope.DATA.current_booking);
			});
		}
	}
	
	$scope.addEditPassenger = function (current_passenger, voucher){
		
		$scope.DATA.myPassenger = angular.copy(current_passenger);	
		
		if (current_passenger.is_data_updated != "1"){
			//if (current_passenger.passenger_number == 0){
				var cus = angular.copy($scope.DATA.current_booking.booking.customer);
				$scope.DATA.myPassenger.first_name	= cus.first_name;
				$scope.DATA.myPassenger.last_name 	= cus.last_name;
				$scope.DATA.myPassenger.country_code= cus.country_code;
				$scope.DATA.myPassenger.country_name= cus.country_name;
				$scope.DATA.myPassenger.email 		= cus.email;
				$scope.DATA.myPassenger.phone 		= cus.phone;
			//}else{
			//	$scope.DATA.myPassenger.first_name	= '';
			//	$scope.DATA.myPassenger.last_name 	= '';
			//}
			
		}
		
		$scope.DATA.myPassenger.voucher = voucher;
		
		if (!$scope.DATA.country_list){
			httpSrv.get({
				url 	: "general/country_list",
				success : function(response){
					$scope.DATA.country_list = response.data;
				},
				error : function (response){}
			});
		}
	}
	$scope.saveDataPassenger = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myPassenger.error_msg = [];
		
		var data = angular.copy($scope.DATA.myPassenger);
		delete data.voucher;
		
		data.booking_code = $scope.DATA.current_booking.booking.booking_code;
		data.voucher_code = $scope.DATA.myPassenger.voucher.voucher_code;
		
		httpSrv.post({
			url 	: "booking/passanger_create/",
			data	: $.param(data),
			success : function(response){
				
				if (response.data.status == "SUCCESS"){
					$scope.loadDataBookingDetail();
					$("#add-edit-passenger").modal("hide");
					toastr.success("Saved...");
				}else{
					$scope.DATA.myPassenger.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	$scope.copyPassenger = function (index_from, index_to){
		if (confirm('Click OK to continue...')){
			var booking_detail 	= $scope.DATA.current_booking.booking.detail;
			var passenger_from 	= angular.copy(booking_detail[index_from].passenger);
			var passenger_to 	= angular.copy(booking_detail[index_to].passenger);
			
			for (i in passenger_to){
				var pss_code = passenger_to[i].passenger_code;
				passenger_to[i] = angular.copy(passenger_from[i]);
				passenger_to[i].passenger_code = pss_code;
			}
			
			var data = {};
			data.booking_code 	= $scope.DATA.current_booking.booking.booking_code;
			data.voucher_code 	= booking_detail[index_to].voucher_code;
			data.passanger		= passenger_to;
			
			httpSrv.post({
				url 	: "booking/passanger_create_bulk/",
				data	: $.param(data),
				success : function(response){
					
					if (response.data.status == "SUCCESS"){
						$scope.loadDataBookingDetail();
						$("#add-edit-passenger").modal("hide");
						toastr.success("Saved...");
					}else{
						alert(response.data.error_desc);
						//$scope.DATA.myPassenger.error_msg = response.data.error_desc;
					}
					$(event.target).find("button:submit").removeAttr("disabled");
					//$.hideLoading();
				}
			});
			
			//$scope.DATA_R = data;
			
			//console.log(data);
			
			//$scope.DATA.current_booking.booking.detail[index_to].passenger = passenger_from;
			
			//console.log(passenger_from);
			//console.log(passenger_to);
		}
	}
	
	$scope.bulkEditPassenger = function (voucher){
		voucher.passenger_edit = angular.copy(voucher.passenger);
		if (!$scope.DATA.country_list){
			httpSrv.get({
				url 	: "general/country_list",
				success : function(response){
					$scope.DATA.country_list = response.data;
				},
				error : function (response){}
			});
		}
	}
	$scope.cancelBulkEditPassenger = function (voucher){
		delete voucher.passenger_edit;
	}
	$scope.saveBulkEditPassenger = function (voucher, event){
		
		$(event.target).find("button:submit").attr("disabled","disabled");
		var booking_detail 	= $scope.DATA.current_booking.booking.detail;
		
		var data = {};
		data.booking_code 	= $scope.DATA.current_booking.booking.booking_code;
		data.voucher_code 	= voucher.voucher_code;
		data.passanger		= voucher.passenger_edit;
		
		httpSrv.post({
			url 	: "booking/passanger_create_bulk/",
			data	: $.param(data),
			success : function(response){
				
				if (response.data.status == "SUCCESS"){
					$scope.loadDataBookingDetail();
					delete voucher.passenger_edit;
					toastr.success("Saved...");
				}else{
					alert(response.data.error_desc);
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});

	}
	//--
	
	//Billing Statement & Payment
	$scope.loadDataBookingPayment = function(){
		booking_code = $stateParams.booking_code;
		
		if ($scope.$parent.DATA.current_booking){
			$scope.DATA.current_booking = $scope.$parent.DATA.current_booking;
		}else{
			$scope.loadDataBookingDetail(booking_code, function(booking_detail){
				$scope.DATA.current_booking = booking_detail;
			});
		}

		$scope.booking_code = booking_code;
		
		httpSrv.get({
			url 	: "payment/reservation/"+booking_code,
			success : function(response){
				$scope.DATA.payment = response.data;
				
				//console.log($scope.DATA.payment);
			},
			error : function (response){}
		});
		
		httpSrv.get({
			url 	: "refund/reservation/"+booking_code,
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.DATA.refund = response.data;
				}
			},
			error : function (response){}
		});
	}
	$scope.paymentDetail = function (current_payment, delete_payment){
		$scope.myPaymentDetail = angular.copy(current_payment);
		$scope.myPaymentDetail.amount = Math.abs($scope.myPaymentDetail.amount);
		if (delete_payment == true){
			$scope.myPaymentDetail.delete_payment = true;
			$scope.myPaymentDetail.sure_to_delete = '0';
		}else{
			delete $scope.myPaymentDetail.delete_payment;
		}
	}
	$scope.addEditPayment = function (current_payment){
		
		if (current_payment){
			$scope.DATA.myPayment = angular.copy(current_payment);
		}else{
			$scope.DATA.myPayment = {"booking_code" : $scope.DATA.current_booking.booking.booking_code,
									 "date" : fn.formatDate(new Date(),"yy-mm-dd"),
									 "description":"Payment #"+$scope.DATA.current_booking.booking.booking_code,
									 "currency" : $scope.DATA.payment.payment.currency,
									 "amount" : $scope.DATA.payment.payment.balance};
			//$scope.DATA.myPayment.payment_type = 'OPENVOUCHER';
			//$scope.changePaymentType();
		}
		
		//Load Data Payment Method
		if (!$scope.$root.DATA_payment_method){
			httpSrv.post({
				url 	: "setting/payment_method/in",
				success : function(response){ $scope.$root.DATA_payment_method = response.data.payment_method; },
			});
		}
		
		//Load Data Available Currency
		delete $scope.DATA.currency_converter;
		if (!$scope.$root.DATA_available_currency){
			httpSrv.post({
				url 	: "currency_converter/available_currency",
				success : function(response){ 
					if (response.data.status == 'SUCCESS' && response.data.currency && response.data.currency.length > 0){
						$scope.$root.DATA_available_currency 	= response.data; 
						$scope.DATA.myPayment.payment_currency 	= angular.copy(response.data.default_currency);
						$scope.DATA.myPayment.payment_amount	= angular.copy($scope.DATA.myPayment.amount);
						$scope.DATA.myPayment.default_currency 	= angular.copy(response.data.default_currency);
					}
				},
			});
		}else{
			$scope.DATA.myPayment.payment_currency 	= $scope.$root.DATA_available_currency.default_currency;
			$scope.DATA.myPayment.payment_amount	= angular.copy($scope.DATA.myPayment.amount);
			$scope.DATA.myPayment.default_currency 	= $scope.$root.DATA_available_currency.default_currency;
		}
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});

		if ($scope.DATA.current_booking.booking.source_code == 'AGENT') {

			var agent = $scope.DATA.current_booking.booking.agent;
			var agent_code = agent.agent_code;

			//Check Agent Deposit
			$scope.DATA.myPayment.deposit = false;
			httpSrv.post({
				url 	: "agent/deposit",
				data	: $.param({"publish_status":"ALL", "agent_code":agent_code}),
				success : function(response){
					if (response.data.status == 'SUCCESS'){
						if (response.data.deposit.current_deposit > 0){
							$scope.DATA.myPayment.deposit = response.data.deposit;
						}
					}
				},
				error : function (response){}
			});

			//Check Agent Credit Limit
			if (agent.payment_type_code == "ACL"){
				$scope.agent_allow_to_use_acl = true;
				$scope.agent_allow_to_use_deposit = false;

				
				if (!agent.out_standing_invoice){
					agent.out_standing_invoice = {};
					httpSrv.post({
						url 	: "agent/out_standing_invoice/",
						data	: $.param({"agent_code":agent_code}),
						success : function(response){
							if (response.data.status == "SUCCESS"){
								agent.out_standing_invoice = response.data.out_standing_invoice;
							}
						},
						error : function (response){}
					});
				}
			}
			
		}
	}
	$scope.addEditPaymentPayWithDeposit = function(){
		if ($scope.DATA.myPayment.deposit){
			$scope.DATA.myPayment.payment_type = 'DEPOSIT';
			
			var balance_payment = $scope.DATA.payment.payment.balance;
			var deposit_remaining = $scope.DATA.myPayment.deposit.current_deposit;
			
			$scope.DATA.myPayment.amount = (deposit_remaining<balance_payment)?deposit_remaining:balance_payment;
			
		}
	}
	$scope.convert_currency = function (){
		if ($scope.$root.DATA_available_currency.currency){
			var data = {"from_currency"	: $scope.DATA.myPayment.default_currency,
						"to_currency"	: $scope.DATA.myPayment.payment_currency,
						"amount"		: $scope.DATA.myPayment.amount}
			$scope.DATA.myPayment.payment_currency_loading = true;
			httpSrv.post({
					url 	: "currency_converter/convert",
					data	: $.param(data),
					success : function(response){ 
						if (response.data.status == 'SUCCESS'){
							$scope.DATA.currency_converter = response.data;
							$scope.DATA.myPayment.payment_amount = parseFloat(response.data.convertion.convertion_amount.toFixed(2));
							$scope.DATA.myPayment.payment_currency = response.data.convertion.to;
						}
						$scope.DATA.myPayment.payment_currency_loading = false;
					},
				});
		}
	}
	$scope.saveDataPayment = function(event){
		//$.showLoading();
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myPayment.error_msg = [];
		
		$scope.error_deposit = false;
		$scope.save_payment = true;

		if ($scope.DATA.current_booking.booking.source_code == 'AGENT') {

			if ($scope.DATA.myPayment.payment_type == 'DEPOSIT') {
				var total = $scope.DATA.myPayment.current_deposit;
				if (total < $scope.DATA.myPayment.amount) {
					$scope.error_deposit = true;
					$scope.save_payment = false;
				}else{
					$scope.error_deposit = false;
				}
			}
		}
		
		if ($scope.save_payment) {	
			httpSrv.post({
				url 	: "payment/create/",
				data	: $.param($scope.DATA.myPayment),
				success : function(response){
					
					if (response.data.status == "SUCCESS"){
						$scope.loadDataBookingPayment();
						$("#add-edit-payment").modal("hide");
						toastr.success("Saved...");
						
						$scope.loadDataBookingDetail(booking_code, function(booking_detail){
							$scope.$parent.DATA.current_booking = booking_detail;
						});
					}else{
						$scope.DATA.myPayment.error_msg = response.data.error_desc;
					}
					$(event.target).find("button:submit").removeAttr("disabled");
				}
			});
		}else{
			toastr.error("Payment Failed");
		}		
	}
	$scope.changePaymentType = function(){
		
		$scope.DATA.payment_is_cc = false;
		$scope.DATA.payment_is_atm = false;
		
		if ($scope.DATA.myPayment.payment_type == 'OPENVOUCHER'){
			$scope.DATA.myPayment.openvoucher_code = [];
			var total_person = 0;
			
			for (i in $scope.DATA.current_booking.booking.detail){
				var detail = $scope.DATA.current_booking.booking.detail[i];

				total_person += detail.qty_1;
				total_person += detail.qty_2;
				total_person += detail.qty_3;
			}
			
			for (j=0; j<total_person; j++){
				$scope.DATA.myPayment.openvoucher_code.push({"code":""});
			}
			
			//console.log($scope.DATA.myPayment.openvoucher_code);
		}else{
			//console.log($scope.$root.DATA_payment_method);
			
			for (index in $scope.$root.DATA_payment_method){
				var payment_method = $scope.$root.DATA_payment_method[index];
				if (payment_method.code == $scope.DATA.myPayment.payment_type){
					if (payment_method.type == 'CC'){
						$scope.DATA.payment_is_cc = true;
						$scope.DATA.payment_is_atm = false;
					}else if (payment_method.type == 'TRANSFER'){
						$scope.DATA.payment_is_cc = false;
						$scope.DATA.payment_is_atm = true;
					}else{
						$scope.DATA.payment_is_cc = false;
						$scope.DATA.payment_is_atm = false;
					}
					break;
				}
			}
			
			//$scope.$root.DATA_payment_method.forEach(function(payment_method, index){
			//	if (payment_method.code == $scope.DATA.data_rsv.payment.payment_type){
					//console.log(item);
					//break;
			//	}
			//});
		}
	}
	$scope.checkValidateOpenVoucherCode = function (ov_code){
		delete ov_code.loading; delete ov_code.valid; delete ov_code.invalid;
		
		if (ov_code.code != ''){
			ov_code.loading = true;
			
			httpSrv.post({
				url 	: "transport/open_voucher/check_openticket_code",
				data	: $.param({"openticket_code":ov_code.code}),
				success : function(response){
					var result = response.data;
					delete ov_code.loading;
					
					if (result.status == "ERROR") ov_code.invalid = true;
					else if (result.used) ov_code.invalid = true;
					else if (result.notyet_used) ov_code.valid = true;
				}
			});
		}
	}
	$scope.submitDeletePayment = function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.myPaymentDetail.error_msg = [];
		
		var data = {"payment_code" 	: $scope.myPaymentDetail.payment_code,
					"delete_remarks": $scope.myPaymentDetail.delete_remarks,
					"booking_code"  : $scope.DATA.current_booking.booking.booking_code};
		
		httpSrv.post({
			url 	: "payment/delete/",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataBookingPayment();
					$("#payment-detail").modal("hide");
					toastr.success("Deleted...");
					$scope.loadDataBookingDetail(booking_code, function(booking_detail){
						$scope.$parent.DATA.current_booking = booking_detail;
					});
				}else{
					$scope.myPaymentDetail.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
			}
		});
	}
	//--
	
	//Void Booking
	$scope.voidBooking = function(booking){
		$scope.DATA.void = {'void_all_trip' : 0,
							'voucher':{}};
		for (i in $scope.DATA.current_booking.booking.detail){
			var voucher = {};
			voucher.code = $scope.DATA.current_booking.booking.detail[i].voucher_code;
			voucher.is_void = 0;
			$scope.DATA.void.voucher[voucher.code] = voucher;
		}
		$scope.DATA.void.void_all_trip = 1;
		$scope.chkVoidAllBookingClick();
	}
	$scope.chkVoidAllBookingClick = function(check_void){
		if (!check_void){
			var is_void_all = $scope.DATA.void.void_all_trip;
	
			for (code in $scope.DATA.void.voucher){
				$scope.DATA.void.voucher[code].is_void = is_void_all;
			}
		}else{
			var is_void_all = 1;
			for (code in $scope.DATA.void.voucher){
				if(!$scope.DATA.void.voucher[code].is_void){
					is_void_all = 0;
					break;
				}
			}
			$scope.DATA.void.void_all_trip = is_void_all;
		}
	}
	$scope.saveVoidBooking = function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.void.error_desc = [];
		
		var data = {"booking_code" 	: $scope.DATA.current_booking.booking.booking_code,
					"void_reason"	: $scope.DATA.void.void_reason}
		
		if ($scope.DATA.void.void_all_trip){
			data.void_all_trip = 1;
		}else{
			data.voucher_code = [];
			for (code in $scope.DATA.void.voucher){
				if ($scope.DATA.void.voucher[code].is_void){
					data.voucher_code.push(code);
				}
			}
		}
				
		httpSrv.post({
			url 	: "booking/void",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					toastr.success("Saved...");
					$scope.loadDataBookingDetail();
					$("#void-booking-form").modal("hide");
				}else{
					$(event.target).find("button:submit").attr("disabled","disabled");
					$scope.DATA.void.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
				}
				//$.hideLoading();
			}
		});
	}
	//--
	
	//Change Bank Account
	$scope.bookingDetailEditBankAccount = function(){
		var current_bank_account = {"id" : ""};
		if ($scope.DATA.current_booking.booking.bank_account){
			current_bank_account = angular.copy($scope.DATA.current_booking.booking.bank_account);
		}
		$scope.DATA.current_booking.booking.edit_bank_account = current_bank_account;
		
		if (!$scope.DATA.bank_account){
			httpSrv.post({
				url 	: "vendor/getRekeningData",
				success : function(response){
					$scope.DATA.bank_account = response.data.bank;
				},
				error : function (response){}
			});
		}
		
	}
	$scope.bookingDetailUpdateBankAccount = function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.current_booking.booking.edit_bank_account.error_msg = [];
		
		var data = {"booking_code" 	: $scope.DATA.current_booking.booking.booking_code,
					"bank_id" 		: $scope.DATA.current_booking.booking.edit_bank_account.id}
		
		httpSrv.post({
			url 	: "booking/update_bank_account",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					toastr.success("Saved...");
					$scope.loadDataBookingDetail();
					$("#edit_bank_account").modal("hide");
				}else{
					$scope.DATA.current_booking.booking.edit_bank_account.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
				//$.hideLoading();
			}
		});
	}
	//--
	
	//Print
	$scope.printVoucher = function(){
		booking_code = $stateParams.booking_code;
		voucher_code = $stateParams.voucher_code;
		
		httpSrv.post({
			url 	: "setting/terms_and_conditions",
			success : function(response){ 
				$scope.terms_and_conditions = response.data.terms_and_conditions; 
				$scope.voucher_notes = response.data.notes; 
			},
		});
		
		$scope.loadDataBookingDetail(booking_code, function(booking_detail){
			$scope.$parent.$parent.show_print_button = true;
			
			$scope.DATA.voucher = {};
			
			for (i in booking_detail.booking.detail){
				if (voucher_code == booking_detail.booking.detail[i].voucher_code){
					$scope.DATA.voucher = booking_detail.booking.detail[i];
					break;
				}
			}
			
			console.log($scope.DATA.voucher);
			
		});
	}
	$scope.printReceipt = function(){
		booking_code = $stateParams.booking_code;
		$scope.loadDataBookingDetail(booking_code, function(booking){
			$scope.$parent.$parent.show_print_button = true;
			$scope.DATA.myPayment = {};
			$scope.DATA.myPayment.payment_currency 	= booking.booking.currency;
			$scope.DATA.myPayment.payment_amount	= angular.copy(booking.booking.balance);
			$scope.DATA.myPayment.amount			= angular.copy(booking.booking.balance);
			$scope.DATA.myPayment.default_currency 	= booking.booking.currency;
		});
		
		//Load Data Available Currency
		delete $scope.DATA.currency_converter;
		httpSrv.post({
			url 	: "currency_converter/available_currency",
			success : function(response){ 
				if (response.data.status == 'SUCCESS' && response.data.currency && response.data.currency.length > 0){
					$scope.$root.DATA_available_currency 	= response.data; 
					//$scope.DATA.myPayment.payment_currency 	= angular.copy(response.data.default_currency);
					//$scope.DATA.myPayment.payment_amount	= angular.copy($scope.DATA.myPayment.amount);
					//$scope.DATA.myPayment.default_currency 	= angular.copy(response.data.default_currency);
				}
			},
		});
		
	}
	//--
	
	//Cancel Booking
	$scope.cancelBooking = function(booking){
		$scope.DATA.cancelation = {	'cancel_all_trip' 	: 0,
									'voucher'			: {},
									'is_cancel_partial' : 0};
		
		if (!$scope.cancelation_fee){
			$scope.cancelation_fee = {};
		}
		
		for (i in $scope.DATA.current_booking.booking.detail){
			var voucher = {};
			voucher.code = $scope.DATA.current_booking.booking.detail[i].voucher_code;
			voucher.is_cancel = 0;
			voucher.fee = $scope.DATA.current_booking.booking.detail[i].subtotal;
			$scope.DATA.cancelation.voucher[voucher.code] = voucher;
			
			if (!$scope.cancelation_fee[voucher.code]) $scope.cancelation_fee[voucher.code] = {};
			$scope.cancelation_fee[voucher.code].pay = $scope.DATA.current_booking.booking.detail[i].subtotal;
		}

		$scope.DATA.cancelation.cancel_all_trip = 1;
		$scope.chkCancelAllBookingClick();

		if ($scope.DATA.current_booking.booking.total_payment == 0){
			$scope.cancelationFee(booking.booking_code);

			if ($scope.cancelation_fee[voucher.code]) {
				$scope.DATA.cancelation.voucher[voucher.code].fee = $scope.cancelation_fee[voucher.code]['pay'];
			}
		}
	}

	$scope.cancelationFee = function(booking_code){

		$scope.cancelation_fee = {};

		httpSrv.get({
			url 	: "transport/booking/cancelation/"+booking_code,
			success : function(response){
				
				var booking = response.data;
				
				if (booking.status == "SUCCESS"){
					$scope.cancelation_fee = booking.booking.cancelation;
					
				}else{
					alert("Sorry, data not found..");
					window.location = "#/reservation/detail/"+booking_code;
				}
			},
			error : function (response){}
		});



	}

	$scope.chkCancelAllBookingClick = function(check_cancel){
		if (!check_cancel){
			var is_cancel_all = $scope.DATA.cancelation.cancel_all_trip;
	
			for (code in $scope.DATA.cancelation.voucher){
				$scope.DATA.cancelation.voucher[code].is_cancel = is_cancel_all;
			}
		}else{
			var is_cancel_all = 1;
			for (code in $scope.DATA.cancelation.voucher){
				if(!$scope.DATA.cancelation.voucher[code].is_cancel){
					is_cancel_all = 0;
					break;
				}
			}
			$scope.DATA.cancelation.cancel_all_trip = is_cancel_all;
		}
	}
	$scope.saveCancelation = function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.cancelation.error_desc = [];
		
		var data = {};
		
		//Cancel Voucher Full
		if (!$scope.DATA.cancelation.is_cancel_partial){
			data = {};
			data = {"booking_code" 			: $scope.DATA.current_booking.booking.booking_code,
					"cancelation_reason"	: $scope.DATA.cancelation.cancelation_reason}
			
			if ($scope.DATA.cancelation.cancel_all_trip){
				data.cancel_all_trip = 1;
			}
			data.voucher_code = [];
			for (code in $scope.DATA.cancelation.voucher){
				if ($scope.DATA.cancelation.voucher[code].is_cancel){
					if ($scope.cancelation_fee) {
						$scope.DATA.cancelation.voucher[code].fee = $scope.cancelation_fee[code]['pay'];
					}
					data.voucher_code.push({"code":code, "fee":$scope.DATA.cancelation.voucher[code].fee});
				}
			}
		}
		//Partial Cancel Voucher
		else if ($scope.DATA.cancelation.is_cancel_partial){
			data = {};
			data = {"booking_code" 			: $scope.DATA.current_booking.booking.booking_code,
					"cancelation_reason"	: $scope.DATA.cancelation.cancelation_reason}
			data.cancel_all_trip = 0;
			
			var _voucher = {"code"	: $scope.DATA.cancelation.partial_cancel.voucher.voucher_code, 
							"fee"	: ($scope.DATA.cancelation.partial_cancel.voucher.cancelation_fee * $scope.DATA.cancelation.partial_cancel.qty)};
			
			_voucher.passengers = [];

			for (_x in $scope.DATA.cancelation.partial_cancel.voucher.passenger){
				if ($scope.DATA.cancelation.partial_cancel.voucher.passenger[_x].selected){
					_voucher.passengers.push({"passenger_code"	: $scope.DATA.cancelation.partial_cancel.voucher.passenger[_x].passenger_code,
											  "type"			: $scope.DATA.cancelation.partial_cancel.voucher.passenger[_x].type.code});
				}
			}
			
			data.voucher_code = [];
			data.voucher_code.push(_voucher);
		}
		
		//REFUND
		if ($scope.DATA.cancelation.is_add_refund == '1'){
			data.refund_amount 	= $scope.DATA.cancelation.refund_amount;
			data.currency		= $scope.DATA.current_booking.booking.currency;
		}
		//--
		
		//console.log($scope.DATA.cancelation);
		//$scope.DATASS = data;
		//console.log(data);
		//return
				
		httpSrv.post({
			url 	: "booking/cancel",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					toastr.success("Saved...");
					$scope.loadDataBookingDetail();
					$("#cancel-booking-form").modal("hide");
				}else{
					$(event.target).find("button:submit").attr("disabled","disabled");
					$scope.DATA.cancelation.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
				}
				//$.hideLoading();
			}
		});
	}
	//--
	
	//Partial Cancel
	$scope.partialCancelSelectVoucher = function(voucher){
		$scope.DATA.cancelation.is_cancel_partial = 1;
		$scope.DATA.cancelation.partial_cancel = {};
		$scope.DATA.cancelation.partial_cancel.qty = 0;
		$scope.DATA.cancelation.partial_cancel.voucher = angular.copy(voucher);
	}
	$scope.calculateQtySelectedPassangerPartialCancel = function(voucher){
		$scope.DATA.cancelation.partial_cancel.qty = 0;
		for (x in voucher.passenger){
			if (voucher.passenger[x].selected){
				$scope.DATA.cancelation.partial_cancel.qty += 1;
			}
		}
	}
	//--
	
	
	//Refund
	$scope.refundBooking = function(booking){
		$scope.DATA.myRefund = {	'refund_amount' : 0,
									'description'	: ''};
	}
	$scope.saveRefund= function(event){
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myRefund.error_desc = [];
		
		var data = {"booking_code" 			: $scope.DATA.current_booking.booking.booking_code,
					"currency" 				: $scope.DATA.current_booking.booking.currency,
					"refund_amount"			: $scope.DATA.myRefund.refund_amount,
					"description"			: $scope.DATA.myRefund.description}
		
		httpSrv.post({
			url 	: "refund/create",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					toastr.success("Saved...");
					$scope.loadDataBookingDetail();
					$("#refund-booking-form").modal("hide");
				}else{
					$(event.target).find("button:submit").attr("disabled","disabled");
					$scope.DATA.myRefund.error_desc = response.data.error_desc;
					$(event.target).find("button:submit").removeAttr("disabled");
				}
				//$.hideLoading();
			}
		});
	}
	//--
	
	//Confirm Tentative Booking
	$scope.ConfirmTentativeBooking = function(booking, after_update_load_function){
		if (confirm("Are you sure to confirm this booking (#"+booking.booking_code+") as Definite?")){

			booking.saving_ConfirmTentativeBooking = true;
			
			var data = {"booking_code" : booking.booking_code};			
			httpSrv.post({
				url 	: "booking/confirm_tentative_booking",
				data	: $.param(data),
				success : function(response){
					if (response.data.status == "SUCCESS"){
						toastr.success("Saved...");
						if (after_update_load_function == "loadDataBookingDetail"){
							$scope.loadDataBookingDetail();
						}else if (after_update_load_function == "loadDataBooking_2"){
							$scope.loadDataBooking_2();
						}
					}else{
						booking.saving_ConfirmTentativeBooking = false;
					}
				}
			});
		
		}
	}
	//--

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	$scope.loadDataBookingPaymentPrint = function(){
		booking_code = $stateParams.booking_code;
		
		
		$scope.loadDataBookingTransportDetail(booking_code, function(booking_detail){
			$scope.DATA.current_booking = booking_detail;
		});
		
		$scope.booking_code = booking_code;

		httpSrv.get({
			url 	: "payment/reservation/"+booking_code,
			success : function(response){
				$scope.DATA.payment = response.data;
				
				//console.log($scope.DATA.payment);
			},
			error : function (response){}
		});

	}

	$scope.loadDataTransactionTransport = function(agent_code){
			
		$scope.search = {   "start_date" : "",
							"end_date" : "",
							"agent_code": agent_code,
							"payment_method": "DEPOSIT"}; 		
		
		var _search = $scope.search;
		
		
		httpSrv.post({
			url 	: "transport/transaction",
			data	: $.param({"search":_search}),
			success : function(response){
				
				$scope.DATA.transaction = response.data.transactions;

				// $scope.used_diposit($scope.DATA.transaction);
			
			},
			error : function (response){}
		});

	}

	// $scope.saveDataPayment = function(event){
	// 	//$.showLoading();
	// 	$(event.target).find("button:submit").attr("disabled","disabled");
	// 	$scope.DATA.myPayment.error_msg = [];
		
	// 	httpSrv.post({
	// 		url 	: "payment/create/",
	// 		data	: $.param($scope.DATA.myPayment),
	// 		success : function(response){
	// 			console.log(response.data);
	// 			if (response.data.status == "SUCCESS"){
	// 				$scope.loadDataBookingPayment();
	// 				$("#add-edit-payment").modal("hide");
	// 				toastr.success("Saved...");
	// 			}else{
	// 				$scope.DATA.myPayment.error_msg = response.data.error_desc;
	// 			}
	// 			$(event.target).find("button:submit").removeAttr("disabled");
	// 			//$.hideLoading();
	// 		}
	// 	});
	// }


	// Gung ari Update

	$scope.send_email_profoma = function(id, is_agent){

		
		httpSrv.post({
			url 	: "invoice/send_email_profoma",
			data 	: $.param({"booking_code": id, "is_agent": is_agent}),
			success : function(response){
				if (response.data.status == "SUCCESS") {
					toastr.success("Email Send...");
				}else{
					toastr.warning("Email Send Failed...");
				}
			},
			error : function (response){}
		});

	}

	$scope.send_email_billing = function(id, is_agent){

		httpSrv.post({
			url 	: "invoice/send_email_billing",
			data 	: $.param({"booking_code": id, "is_agent": is_agent}),
			success : function(response){
				if (response.data.status == "SUCCESS") {
					toastr.success("Email Send...");
				}else{
					toastr.warning("Email Send Failed...");
				}

					
			},
			error : function (response){}
		});

	}

	$scope.loadDataTermsAndConditions = function(){

		$scope.is_cancellation = false;
		$scope.item = 0;

		httpSrv.get({
			url 	: "setting/terms_and_conditions",
			success : function(response){
				
				//$scope.rules = JSON.parse(response.data.advanced_setting.cancelation_rule_crs);
				$scope.convert_int($scope.rules);
			},
			error : function (response){}
		});
	}
	$scope.convert_int = function(data){
		
		var array = [];

		angular.forEach(data, function(row){
			array.push({f_min_activation_days: parseInt(row.f_min_activation_days), f_percentage: parseInt(row.f_percentage)});
		});

		$scope.rules = array;

	}

	$scope.send_email_receipt = function(id, is_agent, payment_code){


		
		httpSrv.post({
			url 	: "invoice/send_email_receipt",
			data 	: $.param({"booking_code": id, "is_agent": is_agent, 'payment_code': payment_code}),
			success : function(response){
				if (response.data.status == "SUCCESS") {
					toastr.success("Email Send...");
				}else{
					toastr.warning("Email Send Failed...");
				}
			},
			error : function (response){}
		});

	}
	
	
	$scope.loadDataBookingTransport = function(){
		$scope.show_loading_DATA_bookings = true;
		httpSrv.post({
			url 	: "transport/booking",
			//data	: $.param({"publish_status":"all"}),
			success : function(response){
				$scope.show_loading_DATA_bookings = false;
				$scope.DATA.bookings = response.data;
			},
			error : function (response){}
		});
	}
	
	
	$scope.loadDataTransactionTransportXXXX = function(page){
		GeneralJS.activateLeftMenu("transaction");
		$scope.show_loading_DATA_bookings = true;
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		
		if (!$scope.search){ 
			var now = new Date(),
				start_date = new Date(),
				end_date = new Date();
			
			start_date.setDate(start_date.getDate() - 30);
			
			$scope.search = { "start_date" : $scope.fn.formatDate(start_date, "yy-mm-dd"),
							  "end_date" : $scope.fn.formatDate(end_date, "yy-mm-dd"),}; 
		}
		
		if (!page) page = 1;
		
		var _search = $scope.search;
			_search.page = page;
		
		httpSrv.post({
			url 	: "transport/transaction",
			data	: $.param({"search":_search}),
			success : function(response){
				$scope.show_loading_DATA_bookings = false;
				$scope.DATA.transaction = response.data;
				
				if ($scope.DATA.transaction.search){
					$scope.DATA.transaction.search.pagination = [];
					for (i=1;i<=$scope.DATA.transaction.search.number_of_pages; i++){
						$scope.DATA.transaction.search.pagination.push(i);
					}
				}
			},
			error : function (response){}
		});
		
		//Load Data Booking Source
		if (!$scope.DATA.booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.DATA.booking_source= response.data; },
			});
		}
		
		//Load Data Booking Status
		if (!$scope.DATA.booking_status){
			httpSrv.post({
				url 	: "setting/booking_status",
				success : function(response){ $scope.DATA.booking_status = response.data; },
			});
		}
	}
	
	
	$scope.editDataBookingTransportDetail = function(){
		$scope.loadDataBookingTransportDetail();
		
		//Load Data Available Port
		httpSrv.post({
			url 	: "transport/port/available_port",
			success : function(response){ 
				$scope.DATA.available_port = response.data;
			},
		});
		
	}
	$scope.edit_trip = function(trip){
		
		$scope.current_edit_trip = trip;
		
		$scope.check_availabilities_data = {};
		$scope.add_trip = {};
		$scope.add_trip.departure_date = trip.date;
		$scope.add_trip.departure_port_id = trip.departure.port.id;
		$scope.add_trip.arrival_port_id = trip.arrival.port.id;
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		$("#modal-add-trip .btn-submit-edit-trip").removeAttr("disabled");
		$scope.check_availabilities_show_OK_button = false;
		
		//Load Data Available Port
		if (!$scope.$root.DATA_available_port){
			httpSrv.post({
				url 	: "transport/port/available_port",
				success : function(response){ $scope.$root.DATA_available_port = response.data.ports; },
			});
		}
		
	}
	$scope.check_availabilities_edit_trip = function(event){
		//$(event.target).find("button:submit").attr("disabled","disabled");
		
		var data_edit_trip = angular.copy($scope.add_trip);

		var data = {};
		data.departure_date = data_edit_trip.departure_date;
		data.trip_model == "ONEWAY";
		
		if (data_edit_trip.change_trip){
			data.departure_port_id	= data_edit_trip.departure_port_id;
			data.arrival_port_id	= data_edit_trip.arrival_port_id;
		}else{
			data.departure_port_id	= $scope.current_edit_trip.departure.port.id;
			data.arrival_port_id	= $scope.current_edit_trip.arrival.port.id;
		}
		data.adult = $scope.current_edit_trip.qty_1;
		data.child = $scope.current_edit_trip.qty_2;
		data.infant = $scope.current_edit_trip.qty_3;
		
		$scope.add_trip.error_desc = [];
		$scope.check_availabilities_data = {};
		$scope.check_availabilities_show_loading = true;

		httpSrv.post({
			url 	: "transport/schedule/check_availabilities",
			data	: $.param(data),
			success : function(response){
				$(event.target).find("button:submit").removeAttr("disabled");
				$scope.check_availabilities_show_loading = false;
				$scope.check_availabilities_show_searching_form = false;
				
				if (response.data.status == "SUCCESS"){
					$scope.check_availabilities_data = response.data;
				}else{
					$scope.add_trip.error_desc = response.data.error_desc;
					//$('html, body').animate({scrollTop:200}, 400);
				}
				
				//fn.pre($scope.check_availabilities_data, "check_availabilities_data");
				//$.hideLoading();
			}
		});
		
	}
	$scope.update_voucher_trip = function (){
		
		$scope.update_voucher_trip_error_desc = {};
		$scope.update_voucher_trip_error_desc.error_desc = [];
		$("#modal-add-trip .btn-submit-edit-trip").attr("disabled","disabled");
		
		var data_submit = {	"voucher_code"		: $scope.current_edit_trip.voucher_code,
							"date" 				: $scope.check_availabilities_data.departure.check.date,
							"rates_code"		: $scope.check_availabilities_data.departure.selected_trip.rates_code,
							"schedule_code"		: $scope.check_availabilities_data.departure.selected_trip.schedule.schedule_code,
							"applicable_rates"	: $scope.check_availabilities_data.departure.selected_trip.aplicable_rates,
							};
		
		httpSrv.post({
			url 	: "transport/booking/update_voucher",
			data	: $.param(data_submit),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.editDataBookingTransportDetail();
					$("#modal-add-trip").modal("hide");
					toastr.success("Updated...");
				}else{
					$scope.update_voucher_trip_error_desc.error_desc = response.data.error_desc;
					$("#modal-add-trip .btn-submit-edit-trip").removeAttr("disabled");
				}
				//$.hideLoading();
			}
		});
		
		//$scope.DATA_r = data_submit;
		//console.log(data_submit);
	}
	
	//Split Voucher
	$scope.split_voucher = function(trip){

		$scope.current_edit_trip = trip;
		
		$scope.check_availabilities_data = {};
		$scope.add_trip = {};
		$scope.add_trip.departure_date = trip.date;
		$scope.add_trip.departure_port_id = trip.departure.port.id;
		$scope.add_trip.arrival_port_id = trip.arrival.port.id;
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		$("#modal-split-voucher .btn-submit-edit-trip").removeAttr("disabled");
		$scope.check_availabilities_show_OK_button = false;
		
		//Load Data Available Port
		if (!$scope.$root.DATA_available_port){
			httpSrv.post({
				url 	: "transport/port/available_port",
				success : function(response){ $scope.$root.DATA_available_port = response.data.ports; },
			});
		}
	}
	$scope.check_availabilities_split_voucher = function(event){
		//$(event.target).find("button:submit").attr("disabled","disabled");
		
		var _qty = 0;
		for (_x in $scope.current_edit_trip.passenger){
			if ($scope.current_edit_trip.passenger[_x].selected){_qty += 1;}
		}
		
		if (_qty == 0){

			alert("Please select at least one passenger.");
			
		}else if(_qty == ($scope.current_edit_trip.qty_1 + $scope.current_edit_trip.qty_2 + $scope.current_edit_trip.qty_3)){
			
			alert("If you select all passenger, please use Edit Trips.");
			
		}else{
			
			var data_edit_trip = angular.copy($scope.add_trip);
	
			var data = {};
			data.departure_date = data_edit_trip.departure_date;
			data.trip_model == "ONEWAY";
			
			if (data_edit_trip.change_trip){
				data.departure_port_id	= data_edit_trip.departure_port_id;
				data.arrival_port_id	= data_edit_trip.arrival_port_id;
			}else{
				data.departure_port_id	= $scope.current_edit_trip.departure.port.id;
				data.arrival_port_id	= $scope.current_edit_trip.arrival.port.id;
			}
			
			data.adult	= _qty; //$scope.current_edit_trip.qty_1;
			data.child	= $scope.current_edit_trip.qty_2;
			data.infant = $scope.current_edit_trip.qty_3;
			
			$scope.add_trip.error_desc = [];
			$scope.check_availabilities_data = {};
			$scope.check_availabilities_show_loading = true;
	
			httpSrv.post({
				url 	: "transport/schedule/check_availabilities",
				data	: $.param(data),
				success : function(response){
					$(event.target).find("button:submit").removeAttr("disabled");
					$scope.check_availabilities_show_loading = false;
					$scope.check_availabilities_show_searching_form = false;
					
					if (response.data.status == "SUCCESS"){
						$scope.check_availabilities_data = response.data;
					}else{
						$scope.add_trip.error_desc = response.data.error_desc;
						//$('html, body').animate({scrollTop:200}, 400);
					}
					
					//fn.pre($scope.check_availabilities_data, "check_availabilities_data");
					//$.hideLoading();
				}
			});
		}
	}
	$scope.update_split_voucher = function (){
		
		$scope.update_voucher_trip_error_desc = {};
		$scope.update_voucher_trip_error_desc.error_desc = [];
		$("#modal-split-voucher .btn-submit-edit-trip").attr("disabled","disabled");
		
		var data_submit = {	"voucher_code"		: $scope.current_edit_trip.voucher_code,
							"date" 				: $scope.check_availabilities_data.departure.check.date,
							"rates_code"		: $scope.check_availabilities_data.departure.selected_trip.rates_code,
							"schedule_code"		: $scope.check_availabilities_data.departure.selected_trip.schedule.schedule_code,
							"applicable_rates"	: $scope.check_availabilities_data.departure.selected_trip.aplicable_rates,
							};
		
		data_submit.passengers = [];
		for (_x in $scope.current_edit_trip.passenger){
			if ($scope.current_edit_trip.passenger[_x].selected){
				data_submit.passengers.push({"passenger_code":$scope.current_edit_trip.passenger[_x].passenger_code});
			}
		}

		httpSrv.post({
			url 	: "transport/booking/update_split_voucher",
			data	: $.param(data_submit),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.editDataBookingTransportDetail();
					$("#modal-split-voucher").modal("hide");
					toastr.success("Updated...");
				}else{
					$scope.update_voucher_trip_error_desc.error_desc = response.data.error_desc;
					$("#modal-split-voucher .btn-submit-edit-trip").removeAttr("disabled");
				}
				//$.hideLoading();
			}
		});
		
		//$scope.DATA_r = data_submit;
		//console.log(data_submit);
	}
	//--
	
	$scope.aplicable_rates_for_agent = function(){
		$scope.agent_allow_to_use_acl = false;
		$scope.agent_allow_to_use_deposit = false;
		$scope.DATA.data_rsv.add_payment = '0';
		$scope.agent_payment_use_acl = '0';
		$scope.DATA.data_rsv.payment.payment_type = '';
		
		var selected_trips = $scope.DATA.data_rsv.selected_trips;
			
		if ($scope.DATA.data_rsv.booking_source == "AGENT" && $scope.DATA.data_rsv.agent){
			
			var agent = $scope.DATA.data_rsv.agent;
			
			//Check Agent Credit Limit
			if (agent.payment_method.payment_code == "ACL"){
				$scope.agent_allow_to_use_acl = true;
				
				$scope.DATA.data_rsv.add_payment = '0';
				$scope.agent_payment_use_acl = '1';
				
				if (!agent.out_standing_invoice){
					agent.out_standing_invoice = {};
					httpSrv.post({
						url 	: "agent/out_standing_invoice/",
						data	: $.param({"agent_code":agent.agent_code}),
						success : function(response){
							if (response.data.status == "SUCCESS"){
								agent.out_standing_invoice = response.data.out_standing_invoice;
							}
						},
						error : function (response){}
					});
				}
			}
			//--
			$scope.deposit = [];

			if (agent.payment_method.payment_code == "DEPOSIT"){
				$scope.agent_allow_to_use_deposit = true;
				$scope.agent_allow_to_use_acl = false;

				$scope.DATA.data_rsv.add_payment = '0';
				httpSrv.post({
					url 	: "agent/deposit/",
					data	: $.param({"publish_status":"ALL", "agent_code":agent.agent_code}),
					success : function(response){
						
						$scope.deposit = response.data.deposit;
					},
					error : function (response){}
				});
			}
			
			for (i in selected_trips){
				var _rates = selected_trips[i].trips.aplicable_rates_original;
				//selected_trips[i].trips.aplicable_rates.rates_1 = _rates.rates_1 - (_rates.rates_1 * agent.category.percentage/100);
				//selected_trips[i].trips.aplicable_rates.rates_2 = _rates.rates_2 - (_rates.rates_2 * agent.category.percentage/100);
				//selected_trips[i].trips.aplicable_rates.rates_3 = _rates.rates_3 - (_rates.rates_3 * agent.category.percentage/100);
				
				$scope.calculate_subtotal(selected_trips[i]);
			}
			
		}else{
			
			for (i in selected_trips){
				var _rates = selected_trips[i].trips.aplicable_rates_original;
				selected_trips[i].trips.aplicable_rates = angular.copy(_rates);
				
				$scope.calculate_subtotal(selected_trips[i]);
			}
			
		}
		$scope.calculate_total();
	}
	
	
	
	// $scope.submit_booking = function(event){
		
	// 	$(event.target).find("button:submit").attr("disabled","disabled");
	// 	$scope.DATA.data_rsv.error_desc = [];
		
	// 	var data_rsv = {};
	// 	var ss_data_rsv = angular.copy($scope.DATA.data_rsv);
		
	// 	data_rsv.booking_source = ss_data_rsv.booking_source;
	// 	if (data_rsv.booking_source == "AGENT"){
	// 		data_rsv.agent_code = ss_data_rsv.agent.agent_code;
	// 		if (ss_data_rsv.voucher_reff_number && ss_data_rsv.voucher_reff_number != ''){
	// 			data_rsv.voucher_reff_number = ss_data_rsv.voucher_reff_number;
	// 		}
	// 	}else if (data_rsv.booking_source == "OFFLINE"){
	// 		if (ss_data_rsv.booking_source_sub){
	// 			data_rsv.booking_source_sub = ss_data_rsv.booking_source_sub;
	// 		}
	// 	}
		
	// 	data_rsv.booking_status = ss_data_rsv.booking_status;
	// 	if (ss_data_rsv.booking_status == 'UNDEFINITE'){
	// 		data_rsv.undefinite_valid_until_hour = ss_data_rsv.undefinite_valid_until_hour;
	// 	}
		
	// 	data_rsv.discount 		= {"type":ss_data_rsv.discount_type, "amount":ss_data_rsv.discount_amount};
	// 	data_rsv.tax_service	= {"type":ss_data_rsv.tax_service_type, "amount":ss_data_rsv.tax_service_amount};
	// 	data_rsv.remarks 		= ss_data_rsv.remarks;
	// 	data_rsv.customer 		= ss_data_rsv.customer;
	// 	data_rsv.trips 			= [];
		
	// 	for (i in ss_data_rsv.selected_trips){
	// 		var trip = {};
			
	// 		trip.date 	= ss_data_rsv.selected_trips[i].date;
	// 		trip.adult 	= ss_data_rsv.selected_trips[i].adult;
	// 		trip.child 	= ss_data_rsv.selected_trips[i].child;
	// 		trip.infant	= ss_data_rsv.selected_trips[i].infant;
			
	// 		trip.schedule_code	= ss_data_rsv.selected_trips[i].trips.schedule.schedule_code;
	// 		trip.rates_code		= ss_data_rsv.selected_trips[i].trips.rates_code;
	// 		trip.applicable_rates = ss_data_rsv.selected_trips[i].trips.aplicable_rates;

	// 		if (ss_data_rsv.selected_trips[i].trips.smart_pricing){
	// 			//console.log(ss_data_rsv.selected_trips[i].trips.smart_pricing);
				
	// 			if (ss_data_rsv.selected_trips[i].trips.smart_pricing && ss_data_rsv.selected_trips[i].trips.smart_pricing.type == "SMARTPRICING"){
	// 				trip.smart_pricing_level = ss_data_rsv.selected_trips[i].trips.smart_pricing.level;
	// 			}
				
	// 			//if (ss_data_rsv.selected_trips[i].trips.smart_pricing.level || ss_data_rsv.selected_trips[i].trips.smart_pricing.level === 0){
	// 				//alert(ss_data_rsv.selected_trips[i].trips.smart_pricing.level);
	// 			//	trip.smart_pricing_level = ss_data_rsv.selected_trips[i].trips.smart_pricing.level;
	// 			//}
	// 		}
			
	// 		if (ss_data_rsv.selected_trips[i].trips.is_return && ss_data_rsv.selected_trips[i].trips.is_return == "yes"){
	// 			trip.is_return	= "yes";
	// 		}
			
	// 		//Check Pickup Service
	// 		if (ss_data_rsv.selected_trips[i].pickup 
	// 			&& ss_data_rsv.selected_trips[i].pickup.area 
	// 			&& !ss_data_rsv.selected_trips[i].pickup.area.nopickup){
				
	// 			trip.pickup = ss_data_rsv.selected_trips[i].pickup;
	// 			trip.pickup.area_id = trip.pickup.area.id;
	// 			trip.pickup.price 	= trip.pickup.area.price;
	// 			delete trip.pickup.area;
				
	// 		}
	// 		//Check Dropoff Service
	// 		if (ss_data_rsv.selected_trips[i].dropoff 
	// 			&& ss_data_rsv.selected_trips[i].dropoff.area 
	// 			&& !ss_data_rsv.selected_trips[i].dropoff.area.nopickup){
				
	// 			trip.dropoff = ss_data_rsv.selected_trips[i].dropoff;
	// 			trip.dropoff.area_id = trip.dropoff.area.id;
	// 			trip.dropoff.price 	 = trip.dropoff.area.price;
	// 			delete trip.dropoff.area;
				
	// 		}
			
	// 		//Check Additional Service
	// 		if (ss_data_rsv.selected_trips[i].additional_service){
				
	// 			trip.additional_service = ss_data_rsv.selected_trips[i].additional_service;
				
	// 		}
			
	// 		data_rsv.trips.push(trip);
			
	// 	}//End For
		
	// 	if ($scope.DATA.data_rsv.add_payment == '1' && $scope.DATA.data_rsv.booking_status != 'COMPLIMEN'){
	// 		var payment = $scope.DATA.data_rsv.payment;
	// 		data_rsv.payment = {"amount" 		: payment.payment_amount,
	// 							"payment_type"	: payment.payment_type,
	// 							"account_number": payment.account_number,
	// 							"name_on_card"	: payment.name_on_card,
	// 							"bank_name"		: payment.bank_name,
	// 							"payment_reff_number" : payment.payment_reff_number,
	// 							"description"	: payment.description};
	// 		if (payment.payment_type == "OPENVOUCHER"){
	// 			data_rsv.payment.openvoucher_code = payment.openvoucher_code
	// 		}
	// 	}
		
	// 	//$scope.DATA_RSV = data_rsv;
	// 	//console.log(data_rsv);
	// 	//return;
		
	// 	httpSrv.post({
	// 		url 	: "transport/booking/create",
	// 		data	: $.param(data_rsv),
	// 		success : function(response){
	// 			if (response.data.status == "SUCCESS"){
	// 				window.location = "#/trans_reservation/detail/"+response.data.booking_code+"/passenger";
	// 				toastr.success("Saved...");
	// 			}else{
	// 				$(event.target).find("button:submit").attr("disabled","disabled");
	// 				$scope.DATA.data_rsv.error_desc = response.data.error_desc;
	// 				$(event.target).find("button:submit").removeAttr("disabled");
	// 				$('html, body').animate({scrollTop:200}, 400);
	// 			}
	// 			//$.hideLoading();
	// 		}
	// 	});
		
	// 	$scope.DATA_RSV = data_rsv;
		
	// 	//fn.pre(ss_data_rsv,"ss_data_rsv");
	// 	//fn.pre(data_rsv,"data_rsv");
	// }
	
		

	$scope.printReceiptTransPayment = function(){
		
		payment_code = $stateParams.payment_code;

		$scope.loadDataBookingPaymentDetail(payment_code, function(){
			$scope.$parent.$parent.show_print_button = true;
		});
	}

	$scope.loadDataBookingPaymentDetail = function(payment_code, after_load_handler_function){

		if (!payment_code){ payment_code = $stateParams.payment_code; }
		
		httpSrv.get({
			url 	: "transport/booking/detail_payment/"+payment_code,
			success : function(response){
				
				var booking = response.data;
				//console.log(booking);
				if (booking.status == "SUCCESS"){
					$scope.DATA.current_booking = booking;
					
					if (fn.isFn(after_load_handler_function)) {
						after_load_handler_function(booking);
					}
				}else{
					alert("Sorry, data not found..");
					window.location = "#/reservation";
				}
			},
			error : function (response){}
		});
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	$scope.changePaymentTypeInNewBookingForm = function(){
		//console.log($scope.DATA);
		
		$scope.DATA.payment_is_cc = false;
		$scope.DATA.payment_is_atm = false;
		//console.log($scope.DATA.data_rsv.payment.payment_type);
		
		if ($scope.DATA.data_rsv.payment.payment_type == 'OPENVOUCHER'){
			$scope.DATA.data_rsv.payment.openvoucher_code = [];
			var total_person = 0;
			
			for (i in $scope.DATA.data_rsv.selected_trips){
				var detail = $scope.DATA.data_rsv.selected_trips[i];

				total_person += detail.adult;
				total_person += detail.child;
				total_person += detail.infant;
				
			}
			
			for (j=0; j<total_person; j++){
				$scope.DATA.data_rsv.payment.openvoucher_code.push({"code":""});
			}
			
			//console.log($scope.DATA.data_rsv.payment.openvoucher_code);
		}else{
			//console.log($scope.$root.DATA_payment_method);
			
			for (index in $scope.$root.DATA_payment_method){
				var payment_method = $scope.$root.DATA_payment_method[index];
				if (payment_method.code == $scope.DATA.data_rsv.payment.payment_type){
					if (payment_method.type == 'CC'){
						$scope.DATA.payment_is_cc = true;
						$scope.DATA.payment_is_atm = false;
					}else if (payment_method.type == 'TRANSFER'){
						$scope.DATA.payment_is_cc = false;
						$scope.DATA.payment_is_atm = true;
					}else{
						$scope.DATA.payment_is_cc = false;
						$scope.DATA.payment_is_atm = false;
					}
					break;
				}
			}
			
			//$scope.$root.DATA_payment_method.forEach(function(payment_method, index){
			//	if (payment_method.code == $scope.DATA.data_rsv.payment.payment_type){
					//console.log(item);
					//break;
			//	}
			//});
		}
	}
	
		
	$scope.checkInVoucherTrans = function(){
		GeneralJS.activateLeftMenu("checkin");	
	}
	$scope.getVoucherInformation = function(){
		var voucher_code = $scope.check_voucher_code;
		$scope.DATA.voucher = {};
		$scope.show_getVoucherInformation = true;
		
		httpSrv.post({
			url 	: "transport/booking/voucher/",
			data	: $.param({"voucher_code":voucher_code}),
			success : function(response){
				var voucher = response.data;

				if (voucher.status == "SUCCESS"){
					$scope.DATA.voucher = voucher;
					if ($scope.DATA.voucher.voucher.booking.balance > 0){
						$scope.DATA.voucher.allow_to_checkin = false;
						if($scope.DATA.voucher.voucher.booking.agent){
							if ($scope.DATA.voucher.voucher.booking.agent.payment_type_code="ACL"){
								$scope.DATA.voucher.allow_to_checkin = true;
							}
						}
					}else if ($scope.DATA.voucher.voucher.booking.status_code == 'CANCEL' || $scope.DATA.voucher.voucher.voucher.booking_detail_status_code == 'CANCEL'){
						$scope.DATA.voucher.allow_to_checkin = false;
						$scope.DATA.voucher.is_canceled = true;
					}else{
						$scope.DATA.voucher.allow_to_checkin = true;
						
						for (i in $scope.DATA.voucher.voucher.voucher.passenger){
							if ($scope.DATA.voucher.voucher.voucher.passenger[i].checkin_status == '1'){
								$scope.DATA.voucher.voucher.voucher.passenger[i].already_checkin = '1';
							}
						}
						
					}
					//console.log($scope.DATA.voucher.voucher.booking.balance);
					$scope.is_checkedin_proccessed = false;
				}else{
					alert("Sorry, voucher not found..");
					$("#check_voucher_code").focus();
					$("#check_voucher_code").val("");
				}
				$scope.show_getVoucherInformation = false;
			},
			error : function (response){}
		});
	}
	$scope.getClickVoucherInformation = function(voucher_code){
		$scope.check_voucher_code = voucher_code;
		$("#checkin-popup-info").modal("hide");
		$scope.getVoucherInformation();
	}
	$scope.selectToCheckIn = function(passenger){
		if (!passenger.already_checkin){
			if (passenger.checkin_status=='1'){
				passenger.checkin_status = '0';
			}else{
				passenger.checkin_status = '1';
			}
		}
	}
	$scope.checkUncheckParticipant = function(status, participant){
		for (i in participant){
			if (!participant[i].already_checkin){
				if (status == 1){
					participant[i].checkin_status = 1;
				}else{
					participant[i].checkin_status = 0;
				}
			}
		}
	}
	$scope.checkInParticipantNow = function(){
		var participants =  $scope.DATA.voucher.voucher.voucher.passenger;
		var data = {"voucher_code" : $scope.DATA.voucher.voucher.voucher.voucher_code,
					"participant"  : []};
		
		for (i in participants){
			data.participant.push({	"participant_code"	:participants[i].passenger_code,
									"pass_code"			:participants[i].pass_code,
									"checkin_status"	:participants[i].checkin_status});
		}
		$scope.is_checkedin_proccessed = true;
		httpSrv.post({
			url 	: "transport/booking/checkin_participant",
			data	: $.param(data),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					alert ("Chekin Success...");
					$('html, body').animate({scrollTop:0}, 400);
					$scope.DATA.voucher = {};
					$scope.check_voucher_code = "";
					$("#check_voucher_code").focus();
				}
			},
			error : function (response){}
		});
		
	}
	
	$scope.arrivalDepartureTrans = function(){
		GeneralJS.activateLeftMenu("arrival_departure");	
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		
		$scope.dept = {	"type" : "departure",
						"date" : fn.formatDate(new Date(), "yy-mm-dd")};
		
		httpSrv.post({
			url 	: "transport/boats",
			success : function(response){
				$scope.DATA.boats = response.data;
				if ($scope.DATA.boats && $scope.DATA.boats.boats[0]){
					$scope.dept.boat = $scope.DATA.boats.boats[0];
				}
			}
		});
		httpSrv.post({
			url 	: "transport/port/available_port",
			success : function(response){
				$scope.DATA.ports = response.data;
				
				$scope.DATA.ports.ports.push({"id":"ALL","name":"All Port","port_code":"ALL"});
				$scope.selectArrivalDepartureType();
				
				//Generate Data Ports By Code
				var ports_by_code = {};
				for (i in $scope.DATA.ports.ports){
					var port = $scope.DATA.ports.ports[i];
					ports_by_code[port.port_code] = port;
				}
				$scope.DATA.ports_by_code = ports_by_code;
				//console.log($scope.DATA.ports_by_code);
			}
		});
		
	}
	$scope.arrivalDepartureTime = function(){
		
		if ($scope.dept && $scope.dept.date && $scope.dept.boat && $scope.dept.departure && $scope.dept.destination){
			$scope.DATA.time = [];
			
			var data = {"type" 				: $scope.dept.type,
						"date" 				: $scope.dept.date,
						"boat_id" 			: $scope.dept.boat.id,
						"departure_port_id" : $scope.dept.departure.id,
						"arrival_port_id" 	: $scope.dept.destination.id};
			
			httpSrv.post({
				url 	: "transport/schedule/arrival_departure_time",
				data	: $.param(data),
				success : function(response){
					if (response.data.status == "SUCCESS"){
						$scope.DATA.time = response.data.arrival_departure_time;
						//if ($scope.DATA.time[0]){
						//	$scope.dept.time = $scope.DATA.time[0];
						//}
					}
				}
			});
		}
	}
	$scope.arrivalDepartureTransSearch = function(data, after_load_handler_function){
		$scope.show_arrivalDepartureTransSearch_loading = true;
		
		if (!data){
			var data = {"type" 				: $scope.dept.type,
						"date" 				: $scope.dept.date,
						"departure_port_id" : $scope.dept.departure.id,
						"arrival_port_id" 	: $scope.dept.destination.id,
						"data.boat_id"		: $scope.dept.boat.id};
			
			if ($scope.dept.time){
				if ($scope.dept.time.schedule_code){ data.schedule_code = $scope.dept.time.schedule_code; }
				if ($scope.dept.time.time){ data.time = $scope.dept.time.time; }
			}else{
				data.boat_id = $scope.dept.boat.id;
				data.time = "ALL";
			}
	
			//data = {"type":"departure","date":"2017-09-11","departure_port_id":"10","arrival_port_id":"3"};
			//$scope.DATA_R = data;
		}
		
		httpSrv.post({
				url 	: "transport/passenger_list",
				data	: $.param(data),
				success : function(response){
					$scope.show_arrivalDepartureTransSearch_loading = false;
					$scope.search = response.data.search;
					
					$scope.summary = {"total_all":0,"total_checked_in":0};
					
					if (response.data.status == "SUCCESS"){
						$scope.arrival_departure = response.data.passenger_list;
						$scope.summary = response.data.summary;
						
						if (fn.isFn(after_load_handler_function)) {
							after_load_handler_function(response.data);
						}
						
					}else{
						$scope.arrival_departure = [];
					}
				}
			});
		
		
	}
	$scope.arrivalDepartureTransPrint = function(){
		
		var data = {"type" 				: $stateParams.search_type,
					"date" 				: $stateParams.search_date,
					"departure_port_id" : $stateParams.search_departure_port_id,
					"arrival_port_id" 	: $stateParams.search_destination_port_id,
					"boat_id"		: $stateParams.search_schedule_code};
		
		/*httpSrv.post({
			url 	: "transport/port/available_port",
			success : function(response){
				$scope.DATA.ports = response.data;
				
				//Generate Data Ports By Code
				var ports_by_code = {};
				for (i in $scope.DATA.ports.ports){
					var port = $scope.DATA.ports.ports[i];
					ports_by_code[port.port_code] = port;
				}
				$scope.DATA.ports_by_code = ports_by_code;
			}
		});*/
		
		$scope.arrivalDepartureTransSearch(data, function(arrival_departure){$scope.$parent.$parent.show_print_button = true;});
	}
	$scope.selectArrivalDepartureType = function(){
		
		$scope.DATA.ports_arrival	= angular.copy($scope.DATA.ports);
		$scope.DATA.ports_departure	= angular.copy($scope.DATA.ports);
		
		if ($scope.dept.type == 'arrival'){
			$scope.DATA.ports_departure.ports.splice($scope.DATA.ports_departure.ports.length-1, 1);
		}else if ($scope.dept.type == 'departure'){
			$scope.DATA.ports_arrival.ports.splice($scope.DATA.ports_arrival.ports.length-1, 1);
		}
	}
	
	$scope.openPopUpWindowVoucher = function (base_url, booking_code, voucher_code){
		var myWindow = window.open(base_url+booking_code+'/'+voucher_code, 'VoucherWindow', 'width=800,height=600');
	}
	
	$scope.pickupDropoffTrans = function(){
		GeneralJS.activateLeftMenu("arrival_departure");	
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		
		$scope.dept = {	"type" : "pickup",
						"date" : fn.formatDate(new Date(), "yy-mm-dd")};
		$scope.pickupDropofTransSearch();
	}
	$scope.pickupDropofTransSearch = function(data, after_load_handler_function){
		$scope.show_arrivalDepartureTransSearch_loading = true;
		
		if (!data){
			var data = {"type" 				: $scope.dept.type,
						"date" 				: $scope.dept.date};
	
			//data = {"type":"departure","date":"2017-06-30","schedule_code":"TDQ0507","departure_port_id":"10","arrival_port_id":"3","time":"09:30"};
			//$scope.DATA_R = data;
		}
		httpSrv.post({
				url 	: "transport/booking/pickup_dropoff_information",
				data	: $.param(data),
				success : function(response){
					$scope.show_arrivalDepartureTransSearch_loading = false;
					$scope.search = response.data.search;
					
					if (response.data.status == "SUCCESS"){
						$scope.pickup_dropoff = response.data.pickup_dropoff;
						
						if (fn.isFn(after_load_handler_function)) {
							after_load_handler_function(response.data);
						}
						
					}else{
						$scope.pickup_dropoff = [];
					}
				}
			});
		
		
	}
	$scope.pickupDropofTransPrint = function(){
		var data = {"type":$stateParams.search_type,
					"date":$stateParams.search_date};
		
		$scope.pickupDropofTransSearch(data, function(pickup_dropoff){$scope.$parent.$parent.show_print_button = true;});
	}
	
	$scope.departureTransGetScheduleDetail = function(){
		if (!$scope.DATA.schedules_detail){
			$scope.DATA.schedules_detail = {};
		}
		
		if (!$scope.DATA.schedules_detail[$scope.dept.schedule.schedule_code]){
			httpSrv.get({
				url 	: "transport/schedule/detail/"+$scope.dept.schedule.schedule_code,
				success : function(response_detail){
					$scope.DATA.schedules_detail[$scope.dept.schedule.schedule_code] = response_detail.data;
				},
				error : function (response){}
			});
		}
	}

	// obtain pop-up data current customer
	$scope.edit_customer = function(){
		
		$scope.DATA.myCustomer = angular.copy($scope.DATA.current_booking.booking.customer);
		$scope.DATA.myCustomer.error_desc = [];

		// get country list
		//Load Data Country List
		if (!$scope.$root.DATA_country_list){
			httpSrv.post({
				url 	: "general/country_list",
				success : function(response){ $scope.$root.DATA_country_list = response.data.country_list;},
			});
		}
		$("#modal-add-cust .btn-submit-edit-cust").removeAttr("disabled");
	}

	// update current customer
	$scope.update_customer = function (event){
		
		$(event.target).find("button:submit").attr("disabled","disabled");
		
		var data_submit = $scope.DATA.myCustomer;
		data_submit.booking_code = $scope.DATA.current_booking.booking.booking_code;
		
		$scope.DATA.myCustomer.error_desc = [];
		
		httpSrv.post({
			url 	: "transport/booking/update_customer",
			data	: $.param(data_submit),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.loadDataBookingTransportDetail();
					$("#modal-add-cust").modal("hide");
					toastr.success("Updated...");
				}else{
					$scope.DATA.myCustomer.error_desc = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
			}
		});
	}

	// 29122017
	$scope.edit_pickup_dropoff = function(type, booking_detail){
		
		$scope.DATA.myPickupDropoff = {};
		if (type == "PICKUP"){
			$scope.DATA.myPickupDropoff = angular.copy(booking_detail.pickup);
		}else if (type == "DROPOFF"){
			$scope.DATA.myPickupDropoff = angular.copy(booking_detail.dropoff);
		}
		
		//console.log($scope.DATA.myPickupDropoff);
		
		$scope.DATA.myPickupDropoff.voucher_code = booking_detail.voucher_code;
		$scope.DATA.myPickupDropoff.type = type;
		
		httpSrv.post({
			url 	: "transport/schedule/rates_detail_by_id/"+booking_detail.rates.id,
			success : function(response){ 
				
				if (response.data.status == "SUCCESS"){
					if (type == "PICKUP"){
						$scope.DATA.myPickupDropoff.rates_area = response.data.pickup_area;
					}else if (type == "DROPOFF"){
						$scope.DATA.myPickupDropoff.rates_area = response.data.dropoff_area;
					}
					for (_x in $scope.DATA.myPickupDropoff.rates_area){
						if ($scope.DATA.myPickupDropoff.rates_area[_x].area == $scope.DATA.myPickupDropoff.area){
							$scope.DATA.myPickupDropoff.selected_area = $scope.DATA.myPickupDropoff.rates_area[_x];
							break
						}
					}
				}
			},
		});
		
	}

	// 29122017
	$scope.submit_pickup_dropoff = function(event){
		
		$(event.target).find("button:submit").attr("disabled","disabled");
		$scope.DATA.myPickupDropoff.error_desc = [];
		
		var data_submit = {	"type" 				: $scope.DATA.myPickupDropoff.type,
							"area"				: $scope.DATA.myPickupDropoff.selected_area.area,
							"time"				: $scope.DATA.myPickupDropoff.selected_area.time,
							"voucher_code" 		: $scope.DATA.myPickupDropoff.voucher_code,
							"hotel_name" 		: $scope.DATA.myPickupDropoff.hotel_name,
							"hotel_address" 	: $scope.DATA.myPickupDropoff.hotel_address,
							"hotel_phone_number": $scope.DATA.myPickupDropoff.hotel_phone_number,
							"price"				: $scope.DATA.myPickupDropoff.price};
		
		httpSrv.post({
			url 	: "transport/booking/update_pickup_dropoff",
			data	: $.param(data_submit),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					$scope.editDataBookingTransportDetail();
					$("#modal-add-picdrp").modal("hide");
					toastr.success(data_submit.type+" updated...");
				}else{
					$scope.DATA.myPickupDropoff.error_desc = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
			}
		});
	}
	
	// update remarks //03012018
	$scope.update_remarks = function(event){
		var booking_code = $scope.DATA.current_booking.booking.booking_code;
		var remarks		 = $scope.DATA.current_booking.booking.remarks;
		httpSrv.post({
			url 	: "transport/booking/update_remarks",
			data	: $.param({"booking_code": booking_code, "remarks": remarks}),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					toastr.success("Remarks updated!");
				}
			}
		});
	}

	// update current customer
	$scope.update_email_customer = function (event, email){
		
		$(event.target).find("button:submit").attr("disabled","disabled");
		
		var data_submit = $scope.DATA.current_booking.booking.customer.email;
		var booking_code = $scope.DATA.current_booking.booking.booking_code;
		
		$scope.error_msg = [];
		
		httpSrv.post({
			url 	: "transport/booking/update_email_customer",
			data	: $.param({'email':data_submit, 'booking_code': booking_code}),
			success : function(response){
				if (response.data.status == "SUCCESS"){
					// $scope.loadDataBookingTransportDetail();
					$("#payment-detail").modal("hide");
					toastr.success("Updated...");
					if (email == 'invoice') {
						$scope.send_email_profoma($scope.DATA.current_booking.booking.booking_code, '0');
					}else if (email == 'billing') {
						$scope.send_email_billing($scope.DATA.current_booking.booking.booking_code, '0');	
					}else if(email == 'receipt'){
						$scope.send_email_receipt($scope.DATA.current_booking.booking.booking_code, '0', $scope.DATA.current_booking.booking.detail.payment_code)
					}
					
				}else{
					$scope.error_msg = response.data.error_desc;
				}
				$(event.target).find("button:submit").removeAttr("disabled");
			}
		});
	}
});

function activate_sub_menu_agent_detail(class_menu){
	$(".reservation-detail ul.nav.sub-nav li").removeClass("active");
	$(".reservation-detail ul.nav.sub-nav li."+class_menu).addClass("active");
}

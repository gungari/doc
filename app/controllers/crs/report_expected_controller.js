angular.module('app').controller('report_expected_controller', function($scope,$http,httpSrv, $stateParams, $interval, fn, $filter){

	GeneralJS.activateLeftMenu("reports");	
	
	$scope.DATA = {};
	$scope.fn = fn;
	$scope.search = [];

	$scope.printReportExpectedActDate = function(){
		var date = $stateParams.date;
		var paid = $stateParams.paid;
		var year = date;
		var month = date;

		$scope.show_loading_DATA = true;
		var _search = {	
			"date" 		: date,
			"year" 		: year,
			"month"		: month,
			"paid" 		: paid
		};

		
		$scope.date = $filter('date')(date, 'yyyy-MM-d');
		$scope.month = $filter('date')(date, 'MM');
		$scope.year = $filter('date')(date, 'yyyy');

		
		$scope.printDateExpected = true;


	   httpSrv.post({
		url 	: "report/act_expecteddate",
		data	: $.param(_search),
		success : function(response){
			$scope.DATA.expected = response.data.expected.data;
			$scope.show_loading_DATA = false;
			$scope.$parent.$parent.show_print_button = true;
		},
		error : function (response){
			$scope.show_loading_DATA = false;
		}
		})
	}

	$scope.printReportActExpected =  function()
	{
		$scope.printDateExpected = true;
		$scope.show_loading_DATA = true;
		var month = $stateParams.month;
		var year = $stateParams.year;
		var date = $stateParams.date;
		var view = $stateParams.view;
		var booking_source = $stateParams.booking_source;
		var start_date = $stateParams.start_date;
		var finish_date = $stateParams.finish_date;

		$scope.view = view;
		$scope.start_date = $filter('date')(start_date, 'yyyy-MM-d');
		$scope.finish_date = $filter('date')(finish_date, 'yyyy-MM-d');
		$scope.month = month;
		$scope.year = year;

		$scope.show_loading_DATA = true;
		
		var _search = {	
						"start_date" 		: (start_date?start_date:""),
						"finish_date"		: (finish_date?finish_date:""),
						"booking_source" 	: (booking_source?booking_source:""),
						"month" 			: month,
						"year"				: year,
						"date" 				: date,
						"view" 				: view
		};

		httpSrv.post({
			url 	: "report/act_expected",
			data	: $.param(_search),
			success : function(response){
				$scope.show_loading_DATA = false;
				$scope.DATA.expected = response.data.expected.data;
				$scope.DATA.date = response.data.expected.date;
				$scope.$parent.$parent.show_print_button = true;
				
			},
			error : function (response){
				$scope.show_loading_DATA = false;
			}
		})
	}

	$scope.printReportExpectedDate = function(){
		var date = $stateParams.date;
		var paid = $stateParams.paid;
		var year = date;
		var month = date;

		$scope.show_loading_DATA = true;
		var _search = {	
			"date" 		: date,
			"year" 		: year,
			"month"		: month,
			"paid" 		: paid
		};

		
		$scope.date = $filter('date')(date, 'yyyy-MM-d');;

		$scope.printDateExpected = true;


	   httpSrv.post({
		url 	: "report/expecteddate",
		data	: $.param(_search),
		success : function(response){
			$scope.DATA.expected = response.data.expected.data;
			$scope.show_loading_DATA = false;
			$scope.$parent.$parent.show_print_button = true;
		},
		error : function (response){
			$scope.show_loading_DATA = false;
		}
		})
	}
	
	$scope.printReportExpected =  function()
	{
		$scope.show_loading_DATA = true;
		var month = $stateParams.month;
		var year = $stateParams.year;
		var date = $stateParams.date;
		var view = $stateParams.view;
		var booking_source = $stateParams.booking_source;
		var start_date = $stateParams.start_date;
		var finish_date = $stateParams.finish_date;

		$scope.view = view;
		$scope.start_date = $filter('date')(start_date, 'yyyy-MM-d');
		$scope.finish_date = $filter('date')(finish_date, 'yyyy-MM-d');
		$scope.month = month;
		$scope.year = year;

		$scope.show_loading_DATA = true;
		
		var _search = {	
						"start_date" 		: (start_date?start_date:""),
						"finish_date"		: (finish_date?finish_date:""),
						"booking_source" 	: (booking_source?booking_source:""),
						"month" 			: month,
						"year"				: year,
						"date" 				: date,
						"view" 				: view
		};

		httpSrv.post({
			url 	: "report/expected",
			data	: $.param(_search),
			success : function(response){
				$scope.show_loading_DATA = false;
				$scope.DATA.expected = response.data.expected.data;
				$scope.DATA.date = response.data.expected.date;
				$scope.$parent.$parent.show_print_button = true;
				
			},
			error : function (response){
				$scope.show_loading_DATA = false;
			}
		})
	}

	$scope.loadReportExpected = function()
	{
		//Load Data Booking Source
		if (!$scope.DATA_booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.DATA_booking_source = response.data.booking_source; },
			});
		}
		
		start_date = new Date(),
		end_date = new Date();
		// start_date.setDate(start_date.getDate() - 30);
		$scope.search.start_date = $filter('date')(start_date, 'yyyy-MM-d');
		$scope.search.end_date = $filter('date')(end_date, 'yyyy-MM-d');
		$scope.show_loading_DATA = true;
		$scope.search.year = $filter('date')(new Date(), 'yyyy');
		$scope.search.month = $filter('date')(new Date(), 'M');
		$scope.search.date = $filter('date')(new Date(), 'd');

		//$scope.loadDateExpected($scope.search.year,$scope.search.month,$scope.search.date);
		$scope.printDateExpected = false;

		$scope.search.view = "date";
		$scope.trips_isChecked = '2';

		$scope.loadReportExpectedData();
	}

	$scope.loadReportActExpected = function()
	{
		//Load Data Booking Source
		if (!$scope.DATA_booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.DATA_booking_source = response.data.booking_source; },
			});
		}
		
		start_date = new Date(),
		end_date = new Date();
		// start_date.setDate(start_date.getDate() - 30);
		$scope.search.start_date = $filter('date')(start_date, 'yyyy-MM-d');
		$scope.search.end_date = $filter('date')(end_date, 'yyyy-MM-d');
		$scope.show_loading_DATA = true;
		$scope.search.year = $filter('date')(new Date(), 'yyyy');
		$scope.search.month = $filter('date')(new Date(), 'M');
		$scope.search.date = $filter('date')(new Date(), 'd');

		//$scope.loadDateExpected($scope.search.year,$scope.search.month,$scope.search.date);

		$scope.search.view = "date";
		$scope.trips_isChecked = '2';

		$scope.loadReportActExpectedData();
	}

	$scope.getDateExpected = function(date){
		var paid = $scope.trips_isChecked;
		$scope.search.date = date;
		
		var year = $filter('date')(date, 'yyyy');
		var month = $filter('date')(date, 'M');
		//$scope.search.date = $filter('date')(new Date(), 'd');

		$scope.is_loading = true;
		var _search = {	
			"date" 		: date,
			"year" 		: year,
			"month"		: month,
			"paid" 		: paid
		};

		$scope.printDateExpected = true;

	   httpSrv.post({
		url 	: "report/expecteddate",
		data	: $.param(_search),
		success : function(response){
			$scope.DATA.expected = response.data.expected.data;
			$scope.is_loading = false;
		},
		error : function (response){
			$scope.is_loading = false;
		}
		})
	}

	$scope.getDateActExpected = function(date){
		$scope.printDateExpected = true;
		var paid = $scope.trips_isChecked;
		$scope.search.date = date;
		$scope.is_loading = true;
		var _search = {	
			"date" 		: date,
			"paid" 		: paid
		};


	   httpSrv.post({
		url 	: "report/actexpecteddate",
		data	: $.param(_search),
		success : function(response){
			$scope.DATA.expected = response.data.expected.data;
			$scope.is_loading = false;
		},
		error : function (response){
			$scope.is_loading = false;
		}
		})
	}

	$scope.loadReportActExpectedData = function()
	{
		
		var paid = $scope.trips_isChecked;
		$scope.printDateExpected = false;
		if (!$scope.search.month) $scope.search.month = $scope.month.toString();
		if (!$scope.search.year) $scope.search.year = $scope.year.toString();
		if (!$scope.search.date) $scope.search.date = $scope.date.toString();
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		$scope.is_loading = true;
		$scope.show_graph = false;

		var _search = {	
						"month" 	: $scope.search.month,
						"year"		: $scope.search.year,
						// "date" 		: $scope.search.date,
						"start_date" 	: $scope.search.start_date,
						"finish_date" 	: $scope.search.end_date,
						"view" 		: $scope.search.view,
						// "paid" 		: paid
						"source" 		: $scope.search.booking_source,
					  };
		

		   httpSrv.post({
			url 	: "report/act_expected",
			data	: $.param(_search),
			success : function(response){
				
				$scope.DATA.expected = response.data.expected.data;
				$scope.DATA.date = response.data.expected.date;
				$scope.is_loading = false;
				
			},
			error : function (response){
				$scope.is_loading = false;
			}
			})

	}

	$scope.loadReportExpectedData = function()
	{
		$scope.printDateExpected = false;
		var paid = $scope.trips_isChecked;
		
		if (!$scope.search.month) $scope.search.month = $scope.month.toString();
		if (!$scope.search.year) $scope.search.year = $scope.year.toString();
		if (!$scope.search.date) $scope.search.date = $scope.date.toString();
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
		$scope.is_loading = true;
		$scope.show_graph = false;

		var _search = {	
						"month" 	: $scope.search.month,
						"year"		: $scope.search.year,
						// "date" 		: $scope.search.date,
						"start_date" 	: $scope.search.start_date,
						"finish_date" 	: $scope.search.end_date,
						"view" 		: $scope.search.view,
						// "paid" 		: paid
						"source" 		: $scope.search.booking_source,
					  };
		

		   httpSrv.post({
			url 	: "report/expected",
			data	: $.param(_search),
			success : function(response){
				
				$scope.DATA.expected = response.data.expected.data;
				$scope.DATA.date = response.data.expected.date;
				$scope.is_loading = false;
				
			},
			error : function (response){
				$scope.is_loading = false;
			}
			})

	}

	$scope.trips_isChecked =  false;
	$scope.trip_change = function(trips){

		if(trips.isChecked) {
			$scope.trips_isChecked =  true;
			
		} else {
		   $scope.trips_isChecked =  false;
		}
		
	}

});
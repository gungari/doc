angular.module('app').controller('report_revenue_controller', function($scope, $http, httpSrv, $stateParams, $interval, fn, $filter){
	GeneralJS.activateLeftMenu("reports");	
	
	$scope.DATA = {};
	$scope.fn = fn;
	$scope.search = [];
	

	$scope.openPopUpWindowVoucher = function (base_url, booking_code, voucher_code){
		var myWindow = window.open(base_url+booking_code+'/'+voucher_code, 'VoucherWindow', 'width=800,height=600');
	}

	$scope.loadReportRevenue = function()
	{

		$scope.show_loading_DATA = true;
		$scope.search.year = $filter('date')(new Date(), 'yyyy');
		$scope.search.month = $filter('date')(new Date(), 'MM');
		$scope.search.date = $filter('date')(new Date(), 'dd');
		$scope.search.view = "month";
		$scope.show_graph = true;
		$scope.date_data = false;
		$scope.month_data = true;
		$scope.show_error = false;

		$scope.loadReportRevenueTrans();
	}

	$scope.loadReportSalesData = function(date){
		date = $filter('date')(date, 'yyyy-M-d');
		var date = date.split("-", 3);

		$scope.search.year = date[0];
		$scope.search.month = date[1];
		$scope.search.date = date[2];
		$scope.search.view = "date";

		$scope.show_graph = false;
		
		$scope.month_data = false;
		$scope.show_loading_DATA = true;
		$scope.show_error = false;
		

		var _search = {	
						"start_date" 		: ($scope.search.date?$scope.search.date:""),
						"group_by_trip"		: "1",
						"booking_source"	: ($scope.search.booking_source?$scope.search.booking_source:""),
						"month" 			: $scope.search.month,
						"year"				: $scope.search.year,
						"date" 				: $scope.search.date,
						"view" 				: $scope.search.view
		};

		httpSrv.post({
			url 	: "transport/report/revenue",
			data	: $.param({"search":_search}),
			success : function(response){

				$scope.show_loading_DATA = false;
				if (response.data.status == "SUCCESS") {

					$scope.date_data = true;
					$scope.DATA = response.data;
					$scope.filter_date = $scope.DATA.search.start_date;
					
				}else{
					$scope.show_error = true;
					$scope.date_data = false;
				}
				
			},
			error : function (response){}
		});

	}

	$scope.loadReportRevenueTrans = function(page){

		$scope.show_loading_DATA = true;
		$scope.show_error = false;
		if (!$scope.search) $scope.search = {}
		
		if (!$scope.$root.DATA_booking_source){
			httpSrv.post({
				url 	: "setting/booking_source",
				success : function(response){ $scope.$root.DATA_booking_source = response.data.booking_source;},
			});
		}
		
		if (!page) page = 1;
		var _search = {	"page" 				: page,
						"start_date" 		: ($scope.search.date?$scope.search.date:""),
						"group_by_trip"		: "1",
						"booking_source"	: ($scope.search.booking_source?$scope.search.booking_source:""),
						"month" 			: $scope.search.month,
						"year"				: $scope.search.year,
						"date" 				: $scope.search.date,
						"view" 				: $scope.search.view
		};

		if ($scope.search.booking_source) _search.booking_source = $scope.search.booking_source;
		if ($scope.search.view == 'date') {
			$scope.date_data = true;
			$scope.month_data = false;
			$scope.show_graph = false;
		}else{
			$scope.date_data = false;
			$scope.month_data = true;
			$scope.show_graph = true;
		}
		httpSrv.post({
			url 	: "transport/report/revenue",
			data	: $.param({"search":_search}),
			success : function(response){
				$scope.show_loading_DATA = false;
				if (response.data.status == "SUCCESS") {
					if (response.data.search){
						$scope.DATA = response.data;
						$scope.filter_date = $scope.DATA.search.start_date;
						$scope.date_data = true;
						$scope.month_data = false;
						$scope.show_graph = false;
					}else{
						console.log('DATA: ',response.data.sales);
						$scope.total_data(response.data.sales);
						$scope.DATA.sales = response.data.sales;
						$scope.graph(response.data.graph);
					}
				}else{
					$scope.show_error = true;
					$scope.month_data = false;
					$scope.date_data = false;
				}
				
			},
			error : function (response){}
		});
		
		$(".datepicker").datepicker({dateFormat :"yy-mm-dd"});
	}

	$scope.total_data = function(data){

		$scope.total = 0;
		$scope.currency = '';
		var total = 0;
	    angular.forEach(data, function(row){
	    	total += parseInt(row.sources.total); 
	    	if ($scope.currency == '') {
	    		$scope.currency = row.sources.currency;
	    	}
	   	});
	   
	    $scope.total = total;
	    
	}


	$scope.graph = function($object){
		// console.log("graph in", $object.rows[0]['c']);
		$scope.chartObject = {};

		var maxNumber = 10;

	   	for(var i=0; i<$object.rows.length; i++){
			
			for(var j=1; j<$object.rows[i]['c'].length; j++){
				if($object.rows[i]['c'][j]['v'] > maxNumber){
					maxNumber = $object.rows[i]['c'][j]['v'];
				}
			}
		}
		
	 	$scope.chartObject.data = $object;
	    
	    $scope.chartObject.type = "LineChart";
	    $scope.chartObject.options = {
	    	vAxis: {format: 'decimal', minValue: 0, maxValue: maxNumber, gridlines: {count: -1}, textStyle: {fontSize: 10}},
			hAxis: {textStyle: {fontSize: 8}},
	        'title': 'Revenue',
	        legend: { position: 'bottom' },
	        fontSize: 10,
	        responsive: true,
	        chartArea: {width: '85%', heigth: '100%'},
	       
	    }
	}

	$scope.loadReportCancell = function()
	{

		$scope.show_loading_DATA = true;
		$scope.search.year = $filter('date')(new Date(), 'yyyy');
		$scope.search.month = $filter('date')(new Date(), 'MM');
		$scope.search.date = $filter('date')(new Date(), 'dd');
		$scope.search.view = "month";
		$scope.show_graph = false;
		$scope.date_data = true;
		$scope.show_error = false;
		$scope.loadReportCancellData();
	}

	$scope.loadReportCancellData = function()
	{
		
		if (!$scope.search.month) $scope.search.month = $scope.month.toString();
		if (!$scope.search.year) $scope.search.year = $scope.year.toString();
		if (!$scope.search.date) $scope.search.date = $scope.date.toString();
		$scope.show_loading_DATA = true;
		$scope.show_graph = false;

		var _search = {	
						"month" 	: $scope.search.month,
						"year"		: $scope.search.year,
						"date" 		: $scope.search.date,
						"view" 		: $scope.search.view
					  };
		if ($scope.search.view != 'date') {
			//$scope.show_graph = true;
			$scope.date_data = false;
			$scope.date_data_agent = false;
		}
		else{
			//$scope.show_graph = false;
			$scope.date_data = true;
			$scope.date_data_agent = false;
		}
		httpSrv.post({
			url 	: "transport/report/sales_cancell",
			data	: $.param(_search),
			success : function(response){
				console.log('hollass', response.data);
				if (response.data.status == "SUCCESS") {
					$scope.show_loading_DATA = false;
					$scope.report = response.data;
					$scope.getTotal(response.data.sales);
					$scope.getTotalAgent(response.data.agent);
					//$scope.chartObject.data = response.graph;
					$scope.graph2(response.data.graph);
					$scope.show_error = false;
					
					
					if ($scope.search.view == 'date') {
						console.log('date!!!');
						$scope.getTotalDate(response.data);
						$scope.date_data_agent = false;
					}else{
						console.log('not date!!!');
						$scope.getTotalSales(response.data.sales);
						$scope.total_books(response.data.sales);
						$scope.total_guests(response.data.sales);
						$scope.show_graph = true;
					}
					console.log('report', $scope.report);
				}else{
					$scope.report = [];
					$scope.show_loading_DATA = false;
					$scope.show_graph = false;
					$scope.show_error = true;
					
				}
				
			},
			error : function (response){}
			})
	}

	$scope.loadReportAgent = function()
	{

		$scope.show_loading_DATA = true;
		$scope.search.year = $filter('date')(new Date(), 'yyyy');
		$scope.search.month = $filter('date')(new Date(), 'MM');
		$scope.search.date = $filter('date')(new Date(), 'dd');
		$scope.search.view = "month";
		$scope.show_graph = false;
		$scope.date_data = true;
		
		$scope.loadReportAgentData();
	}

	$scope.loadReportAgentData = function()
	{
		
		if (!$scope.search.month) $scope.search.month = $scope.month.toString();
		if (!$scope.search.year) $scope.search.year = $scope.year.toString();
		if (!$scope.search.date) $scope.search.date = $scope.date.toString();

		$scope.show_graph = false;

		var _search = {	
						"month" 	: $scope.search.month,
						"year"		: $scope.search.year,
						"date" 		: $scope.search.date,
						"view" 		: $scope.search.view
					  };
		
		httpSrv.post({
			url 	: "transport/report/agent",
			data	: $.param(_search),
			success : function(response){
				
				$scope.show_loading_DATA = false;
				if (response.data.status == "SUCCESS") {
					$scope.show_error = false;
					$scope.agent = response.data.sales;
					$scope.graph7(response.data.graph);
					$scope.getTotalAgent2(response.data.sales);
				}else{
					
					$scope.show_error = true;
				}
				
			},
			error : function (error){
				$scope.show_error = true;
				$scope.show_loading_DATA = false;
			}
		})
	}

	$scope.getTotal = function(data){
	    var total = 0;
	    angular.forEach(data.sources, function(row){
	    total += parseInt(row.total); 
	    //console.log("total", row.sales);
	    	   });
	   
	    $scope.total = total;
	   
	}
	$scope.getTotalDate = function(data){
	    var total = 0;
	     var total_trip = 0;
	     var currency = '';

	    angular.forEach(data.sales, function(row){
	    	total += parseInt(row.total);
	    	total_trip += parseInt(row.book);

	    	currency = row.currency;
	    });
	   
	    $scope.total_date = total;
	    $scope.total_trip = total_trip;

	    $scope.currency = currency;
	   
	}
	$scope.getTotalAgent = function(data){

	    var total = 0;
	    angular.forEach(data, function(row){
	    total += parseInt(row.total);

	    });

	    $scope.totalagent = total;
	}
	$scope.getTotalAgent2 = function(data){

	    var total = 0;
	    var book = 0;
	    var guest = 0;
	    angular.forEach(data, function(row){
	    	total += parseInt(row.total);
	    	book += parseInt(row.book);
	    	guest += parseInt(row.guest);

	    });

	    $scope.total_book = book;
	    $scope.totalagent = total;
	    $scope.total_guest = guest;
	}
	$scope.getTotalSales = function(data){
	    var total = 0;
	    var total_guest = 0;
	    var total_book = 0;
	    var currency = '';

	    angular.forEach(data, function(row){

	    	total += parseInt(row.sources.total); 
	    	total_guest += parseInt(row.sources.total_guest);
			total_book += parseInt(row.sources.total_book);

			currency = row.sources.currency;
	    
	    });
	   
	    $scope.total_sales = total;
	    $scope.total_guest = total_guest;
	    $scope.total_book = total_book;
	    $scope.currency = currency;
	   // console.log('total',  data);
	   
	}
	$scope.total_books = function(data)
	{
		var total_agent = 0;
		var total_offline = 0;
		var total_online = 0;
		angular.forEach(data, function(row){
			//total += parseInt(row.sources);
			total_agent += parseInt(row.sources['AGENT'].book);
			total_offline += parseInt(row.sources['OFFLINE'].book);
			total_online += parseInt(row.sources['ONLINE'].book);
		});

		$scope.total_book_agent = total_agent;
		$scope.total_book_offline = total_offline;
		$scope.total_book_online = total_online;
	}

	$scope.total_guests = function(data)
	{
		var total_agent = 0;
		var total_offline = 0;
		var total_online = 0;
		angular.forEach(data, function(row){
			//total += parseInt(row.sources['AGENT'].total_guest);
			total_agent += parseInt(row.sources['AGENT'].guest);
			total_offline += parseInt(row.sources['OFFLINE'].guest);
			total_online += parseInt(row.sources['ONLINE'].guest);
		});
		
		$scope.total_guest_agent = total_agent;
		$scope.total_guest_offline = total_offline;
		$scope.total_guest_online = total_online;
	}
	$scope.graph2 = function($object){
		console.log("graph in", $object.rows[0]['c']);
		$scope.chartObject = {};
		var maxNumber = 10;
		for(var i=0; i<$object.rows.length; i++){
			
			for(var j=1; j<$object.rows[i]['c'].length; j++){
				if($object.rows[i]['c'][j]['v'] > maxNumber){
					maxNumber = $object.rows[i]['c'][j]['v'];
				}
			}
		}
		
	    $scope.chartObject.data = $object;
	   
	    $scope.chartObject.type = "LineChart";
	    $scope.chartObject.options = {
	    	vAxis: {format: 'decimal', minValue: 0, maxValue: maxNumber, gridlines: {count: -1}, textStyle: {fontSize: 10}},
			hAxis: {textStyle: {fontSize: 8}},
	        'title': 'Booking',
	        legend: { position: 'bottom' },
	        fontSize: 10,
	        responsive: true,
	        chartArea: {width: '85%', heigth: '100%'},
	       
	    }

	  //  $scope.loading(true);
	}
	$scope.graph7 = function($object){
		
		$scope.chartObject4 = {};
		//var maxNumber = 10;
		// for(var i=0; i<$object.rows.length; i++){
			
		// 	for(var j=1; j<$object.rows[i]['c'].length; j++){
		// 		if($object.rows[i]['c'][j]['v'] > maxNumber){
		// 			maxNumber = $object.rows[i]['c'][j]['v'];
		// 		}
		// 	}
		// }
		
	   $scope.chartObject4.data = $object;

	   
	    $scope.chartObject4.type = "BarChart";
	    $scope.chartObject4.options = {
	    	
	        'title': 'Most Active Agencies',
	        legend: { position: 'bottom' },
	        fontSize: 10,
	        responsive: true,
	        chartArea: {width: '40%', heigth: '100%'},
	       
	    }

	  //  $scope.loading(true);
	}
});
function activate_sub_menu_agent_detail(class_menu){
	$(".agent-detail ul.nav.sub-nav li").removeClass("active");
	$(".agent-detail ul.nav.sub-nav li."+class_menu).addClass("active");
}